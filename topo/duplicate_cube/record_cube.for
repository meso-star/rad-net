      subroutine record_cube_definition_file(dim,
     &     cube_file,lambda_file,
     &     Npix,corner_theta,corner_phi,corner_z,brdf,
     &     Nlambda,lambda,reflectivity)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a pixel zone file
c     
c     Input:
c       + dim: dimension of space
c       + cube_file: pixel zone definition file
c       + lambda_file: spectral mesh definition file
c       + Npix: number of pixel in theta/phi directions
c       + corner_theta: latitude of each corner for each pixel [deg]
c       + corner_phi: longitude of each corner for each pixel [deg]
c       + corner_z: altitude of each corner for each pixel [m]
c       + brdf: BRDF for each pixel [0/1]
c       + Nlambda: number of wavelength
c       + lambda: values of the wavelength [nm]
c       + reflectivity: reflectivity for each pixel, for each wavelength
c     
c     Output: the required cube_file and lambda_file
c     
c     I/O
      integer dim
      character*(Nchar_mx) cube_file
      character*(Nchar_mx) lambda_file
      integer Npix(1:2)
      double precision corner_theta(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf(1:Npix_mx,1:Npix_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
c     temp
      integer i,j,Ncol,Nline,Nll,iline,nread,icorner
      integer ipix,jpix,line_index,ilambda
      logical keep_reading
      double precision tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine record_cube_definition_file'

      open(11,file=trim(lambda_file))
      do i=1,Nlambda
         write(11,*) lambda(i)*1.0D-3 ! µm
      enddo                     ! i
      close(11)
      
      open(12,file=trim(cube_file))
      Ncol=6
      Nline=Npix(1)/Ncol
      Nll=modulo(Npix(1),Ncol)
      line_index=0
c     longitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               write(12,*) (corner_phi(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               write(12,*) (corner_phi(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         write(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     latitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               write(12,*) (corner_theta(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               write(12,*) (corner_theta(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         write(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     altitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               write(12,*) (corner_z(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               write(12,*) (corner_z(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         write(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     BRDF of each pixel
      do jpix=1,Npix(2)
         nread=0
         do i=1,Nline
            write(12,*) (brdf(nread+j,jpix),j=1,Ncol) ! deg
            nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               write(12,*) (brdf(nread+j,jpix),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
      enddo                     ! jpix
      write(12,*)
      line_index=line_index+1
c     reflectivity of each pixel
      do ilambda=1,Nlambda
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               write(12,*) (reflectivity(nread+j,jpix,ilambda),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               write(12,*) (reflectivity(nread+j,jpix,ilambda),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         if (ilambda.lt.Nlambda) then
            write(12,*)
            line_index=line_index+1
         endif
      enddo                     ! ilambda
      close(12)
      
      return
      end
