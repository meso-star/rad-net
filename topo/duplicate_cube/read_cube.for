      subroutine read_cube_definition_file(dim,
     &     file_in,lambda_file,
     &     Npix,corner_theta,corner_phi,corner_z,brdf,
     &     Nlambda,lambda,reflectivity)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a pixel zone file
c     
c     Input:
c       + dim: dimension of space
c       + file_in: pixel zone definition file
c       + lambda_file: spectral mesh definition file
c     
c     Output:
c       + Npix: number of pixel in theta/phi directions
c       + corner_theta: latitude of each corner for each pixel [deg]
c       + corner_phi: longitude of each corner for each pixel [deg]
c       + corner_z: altitude of each corner for each pixel [m]
c       + brdf: BRDF for each pixel [0/1]
c       + Nlambda: number of wavelength
c       + lambda: values of the wavelength [nm]
c       + reflectivity: reflectivity for each pixel, for each wavelength
c     
c     I/O
      integer dim
      character*(Nchar_mx) file_in
      character*(Nchar_mx) lambda_file
      integer Npix(1:2)
      double precision corner_theta(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf(1:Npix_mx,1:Npix_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
c     temp
      integer ios
      integer i,j,Ncol,Nline,Nll,iline,nread,icorner
      integer ipix,jpix,line_index,ilambda
      logical keep_reading
      double precision tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine read_cube_definition_file'

      open(11,file=trim(lambda_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(lambda_file)
         stop
      endif
      Nlambda=0
      keep_reading=.true.
      do while (keep_reading)
         read(11,*,iostat=ios) tmp1 ! µm
         if (ios.eq.0) then
            Nlambda=Nlambda+1
            if (Nlambda.gt.Nlambda_mx) then
               call error(label)
               write(*,*) 'Nlambda_mx was reached'
               stop
            endif
            lambda(Nlambda)=tmp1*1.0D+3 ! nm
         else
            keep_reading=.false.
         endif
      enddo                     ! keep_reading
      close(11)
c     Debug
c      write(*,*) 'Nlambda=',Nlambda
c      do ilambda=1,Nlambda
c         write(*,*) 'lambda(',ilambda,')=',lambda(ilambda)
c      enddo                     ! ilambda
c     Debug
      
      open(12,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(file_in)
         stop
      endif
      Npix(1)=60
      Npix(2)=64
      Nlambda=50
      Ncol=6
      Nline=Npix(1)/Ncol
      Nll=modulo(Npix(1),Ncol)
      line_index=0
c     longitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               read(12,*) (corner_phi(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (corner_phi(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         read(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     Debug
c     write(*,*) 'After reading phi: line_index=',line_index
c      j=1
c      do i=1,Npix(1)-1
c         write(*,*) corner_phi(i,j,2),corner_phi(i+1,j,1)
c      enddo                     ! j
c     Debug
c     latitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               read(12,*) (corner_theta(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (corner_theta(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         read(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     Debug
c      write(*,*) 'After reading theta: line_index=',line_index
c      i=1
c      do j=1,Npix(2)-1
c         write(*,*) corner_theta(i,j,4),corner_theta(i,j+1,1)
c      enddo ! i
c     Debug
c     altitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               read(12,*) (corner_z(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (corner_z(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         read(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     Debug
c      write(*,*) 'After reading z: line_index=',line_index
c     Debug
c     BRDF of each pixel
      do jpix=1,Npix(2)
         nread=0
         do i=1,Nline
            read(12,*) (brdf(nread+j,jpix),j=1,Ncol) ! deg
            nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (brdf(nread+j,jpix),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
      enddo                     ! jpix
      read(12,*)
      line_index=line_index+1
c     Debug
c      write(*,*) 'After reading brdf: line_index=',line_index
c      write(*,*) 'brdf(',Npix(1),',',Npix(2),')=',brdf(Npix(1),Npix(2))
c     Debug

c     reflectivity of each pixel
      do ilambda=1,Nlambda
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               read(12,*) (reflectivity(nread+j,jpix,ilambda),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (reflectivity(nread+j,jpix,ilambda),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         if (ilambda.lt.Nlambda) then
            read(12,*)
            line_index=line_index+1
         endif
      enddo                     ! ilambda
      close(12)

c     Debug
c      write(*,*) 'After reading reflectivity: line_index=',line_index
c      write(*,*) 'reflectivity(',Npix(1),',',Npix(2),',',Nlambda,')=',
c     &     reflectivity(Npix(1),Npix(2),Nlambda)
c     Debug

      
      return
      end


      
      subroutine position_is_in_pixel(dim,
     &     x,Npix,corner_theta,corner_phi,
     &     is_in_pixel,Npixel,pixel_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify whether or not a given (theta,phi) position is in a pixel
c     
c     Input:
c       + dim: dimension of space
c       + x: (phi,theta) position [deg, deg]
c       + Npix: number of pixels along theta/phi directions
c       + corner_theta: value of theta for each corner of each pixel
c       + corner_phi: value of phi for each corner of each pixel
c     
c     Output:
c       + is_in_pixel: true if "x" is inside at least one pixel
c       + Npixel: number of pixels position "x" belongs to
c       + pixel_index: index of each pixels "x" belgons to, when is_in_pixel=T
c     
c     I/O
      integer dim
      double precision x(1:Ndim_mx-1)
      integer Npix(1:2)
      double precision corner_theta(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Npix_mx,1:Npix_mx,1:4)
      logical is_in_pixel
      integer Npixel
      integer pixel_index(1:Npix_mx,1:2)
c     temp
      integer ipix,jpix,icorner
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision P(1:Ndim_mx)
      logical inside
      integer Npixels
c     label
      character*(Nchar_mx) label
      label='subroutine position_is_in_pixel'

      Ncontour=1
      Nppc(1)=4
      P(1)=x(1)                 ! phi
      P(2)=x(2)                 ! theta
      P(3)=0.0D+0
      is_in_pixel=.false.
      Npixel=0
      do ipix=1,Npix(1)
         do jpix=1,Npix(2)
            do icorner=1,Nppc(1)
c     VIMS pixels are defined clockwise while all contours should
c     be defined counter-clockwise
               contour(1,icorner,1)=
     &              corner_phi(ipix,jpix,Nppc(1)-icorner+1)
               contour(1,icorner,2)=
     &              corner_theta(ipix,jpix,Nppc(1)-icorner+1)
            enddo               ! icorner
            call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,
     &           1,P,inside)
            if (inside) then
               is_in_pixel=.true.
               Npixel=Npixel+1
               if (Npixel.gt.Npix_mx) then
                  call error(label)
                  write(*,*) 'Npix_mx has been reached'
                  stop
               endif
               pixel_index(Npixel,1)=ipix
               pixel_index(Npixel,2)=jpix
            endif
         enddo                  ! jpix
      enddo                     ! ipix
c     
      return
      end
      


      subroutine higher_power2(int_in,n)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find the closest-higher value of 2^n
c     from a given integer
c     
c     Input:
c       + int_in: input integer
c     
c     Output:
c       + n: the lowest possible integer such that 2^n > int_in
c     
c     I/O
      integer int_in
      integer n
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine higher_power2'

      n=0
      do while (2**n.lt.int_in)
         n=n+1
      enddo

      return
      end
