      program duplicate_cube
      implicit none
      include 'max.inc'
c     
c     Purpose: to duplicate a VIMS data cube, with a possibility to move it
c     
c     Variables
      integer dim
      character*(Nchar_mx) original_cube_file
      character*(Nchar_mx) original_lambda_file
      character*(Nchar_mx) duplicated_cube_file
      character*(Nchar_mx) duplicated_lambda_file
      double precision delta_theta,delta_phi
      logical cube_file_exists,lambda_file_exists
      integer Npix(1:2)
      double precision corner_theta(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf(1:Npix_mx,1:Npix_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
      double precision min_theta,min_phi
      double precision max_theta,max_phi
      double precision theta_limit(1:2)
      double precision phi_limit(1:2)
      integer ipix,jpix,icorner,i,j
      logical out_of_bounds
c     label
      character*(Nchar_mx) label
      label='program duplicate_cube'

      dim=3                     ! dimension of space

c     File holding the original information (VIMS cube)
      original_cube_file='../Surf_input_HTRDR_VIMS_CM_1764550739_16.cub.dat'
c     File holding the original spectral mesh
      original_lambda_file='../lambda_cube01.dat'
c     Name of the copy of the VIMS cube data
      duplicated_cube_file='../VIMS_cube04.dat'
c     Name of the copy of the spectra mesh
      duplicated_lambda_file='../lambda_cube04.dat'
c     longitude shift [deg]
      delta_phi=10.0D+0
c     latitude shift [deg]
      delta_theta=-5.0D+0

      inquire(file=trim(original_cube_file),exist=cube_file_exists)
      if (.not.cube_file_exists) then
         call error(label)
         write(*,*) 'File not found: ',trim(original_cube_file)
         stop
      endif
      inquire(file=trim(original_lambda_file),exist=lambda_file_exists)
      if (.not.lambda_file_exists) then
         call error(label)
         write(*,*) 'File not found: ',trim(original_lambda_file)
         stop
      endif
      call read_cube_definition_file(dim,
     &     original_cube_file,original_lambda_file,
     &     Npix,corner_theta,corner_phi,corner_z,brdf,
     &     Nlambda,lambda,reflectivity)
c     Find (phi/theta) limits
      max_theta=corner_theta(1,1,1)
      max_phi=corner_phi(1,1,1)
      min_theta=corner_theta(1,1,1)
      min_phi=corner_phi(1,1,1)
      do ipix=1,Npix(1)
         do jpix=1,Npix(2)
            do icorner=1,4
               if (corner_theta(ipix,jpix,icorner).gt.max_theta) then
                  max_theta=corner_theta(ipix,jpix,icorner)
               endif
               if (corner_theta(ipix,jpix,icorner).lt.min_theta) then
                  min_theta=corner_theta(ipix,jpix,icorner)
               endif
               if (corner_phi(ipix,jpix,icorner).gt.max_phi) then
                  max_phi=corner_phi(ipix,jpix,icorner)
               endif
               if (corner_phi(ipix,jpix,icorner).lt.min_phi) then
                  min_phi=corner_phi(ipix,jpix,icorner)
               endif
            enddo               ! icorner
         enddo                  ! jpix
      enddo                     ! ipix
c     Adjust longitudes to [0-360]°
      if (min_phi.lt.0.0D+0) then
         min_phi=min_phi+360.0D+0
         max_phi=max_phi+360.0D+0
         do ipix=1,Npix(1)
            do jpix=1,Npix(2)
               do icorner=1,4
                  corner_phi(ipix,jpix,icorner)=
     &                 corner_phi(ipix,jpix,icorner)+360.0D+0
               enddo            ! icorner
            enddo               ! jpix
         enddo                  ! ipix
      endif                     ! min_theta < 0
c     Absolute limits (taking into account the space needed for transitioning toward the global 1° planetary grid)
      theta_limit(1)=min_theta-1.0D+0
      theta_limit(2)=max_theta+1.0D+0
      phi_limit(1)=min_phi-1.0D+0
      phi_limit(2)=max_phi+1.0D+0
c     Debug
c      write(*,*) 'theta_limit=',theta_limit
c      write(*,*) 'phi_limit=',phi_limit
c     Debug
c     check that the required translation is possible
      out_of_bounds=.false.
      do j=1,2
         if (((theta_limit(j)+delta_theta).lt.-89.0D+0).or.((theta_limit(j)+delta_theta).gt.89.0D+0)) then
            out_of_bounds=.true.
            goto 111
         endif
         if (((phi_limit(j)+delta_phi).lt.0.0D+0).or.((phi_limit(j)+delta_phi).gt.360.0D+0)) then
            out_of_bounds=.true.
            goto 111
         endif
      enddo                     ! j
 111  continue
      if (out_of_bounds) then
         call error(label)
         write(*,*) 'The required translation is not possible'
         stop
      else
         write(*,*) 'Translation is accepted'
      endif
c     Translation
      do ipix=1,Npix(1)
         do jpix=1,Npix(2)
            do icorner=1,4
               corner_theta(ipix,jpix,icorner)=corner_theta(ipix,jpix,icorner)+delta_theta
               corner_phi(ipix,jpix,icorner)=corner_phi(ipix,jpix,icorner)+delta_phi
            enddo               ! icorner
         enddo                  ! jpix
      enddo                     ! ipix
      call record_cube_definition_file(dim,
     &     duplicated_cube_file,duplicated_lambda_file,
     &     Npix,corner_theta,corner_phi,corner_z,brdf,
     &     Nlambda,lambda,reflectivity)
      write(*,*) 'Files have been recorded:'
      write(*,*) trim(duplicated_cube_file)
      write(*,*) trim(duplicated_lambda_file)
      
      end
