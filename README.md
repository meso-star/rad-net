# Rad-Net

Ce projet a pour but de stocker tous les documents de travail, les données et programmes utilisées à titre d'exemple, dans le cadre de la prestation de Méso-Star pour le projet ANR Rad-Net.

Contenu:

+ radnet_models: document de travail afin de préparer la concertation à propos des modèles physiques à mettre en place.

+ obj_format_description: document de travail afin de décrire le format des fichiers .obj qui sera utilisé pour décrire la géométrie ainsi que les propriétés radiatives des matériaux

+ spherical_mesh: programme proposant un exemple de mise en oeuvre de chaîne de production de fichiers de géométrie utilisables par HTRDR.

Compilation: aller dans le répertoire spherical_mesh:

\>cd spherical_mesh

Eventuellement éditer le fichier Makefile pour modifier les options de compilation pour une architecture particulière.

Puis compiler:

\>make all

A ce stade l'exécutable "spherical_mesh.exe" a du être produit.

Le guide de l'utilisateur (spherical_mesh/Doc/user_guide.pdf) fournit les informations relatives à l'utilisation du programme.

Pour les plus pressés: éventuellement éditer le fichier "data.in", puis lancer l'exécution:

\>./spherical_mesh.exe

Les fichiers de sortie sont dans le répertoire spherical_mehs/results; un script "make_archive.bash" permet de stocker toutes les
sorties dans une archive compressée "scene.tgz".

Seules les sources LaTeX de la documentation sont fournies; afin de compiler le pdf, on peut utiliser le script "f1" du répertoire spherical_mesh/Doc
(sous réserve que pdflatex et bibtex soient installés).