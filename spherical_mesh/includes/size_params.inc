	integer Nvincell
	integer Nvinface
	integer pagesize
	integer size_of_int4
	integer size_of_int8
	integer size_of_real
	integer size_of_double
	parameter(Nvincell=4)
	parameter(Nvinface=3)
	parameter(pagesize=4096) ! bytes
	parameter(size_of_int4=4) ! bytes
	parameter(size_of_int8=8) ! bytes
	parameter(size_of_real=4) ! bytes
	parameter(size_of_double=8) ! bytes