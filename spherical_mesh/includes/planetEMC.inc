      double precision theta(0:Ntheta_mx)
      double precision phi(0:Nphi_mx)
      double precision z(0:Nlay_mx,1:Ntheta_mx,1:Nphi_mx)
      double precision z_mc(1:Nlay_mx,1:Ntheta_mx,1:Nphi_mx)
      double precision Tground(1:Ntheta_mx,1:Nphi_mx)
      double precision Tgas(0:Nlay_mx+1,1:Ntheta_mx,1:Nphi_mx)
      double precision Tspace(1:Ntheta_mx,1:Nphi_mx)
      double precision ground_emissivity(1:Ntheta_mx,1:Nphi_mx,1:Nb_mx)
      double precision space_emissivity(1:Ntheta_mx,1:Nphi_mx,1:Nb_mx)
      double precision emissivity_zone(1:Nzones_mx,4)
      double precision emissivity(1:Nzones_mx,1:Nb_mx)
      double precision n_ref(1:Nlay_mx,1:Ntheta_mx,1:Nphi_mx,1:Nb_mx)
      double precision ka_gas(1:Nlay_mx,1:Ntheta_mx,1:Nphi_mx,1:Nb_mx,1:Nq_mx)
      double precision ks_gas(1:Nlay_mx,1:Ntheta_mx,1:Nphi_mx,1:Nb_mx)

      common /aa/ theta
      common /ab/ phi
      common /ad/ z
      common /ae/ z_mc
      common /af/ Tground
      common /ag/ Tgas
      common /ah/ Tspace
      common /ai/ ground_emissivity
      common /aj/ space_emissivity
      common /ak/ emissivity_zone
      common /al/ emissivity
      common /am/ n_ref
      common /an/ ka_gas
      common /ao/ ks_gas

      save /aa/,/ab/,/ad/,/ae/,/af/,/ag/,/ah/,/ai/,/aj/,/ak/,/al/,/am/,/an/,/ao/