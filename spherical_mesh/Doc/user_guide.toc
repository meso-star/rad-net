\babel@toc {french}{}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {section}{\numberline {2}Les fichiers de sortie}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Surface}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Mélange de gaz atmosphérique}{5}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Nuages et brumes}{8}{subsection.2.3}%
\contentsline {section}{\numberline {3}Concepts et fonctionnement général}{12}{section.3}%
\contentsline {subsection}{\numberline {3.1}Surface}{12}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Aérosols}{14}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Le coefficient d'extinction totale}{14}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}L'albédo de diffusion simple}{15}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}La fonction de phase de diffusion}{17}{subsubsection.3.2.3}%
\contentsline {subsection}{\numberline {3.3}Mélange de gaz}{17}{subsection.3.3}%
\contentsline {section}{\numberline {4}Données de base}{20}{section.4}%
\contentsline {subsection}{\numberline {4.1}Le fichier {\it data.in}}{20}{subsection.4.1}%
\contentsline {paragraph}{Informations générales}{20}{section*.2}%
\contentsline {paragraph}{Surface}{20}{section*.3}%
\contentsline {paragraph}{Mélange de gaz atmosphérique}{20}{section*.4}%
\contentsline {paragraph}{Aérosols}{21}{section*.5}%
\contentsline {subsection}{\numberline {4.2}Identifiants des méthodes d'interpolation et d'extrapolation des données de base}{22}{subsection.4.2}%
\contentsline {paragraph}{Méthodes d'interpolation selon la verticale, entre deux niveaux verticaux successifs où la donnée est fournie:}{22}{section*.6}%
\contentsline {paragraph}{Méthodes d'interpolation selon la dimension spectrale, entre deux longueurs d'onde successives où la donnée est fournie:}{22}{section*.7}%
\contentsline {paragraph}{Méthodes d'extrapolation selon la verticale, donc pour une position située en dehors de la zone spatiale couverte par les données:}{22}{section*.8}%
\contentsline {paragraph}{Méthodes d'extrapolation selon la dimension spectrale, donc pour une longueur d'onde située en dehors de la zone spectrale couverte par les données:}{22}{section*.9}%
\contentsline {subsection}{\numberline {4.3}Le fichier {\it default\_reflectivity.dat}}{23}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Le fichier {\it surface\_temperature.dat}}{23}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Le fichier {\it VIMS\_cubes\_definition.dat}}{23}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Les fichiers {\it VIMS\_cube*.dat}}{23}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}Les fichiers {\it lambda\_cube*.dat}}{23}{subsection.4.7}%
\contentsline {subsection}{\numberline {4.8}Le fichier {\it haze\_zones.in}}{24}{subsection.4.8}%
\contentsline {subsection}{\numberline {4.9}Le répertoire {\it kext\_haze}}{24}{subsection.4.9}%
\contentsline {subsection}{\numberline {4.10}Le fichier {\it ssaTomasko\_Hirtzig.dat}}{24}{subsection.4.10}%
\contentsline {subsection}{\numberline {4.11}Le fichier {\it fonction\_phase\_haze\_Titan\_80km.txt}}{24}{subsection.4.11}%
\contentsline {subsection}{\numberline {4.12}Les tables de CK pour les espèces gazeuses}{24}{subsection.4.12}%
\contentsline {subsection}{\numberline {4.13}Les profils d'abondance}{25}{subsection.4.13}%
\contentsline {section}{\numberline {5}Compilation et exécution}{26}{section.5}%
\contentsline {section}{\numberline {6}Validité des sorties}{27}{section.6}%
\contentsline {subsection}{\numberline {6.1}read\_surface\_grid\_file}{27}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}read\_surface\_properties\_file}{27}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}read\_volume\_grid\_file}{27}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}read\_gas\_radiative\_properties\_file}{28}{subsection.6.4}%
\contentsline {subsection}{\numberline {6.5}read\_gas\_thermodynamic\_properties\_file}{28}{subsection.6.5}%
\contentsline {subsection}{\numberline {6.6}read\_clouds\_radiative\_properties\_file}{28}{subsection.6.6}%
\contentsline {subsection}{\numberline {6.7}read\_clouds\_phase\_function\_file}{29}{subsection.6.7}%
\contentsline {subsection}{\numberline {6.8}run\_tests.bash}{29}{subsection.6.8}%
\contentsline {subsection}{\numberline {6.9}vtk4cloud}{29}{subsection.6.9}%
\contentsline {subsection}{\numberline {6.10}vtk4gas}{30}{subsection.6.10}%
\contentsline {subsection}{\numberline {6.11}visualisation de la géométrie de surface avec matériaux}{31}{subsection.6.11}%
