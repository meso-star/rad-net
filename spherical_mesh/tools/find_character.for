      subroutine find_character(str,char,Nchar,idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify every occurence of a given character in a character string
c     
c     Input:
c       + str: character string
c       + char: character to identify
c     
c     Output:
c       + Nchar: number of occurences found
c       + idx: index of every occurence
c     
c     I/O
      character*(Nchar_mx) str
      character*1 char
      integer Nchar
      integer idx(1:Nchar_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine find_character'

c     Debug
c      write(*,*) 'In: ',trim(label)
c     Debug

c     Debug
c      write(*,*) 'str="',trim(str),'"'
c      write(*,*) 'length=',len_trim(str)
c     Debug
      Nchar=0
      do i=1,len_trim(str)
         if (str(i:i).eq.char(1:1)) then
            Nchar=Nchar+1
            if (Nchar.gt.Nchar_mx) then
               call error(label)
               write(*,*) 'Nchar_mx has been reached'
               stop
            endif
            idx(Nchar)=i
         endif
      enddo                     ! i

c     Debug
c      write(*,*) 'Out: ',trim(label)
c     Debug
      
      return
      end
      


      subroutine identify_groups(Nchar,idx,Ngroup,group_idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify groups of contiguous characters
c     
c     Input:
c       + Nchar: number of occurences of a given character
c       + idx: index of every occurence
c     
c     Output:
c       + Ngroup: number of contiguous character groups
c       + group_idx: indexes of the first and last character indexes, for each group
c     
c     I/O
      integer Nchar
      integer idx(1:Nchar_mx)
      integer Ngroup
      integer group_idx(1:Ngroup_mx,1:2)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine identify_groups'

      Ngroup=1
      group_idx(Ngroup,1)=idx(1)
      do i=2,Nchar
         if (idx(i).ne.idx(i-1)+1) then
            group_idx(Ngroup,2)=idx(i-1)
            Ngroup=Ngroup+1
            if (Ngroup.gt.Ngroup_mx) then
               call error(label)
               write(*,*) 'Ngroup_mx has been reached'
               stop
            endif
            group_idx(Ngroup,1)=idx(i)
         endif
         if (i.eq.Nchar) then
            group_idx(Ngroup,2)=idx(Nchar)
         endif
      enddo                     ! i

      return
      end
