Ce répertoire contient 9 programmes permettant de réaliser un
diagnostic sur les fichiers de sortie de spherical_mesh ou gcm2hrdr
(fichiers d'entrée de HTRDR-planeto).

Chaque programme est accompagné d'un script permettant sa compilation.
Chaque programme permet de vérifier la bonne lecture du ou des fichiers
concernés, et d'assurer des tests de cohérence sur les données lues.

La liste des programmes, accompagnés de leur(s) argument(s) d'entrée,
est la suivante (le nom du script de compilation est donné entre parenthèses):

+ read_surface_grid_file [surfacic grid file] (compilation: f00)
+ read_surface_properties_file [materials list file] [surface properties file] (compilation: f01)
+ read_volume_grid_file [volumic grid file] (compilation: f02)
+ read_gas_radiative_properties_file [gas radiative properties file] (compilation: f03)
+ read_gas_thermodynamic_properties_file [gas thermodynamic file] (compilation: f04)
+ read_clouds_radiative_properties_file [aerosol radiative properties file] (compilation: f05)
+ read_clouds_phase_function_file [phase function list file] [phase function file] (compilation: f06)

Par ailleurs, un script bash permet d'effectuer automatiquement
toute la batterie de tests sur un répertoire contenant les sorties
de spherical_mesh ou gcm2htrdr; il s'agit du script "run_tests.bash",
qui compile successivement puis exécute chacun des programmes
sus-nommés. Avant d'exécuter ce script, il convient de l'éditer pour
modifier le répertoire (DIR) contenant les fichiers à tester.

Deux programmes de diagnostic supplémentaires sont disponibles, mais non exécutés
par le script "run_tests"; il s'aigt des programmes:

+ vtk4gas (compilation: f08)
+ vtk4cloud (compilation: f09)

Ces deux programmes ont vocation à produire des fichiers VTK dans
le but de visualiser des champs de propriétés optiques (coefficient
d'absorption et de diffusion) pour le mélange de gaz et un mode
d'aérosol. Se référer aux commentaires d'en-tête des fichiers source
correspondants pour accéder à la liste des arguments d'entrée de
ces programmes.
