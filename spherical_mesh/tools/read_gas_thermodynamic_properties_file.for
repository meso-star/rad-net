      program read_gas_thermodynamic_properties_file
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: to read a thermodynamic properties file produced by "spherical_mesh"
c     
c     Variables
      character*(Nchar_mx) input_file
      integer dim,ios,remaining_byte,i,j
      integer*8 page_size,Nnode,Nrecord
      integer*8 inode
      integer*8 total_recorded
      logical*1 l1
      integer vdim,Nv_in_cell
      real T
      logical is_an_integer
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
      integer*8 record_size
      integer*8 alignment
c     label
      character*(Nchar_mx) label
      label='program read_gas_thermodynamic_properties_file'
c     
      dim=3                     ! dimension of space
c
      if (command_argument_count().lt.1) then
         call error(label)
         write(*,*) 'Specify the access path to a gas thermodynamic properties file'
         stop
      else if (command_argument_count().gt.1) then
         call error(label)
         write(*,*) 'Too many arguments'
         stop
      endif
      call get_command_argument(1,input_file)
c     
      open(11,file=trim(input_file),status='old',
     &     form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(input_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(input_file)
      endif
c     read page size
      read(11) page_size
      if (page_size.lt.0) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(page_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read number of nodes
      read(11) Nnode
      if (Nnode.lt.0) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'should be positive'
         stop
      endif
c     Read record size
      read(11) record_size
      if (record_size.lt.0) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(record_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read alignment
      read(11) alignment
      if (alignment.lt.0) then
         call error(label)
         write(*,*) 'alignment=',alignment
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(alignment/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'alignment=',alignment
         write(*,*) 'is not a multiple of 2'
         stop
      endif
      if (record_size.ne.int(size_of_real,kind(record_size))) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'should be equal to:',int(size_of_real,kind(record_size))
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=4*size_of_int8
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
      do inode=1,Nnode
         read(11) T
         if (T.lt.0.0D+0) then
            call error(label)
            write(*,*) 'T=',T
            write(*,*) 'shoud be positive'
            stop
         endif
         if (isnan(dble(T))) then
            call error(label)
            write(*,*) 'T=',T
            stop
         endif
      enddo                     ! inode
c     ------------------------------------------------------------------
c     Padding
      total_recorded=Nnode*record_size
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
      close(11)

c     Debug
      write(*,*) 'page_size=',page_size
      write(*,*) 'record_size=',record_size
      write(*,*) 'alignment=',alignment
      write(*,*) 'Nnode=',Nnode
c     Debug

      end
