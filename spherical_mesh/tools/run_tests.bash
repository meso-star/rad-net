#!/bin/bash
#
# The DIR variable must contain the path to the folder containing
# all files to analyze (without a final / )
#
#DIR='../results'
#DIR='../../GCM2htrdr/results'
DIR='../../TOM/data/dummy/results'

# read_surface_grid_file
./f00
if [ $? -ne 0 ]; then
    echo "Compilation script f00 (read_surface_grid_file) failed"
    exit 1
else
    echo '+-------------------------------------------------+'
    echo '|  Running read_surface_grid_file                 |'
    echo '+-------------------------------------------------+'
    ./read_surface_grid_file.exe $DIR/sphere.bin
fi
echo ''
# read_surface_properties_file
./f01
if [ $? -ne 0 ]; then
    echo "Compilation script f01 (read_surface_properties_file) failed"
    exit 1
else
    echo '+-------------------------------------------------+'
    echo '|  Running read_surface_properties_file           |'
    echo '+-------------------------------------------------+'
    ./read_surface_properties_file.exe $DIR/materials_list.txt $DIR/surface_properties.bin
fi
echo ''
# read_volume_grid_file
./f02
if [ $? -ne 0 ]; then
    echo "Compilation script f02 (read_volume_grid_file) failed"
    exit 1
else
    echo '+-------------------------------------------------+'
    echo '|  Running read_volume_grid_file                  |'
    echo '+-------------------------------------------------+'
    ./read_volume_grid_file.exe $DIR/gas_grid.bin
#    if [ -f $DIR/cloud_grid.bin ]; then
#	./read_volume_grid_file.exe $DIR/cloud_grid.bin
#    fi
#    if [ -f $DIR/haze_grid.bin ]; then
#	./read_volume_grid_file.exe $DIR/haze_grid.bin
#    fi
    ./read_volume_grid_file.exe $DIR/aerosol001_grid.bin
    ./read_volume_grid_file.exe $DIR/aerosol002_grid.bin
    ./read_volume_grid_file.exe $DIR/aerosol003_grid.bin
fi
echo ''
# read_gas_radiative_properties_file
./f03
if [ $? -ne 0 ]; then
    echo "Compilation script f03 (read_gas_radiative_properties_file) failed"
    exit 1
else
    echo '+-------------------------------------------------+'
    echo '|  Running read_gas_radiative_properties_file     |'
    echo '+-------------------------------------------------+'
    ./read_gas_radiative_properties_file.exe $DIR/gas_radiative_properties.bin
fi
echo ''
# read_gas_thermodynamic_properties_file
./f04
if [ $? -ne 0 ]; then
    echo "Compilation script f04 (read_gas_thermodynamic_properties_file) failed"
    exit 1
else
    echo '+-------------------------------------------------+'
    echo '|  Running read_gas_thermodynamic_properties_file |'
    echo '+-------------------------------------------------+'
    ./read_gas_thermodynamic_properties_file.exe $DIR/gas_thermodynamic_properties.bin
fi
echo ''
# read_clouds_radiative_properties_file
./f05
if [ $? -ne 0 ]; then
    echo "Compilation script f05 (read_clouds_radiative_properties_file) failed"
    exit 1
else
    echo '+-------------------------------------------------+'
    echo '|  Running read_clouds_radiative_properties_file  |'
    echo '+-------------------------------------------------+'
#    if [ -f $DIR/cloud_radiative_properties.bin ]; then
#	./read_clouds_radiative_properties_file.exe $DIR/cloud_radiative_properties.bin
#    fi
#    if [ -f $DIR/haze_radiative_properties.bin ]; then
#	./read_clouds_radiative_properties_file.exe $DIR/haze_radiative_properties.bin
#    fi
    ./read_clouds_radiative_properties_file.exe $DIR/aerosol001_radiative_properties.bin
    ./read_clouds_radiative_properties_file.exe $DIR/aerosol002_radiative_properties.bin
    ./read_clouds_radiative_properties_file.exe $DIR/aerosol003_radiative_properties.bin
fi
echo ''
# read_clouds_phase_function_file
./f06
if [ $? -ne 0 ]; then
    echo "Compilation script f06 (read_clouds_phase_function_file) failed"
    exit 1
else
    echo '+-------------------------------------------------+'
    echo '|  Running read_clouds_phase_function_file        |'
    echo '+-------------------------------------------------+'
#    if [ -f $DIR/cloud_phase.bin ]; then
#	./read_clouds_phase_function_file.exe $DIR/phase_function_list.txt $DIR/cloud_phase.bin
#    fi
#    if [ -f $DIR/haze_phase.bin ]; then
#	./read_clouds_phase_function_file.exe $DIR/phase_function_list.txt $DIR/haze_phase.bin
#    fi
    ./read_clouds_phase_function_file.exe $DIR/aerosol001_phase_function_list.txt $DIR/aerosol001_phase.bin
    ./read_clouds_phase_function_file.exe $DIR/aerosol002_phase_function_list.txt $DIR/aerosol002_phase.bin
    ./read_clouds_phase_function_file.exe $DIR/aerosol003_phase_function_list.txt $DIR/aerosol003_phase.bin
fi

exit 0
