      program read_clouds_phase_function_file
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'formats.inc'
      include 'param.inc'
c     
c     Purpose: to read a aerosol phase function file produced by "spherical_mesh"
c     
c     Input arguments:
c     
c     1- The access path to a phase function list file
c     
c     2- The acess path to a phase function file
c     
c     Variables
      character*(Nchar_mx) list_file,phase_function_file
      integer dim,ios,remaining_byte,i,j,ilambda,iangle,Nlambda,Nangle
      integer*8 page_size,Nnode,Nrecord
      integer*8 record_size
      integer*8 alignment
      integer*8 inode,iband
      integer*8 total_recorded
      logical*1 l1
      double precision cartesian_coordinates(1:Ndim_mx)
      integer*8 index(1:Nvincell)
      integer vdim,Nv_in_cell
      integer phase_function_index
      integer Npf,ipf
      character*(Nchar_mx) filename,path
      logical file_exists
      integer Nchar
      integer idx(1:Nchar_mx)
      character*(Nchar_mx) type,line,Nangle_str,g_str
      character*11 str11
      integer Ngroup,igroup
      integer group_idx(1:Ngroup_mx,1:2)
      integer errcode
      double precision angle(1:Nangle_mx),phi(1:Nangle_mx),alpha,g_HG
      integer Ninterval
      logical is_an_integer
      integer pf_idx_min,pf_idx_max
c     label
      character*(Nchar_mx) label
      label='program read_clouds_phase_function_file'
c     
      dim=3                     ! dimension of space
c     
      if (command_argument_count().le.0) then
         call error(label)
         write(*,*) 'Specify the access path to a phase function list file and a phase function file'
         stop
      else if (command_argument_count().eq.1) then
         call error(label)
         write(*,*) 'Second argument missing: access path to a phase function file'
         stop
      else if (command_argument_count().gt.2) then
         call error(label)
         write(*,*) 'Too many arguments'
         stop
      endif
      call get_command_argument(1,list_file)
      call get_command_argument(2,phase_function_file)
c     
      call find_character(list_file,'/',Nchar,idx)
      if (Nchar.le.0) then
         call error(label)
         write(*,*) 'Could not identify / character in path'
         stop
      endif
      path=list_file(1:idx(Nchar))
      open(11,file=trim(list_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(list_file)
         stop
      else
         write(*,*) 'Reading file: ',trim(list_file)
      endif
      read(11,*) Npf
      if (Npf.le.0) then
         call error(label)
         write(*,*) 'Npf=',Npf
         write(*,*) 'should be > 0'
         stop
      endif
      do ipf=1,Npf
         read(11,10) filename
         if (filename(1:12).eq.'${DATA_PATH}') then
            filename=filename(14:len_trim(filename))
         endif
         filename=trim(path)//trim(filename)
         inquire(file=trim(filename),exist=file_exists)
         if (.not.file_exists) then
            call error(label)
            write(*,*) 'Phase function definition file index:',ipf
            write(*,*) 'not found: ',trim(filename)
            stop
         endif
         open(12,file=trim(filename))
         read(12,*) str11,Nlambda
c     Debug
c         write(*,*) 'str11=',trim(str11)
c         write(*,*) 'filename=',trim(filename),' Nlambda=',Nlambda
c         stop
c     Debug
         if (Nlambda.le.0) then
            call error(label)
            write(*,*) 'File:',trim(filename)
            write(*,*) 'Nlambda=',Nlambda
            stop
         endif
         do ilambda=1,Nlambda
            read(12,10) line     
            call find_character(line,' ',Nchar,idx)
            call identify_groups(Nchar,idx,Ngroup,group_idx)
            if (Ngroup.ne.3) then
               call error(label)
               write(*,*) 'line="',trim(line),'"'
               write(*,*) 'should contain 3 groups of spaces'
               write(*,*) 'found: Ngroup=',Ngroup
               stop
            endif
            type=line(group_idx(2,2)+1:group_idx(3,1)-1)
            if (type.eq.'HG') then
               g_str=line(group_idx(3,2)+1:len_trim(line))
               call str2dble(g_str,g_HG,errcode)
               if (errcode.ne.0) then
                  call error(label)
                  write(*,*) 'while converting to double: g_str="',trim(g_str),'"'
                  stop
               endif
               if ((g_HG.lt.-1.0D+0).or.(g_HG.gt.1.0D+0)) then
                  call error(label)
                  write(*,*) 'g_HG=',g_HG
                  write(*,*) 'should be in the [-1,1] range'
                  stop
               endif
               if (isnan(g_HG)) then
                  call error(label)
                  write(*,*) 'g_HG=',g_HG
                  write(*,*) 'filename=',trim(filename)
                  write(*,*) 'ilambda=',ilambda
                  stop
               endif
            else if (type.eq.'discrete') then
               Nangle_str=line(group_idx(3,2)+1:len_trim(line))
               call str2int(Nangle_str,Nangle,errcode)
               if (errcode.ne.0) then
                  call error(label)
                  write(*,*) 'while converting to integer: "',trim(Nangle_str),'"'
                  stop
               endif
               if (Nangle.gt.Nangle_mx) then
                  call error(label)
                  write(*,*) 'Nangle=',Nangle
                  write(*,*) '> Nangle_mx=',Nangle_mx
                  stop
               endif
               do iangle=1,Nangle
                  read(12,*) angle(iangle),phi(iangle)
               enddo            ! iangle
               if (angle(1).ne.0.0D+0) then
                  call error(label)
                  write(*,*) 'angle(1)=',angle(1)
                  write(*,*) 'should be equal to 0'
                  stop
               endif
               if (angle(Nangle).ne.pi) then
                  call error(label)
                  write(*,*) 'angle(',Nangle,')=',angle(Nangle)
                  write(*,*) 'should be equal to ',pi
                  stop
               endif
               Ninterval=Nangle-1
               alpha=0.0D+0
               do i=1,Ninterval
                  alpha=alpha+(phi(i)+phi(i+1))*(dcos(angle(i))-dcos(angle(i+1)))
               enddo            ! i
               alpha=alpha*pi
               if (dabs(alpha-1.0D+0).gt.1.0D-3) then
                  call warning(label)
                  write(*,*) 'Phase function not normalized'
               endif
            else
               call error(label)
               write(*,*) 'type="',trim(type),'"'
               write(*,*) 'only "HG" and "discrete" are currently allowed'
               stop
            endif
         enddo                  ! ilambda
         close(12)
      enddo                     ! ipf
      close(11)
c     
      open(11,file=trim(phase_function_file),status='old',
     &     form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(phase_function_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(phase_function_file)
      endif
c     read page size
      read(11) page_size
      if (page_size.lt.0) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(page_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read number of nodes
      read(11) Nnode
      if (Nnode.lt.0) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'should be positive'
         stop
      endif
c     Read record size
      read(11) record_size
      if (record_size.lt.0) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(record_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read alignment
      read(11) alignment
      if (alignment.lt.0) then
         call error(label)
         write(*,*) 'alignment=',alignment
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(alignment/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'alignment=',alignment
         write(*,*) 'is not a multiple of 2'
         stop
      endif
      if (record_size.ne.int(size_of_real,kind(record_size))) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'should be equal to:',int(size_of_real,kind(record_size))
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=4*size_of_int8
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
      pf_idx_min=Nmaterial_mx
      pf_idx_max=0
      do inode=1,Nnode
         read(11) phase_function_index
         if (isnan(dble(phase_function_index))) then
            call error(label)
            write(*,*) 'Node index:',inode
            write(*,*) 'phase_function_index=',phase_function_index
            stop
         endif
         if ((phase_function_index.lt.0).or.(phase_function_index.gt.Npf)) then
            call error(label)
            write(*,*) 'Node index:',inode
            write(*,*) 'phase_function_index=',phase_function_index
            if (phase_function_index.le.0) then
               write(*,*) 'should be >= 0'
            else
               write(*,*) 'should be < Npf=',Npf
            endif
            stop
         endif
         if (phase_function_index.lt.pf_idx_min) then
            pf_idx_min=phase_function_index
         endif
         if (phase_function_index.gt.pf_idx_max) then
            pf_idx_max=phase_function_index
         endif
      enddo                     ! inode
      if (pf_idx_min.ne.0) then
         call error(label)
         write(*,*) 'minimum phase function index=',pf_idx_min
         write(*,*) 'should be 0'
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=Nnode*size_of_int4
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
      close(11)

c     Debug
      write(*,*) 'page_size=',page_size
      write(*,*) 'Nnode=',Nnode
c     Debug

      end
