      subroutine spher2cart(parent,verif,r,theta,phi,x,y,z)
      implicit none
      include 'max.inc'
c     
c     Purpose: to convert (r,theta,phi) spherical coordinates
c     into (x,y,z) cartesian coordinates
c
c     Inputs:
c       + parent: name of the routine that is calling "spher2cart"
c       + verif: 1 if angles checking required; 0 otherwise
c       + r: radius (m), or altitude relative to the center of the planet
c       + theta: latitude (rad) [-pi/2,pi/2]
c       + phi: longitude rad) [0:2*pi]
c
c     Outputs:
c       + x (m)
c       + y (m)
c       + z (m)
c
c     I/O
      character*(Nchar_mx) parent
      integer verif
      double precision r,theta,phi
      double precision x,y,z
c     label
      character*(Nchar_mx) label
      label='subroutine spher2cart from: '//trim(parent)

      x=r*dcos(theta)*dcos(phi)
      y=r*dcos(theta)*dsin(phi)
      z=r*dsin(theta)
      
      return
      end



      subroutine cart2spher(x,y,z,r,theta,phi)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to convert (x,y,z) cartesian coordinates
c     into (r,theta,phi) spherical coordinates
c
c     Inputs:
c       + x (m)
c       + y (m)
c       + z (m)
c
c     Outputs:
c       + r: radius (m), or altitude relative to the center of the planet
c       + theta: latitude (rad) [-pi/2,pi/2]
c       + phi: longitude rad) [0:2*pi]
c
c     I/O
      double precision x,y,z
      double precision r,theta,phi
c     temp
      double precision cosp,sinp,cost,sint
      double precision a,t1,t2
c     label
      character*(Nchar_mx) label
      label='subroutine cart2spher'

      a=dsqrt(x**2.0D+0+y**2.0D+0)
      r=dsqrt(x**2.0D+0+y**2.0D+0+z**2.0D+0)
      if (a.eq.0.0D+0) then
         cosp=1.0D+0
         sinp=0.0D+0
      else
         cosp=x/a
         sinp=y/a
      endif
      cost=a/r
      sint=z/r
c     Debug
c      write(*,*) 'cost=',cost
c      write(*,*) 'sint=',sint
c     Debug
c     Debug
c      write(*,*) 'cosp=',cosp
c      write(*,*) 'sinp=',sinp
c     Debug
      
      t1=dacos(dabs(cosp))
c     Debug
c      write(*,*) 't1=',t1,t1*180.0D+0/pi
c     Debug
      if (x.ge.0.0D+0) then
         if (sinp.ge.0.0D+0) then
            phi=t1
         else
            phi=2.0D+0*pi-t1
         endif
      else
         if (sinp.ge.0.0D+0) then
c     Debug
c            write(*,*) 'case x<0, sinp>0'
c     Debug
            phi=pi-t1
c     Debug
c            write(*,*) 'phi=',phi,phi*180.0D+0/pi
c     Debug
         else
            phi=pi+t1
         endif
      endif

      t2=dacos(cost)
      if (sint.ge.0.0D+0) then
         theta=t2
      else
         theta=-t2
      endif
c     Debug
c      write(*,*) 'phi=',phi,phi*180.0D+0/pi
c     Debug
      
      return
      end
