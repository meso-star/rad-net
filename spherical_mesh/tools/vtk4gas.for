      program vtk4gas
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: to dump ka and ks for the gas mixture to vtk files
c     
c     This program takes 6 arguments on the command line:
c     1- file that contains the tetrahedric grid of the gas
c     2- file that contains the radiative properties of the gas
c     3- index of the band radiative properties have to be considered for;
c     The following syntaxes are supported:
c       + bandXX with XX the actual numeric value of the band index
c       + XX, the actual numeric value of the band index
c     
c     4- index of the quadrature point radiative properties have to be considered for;
c     The following syntaxes are supported:
c       + quadXX with XX the actual numeric value of the quadrature point
c       + XX, the actual numeric value of the quadrature point
c     
c     5- name of the VTK file for dumping the absorption coefficient
c     6- name of the VTK file for dumping the scattering coefficient
c     
c     Variables
      character*(Nchar_mx) grid_file
      character*(Nchar_mx) radiative_properties_file
      character*(Nchar_mx) band_str
      character*(Nchar_mx) quad_str
      character*(Nchar_mx) ka_vtk_file
      character*(Nchar_mx) ks_vtk_file
      integer band_index,quad_index
      integer dim,ios,remaining_byte,i,j
      integer record_size
      integer*8 page_size,Nnode,Nnode_tmp,Ncell,Nrecord,Nband,Nq(1:Nb_mx),sum_Nq
      integer*8 inode,icell
      integer*8 total_recorded
      logical*1 l1
      double precision cartesian_coordinates(1:Nnode_mx,1:Ndim_mx)
      integer*8 index(1:Ncell_mx,1:Nvincell)
      integer*8 index1(1:Ncell_mx,1:Nvincell)
      integer vdim,Nv_in_cell
      double precision volume
      integer negp,iband,iq,errcode
      character*(Nchar_mx) command
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision w(1:Nq_mx),sum_w
      real ka(1:Nnode_mx),ks(1:Nnode_mx)
      logical is_an_integer
      logical err_found
      character*(Nchar_mx) descriptor
      integer*8 start_index
      character*(Nchar_mx) field_name
      double precision ka_field(1:Ncell_mx)
      double precision ks_field(1:Ncell_mx)
      logical file_exists
      logical check_homogeneous_cells
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
c     label
      character*(Nchar_mx) label
      label='program vtk4gas'

      dim=3                     ! dimension of space

      if (command_argument_count().lt.6) then
         call error(label)
         write(*,*) 'Too few input arguments'
         stop
      else if (command_argument_count().eq.6) then
         call get_command_argument(1,grid_file)
         call get_command_argument(2,radiative_properties_file)
         call get_command_argument(3,band_str)
         call get_command_argument(4,quad_str)
         call get_command_argument(5,ka_vtk_file)
         call get_command_argument(6,ks_vtk_file)
         inquire(file=trim(ka_vtk_file),exist=file_exists)
         if (file_exists) then
            command='rm -f '//trim(ka_vtk_file)
            call exec(command)
         endif
         inquire(file=trim(ks_vtk_file),exist=file_exists)
         if (file_exists) then
            command='rm -f '//trim(ks_vtk_file)
            call exec(command)
         endif
      else
         call error(label)
         write(*,*) 'Too many input arguments'
         stop
      endif
c     ======================================================================================
c     Tetrahedric grid
c     ======================================================================================      
      open(11,file=trim(grid_file),status='old',
     &     form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(grid_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(grid_file)
      endif
c     read page size
      read(11) page_size
c     Read number of nodes
      read(11) Nnode
      if (Nnode.gt.Nnode_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) '> Nnode_mx=',Nnode_mx
         stop
      endif
c     Read number of cells
      read(11) Ncell
      if (Ncell.gt.Ncell_mx) then
         call error(label)
         write(*,*) 'Ncell=',Ncell
         write(*,*) '> Ncell_mx=',Ncell_mx
         stop
      endif
c     Read spatial dimension
      read(11) vdim
      if (vdim.ne.dim) then
         call error(label)
         write(*,*) 'Dimension of space read:',vdim
         write(*,*) 'should be:',dim
         stop
      endif
c     Read the number of vertex per cell
      read(11) Nv_in_cell
      if (Nv_in_cell.ne.Nvincell) then
         call error(label)
         write(*,*) 'Number of vertex per cell read:',Nv_in_cell
         write(*,*) 'should be equal to:',Nvincell
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=3*size_of_int8+2*size_of_int4
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
c     Read node coordinates, sorted by increasing absolute node index
      do inode=1,Nnode
         read(11) (cartesian_coordinates(inode,j),j=1,vdim)
         do j=1,vdim
            if (isnan(cartesian_coordinates(inode,j))) then
               call error(label)
               write(*,*) 'node index:',inode
               write(*,*) 'cartesian_coordinates(',inode,',',j,')=',cartesian_coordinates(inode,j)
               stop
            endif
         enddo                  ! j
      enddo                     ! inode
c     ------------------------------------------------------------------
c     Padding
      record_size=vdim*size_of_double
      Nrecord=Nnode
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
c     Read cell vertices (absolute indexes of the 4 nodes that define each tetrahedron)
      do icell=1,Ncell
         read(11) (index(icell,j),j=1,Nv_in_cell) ! start @ 0
         do j=1,Nv_in_cell
            if ((index(icell,j).lt.0).or.(index(icell,j).ge.Nnode)) then
               call error(label)
               write(*,*) 'cell index:',icell
               write(*,*) 'index(',icell,',',j,')=',index(icell,j)
               if (index(icell,j).lt.0) then
                  write(*,*) 'should be >= 0'
               else
                  write(*,*) 'should be < Nnode=',Nnode
               endif
               stop
            endif
            index1(icell,j)=index(icell,j)+1 ! start @ 1
         enddo                  ! j
      enddo                     ! icell
c     ------------------------------------------------------------------
c     Padding
      record_size=Nvincell*size_of_int8
      Nrecord=Ncell
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------     
      close(11)

      call volumic_grid_volume(vdim,Nnode,Ncell,Nv_in_cell,cartesian_coordinates,index1,volume,negp)
c     Debug
      write(*,*) 'page_size=',page_size
      write(*,*) 'Nnode=',Nnode
      write(*,*) 'Ncell=',Ncell
      write(*,*) 'V=',volume,'m³'
      if (negp.gt.0) then
         write(*,*) 'Negative volume cells:',negp,' %'
      endif
c     Debug

c     ======================================================================================
c     Radiative properties
c     ======================================================================================

      if (Nnode.eq.Ncell*Nvincell) then
         check_homogeneous_cells=.true.
         write(*,*) 'Cells will be tested for homogeneity'
      else
         check_homogeneous_cells=.false.
      endif
      
      if (band_str(1:4).eq.'band') then
         band_str=band_str(5:len_trim(band_str))
      endif
      call str2int(band_str,band_index,errcode)
      if (errcode.ne.0) then
         call error(label)
         write(*,*) 'Could not convert to integer:'
         write(*,*) 'band_str="',trim(band_str),'"'
         stop
      endif
      if (band_index.le.0) then
         call error(label)
         write(*,*) 'Band index=',band_index
         write(*,*) 'should be positive'
         stop
      endif
      
      if (quad_str(1:4).eq.'quad') then
         quad_str=quad_str(5:len_trim(quad_str))
      endif
      call str2int(quad_str,quad_index,errcode)
      if (errcode.ne.0) then
         call error(label)
         write(*,*) 'Could not convert to integer:'
         write(*,*) 'quad_str="',trim(quad_str),'"'
         stop
      endif
      if (quad_index.le.0) then
         call error(label)
         write(*,*) 'Quad index=',quad_index
         write(*,*) 'should be positive'
         stop
      endif
      
      open(12,file=trim(radiative_properties_file),status='old',
     &     form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(radiative_properties_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(radiative_properties_file)
      endif
c     read page size
      read(12) page_size
      if (page_size.lt.0) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(page_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read number of bands
      read(12) Nband
      if (Nband.lt.0) then
         call error(label)
         write(*,*) 'Nband=',Nband
         write(*,*) 'should be positive'
         stop
      endif
      if (band_index.gt.Nband) then
         call error(label)
         write(*,*) 'Band index=',band_index
         write(*,*) '> Nband=',Nband
         stop
      endif
c     Read number of nodes
      read(12) Nnode_tmp
      if (Nnode_tmp.lt.0) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'should be positive'
         stop
      endif
      if (Nnode_tmp.ne.Nnode) then
         call error(label)
         write(*,*) 'Major inconsistency:'
         write(*,*) 'Number of nodes from grid file:',Nnode
         write(*,*) 'Number of nodes from radiative prop. file:',Nnode_tmp
         write(*,*) 'Check input file names & argument order'
         stop
      endif
c     Read spectral metadata
      sum_Nq=0
      do iband=1,Nband
         read(12) lambda_min(iband),lambda_max(iband),Nq(iband)
         sum_Nq=sum_Nq+Nq(iband)
         if (Nq(iband).lt.0) then
            call error(label)
            write(*,*) 'band=',iband
            write(*,*) 'Nq=',Nq(iband)
            write(*,*) 'should be positive'
            stop
         endif
         if ((band_index.eq.iband).and.(quad_index.gt.Nq(iband))) then
            call error(label)
            write(*,*) 'Required band:',band_index
            write(*,*) 'Quad index=',quad_index
            write(*,*) '> Nq(',iband,')=',Nq(iband)
            stop
         endif
         do iq=1,Nq(iband)
            read(12) w(iq)
            if (w(iq).le.0.0D+0) then
               call error(label)
               write(*,*) 'band:',iband
               write(*,*) 'w(',iq,')=',w(iq)
               write(*,*) 'should be > 0'
               stop
            endif
            if (w(iq).gt.1.0D+0) then
               call error(label)
               write(*,*) 'band:',iband
               write(*,*) 'w(',iq,')=',w(iq)
               write(*,*) 'should be <= 1'
               stop
            endif
         enddo                  ! iq
         if (lambda_min(iband).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda_min(',iband,')=',lambda_min(iband)
            write(*,*) 'should be > 0'
            stop
         endif
         if (lambda_max(iband).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda_max(',iband,')=',lambda_max(iband)
            write(*,*) 'should be > 0'
            stop
         endif
         if (lambda_min(iband).gt.lambda_max(iband)) then
            call error(label)
            write(*,*) 'lambda_min(',iband,')=',lambda_min(iband)
            write(*,*) '> lambda_max(',iband,')=',lambda_max(iband)
            stop
         endif
c     
         sum_w=0.0D+0
         do iq=1,Nq(iband)
            sum_w=sum_w+w(iq)
         enddo                  ! iq
         if (dabs(sum_w-1.0D+0).gt.1.0D-4) then
            call error(label)
            write(*,*) 'Band index:',iband
            write(*,*) 'quadrature weights:'
            do iq=1,Nq(iband)
               write(*,*) 'w(',iq,')=',w(iq)
            enddo               ! iq
            write(*,*) 'sum of weights=',sum_w
            write(*,*) 'should be close to 1'
            stop
         endif
      enddo                     ! iband
      do iband=1,Nband
         if (iband.lt.Nband) then
            if (lambda_max(iband).gt.lambda_min(iband+1)) then
               call error(label)
               write(*,*) 'lambda_max(',iband,')=',lambda_max(iband)
               write(*,*) '> lambda_min(',iband+1,')=',lambda_min(iband+1)
               stop
            endif
         endif
      enddo                     ! iband
c     ------------------------------------------------------------------
c     Padding
      total_recorded=(Nband+3)*size_of_int8+(sum_Nq+2*Nband)*size_of_double
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(12) (l1,i=1,remaining_byte)
c     Padding

c     progress display
      ntot=Nnode*int(sum_Nq,kind(ntot))
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display
      do iband=1,Nband
         do inode=1,Nnode
            read(12) ks(inode)
            if (ks(inode).lt.0.0D+0) then
               call error(label)
               write(*,*) 'iband=',iband,' inode=',inode
               write(*,*) 'ks=',ks(inode)
               write(*,*) 'shoud be positive'
               stop
            endif
            if (isnan(dble(ks(inode)))) then
               call error(label)
               write(*,*) 'iband=',iband,' inode=',inode
               write(*,*) 'ks=',ks(inode)
               stop
            endif
         enddo                  ! inode
c     Padding
         total_recorded=Nnode*size_of_real
         call compute_padding2(page_size,total_recorded,remaining_byte)
         read(12) (l1,i=1,remaining_byte)
c     Padding
         do iq=1,Nq(iband)
            do inode=1,Nnode
               read(12) ka(inode)
               if (ka(inode).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'iband=',iband,' inode=',inode
                  write(*,*) 'ka=',ka(inode)
                  write(*,*) 'shoud be positive'
                  stop
               endif
               if (isnan(dble(ka(inode)))) then
                  call error(label)
                  write(*,*) 'iband=',iband,' inode=',inode
                  write(*,*) 'ka=',ka(inode)
                  stop
               endif
c     progress display
               ndone=ndone+1
               fdone=dble(ndone)/dble(ntot)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no') floor(fdone),' %'
                  pifdone=ifdone
               endif
c     progress display
            enddo               ! inode
c            
            if ((iband.eq.band_index).and.(iq.eq.quad_index)) then
c     The radiative properties fields are provided over nodes, we have to average
c     then over each cell
               do icell=1,Ncell
                  ka_field(icell)=0.0D+0
                  ks_field(icell)=0.0D+0
                  do j=1,Nvincell
                     ka_field(icell)=ka_field(icell)+ka(index1(icell,j))
                     ks_field(icell)=ks_field(icell)+ks(index1(icell,j))
                  enddo         ! j
                  ka_field(icell)=ka_field(icell)/dble(Nvincell)
                  ks_field(icell)=ks_field(icell)/dble(Nvincell)
               enddo            ! icell
c     Now ka and ks fields can be dumped to their respective VTK files
               start_index=0
               descriptor='Gas absorption coefficient (inv. m)'
               field_name='ka_gas'
               call dump_tetrahedral_grid_to_vtk(dim,descriptor,Nnode,Ncell,cartesian_coordinates,index,start_index,field_name,ka_field,ka_vtk_file)
               descriptor='Gas scattering coefficient (inv. m)'
               field_name='ks_gas'
               call dump_tetrahedral_grid_to_vtk(dim,descriptor,Nnode,Ncell,cartesian_coordinates,index,start_index,field_name,ks_field,ks_vtk_file)
c     Check cell homogeneity if required
               if (check_homogeneous_cells) then
                  do icell=1,Ncell
                     do j=1,Nvincell
                        if (ka(index1(icell,j)).ne.ka(index1(icell,1))) then
                           call error(label)
                           write(*,*) 'Inhomogeneous cell:',icell
                           do i=1,Nvincell
                              write(*,*) 'ka(',index1(icell,i),')=',ka(index1(icell,i))
                           enddo ! i
                           stop
                        endif
                     enddo      ! j
                  enddo         ! icell
               endif            ! check_homogeneous_cells
c     
            endif               ! iband=band_index
c
c     Padding
            total_recorded=Nnode*size_of_real
            call compute_padding2(page_size,total_recorded,remaining_byte)
            read(12) (l1,i=1,remaining_byte)
c     Padding
         enddo                  ! iq
      enddo                     ! iband
      close(12)

c     Debug
      write(*,*)
      write(*,*) 'page_size=',page_size
      write(*,*) 'Nnode=',Nnode
      write(*,*) 'Nband=',Nband
      inquire(file=trim(ka_vtk_file),exist=file_exists)
      if (file_exists) then
         write(*,*) 'VTK file has been recorded: ',trim(ka_vtk_file)
      endif
      inquire(file=trim(ks_vtk_file),exist=file_exists)
      if (file_exists) then
         write(*,*) 'VTK file has been recorded: ',trim(ks_vtk_file)
      endif
c     Debug
      
      end
