      subroutine dump_tetrahedral_grid_to_vtk(dim,descriptor,Nnode,Ncell,cartesian_coordinates,index,start_index,field_name,field,vtk_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: to generate a VTK file for a tetrahedral grid and associated data
c     
c     Input:
c       + dim: dimension of space
c       + descriptor: data field descriptor
c       + Nnode: number of nodes
c       + Ncell: number of tetrahedric cells
c       + cartesian_coordinates: array of x/y/z coordinates for the nodes
c       + index: array of node indexes for the cells
c       + start_index: value of the smallest index (typically: indexes start at 0 or 1)
c       + field_name: field descriptor
c       + field: data associated to cells
c       + vtk_file: VTK file to record
c     
c     Output: the required file
c     
c     I/O
      integer dim
      character*(Nchar_mx) descriptor
      integer*8 Nnode
      integer*8 Ncell
      double precision cartesian_coordinates(1:Nnode_mx,1:Ndim_mx)
      integer*8 index(1:Ncell_mx,1:Nvincell)
      integer*8 start_index
      character*(Nchar_mx) field_name
      double precision field(1:Ncell_mx)
      character*(Nchar_mx) vtk_file
c     temp
      integer j
      integer*8 inode,icell,Nelem
      character*(Nchar_mx) line
      character*(Nchar_mx) Nnode_str
      character*(Nchar_mx) Ncell_str
      character*(Nchar_mx) Nelem_str
      logical err_code
c     label
      character*(Nchar_mx) label
      label='subroutine dump_tetrahedral_grid_to_vtk'
      
      open(11,file=trim(vtk_file))
      write(11,10) '# vtk DataFile Version 2.0'
      write(11,10) trim(descriptor)
      write(11,10) 'ASCII'
      write(11,10) 'DATASET UNSTRUCTURED_GRID'
      call bignum2str(Nnode,Nnode_str,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Could not convert to character string:'
         write(*,*) 'Nnode=',Nnode
         stop
      endif
      line='POINTS '//trim(Nnode_str)//' float'
      write(11,10) trim(line)
      do inode=1,Nnode
         write(11,*) (cartesian_coordinates(inode,j),j=1,dim)
      enddo                     ! inode
      write(11,*)
      call bignum2str(Ncell,Ncell_str,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Could not convert to character string:'
         write(*,*) 'Ncell=',Ncell
         stop
      endif
      Nelem=5*Ncell
      call bignum2str(Nelem,Nelem_str,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Could not convert to character string:'
         write(*,*) 'Nelem=',Nelem
         stop
      endif
      line='CELLS '//trim(Ncell_str)//' '//trim(Nelem_str)
      write(11,10) trim(line)
      do icell=1,Ncell
         write(11,*) Nvincell,(index(icell,j)-start_index,j=1,Nvincell)
      enddo                     ! icell
      write(11,*)
      line='CELL_TYPES '//trim(Ncell_str)
      write(11,10) trim(line)
      do icell=1,Ncell
         write(11,*) 10         ! code for a tetrahedron
      enddo                     ! icell
      write(11,*)
      line='CELL_DATA '//trim(Ncell_str)
      write(11,10) trim(line)
      write(11,10) 'SCALARS '//trim(field_name)//' float 1'
      write(11,10) 'LOOKUP_TABLE default'
      do icell=1,Ncell
         write(11,*) field(icell)
      enddo                     ! icell
      close(11)
c      write(*,*) 'File was recorded: ',trim(vtk_file)

      return
      end
