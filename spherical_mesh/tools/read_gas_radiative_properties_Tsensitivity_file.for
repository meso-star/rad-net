      program read_gas_radiative_properties_Tsensitivity_file
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: to read a gas 'radiative properties sensitivity to temperature' file produced by "planet_generator"
c     
c     Variables
      character*(Nchar_mx) input_file
      integer dim,ios,remaining_byte,i,j
      integer record_size
      integer*8 page_size,Nnode,Nband,Nq(1:Nb_mx),Nrecord
      integer*8 inode,iband,iq
      integer*8 total_recorded
      logical*1 l1
      double precision cartesian_coordinates(1:Ndim_mx)
      integer*8 index(1:Nvincell)
      integer vdim,Nv_in_cell
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision w(1:Nq_mx),sum_w,sum_Nq
      real dkadT,dksdT
      logical is_an_integer
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='program read_gas_radiative_properties_Tsensitivity_file'

      dim=3                     ! dimension of space

      if (command_argument_count().lt.1) then
         call error(label)
         write(*,*) 'Specify the access path to a gas radiative properties file'
         stop
      else if (command_argument_count().gt.1) then
         call error(label)
         write(*,*) 'Too many arguments'
         stop
      endif
      call get_command_argument(1,input_file)
c      
      open(11,file=trim(input_file),status='old',
     &     form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(input_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(input_file)
      endif
c     read page size
      read(11) page_size
      if (page_size.lt.0) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(page_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read number of bands
      read(11) Nband
      if (Nband.lt.0) then
         call error(label)
         write(*,*) 'Nband=',Nband
         write(*,*) 'should be positive'
         stop
      endif
c     Read number of nodes
      read(11) Nnode
      if (Nnode.lt.0) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'should be positive'
         stop
      endif
c     Read spectral metadata
      sum_Nq=0
      do iband=1,Nband
         read(11) lambda_min(iband),lambda_max(iband),Nq(iband)
         sum_Nq=sum_Nq+Nq(iband)
         do iq=1,Nq(iband)
            read(11) w(iq)
            if (w(iq).le.0.0D+0) then
               call error(label)
               write(*,*) 'band:',iband
               write(*,*) 'w(',iq,')=',w(iq)
               write(*,*) 'should be > 0'
               stop
            endif
            if (w(iq).gt.1.0D+0) then
               call error(label)
               write(*,*) 'band:',iband
               write(*,*) 'w(',iq,')=',w(iq)
               write(*,*) 'should be <= 1'
               stop
            endif
         enddo                  ! iq
         if (lambda_min(iband).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda_min(',iband,')=',lambda_min(iband)
            write(*,*) 'should be > 0'
            stop
         endif
         if (lambda_max(iband).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda_max(',iband,')=',lambda_max(iband)
            write(*,*) 'should be > 0'
            stop
         endif
         if (lambda_min(iband).gt.lambda_max(iband)) then
            call error(label)
            write(*,*) 'lambda_min(',iband,')=',lambda_min(iband)
            write(*,*) '> lambda_max(',iband,')=',lambda_max(iband)
            stop
         endif
c               
         sum_w=0.0D+0
         do iq=1,Nq(iband)
            sum_w=sum_w+w(iq)
         enddo                  ! iq
c     Debug
c         write(*,*) iband,lambda_min(iband),lambda_max(iband),Nq(iband)
c         write(*,*) (w(iq),iq=1,Nq(iband)),sum_w
c     Debug
         if (dabs(sum_w-1.0D+0).gt.1.0D-4) then
            call error(label)
            write(*,*) 'Band index:',iband
            write(*,*) 'quadrature weights:'
            do iq=1,Nq(iband)
               write(*,*) 'w(',iq,')=',w(iq)
            enddo               ! iq
            write(*,*) 'sum of weights=',sum_w
            write(*,*) 'should be close to 1'
            stop
         endif
      enddo                     ! iband
      do iband=1,Nband
         if (iband.lt.Nband) then
            if (lambda_max(iband).gt.lambda_min(iband+1)) then
               call error(label)
               write(*,*) 'lambda_max(',iband,')=',lambda_max(iband)
               write(*,*) '> lambda_min(',iband+1,')=',lambda_min(iband+1)
               stop
            endif
         endif
      enddo                     ! iband
c     ------------------------------------------------------------------
c     Padding
      total_recorded=(Nband+3)*size_of_int8+(sum_Nq+2*Nband)*size_of_double
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding

c     progress display
      ntot=Nnode*int(sum_Nq,kind(ntot))
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display
      do iband=1,Nband
         do inode=1,Nnode
            read(11) dksdT
            if (isnan(dksdT)) then
               call error(label)
               write(*,*) 'iband=',iband,' inode=',inode
               write(*,*) 'dksdT=',dksdT
               stop
            endif
         enddo                  ! inode
c     Padding
         total_recorded=Nnode*size_of_real
         call compute_padding2(page_size,total_recorded,remaining_byte)
         read(11) (l1,i=1,remaining_byte)
c     Padding
         do iq=1,Nq(iband)
            do inode=1,Nnode
               read(11) dkadT
               if (isnan(dkadT)) then
                  call error(label)
                  write(*,*) 'iband=',iband,' inode=',inode
                  write(*,*) 'dkadT=',dkadT
                  stop
               endif
c     progress display
               ndone=ndone+1
               fdone=dble(ndone)/dble(ntot)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no') floor(fdone),' %'
                  pifdone=ifdone
               endif
c     progress display
            enddo               ! inode
c     Padding
            total_recorded=Nnode*size_of_real
            call compute_padding2(page_size,total_recorded,remaining_byte)
            read(11) (l1,i=1,remaining_byte)
c     Padding
         enddo                  ! iq
      enddo                     ! iband
      close(11)

c     Debug
      write(*,*)
      write(*,*) 'page_size=',page_size
      write(*,*) 'Nnode=',Nnode
      write(*,*) 'Nband=',Nband
      write(*,*) 'Lower wavelength=',lambda_min(1),' nm'
      write(*,*) 'Higher wavelength=',lambda_max(Nband),' nm'
c     Debug

      end
