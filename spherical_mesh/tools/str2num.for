c     Copyright (C)
c     2008-2013 - Université Paul Sabatier
c     2008-2013 - Centre National de la Recherche Scientifique
c     2008-2013 - Ecole des Mines d'Albi
c     2008-2013 - Université de Bordeaux 1
c     2008-2013 - Fondation STAE
c     2014-2019 - Méso-Star
c
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine str2num(str,num,errcode)
      implicit none
      include 'max.inc'
c     
c     Purpose: convert a single character into a numeric value
c     
c     Input:
c       + str: character (of a integer between 0 and 9)
c     
c     Output:
c       + num: corresponding integer value (between 0 and 9)
c       + errcode: error code (a non-null value means there was a error)
c     
c     I/O
      character*1 str
      integer num
      integer errcode
c     label
      character*(Nchar_mx) label
      label='subroutine str2num'

      errcode=0
      if (str.eq."0") then
         num=0
      else if (str.eq."1") then
         num=1
      else if (str.eq."2") then
         num=2
      else if (str.eq."3") then
         num=3
      else if (str.eq."4") then
         num=4
      else if (str.eq."5") then
         num=5
      else if (str.eq."6") then
         num=6
      else if (str.eq."7") then
         num=7
      else if (str.eq."8") then
         num=8
      else if (str.eq."9") then
         num=9
      else
         errcode=1 ! character is not numeric
      endif

      return
      end
