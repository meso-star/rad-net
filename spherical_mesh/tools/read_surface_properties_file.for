      program read_surface_properties_file
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'size_params.inc'
c     
c     Purpose: to read a surface properties file produced by "spherical_mesh"
c     
c     Input arguments:
c     
c     1- path to the materials list file
c     
c     2- path to a surface properties file
c     
c     Variables
      character*(Nchar_mx) materials_list_file
      character*(Nchar_mx) surface_properties_file
      integer dim,ios,remaining_byte,i,j
      integer*8 page_size,Nface,Nrecord
      integer*8 iface
      integer*8 total_recorded
      logical*1 l1
      integer mtl_idx
      real T
      integer Nmat,ifile
      character*(Nchar_mx) filename,path
      logical file_exists
      integer Nchar
      integer idx(1:Nchar_mx)
      integer*8 record_size,alignment
      logical is_an_integer
      integer mtl_idx_min,mtl_idx_max
c     label
      character*(Nchar_mx) label
      label='program read_surface_properties_file'

      dim=3                     ! dimension of space

      if (command_argument_count().le.0) then
         call error(label)
         write(*,*) 'Specify the access path to a materials list file and a surface properties file'
         stop
      else if (command_argument_count().eq.1) then
         call error(label)
         write(*,*) 'Second argument missing: access path to a surface properties file'
         stop
      else if (command_argument_count().gt.2) then
         call error(label)
         write(*,*) 'Too many arguments'
         stop
      endif
      call get_command_argument(1,materials_list_file)
      call get_command_argument(2,surface_properties_file)
c     Materials list file
      call find_character(materials_list_file,'/',Nchar,idx)
      if (Nchar.le.0) then
         call error(label)
         write(*,*) 'Could not identify / character in path'
         stop
      endif
      path=materials_list_file(1:idx(Nchar))
      open(11,file=trim(materials_list_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(materials_list_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(materials_list_file)
      endif
      read(11,*) Nmat
      if (Nmat.lt.1) then
         call error(label)
         write(*,*) 'Nmat=',Nmat
         stop
      endif
      do ifile=1,Nmat
         read(11,10) filename
         filename=trim(path)//trim(filename)
         inquire(file=trim(filename),exist=file_exists)
         if (.not.file_exists) then
            call error(label)
            write(*,*) 'Material definition file index:',ifile
            write(*,*) 'not found: ',trim(filename)
            stop
         endif
      enddo                     ! ifile
      close(11)
      write(*,*) 'Material definition files: OK'
c     Surface properties file
      open(11,file=trim(surface_properties_file),status='old',
     &     form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(surface_properties_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(surface_properties_file)
      endif
c     read page size
      read(11) page_size
      if (page_size.lt.0) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(page_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read number of nodes
      read(11) Nface
      if (Nface.lt.0) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'should be positive'
         stop
      endif
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) '> Nf_so_mx=',Nf_so_mx
         stop
      endif
c     Read record size
      read(11) record_size
      if (record_size.lt.0) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(record_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
      if (record_size.ne.size_of_int4+size_of_real) then
         call error(label)
         write(*,*) 'record_size=',record_size
         write(*,*) 'should be equal to:',size_of_int4+size_of_real
         stop
      endif
c     Read alignment
      read(11) alignment
      if (alignment.lt.0) then
         call error(label)
         write(*,*) 'alignment=',alignment
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(alignment/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'alignment=',alignment
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=4*size_of_int8
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
c     Read node coordinates, sorted by increasing absolute node index
      mtl_idx_min=Nmaterial_mx
      mtl_idx_max=0
      do iface=1,Nface
         read(11) mtl_idx,T
         if ((mtl_idx.lt.0).or.(mtl_idx.gt.Nmat)) then
            call error(label)
            write(*,*) 'face index:',iface
            write(*,*) 'mtl_idx=',mtl_idx
            if (mtl_idx.le.0) then
               write(*,*) 'should be >= 0'
            else
               write(*,*) 'should be < Nmat=',Nmat
            endif
            stop
         endif
         if (T.le.0.0) then
            call error(label)
            write(*,*) 'face index:',iface
            write(*,*) 'T=',T
            write(*,*) 'should be positive'
            stop
         endif
         if (isnan(dble(T))) then
            call error(label)
            write(*,*) 'face index:',iface
            write(*,*) 'T=',T
            stop
         endif
         if (mtl_idx.lt.mtl_idx_min) then
            mtl_idx_min=mtl_idx
         endif
         if (mtl_idx.gt.mtl_idx_max) then
            mtl_idx_max=mtl_idx
         endif
      enddo                     ! iface
      if (mtl_idx_min.ne.0) then
         call error(label)
         write(*,*) 'minimum material index=',mtl_idx_min
         write(*,*) 'should be 0'
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=Nface*record_size
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
      close(11)

c     Debug
      write(*,*) 'page_size=',page_size
      write(*,*) 'record_size=',record_size
      write(*,*) 'alignment=',alignment
      write(*,*) 'Nface=',Nface
c     Debug

      end
