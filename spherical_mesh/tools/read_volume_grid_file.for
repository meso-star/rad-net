      program read_volume_grid_file
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: to read a volumic grid file produced by "spherical_mesh"
c     
c     This program takes one or two arguments on the command line:
c     - in read-only mode, the program will only read the binary grid file. The single argument
c     that should be specified is the path to the binary grid file.
c     - in read/write mode, the program will simultaneously read the binary grid file and dump
c     the grid into a ascii file; the two arguments that should be specfied are the binary
c     grid file and the ascii file for dumping the grid.
c     
c     Variables
      character*(Nchar_mx) grid_file
      integer dim,ios,remaining_byte,i,j
      integer record_size
      integer*8 page_size,Nnode,Ncell,Nrecord
      integer*8 inode,icell
      integer*8 total_recorded
      logical*1 l1
      double precision cartesian_coordinates(1:Nnode_mx,1:Ndim_mx)
      integer*8 index(1:Ncell_mx,1:Nvincell)
      integer vdim,Nv_in_cell
      double precision volume
      integer negp
      logical dump_grid
      character*(Nchar_mx) ascii_grid_file
c     label
      character*(Nchar_mx) label
      label='program read_volume_grid_file'

      dim=3                     ! dimension of space

      if (command_argument_count().lt.1) then
         call error(label)
         write(*,*) 'Specify the access path to a binary volume grid file'
         stop
      else if (command_argument_count().eq.1) then
         dump_grid=.false.
         call get_command_argument(1,grid_file)
      else if (command_argument_count().eq.2) then
         dump_grid=.true.
         call get_command_argument(1,grid_file)
         call get_command_argument(2,ascii_grid_file)
      else if (command_argument_count().gt.2) then
         call error(label)
         write(*,*) 'Too many arguments'
         stop
      endif
c     
      open(11,file=trim(grid_file),status='old',
     &     form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(grid_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(grid_file)
      endif
      if (dump_grid) then
         open(12,file=trim(ascii_grid_file))
      endif                     ! dump_grid
c     read page size
      read(11) page_size
c     Read number of nodes
      read(11) Nnode
      if (Nnode.gt.Nnode_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) '> Nnode_mx=',Nnode_mx
         stop
      endif
      if (dump_grid) then
         write(12,*) Nnode
      endif                     ! dump_grid
c     Read number of cells
      read(11) Ncell
      if (Ncell.gt.Ncell_mx) then
         call error(label)
         write(*,*) 'Ncell=',Ncell
         write(*,*) '> Ncell_mx=',Ncell_mx
         stop
      endif
c     Read spatial dimension
      read(11) vdim
      if (vdim.ne.dim) then
         call error(label)
         write(*,*) 'Dimension of space read:',vdim
         write(*,*) 'should be:',dim
         stop
      endif
c     Read the number of vertex per cell
      read(11) Nv_in_cell
      if (Nv_in_cell.ne.Nvincell) then
         call error(label)
         write(*,*) 'Number of vertex per cell read:',Nv_in_cell
         write(*,*) 'should be equal to:',Nvincell
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=3*size_of_int8+2*size_of_int4
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
c     Read node coordinates, sorted by increasing absolute node index
      do inode=1,Nnode
         read(11) (cartesian_coordinates(inode,j),j=1,vdim)
         do j=1,vdim
            if (isnan(cartesian_coordinates(inode,j))) then
               call error(label)
               write(*,*) 'node index:',inode
               write(*,*) 'cartesian_coordinates(',inode,',',j,')=',cartesian_coordinates(inode,j)
               stop
            endif
         enddo                  ! j
         if (dump_grid) then
            write(12,*) (cartesian_coordinates(inode,j),j=1,vdim)
         endif                  ! dump_grid
      enddo                     ! inode
c     ------------------------------------------------------------------
c     Padding
      record_size=vdim*size_of_double
      Nrecord=Nnode
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
      if (dump_grid) then
         write(12,*) Ncell
      endif                     ! dump_grid
c     Read cell vertices (absolute indexes of the 4 nodes that define each tetrahedron)
      do icell=1,Ncell
         read(11) (index(icell,j),j=1,Nv_in_cell)
         do j=1,Nv_in_cell
            index(icell,j)=index(icell,j)+1
            if ((index(icell,j).le.0).or.(index(icell,j).gt.Nnode)) then
               call error(label)
               write(*,*) 'cell index:',icell
               write(*,*) 'index(',icell,',',j,')=',index(icell,j)
               if (index(icell,j).lt.0) then
                  write(*,*) 'should be >= 0'
               else
                  write(*,*) 'should be < Nnode=',Nnode
               endif
               stop
            endif
         enddo                  ! j
         if (dump_grid) then
            write(12,*) (index(icell,j),j=1,Nv_in_cell)
         endif                  ! dump_grid
      enddo                     ! icell
c     ------------------------------------------------------------------
c     Padding
      record_size=Nvincell*size_of_int8
      Nrecord=Ncell
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------     
      close(11)
      if (dump_grid) then
         close(12)
         write(*,*) 'Grid was dumped into ascii file: ',trim(ascii_grid_file)
      endif                     ! dump_grid

      call volumic_grid_volume(vdim,Nnode,Ncell,Nv_in_cell,cartesian_coordinates,index,volume,negp)
c     Debug
      write(*,*) 'page_size=',page_size
      write(*,*) 'Nnode=',Nnode
      write(*,*) 'Ncell=',Ncell
      write(*,*) 'V=',volume,'m³'
      if (negp.gt.0) then
         write(*,*) 'Negative volume cells:',negp,' %'
      endif
c     Debug

      end
