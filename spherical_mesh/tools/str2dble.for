      subroutine str2dble(str,dp,errcode)
      implicit none
      include 'max.inc'
c     
c     Purpose: to convert a value contained within a character string
c     into its double precision value
c
c     Input:
c       + str: character string that contains a numerical value
c     This version can work with simple numbers such as 1.2345
c     or scientific-notation
c
c     Output:
c       + dp: double-precision variable that contains the numerical value
c       + errcode: 0 indicates no error; 1 indicates "str" contains non-numeric characters
c
c     I/O
      character*(Nchar_mx) str
      double precision dp
      integer errcode
c     temp
      integer n,i,i0,n0
      integer sign
      integer pf,pi
      character*(Nchar_mx) char
      integer num
      logical exponent_found
      character*(Nchar_mx) exp_str,str0
      integer exp_int
c     label
      character*(Nchar_mx) label
      label='subroutine str2dble'

c     Debug
c      write(*,*) trim(label),' in'
c      write(*,*) 'str=',trim(str)
c     Debug

      n=len_trim(str)
      if (str(1:1).eq.'-') then
         i0=2
         sign=-1.0D+0
      else
         i0=1
         sign=1.0D+0
      endif

      exponent_found=.false.
      do i=1,n
         if ((str(i:i).eq.'D').or.(str(i:i).eq.'E')) then
            exponent_found=.true.
            exp_str=str(i+1:n)
            str0=str(1:i-1)
            goto 222
         endif
      enddo                     ! i
 222  continue
      if (exponent_found) then
         call str2int(exp_str,exp_int,errcode)
         if (errcode.ne.0) then
            call error(label)
            write(*,*) 'could not convert to integer:'
            write(*,*) 'exp_str=',trim(exp_str)
            stop
         endif
      else
         str0=trim(str)
      endif
c     Debug
c      write(*,*) 'exponent_found=',exponent_found
c      if (exponent_found) then
c         write(*,*) 'exp_int=',exp_int
c      endif
c      write(*,*) 'str0=',trim(str0)
c     Debug

      n0=len_trim(str0)
      pf=0
      do i=1,n0
         if (str0(i:i).eq.'.') then
            if (pf.eq.1) then
               call error(label)
               write(*,*) 'multiple points defined in string:'
               write(*,*) trim(str0)
               stop
            else
               pf=1
               pi=i
            endif
         endif
      enddo ! i

      dp=0.0D+0
c     Debug
c      write(*,*) 'i0=',i0,' n0=',n0
c     Debug
      do i=i0,n0
         if ((pf.eq.1).and.(i.eq.pi)) then
            goto 111
         else
            char=str0(i:i)
            call str2num(char,num,errcode)
            if (errcode.eq.0) then
               if (((pf.eq.1).and.(i.lt.pi)).or.(pf.eq.0)) then
                  dp=dp*1.0D+1+num
               else
                  dp=dp+num/((10.0D+0)**(i-pi))
               endif
            else
c     Debug
c               write(*,*) 'char=',trim(char),' num=',num
c     Debug
               goto 666         ! propagate the error code
            endif               ! errcode=0
         endif                  ! pf=1 and i=pi
 111     continue
      enddo

      if (exponent_found) then
         dp=dp*(10**exp_int)
      endif
      dp=dp*sign
      
 666  continue
      
      return
      end
