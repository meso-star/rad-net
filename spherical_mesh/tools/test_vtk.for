      program test_vtk
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to generate a test VTK file
c     
c     Variables
      integer dim
      character*(Nchar_mx) descriptor
      integer*8 Nnode
      integer*8 Ncell
      double precision cartesian_coordinates(1:Nnode_mx,1:Ndim_mx)
      integer*8 index(1:Ncell_mx,1:Nvincell)
      integer*8 start_index
      character*(Nchar_mx) field_name
      double precision field(1:Ncell_mx)
      character*(Nchar_mx) vtk_file
c     label
      character*(Nchar_mx) label
      label='program test_vtk'

      dim=3
      descriptor='Simple tetrahedral grid'
      Nnode=6
      Ncell=2
      cartesian_coordinates(1,1)=0.0D+0
      cartesian_coordinates(1,2)=0.0D+0
      cartesian_coordinates(1,3)=0.0D+0
      cartesian_coordinates(2,1)=1.0D+0
      cartesian_coordinates(2,2)=0.0D+0
      cartesian_coordinates(2,3)=0.0D+0
      cartesian_coordinates(3,1)=0.0D+0
      cartesian_coordinates(3,2)=1.0D+0
      cartesian_coordinates(3,3)=0.0D+0
      cartesian_coordinates(4,1)=0.0D+0
      cartesian_coordinates(4,2)=0.0D+0
      cartesian_coordinates(4,3)=1.0D+0
      cartesian_coordinates(5,1)=1.0D+0
      cartesian_coordinates(5,2)=1.0D+0
      cartesian_coordinates(5,3)=0.0D+0
      cartesian_coordinates(6,1)=1.0D+0
      cartesian_coordinates(6,2)=1.0D+0
      cartesian_coordinates(6,3)=1.0D+0
      index(1,1)=0
      index(1,2)=1
      index(1,3)=2
      index(1,4)=3
      index(2,1)=1
      index(2,2)=4
      index(2,3)=2
      index(2,4)=5
      start_index=0
      field_name='ka'
      field(1)=0.1
      field(2)=0.35
      vtk_file='./ka.vtk'
      
      call dump_tetrahedral_grid_to_vtk(dim,descriptor,Nnode,Ncell,cartesian_coordinates,index,start_index,field_name,field,vtk_file)

      end
      
