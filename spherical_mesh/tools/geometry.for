      subroutine trianglemesh_area(dim,Nv,Nf,v,f,area)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the area of a trianglemesh
c     
c     Input:
c       + dim: dimension of space 
c       + Nv, v, Nf, f: definition of the trianglmesh
c     
c     Output:
c       + area: area of the trianglmesh
c     
c     I/O
      integer dim
      integer*8 Nv
      integer*8 Nf
      double precision v(1:Nv_so_mx,1:Ndim_mx)
      integer*8 f(1:Nf_so_mx,1:3)
      double precision area
c     temp
      integer*8 iface
      integer j
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision dA
c     label
      character*(Nchar_mx) label
      label='subroutine trianglemesh_area'

      area=0.0D+0
      do iface=1,Nf
         do j=1,dim
            p1(j)=v(f(iface,1),j)
            p2(j)=v(f(iface,2),j)
            p3(j)=v(f(iface,3),j)
         enddo                  ! j
         call triangle_area(dim,p1,p2,p3,dA)
         area=area+dA
c     Debug
c         if (dA.lt.0.0D+0) then
c            write(*,*) 'iface=',iface
c            write(*,*) 'dA=',dA
c            write(*,*) 'p1=',p1
c            write(*,*) 'p2=',p2
c            write(*,*) 'p3=',p3
c            stop
c         endif
c     Debug
      enddo ! iface
      
      return
      end



      subroutine triangle_area(dim,p1,p2,p3,area)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the area of a triangle
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c     
c     Output:
c       + area: area of the triangle
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision area
c     temp
      double precision v12(1:Ndim_mx)
      double precision v13(1:Ndim_mx)
      double precision w(1:Ndim_mx)
      double precision length
c     label
      character*(Nchar_mx) label
      label='subroutine triangle_area'

      call substract_vectors(dim,p2,p1,v12)
      call substract_vectors(dim,p3,p1,v13)
      call vector_product(dim,v12,v13,w)
      call vector_length(dim,w,length)
      area=length/2.0D+0

      return
      end



      subroutine enclosure_volume(dim,Nv,Nf,v,f,volume,negp)
      implicit none
      include 'max.inc'
c     
c     Purpose: to evaluate the volume of a enclosure defined by a surfacic grid
c     
c     Input:
c       + dim: dimension of space 
c       + Nv,Nf,v,f: defintion of the enclosure as a trianglemesh
c     
c     Output:
c       + volume: enclosed volume
c       + negp: percentage of cells with a negative volume (0-100)
c     
c     I/O
      integer dim
      integer*8 Nv
      integer*8 Nf
      double precision v(1:Nv_so_mx,1:Ndim_mx)
      integer*8 f(1:Nf_so_mx,1:3)
      double precision volume
      integer negp
c     temp
      integer*8 vidx,fidx,Nvneg
      integer j
      double precision minmax(1:Ndim_mx,1:2)
      double precision p0(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision dV
c     label
      character*(Nchar_mx) label
      label='subroutine enclosure_volume'

c     get the min and max of the enclose along each direction
      do j=1,dim
         minmax(j,2)=v(1,j)
      enddo                     ! j
      do vidx=1,Nv
         do j=1,dim
            if (v(vidx,j).gt.minmax(j,2)) then
               minmax(j,2)=v(vidx,j)
            endif
         enddo                  ! j
      enddo                     ! vidx
      do j=1,dim
         minmax(j,1)=minmax(j,2)
      enddo                     ! j
      do vidx=1,Nv
         do j=1,dim
            if (v(vidx,j).lt.minmax(j,1)) then
               minmax(j,1)=v(vidx,j)
            endif
         enddo                  ! j
      enddo                     ! vidx
      
c     guess a central position
      do j=1,dim
         p0(j)=(minmax(j,1)+minmax(j,2))/2.0D+0
      enddo                     ! j

c     compute the volume
      volume=0.0D+0
      Nvneg=0
      do fidx=1,Nf
         do j=1,dim
            p1(j)=v(f(fidx,1),j)
            p2(j)=v(f(fidx,3),j)
            p3(j)=v(f(fidx,2),j)
         enddo                  ! j
         call tetrahedron_volume(dim,p1,p2,p3,p0,dV)
         volume=volume+dV
         if (dV.le.0.0D+0) then
            Nvneg=Nvneg+1
         endif
      enddo                     ! fidx
      negp=nint(dble(Nvneg)/dble(Nf)*1.0D+2)
            
      return
      end



      subroutine volumic_grid_volume(dim,Nnode,Ncell,Nv_in_cell,coordinates,index,volume,negp)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: compute the volume of a tetrahedric mesh
c     
c     Input:
c       + dim: dimension of space
c       + Nnode: number of nodes
c       + Ncell: number of cells
c       + Nv_in_cell: number of nodes that define a cell
c       + coordinates: coordinates of each node
c       + index: indexes of each node for each cell
c     
c     Output:
c       + volume: volume of the mesh
c       + negp: percentage of cells with a negative volume (0-100)
c     
c     I/O
      integer dim
      integer*8 Nnode
      integer*8 Ncell
      integer Nv_in_cell
      double precision coordinates(1:Nnode_mx,1:Ndim_mx)
      integer*8 index(1:Ncell_mx,1:Nvincell)
      double precision volume
      integer negp
c     temp
      integer*8 inode,icell,Nvneg
      integer j
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision p4(1:Ndim_mx)
      double precision dV
c     label
      character*(Nchar_mx) label

      volume=0.0D+0
      Nvneg=0
      do icell=1,Ncell
         do j=1,dim
            p1(j)=coordinates(index(icell,1),j)
            p2(j)=coordinates(index(icell,2),j)
            p3(j)=coordinates(index(icell,3),j)
            p4(j)=coordinates(index(icell,4),j)
         enddo                  ! j
         call tetrahedron_volume(dim,p1,p2,p3,p4,dV)
         volume=volume+dV
         if (dV.le.0.0D+0) then
            Nvneg=Nvneg+1
c     Debug
            write(*,*) 'icell=',icell,' /',Ncell,' dV=',dV
            write(*,*) 'indexes:',(index(icell,j),j=1,4)
            stop
c     Debug
         endif
      enddo                     ! icell
      negp=nint(dble(Nvneg)/dble(Ncell)*1.0D+2)

      return
      end
      


      subroutine tetrahedron_volume(dim,p1,p2,p3,p4,volume)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the volume of a tetrahedron from the
c     coordinates of its 4 nodes.
c     Note: the volume will be positive when (p1, p2, p3) define
c     a triangular base of the tetrahedron, provided a order such
c     that vector n=(p1p2)x(p1p3) is directed in the same direction
c     as vector (p1p4).
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c       + p4: coordinates of the 4th point
c     
c     Output:
c       + volume: volume of the tetrahedron
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision p4(1:Ndim_mx)
      double precision volume
c     temp
      double precision u12(1:Ndim_mx)
      double precision u13(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision product
c     label
      character*(Nchar_mx) label
      label='subroutine tetrahedron_volume'

      call substract_vectors(dim,p2,p1,u12)
      call substract_vectors(dim,p3,p1,u13)
      call substract_vectors(dim,p4,p1,u14)
      call vector_product(dim,u12,u13,n)
      call vector_vector(dim,n,u14,product)
      volume=product/6.0D+0
      
      return
      end
