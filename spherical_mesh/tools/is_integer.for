      subroutine is_integer(in,is_an_integer)
      implicit none
      include 'max.inc'
c
c     Purpose: to detect wether the provided (double precision)
c     value 'in' is an integer value or not
c
c     Input:
c       + in: double precision data
c
c     Output:
c       + is_an_integer: true if "in" is a integer value; false otherwise
c
c     I/O
      double precision in
      logical is_an_integer
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine is_integer'

      if (dble(int(in)).eq.in) then
         is_an_integer=.true.
      else
         is_an_integer=.false.
      endif

      return
      end
