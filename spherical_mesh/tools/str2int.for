      subroutine str2int(str,intv,errcode)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert a character string to an integer
c
c     Inputs:
c       + str: character string
c
c     Outputs:
c       + intv: integer value
c       + errcode: 0 indicates no error; 1 indicates "str" contains non-numeric characters
c
c     I/O
      character*(Nchar_mx) str
      integer intv,errcode
c     temp
      integer absnum,index
      integer n,i,sign,first
      character*1 char
c     label
      character*(Nchar_mx) label
      label='subroutine str2int'

      n=len_trim(str)
      if (str(1:1).eq.'-') then
         sign=-1
         first=2
      else
         sign=1
         first=1
      endif

      absnum=0
      errcode=0
      do i=first,n
         char=str(i:i)
         call identify_index(char,index)
         if (index.eq.-1) then
            errcode=1
            goto 666
         endif
         absnum=10*absnum+index
      enddo ! i

      if (sign.eq.-1) then
         intv=-absnum
      else
         intv=absnum
      endif

 666  continue
      return
      end


      
      subroutine identify_index(char,index)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to identify the index that corresponds to the input character
c
c     Input:
c       + char: single character
c
c     Outputs:
c       + index: numerical index (0-9); a value of -1 indicates "char" is a non-numeric character
c

c     I/O
      character*1 char
      integer index
c     temp
      integer i,indexf
      character*1 ich
c     label
      character*(Nchar_mx) label
      label='subroutine identify_index'

      indexf=0
      do i=0,9
         write(ich,11) i
         if (trim(char).eq.trim(ich)) then
            indexf=1
            index=i
            goto 111
         endif
      enddo ! i
 111  continue
      if (indexf.eq.0) then
         index=-1
      endif

      return
      end
