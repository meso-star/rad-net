      program read_surface_grid_file
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to read a surface grid file produced by "spherical_mesh"
c     
c     Input arguments:
c     1- full path to the binary grid file to read
c     
c     Variables
      character*(Nchar_mx) grid_file
      integer dim,ios,remaining_byte,i,j
      integer record_size
      integer*8 page_size,Nnode,Nface,Nrecord
      integer*8 inode,iface
      integer*8 total_recorded
      logical*1 l1
      double precision spherical_coordinates(1:Nv_so_mx,1:Ndim_mx)
      integer vdim,Nv_in_face
      integer Ntheta,Nphi,itheta,iphi
      logical keep_looking,Nphi_found,homogeneous_grid
      double precision delta_theta
      integer Nv
      double precision v(1:Nv_so_mx,1:Ndim_mx)
      integer Nf
      integer*8 f(1:Nf_so_mx,1:3)
      double precision area,volume
      integer negp
      logical is_an_integer
c     label
      character*(Nchar_mx) label
      label='program read_surface_grid_file'

      dim=3                     ! dimension of space

      if (command_argument_count().lt.1) then
         call error(label)
         write(*,*) 'Specify the access path to a surface grid file'
         stop
      else if (command_argument_count().gt.1) then
         call error(label)
         write(*,*) 'Too many arguments'
         stop
      endif
      call get_command_argument(1,grid_file)
c     
      open(11,file=trim(grid_file),status='old',
     &     form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(grid_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(grid_file)
      endif
c     read page size
      read(11) page_size
      if (page_size.lt.0) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(page_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read number of nodes
      read(11) Nnode
      if (Nnode.lt.0) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'should be positive'
         stop
      endif
      if (Nnode.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) '> Nv_so_mx=',Nv_so_mx
         stop
      endif
c     Read number of faces
      read(11) Nface
      if (Nface.lt.0) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) 'should be positive'
         stop
      endif
      if (Nface.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) '> Nf_so_mx=',Nf_so_mx
         stop
      endif
c     Read spatial dimension
      read(11) vdim
      if (vdim.lt.0) then
         call error(label)
         write(*,*) 'vdim=',vdim
         write(*,*) 'should be positive'
         stop
      endif
      if (vdim.ne.dim) then
         call error(label)
         write(*,*) 'Dimension of space read:',vdim
         write(*,*) 'should be:',dim
         stop
      endif
c     Read the number of vertex per face
      read(11) Nv_in_face
      if (Nv_in_face.lt.0) then
         call error(label)
         write(*,*) 'Nv_in_face=',Nv_in_face
         write(*,*) 'should be positive'
         stop
      endif
      if (Nv_in_face.ne.Nvinface) then
         call error(label)
         write(*,*) 'Number of vertex per face read:',Nv_in_face
         write(*,*) 'should be equal to:',Nvinface
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=3*size_of_int8+2*size_of_int4
      call compute_padding2(page_size,total_recorded,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
c     Read node coordinates, sorted by increasing absolute node index
      do inode=1,Nnode
         read(11) (v(inode,j),j=1,vdim)
         do j=1,vdim
            if (isnan(v(inode,j))) then
               call error(label)
               write(*,*) 'node index:',inode
               write(*,*) 'v(',inode,',',j,')=',v(inode,j)
               stop
            endif
         enddo                  ! j
      enddo                     ! inode
c     ------------------------------------------------------------------
c     Padding
      record_size=vdim*size_of_double
      Nrecord=Nnode
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
c     Read face vertices (absolute indexes of the 4 nodes that define each tetrahedron)
      do iface=1,Nface
         read(11) (f(iface,j),j=1,Nv_in_face)
         do j=1,Nv_in_face
            f(iface,j)=f(iface,j)+1 ! Add 1 to node indexes since HTRDR likes them from 0
            if ((f(iface,j).le.0).or.(f(iface,j).gt.Nnode)) then
               call error(label)
               write(*,*) 'face index:',iface
               write(*,*) 'f(,',j,')=',f(iface,j)
               if (f(iface,j).lt.0) then
                  write(*,*) 'should be >= 0'
               else
                  write(*,*) 'should be < Nnode=',Nnode
               endif
               stop
            endif
         enddo                  ! j
      enddo                     ! iface
c     ------------------------------------------------------------------
c     Padding
      record_size=Nvinface*size_of_int8
      Nrecord=Nface
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------     
      close(11)

c     Debug
      write(*,*) 'page_size=',page_size
      write(*,*) 'Nnode=',Nnode
      write(*,*) 'Nface=',Nface
c     Debug

c     Data analysis
      write(*,*) 'Data consistency...'
c     convert cartesian to spherical coordinates
      do inode=1,Nnode
         call cart2spher(v(inode,1),v(inode,2),v(inode,3),
     &        spherical_coordinates(inode,1),spherical_coordinates(inode,2),spherical_coordinates(inode,3))
      enddo                     ! inode
c     Guess the number of points over longitude -> Nphi, NOT the number of longitude intervals
      keep_looking=.true.
      inode=1
      do while (keep_looking)
         inode=inode+1
         if (spherical_coordinates(inode,2).gt.spherical_coordinates(2,2)) then
            keep_looking=.false.
            Nphi=inode-2
            Nphi_found=.true.
         endif
         if (inode.gt.Nnode) then
            keep_looking=.false.
            Nphi_found=.false.
         endif
      enddo                     ! while (keep_looking)
      if (Nphi_found) then
         delta_theta=spherical_coordinates(Nphi+2,2)-spherical_coordinates(Nphi+1,2)
         Ntheta=anint(pi/delta_theta)
         if (Nnode.eq.Nphi*(Ntheta-1)+2) then
            homogeneous_grid=.true.
         else
            homogeneous_grid=.false.
         endif
      else
         call error(label)
         write(*,*) 'Difficulty to identify Nphi'
         stop
      endif
      if (homogeneous_grid) then
         write(*,*) 'Homogeneous grid detected'
         write(*,*) 'Number of latitude intervals:',Ntheta
         write(*,*) 'Number of longitude intervals:',Nphi
         if (spherical_coordinates(1,2).ne.-pi/2.0D+0) then
            call error(label)
            write(*,*) 'spherical_coordinates(1,2)=',spherical_coordinates(1,2)
            write(*,*) 'should be equal to -pi/2=',-pi/2.0D+0
            stop
         endif
         if (spherical_coordinates(Nnode,2).ne.pi/2.0D+0) then
            call error(label)
            write(*,*) 'spherical_coordinates(',Nnode,',2)=',spherical_coordinates(Nnode,2)
            write(*,*) 'should be equal to pi/2=',pi/2.0D+0
            stop
         endif
         do itheta=1,Ntheta-1
            inode=Nphi*(itheta-1)+2
            if (spherical_coordinates(inode,2).le.spherical_coordinates(inode-1,2)) then
               call error(label)
               write(*,*) 'Change of latitude interval'
               write(*,*) 'Node ',inode-1,':',(spherical_coordinates(inode-1,j),j=1,vdim)
               write(*,*) 'Node ',inode,':',(spherical_coordinates(inode-1,j),j=1,vdim)
               stop
            endif
            do iphi=2,Nphi
               inode=Nphi*(itheta-1)+iphi+1
               if (spherical_coordinates(inode,3).le.spherical_coordinates(inode-1,3)) then
                  call error(label)
                  write(*,*) 'Change of longitude interval'
                  write(*,*) 'Node ',inode-1,':',(spherical_coordinates(inode-1,j),j=1,vdim)
                  write(*,*) 'Node ',inode,':',(spherical_coordinates(inode-1,j),j=1,vdim)
                  stop
               endif
            enddo               ! iphi
         enddo                  ! itheta
      else
         write(*,*) 'Inhomogeneous grid'
      endif
      write(*,*) 'no inconsistency found'
c     
      call trianglemesh_area(dim,Nnode,Nface,v,f,area)
      write(*,*) 'S=',area,'m²'
      call enclosure_volume(dim,Nnode,Nface,v,f,volume,negp)
      write(*,*) 'V=',volume,'m³'
      if (negp.gt.0) then
         write(*,*) 'Negative volume cells:',negp,' %'
      endif
      write(*,*) '4V/S=',4.0D+0*volume/area
      write(*,*) 'Best guess for radius is:',3.0D-3*volume/area,' km'
c      
      end
