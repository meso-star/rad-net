      program check_new_gas_grid
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: to check a gas "independant cells" grid file and radiative properties
c     
c     This program takes 6 arguments on the command line:
c     1- file that contains the tetrahedric grid of the gas
c     2- file that contains the radiative properties of the gas
c     3- Nlayer: number of altitude intervals used to produce the original (lon/lat/alt) grid
c     4- Ntheta: number of latitude intervals used to produce the original (lon/lat/alt) grid
c     5- Nphi: number of longitude intervals used to produce the original (lon/lat/alt) grid
c     
c     
c     Input arguments
      character*(Nchar_mx) grid_file
      character*(Nchar_mx) radiative_properties_file
      character*(Nchar_mx) Nlayer_str
      character*(Nchar_mx) Ntheta_str
      character*(Nchar_mx) Nphi_str
c     Variables
      integer Nlayer
      integer Ntheta
      integer Nphi
      integer dim,ios,remaining_byte,i,j,ilayer,itheta,iphi,cell_idx,inodeincell
      integer record_size
      integer*8 page_size,Nnode,Nnode_tmp,Ncell,Nrecord,Nband,Nq(1:Nb_mx),sum_Nq
      integer*8 Nnode_assumed,Ncell_assumed
      integer*8 inode,icell
      integer*8 total_recorded
      logical*1 l1
      double precision cartesian_coordinates(1:Nnode_mx,1:Ndim_mx)
      integer*8 index(1:Ncell_mx,1:Nvincell),index_min
      integer*8 index1(1:Ncell_mx,1:Nvincell)
      integer vdim,Nv_in_cell,Nnode_sb,Ncell_sb,Nnode_sl,Ncell_sl
      double precision volume
      integer negp,iband,iq,errcode
      character*(Nchar_mx) command
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision w(1:Nq_mx),sum_w
      real ka(1:Nnode_mx),ks(1:Nnode_mx)
      logical is_an_integer
      logical err_found
      character*(Nchar_mx) descriptor
      integer*8 start_index
      character*(Nchar_mx) field_name
      double precision ka_ref,ks_ref
      logical file_exists
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
c     label
      character*(Nchar_mx) label
      label='program check_new_gas_grid'

      dim=3                     ! dimension of space

      if (command_argument_count().lt.5) then
         call error(label)
         write(*,*) 'Too few input arguments'
         stop
      else if (command_argument_count().eq.5) then
         call get_command_argument(1,grid_file)
         call get_command_argument(2,radiative_properties_file)
         call get_command_argument(3,Nlayer_str)
         call get_command_argument(4,Ntheta_str)
         call get_command_argument(5,Nphi_str)
      else
         call error(label)
         write(*,*) 'Too many input arguments'
         stop
      endif
      call str2int(Nlayer_str,Nlayer,errcode)
      if (errcode.ne.0) then
         call error(label)
         write(*,*) 'Could not convert to integer:'
         write(*,*) 'Nlayer_str="',trim(Nlayer_str),'"'
         stop
      endif
      call str2int(Ntheta_str,Ntheta,errcode)
      if (errcode.ne.0) then
         call error(label)
         write(*,*) 'Could not convert to integer:'
         write(*,*) 'Ntheta_str="',trim(Ntheta_str),'"'
         stop
      endif
      call str2int(Nphi_str,Nphi,errcode)
      if (errcode.ne.0) then
         call error(label)
         write(*,*) 'Could not convert to integer:'
         write(*,*) 'Nphi_str="',trim(Nphi_str),'"'
         stop
      endif
      if (Nlayer.lt.1) then
         call error(label)
         write(*,*) 'Nlayer=',Nlayer
         write(*,*) 'should be > 0'
         stop
      endif
      if (Ntheta.lt.1) then
         call error(label)
         write(*,*) 'Ntheta=',Ntheta
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nphi.lt.1) then
         call error(label)
         write(*,*) 'Nphi=',Nphi
         write(*,*) 'should be > 0'
         stop
      endif
c     
      Ncell_sl=6*Nphi*(Ntheta-1)          ! number of cells per single vertical layer
      Nnode_sl=6*Nvincell*Nphi*(Ntheta-1) ! number of nodes per altitude layer
      Ncell_assumed=Ncell_sl*Nlayer       ! assumed number of cells
      Nnode_assumed=Nnode_sl*Nlayer       ! assumed number of nodes
c     Debug
      write(*,*) 'Ntheta=',Ntheta,' Nphi=',Nphi,' Nlayer=',Nlayer
      write(*,*) 'Ncell_sl=',Ncell_sl
      write(*,*) 'Nnode_sl=',Nnode_sl
      write(*,*) 'Nnode_assumed=',Nnode_assumed
      write(*,*) 'Ncell_assumed=',Ncell_assumed
c     Debug

c     ======================================================================================
c     Tetrahedric grid
c     ======================================================================================
c     Debug
      write(*,*) '------------------------------------------------------------------'
      write(*,*) 'Volumic grid file'
      write(*,*) '------------------------------------------------------------------'
c     Debug
      open(11,file=trim(grid_file),status='old',form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(grid_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(grid_file)
      endif
c     read page size
      read(11) page_size
c     Debug
      write(*,*) 'page_size=',page_size
c     Debug
c     Read number of nodes
      read(11) Nnode
c     Debug
      write(*,*) 'Nnode=',Nnode
c     Debug
      if (Nnode.gt.Nnode_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) '> Nnode_mx=',Nnode_mx
         stop
      endif
      if (Nnode.ne.Nnode_assumed) then
         call error(label)
         write(*,*) 'value read: Nnode=',Nnode
         write(*,*) 'value assumed:',Nnode_assumed
         stop
      endif
c     Read number of cells
      read(11) Ncell
c     Debug
      write(*,*) 'Ncell=',Ncell
c     Debug
      if (Ncell.gt.Ncell_mx) then
         call error(label)
         write(*,*) 'Ncell=',Ncell
         write(*,*) '> Ncell_mx=',Ncell_mx
         stop
      endif
      if (Ncell.ne.Ncell_assumed) then
         call error(label)
         write(*,*) 'value read: Ncell=',Ncell
         write(*,*) 'value assumed:',Ncell_assumed
         stop
      endif
c     Read spatial dimension
      read(11) vdim
c     Debug
      write(*,*) 'vdim=',vdim
c     Debug
      if (vdim.ne.dim) then
         call error(label)
         write(*,*) 'Dimension of space read:',vdim
         write(*,*) 'should be:',dim
         stop
      endif
c     Read the number of vertex per cell
      read(11) Nv_in_cell
c     Debug
      write(*,*) 'Nv_in_cell=',Nv_in_cell
c     Debug
      if (Nv_in_cell.ne.Nvincell) then
         call error(label)
         write(*,*) 'Number of vertex per cell read:',Nv_in_cell
         write(*,*) 'should be equal to:',Nvincell
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      total_recorded=3*size_of_int8+2*size_of_int4
      call compute_padding2(page_size,total_recorded,remaining_byte)
c     Debug
      write(*,*) 'First padding:',remaining_byte,' bytes'
c     Debug      
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
c     Read node coordinates, sorted by increasing absolute node index
      do inode=1,Nnode
         read(11) (cartesian_coordinates(inode,j),j=1,vdim)
         do j=1,vdim
            if (isnan(cartesian_coordinates(inode,j))) then
               call error(label)
               write(*,*) 'node index:',inode
               write(*,*) 'cartesian_coordinates(',inode,',',j,')=',cartesian_coordinates(inode,j)
               stop
            endif
         enddo                  ! j
      enddo                     ! inode
c     ------------------------------------------------------------------
c     Padding
      record_size=vdim*size_of_double
      Nrecord=Nnode
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
c     Debug
      write(*,*) 'Second padding:',remaining_byte,' bytes'
c     Debug
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------
c     Read cell vertices (absolute indexes of the 4 nodes that define each tetrahedron)
      do icell=1,Ncell
         read(11) (index(icell,j),j=1,Nv_in_cell) ! start @ 0
         do j=1,Nv_in_cell
            if ((index(icell,j).lt.0).or.(index(icell,j).ge.Nnode)) then
               call error(label)
               write(*,*) 'cell index:',icell
               write(*,*) 'index(',icell,',',j,')=',index(icell,j)
               if (index(icell,j).lt.0) then
                  write(*,*) 'should be >= 0'
               else
                  write(*,*) 'should be < Nnode=',Nnode
               endif
               stop
            endif
            index1(icell,j)=index(icell,j)+1 ! start @ 1
         enddo                  ! j
      enddo                     ! icell
c     Detect minimum value of "index"
      index_min=index(1,1)
      do icell=1,Ncell
         do j=1,Nvincell
            if (index(icell,j).lt.index_min) then
               index_min=index(icell,j)
            endif
         enddo                  ! j
      enddo                     ! icell
      if (index_min.ne.0) then
         call error(label)
         write(*,*) 'Minimum value of index:',index_min
         stop
      endif
c     ------------------------------------------------------------------
c     Padding
      record_size=Nvincell*size_of_int8
      Nrecord=Ncell
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
c     Debug
c      write(*,*) 'record_size=',record_size
c      write(*,*) 'Nrecord=',Nrecord
c      write(*,*) 'Total recorded:',Nrecord*record_size
c      write(*,*) 'Number of pages:',dble(Nrecord*record_size)/dble(pagesize)
      write(*,*) 'Third padding:',remaining_byte,' bytes'
c     Debug
      read(11) (l1,i=1,remaining_byte)
c     Padding
c     ------------------------------------------------------------------     
      close(11)

      call volumic_grid_volume(vdim,Nnode,Ncell,Nv_in_cell,cartesian_coordinates,index1,volume,negp)
c     Debug
c      write(*,*) 'page_size=',page_size
c      write(*,*) 'Nnode=',Nnode
c      write(*,*) 'Ncell=',Ncell
      write(*,*) 'V=',volume,'m³'
      if (negp.gt.0) then
         write(*,*) 'Negative volume cells:',negp,' %'
      endif
c      stop
c     Debug

c     ======================================================================================
c     Radiative properties
c     ======================================================================================
      
c     Debug
      write(*,*) '------------------------------------------------------------------'
      write(*,*) 'Radiative properties file'
      write(*,*) '------------------------------------------------------------------'
c     Debug
      open(12,file=trim(radiative_properties_file),status='old',form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(radiative_properties_file)
         stop
      else
         write(*,*) 'Reading file:'
         write(*,*) trim(radiative_properties_file)
      endif
c     read page size
      read(12) page_size
c     Debug
      write(*,*) 'page_size=',page_size
c     Debug
      if (page_size.lt.0) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'should be positive'
         stop
      endif
      call is_integer(dble(page_size/2),is_an_integer)
      if (.not.is_an_integer) then
         call error(label)
         write(*,*) 'page_size=',page_size
         write(*,*) 'is not a multiple of 2'
         stop
      endif
c     Read number of bands
      read(12) Nband
c     Debug
      write(*,*) 'Nband=',Nband
c     Debug
      if (Nband.lt.0) then
         call error(label)
         write(*,*) 'Nband=',Nband
         write(*,*) 'should be positive'
         stop
      endif
c     Read number of nodes
      read(12) Nnode_tmp
c     Debug
      write(*,*) 'Nnode=',Nnode
c     Debug
      if (Nnode_tmp.lt.0) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) 'should be positive'
         stop
      endif
      if (Nnode_tmp.ne.Nnode) then
         call error(label)
         write(*,*) 'Major inconsistency:'
         write(*,*) 'Number of nodes from grid file:',Nnode
         write(*,*) 'Number of nodes from radiative prop. file:',Nnode_tmp
         write(*,*) 'Check input file names & argument order'
         stop
      endif
c     Read spectral metadata
      sum_Nq=0
c     Debug
      write(*,*) 'band / lower lambda [nm] / higher lambda [nm] / quad. order'
c     Debug
      do iband=1,Nband
         read(12) lambda_min(iband),lambda_max(iband),Nq(iband)
c     Debug
         write(*,*) iband,lambda_min(iband),lambda_max(iband),Nq(iband)
c     Debug
         sum_Nq=sum_Nq+Nq(iband)
         if (Nq(iband).lt.0) then
            call error(label)
            write(*,*) 'band=',iband
            write(*,*) 'Nq=',Nq(iband)
            write(*,*) 'should be positive'
            stop
         endif
         do iq=1,Nq(iband)
            read(12) w(iq)
            if (w(iq).le.0.0D+0) then
               call error(label)
               write(*,*) 'band:',iband
               write(*,*) 'w(',iq,')=',w(iq)
               write(*,*) 'should be > 0'
               stop
            endif
            if (w(iq).gt.1.0D+0) then
               call error(label)
               write(*,*) 'band:',iband
               write(*,*) 'w(',iq,')=',w(iq)
               write(*,*) 'should be <= 1'
               stop
            endif
         enddo                  ! iq
         if (lambda_min(iband).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda_min(',iband,')=',lambda_min(iband)
            write(*,*) 'should be > 0'
            stop
         endif
         if (lambda_max(iband).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda_max(',iband,')=',lambda_max(iband)
            write(*,*) 'should be > 0'
            stop
         endif
         if (lambda_min(iband).gt.lambda_max(iband)) then
            call error(label)
            write(*,*) 'lambda_min(',iband,')=',lambda_min(iband)
            write(*,*) '> lambda_max(',iband,')=',lambda_max(iband)
            stop
         endif
c     
         sum_w=0.0D+0
         do iq=1,Nq(iband)
            sum_w=sum_w+w(iq)
         enddo                  ! iq
         if (dabs(sum_w-1.0D+0).gt.1.0D-4) then
            call error(label)
            write(*,*) 'Band index:',iband
            write(*,*) 'quadrature weights:'
            do iq=1,Nq(iband)
               write(*,*) 'w(',iq,')=',w(iq)
            enddo               ! iq
            write(*,*) 'sum of weights=',sum_w
            write(*,*) 'should be close to 1'
            stop
         endif
      enddo                     ! iband
      do iband=1,Nband
         if (iband.lt.Nband) then
            if (lambda_max(iband).gt.lambda_min(iband+1)) then
               call error(label)
               write(*,*) 'lambda_max(',iband,')=',lambda_max(iband)
               write(*,*) '> lambda_min(',iband+1,')=',lambda_min(iband+1)
               stop
            endif
         endif
      enddo                     ! iband
c     ------------------------------------------------------------------
c     Padding
      total_recorded=(Nband+3)*size_of_int8+(sum_Nq+2*Nband)*size_of_double
      call compute_padding2(page_size,total_recorded,remaining_byte)
c     Debug
      write(*,*) 'First padding:',remaining_byte,' bytes'
c     Debug
      read(12) (l1,i=1,remaining_byte)
c     Padding

c     progress display
      ntot=Nnode*int(sum_Nq,kind(ntot))
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display
      do iband=1,Nband
         do inode=1,Nnode
            read(12) ks(inode)
            if (ks(inode).lt.0.0D+0) then
               call error(label)
               write(*,*) 'iband=',iband,' inode=',inode
               write(*,*) 'ks=',ks(inode)
               write(*,*) 'shoud be positive'
               stop
            endif
            if (isnan(dble(ks(inode)))) then
               call error(label)
               write(*,*) 'iband=',iband,' inode=',inode
               write(*,*) 'ks=',ks(inode)
               stop
            endif
         enddo                  ! inode
c     Padding
         total_recorded=Nnode*size_of_real
         call compute_padding2(page_size,total_recorded,remaining_byte)
         read(12) (l1,i=1,remaining_byte)
c     Padding
         do iq=1,Nq(iband)
            do inode=1,Nnode
               read(12) ka(inode)
               if (ka(inode).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'iband=',iband,' inode=',inode
                  write(*,*) 'ka=',ka(inode)
                  write(*,*) 'shoud be positive'
                  stop
               endif
               if (isnan(dble(ka(inode)))) then
                  call error(label)
                  write(*,*) 'iband=',iband,' inode=',inode
                  write(*,*) 'ka=',ka(inode)
                  stop
               endif
c     progress display
               ndone=ndone+1
               fdone=dble(ndone)/dble(ntot)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no') floor(fdone),' %'
                  pifdone=ifdone
               endif
c     progress display
            enddo               ! inode
c     Debug
c            if ((iband.eq.1).and.(iq.eq.1)) then
c               write(*,*) 'kext_max=',ka(1)+ks(1)
c            endif
c     Debug
c     Check that every (lon/lat/alt) box is homogeneous
            icell=0
            do ilayer=1,Nlayer
               do itheta=1,Ntheta
c     Number of cells in a single box
                  if ((itheta.eq.1).or.(itheta.eq.Ntheta)) then
                     Ncell_sb=3
                  else
                     Ncell_sb=6
                  endif
c     Number of nodes in a single box
                  Nnode_sb=Ncell_sb*Nvincell
                  do iphi=1,Nphi
                     call box2inode(Nlayer,Ntheta,Nphi,ilayer,itheta,iphi,1,1,inode)
                     ka_ref=ka(inode)
                     do cell_idx=1,Ncell_sb
                        icell=icell+1
                        do inodeincell=1,Nvincell
                           call box2inode(Nlayer,Ntheta,Nphi,ilayer,itheta,iphi,cell_idx,inodeincell,inode)
c     Checking "index1" for every cell against "inode"
c                           if (inode.ne.index1(icell,inodeincell)) then
c                              call error(label)
c                              write(*,*) 'ilayer=',ilayer,' itheta=',itheta,' iphi=',iphi
c                              write(*,*) 'cell index:',cell_idx
c                              write(*,*) 'node in cell index:',inodeincell
c                              write(*,*) 'index1(',icell,',',inodeincell,')=',index1(icell,inodeincell)
c                              write(*,*) 'while inode=',inode
c                              stop
c                           endif
c     Checking ka and ks against reference values
                           if (ka(index1(icell,inodeincell)).ne.ka_ref) then
                              call error(label)
                              write(*,*) 'Box was found non homogeneous in absorption:'
                              write(*,*) 'ilayer=',ilayer,' itheta=',itheta,' iphi=',iphi
                              write(*,*) 'ka_ref=',ka_ref
                              write(*,*) 'cell index:',cell_idx
                              write(*,*) 'node in cell index:',inodeincell
                              write(*,*) 'ka(',index1(icell,inodeincell),')=',ka(index1(icell,inodeincell))
                              stop
                           endif
                        enddo   ! inodeincell
                     enddo      ! cell_idx
                     if (iq.eq.1) then
                        call box2inode(Nlayer,Ntheta,Nphi,ilayer,itheta,iphi,1,1,inode)
                        ks_ref=ks(inode)
c     Debug
                        if ((iband.eq.1).and.(itheta.eq.1).and.(iphi.eq.1)) then
                           write(*,*) 'ilayer=',ilayer,' inode=',inode,' ka_ref=',ka_ref,' ks_ref=',ks_ref
                        endif
c     Debug
                        do cell_idx=1,Ncell_sb
                           do inodeincell=1,Nvincell
                              call box2inode(Nlayer,Ntheta,Nphi,ilayer,itheta,iphi,cell_idx,inodeincell,inode)
                              if (ks(inode).ne.ks_ref) then
                                 call error(label)
                                 write(*,*) 'Box was found non homogeneous in scattering:'
                                 write(*,*) 'ilayer=',ilayer,' itheta=',itheta,' iphi=',iphi
                                 write(*,*) 'ks_ref=',ks_ref
                                 write(*,*) 'cell index:',cell_idx
                                 write(*,*) 'node in cell index:',inodeincell
                                 write(*,*) 'ks(',inode,')=',ks(inode)
                                 stop
                              endif
                           enddo ! inodeincell
                        enddo   ! cell_idx
                     endif      ! iq=1
c     
                  enddo         ! iphi
               enddo            ! itheta
            enddo               ! ilayer
c     Padding
            total_recorded=Nnode*size_of_real
            call compute_padding2(page_size,total_recorded,remaining_byte)
            read(12) (l1,i=1,remaining_byte)
c     Padding
         enddo                  ! iq
      enddo                     ! iband
      close(12)

c     Debug
      write(*,*)
      write(*,*) 'Every box seems homogeneous !'
c     Debug
      
      end
