#!/bin/bash

# Specify the list of file to be copied
LIST="sphere.bin
materials_list.txt
surface_properties.bin
mtl_*.dat
gas_grid.bin
gas_radiative_properties.bin
gas_thermodynamic_properties.bin
haze_grid.bin
haze_radiative_properties.bin
haze_phase.bin
*_phase_function_list.txt
*_phase_function_*.dat"

# Specify the name of the archive to be produced
DIR='./scene'
archive=$DIR'.tgz'

if [ -f "$DIR" ]; then
    rm -rf $DIR
fi
mkdir $DIR
if [ -f "$archive" ]; then
    rm -f $archive
fi

for file in $LIST
do
    if [ ! -f "$file" ]; then
	echo "File not found: "$file
	exit 1
    fi
    echo "Copying: "$file
    mv $file $DIR
done
tar -zcvf $archive $DIR
rm -rf $DIR
echo "Archive file: "$archive

exit 0
