c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine record_poly_file(dim,Ncontour,Npoints,points,
     &     poly_file,outfile)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce a .poly file that will be used by the "triangle" program
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Npoints: number of points that define each contour
c       + points: list of points that define each contour
c       + poly_file: name of the poly file to produce, without the ".poly" extension
c     
c     Output:
c       + outfile: name of the file that was recorded
c     
c     I/O
      integer dim
      integer Ncontour
      integer Npoints(1:Ncontour_mx)
      double precision points(1:Ncontour_mx,1:Nppt_mx,1:Ndim_mx)
      character*(Nchar_mx) poly_file
      character*(Nchar_mx) outfile
c     temp
      integer ic,i,j,Nsegments,i1,i2,ip,is,ip0,Nholes,ih
      double precision xmin,xmax,ymin,ymax
      double precision xh,yh
      integer Np
      double precision contour(1:Nppt_mx,1:Ndim_mx)
      double precision x_inside(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine record_poly_file'

c     Debug
c      write(*,*) 'This is :',trim(label)
c      write(*,*) 'Ncontour=',Ncontour
c      do ic=1,Ncontour
c         write(*,*) 'Npoints(',ic,')=',Npoints(ic)
c      enddo                     ! ic
c     Debug
      
      if (poly_file(len_trim(poly_file)-4:len_trim(poly_file))
     &     .eq.'.poly') then
         outfile=trim(poly_file)
      else
         outfile=trim(poly_file)//'.poly'
      endif

      Np=0
      do ic=1,Ncontour
         Np=Np+Npoints(ic)
      enddo                     ! ic
      
      open(11,file=trim(outfile))
      write(11,*) Np,2,0,1
      ip=0
      do ic=1,Ncontour
         do i=1,Npoints(ic)
            ip=ip+1
            write(11,*) ip,(points(ic,i,j),j=1,2),ic+1
         enddo                  ! i
      enddo                     ! ic
      write(11,*)
      Nsegments=0
      do ic=1,Ncontour
         Nsegments=Nsegments+Npoints(ic)
      enddo                     ! ic
      write(11,*) Nsegments,1
      is=0
      ip0=0
      do ic=1,Ncontour
         do i=1,Npoints(ic)
            is=is+1
            i1=i
            if (i.eq.Npoints(ic)) then
               i2=1
            else
               i2=i+1
            endif
            write(11,*) is,ip0+i1,ip0+i2,ic+1
         enddo                  ! i
         ip0=ip0+Npoints(ic)
      enddo                     ! ic
      write(11,*)
      Nholes=Ncontour-1
      if (Nholes.lt.0) then
         call error(label)
         write(*,*) 'Nholes=',Nholes
         write(*,*) '< 0'
         stop
      endif                     ! Nholes <0
      write(11,*) Nholes
      if (Nholes.gt.0) then
         do ih=1,Nholes
            ic=ih+1
            xmin=points(ic,1,1)
            xmax=points(ic,1,1)
            ymin=points(ic,1,2)
            ymax=points(ic,1,2)
            do i=1,Npoints(ic)
               if (points(ic,i,1).lt.xmin) then
                  xmin=points(ic,i,1)
               endif
               if (points(ic,i,1).gt.xmax) then
                  xmax=points(ic,i,1)
               endif
               if (points(ic,i,2).lt.ymin) then
                  ymin=points(ic,i,2)
               endif
               if (points(ic,i,2).gt.ymax) then
                  ymax=points(ic,i,2)
               endif
            enddo               ! ic
            xh=(xmin+xmax)/2.0D+0
            yh=(ymin+ymax)/2.0D+0
c     write(11,*) ih,xh,yh
            Np=Npoints(ic)
            do i=1,Np
               do j=1,dim
                  contour(i,j)=points(ic,i,j)
               enddo            ! j
            enddo               ! i
            call point_in_contour2(dim,Np,contour,x_inside)
            write(11,*) ih,(x_inside(j),j=1,2)
         enddo                  ! ih
      endif                     ! Nholes > 0
      close(11)

      return
      end


      
      subroutine read_result_files(poly_file,dim,Nv01,v01,Nf01,f01)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read "triangle" output files
c     
c     Input:
c       + poly_file: name of the poly file to produce, without the ".poly" extension
c       + dim: dimension of space
c     
c     Output:
c       + Nv01: number of vertices
c       + v01: vertices
c       + Nf01: number of triangular faces that have been generated
c       + f01: index of the 3 vertices that define each face
c     
c     I/O
      character*(Nchar_mx) poly_file
      integer dim
      integer Npoints
      double precision points(1:Nppt_mx,1:Ndim_mx)
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      character*(Nchar_mx) filename
      character*(Nchar_mx) res_file
      integer ios,iv,iface,index,j,Nd,Nppf
      double precision a,b
c     label
      character*(Nchar_mx) label
      label='subroutine read_result_files'

      if (poly_file(len_trim(poly_file)-4:len_trim(poly_file))
     &     .eq.'.poly') then
         filename=poly_file(1:len_trim(poly_file)-5)
      else
         filename=trim(poly_file)
      endif
      
      res_file=trim(filename)//'.1.node'
      open(11,file=trim(res_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(res_file)
         stop
      else
c         write(*,*) 'reading: ',trim(res_file)
         read(11,*) Nv01,Nd
         if (Nv01.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv01=',Nv01
            write(*,*) '> Nv_so_mx=',Nv_so_mx
            stop
         endif
         if (Nd.ne.dim-1) then
            call error(label)
            write(*,*) 'Nd=',Nd
            write(*,*) 'should be equal to: ',dim-1
            write(*,*) 'dim=',dim
            stop
         endif
         do iv=1,Nv01
            read(11,*) index,(v01(iv,j),j=1,Nd)
            v01(iv,dim)=0.0D+0
         enddo                  ! iv
      endif                     ! ios
      close(11)

      res_file=trim(filename)//'.1.ele'
      open(12,file=trim(res_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(res_file)
         stop
      else
c         write(*,*) 'reading: ',trim(res_file)
         read(12,*) Nf01,Nppf
         if (Nf01.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nf01=',Nf01
            write(*,*) '> Nf_so_mx=',Nf_so_mx
            stop
         endif
         if (Nppf.ne.3) then
            call error(label)
            write(*,*) 'Number of points per face=',Nppf
            write(*,*) 'should be equal to 3'
            stop
         endif
         do iface=1,Nf01
            read(12,*) index,(f01(iface,j),j=1,3)
         enddo                  ! iface
      endif                     ! ios
      close(12)

      return
      end



      subroutine point_in_contour(dim,Np,contour,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find a position inside a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of positions that define the contour
c       + contour: positions that define the contour
c     
c     Output:
c       + x: position inside the closed contour
c     
c     I/O
      integer dim
      integer Np
      double precision contour(1:Nppt_mx,1:Ndim_mx)
      double precision x(1:Ndim_mx)
c     temp
      integer i,j,int
      double precision ymin,ymax,yav
      logical x_found(1:2)
      double precision xpos(1:2),x1,x2,y1,y2
c     label
      character*(Nchar_mx) label
      label='subroutine point_in_contour'

      ymin=contour(1,2)
      ymax=contour(1,2)
      do i=1,Np
         if (contour(i,2).lt.ymin) then
            ymin=contour(i,2)
         endif
         if (contour(i,2).gt.ymax) then
            ymax=contour(i,2)
         endif
      enddo                     ! i
      yav=(ymin+ymax)/2.0D+0

      x_found(1)=.false.
      x_found(2)=.false.

      int=0
      do i=1,Np
         x1=contour(i,1)
         y1=contour(i,2)
         if (i.eq.Np) then
            x2=contour(1,1)
            y2=contour(1,2)
         else
            x2=contour(i+1,1)
            y2=contour(i+1,2)
         endif
         if (((y1.le.yav).and.(y2.ge.yav)).or.
     &        ((y1.ge.yav).and.(y2.le.yav))) then
            int=int+1
            x_found(int)=.true.
            xpos(int)=x1+(x2-x1)*(yav-y1)/(y2-y1)
         endif
         if ((x_found(1)).and.(x_found(2))) then
            goto 111
         endif
      enddo                     ! i
 111  continue
      if ((.not.x_found(1)).or.(.not.x_found(2))) then
         call error(label)
         write(*,*) 'x_found(1)=',x_found(1)
         write(*,*) 'x_found(2)=',x_found(2)
         stop
      else
         x(1)=(xpos(1)+xpos(2))/2.0D+0
         x(2)=yav
      endif

      return
      end




      subroutine point_in_contour2(dim,Np,contour,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find a position inside a closed contour
c     New method, based on sampling the bounding box of the contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of positions that define the contour
c       + contour: positions that define the contour
c     
c     Output:
c       + x: position inside the closed contour
c     
c     I/O
      integer dim
      integer Np
      double precision contour(1:Nppt_mx,1:Ndim_mx)
      double precision x(1:Ndim_mx)
c     temp
      integer i,j,Ntry,itry
      double precision xmin,xmax,ymin,ymax
      integer Ncontour_tmp
      integer Nppc_tmp(1:Ncontour_mx)
      double precision contour_tmp(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical inside,keep_searching,debug,is_inside,position_found
c     label
      character*(Nchar_mx) label
      label='subroutine point_in_contour2'

      xmin=contour(1,1)
      xmax=contour(1,1)
      ymin=contour(1,2)
      ymax=contour(1,2)
      do i=1,Np
         if (contour(i,1).lt.xmin) then
            xmin=contour(i,1)
         endif
         if (contour(i,1).gt.xmax) then
            xmax=contour(i,1)
         endif
         if (contour(i,2).lt.ymin) then
            ymin=contour(i,2)
         endif
         if (contour(i,2).gt.ymax) then
            ymax=contour(i,2)
         endif
      enddo                     ! i

      Ncontour_tmp=1
      Nppc_tmp(1)=Np
      do i=1,Np
         do j=1,dim-1
            contour_tmp(1,i,j)=contour(i,j)
         enddo                  ! j
      enddo                     ! i
      Ntry=10000
      position_found=.false.
      keep_searching=.true.
      debug=.false.
      itry=0
      do while (keep_searching)
         itry=itry+1
         call uniform(xmin,xmax,x(1))
         call uniform(ymin,ymax,x(2))
         x(3)=0.0D+0
         call is_inside_contour(debug,dim,
     &        Ncontour_tmp,Nppc_tmp,contour_tmp,
     &        1,x,is_inside)
         if (is_inside) then
            position_found=.true.
            keep_searching=.false.
         endif
         if (itry.gt.Ntry) then
            keep_searching=.false.
            call error(label)
            write(*,*) 'Could not find position inside contour'
            stop
         endif
      enddo                     ! while (keep_searching)
         
      return
      end
