      subroutine fill_gas_grid(dim,
     &     haze_zone_file,
     &     CH4_ck_file,CO_ck_file,CH4_abundance_file,CO_abundance_file,
     &     Nlambda,lambda,Nq,g,w,
     &     Nnode,xnode,
     &     T_z_interpolation_mode,T_z_extrapolation_mode,T_set_value,
     &     k_z_interpolation_mode,k_z_extrapolation_mode,k_z_set_value,
     &     gas_radiative_properties_file,gas_thermodynamic_properties_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'param.inc'
c     
c     Purpose: to fill a gas tetrahedric grid with optical properties
c     
c     Input:
c       + dim: dimension of space
c       + haze_zone_file: zone definition file for the haze
c       + CH4_ck_file: file containing the raw CK data for CH4
c       + CO_ck_file: file containing the raw CK data for CO
c       + CH4_abundance_file: file containing the raw molar abundance data for CH4
c       + CO_abundance_file: file containing the raw molar abundance data for CO
c       + Nlambda: number of values for the wavelength if reference=F
c       + lambda: wavelength array [nm] if reference=F
c       + Nq: quadrature order if reference=F
c       + g: quadrature abscissas if reference=F
c       + w: quadrature weights if reference=F
c       + Nnode: number of grid nodes
c       + xnode: coordinate of each node (longitude 0,360 ; latitude -90,90 ; altitude 0,+inf) [deg, deg, m]
c       + T_z_interpolation_mode: interpolation mode for the temperature over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + T_z_extrapolation_mode: extrapolation mode for the temperature over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "T_set_value"
c       + T_set_value: value of T to use for T_z_extrapolation_mode='set'
c       + k_z_interpolation_mode: interpolation mode for CK coefficients over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + k_z_extrapolation_mode: extrapolation mode for CK coefficients over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "k_set_value"
c       + k_z_set_value: value of CK coefficient to use for extrapolation_mode='set'
c       + Nzone: number of latitude zones
c       + zone_limit: lower and higer latitude for each latitude zone [deg]
c       + zone_profile: index of the altitude profile to use for each latitude zone
c       + gas_radiative_properties_file: binary file containing the optical properties of the gas mixture (ka & ks)
c       + gas_thermodynamic_properties_file: gas thermodynamic file (mainly: the temperature)
c     
c     I/O
      integer dim
      character*(Nchar_mx) haze_zone_file
      character*(Nchar_mx) CH4_ck_file
      character*(Nchar_mx) CO_ck_file
      character*(Nchar_mx) CH4_abundance_file
      character*(Nchar_mx) CO_abundance_file
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      integer Nq
      double precision w(1:Nq_mx)
      double precision g(1:Nq_mx)
      integer*8 Nnode
      double precision xnode(1:Nnode_mx,1:Ndim_mx)
      character*(Nchar_mx) T_z_interpolation_mode
      character*(Nchar_mx) T_z_extrapolation_mode
      double precision T_set_value
      character*(Nchar_mx) k_z_interpolation_mode
      character*(Nchar_mx) k_z_extrapolation_mode
      double precision k_z_set_value
      character*(Nchar_mx) gas_radiative_properties_file
      character*(Nchar_mx) gas_thermodynamic_properties_file
c     temp
      logical reference
      integer Nzone_haze
      double precision zone_haze_theta(1:Nzone_mx,1:2)
      double precision zone_haze_phi(1:Nzone_mx,1:2)
      double precision zone_haze_lambda(0:Nzone_mx)
      integer zone_haze_Nlev(0:Nzone_mx)
      double precision zone_haze_pressure(0:Nzone_mx,1:Nlev_mx)
      double precision zone_haze_altitude(0:Nzone_mx,1:Nlev_mx)
      double precision zone_haze_temperature(0:Nzone_mx,1:Nlev_mx)
      double precision zone_haze_kext(0:Nzone_mx,1:Nlev_mx)
      integer CH4_Nlev,CO_Nlev
      integer CH4_Nprofile,CO_Nprofile
      double precision CH4_pressure(1:Nlev_mx)
      double precision CH4_temperature(1:Nprofile_mx,1:Nlev_mx)
      double precision CO_pressure(1:Nlev_mx)
      double precision CO_temperature(1:Nprofile_mx,1:Nlev_mx)
      integer ilambda,iq,i,j
      integer iprofile,ilev,inode
      double precision z,theta,phi,P,k,rho
      integer haze_zone
      double precision zone_pressure(1:Nlev_mx)
      double precision zone_altitude(1:Nlev_mx)
      double precision zone_temperature(1:Nlev_mx)
      double precision zone_k(1:Nlev_mx)
      double precision p1,z1,T1,k1,p2,z2,T2,k2
      double precision C_CH4,C_CO,k_CH4,k_CO,raf
      double precision rho_CH4,rho_CO
      double precision xCH4,xCO
      integer Nlev_ref
      double precision pressure_ref(1:Nlev_mx)
      double precision altitude_ref(1:Nlev_mx)
      double precision temperature_ref(1:Nlev_mx)
      double precision kext_ref(1:Nlev_mx)
      integer Nband,iband
      double precision delta_lambda,lambda_ref
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
      character*(Nchar_mx) command
      logical CH4_data_found(1:Nlambda_mx)
      integer CH4_lambda_idx(1:Nlambda_mx)
      logical CO_data_found(1:Nlambda_mx)
      integer CO_lambda_idx(1:Nlambda_mx)
      double precision ck_CH4(1:Nprofile_mx,1:Nlev_mx)
      double precision ck_CO(1:Nprofile_mx,1:Nlev_mx)
      logical is_nan
      integer Nlev_CH4
      double precision pressure_CH4(1:Nlev_mx)
      double precision x_CH4(1:Nlev_mx)
      integer Nlev_CO
      double precision pressure_CO(1:Nlev_mx)
      double precision x_CO(1:Nlev_mx)
      double precision altitude_CH4(1:Nlev_mx)
      double precision temperature_CH4(1:Nlev_mx)
      double precision altitude_CO(1:Nlev_mx)
      double precision temperature_CO(1:Nlev_mx)
      double precision ka(1:Nnode_mx,1:Nq_mx)
      double precision ks(1:Nnode_mx)
      double precision T(1:Nnode_mx)
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     parameters
      parameter(delta_lambda=8.0D+0) ! band width (arbitrary, but has to be really small) [nm]
c     label
      character*(Nchar_mx) label
      label='subroutine fill_gas_grid'
      
      write(*,*) 'Interpolation and recording of CK for gas mixture (go grab a cup of tea)'
c     ----------------------------------------------------------------
c     Gather raw data
c     ----------------------------------------------------------------
c     kext by zone: needed to get the haze zones information
      call read_kext_haze(haze_zone_file,
     &     Nzone_haze,zone_haze_theta,zone_haze_phi,
     &     zone_haze_lambda,zone_haze_Nlev,
     &     zone_haze_pressure,zone_haze_altitude,
     &     zone_haze_temperature,zone_haze_kext)
c     analyze the presence of spectral data in the required file
      reference=.true.          ! use "Nlambda", "lambda", "Nq", "g" and "w" of CH4 as a reference
      call analyze_spectral_structure(CH4_ck_file,reference,
     &     CH4_Nlev,CH4_Nprofile,
     &     Nlambda,lambda,Nq,g,w,
     &     CH4_data_found,CH4_lambda_idx)
      reference=.false.         ! values of "Nlambda", "lambda", "Nq", "g" and "w" of CH4 will be used
      call analyze_spectral_structure(CO_ck_file,reference,
     &     CO_Nlev,CO_Nprofile,
     &     Nlambda,lambda,Nq,g,w,
     &     CO_data_found,CO_lambda_idx)
c     read CH4 molar abundance
      call read_abundance_file(CH4_abundance_file,Nlev_CH4,pressure_CH4,x_CH4)
c     read CO molar abundance
      call read_abundance_file(CO_abundance_file,Nlev_CO,pressure_CO,x_CO)
c     spectral limits of each narrow spectral interval (arbitrary)
      Nband=Nlambda
      do iband=1,Nband
         lambda_min(iband)=lambda(iband)-delta_lambda/2.0D+0
         lambda_max(iband)=lambda(iband)+delta_lambda/2.0D+0
         if (iband.gt.1) then
            if (lambda_max(iband-1).gt.lambda(iband)) then
               lambda_max(iband-1)=(lambda(iband-1)+lambda(iband))/2.0D+0
            endif
            if (lambda_min(iband).lt.lambda_max(iband-1)) then
               lambda_min(iband)=lambda_max(iband-1)
            endif
         endif                  ! iband > 1
      enddo                     ! iband
c     remove previous gas_radiative_properties_file
      command='rm -f '//trim(gas_radiative_properties_file)
      call exec(command)
c     progress display ---
      ntot=Nnode*int(Nband,kind(ntot))*int(Nq,kind(ntot))
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     --- progress display
c      
c     ----------------------------------------------------------------
c     Fill grid nodes and generate corresponding files
c     ----------------------------------------------------------------
      do ilambda=1,Nlambda
c     record the scattering coefficient of the gaz for the current spectral interval (/node)
         do inode=1,Nnode
            ks(inode)=0.0D+0
         enddo                  ! inode
         do iq=1,Nq
c     read CK LUT for current (ilambda,iq) quadrature point
            call read_ck(CH4_ck_file,
     &           CH4_Nlev,CH4_Nprofile,Nlambda,Nq,
     &           ilambda,iq,
     &           CH4_data_found,CH4_lambda_idx,
     &           CH4_pressure,CH4_temperature,ck_CH4)
            call read_ck(CO_ck_file,
     &           CO_Nlev,CO_Nprofile,Nlambda,Nq,
     &           ilambda,iq,
     &           CO_data_found,CO_lambda_idx,
     &           CO_pressure,CO_temperature,ck_CO)
c     
            do inode=1,Nnode
               phi=xnode(inode,1) ! longitude (0,360) [deg]
               theta=xnode(inode,2) ! latitude (-90,90) [deg]
               z=xnode(inode,3) ! altitude [m]
c     Identify the zone position (theta,phi) belongs to
               call identify_zone(Nzone_haze,
     &              zone_haze_theta,
     &              zone_haze_phi,
     &              theta,phi,
     &              haze_zone)
c     "ref": reference data, used to retrieve the altitude and temperature of the current node
               call get_kext_profile(haze_zone,Nzone_haze,
     &              zone_haze_lambda,zone_haze_Nlev,
     &              zone_haze_pressure,zone_haze_altitude,
     &              zone_haze_temperature,zone_haze_kext,
     &              lambda_ref,Nlev_ref,pressure_ref,altitude_ref,temperature_ref,kext_ref)
               call identify_vertical_interval(Nlev_ref,
     &              pressure_ref,altitude_ref,temperature_ref,kext_ref,z,
     &              p1,z1,T1,k1,p2,z2,T2,k2)
c     interpolate P and T from reference data
c     pressure
               call z2p(Nlev_ref,pressure_ref,altitude_ref,
     &              temperature_ref,z,
     &              P,raf)
c     temperature
               call altitude_interpolation(Nlev_ref,
     &              altitude_ref,z1,p1,T1,z2,p2,T2,z,P,
     &              T_z_interpolation_mode,T_z_extrapolation_mode,
     &              T_set_value,
     &              T(inode))
               if (T(inode).le.0.0D+0) then
                  call error(label)
                  write(*,*) 'T(',inode,')=',T(inode)
                  write(*,*) 'should be positive'
                  stop
               endif
c     molecular density
               rho=Nav*P/(Rpg*T(inode)) ! [molec/m³]
c     interpolate the current CK-coefficient in the (P,T) LUT
               call PT_k_interpolation(CH4_Nlev,CH4_Nprofile,
     &              CH4_pressure,CH4_temperature,
     &              Nlambda,lambda,Nq,w,ck_CH4,
     &              k_z_interpolation_mode,k_z_extrapolation_mode,k_z_set_value,
     &              ilambda,iq,
     &              Nlev_ref,pressure_ref,altitude_ref,
     &              z,P,T(inode),
     &              C_CH4)      ! C_CH4=[cm²/molec of CH4]
c     inter/extra-polate the molar fraction of CH4
               call interpolate_molar_fraction(Nlev_CH4,pressure_CH4,x_CH4,P,xCH4) ! xCH4=[mol of CH4/mol of mixture]
c     value of k for CH4
               rho_CH4=rho*xCH4 ! [molec of CH4/m³]
               k_CH4=1.0D-4*C_CH4*rho_CH4 ! [m⁻¹]
c     
               call test_nan(k_CH4,is_nan)
               if (is_nan) then
                  call error(label)
                  write(*,*) 'k_CH4=',k_CH4
                  write(*,*) 'lambda=',lambda(ilambda)
                  write(*,*) 'iq=',iq
                  write(*,*) 'inode=',inode
                  write(*,*) 'C_CH4=',C_CH4
                  stop
               endif
               call PT_k_interpolation(CO_Nlev,CO_Nprofile,
     &              CO_pressure,CO_temperature,
     &              Nlambda,lambda,Nq,w,ck_CO,
     &              k_z_interpolation_mode,k_z_extrapolation_mode,k_z_set_value,
     &              ilambda,iq,
     &              Nlev_ref,pressure_ref,altitude_ref,
     &              z,P,T(inode),
     &              C_CO)       ! C_CO=[cm²/molec of CO]
c     inter/extra-polate the molar fraction of CO
               call interpolate_molar_fraction(Nlev_CO,pressure_CO,x_CO,P,xCO) ! xCO=[mol of CO/mol of mixture]
c     value of k for CO
               rho_CO=rho*xCO   ! [molec of CO/m³]
               k_CO=1.0D-4*C_CO*rho_CO ! [m⁻¹]
c     
               call test_nan(k_CO,is_nan)
               if (is_nan) then
                  call error(label)
                  write(*,*) 'k_CO=',k_CO
                  write(*,*) 'lambda=',lambda(ilambda)
                  write(*,*) 'iq=',iq
                  write(*,*) 'inode=',inode
                  write(*,*) 'C_CO=',C_CO
                  stop
               endif
c     Final value of k
               k=k_CH4+k_CO     ! [m⁻¹]
c     record the absorption coefficient, for the current spectral interval and quadrature point (/node)
               ka(inode,iq)=k
c     progress display ---
               ndone=ndone+1
               fdone=dble(ndone)/dble(ntot)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no') floor(fdone),' %'
                  pifdone=ifdone
               endif
c     --- progress display
            enddo               ! inode
         enddo                  ! iq
         call record_gas_radiative_properties_file(Nnode,Nband,Nq,
     &        lambda_min,lambda_max,w,ka,ks,ilambda,gas_radiative_properties_file)
      enddo                     ! ilambda
c     -------------------------------------------------------------------------------------------
      call record_gas_thermodynamic_properties_file(Nnode,T,gas_thermodynamic_properties_file)
      write(*,*)
      write(*,*) 'Files have been recorded:'
      write(*,*) trim(gas_radiative_properties_file)
      write(*,*) trim(gas_thermodynamic_properties_file)

 666  continue
      return
      end



      subroutine PT_k_interpolation(Nlev,Nprofile,
     &     pressure,temperature,
     &     Nlambda,lambda,Nq,w,ck,
     &     interpolation_mode,extrapolation_mode,set_value,
     &     ilambda,iq,
     &     Nlev_ref,pressure_ref,altitude_ref,
     &     z,P,T,
     &     k)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to interpolate / extrapolate a value of k (for a given lambda / quadrature point)
c     from the CK tabulation as a function of pressure and temperature
c     
c     Input:
c       + Nlev: number of vertical levels (identical for every profile)
c       + Nprofile: number of profiles
c       + pressure: pressure array, identical for every profile [Pa]
c       + temperature: temperature array, depends on the profile [K]
c       + Nlambda: number of values for the wavelength if reference=T
c       + lambda: wavelength array [nm] if reference=T
c       + Nq: quadrature order if reference=T
c       + w: quadrature weights if reference=T
c       + ck: CK corss-section LUT for the current quadrature point [cm²/molec]
c       + interpolation_mode: interpolation mode for CK coefficients over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + extrapolation_mode: extrapolation mode for CK coefficients over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "k_set_value"
c       + set_value: value of k to use when extrapolation_mode=set
c       + ilambda: index of the wavelength
c       + iq: quadrature index
c       + Nlev_ref: number of reference levels
c       + pressure_ref: reference pressure array [Pa]
c       + altitude_ref: reference altitude array [m]
c       + z: value of the altitude for the current node [m]
c       + P: value of the pressure for the current node [Pa]
c       + T: value of the temperature for the current node [Pa]
c     
c     Output:
c       + k: the interpolated value of the cross-section, for the required lambda and quadrature point, for a given node [cm²/molec]
c     
c     I/O
      integer Nlev
      integer Nprofile
      double precision pressure(1:Nlev_mx)
      double precision temperature(1:Nprofile_mx,1:Nlev_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      integer Nq
      double precision w(1:Nq_mx)
      double precision ck(1:Nprofile_mx,1:Nlev_mx)
      character*(Nchar_mx) interpolation_mode
      character*(Nchar_mx) extrapolation_mode
      double precision set_value
      integer ilambda
      integer iq
      integer Nlev_ref
      double precision pressure_ref(1:Nlev_mx)
      double precision altitude_ref(1:Nlev_mx)
      double precision z
      double precision P
      double precision T
      double precision k
c     temp
      integer iprofile,ilev,i
      double precision T1(1:Nprofile_mx)
      double precision k1(1:Nprofile_mx)
      double precision altitude(1:Nlev_mx)
      double precision tmp1(1:Nlev_mx),raf
      logical interval_found
      integer interval,pinterval
      double precision p1,p2,z1,z2
      double precision Tmin,Tmax
c     label
      character*(Nchar_mx) label
      label='subroutine PT_k_interpolation'

c     ---------------------------------------------------------------------------------
c     First, interpolate CK as a function of pressure
c     ---------------------------------------------------------------------------------
      if (P.gt.pressure(1)) then
         interval_found=.true.
         interval=1
      else if (P.lt.pressure(Nlev)) then
         interval_found=.true.
         interval=Nlev-1
      else
         interval_found=.false.         
         do i=1,Nlev-1
            if ((P.le.pressure(i)).and.(P.ge.pressure(i+1))) then
               interval_found=.true.
               interval=i
               goto 111
            endif
         enddo                  ! i
 111     continue
         if (.not.interval_found) then
            call error(label)
            write(*,*) 'Could not identify interval for P=',P
            write(*,*) 'Interval / Pmax / Pmin'
            do i=1,Nlev-1
               write(*,*) i,pressure(i),pressure(i+1)
            enddo               ! i
            stop
         endif
      endif
      pinterval=interval
      p1=pressure(interval)
      call p2z(Nlev_ref,pressure_ref,altitude_ref,
     &     tmp1,p1,z1,raf)
      p2=pressure(interval+1)
      call p2z(Nlev_ref,pressure_ref,altitude_ref,
     &     tmp1,p2,z2,raf)
c
c     In all cases: temperature is linearly interpolated as a function of altitude
      do iprofile=1,Nprofile
         T1(iprofile)=temperature(iprofile,interval)
     &        +(temperature(iprofile,interval+1)
     &        -temperature(iprofile,interval))
     &        *(z-z1)
     &        /(z2-z1)
      enddo                     ! iprofile
c
      if ((P.le.pressure(1)).and.(P.ge.pressure(Nlev))) then
c     interpolation
         if (trim(interpolation_mode).eq.
     &        'pressure_linear') then
            do iprofile=1,Nprofile
               k1(iprofile)=ck(iprofile,interval)
     &              +(ck(iprofile,interval+1)
     &              -ck(iprofile,interval))
     &              *(p-p1)
     &              /(p2-p1)
            enddo               ! iprofile
         else if (trim(interpolation_mode).eq.
     &           'altitude_linear') then
            do iprofile=1,Nprofile
               k1(iprofile)=ck(iprofile,interval)
     &              +(ck(iprofile,interval+1)
     &              -ck(iprofile,interval))
     &              *(z-z1)
     &              /(z2-z1)
            enddo               ! iprofile
         else if (trim(interpolation_mode).eq.
     &           'bottom_constant') then
            do iprofile=1,Nprofile
               k1(iprofile)=ck(iprofile,interval)
            enddo               ! iprofile
         else if (trim(interpolation_mode).eq.
     &           'top_constant') then
            do iprofile=1,Nprofile
               k1(iprofile)=ck(iprofile,interval+1)
            enddo               ! iprofile
         else if (trim(interpolation_mode).eq.
     &           'avg_constant') then
            do iprofile=1,Nprofile
               k1(iprofile)=(ck(iprofile,interval)
     &              +ck(iprofile,interval+1))/2.0D+0
            enddo               ! iprofile
         else
            call error(label)
            write(*,*) 'Incorrect value for:'
            write(*,*) 'interpolation_mode=',
     &           trim(interpolation_mode)
            stop
         endif
      else                      ! P is out of "pressure" boundaries
c     extrapolation
         if (trim(extrapolation_mode).eq.
     &        'pressure_linear') then
            do iprofile=1,Nprofile
               k1(iprofile)=ck(iprofile,interval)
     &              +(ck(iprofile,interval+1)
     &              -ck(iprofile,interval))
     &              *(p-p1)
     &              /(p2-p1)
            enddo               ! iprofile
         else if (trim(extrapolation_mode).eq.
     &           'altitude_linear') then
            do iprofile=1,Nprofile
               k1(iprofile)=ck(iprofile,interval)
     &              +(ck(iprofile,interval+1)
     &              -ck(iprofile,interval))
     &              *(z-z1)
     &              /(z2-z1)
            enddo               ! iprofile
         else if (trim(extrapolation_mode).eq.
     &           'constant') then
            if (P.gt.pressure(1)) then
               do iprofile=1,Nprofile
                  k1(iprofile)=ck(iprofile,interval)
               enddo            ! iprofile
            else if (P.lt.pressure(Nlev)) then
               do iprofile=1,Nprofile
                  k1(iprofile)=ck(iprofile,interval+1)
               enddo            ! iprofile
            endif
         else if (trim(extrapolation_mode).eq.
     &           'set') then
            do iprofile=1,Nprofile
               k1(iprofile)=set_value
            enddo               ! iprofile
         else
            call error(label)
            write(*,*) 'Incorrect value for:'
            write(*,*) 'extrapolation_mode=',
     &           trim(extrapolation_mode)
            stop
         endif
c     additionnal issue: k may now be <0 using "altitude_linear"
         do iprofile=1,Nprofile
            if (k1(iprofile).lt.0.0D+0) then
               call warning(label)
               write(*,*) 'extrapolation_mode induces'
     &              //' negative values'
               goto 112
            endif
         enddo                  ! iprofile
 112     continue
      endif                     ! P in or out of "pressure" boundaries
c
c     ---------------------------------------------------------------------------------
c     Then interpolate as a function of temperature "T1": linear interpolation
c     ---------------------------------------------------------------------------------
      interval_found=.false.
      do iprofile=1,Nprofile-1
         Tmin=dmin1(T1(iprofile),T1(iprofile+1))
         Tmax=dmax1(T1(iprofile),T1(iprofile+1))
         if ((T.ge.Tmin).and.(T.le.Tmax)) then
            interval_found=.true.
            interval=iprofile
            goto 221
         endif
      enddo                     ! iprofile
 221  continue
      if (.not.interval_found) then
         if (dabs(T-T1(1)).lt.dabs(T-T1(Nprofile))) then
            interval_found=.true.
            interval=1
         else
            interval_found=.true.
            interval=Nprofile-1
         endif
      endif                     ! interval not found
      if (.not.interval_found) then
         call error(label)
         write(*,*) 'Interval not found for T=',T
         write(*,*) 'iprofile / T1'
         do iprofile=1,Nprofile
            write(*,*) iprofile,T1(iprofile)
         enddo                  ! iprofile
         stop
      endif
c     Interpolation
      k=k1(interval)+(k1(interval+1)-k1(interval))
     &     *(T-T1(interval))/(T1(interval+1)-T1(interval))
      if (k.lt.0.0D+0) then
c         call warning(label)
c         write(*,*) 'Temperature interpolation induces '
c     &        //'negative values'
c         write(*,*) 'T(',interval,')=',T1(interval),' k=',k1(interval)
c         write(*,*) 'T(',interval+1,')=',T1(interval+1),
c     &        ' k=',k1(interval+1)
c         write(*,*) 'T=',T,' k=',k
c         write(*,*) 'iprofile / T1'
c         do iprofile=1,Nprofile
c            write(*,*) iprofile,T1(iprofile)
c         enddo                  ! iprofile
c         write(*,*) 'P=',P
c         write(*,*) 'pinterval=',pinterval
c         write(*,*) 'p1=',p1
c         write(*,*) 'p2=',p2
         k=0.0D+0
      endif

      return
      end
