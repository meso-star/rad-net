      subroutine record_cloud_radiative_properties_file(Nnode,Nband,
     &     lambda_min,lambda_max,ka,ks,band_idx,cloud_radiative_properties_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record the cloud's radiative properties binary file.
c     Due to the huge amount of data the file is supposed to contain, this
c     routine must be called for every spectral interval (band). The
c     data arrays (ka and ks) have to be updated for the current band.
c     Triggers for opening/closing the binary file are respectively
c     band_idx=1 and band_idx=Nband, so the "band_idx" input has to
c     be updated accordingly between calls.
c     
c     Input:
c       + Nnode: number of nodes
c       + Nband: number of spectral intervals
c       + lambda_min: lower wavelength for each interval [nm]
c       + lambda_max: higher wavelength for each interval [nm]
c       + ka: absorption coefficient of every node for the current band [m⁻¹]
c       + ks: scattering coefficient of every node for the current band [m⁻¹]
c       + band_idx: band index
c       + cloud_radiative_properties_file: file to record
c     
c     Output: the required binary file
c     
c     I/O
      integer*8 Nnode
      integer Nband
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision ka(1:Nnode_mx)
      double precision ks(1:Nnode_mx)
      integer band_idx
      character*(Nchar_mx) cloud_radiative_properties_file
c     temp
      logical open_file,close_file
      integer inode,iband,i
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
c     label
      character*(Nchar_mx) label
      label='subroutine record_cloud_radiative_properties_file'

      if (band_idx.eq.1) then
         open_file=.true.
      else
         open_file=.false.
      endif
      if (band_idx.eq.Nband) then
         close_file=.true.
      else
         close_file=.false.
      endif

      if (open_file) then
c     If binary file has to be opened, then write the preamble
         open(33,file=trim(cloud_radiative_properties_file),form='unformatted',access='stream')
         write(33) int(pagesize,kind(Nnode))
         write(33) int(Nband,kind(Nnode))
         write(33) int(Nnode,kind(Nnode))
         do iband=1,Nband
            write(33) lambda_min(iband),lambda_max(iband)
         enddo                  ! iband
c     Padding file 33 ---
         total_recorded=3*size_of_int8+2*Nband*size_of_double
         call compute_padding2(pagesize,total_recorded,remaining_byte)
         write(33) (l1,i=1,remaining_byte)
c     --- Padding file 33
      endif                     ! open_file
c     Then record all data for the current band
      do inode=1,Nnode
         write(33) real(ka(inode)),real(ks(inode))
      enddo                     ! inode
c     Padding file 33 ---
      total_recorded=2*Nnode*size_of_real
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(33) (l1,i=1,remaining_byte)
c     --- Padding file 33
      if (close_file) then
         close(33)
      endif

      return
      end



      subroutine record_phase_function_definition_file(prefix,
     &     phase_function_index,phase_function_type,Nlambda_HG,lambda_HG,g_HG,
     &     phase_Nlambda,phase_lambda,Nangle,phase_angle,pf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to record a phase function definition file (ascii)
c     
c     Input:
c       + prefix: character string that constitutes the basis of the phase function file names
c       + phase_function_index: index of the phase function definition file
c       + phase_function_type: 0 for Henyey-Greenstein, 1 for discretized phase function
c       + Nlambda_HG: number of values of the wavelength for a HG function
c       + lambda_HG: values of the wavelength for a HG function [nm]
c       + g_HG: associated values of the asymetry parameter
c       + phase_Nlambda: number of values of the wavelength for a discretized function
c       + phase_lambda: values of the wavelength for a discretized function [nm]
c       + Nangle: number of values of the scattering angle for a discretized function
c       + phase_angle: values of the scattering angle for a discretized function [rad]
c       + pf: values of the normalized discretized phase function [sr⁻¹]
c     
c     Output: the required phase function definition file
c     
c     I/O
      character*(Nchar_mx) prefix
      integer phase_function_index
      integer phase_function_type
      integer Nlambda_HG
      double precision lambda_HG(1:Nlambda_mx)
      double precision g_HG(1:Nlambda_mx)
      integer phase_Nlambda
      double precision phase_lambda(1:Nlambda_mx)
      integer Nangle
      double precision phase_angle(1:Nangle_mx)
      double precision pf(1:Nlambda_mx,1:Nangle_mx)
c     temp
      logical err_code
      character*(Nchar_mx) pf_str,filename,phase_function_definition_file
      integer iangle,ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine record_phase_function_definition_file'

c     Produce file name
      call phase_function_file_name(prefix,phase_function_index,filename)
      phase_function_definition_file='./results/'//trim(filename)
c     Record file
      open(23,file=trim(phase_function_definition_file))
      if (phase_function_type.eq.0) then ! HG
         write(23,*) 'wavelengths',Nlambda_HG
         do ilambda=1,Nlambda_HG
            write(23,*) lambda_HG(ilambda),'HG',g_HG(ilambda)
         enddo                  ! ilambda
      else if (phase_function_type.eq.1) then ! discretized
         write(23,*) 'wavelengths',phase_Nlambda
         do ilambda=1,phase_Nlambda
            write(23,*) phase_lambda(ilambda),'discrete',Nangle
            do iangle=1,Nangle
               write(23,*) phase_angle(iangle),pf(ilambda,iangle)
            enddo               ! iangle
c     Debug
c            if (ilambda.eq.1) then
c               write(*,*) 'Nangle=',Nangle
c               do iangle=1,Nangle
c                  write(*,*) phase_angle(iangle),dcos(phase_angle(iangle)),pf(ilambda,iangle)
c               enddo            ! iangle
c            endif
c     Debug
         enddo                  ! ilambda
      endif                     ! phase_function_type
      close(23)
      write(*,*) 'File has been recorded: ',trim(phase_function_definition_file)

      return
      end



      subroutine record_phase_function_list_file(prefix,N,phase_function_list_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record the phase function list file
c     
c     Input:
c       + prefix: character string that constitutes the basis of the phase function file names
c       + N: number of phase function definition files
c       + phase_function_list_file: file to record (ascii)
c     
c     Output: the required ascii file
c     
c     I/O
      character*(Nchar_mx) prefix
      integer N
      character*(Nchar_mx) phase_function_list_file
c     temp
      integer i
      logical err_code
      character*(Nchar_mx) pf_str,filename
c     label
      character*(Nchar_mx) label
      label='subroutine record_phase_function_list_file'

      open(24,file=trim(phase_function_list_file))
      write(24,*) N             ! number of phase function definition files
      do i=1,N
         call phase_function_file_name(prefix,i,filename)
         write(24,10) trim(filename) ! list of phase function definition files
      enddo                     ! i
      close(24)
      write(*,*) 'File has been recorded: ',trim(phase_function_list_file)

      return
      end



      subroutine phase_function_file_name(prefix,index,filename)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce a phase function file name
c     
c     Input:
c       + prefix: character string that constitutes the basis of the phase function file names
c       + index: phase function index
c     
c     Output:
c       + filename: phase function file name
c     
c     I/O
      character*(Nchar_mx) prefix
      integer index
      character*(Nchar_mx) filename
c     temp
      character*(Nchar_mx) pf_str
      logical err_code
c     label
      character*(Nchar_mx) label

      if (index.lt.10000) then
         call num2str4(index,pf_str,err_code)
      else
         call error(label)
         write(*,*) 'index=',index
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      filename=trim(prefix)//'_phase_function_'//trim(pf_str)//'.dat'

      return
      end



      subroutine record_phase_function_file(Nnode,phase_function_index,phase_function_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record the phase function (binary) file for a aerosol mode
c     
c     Input:
c       + Nnode: number of nodes
c       + phase_function_index: index of the definition file for each node
c       + phase_function_file: phase function file to record
c     
c     Output: the required binary file
c     
c     I/O
      integer*8 Nnode
      integer phase_function_index(1:Nnode_mx)
      character*(Nchar_mx) phase_function_file
c     temp
      integer inode,i
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
      integer*8 record_size
      integer*8 alignment
c     label
      character*(Nchar_mx) label
      label='subroutine record_phase_function_file'

      record_size=int(size_of_real,kind(record_size))
      alignment=record_size
      
      open(12,file=trim(phase_function_file),form='unformatted',access='stream')
      write(12) int(pagesize,kind(Nnode))
      write(12) int(Nnode,kind(Nnode))
      write(12) record_size
      write(12) alignment
c     Padding file 12 ---
      total_recorded=4*size_of_int8
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(12) (l1,i=1,remaining_byte)
c     --- Padding file 12
      do inode=1,Nnode
         write(12) phase_function_index(inode)-1 ! HTRDR requires indexes starting from 0
      enddo                     ! inode
c     Padding file 12 ---
      total_recorded=Nnode*size_of_int4
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(12) (l1,i=1,remaining_byte)
c     --- Padding file 12
      close(12)

      return
      end
