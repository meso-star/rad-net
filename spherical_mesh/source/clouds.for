      subroutine fill_cloud_grid(dim,haze_zone_file,haze_ssa_file,
     &     haze_phase_file,Nnode,xnode,
     &     Nband,lambda_min,lambda_max,
     &     kext_z_interpolation_mode,
     &     kext_z_extrapolation_mode,
     &     kext_set_value,
     &     T_z_interpolation_mode,
     &     T_z_extrapolation_mode,
     &     T_set_value,
     &     ssa_z_interpolation_mode,
     &     ssa_z_extrapolation_mode,
     &     ssa_set_Nlambda,ssa_set_lambda,ssa_set_value,
     &     ssa_lambda_interpolation_mode,
     &     ssa_lambda_extrapolation_mode,
     &     prefix,
     &     phase_function_list_file,
     &     cloud_radiative_properties_file,
     &     phase_function_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: to fill a cloud tetrahedric grid with optical properties
c     
c     Input:
c       + dim: dimension of space
c       + haze_zone_file: zone definition file for the haze
c       + haze_ssa_file: haze single-scattering albedo file
c       + haze_phase_file: haze phase function file
c       + Nnode: number of grid nodes
c       + xnode: coordinate of each node (longitude 0,360 ; latitude -90,90 ; altitude 0,+inf) [deg, deg, m]
c       + Nband: number of narrowbands
c       + lambda_min: lower wavelength for each band [nm]
c       + lambda_max: higher wavelength for each band [nm]
c       + kext_z_interpolation_mode: interpolation mode for the extinction coefficient over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + kext_z_extrapolation_mode: extrapolation mode for the extinction coefficient over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "kext_set_value"
c       + kext_set_value: value of kext to use for kext_z_extrapolation_mode='set'
c       + T_z_interpolation_mode: interpolation mode for the temperature over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + T_z_extrapolation_mode: extrapolation mode for the temperature over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "T_set_value"
c       + T_set_value: value of T to use for T_z_extrapolation_mode='set'
c       + ssa_z_interpolation_mode: interpolation mode for the single-scattering albedo over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + ssa_z_extrapolation_mode: extrapolation mode for the single-scattering albedo over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input values of "ssa_set_value"
c       + ssa_set_Nlambda: size of the "ssa_set_lambda" and "ssa_set_value" arrays
c       + ssa_set_lambda: array of wavelength for the definition of "ssa_set_value" [nm]
c       + ssa_set_value: array of singe-scattering albedo to use for ssa_z_extrapolation_mode='set'
c       + ssa_lambda_interpolation_mode: interpolation mode for the single-scattering albedo over the spectral dimension
c         - linear: linear with wavelength
c         - low_constant: constant by interval (value of the lower limit)
c         - high_constant: constant by interval (value of the higher limit)
c         - avg_constant: constant by interval (average of the values for the lower and higher limits)
c       + ssa_lambda_extrapolation_mode: extrapolation mode for the single-scattering albedo over the spectral dimension
c         - linear: linear with wavelength
c         - constant: constant to the low/high limit
c       + prefix: character string that constitutes the basis of the phase function file names
c       + phase_function_list_file: file that holds the list of phase function definition files
c       + cloud_radiative_properties_file: binary file containing the optical properties of the haze (ka and ks)
c       + phase_function_file: binary file containing the phase function of the haze
c     
c     Output:
c       + the required "cloud_radiative_properties_file" file
c     
c     I/O
      integer dim
      character*(Nchar_mx) haze_zone_file
      character*(Nchar_mx) haze_ssa_file
      character*(Nchar_mx) haze_phase_file
      integer*8 Nnode
      double precision xnode(1:Nnode_mx,1:Ndim_mx)
      integer Nband
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      character*(Nchar_mx) kext_z_interpolation_mode
      character*(Nchar_mx) kext_z_extrapolation_mode
      double precision kext_set_value
      character*(Nchar_mx) T_z_interpolation_mode
      character*(Nchar_mx) T_z_extrapolation_mode
      double precision T_set_value
      character*(Nchar_mx) ssa_z_interpolation_mode
      character*(Nchar_mx) ssa_z_extrapolation_mode
      integer ssa_set_Nlambda
      double precision ssa_set_lambda(1:Nlambda_mx)
      double precision ssa_set_value(1:Nlambda_mx)
      character*(Nchar_mx) ssa_lambda_interpolation_mode
      character*(Nchar_mx) ssa_lambda_extrapolation_mode
      character*(Nchar_mx) prefix
      character*(Nchar_mx) phase_function_list_file
      character*(Nchar_mx) cloud_radiative_properties_file
      character*(Nchar_mx) phase_function_file
c     temp
      integer Nzone
      double precision zone_theta(1:Nzone_mx,1:2)
      double precision zone_phi(1:Nzone_mx,1:2)
      double precision zone_lambda(0:Nzone_mx)
      integer zone_Nlev(0:Nzone_mx)
      double precision zone_altitude(0:Nzone_mx,1:Nlev_mx)
      double precision zone_pressure(0:Nzone_mx,1:Nlev_mx)
      double precision zone_temperature(0:Nzone_mx,1:Nlev_mx)
      double precision zone_kext(0:Nzone_mx,1:Nlev_mx)
      integer ssa_Nlev
      integer ssa_Nlambda
      double precision ssa_altitude(1:Nlev_mx)
      double precision ssa_lambda(1:Nlambda_mx)
      double precision ssa(1:Nlev_mx,1:Nlambda_mx)
      integer phase_Nlambda
      integer Nangle
      double precision phase_lambda(1:Nlambda_mx)
      double precision phase_angle(1:Nangle_mx)
      double precision pf(1:Nlambda_mx,1:Nangle_mx)
      integer inode,izone,i,j,ilev
      integer ilambda,iangle
      double precision theta,phi,z
      double precision lambda
      integer Nlev
      double precision altitude(1:Nlev_mx)
      double precision pressure(1:Nlev_mx)
      double precision temperature(1:Nlev_mx)
      double precision kext(1:Nlev_mx)
      double precision p1,z1,T1,k1,p2,z2,T2,k2
      double precision ssa1,ssa2
      double precision single_scattering_albedo
      double precision p,k,T,raf
      double precision ss_ssa(1:Nlambda_mx)
      double precision mono_ssa(1:Nlev_mx)
      double precision set_value
      double precision p_ssa(1:Nlev_mx)
      double precision T_ssa(1:Nlev_mx)
      double precision ss_pf(1:Nlambda_mx)
      double precision phase_function(1:Nangle_mx)
      integer iband
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
      character*(Nchar_mx) phase_function_definition_file
      integer phase_function_type,phase_function_index
      integer Nlambda_HG
      double precision lambda_HG(1:Nlambda_mx)
      double precision g_HG(1:Nlambda_mx)
      integer phase_function_idx(1:Nnode_mx)
      logical err_code
      character*(Nchar_mx) pf_str,filename
      double precision ka(1:Nnode_mx)
      double precision ks(1:Nnode_mx)
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine fill_cloud_grid'

      write(*,*) 'Generating haze data...'
c     ----------------------------------------------------------------
c     Gather raw data
c     ----------------------------------------------------------------
c     kext by zone
      call read_kext_haze(haze_zone_file,
     &     Nzone,zone_theta,zone_phi,zone_lambda,zone_Nlev,
     &     zone_pressure,zone_altitude,zone_temperature,zone_kext)
c     single-scattering albedo
      call read_ssa_file(haze_ssa_file,
     &     ssa_Nlev,ssa_Nlambda,ssa_altitude,ssa_lambda,ssa)
      
      phase_function_type=1     ! 0: HG; 1: discrete
      if (phase_function_type.eq.0) then
c     dummy data
         Nlambda_HG=2
         lambda_HG(1)=2.0D+3    ! nm
         lambda_HG(2)=5.0D+3    ! nm
         do ilambda=1,Nlambda_HG
            g_HG(ilambda)=0.0D+0
         enddo                  ! ilambda
      else if (phase_function_type.eq.1) then
c     phase function
         call read_phase_file(haze_phase_file,
     &        phase_Nlambda,Nangle,phase_lambda,phase_angle,pf)
      else
         call error(label)
         write(*,*) 'phase_function_type=',phase_function_type
         write(*,*) 'allowed values: 0, 1'
         stop
      endif
c     record the phase function into its definition file
c     for the moment, there is only one discretized phase function
c     but if more than one discretized phase function has to be defined,
c     each one must be recorded into its own definition file
      phase_function_index=1
      call record_phase_function_definition_file(prefix,
     &     phase_function_index,phase_function_type,Nlambda_HG,lambda_HG,g_HG,
     &     phase_Nlambda,phase_lambda,Nangle,phase_angle,pf)
c     add this file to the list of phase function definition files
      call record_phase_function_list_file(prefix,1,phase_function_list_file)
c     binary file that defines the phase function at each node of the grid
      do inode=1,Nnode
         phase_function_idx(inode)=1 ! index of the phase function definition file for each node
      enddo                     ! inode
      call record_phase_function_file(Nnode,phase_function_idx,phase_function_file)
c     
c     progress display
      ntot=Nnode*int(Nband,kind(ntot))
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display
c     ----------------------------------------------------------------
c     Fill grid nodes and generate corresponding files
c     ----------------------------------------------------------------
      do iband=1,Nband
         do inode=1,Nnode
            phi=xnode(inode,1)  ! longitude (0,360) [deg]
            theta=xnode(inode,2) ! latitude (-90,90) [deg]
            z=xnode(inode,3)    ! altitude [m]
c     Identify the zone where position (theta,phi) belongs: izone=0,Nzone
            call identify_zone(Nzone,zone_theta,zone_phi,theta,phi,
     &           izone)
c     Extract the kext profile for the zone
            call get_kext_profile(izone,Nzone,
     &           zone_lambda,zone_Nlev,
     &           zone_pressure,zone_altitude,
     &           zone_temperature,zone_kext,
     &           lambda,Nlev,pressure,altitude,temperature,kext)
            call identify_vertical_interval(Nlev,
     &           pressure,altitude,temperature,kext,z,
     &           p1,z1,T1,k1,p2,z2,T2,k2)
            call z2p(Nlev,pressure,altitude,temperature,z,p,raf)
c     
c     ***************************************************************************************
c     Interpolation/extrapolation of kext for the current value of z -> "k"
c     ***************************************************************************************
            call altitude_interpolation(Nlev,
     &           altitude,z1,p1,k1,z2,p2,k2,z,p,
     &           kext_z_interpolation_mode,
     &           kext_z_extrapolation_mode,
     &           kext_set_value,
     &           k)
c     note: k is provided for a single wavelength (specified in the profile data file,
c     retrieved as variable "lambda"). Since this is the only available spectral data,
c     the value obtained for "k" at this wavelength will be recorded in the cloud_radiative_properties_file,
c     no matter the spectral limits of the current narrowband.
            if (k.lt.0.0D+0) then
               call error(label)
               write(*,*) 'k=',k
               write(*,*) 'should not be negative'
               stop
            endif
c     
c     ***************************************************************************************
c     Interpolation/extrapolation of T for the current value of z -> "T"
c     ***************************************************************************************
            call altitude_interpolation(Nlev,
     &           altitude,z1,p1,T1,z2,p2,T2,z,p,
     &           T_z_interpolation_mode,T_z_extrapolation_mode,
     &           T_set_value,
     &           T)
            if (T.le.0.0D+0) then
               call error(label)
               write(*,*) 'T=',T
               write(*,*) 'should be positive'
               stop
            endif
c     
c     ***************************************************************************************
c     Interpolation/extrapolation of single-scattering albedo -> "single_scattering_albedo"
c     ***************************************************************************************
c     production of pressure and temperature for each level of "ssa_altitude"
            do ilev=1,ssa_Nlev
               call z2p(Nlev,pressure,altitude,temperature,
     &              ssa_altitude(ilev),
     &              p_ssa(ilev),T_ssa(ilev))
            enddo               ! ilev
            do ilambda=1,ssa_Nlambda
c     set_value: interpolated from the "ssa_set_value" array for lambda=ssa_lambda(ilambda)
               call wavelength_interpolation(ssa_set_Nlambda,
     &              ssa_set_lambda,ssa_set_value,ssa_lambda(ilambda),
     &              ssa_lambda_interpolation_mode,
     &              ssa_lambda_extrapolation_mode,
     &              set_value)
c     extraction of single-scattering albedo for the current spectral point
               do ilev=1,ssa_Nlev
                  mono_ssa(ilev)=ssa(ilev,ilambda)
               enddo            ! i
c     identify interval for current value of z
               call identify_vertical_interval(ssa_Nlev,
     &              p_ssa,ssa_altitude,T_ssa,mono_ssa,z,
     &              p1,z1,T1,ssa1,p2,z2,T2,ssa2)
               call altitude_interpolation(ssa_Nlev,
     &              ssa_altitude,z1,p1,ssa1,z2,p2,ssa2,z,p,
     &              ssa_z_interpolation_mode,
     &              ssa_z_extrapolation_mode,
     &              set_value,
     &              ss_ssa(ilambda))
            enddo               ! ilambda
c     Inter/extra-polate "ssa" for the current node and the current
c     wevelength for which "k" is provided [lambda] -> "single_scattering_albedo"
            call wavelength_interpolation(ssa_Nlambda,
     &           ssa_lambda,ss_ssa,lambda,
     &           ssa_lambda_interpolation_mode,
     &           ssa_lambda_extrapolation_mode,
     &           single_scattering_albedo)
c     
            if ((single_scattering_albedo.lt.0.0D+0).or.
     &           (single_scattering_albedo.gt.1.0D+0)) then
               call error(label)
               write(*,*) 'single_scattering_albedo=',
     &              single_scattering_albedo
               write(*,*) 'should be in the [0,1] range'
               stop
            endif
            ka(inode)=k*(1.0D+0-single_scattering_albedo)
            ks(inode)=k*single_scattering_albedo
c     
c     progress display
            ndone=ndone+1
            fdone=dble(ndone)/dble(ntot)*1.0D+2
            ifdone=floor(fdone)
            if (ifdone.gt.pifdone) then
               do j=1,len+2
                  write(*,"(a)",advance='no') "\b"
               enddo            ! j
               write(*,trim(fmt),advance='no') floor(fdone),' %'
               pifdone=ifdone
            endif
c     progress display
         enddo                  ! inode
         call record_cloud_radiative_properties_file(Nnode,Nband,
     &        lambda_min,lambda_max,ka,ks,iband,cloud_radiative_properties_file)
      enddo                     ! iband
c     
      write(*,*)
      write(*,*) 'Files have been recorded:'
      write(*,*) trim(cloud_radiative_properties_file)
      write(*,*) trim(phase_function_file)
      
      return
      end



      subroutine altitude_interpolation(Nlev,
     &     altitude_array,z1,p1,dt1,z2,p2,dt2,z,p,
     &     interpolation_mode,extrapolation_mode,
     &     set_value,
     &     output_data)
      implicit none
      include 'max.inc'
c     
c     Purpose: to interpolate over the vertical
c     
c     Input:
c       + Nlev: number of vertical levels
c       + altitude_array: altitude data [m]
c       + z1: altitude of the first level [m]
c       + p1: pressure of the first level [Pa]
c       + dt1: data of the first level
c       + z2: altitude of the second level [m]
c       + p2: pressure of the second level [Pa]
c       + dt2: data of the second level
c       + z: interpolation altitude [m]
c       + z: interpolation pressure [Pa]
c       + interpolation_mode: interpolation mode over the vertical
c         - linear: linear with wavelength
c         - low_constant: constant by interval (value of the lower limit)
c         - high_constant: constant by interval (value of the higher limit)
c         - avg_constant: constant by interval (average of the values for the lower and higher limits)
c       + extrapolation_mode: extrapolation mode over the spectral dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the provided "set_value"
c       + set_value: value to use when extrapolation_mode='set'
c     
c     Output:
c       + output_data: data, inter/extra-polated for the value of "z"
c     
c     I/O
      integer Nlev
      double precision altitude_array(1:Nlev_mx)
      double precision z1,p1,dt1
      double precision z2,p2,dt2
      double precision z,p
      character*(Nchar_mx) interpolation_mode
      character*(Nchar_mx) extrapolation_mode
      double precision set_value
      double precision output_data
c     temp
c     label
      character*(Nchar_mx) label
      label='altitude_interpolation'

      if ((z.ge.altitude_array(1)).and.(z.le.altitude_array(Nlev))) then
c     interpolation
         if (trim(interpolation_mode).eq.
     &        'pressure_linear') then
            output_data=dt1+(dt2-dt1)*(p-p1)/(p2-p1)
         else if (trim(interpolation_mode).eq.
     &           'altitude_linear') then
            output_data=dt1+(dt2-dt1)*(z-z1)/(z2-z1)
         else if (trim(interpolation_mode).eq.
     &           'bottom_constant') then
            output_data=dt1
         else if (trim(interpolation_mode).eq.
     &           'top_constant') then
            output_data=dt2
         else if (trim(interpolation_mode).eq.
     &           'avg_constant') then
            output_data=(dt1+dt2)/2.0D+0
         else
            call error(label)
            write(*,*) 'Incorrect value for:'
            write(*,*) 'interpolation_mode=',
     &           trim(interpolation_mode)
            stop
         endif
c     Debug
         if (output_data.lt.0.0D+0) then
            call warning(label)
            write(*,*) 'interpolation_mode induces'
     &           //' negative values'
            write(*,*) 'interpolation_mode=',trim(interpolation_mode)
            write(*,*)
         endif
c     Debug
      else
c     extrapolation
         if (trim(extrapolation_mode).eq.
     &        'pressure_linear') then
            output_data=dt1+(dt2-dt1)*(p-p1)/(p2-p1)
         else if (trim(extrapolation_mode).eq.
     &           'altitude_linear') then
            output_data=dt1+(dt2-dt1)*(z-z1)/(z2-z1)
         else if (trim(extrapolation_mode).eq.
     &           'constant') then
            if (z.lt.altitude_array(1)) then
               output_data=dt1
            else if (z.gt.altitude_array(Nlev)) then
               output_data=dt2
            else
               call error(label)
               write(*,*) 'z=',z
               write(*,*) '> altitude_array(1)=',altitude_array(1)
               write(*,*) 'and < altitude_array(',Nlev,')=',
     &              altitude_array(Nlev)
               write(*,*) 'should not occur here'
               stop
            endif
         else if (trim(extrapolation_mode).eq.
     &           'set') then
            output_data=set_value
         else
            call error(label)
            write(*,*) 'Incorrect value for:'
            write(*,*) 'extrapolation_mode=',
     &           trim(extrapolation_mode)
            stop
         endif
c     additionnal issue: k may now be <0 using "altitude_linear"
         if (output_data.lt.0.0D+0) then
            call warning(label)
            write(*,*) 'extrapolation_mode induces'
     &           //' negative values'
         endif
      endif                     ! z in or out altitude limits

      return
      end      
      


      subroutine wavelength_interpolation(Nlambda,
     &     lambda_array,data_array,lambda,
     &     interpolation_mode,extrapolation_mode,
     &     interpolated_data)
      implicit none
      include 'max.inc'
c     
c     Purpose: to interpolate spectral data
c     
c     Input:
c       + Nlambda: number of wavelength points
c       + lambda_array: wavelength data [nm]
c       + data_array: monochromatic data
c       + lambda: interpolation wavelength [nm]
c       + interpolation_mode: interpolation mode for the single-scattering albedo over the spectral dimension
c         - linear: linear with wavelength
c         - low_constant: constant by interval (value of the lower limit)
c         - high_constant: constant by interval (value of the higher limit)
c         - avg_constant: constant by interval (average of the values for the lower and higher limits)
c       + extrapolation_mode: extrapolation mode for the single-scattering albedo over the spectral dimension
c         - linear: linear with wavelength
c         - constant: constant to the low/high limit
c     
c     Output:
c       + interpolated_data: data, interpolated for the value of "lambda"
c     
c     I/O
      integer Nlambda
      double precision lambda_array(1:Nlambda_mx)
      double precision data_array(1:Nlambda_mx)
      double precision lambda
      character*(Nchar_mx) interpolation_mode
      character*(Nchar_mx) extrapolation_mode
      double precision interpolated_data
c     temp
      logical interval_found
      integer i,interval
c     label
      character*(Nchar_mx) label
      label='wavelength_interpolation'

      if (Nlambda.gt.Nlambda_mx) then
         call error(label)
         write(*,*) 'Nlambda=',Nlambda
         write(*,*) '> Nlambda_mx=',Nlambda_mx
         stop
      endif
      
      if (lambda.lt.lambda_array(1)) then
         interval_found=.true.
         interval=1
      else if (lambda.gt.lambda_array(Nlambda)) then
         interval_found=.true.
         interval=Nlambda-1
      else
         interval_found=.false.
         do i=1,Nlambda-1
            if ((lambda.ge.lambda_array(i)).and.
     &           (lambda.le.lambda_array(i+1))) then
               interval_found=.true.
               interval=i
               goto 111
            endif
         enddo                  ! i
      endif
 111  continue
      if (.not.interval_found) then
         call error(label)
         write(*,*) 'Interval could not be found for lambda=',lambda
         do i=1,Nlambda
            write(*,*) 'lambda_array(',i,')=',lambda_array(i)
         enddo                  ! i
         stop
      endif

      if ((lambda.ge.lambda_array(1)).and.
     &     (lambda.le.lambda_array(Nlambda))) then
c     Interpolation
         if (trim(interpolation_mode).eq.'linear') then
            interpolated_data=data_array(interval)+
     &           (data_array(interval+1)-data_array(interval))
     &           *(lambda-lambda_array(interval))
     &           /(lambda_array(interval+1)-lambda_array(interval))
         else if (trim(interpolation_mode).eq.'low_constant') then
            interpolated_data=data_array(interval)
         else if (trim(interpolation_mode).eq.'high_constant') then
            interpolated_data=data_array(interval+1)
         else if (trim(interpolation_mode).eq.'avg_constant') then
            interpolated_data=(data_array(interval)
     &           +data_array(interval+1))/2.0D+0
         else
            call error(label)
            write(*,*) 'Incorrect value for:'
            write(*,*) 'interpolation_mode=',trim(interpolation_mode)
            stop
         endif
      else
c     Extrapolation
         if (trim(extrapolation_mode).eq.'linear') then
            interpolated_data=data_array(interval)+
     &           (data_array(interval+1)-data_array(interval))
     &           *(lambda-lambda_array(interval))
     &           /(lambda_array(interval+1)-lambda_array(interval))
         else if (trim(extrapolation_mode).eq.'constant') then
            if (lambda.lt.lambda_array(1)) then
               interpolated_data=data_array(1)
            else if (lambda.gt.lambda_array(Nlambda)) then
               interpolated_data=data_array(Nlambda)
            else
               call error(label)
               write(*,*) 'lambda=',lambda
               write(*,*) '> lambda_array(1)=',lambda_array(1)
               write(*,*) 'and < lambda_array(',Nlambda,')=',
     &              lambda_array(Nlambda)
               write(*,*) 'should not occur here'
            endif
         else
            call error(label)
            write(*,*) 'Incorrect value for:'
            write(*,*) 'extrapolation_mode=',trim(extrapolation_mode)
            stop
         endif
      endif
      
      return
      end

      
      
      subroutine identify_vertical_interval(Nlev_ref,
     &     pressure_ref,altitude_ref,temperature_ref,kext_ref,z,
     &     p1,z1,T1,k1,p2,z2,T2,k2)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to identify the altitude interval
c     
c     Input:
c       + Nlev_ref: number of levels in the reference profile
c       + pressure_ref: reference pressure profile [Pa]
c       + altitude_ref: reference altitude profile [m]
c       + temperature_ref: reference temperature profile [K]
c       + kext: value of kext @ nu for each level, sorted by increasing altitude [inv. m]
c       + z: arbitrary altitude level [m]
c     
c     Output:
c       + p1: pressure for level 1 [Pa]
c       + z1: altitude for level 1 [m]
c       + T1: temperature for level 1 [K]
c       + k1: kext for level 1 [inv. m]
c       + p2: pressure for level 2 [Pa]
c       + z2: altitude for level 2 [m]
c       + T2: temperature for level 2 [K]
c       + k2: kext for level 2 [inv. m]
c     
c     I/O
      integer Nlev_ref
      double precision pressure_ref(1:Nlev_mx)
      double precision altitude_ref(1:Nlev_mx)
      double precision temperature_ref(1:Nlev_mx)
      double precision kext_ref(1:Nlev_mx)
      double precision z
      double precision p1,z1,T1,k1
      double precision p2,z2,T2,k2
c     temp
      logical interval_found
      integer int,i
c     label
      character*(Nchar_mx) label
      label='subroutine identify_vertical_interval'

      if (Nlev_ref.gt.Nlev_mx) then
         call error(label)
         write(*,*) 'Nlev=ref=',Nlev_ref
         write(*,*) '> Nlev_mx=',Nlev_mx
         stop
      endif
      if (z.lt.altitude_ref(1)) then
         interval_found=.true.
         int=1
         goto 111
      else if (z.gt.altitude_ref(Nlev_ref)) then
         interval_found=.true.
         int=Nlev_ref-1
         goto 111
      else
         interval_found=.false.
         do i=1,Nlev_ref-1
            if ((z.ge.altitude_ref(i)).and.
     &           (z.le.altitude_ref(i+1))) then
               interval_found=.true.
               int=i
               goto 111
            endif
         enddo                  ! i
      endif
 111  continue
      if (.not.interval_found) then
         call error(label)
         write(*,*) 'Altitude interval not found for'
         write(*,*) 'z=',z
         do i=1,Nlev_ref
            write(*,*) 'altitude_ref(',i,')=',altitude_ref(i),' m'
         enddo                  ! i
         stop
      endif
c      
      p1=pressure_ref(int)
      z1=altitude_ref(int)
      T1=temperature_ref(int)
      k1=kext_ref(int)
      if (p1.le.0.0D+0) then
         call error(label)
         write(*,*) 'p1=',p1
         write(*,*) 'is < 0'
         stop
      endif
      if (T1.le.0.0D+0) then
         call error(label)
         write(*,*) 'T1=',T1
         write(*,*) 'is < 0'
         stop
      endif
      if (k1.le.0.0D+0) then
         call error(label)
         write(*,*) 'k1=',k1
         write(*,*) 'is < 0'
         stop
      endif
c      
      p2=pressure_ref(int+1)
      z2=altitude_ref(int+1)
      T2=temperature_ref(int+1)
      k2=kext_ref(int+1)
      if (p2.le.0.0D+0) then
         call error(label)
         write(*,*) 'p2=',p2
         write(*,*) 'is < 0'
         stop
      endif
      if (T2.le.0.0D+0) then
         call error(label)
         write(*,*) 'T2=',T2
         write(*,*) 'is < 0'
         stop
      endif
      if (k2.le.0.0D+0) then
         call error(label)
         write(*,*) 'k2=',k2
         write(*,*) 'is < 0'
         stop
      endif
c
      return
      end



      subroutine read_kext_haze(file_in,
     &     Nzone,zone_theta,zone_phi,zone_lambda,zone_Nlev,
     &     zone_pressure,zone_altitude,zone_temperature,zone_kext)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read a zone definition file
c     
c     Input:
c       + file_in: input zone definition file
c     
c     Output:
c       + Nzone: number of zones
c       + zone_theta: latitude limits for each zone [deg]
c       + zone_phi: longitude limits for each zone [deg]
c       + zone_lambda: wavelenght the extinction coefficient of the zone is defined for [nm]
c       + zone_Nlev: number of atmospheric levels for the zone
c       + zone_pressure: pressure data for the zone [Pa]
c       + zone_altitude: altitude data for the zone [m]
c       + zone_temperature: temperature data for the zone [K]
c       + zone_kext: haze extinction coefficient data for the zone [inv. m]
c     
c     I/O
      character*(Nchar_mx) file_in
      integer Nzone
      double precision zone_theta(1:Nzone_mx,1:2)
      double precision zone_phi(1:Nzone_mx,1:2)
      double precision zone_lambda(0:Nzone_mx)
      integer zone_Nlev(0:Nzone_mx)
      double precision zone_altitude(0:Nzone_mx,1:Nlev_mx)
      double precision zone_pressure(0:Nzone_mx,1:Nlev_mx)
      double precision zone_temperature(0:Nzone_mx,1:Nlev_mx)
      double precision zone_kext(0:Nzone_mx,1:Nlev_mx)
c     temp
      character*(Nchar_mx) zone_file(1:Nzone_mx)
      character*(Nchar_mx) default_file
      double precision nu
      integer Nlev
      double precision altitude(1:Nlev_mx)
      double precision pressure(1:Nlev_mx)
      double precision temperature(1:Nlev_mx)
      double precision kext(1:Nlev_mx)
      integer i,izone
c     label
      character*(Nchar_mx) label
      label='subroutine read_kext_haze'

      call read_zones(file_in,Nzone,zone_theta,zone_phi,zone_file,
     &     default_file)
c     Index 0: default profile
      call read_kext_file(default_file,
     &     nu,Nlev,altitude,pressure,temperature,kext)
      zone_lambda(0)=1.0D+7/nu       ! [nm]
      zone_Nlev(0)=Nlev
      do i=1,Nlev
         zone_altitude(0,i)=altitude(i)
         zone_pressure(0,i)=pressure(i)
         zone_temperature(0,i)=temperature(i)
         zone_kext(0,i)=kext(i)
      enddo                     ! i
c     Index 1-Nzone: profile for each zone
      do izone=1,Nzone
         call read_kext_file(zone_file(izone),
     &        nu,Nlev,altitude,pressure,temperature,kext)
         zone_lambda(izone)=1.0D+7/nu    ! [nm]
         zone_Nlev(izone)=Nlev
         do i=1,Nlev
            zone_altitude(izone,i)=altitude(i)
            zone_pressure(izone,i)=pressure(i)
            zone_temperature(izone,i)=temperature(i)
            zone_kext(izone,i)=kext(i)
         enddo                  ! i
      enddo                     ! izone
      
      return
      end



      subroutine get_kext_profile(izone,Nzone,
     &     zone_lambda,zone_Nlev,
     &     zone_pressure,zone_altitude,zone_temperature,zone_kext,
     &     lambda,Nlev,pressure,altitude,temperature,kext)
      implicit none
      include 'max.inc'
c     
c     Purpose: to retrieve the kext profile data for a given zone
c     
c     Input:
c       + izone: zone index
c       + Nzone: number of zones
c       + zone_lambda: wavelenght the extinction coefficient of the zone is defined for [nm]
c       + zone_Nlev: number of atmospheric levels for the zone
c       + zone_pressure: pressure data for the zone [Pa]
c       + zone_altitude: altitude data for the zone [m]
c       + zone_temperature: temperature data for the zone [K]
c       + zone_kext: haze extinction coefficient data for the zone [inv. m]
c     
c     Output:
c       + lambda: wavelenght the extinction coefficient is defined for [nm]
c       + Nlev: number of vertical levels
c       + altitude: value of the altitude for each level, sorted by increasing altitude [m]
c       + pressure: value of the pressure for each level, sorted by increasing altitude [Pa]
c       + temperature: value of the temperature for each level, sorted by increasing altitude [K]
c       + kext: value of kext @ nu for each level, sorted by increasing altitude [inv. m]
c     
c     I/O
      integer izone
      integer Nzone
      double precision zone_lambda(0:Nzone_mx)
      integer zone_Nlev(0:Nzone_mx)
      double precision zone_altitude(0:Nzone_mx,1:Nlev_mx)
      double precision zone_pressure(0:Nzone_mx,1:Nlev_mx)
      double precision zone_temperature(0:Nzone_mx,1:Nlev_mx)
      double precision zone_kext(0:Nzone_mx,1:Nlev_mx)
      double precision lambda
      integer Nlev
      double precision altitude(1:Nlev_mx)
      double precision pressure(1:Nlev_mx)
      double precision temperature(1:Nlev_mx)
      double precision kext(1:Nlev_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine get_kext_profile'

      if ((izone.lt.0).or.(izone.gt.Nzone)) then
         call error(label)
         write(*,*) 'izone=',izone
         write(*,*) 'should be in the [0,',Nzone,'] range'
         stop
      endif
      lambda=zone_lambda(izone)
      Nlev=zone_Nlev(izone)
      do i=1,Nlev
         altitude(i)=zone_altitude(izone,i)
         pressure(i)=zone_pressure(izone,i)
         temperature(i)=zone_temperature(izone,i)
         kext(i)=zone_kext(izone,i)
      enddo                     ! i

      return
      end



      subroutine identify_zone(Nzone,zone_theta,zone_phi,theta,phi,
     &     izone)
      implicit none
      include 'max.inc'
c     
c     Purpose: identify the zone for a given lon/lat position
c     
c     Input:
c       + Nzone: number of zones
c       + zone_theta: latitude limits for each zone [deg]
c       + zone_phi: longitude limits for each zone [deg]
c       + theta: value of the latitude (-90,90) [deg]
c       + phi: value of the longitude (0,360) [deg]
c     
c     Output:
c       + izone: index of the zone [0-Nzone]
c     
c     I/O
      integer Nzone
      double precision zone_theta(1:Nzone_mx,1:2)
      double precision zone_phi(1:Nzone_mx,1:2)
      double precision theta
      double precision phi
      integer izone
c     temp
      integer i
      double precision theta_min,theta_max
      double precision phi_min,phi_max
      logical zone_found
      logical belongs_to_zone
c     label
      character*(Nchar_mx) label
      label='subroutine identify_zone'

      if ((theta.lt.-90.0D+0).or.(theta.gt.90.0D+0)) then
         call error(label)
         write(*,*) 'theta=',theta
         write(*,*) 'should be in the [-90,90] range'
         stop
      endif
      if ((phi.lt.0.0D+0).or.(phi.gt.360.0D+0)) then
         call error(label)
         write(*,*) 'phi=',phi
         write(*,*) 'should be in the [0,360] range'
         stop
      endif
      
      zone_found=.false.
      do i=1,Nzone
         belongs_to_zone=.false.
         theta_min=zone_theta(i,1)
         theta_max=zone_theta(i,2)
         if (theta_min.ge.theta_max) then
            call error(label)
            write(*,*) 'zone index:',i
            write(*,*) 'theta_min=',theta_min
            write(*,*) 'is > theta_max=',theta_max
            stop
         endif
         phi_min=zone_phi(i,1)
         phi_max=zone_phi(i,2)
         if (phi_min.ge.phi_max) then
            call error(label)
            write(*,*) 'zone index:',i
            write(*,*) 'phi_min=',phi_min
            write(*,*) 'is > phi_max=',phi_max
            stop
         endif
         if ((theta.ge.theta_min).and.(theta.lt.theta_max)) then
            if (phi_max.le.360.0D+0) then
               if ((phi.ge.phi_min).and.(phi.lt.phi_max)) then
                  belongs_to_zone=.true.
               endif
            else                ! phi_max > 360
               do while (phi_max.ge.0.0D+0)
                  phi_max=phi_max-360.0D+0
               enddo            ! while phi_max > 0
               phi_max=phi_max+360.0D+0 ! now phi_max belongs to [0,360] deg
               if ((phi.ge.phi_min).or.(phi.lt.phi_max)) then
                  belongs_to_zone=.true.
               endif
            endif               ! phi_max <> 360
         endif                  ! theta
c     Now "belongs_to_zone" is known
         if ((belongs_to_zone).and.(zone_found)) then
            call error(label)
            write(*,*) 'position:',theta,',',phi
            write(*,*) 'has be identified in zone:',i
            write(*,*) 'and was previously identified in a zone'
            write(*,*) 'overlapping zones ?'
            stop
         endif
         if ((belongs_to_zone).and.(.not.zone_found)) then
            zone_found=.true.
            izone=i
         endif
      enddo                     ! i
c     If provided position does not belong to any defined zone,
c     the use default properties
      if (.not.zone_found) then
         izone=0
      endif

      return
      end
