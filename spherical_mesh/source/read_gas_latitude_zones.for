      subroutine read_gas_latitude_zones(file_in,
     &     Nzone,zone_limit,zone_profile)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a gas latitude limits definition file
c     
c     Input:
c       + file_in: file to read
c     
c     Output:
c       + Nzone: number of latitude zones
c       + zone_limit: lower and higer latitude for each latitude zone [deg]
c       + zone_profile: index of the altitude profile to use for each latitude zone
c     
c     I/O
      character*(Nchar_mx) file_in
      integer Nzone
      double precision zone_limit(1:Nzone_mx,1:2)
      integer zone_profile(1:Nzone_mx)
c     temp
      integer ios
      logical keep_reading
      double precision theta1,theta2
      integer index
c     label
      character*(Nchar_mx) label
      label='subroutine read_gas_latitude_zones'

      open(11,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(file_in)
         stop
      endif
      
      Nzone=0
      keep_reading=.true.
      do while (keep_reading)
         read(11,*,iostat=ios) theta1,theta2,index
         if (ios.ne.0) then
            keep_reading=.false.
            goto 111
         endif
         if (theta1.gt.theta2) then
            call error(label)
            write(*,*) 'while reading file: ',trim(file_in)
            write(*,*) 'theta1=',theta1
            write(*,*) 'should be < theta2=',theta2
            stop
         endif
         if ((theta1.lt.-90.0D+0).or.(theta1.gt.90.0D+0)) then
            call error(label)
            write(*,*) 'while reading file: ',trim(file_in)
            write(*,*) 'theta1=',theta1
            write(*,*) 'should be in the [-90,90] range'
            stop
         endif
         if ((theta2.lt.-90.0D+0).or.(theta2.gt.90.0D+0)) then
            call error(label)
            write(*,*) 'while reading file: ',trim(file_in)
            write(*,*) 'theta2=',theta2
            write(*,*) 'should be in the [-90,90] range'
            stop
         endif
         if (index.le.0) then
            call error(label)
            write(*,*) 'index=',index
            write(*,*) 'should be positive'
            stop
         endif
         Nzone=Nzone+1
         if (Nzone.gt.Nzone_mx) then
            call error(label)
            write(*,*) 'Nzone_mx has been reached'
            stop
         endif
         zone_limit(Nzone,1)=theta1
         zone_limit(Nzone,2)=theta2
         zone_profile(Nzone)=index
c     
 111     continue
      enddo                     ! while (keep_reading)
      close(11)
c     
      if (Nzone.le.0) then
         call error(label)
         write(*,*) 'Nzone=',Nzone
         write(*,*) 'should be positive'
         stop
      endif
      if (zone_limit(1,1).ne.-90.0D+0) then
         call error(label)
         write(*,*) 'zone_limit(1,1)=',zone_limit(1,1)
         write(*,*) 'should be equal to -90 deg'
         stop
      endif
      if (zone_limit(Nzone,2).ne.90.0D+0) then
         call error(label)
         write(*,*) 'zone_limit(',Nzone,',2)=',zone_limit(Nzone,2)
         write(*,*) 'should be equal to 90 deg'
         stop
      endif
      
      return
      end
