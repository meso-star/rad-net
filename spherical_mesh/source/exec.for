c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine exec(command)
      implicit none
      include 'max.inc'
c
c     Purpose: to execute the system command defined in the "command" character string
c
c     Input:
c       + command: character string that defines the system command to execute
c         (example: command='cp toto1 toto2')
c
      character*(Nchar_mx) command,label
      label='subroutine exec'

      call system(trim(command))

      return
      end
