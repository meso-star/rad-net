      subroutine read_abundance_file(input_file,Nlev,pressure,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a gas molar abundance file
c     
c     Input:
c       + input_file: file to read
c     
c     Output:
c       + Nlev: number of pressure/x levels
c       + pressure: pressure levels [Pa]
c       + x: abundance [mol/mol]
c     
c     I/O
      character*(Nchar_mx) input_file
      integer Nlev
      double precision pressure(1:Nlev_mx)
      double precision x(1:Nlev_mx)
c     temp
      integer ios,lineios
      logical keep_reading
      double precision tmp1,tmp2
c     label
      character*(Nchar_mx) label
      label='subroutine read_abundance_file'

      open(19,file=trim(input_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(input_file)
         stop
      else
         write(*,*) 'Reading abundance file: ',trim(input_file)
      endif
      read(19,*)                ! comment line
      keep_reading=.true.
      Nlev=0
      do while (keep_reading)
         read(19,*,iostat=lineios) tmp1,tmp2
         if (lineios.eq.0) then
            Nlev=Nlev+1
            if (Nlev.gt.Nlev_mx) then
               call error(label)
               write(*,*) 'Nlev_mx has been reached'
               stop
            endif
            pressure(Nlev)=tmp1*1.0D+2 ! mb -> Pa
            x(Nlev)=tmp2        ! mol/mol
         else
            keep_reading=.false.
         endif                  ! lineios
      enddo                     ! while (keep_reading)
      close(19)

      return
      end
