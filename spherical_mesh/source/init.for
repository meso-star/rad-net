c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine init(obj_file,mtllib_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: perform global initialiation tasks
c
c     Input:
c       + obj_file: obj file that will be produced by the code
c       + mtllib_file: name of the materials library file
c
c     I/O
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
c     temp
      character*(Nchar_mx) command,file,exec_file
      logical file_exists
c     label
      character*(Nchar_mx) label
      label='subroutine init'

      write(*,*) 'Initialization...'
      command='rm -f '//trim(obj_file)
      call exec(command)
      file='./results/'//trim(mtllib_file)
      command='rm -f '//trim(file)
      call exec(command)
      command='rm -f ./results/*.dat'
      call exec(command)
c     Compilation of "triangle"
c$$$      write(*,*) 'Compilation of "triangle"...'
c$$$      exec_file='./triangle/triangle'
c$$$      command='rm -f '//trim(exec_file)
c$$$      command='cd ./triangle; ./compile_triangle.bash'
c$$$      call exec(command)
c$$$      inquire(file=trim(exec_file),exist=file_exists)
c$$$      if (.not.file_exists) then
c$$$         write(*,*) 'Compilation of TRIANGLE failed'
c$$$         stop
c$$$      else
c$$$         write(*,*) 'success'
c$$$      endif

      return
      end
