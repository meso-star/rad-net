      subroutine read_kext_file(file_in,
     &     nu,Nlev,altitude,pressure,temperature,kext)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read a kext input data file for the haze of Titan
c     
c     Input:
c       + file_in: access path to the file to read
c     
c     Output:
c       + nu: wavenumber [inv. cm]
c       + Nlev: number of vertical levels
c       + altitude: value of the altitude for each level, sorted by increasing altitude [m]
c       + pressure: value of the pressure for each level, sorted by increasing altitude [Pa]
c       + temperature: value of the temperature for each level, sorted by increasing altitude [K]
c       + kext: value of kext @ nu for each level, sorted by increasing altitude [inv. m]
c     
c     I/O
      character*(Nchar_mx) file_in
      double precision nu
      integer Nlev
      double precision altitude(1:Nlev_mx)
      double precision pressure(1:Nlev_mx)
      double precision temperature(1:Nlev_mx)
      double precision kext(1:Nlev_mx)
c     temp
      logical consistent
      integer ios,i
      character*(Nchar_mx) str
      logical nu_found
      logical keep_reading
      double precision alt,pres,k,min,kmax,sigma,temp
      logical need2invert
      double precision temp1(1:Nlev_mx)
      double precision temp2(1:Nlev_mx)
      double precision temp3(1:Nlev_mx)
      double precision temp4(1:Nlev_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine read_kext_file'

c     testing consistency
      call check_kext_consistency(file_in,consistent)
      if (.not.consistent) then
         call error(label)
         write(*,*) 'Input file: ',trim(file_in)
         write(*,*) 'Is not consistent or does not exist'
         stop
      endif
      
      open(11,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(file_in)
         stop
      else
         write(*,*) 'reading file: ',trim(file_in)
         read(11,10) str
         call identify_nu(str,nu_found,nu)
         keep_reading=.true.
         do while (keep_reading)
            read(11,10) str
            if (str(1:9).eq.'#Altitude') then
               keep_reading=.false.
            endif
         enddo                  ! while (keep_reading)
         Nlev=0
         keep_reading=.true.
         do while (keep_reading)
            read(11,*,iostat=ios) alt,pres,k,min,kmax,sigma,temp
            if (ios.eq.0) then
               Nlev=Nlev+1
               if (Nlev.gt.Nlev_mx) then
                  call error(label)
                  write(*,*) 'Nlev_mx=',Nlev_mx
                  write(*,*) 'has been reached'
                  stop
               endif
               altitude(Nlev)=alt*1.0D+3 ! km -> m
               pressure(Nlev)=pres*1.D+2 ! mb -> Pa
               temperature(Nlev)=temp    ! K
               kext(Nlev)=k*1.0D+2       ! inv. cm -> inv. m
            else
               keep_reading=.false.
            endif
         enddo                  ! while (keep_reading)
         need2invert=.false.
         if (altitude(Nlev).lt.altitude(1)) then
            need2invert=.true.
         endif
         if (need2invert) then
            do i=1,Nlev
               temp1(i)=altitude(i)
               temp2(i)=pressure(i)
               temp3(i)=temperature(i)
               temp4(i)=kext(i)
            enddo               ! i
            do i=1,Nlev
               altitude(i)=temp1(Nlev-i+1)
               pressure(i)=temp2(Nlev-i+1)
               temperature(i)=temp3(Nlev-i+1)
               kext(i)=temp4(Nlev-i+1)
            enddo               ! i
         endif ! need2invert
      endif                     ! ios=0
      close(11)
      
      return
      end


      
      subroutine check_kext_consistency(file_in,consistent)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to test the consistency of a given file as providing values of the extinction coefficient
c     
c     Input:
c       + file_in: file to test
c     
c     Output:
c       + consistent: true if the provided file is consistent
c     
c     I/O
      character*(Nchar_mx) file_in
      logical consistent
c     temp
      integer i,ios
      character*(Nchar_mx) str
c     parameters
      character*(Nchar_mx) str_ref
      parameter(str_ref='#Altitude(km) Pressure(mbar) kext(cm-1)'
     &     //'       kext_min       kext_max'
     &     //'     sigma  Temperature(K)')
c     label
      character*(Nchar_mx) label
      label='subroutine check_kext_consistency'

      consistent=.false.
      open(11,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then        ! file does not exist
         goto 666
      endif
      do i=1,12
         read(11,10,iostat=ios) str
         if (ios.ne.0) then     ! line should be there but is not
            close(11)
            goto 666
         endif
      enddo                     ! i
      if (trim(str).eq.trim(str_ref)) then ! the right character string must be found
         close(11)
         consistent=.true.
      endif
      
 666  continue

      return
      end
