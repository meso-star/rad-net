      subroutine read_surface_temperature_file(in_file,Nt,surf_T)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a surface temperature definition file
c     
c     Input:
c       + in_file: file to read
c     
c     Output:
c       + Nt: number of theta/T values
c       + surf_T: values of the latitude [deg] and associated surface temperature [K]
c     
c     I/O
      character*(Nchar_mx) in_file
      integer Nt
      double precision surf_T(1:Nt_mx,1:2)
c     temp
      integer ios,rerr,i
      logical keep_looking
      double precision tmp1,tmp2
c     label
      character*(Nchar_mx) label
      label='subroutine read_surface_temperature_file'

      open(18,file=trim(in_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(in_file)
         stop
      endif
      read(18,*)                ! comment line
      Nt=0
      keep_looking=.true.
      do while (keep_looking)
         read(18,*,iostat=rerr) tmp1,tmp2
         if (rerr.eq.0) then
            Nt=Nt+1
            if (Nt.gt.Nt_mx) then
               call error(label)
               write(*,*) 'Nt_mx has been reached'
               stop
            endif
            if ((tmp1.lt.-90.0D+0).or.(tmp1.gt.90.0D+0)) then
               call error(label)
               write(*,*) 'latitude:',tmp1
               write(*,*) 'should be in the [-90,90]° range'
               stop
            endif
            if (tmp2.le.0.0D+0) then
               call error(label)
               write(*,*) 'temperature:',tmp2
               write(*,*) 'should be positive'
               stop
            endif
            surf_T(Nt,1)=tmp1
            surf_T(Nt,2)=tmp2
         else                   ! rerr.ne.0
            keep_looking=.false.
         endif                  ! rerr
      enddo                     ! while (keep_looking)
      close(18)
      
c     check
      do i=2,Nt
         if (surf_T(i,1).le.surf_T(i-1,1)) then
            call error(label)
            write(*,*) 'surf_T(',i,',1)=',surf_T(i,1)
            write(*,*) 'should be > surf_T(',i-1,',1)=',surf_T(i-1,1)
            stop
         endif
      enddo                     ! i

      return
      end
