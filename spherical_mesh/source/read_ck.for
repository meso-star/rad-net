      subroutine read_ck(kfile,
     &     Nlev,Nprofile,Nlambda,Nq,
     &     ilambda,iq,
     &     data_found,lambda_idx,
     &     pressure,temperature,ck)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a file containing CK data
c     
c     Input:
c       + kfile: name of the data file
c       + Nlev: number of vertical levels (identical for every profile)
c       + Nprofile: number of profiles
c       + Nlambda: number of values for the wavelength
c       + Nq: quadrature order
c       + ilambda: index of the wavelength data has to be retrieved for
c       + iq: quadrature index data has to be retrieved for
c       + data_found: array of T/F values depending on the presence or the absence of data for each value of lambda
c       + lambda_idx:
c         - if data_found(ilambda): lambda_idx(ilambda) provides the index of the band (in the file)
c           that contains the data for lambda(ilambda)
c         - otherwise, lambda_idx(ilambda)=0
c     
c     Output:
c       + pressure: pressure array, identical for every profile [Pa]
c       + temperature: temperature array, depends on the profile [K]
c       + ck: CK cross-section for the required quadrature point [cm²/molec]
c     
c     I/O
      character*(Nchar_mx) kfile
      integer Nlev
      integer Nprofile
      integer Nlambda
      integer Nq
      integer ilambda,iq
      logical data_found(1:Nlambda_mx)
      integer lambda_idx(1:Nlambda_mx)
      double precision pressure(1:Nlev_mx)
      double precision temperature(1:Nprofile_mx,1:Nlev_mx)
      double precision ck(1:Nprofile_mx,1:Nlev_mx)
c     temp
      integer Nline,Ncol,Nll,i,j,nread
      integer iprofile,ilev,il1,iq1
      integer ios
      integer Nlambda_tmp,Nq_tmp,itmp,Nlpl
      double precision sum_w
      double precision lambda_tmp,raf
      double precision tmp1(1:Nq_mx)
      logical ilambda_found
c     label
      character*(Nchar_mx) label
      label='subroutine read_ck'

      open(21,file=trim(kfile),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(kfile)
         stop
      endif
c     Metadata
      read(21,*)
c     reading pressure levels
      Ncol=5
      Nline=Nlev/Ncol
      Nll=modulo(Nlev,Ncol)
      nread=0
      do i=1,Nline
         read(21,*) (pressure(nread+j),j=1,Ncol) ! hPa
         nread=nread+Ncol
      enddo                     ! i
      read(21,*) (pressure(nread+j),j=1,Nll) ! hPa
c     conversion
      do i=1,Nlev
         pressure(i)=pressure(i)*1.0D+2
      enddo                     ! i
c     reading temperatures
      do ilev=1,Nlev
         read(21,*) (temperature(iprofile,ilev),iprofile=1,Nprofile) ! K
      enddo                     ! ilev

      if (data_found(ilambda)) then
c     skipping quadrature abscissas and weights
         Nline=2*(Nq/Ncol+1)
         do i=1,Nline
            read(21,*)
         enddo                  ! i
c     reading values of the wavelength and CK data, per level, per profile
         Nlpl=(Nq/Ncol+2)*Nlev*Nprofile
         do i=1,lambda_idx(ilambda)-1
            do j=1,Nlpl
               read(21,*)
            enddo               ! j
         enddo                  ! i
c     
         Ncol=5
         Nline=Nq/Ncol
         Nll=modulo(Nq,Ncol)
         do iprofile=1,Nprofile
            do ilev=1,Nlev
               read(21,*)
               nread=0
               do i=1,Nline
                  read(21,*) (tmp1(nread+j),j=1,Ncol)
                  nread=nread+Ncol
               enddo            ! i
               read(21,*) (tmp1(nread+j),j=1,Nll)
               ck(iprofile,ilev)=tmp1(iq)
            enddo               ! ilev
         enddo                  ! iprofile
      else                      ! data_found(ilambda)=F
         do iprofile=1,Nprofile
            do ilev=1,Nlev
               ck(iprofile,ilev)=0.0D+0
            enddo               ! ilev
         enddo                  ! iprofile
      endif                     ! data_found(ilambda)
      close(21)

      return
      end


      
      subroutine analyze_spectral_structure(kfile,reference,
     &     Nlev,Nprofile,
     &     Nlambda,lambda,Nq,g,w,
     &     data_found,lambda_idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to analyze the spectral structure of a file containing CK data
c     
c     Input:
c       + kfile: name of the data file
c       + reference: defines the spectral grid as reference or not
c         - reference=T: the Nlambda, lambda, Nq, g and w information that is read from the
c                        data file is considered as a reference (this is output data)
c         - reference=F: Nlambda, lambda, Nq, g and w information is considered
c                        as INPUT DATA (reference data, coming from a different source).
c                        All spectral data that does not match any wavelength
c                        of the reference grid will cause a error; all wavelength
c                        points of the reference grid that are not provided with
c                        some data will be replaced by null data.
c       + Nlambda: number of values for the wavelength if reference=F
c       + lambda: wavelength array [nm] if reference=F
c       + Nq: quadrature order if reference=F
c       + g: quadrature abscissas if reference=F
c       + w: quadrature weights if reference=F
c     
c       + ilambda: index of the wavelength data has to be retrieved for
c       + iq: quadrature index data has to be retrieved for
c     
c     
c     Output:
c       + Nlev: number of vertical levels (identical for every profile)
c       + Nprofile: number of profiles
c       + Nlambda: number of values for the wavelength if reference=T
c       + lambda: wavelength array [nm] if reference=T
c       + Nq: quadrature order if reference=T
c       + g: quadrature abscissas if reference=T
c       + w: quadrature weights if reference=T
c       + data_found: array of T/F values depending on the presence or the absence of data for each value of lambda
c       + lambda_idx:
c         - if data_found(ilambda): lambda_idx(ilambda) provides the index of the band (in the file)
c           that contains the data for lambda(ilambda)
c         - otherwise, lambda_idx(ilambda)=0
c     
c     I/O
      character*(Nchar_mx) kfile
      logical reference
      integer Nlev
      integer Nprofile
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      integer Nq
      double precision w(1:Nq_mx)
      double precision g(1:Nq_mx)
      logical data_found(1:Nlambda_mx)
      integer lambda_idx(1:Nlambda_mx)
c     temp
      integer ios
      integer Nlambda_tmp,Nq_tmp
      integer Ncol,Nline,Nll,nread
      integer i,j,itmp,ilambda,ilev,iq
      double precision sum_w,lambda_tmp,raf
      logical ilambda_found
c     label
      character*(Nchar_mx) label
      label='subroutine analyze_spectral_structure'

      open(21,file=trim(kfile),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(kfile)
         stop
      else
         write(*,*) 'Reading file: ',trim(kfile)
      endif
c     Metadata
      if (reference) then
         read(21,*) Nlev,Nprofile,Nlambda,Nq
c     Debug
c         write(*,*) 'Nlambda=',Nlambda,' Nq=',Nq
c     Debug
      else
         read(21,*) Nlev,Nprofile,Nlambda_tmp,Nq_tmp
      endif                     ! reference
      if (Nlev.gt.Nlev_mx) then
         call error(label)
         write(*,*) 'Nlev=',Nlev
         write(*,*) '> Nlev_mx=',Nlev_mx
         stop
      endif
      if (Nprofile.gt.Nprofile_mx) then
         call error(label)
         write(*,*) 'Nprofile=',Nprofile
         write(*,*) '> Nprofile_mx=',Nprofile_mx
         stop
      endif
      if (reference) then
         if (Nlambda.gt.Nlambda_mx) then
            call error(label)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) '> Nlambda_mx=',Nlambda_mx
            stop
         endif
         if (Nq.gt.Nq_mx) then
            call error(label)
            write(*,*) 'Nq=',Nq
            write(*,*) '> Nq_mx=',Nq_mx
            stop
         endif
      else
         if (Nlambda_tmp.gt.Nlambda_mx) then
            call error(label)
            write(*,*) 'Nlambda_tmp=',Nlambda_tmp
            write(*,*) '> Nlambda_mx=',Nlambda_mx
            stop
         endif
         if (Nq_tmp.gt.Nq_mx) then
            call error(label)
            write(*,*) 'Nq_tmp=',Nq_tmp
            write(*,*) '> Nq_mx=',Nq_mx
            stop
         endif
         if (Nq_tmp.ne.Nq) then
            call error(label)
            write(*,*) 'Nq=',Nq
            write(*,*) 'Nq_tmp=',Nq_tmp
            stop
         endif
c     From there, only "Nq" can be used since it is supposed to be identical to "Nq_tmp"
      endif                     ! reference
      Ncol=5
c     skipping pressure levels
      Nline=Nlev/Ncol+1
      do i=1,Nline
         read(21,*)
      enddo                     ! i
c     skipping temperatures
      do ilev=1,Nlev
         read(21,*)
      enddo                     ! ilev
c     reading quadrature abscissas
      Ncol=5
      Nline=Nq/Ncol
      Nll=modulo(Nq,Ncol)
      nread=0
      do i=1,Nline
         read(21,*) (g(nread+j),j=1,2)
         nread=nread+Ncol
      enddo                     ! i
      read(21,*) (g(nread+j),j=1,Nll)
c     reading quadrature weights
      Ncol=5
      Nline=Nq/Ncol
      Nll=modulo(Nq,Ncol)
      nread=0
      do i=1,Nline
         read(21,*) (w(nread+j),j=1,Ncol)
         nread=nread+Ncol
      enddo                     ! i
      read(21,*) (w(nread+j),j=1,Nll)
      sum_w=0.0D+0
      do iq=1,Nq
         sum_w=sum_w+w(iq)
      enddo                     ! i
      if (sum_w.ne.1.0D+0) then
         if (dabs(sum_w-1.0D+0).lt.1.0D-2) then
c     normalization
            do iq=1,Nq
               w(iq)=w(iq)/sum_w
            enddo               ! iq
         else
c     error
            call error(label)
            do iq=1,Nq
               write(*,*) 'w(',iq,')=',w(iq)
            enddo               ! iq
            write(*,*) 'sum(w)=',sum_w
            write(*,*) 'Is too different from 1'
            stop
         endif
         do iq=1,Nq
            w(iq)=w(iq)/sum_w
         enddo                  ! iq
      endif
c     read values of the wavelength, skip CK data
      if (reference) then
         do ilambda=1,Nlambda
            read(21,*) lambda(ilambda) ! µm
            lambda(ilambda)=lambda(ilambda)*1.0D+3 ! µm -> nm
c     Debug
c            write(*,*) 'lambda(',ilambda,')=',lambda(ilambda)
c     Debug
            data_found(ilambda)=.true.
            lambda_idx(ilambda)=ilambda
            Nline=(Nq/Ncol+2)*Nlev*Nprofile-1
            do i=1,Nline
               read(21,*) 
            enddo               ! i
         enddo                  ! ilambda
      else                      ! reference=F
         do ilambda=1,Nlambda
            data_found(ilambda)=.false.
            lambda_idx(ilambda)=0
         enddo                  ! ilambda
         do itmp=1,Nlambda_tmp
            read(21,*) lambda_tmp ! µm
            lambda_tmp=lambda_tmp*1.0D+3 ! µm -> nm
c     
            ilambda_found=.false.
            do i=1,Nlambda
               if (lambda(i).eq.lambda_tmp) then
                  ilambda_found=.true.
                  data_found(i)=.true.
                  lambda_idx(i)=itmp
                  goto 211
               endif
            enddo               ! i
 211        continue
            if (.not.ilambda_found) then
               call error(label)
               write(*,*) 'lambda_tmp=',lambda_tmp
               write(*,*) 'does not match any '
     &              //'reference lambda:'
               do i=1,Nlambda
                  write(*,*) i,lambda(i)
               enddo            ! i
               stop
            endif
            Nline=(Nq/Ncol+2)*Nlev*Nprofile-1
            do i=1,Nline
               read(21,*) 
            enddo               ! i
         enddo                  ! itmp
      endif                     ! reference
      close(21)
c     Debug
c      if (reference) then
c         stop
c      endif
c     Debug

      return
      end
