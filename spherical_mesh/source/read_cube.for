      subroutine cube_fusion(dim,data_dir,
     &     Ncube_def,definition,
     &     Ngroup,Ncube_in_group,group_file,cube_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to detect all VIMS cube definition files and
c     try to fusion then into groups when they overlap; cubes that
c     have not been fusioned are the only member of their group
c     
c     Input:
c       + dim: dimension of space
c       + data_dir: base directory for finding input data files
c       + Ncube_def: number of cubes found in definition file
c       + definition(icube,1): pixel definition 1
c       + definition(icube,2): pixel definition 2
c       + definition(icube,3): spectral definition
c     
c     Output:
c       + Ngroup: number of groups
c       + Ncube_in_group: number of files/cubes in each group
c       + group_file: list of group definition files and spectral grid files, per group
c       + cube_index: index of every cube in each group
c     
c     I/O
      integer dim
      character*(Nchar_mx) data_dir
      integer Ncube_def
      integer definition(1:Ncube_mx,1:3)
      integer Ngroup
      integer Ncube_in_group(1:Ngroup_mx)
      character*(Nchar_mx) group_file(1:Ngroup_mx,1:Nfig_mx,1:2)
      integer cube_index(1:Ngroup_mx,1:Nfig_mx)
c     temp
      integer igroup,ifile,i,j,icorner
      integer cube_def(1:3)
      logical keep_looking
      integer Ncube,icube,jcube
      character*(Nchar_mx) Ncube_str,cube_file,lambda_file
      logical cube_file_exists,lambda_file_exists,err_code
      character*(Nchar_mx) files(1:Ncube_mx,1:2)
      integer Npix(1:2)
      double precision corner_theta(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf(1:Npix_mx,1:Npix_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
      double precision min_phi(1:Ncube_mx),max_phi(1:Ncube_mx)
      double precision min_theta(1:Ncube_mx),max_theta(1:Ncube_mx)
      double precision dphi(1:Ncube_mx),dtheta(1:Ncube_mx)
      logical is_in_group(1:Ngroup_mx),overlap,icube_is_alone,is_already_in_group
      integer group_index(1:Ngroup_mx)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      double precision P(1:Ndim_mx)
      logical inside
      character*(Nchar_mx) output_file
c     label
      character*(Nchar_mx) label
      label='subroutine cube_fusion'

      write(*,*) 'Identification of VIMS cubes...'
c     Identify all cubes (a cube definition must exist, and the corresponding spectral grid file also)
      Ncube=0
      keep_looking=.true.
      do while (keep_looking)
         call num2str2(Ncube+1,Ncube_str,err_code)
         if (err_code) then
            call error(label)
            write(*,*) 'Could not convert to integer:'
            write(*,*) 'Ncube+1=',Ncube+1
            stop
         endif
         cube_file=trim(data_dir)//'VIMS_cube'//trim(Ncube_str)//'.dat'
         inquire(file=trim(cube_file),exist=cube_file_exists)
         if (cube_file_exists) then
            Ncube=Ncube+1
            if (Ncube.gt.Ncube_mx) then
               call error(label)
               write(*,*) 'Ncube_mx has been reached'
               stop
            endif
            files(Ncube,1)=trim(cube_file)
            write(*,*) 'Found VIMS cube ',Ncube,' ',trim(cube_file)
            lambda_file=trim(data_dir)//'lambda_cube'
     &           //trim(Ncube_str)//'.dat'
            inquire(file=trim(lambda_file),exist=lambda_file_exists)
            if (lambda_file_exists) then
               write(*,*) 'Found corresponding spectral grid: ',trim(lambda_file)
               files(Ncube,2)=trim(lambda_file)
            else
               call error(label)
               write(*,*) 'File not found: ',trim(lambda_file)
               write(*,*) 'For VIMS cube definitiion file: ',
     &              trim(cube_file)
               stop
            endif
         else                   ! cube_file_exists=F
            keep_looking=.false.
         endif
      enddo                     ! while (keep_reading)
      if (Ncube.gt.Ncube_def) then
         call error(label)
         write(*,*) 'Number of cubes found in definition file:',Ncube_def
         write(*,*) '< number of cube files found is:',Ncube
         stop
      endif
c     Reading data cubes
      do icube=1,Ncube
         do j=1,3
            cube_def(j)=definition(icube,j)
         enddo                  ! j
         call read_cube_file(dim,
     &        files(icube,1),files(icube,2),cube_def,
     &        Npix,corner_theta,corner_phi,corner_z,brdf,
     &        Nlambda,lambda,reflectivity)
         call analyze_cube(Npix,corner_theta,corner_phi,
     &        min_phi(icube),max_phi(icube),
     &        min_theta(icube),max_theta(icube),
     &        dphi(icube),dtheta(icube))
         is_in_group(icube)=.false.
      enddo                     ! icube
c     Detect overlapping cubes
      Ngroup=0
      Ncontour=2
      Nppc(1)=4
      Nppc(2)=4
      do icube=1,Ncube
c     Debug
c         write(*,*) 'icube=',icube
c     Debug
         icube_is_alone=.true.  ! default
         contour(1,1,1)=min_phi(icube)
         contour(1,1,2)=min_theta(icube)
         contour(1,2,1)=max_phi(icube)
         contour(1,2,2)=min_theta(icube)
         contour(1,3,1)=max_phi(icube)
         contour(1,3,2)=max_theta(icube)
         contour(1,4,1)=min_phi(icube)
         contour(1,4,2)=max_theta(icube)
c     Debug
c         do icorner=1,4
c            write(*,*) (contour(1,icorner,j),j=1,2)
c         enddo                  ! icorner
c     Debug
         do jcube=1,Ncube
            if (jcube.eq.icube) then
               goto 112
            endif
c     Debug
c            write(*,*) 'jcube=',jcube
c     Debug
            contour(2,1,1)=min_phi(jcube)
            contour(2,1,2)=min_theta(jcube)
            contour(2,2,1)=max_phi(jcube)
            contour(2,2,2)=min_theta(jcube)
            contour(2,3,1)=max_phi(jcube)
            contour(2,3,2)=max_theta(jcube)
            contour(2,4,1)=min_phi(jcube)
            contour(2,4,2)=max_theta(jcube)
c     Debug
c            do icorner=1,4
c               write(*,*) (contour(2,icorner,j),j=1,2)
c            enddo               ! icorner
c     Debug
c     find whether or not cubes "icube" and "jcube" do overlap
            overlap=.false.
c     all it takes is one corner of icube inside jcube...
            do icorner=1,4
c     corner of icube (contour 1)
               do j=1,dim-1
                  P(j)=contour(1,icorner,j)
               enddo            ! j
               P(dim)=0.0D+0
c     inside contour of jcube (contour 2) ?
               icontour=2
               call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,
     &              icontour,P,inside)
               if (inside) then
                  overlap=.true.
                  goto 111
               endif            ! inside
            enddo               ! icorner
c     ...or one corner of jcube inside jcube
            do icorner=1,4
c     corner of jcube (contour 2)
               do j=1,dim-1
                  P(j)=contour(2,icorner,j)
               enddo            ! j
               P(dim)=0.0D+0
c     inside contour of icube (contour 1) ?
               icontour=1
               call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,
     &              icontour,P,inside)
               if (inside) then
                  overlap=.true.
                  goto 111
               endif            ! inside
            enddo               ! icorner
 111        continue
c     At this point, overlap=T means icube and jcube do overlap: at least one corner
c     of either icube or jcube is inside the other, which encompasses the case of
c     one cube completely defined inside the other
c     Debug
c            write(*,*) 'overlap=',overlap
c     Debug
            if (overlap) then
               icube_is_alone=.false.
               if (is_in_group(jcube)) then
c     icube should be added to the group jcube belongs to. But there is a possibility that it
c     is already in the group ! When jcube first tested positive the overlap by icube.
                  is_already_in_group=.false.
                  do i=1,Ncube_in_group(group_index(jcube))
                     if (trim(files(icube,1)).eq.trim(group_file(group_index(jcube),i,1))) then
                        is_already_in_group=.true.
                     endif
                  enddo         ! i
c     Debug
c                  write(*,*) 'is_already_in_group=',is_already_in_group
c     Debug
                  if (.not.is_already_in_group) then ! only if icube is not already in the group jcube belongs to
c     If jcube is already in a group, then icube must be added to this group
                     Ncube_in_group(group_index(jcube))=Ncube_in_group(group_index(jcube))+1
                     if (Ncube_in_group(group_index(jcube)).gt.Nfig_mx) then
                        call error(label)
                        write(*,*) 'Nfig_mx has been reached'
                        stop
                     endif
                     do j=1,2
                        group_file(group_index(jcube),Ncube_in_group(group_index(jcube)),j)=files(icube,j)
                     enddo      ! j
                     is_in_group(icube)=.true.
                     group_index(icube)=group_index(jcube)
                     cube_index(group_index(jcube),Ncube_in_group(group_index(jcube)))=icube
c     Debug
c                     write(*,*) 'cube ',icube,' has been added to group:',group_index(jcube)
c     Debug
                  endif         ! is_already_in_group=F
               else             ! is_in_group(jcube)=F
c     If jcube is not already in a group, then jcube must be added to the group icube belongs to (a new group may need to be created)
                  if (.not.is_in_group(icube)) then
                     Ngroup=Ngroup+1
                     if (Ngroup.gt.Ngroup_mx) then
                        call error(label)
                        write(*,*) 'Ngroup_mx has been reached'
                        stop
                     endif
                     Ncube_in_group(Ngroup)=1
                     if (Ncube_in_group(Ngroup).gt.Nfig_mx) then
                        call error(label)
                        write(*,*) 'Nfig_mx has been reached'
                        stop
                     endif
                     do j=1,2
                        group_file(Ngroup,1,j)=files(icube,j)
                     enddo      ! j
                     is_in_group(icube)=.true.
                     group_index(icube)=Ngroup
                     cube_index(Ngroup,Ncube_in_group(Ngroup))=icube
c     Debug
c                     write(*,*) 'group index:',group_index(icube),' has been created with cube',icube
c     Debug
                  endif         ! is_in_group(icube)=F
c     
                  Ncube_in_group(group_index(icube))=Ncube_in_group(group_index(icube))+1
                  if (Ncube_in_group(group_index(icube)).gt.Nfig_mx) then
                     call error(label)
                     write(*,*) 'Nfig_mx has been reached'
                     stop
                  endif
                  do j=1,2
                     group_file(group_index(icube),Ncube_in_group(group_index(icube)),j)=files(jcube,j)
                  enddo         ! j
                  is_in_group(jcube)=.true.
                  group_index(jcube)=group_index(icube)
                  cube_index(group_index(icube),Ncube_in_group(group_index(icube)))=jcube
c     Debug
c                  write(*,*) 'cube ',jcube,' has been added to group:',group_index(icube)
c     Debug
c     
               endif            ! is_in_group(jcube)
c     
            endif               ! overlap
c
 112        continue
         enddo                  ! jcube
c     
         if (icube_is_alone) then
c     If icube does not overlap with any other cube, then a new group has to be
c     created, containing only icube
            Ngroup=Ngroup+1
            if (Ngroup.gt.Ngroup_mx) then
               call error(label)
               write(*,*) 'Ngroup_mx has been reached'
               stop
            endif
            Ncube_in_group(Ngroup)=1
            if (Ncube_in_group(Ngroup).gt.Nfig_mx) then
               call error(label)
               write(*,*) 'Nfig_mx has been reached'
               stop
            endif
            do j=1,2
               group_file(Ngroup,1,j)=files(icube,j)
            enddo               ! j
            is_in_group(icube)=.true.
            group_index(icube)=Ngroup
            cube_index(Ngroup,Ncube_in_group(Ngroup))=icube
c     Debug
c            write(*,*) 'group index:',group_index(icube),' has been created with cube',icube
c     Debug
         endif                  ! icube_is_alone
      enddo                     ! icube
c     Debug
      output_file='./results/groups.txt'
      open(16,file=trim(output_file))
      write(*,*) 'Ngroup=',Ngroup
      do igroup=1,Ngroup
         write(16,*) 'Ncube_in_group(',igroup,')=',Ncube_in_group(igroup)
         do i=1,Ncube_in_group(igroup)
            write(16,*) 'group_file(',i,'):'
            do j=1,2
               write(16,*) trim(group_file(igroup,i,j))
            enddo               ! j
         enddo                  ! i
      enddo                     ! igroup
      close(16)
      write(*,*) 'Group info was recorded in: ',trim(output_file)
c     Debug
      
      return
      end
      


      subroutine read_cube_file(dim,
     &     file_in,lambda_file,definition,
     &     Npix,corner_theta,corner_phi,corner_z,brdf,
     &     Nlambda,lambda,reflectivity)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read a pixel zone file
c     
c     Input:
c       + dim: dimension of space
c       + file_in: pixel zone definition file
c       + lambda_file: spectral mesh definition file
c       + definition: expected number of pixels (in both directions) and spectral resolution
c     
c     Output:
c       + Npix: number of pixel in theta/phi directions
c       + corner_theta: latitude of each corner for each pixel [deg]
c       + corner_phi: longitude of each corner for each pixel [deg]
c       + corner_z: altitude of each corner for each pixel [m]
c       + brdf: BRDF for each pixel [0/1]
c       + Nlambda: number of wavelength
c       + lambda: values of the wavelength [nm]
c       + reflectivity: reflectivity for each pixel, for each wavelength
c     
c     I/O
      integer dim
      character*(Nchar_mx) file_in
      character*(Nchar_mx) lambda_file
      integer definition(1:3)
      integer Npix(1:2)
      double precision corner_theta(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf(1:Npix_mx,1:Npix_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
c     temp
      integer ios
      integer i,j,Ncol,Nline,Nll,iline,nread,icorner
      integer ipix,jpix,line_index,ilambda
      logical keep_reading
      double precision tmp1
      character*(Nchar_mx) line
c     functions
      integer nitems
c     label
      character*(Nchar_mx) label
      label='subroutine read_cube_file'

      if (definition(1).gt.Npix_mx) then
         call error(label)
         write(*,*) 'definition(1)=',definition(1)
         write(*,*) '> Npix_mx=',Npix_mx
         stop
      endif
      if (definition(2).gt.Npix_mx) then
         call error(label)
         write(*,*) 'definition(2)=',definition(2)
         write(*,*) '> Npix_mx=',Npix_mx
         stop
      endif
      if (definition(3).gt.Nlambda_mx) then
         call error(label)
         write(*,*) 'definition(3)=',definition(3)
         write(*,*) '> Nlambda_mx=',Nlambda_mx
         stop
      endif
c
      open(11,file=trim(lambda_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(lambda_file)
         stop
      endif
      Nlambda=0
      keep_reading=.true.
      do while (keep_reading)
         read(11,*,iostat=ios) tmp1 ! µm
         if (ios.eq.0) then
            Nlambda=Nlambda+1
            if (Nlambda.gt.Nlambda_mx) then
               call error(label)
               write(*,*) 'Nlambda_mx was reached'
               stop
            endif
            lambda(Nlambda)=tmp1*1.0D+3 ! nm
         else
            keep_reading=.false.
         endif
      enddo                     ! keep_reading
      close(11)
      if (Nlambda.ne.definition(3)) then
         call error(label)
         write(*,*) 'Provided spectral definition is:',definition(3)
         write(*,*) 'while the number wavelength is:',Nlambda
         write(*,*) 'from reading file: ',trim(lambda_file)
         stop
      endif
c      
      open(12,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(file_in)
         stop
      endif
c     First step: identify the number of columns
      read(12,10) line
      Ncol=nitems(line)
      rewind(12)
c     Then read the actual data
      Npix(1)=definition(1)
      Npix(2)=definition(2)
      Nline=Npix(1)/Ncol
      Nll=modulo(Npix(1),Ncol)
      line_index=0
c     longitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               read(12,*) (corner_phi(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (corner_phi(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         read(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     Debug
c     write(*,*) 'After reading phi: line_index=',line_index
c      j=1
c      do i=1,Npix(1)-1
c         write(*,*) corner_phi(i,j,2),corner_phi(i+1,j,1)
c      enddo                     ! j
c     Debug
c     latitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               read(12,*) (corner_theta(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (corner_theta(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         read(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     Debug
c      write(*,*) 'After reading theta: line_index=',line_index
c      i=1
c      do j=1,Npix(2)-1
c         write(*,*) corner_theta(i,j,4),corner_theta(i,j+1,1)
c      enddo ! i
c     Debug
c     altitude of each corner of each pixel
      do icorner=1,4
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               read(12,*) (corner_z(nread+j,jpix,icorner),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (corner_z(nread+j,jpix,icorner),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         read(12,*)
         line_index=line_index+1
      enddo                     ! icorner
c     Debug
c      write(*,*) 'After reading z: line_index=',line_index
c     Debug
c     BRDF of each pixel
      do jpix=1,Npix(2)
         nread=0
         do i=1,Nline
            read(12,*) (brdf(nread+j,jpix),j=1,Ncol) ! deg
            nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (brdf(nread+j,jpix),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
      enddo                     ! jpix
      read(12,*)
      line_index=line_index+1
c     Debug
c      write(*,*) 'After reading brdf: line_index=',line_index
c      write(*,*) 'brdf(',Npix(1),',',Npix(2),')=',brdf(Npix(1),Npix(2))
c      stop
c     Debug

c     reflectivity of each pixel
      do ilambda=1,Nlambda
         do jpix=1,Npix(2)
            nread=0
            do i=1,Nline
               read(12,*) (reflectivity(nread+j,jpix,ilambda),j=1,Ncol) ! deg
               nread=nread+Ncol
               line_index=line_index+1
            enddo               ! i
            if (Nll.gt.0) then
               read(12,*) (reflectivity(nread+j,jpix,ilambda),j=1,Nll) ! deg
               line_index=line_index+Nll
            endif
         enddo                  ! jpix
         if (ilambda.lt.Nlambda) then
            read(12,*)
            line_index=line_index+1
         endif
      enddo                     ! ilambda
      close(12)

c     Debug
c      write(*,*) 'After reading reflectivity: line_index=',line_index
c      write(*,*) 'reflectivity(',Npix(1),',',Npix(2),',',Nlambda,')=',
c     &     reflectivity(Npix(1),Npix(2),Nlambda)
c     Debug

      
      return
      end


      
      subroutine position_is_in_pixel(dim,
     &     x,igroup,Ncube_in_group,Npix,corner_theta,corner_phi,
     &     is_in_pixel,Npixel,pixel_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify whether or not a given (theta,phi) position is in a pixel
c     
c     Input:
c       + dim: dimension of space
c       + x: (phi,theta) position [deg, deg]
c       + igroup: index of the group
c       + Ncube_in_group: number of files/cubes in each group
c       + Npix: number of pixels along theta/phi directions
c       + corner_theta: value of theta for each corner of each pixel
c       + corner_phi: value of phi for each corner of each pixel
c     
c     Output:
c       + is_in_pixel: true if "x" is inside at least one pixel
c       + Npixel: number of pixels position "x" belongs to
c       + pixel_index: index of each cube/pixels "x" belgons to, when is_in_pixel=T
c     
c     I/O
      integer dim
      double precision x(1:Ndim_mx-1)
      integer igroup
      integer Ncube_in_group(1:Ngroup_mx)
      integer Npix(1:Nfig_mx,1:2)
      double precision corner_theta(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      logical is_in_pixel
      integer Npixel
      integer pixel_index(1:Npix_mx,1:3)
c     temp
      integer icube,ipix,jpix,icorner
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision P(1:Ndim_mx)
      logical inside
      integer Npixels
c     label
      character*(Nchar_mx) label
      label='subroutine position_is_in_pixel'

      Ncontour=1
      Nppc(1)=4
      P(1)=x(1)                 ! phi
      P(2)=x(2)                 ! theta
      P(3)=0.0D+0
      Npixel=0
      is_in_pixel=.false.
      do icube=1,Ncube_in_group(igroup)
         do ipix=1,Npix(icube,1)
            do jpix=1,Npix(icube,2)
               do icorner=1,Nppc(1)
c     VIMS pixels are defined clockwise while all contours should
c     be defined counter-clockwise
                  contour(1,icorner,1)=
     &                 corner_phi(icube,ipix,jpix,Nppc(1)-icorner+1)
                  contour(1,icorner,2)=
     &                 corner_theta(icube,ipix,jpix,Nppc(1)-icorner+1)
               enddo            ! icorner
               call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,
     &              1,P,inside)
               if (inside) then
                  is_in_pixel=.true.
                  Npixel=Npixel+1
                  if (Npixel.gt.Npix_mx) then
                     call error(label)
                     write(*,*) 'Npix_mx has been reached'
                     stop
                  endif
                  pixel_index(Npixel,1)=icube
                  pixel_index(Npixel,2)=ipix
                  pixel_index(Npixel,3)=jpix
               endif
            enddo               ! jpix
         enddo                  ! ipix
      enddo                     ! icube
c     
      return
      end
      


      subroutine higher_power2(int_in,n)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find the closest-higher value of 2^n
c     from a given integer
c     
c     Input:
c       + int_in: input integer
c     
c     Output:
c       + n: the lowest possible integer such that 2^n > int_in
c     
c     I/O
      integer int_in
      integer n
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine higher_power2'

      n=0
      do while (2**n.lt.int_in)
         n=n+1
      enddo

      return
      end
