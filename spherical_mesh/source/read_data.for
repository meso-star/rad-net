      subroutine read_data(in_file,data_dir,
     &     produce_surface_properties,planet_radius,mtlidx_file,grid_file_surface,surface_properties_file,
     &     produce_gas_properties,radius_gas,top_altitude_gas,Ntheta_gas,Nphi_gas,Nlayer_gas,gT_z_interpolation_mode,gT_z_extrapolation_mode,k_z_interpolation_mode,k_z_extrapolation_mode,gas_grid_file,gas_radiative_properties_file,gas_thermodynamic_properties_file,
     &     produce_cloud_properties,radius_cloud,top_altitude_cloud,Ntheta_cloud,Nphi_cloud,Nlayer_cloud,
     &     kext_z_interpolation_mode,kext_z_extrapolation_mode,cT_z_interpolation_mode,cT_z_extrapolation_mode,
     &     ssa_z_interpolation_mode,ssa_z_extrapolation_mode,ssa_lambda_interpolation_mode,ssa_lambda_extrapolation_mode,
     &     phase_function_list_file,cloud_grid_file,cloud_radiative_properties_file,phase_function_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read the input data file
c     
c     Input:
c       + in_file: file to read
c     
c     Output:
c       + data_dir: base directory for finding input data files
c       + produce_surface_properties: T if surface properties have to be generated
c       + planet_radius: radius of the spherical(ish) body for surface properties [m]
c       + mtlidx_file: file that will record the index of each declared material
c       + grid_file_surface: file that will record geometric data of the surface
c       + surface_properties_file: file that will record material and temperature information for the surface
c       + produce_gas_properties: T if gas mixture properties have to be generated
c       + radius_gas: radius of the spherical body for the gas mixture grid [m]
c       + altitude_gas: top altitude for the gas mixture grid [m]
c       + Ntheta_gas: number of latitude intervals for the gas mixture grid
c       + Nphi_gas: number of longitude intervals for the gas mixture grid
c       + Nlayer_gas: number of altitude intervals for the gas mixture grid
c       + gT_z_interpolation_mode: interpolation mode for the temperature over the spatial dimension for gas
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + gT_z_extrapolation_mode: extrapolation mode for the temperature over the spatial dimension for gas
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "T_set_value"
c       + k_z_interpolation mode: interpolation method for CK coefficients over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + k_z_extrapolation_mode: extrapolation mode for CK coefficients over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "k_set_value"
c       + gas_grid_file: file that will record the spatial grid for the gas mixture
c       + gas_radiative_properties_file: file that will record spectral data for the gas mixture
c       + gas_thermodynamic_properties_file: file that will record thermodynamic properties for the gas mixture
c       + produce_cloud_properties: T if cloud properties have to be generated
c       + radius_cloud: radius of the spherical body for the cloud grid [m]
c       + altitude_cloud: top altitude for the cloud grid [m]
c       + Ntheta_cloud: number of latitude intervals for the cloud grid
c       + Nphi_cloud: number of longitude intervals for the cloud grid
c       + Nlayer_cloud: number of altitude intervals for the cloud grid
c       + kext_z_interpolation_mode: interpolation mode for the extinction coefficient over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + kext_z_extrapolation_mode: extrapolation mode for the extinction coefficient over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "kext_set_value"
c       + cT_z_interpolation_mode: interpolation mode for the temperature over the spatial dimension for clouds
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + cT_z_extrapolation_mode: extrapolation mode for the temperature over the spatial dimension for clouds
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input value of "T_set_value"
c       + ssa_z_interpolation_mode: interpolation mode for the single-scattering albedo over the spatial dimension
c         - pressure_linear: linear with pressure
c         - altitude_linear: linear with altitude
c         - bottom_constant: constant by pressure/altitude level (value of the bottom level)
c         - top_constant: constant by pressure/altitude level (value of the top level)
c         - avg_constant: constant by pressure/altitude level (average of the values at bottom and top levels)
c       + ssa_z_extrapolation_mode: extrapolation mode for the single-scattering albedo over the spatial dimension
c         - pressure_linear: linear with pressure (over the first/last interval)
c         - altitude_linear: linear with altitude (over the first/last interval)
c         - constant: constant to the bottom/top value
c         - set: use the input values of "ssa_set_value"
c       + ssa_lambda_interpolation_mode: interpolation mode for the single-scattering albedo over the spectral dimension
c         - linear: linear with wavelength
c         - low_constant: constant by interval (value of the lower limit)
c         - high_constant: constant by interval (value of the higher limit)
c         - avg_constant: constant by interval (average of the values for the lower and higher limits)
c       + ssa_lambda_extrapolation_mode: extrapolation mode for the single-scattering albedo over the spectral dimension
c         - linear: linear with wavelength
c         - constant: constant to the low/high limit
c       + phase_function_list_file: file that holds the list of phase function definition files
c       + cloud_grid_file: file that will record the spatial grid for the clouds
c       + cloud_radiative_properties_file: file that will record the spectral properties of the clouds
c       + phase_function_file: file that will record the phase function of the clouds
c     
c     I/O
      character*(Nchar_mx) in_file
      character*(Nchar_mx) data_dir
      logical produce_surface_properties
      double precision planet_radius
      character*(Nchar_mx) mtlidx_file
      character*(Nchar_mx) grid_file_surface
      character*(Nchar_mx) surface_properties_file
      logical produce_gas_properties
      double precision radius_gas
      double precision top_altitude_gas
      integer Ntheta_gas
      integer Nphi_gas
      integer Nlayer_gas
      character*(Nchar_mx) gT_z_interpolation_mode
      character*(Nchar_mx) gT_z_extrapolation_mode
      character*(Nchar_mx) k_z_interpolation_mode
      character*(Nchar_mx) k_z_extrapolation_mode
      character*(Nchar_mx) gas_grid_file
      character*(Nchar_mx) gas_radiative_properties_file
      character*(Nchar_mx) gas_thermodynamic_properties_file
      logical produce_cloud_properties
      double precision radius_cloud
      double precision top_altitude_cloud
      integer Ntheta_cloud
      integer Nphi_cloud
      integer Nlayer_cloud
      character*(Nchar_mx) kext_z_interpolation_mode
      character*(Nchar_mx) kext_z_extrapolation_mode
      character*(Nchar_mx) cT_z_interpolation_mode
      character*(Nchar_mx) cT_z_extrapolation_mode
      character*(Nchar_mx) ssa_z_interpolation_mode
      character*(Nchar_mx) ssa_z_extrapolation_mode
      character*(Nchar_mx) ssa_lambda_interpolation_mode
      character*(Nchar_mx) ssa_lambda_extrapolation_mode
      character*(Nchar_mx) phase_function_list_file
      character*(Nchar_mx) cloud_grid_file
      character*(Nchar_mx) cloud_radiative_properties_file
      character*(Nchar_mx) phase_function_file
c     temp
      double precision radius
      logical data_dir_exists
      integer ios,j
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'
      
      open(11,file=trim(in_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(in_file)
         stop
      endif
      do j=1,8
         read(11,*)
      enddo
      read(11,*) radius  ! km
      read(11,*)
      read(11,10) data_dir
      do j=1,5
         read(11,*)
      enddo
      read(11,*) produce_surface_properties
      read(11,*)
      read(11,10) mtlidx_file
      read(11,*)
      read(11,10) grid_file_surface
      read(11,*)
      read(11,10) surface_properties_file
      do j=1,5
         read(11,*)
      enddo
      read(11,*) produce_gas_properties
      read(11,*)
      read(11,*) top_altitude_gas
      read(11,*)
      read(11,*) Ntheta_gas
      read(11,*)
      read(11,*) Nphi_gas
      read(11,*)
      read(11,*) Nlayer_gas
      read(11,*)
      read(11,*) gT_z_interpolation_mode
      read(11,*)
      read(11,*) gT_z_extrapolation_mode
      read(11,*)
      read(11,*) k_z_interpolation_mode
      read(11,*)
      read(11,*) k_z_extrapolation_mode
      read(11,*)
      read(11,10) gas_grid_file
      read(11,*)
      read(11,10) gas_radiative_properties_file
      read(11,*)
      read(11,10) gas_thermodynamic_properties_file
      do j=1,5
         read(11,*)
      enddo
      read(11,*) produce_cloud_properties
      read(11,*)
      read(11,*) top_altitude_cloud
      read(11,*)
      read(11,*) Ntheta_cloud
      read(11,*)
      read(11,*) Nphi_cloud
      read(11,*)
      read(11,*) Nlayer_cloud
      read(11,*)
      read(11,*) kext_z_interpolation_mode
      read(11,*)
      read(11,*) kext_z_extrapolation_mode
      read(11,*)
      read(11,*) cT_z_interpolation_mode
      read(11,*)
      read(11,*) cT_z_extrapolation_mode
      read(11,*)
      read(11,*) ssa_z_interpolation_mode
      read(11,*)
      read(11,*) ssa_z_extrapolation_mode
      read(11,*)
      read(11,*) ssa_lambda_interpolation_mode
      read(11,*)
      read(11,*) ssa_lambda_extrapolation_mode
      read(11,*)
      read(11,10) phase_function_list_file
      read(11,*)
      read(11,10) cloud_grid_file
      read(11,*)
      read(11,10) cloud_radiative_properties_file
      read(11,*)
      read(11,10) phase_function_file
      close(11)
c     Check
      call add_slash_if_absent(data_dir)
      inquire(file=trim(data_dir),exist=data_dir_exists)
      if (.not.data_dir_exists) then
         call error(label)
         write(*,*) 'Base directory not found: ',trim(data_dir)
         stop
      endif
      if (radius.le.0.0D+0) then
         call error(label)
         write(*,*) 'radius=',radius
         write(*,*) 'should be > 0'
         stop
      endif
      if (produce_gas_properties) then
         if (top_altitude_gas.le.0.0D+0) then
            call error(label)
            write(*,*) 'top_altitude_gas=',top_altitude_gas
            write(*,*) 'should be > 0'
            stop
         endif
         if (Ntheta_gas.le.0) then
            call error(label)
            write(*,*) 'Ntheta_gas=',Ntheta_gas
            write(*,*) 'should be > 0'
            stop
         endif
         if (Nphi_gas.le.0) then
            call error(label)
            write(*,*) 'Nphi_gas=',Nphi_gas
            write(*,*) 'should be > 0'
            stop
         endif
         if (Nlayer_gas.le.0) then
            call error(label)
            write(*,*) 'Nlayer_gas=',Nlayer_gas
            write(*,*) 'should be > 0'
            stop
         endif
      endif                     ! produce_gas_properties
      if (produce_cloud_properties) then
         if (top_altitude_cloud.le.0.0D+0) then
            call error(label)
            write(*,*) 'top_altitude_cloud=',top_altitude_cloud
            write(*,*) 'should be > 0'
            stop
         endif
         if (Ntheta_cloud.le.0) then
            call error(label)
            write(*,*) 'Ntheta_cloud=',Ntheta_cloud
            write(*,*) 'should be > 0'
            stop
         endif
         if (Nphi_cloud.le.0) then
            call error(label)
            write(*,*) 'Nphi_cloud=',Nphi_cloud
            write(*,*) 'should be > 0'
            stop
         endif
         if (Nlayer_cloud.le.0) then
            call error(label)
            write(*,*) 'Nlayer_cloud=',Nlayer_cloud
            write(*,*) 'should be > 0'
            stop
         endif
      endif                     ! produce_cloud_properties
c     Conversions
      radius=radius*1.0D+3      ! km -> m
      if (produce_surface_properties) then
         planet_radius=radius
      endif                     ! produce_surface_properties
      if (produce_gas_properties) then
         radius_gas=radius ! m
         top_altitude_gas=top_altitude_gas*1.0D+3 ! km -> m
      endif                     ! produce_gas_properties
      if (produce_cloud_properties) then
         radius_cloud=radius ! m
         top_altitude_cloud=top_altitude_cloud*1.0D+3 ! km -> m
      endif                     ! produce_cloud_properties

      return
      end
