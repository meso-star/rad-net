      subroutine record_planetEMC_grid_file(grid_file,Ntheta,Nphi,Nz,Nb,Nq,Nzones,lambda_gas_lo,lambda_gas_hi,w_quad)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'planetEMC.inc'
c     
c     Purpose: to record planet_EMC grid input file
c     
c     Input:
c       + grid_file; grid file to record
c       + Ntheta: number of latitude intervals
c       + Nphi: number of longitude intervals
c       + Nz: number of layers per column
c       + Nb: number of narrowbands
c       + Nq: quadrature order
c       + Nzones: number of special emissivity zones
c       + theta: latitude array [rad]; requirements:
c         - theta(0)=-pi/2
c         - theta(i)=theta(i-1)+Delta(theta)
c         - theta(Ntheta)=pi/2
c       + phi: longitude array [rad]
c         - phi(0)=0
c         - phi(i)=phi(i-1)+Delta(phi)
c         - phi(Nphi)=2pi
c       + z: altitude array [m] with reference ground altitude = 0; requirements:
c         - z(0)=0
c         - z(i) > z(i-1)
c       + z_mc: altitude of mass center for each cell [m]
c       + Tground: temperature of the ground [K]
c       + Tgas: temperature of the gas [K]
c       + Tspace: temperature of space [K]
c       + lambda_gas_lo: lower wavelength of each spectral interval [µm]
c       + lambda_gas_hi: higher wavelength of each spectral interval [µm]
c       + w_quad: quadrature weights
c       + ground_emissivity: emissivity of the ground
c       + space_emissivity: emissivity of space
c       + emissivity_zone: angular limits of each zone [rad]
c       + emissivity: emssivity in zones
c       + n_ref: gas refraction index
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c     
c     Output: the required "grid_file"
c     
c     I/O
      character*(Nchar_mx) grid_file
      integer Ntheta
      integer Nphi
      integer Nz(1:Ntheta_mx,1:Nphi_mx)
      integer Nb
      integer Nq
      integer Nzones
      double precision lambda_gas_lo(1:Nb_mx)
      double precision lambda_gas_hi(1:Nb_mx)
      double precision w_quad(1:Nq_mx)
c     temp
      integer irec
      integer itheta,iphi,ir,band,quad,izone,j
      double precision sum_w
c     label
      character*(Nchar_mx) label
      label='subroutine record_planetEMC_grid_file'

c     ===========================================================================================
c     check everything
c     ===========================================================================================
c     Latitude grid
      if (Ntheta.gt.Ntheta_mx) then
         call error(label)
         write(*,*) 'Ntheta=',Ntheta
         write(*,*) '> Ntheta_mx=',Ntheta_mx
         stop
      endif
      if (dabs(theta(0)+pi/2.0D+0).ge.1.0D-8) then
         call error(label)
         write(*,*) 'theta(0)=',theta(0)
         write(*,*) 'should be equal to -pi/2=',-pi/2.0D+0
         stop
      endif
      do itheta=1,Ntheta
         if (theta(itheta).le.theta(itheta-1)) then
            call error(label)
            write(*,*) 'theta(',itheta,')=',theta(itheta)
            write(*,*) 'should be > theta(',itheta-1,')=',theta(itheta-1)
            stop
         endif
      enddo                     ! itheta
      if (dabs(theta(Ntheta)-pi/2.0D+0).ge.1.0D-8) then
         call error(label)
         write(*,*) 'theta(',Ntheta,')=',theta(Ntheta)
         write(*,*) 'should be equal to pi/2=',pi/2.0D+0
         stop
      endif
c     ===========================================================================================
c     Longitude grid
      if (Nphi.gt.Nphi_mx) then
         call error(label)
         write(*,*) 'Nphi=',Nphi
         write(*,*) '> Nphi_mx=',Nphi_mx
         stop
      endif
      if (dabs(phi(0)).ge.1.0D-8) then
         call error(label)
         write(*,*) 'phi(0)=',phi(0)
         write(*,*) 'should be equal to 0'
         stop
      endif
      do iphi=1,Nphi
         if (phi(iphi).le.phi(iphi-1)) then
            call error(label)
            write(*,*) 'phi(',iphi,')=',phi(iphi)
            write(*,*) 'should be > phi(',iphi-1,')=',phi(iphi-1)
            stop
         endif
      enddo                     ! iphi
      if (dabs(phi(Nphi)-2.0D+0*pi).ge.1.0D-8) then
         call error(label)
         write(*,*) 'phi(',Nphi,')=',phi(Nphi)
         write(*,*) 'should be equal to 2pi='
         stop
      endif
c     ===========================================================================================
c     Band intervals
      if (Nb.gt.Nb_mx) then
         call error(label)
         write(*,*) 'Nb=',Nb
         write(*,*) '> Nb_mx=',Nb_mx
         stop
      endif
      do band=1,Nb
         if (lambda_gas_lo(band).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda_gas_lo(',band,')=',lambda_gas_lo(band)
            write(*,*) 'should be > 0'
            stop
         endif
         if (lambda_gas_hi(band).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda_gas_hi(',band,')=',lambda_gas_hi(band)
            write(*,*) 'should be > 0'
            stop
         endif
         if (lambda_gas_hi(band).lt.lambda_gas_lo(band)) then
            call error(label)
            write(*,*) 'lambda_gas_hi(',band,')=',lambda_gas_hi(band)
            write(*,*) 'should be > lambda_gas_lo(',band,')=',lambda_gas_lo(band)
            stop
         endif
         if (band.lt.Nb) then
            if (lambda_gas_hi(band).ne.(lambda_gas_lo(band+1))) then
               call error(label)
               write(*,*) 'lambda_gas_lo(',band,')=',lambda_gas_lo(band)
               write(*,*) 'lambda_gas_hi(',band,')=',lambda_gas_hi(band)
               write(*,*) 'should be equal'
               stop
            endif
         endif
      enddo                     ! band
      sum_w=0.0D+0
      do quad=1,Nq
         sum_w=sum_w+w_quad(quad)
      enddo                     ! quad
      if (dabs(sum_w-1.0D+0).gt.1.0D-8) then
         do quad=1,Nq
            w_quad(quad)=w_quad(quad)/sum_w
         enddo                  ! quad
      endif
c     ===========================================================================================
c     Vertical grid
      do itheta=1,Ntheta
         do iphi=1,Nphi
            if (Nz(itheta,iphi).gt.Nlay_mx) then
               call error(label)
               write(*,*) 'Nz(',itheta,',',iphi,')=',Nz(itheta,iphi)
               write(*,*) '> Nlay_mx=',Nlay_mx
               stop
            endif
            if (z(0,itheta,iphi).ne.0.0D+0) then
               call error(label)
               write(*,*) 'z(0,',itheta,',',iphi,')=',z(0,itheta,iphi)
               write(*,*) 'should be equal to 0'
               stop
            endif
            do ir=1,Nz(itheta,iphi)
               if (z(ir,itheta,iphi).lt.z(ir-1,itheta,iphi)) then
                  call error(label)
                  write(*,*) 'z(',ir,',',itheta,',',iphi,')=',z(ir,itheta,iphi)
                  write(*,*) 'should be > z(',ir-1,',',itheta,',',iphi,')=',z(ir-1,itheta,iphi)
                  stop
               endif
               if (ir.gt.1) then
                  if (z_mc(ir,itheta,iphi).lt.z_mc(ir-1,itheta,iphi)) then
                     call error(label)
                     write(*,*) 'z_mc(',ir,',',itheta,',',iphi,')=',z_mc(ir,itheta,iphi)
                     write(*,*) 'should be > z_mc(',ir-1,',',itheta,',',iphi,')=',z_mc(ir-1,itheta,iphi)
                     stop
                  endif
               endif
            enddo               ! i
         enddo                  ! iphi
      enddo                     ! itheta
c     ===========================================================================================
c     emissivity
      do itheta=1,Ntheta
         do iphi=1,Nphi
            do band=1,Nb
               if ((ground_emissivity(itheta,iphi,band).lt.0.0D+0).or.(ground_emissivity(itheta,iphi,band).gt.1.0D+0)) then
                  call error(label)
                  write(*,*) 'ground_emissivity(',itheta,',',iphi,',',band,')=',ground_emissivity(itheta,iphi,band)
                  write(*,*) 'should be in the [0,1] range'
                  stop
               endif
               if ((space_emissivity(itheta,iphi,band).lt.0.0D+0).or.(space_emissivity(itheta,iphi,band).gt.1.0D+0)) then
                  call error(label)
                  write(*,*) 'space_emissivity(',itheta,',',iphi,',',band,')=',space_emissivity(itheta,iphi,band)
                  write(*,*) 'should be in the [0,1] range'
                  stop
               endif
            enddo               ! band
         enddo                  ! iphi
      enddo                     ! itheta
c     ===========================================================================================
c     Temperature field
      do itheta=1,Ntheta
         do iphi=1,Nphi
            if (Tground(itheta,iphi).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Tground(',itheta,',',iphi,')=',Tground(itheta,iphi)
               write(*,*) 'should be positive'
               stop
            endif
            if (Tspace(itheta,iphi).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Tspace(',itheta,',',iphi,')=',Tspace(itheta,iphi)
               write(*,*) 'should be positive'
               stop
            endif
            do ir=1,Nz(itheta,iphi)
               if (Tgas(ir,itheta,iphi).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'Tgas(',ir,',',itheta,',',iphi,')=',Tgas(ir,itheta,iphi)
                  write(*,*) 'should be positive'
                  stop
               endif
            enddo               ! i
         enddo                  ! iphi
      enddo                     ! itheta
c     ===========================================================================================
c     Volumic radiative properties
      do itheta=1,Ntheta
         do iphi=1,Nphi
            do ir=1,Nz(itheta,iphi)
               do band=1,Nb
                  if (n_ref(ir,itheta,iphi,band).lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'n_ref(',ir,',',itheta,',',iphi,',',band,')=',n_ref(ir,itheta,iphi,band)
                     write(*,*) 'should be positive'
                     stop
                  endif
                  if (ks_gas(ir,itheta,iphi,band).lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'ks_gas(',ir,',',itheta,',',iphi,',',band,')=',ks_gas(ir,itheta,iphi,band)
                     write(*,*) 'should be positive'
                     stop
                  endif
                  do quad=1,Nq
                     if (ka_gas(ir,itheta,iphi,band,quad).lt.0.0D+0) then
                        call error(label)
                        write(*,*) 'ka_gas(',ir,',',itheta,',',iphi,',',band,',',quad,')=',ka_gas(ir,itheta,iphi,band,quad)
                        write(*,*) 'should be positive'
                        stop
                     endif
                  enddo         ! quad
               enddo            ! band
            enddo               ! ir
         enddo                  ! iphi
      enddo                     ! itheta

c     ===========================================================================================
c     Record the grid file
c     ===========================================================================================
      irec=0
      open(11,file=trim(grid_file),form='unformatted',access='direct',recl=8)
c     Number of cells along the vertical, along the latitude, along the longitude,
c     number of gas narrowband spectral intervals, quadrature order, 
c     number of special emissivity zones
      irec=irec+1
      write(11,rec=irec) Ntheta
      irec=irec+1
      write(11,rec=irec) Nphi
      irec=irec+1
      write(11,rec=irec) Nb
      irec=irec+1
      write(11,rec=irec) Nq
      irec=irec+1
      write(11,rec=irec) Nzones
      do itheta=1,Ntheta
         do iphi=1,Nphi
            irec=irec+1
            write(11,rec=irec) Nz(itheta,iphi)
c     limits in terms of radiuses
            do ir=0,Nz(itheta,iphi)
               irec=irec+1
               write(11,rec=irec) z(ir,itheta,iphi) ! m
            enddo ! ir
         enddo ! iphi
      enddo ! itheta
c     limits in latitude
      do itheta=0,Ntheta
         irec=irec+1
         write(11,rec=irec) theta(itheta) ! rad
      enddo ! itheta
c     limits in longitude
      do iphi=0,Nphi
         irec=irec+1
         write(11,rec=irec) phi(iphi) ! rad
      enddo ! iphi
c     altitudes of mass center
      do itheta=1,Ntheta
         do iphi=1,Nphi
            do ir=1,Nz(itheta,iphi)
               irec=irec+1
               write(11,rec=irec) z_mc(ir,itheta,iphi) ! m
            enddo ! ir
         enddo ! iphi
      enddo ! itheta
c     temperature profiles
      do itheta=1,Ntheta
         do iphi=1,Nphi
            irec=irec+1
            write(11,rec=irec) Tground(itheta,iphi) ! K
            do ir=0,Nz(itheta,iphi)+1
               irec=irec+1
               write(11,rec=irec) Tgas(ir,itheta,iphi) ! K
            enddo ! ir
            irec=irec+1
            write(11,rec=irec) Tspace(itheta,iphi) ! K
         enddo ! iphi
      enddo ! itheta
c     spectral stuff:
c     lower limits of gas data narrowband intervals
      do band=1,Nb
         irec=irec+1
         write(11,rec=irec) lambda_gas_lo(band) ! µm
      enddo ! band
c     higher limits of gas data narrowband intervals
      do band=1,Nb
         irec=irec+1
         write(11,rec=irec) lambda_gas_hi(band) ! µm
      enddo ! band
c     quadrature weights
      do quad=1,Nq
         irec=irec+1
         write(11,rec=irec) w_quad(quad)
      enddo ! quad
c     emissivities...
      do itheta=1,Ntheta
         do iphi=1,Nphi
c     ... of ground
            do band=1,Nb
               irec=irec+1
               write(11,rec=irec) ground_emissivity(itheta,iphi,band)
            enddo ! band
c     ... and of space
            do band=1,Nb
               irec=irec+1
               write(11,rec=irec) space_emissivity(itheta,iphi,band)
            enddo ! band
         enddo ! iphi
      enddo ! itheta
c     absorption and scattering coefficients, for each gas cell, and
c     for each narrowband interval / quadrature element
      do itheta=1,Ntheta
         do iphi=1,Nphi
            do ir=1,Nz(itheta,iphi)
               do band=1,Nb
                  do quad=1,Nq
                     irec=irec+1
                     write(11,rec=irec) ka_gas(ir,itheta,iphi,band,quad) ! m⁻¹
                  enddo ! quad
                  do quad=1,Nq
                     irec=irec+1
                     write(11,rec=irec) ks_gas(ir,itheta,iphi,band) ! m⁻¹
                  enddo! quad
               enddo            ! band
            enddo               ! ir
         enddo                  ! iphi
      enddo                     ! itheta
c     special emissivity zones data:
c     spatial extension
      do izone=1,Nzones
         do j=1,4
            irec=irec+1
            write(11,rec=irec) emissivity_zone(izone,j) ! rad
         enddo ! j
      enddo
c     and values of emissivity, for each gas narrowband spectral interval
      do izone=1,Nzones
         do band=1,Nb
            irec=irec+1
            write(11,rec=irec) emissivity(izone,band)
         enddo ! band
      enddo
c     refraction indexes
      do itheta=1,Ntheta
         do iphi=1,Nphi
            do band=1,Nb
               do ir=1,Nz(itheta,iphi)
                  irec=irec+1
                  write(11,rec=irec) n_ref(ir,itheta,iphi,band)
               enddo ! ir
            enddo ! band
         enddo ! iphi
      enddo ! itheta
 666  continue
      close(11)
      write(*,*) 'File was recorded: ',trim(grid_file)

      return
      end
