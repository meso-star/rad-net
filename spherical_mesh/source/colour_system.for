c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine colour_system(cs,
     &     xred,yred,xgreen,ygreen,xblue,yblue,xw,yw)
      implicit none
      include 'max.inc'
c     
c     Purpose: provide various information about the colour system of choice
c     
c     Input:
c       + cs: colour system; must be one of the following:
c     NTSC
c     EBU
c     SMPTE
c     HDTV
c     CIE
c     Rec709
c     
c     Output:
c       + xred: X component of red
c       + yred: Y component of red
c       + xgreen: X component of green
c       + ygreen: Y component of green
c       + xblue: X component of blue
c       + yblue: Y component of blue
c       + xw: X component of the white point
c       + yw: Y component of the white point
c     
c     I/O
      character*(Nchar_mx) cs
      double precision xred,yred
      double precision xgreen,ygreen
      double precision xblue,yblue
      double precision xw,yw
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine colour_system'

      if (trim(cs).eq.'NTSC') then
         xred=0.67D+0
         yred=0.33D+0
         xgreen=0.21D+0
         ygreen=0.71D+0
         xblue=0.14D+0
         yblue=0.08D+0
         xw=0.3101D+0
         yw=0.3162D+0
      else if (trim(cs).eq.'EBU') then
         xred=0.64D+0
         yred=0.33D+0
         xgreen=0.29D+0
         ygreen=0.60D+0
         xblue=0.15D+0
         yblue=0.06D+0
         xw=0.3127D+0
         yw=0.3291D+0
      else if (trim(cs).eq.'SMPTE') then
         xred=0.63D+0
         yred=0.34D+0
         xgreen=0.31D+0
         ygreen=0.595D+0
         xblue=0.155D+0
         yblue=0.07D+0
         xw=0.3127D+0
         yw=0.3291D+0
      else if (trim(cs).eq.'HDTV') then
         xred=0.67D+0
         yred=0.33D+0
         xgreen=0.21D+0
         ygreen=0.71D+0
         xblue=0.15D+0
         yblue=0.06D+0
         xw=0.3127D+0
         yw=0.3291D+0
      else if (trim(cs).eq.'CIE') then
         xred=0.7355D+0
         yred=0.2645D+0
         xgreen=0.2658D+0
         ygreen=0.7243D+0
         xblue=0.1669D+0
         yblue=0.0085D+0
         xw=0.333333333333D+0
         yw=0.333333333333D+0
      else if (trim(cs).eq.'Rec709') then
         xred=0.64D+0
         yred=0.33D+0
         xgreen=0.30D+0
         ygreen=0.60D+0
         xblue=0.15D+0
         yblue=0.060D+0
         xw=0.3127D+0
         yw=0.3290D+0
      else
         call error(label)
         write(*,*) 'cs=',trim(cs)
         write(*,*) 'is not defined'
         stop
      endif

      return
      end
