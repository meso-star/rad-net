c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine add_contour(Ncontour,Np,contour,
     &     Npoints,track)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a given contour to a list of contours
c
c     Input:
c       + Ncontour: number of contours in the existing list of contours
c       + Np: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + Npoints: number of points for the contour to add
c       + track: track for the contour to add
c
c     Output:
c     updated values of "Ncontour", "Np" and "contour"
c
c     I/O
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_contour'

      Ncontour=Ncontour+1
      if (Ncontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
      Np(Ncontour)=Npoints
      do i=1,Np(Ncontour)
         do j=1,2
            contour(Ncontour,i,j)=track(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine get_contour(Ncontour,Np,contour,icontour,
     &     Npoints,track)
      implicit none
      include 'max.inc'
c
c     Purpose: to extract a given contour from a list of contours
c
c     Input:
c       + Ncontour: number of contours in the existing list of contours
c       + Np: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + icontour: index of the contour to extract
c     
c     Output:
c       + Npoints: number of points for the extracted contour
c       + track: track for the extracted contour
c
c     I/O
      integer Ncontour
      integer Np(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:2)
      integer icontour
      integer Npoints
      double precision track(1:Nppt_mx,1:2)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine get_contour'

      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'while Ncontour=',Ncontour
         stop
      endif

      Npoints=Np(icontour)
      do i=1,Npoints
         do j=1,2
            track(i,j)=contour(icontour,i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end
      


      subroutine get_position_from_ctr(dim,Npoints,ctr,i,position)
      implicit none
      include 'max.inc'
c     
c     Purpose: to extract a position from a contour
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the contour
c       + ctr: contour
c       + i: index of the position to extract
c     
c     Output:
c       + position: required position
c     
c     I/O
      integer dim
      integer Npoints
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i
      double precision position(1:Ndim_mx)
c     temp
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine get_position_from_ctr'

      if (i.lt.1) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be > 0'
         stop
      endif
      if (i.gt.Npoints) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be <= Npoints=',Npoints
         stop
      endif

      do j=1,dim-1
         position(j)=ctr(i,j)
      enddo                     ! j
      position(dim)=0.0D+0
      
      return
      end
      


      subroutine set_position_in_ctr(dim,Npoints,ctr,i,position)
      implicit none
      include 'max.inc'
c     
c     Purpose: to set a position in a contour
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the contour
c       + ctr: contour
c       + i: index of the position to set
c     
c     Output:
c       + position: position to set
c     
c     I/O
      integer dim
      integer Npoints
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i
      double precision position(1:Ndim_mx)
c     temp
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine get_position_from_ctr'

      if (i.lt.1) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be > 0'
         stop
      endif
      if (i.gt.Npoints) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be <= Npoints=',Npoints
         stop
      endif

      do j=1,dim-1
         ctr(i,j)=position(j)
      enddo                     ! j
      
      return
      end

      
      
      subroutine add_point_to_ctr(dim,x,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to add a position to a contour definition
c     
c     Input:
c       + dim: dimension of space
c       + x: position to add
c       + Np: number of points in the "ctr" contour
c       + ctr: definition of the contour
c     
c     Output:
c       + updated values for Np and ctr
c     
c     I/O
      integer dim
      double precision x(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer j
      double precision x0(1:Ndim_mx)
      logical check,identical
c     label
      character*(Nchar_mx) label
      label='subroutine add_point_to_ctr'

      if (Np.eq.0) then
         check=.false.
      else
         check=.true.
         do j=1,dim-1
            x0(j)=ctr(Np,j)
         enddo                  ! j
         x0(dim)=0.0D+0
      endif                     ! Np>0
      if (check) then
         call identical_positions(dim,x0,x,identical)
      endif
      if ((.not.check).or.((check).and.(.not.identical))) then
         Np=Np+1
         if (Np.gt.Nppc_mx) then
            call error(label)
            write(*,*) 'Np=',Np
            write(*,*) '> Nppc_mx=',Nppc_mx
            stop
         else
            do j=1,dim-1
               ctr(Np,j)=x(j)
            enddo               ! j
         endif                  ! Np>Nppc_mx
      endif                     ! .not.identical

      return
      end
      


      subroutine contour_length(dim,Nppc,ctr,length)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the length of a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points for the trak that defines the contour
c       + ctr: ctr
c     
c     Output:
c       + length: length of the contour
c     
c     I/O
      integer dim
      integer Nppc
      double precision ctr(1:Nppt_mx,1:2)
      double precision length
c     temp
      integer Nseg,iseg,i1,i2,j
      double precision d
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine contour_length'

      length=0.0D+0
      Nseg=Nppc
      do iseg=1,Nseg
         i1=iseg
         if (iseg.eq.Nseg) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,tmp)
         call vector_length(dim,tmp,d)
         length=length+d
      enddo                     ! iseg

      return
      end
      


      subroutine identify_smallest_segment(dim,Nppc,ctr,
     &     dmin,iseg_dmin,i01,i02,dop)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the index of the smallest segment
c     of a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points for the trak that defines the contour
c       + ctr: ctr
c     
c     Output:
c       + dmin: length of the smallest segment
c       + iseg_dmin: index of the smallest segment
c       + i01 & i02: indexes of the two points for segment index "iseg_dmin"
c       + dop: length of the segment opposite to the smallest segment
c     
c     I/O
      integer dim
      integer Nppc
      double precision ctr(1:Nppt_mx,1:2)
      double precision dmin
      integer iseg_dmin
      integer i01,i02
      double precision dop
c     temp
      integer Nseg,iseg,j,iseg_op,i1,i2
      double precision d
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine identify_smallest_segment'

      Nseg=Nppc
      do iseg=1,Nseg
         i1=iseg
         if (iseg.eq.Nseg) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,tmp)
         call vector_length(dim,tmp,d)
c     Debug
         if (d.le.0.0D+0) then
            call error(label)
            write(*,*) 'd=',d
            write(*,*) 'Nseg=',Nseg
            write(*,*) 'iseg=',iseg
            write(*,*) 'i1=',i1,' i2=',i2
            write(*,*) 'x1=',x1
            write(*,*) 'x2=',x2
            stop
         endif
c     Debug
         if (iseg.eq.1) then
            dmin=d
            iseg_dmin=iseg
         else
            if (d.lt.dmin) then
               dmin=d
               iseg_dmin=iseg
            endif               ! d < dmin
         endif                  ! iseg=1
      enddo                     ! iseg
      i01=iseg_dmin
      if (iseg_dmin.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif

      iseg_op=iseg_dmin+Nseg/2
      if (iseg_op.gt.Nseg) then
         iseg_op=iseg_op-Nseg
      endif
      i1=iseg_op
      if (iseg_op.eq.Nseg) then
         i2=1
      else
         i2=i1+1
      endif
      do j=1,dim-1
         x1(j)=ctr(i1,j)
         x2(j)=ctr(i2,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      call substract_vectors(dim,x2,x1,tmp)
      call vector_length(dim,tmp,dop)
      
      return
      end



      subroutine identify_close_segments(dim,Nppc,ctr,iseg,
     &     iseg1,iseg2,i11,i12,i21,i22)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two surrounding segments for
c     a given segment of a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points for the trak that defines the contour
c       + ctr: ctr
c       + iseg: required segment index
c     
c     Output:
c       + iseg1: index of the segment just before "iseg"
c       + iseg2: index of the segment just after "iseg"
c       + i11 & i12: indexes of the first and second point for segment "iseg1"
c       + i21 & i22: indexes of the first and second point for segment "iseg2"
c     
c     I/O
      integer dim
      integer Nppc
      double precision ctr(1:Nppt_mx,1:2)
      integer iseg
      integer iseg1
      integer iseg2
      integer i11,i12
      integer i21,i22
c     temp
      integer Nseg,i1,i2
c     label
      character*(Nchar_mx) label
      label='subroutine identify_close_segments'

      Nseg=Nppc
c     + previous segment
      if (iseg.eq.1) then
         iseg1=Nseg
      else
         iseg1=iseg-1
      endif
      i11=iseg1
      if (iseg1.eq.Nseg) then
         i12=1
      else
         i12=i11+1
      endif
      if (iseg.eq.Nseg) then
         iseg2=1
      else
         iseg2=iseg+1
      endif
      i21=iseg2
      if (iseg2.eq.Nseg) then
         i22=1
      else
         i22=i21+1
      endif
      
      return
      end


      
      subroutine add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &     Npoints,ctr)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a given contour to a list of contours
c
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + Npoints: number of points for the contour to add
c       + ctr: contour to add
c
c     Output:
c     updated values of "Ncontour", "Np" and "contour"
c
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppt_mx,1:Ndim_mx-1)
      integer Npoints
      double precision ctr(1:Nppt_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_ctr_to_contour'

      Ncontour=Ncontour+1
      if (Ncontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
c      
      if (Npoints.gt.Nppt_mx) then
         call error(label)
         write(*,*) 'Npoints=',Npoints
         write(*,*) '> Nppt_mx=',Nppt_mx
         stop
      else
         Nppc(Ncontour)=Npoints
      endif
c      
      do i=1,Nppc(Ncontour)
         do j=1,dim-1
            contour(Ncontour,i,j)=ctr(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end


      
      subroutine is_contour_valid(dim,Np_ref,ctr_ref,Np,ctr,
     &     print_err,test_inside_positions,test_self_intersection,
     &     test_intersection,test_crossed_intersection,test_clockwise,
     &     valid)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find whether or not a given contour is valid, relatively
c     to a reference contour.
c     
c     Input:
c       + dim: dimension of space
c       + Np_ref: number of points that define the reference contour
c       + ctr_ref: reference contour
c       + Np: number of points that define the contour to test
c       + ctr: contour to test
c       + print_err: true if origin of failure has to be displayed
c       + test_inside_positions: T when each position of "ctr" has to be tested as inside "ctr_ref"
c       + test_self_intersection: T when "ctr" has to be tested for self-intersection
c       + test_intersection: T when "ctr" has to be tested for intersecting "ctr_ref"
c       + test_crossed_intersection: T when each segment constituted by a point of "ctr_ref"
c     and its image in "ctr" has to NOT intersect "ctr"
c       + test_clockwise: T when "ctr" has to be tested for rotation direction
c     
c     Output:
c       + valid: T when "ctr" is valid
c     
c     I/O
      integer dim
      integer Np_ref
      double precision ctr_ref(1:Nppc_mx,1:Ndim_mx-1)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical print_err
      logical test_inside_positions
      logical test_self_intersection
      logical test_intersection
      logical test_crossed_intersection
      logical test_clockwise
      logical valid
c     temp
      integer i,j
      double precision P(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical inside,intersection_found,clockwise
c     label
      character*(Nchar_mx) label
      label='subroutine is_contour_valid'

      valid=.true.
c
c     prerequisite: number of points should be identical
      if (Np_ref.ne.Np) then
         valid=.false.
         goto 666
      endif
c     
      if (test_inside_positions) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_ref,ctr_ref)
         do i=1,Np
            call get_position_from_ctr(dim,Np,ctr,i,P)
            call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,
     &           1,P,inside)
            if (.not.inside) then
               valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed inside_positions'
               endif
               goto 666
            endif               ! inside=F
         enddo                  ! i
      endif                     ! test_inside_positions
c
      if (test_self_intersection) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np,ctr)
         call contour_self_intersects(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour,Nppc,contour,1,
     &        intersection_found)
         if (intersection_found) then
            valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed self_intersects'
               endif
            goto 666
         endif                  ! intersection_found
      endif                     ! test_self_intersection
c
      if (test_intersection) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_ref,ctr_ref)
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np,ctr)
         call contours_intersect(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour,Nppc,contour,1,2,
     &        intersection_found)
         if (intersection_found) then
            valid=.false.
            if (print_err) then
               call error(label)
               write(*,*) 'contour failed intersection'
            endif
            goto 666
         endif                  ! intersection_found
      endif                     ! test_intersection
c
      if (test_crossed_intersection) then
         do i=1,Np_ref
            call get_position_from_ctr(dim,Np_ref,ctr_ref,i,P1)
            call get_position_from_ctr(dim,Np,ctr,i,P2)
            call segment_intersects_contour(dim,P1,P2,Np,ctr,
     &           intersection_found,Pint)
            if (intersection_found) then
               valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed crossed_intersection'
               endif
               goto 666
            endif               ! intersection_found
         enddo                  ! i
      endif                     ! test_crossed_intersection
c
      if (test_clockwise) then
         call is_contour_clockwise2(dim,Np,ctr,clockwise)
         if (clockwise) then
            valid=.false.
            if (print_err) then
               call error(label)
               write(*,*) 'contour failed clockwise'
            endif
            goto 666
         endif                  ! clockwise
      endif                     ! test_clockwise
c     
 666  continue
      return
      end
      


      subroutine is_contour_clockwise(dim,Np,ctr,clockwise)
      implicit none
      include 'max.inc'
c     
c     Purpose: find whether or not the provided contour
c     is clockwise (which is really bad)
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c     
c     Output:
c       + clockwise: true if contour is clockwise
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical clockwise
c     temp
      integer i,j,i1,i2,i3,Nd,Ni
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision x1x3(1:Ndim_mx)
      double precision v12(1:Ndim_mx)
      double precision v13(1:Ndim_mx)
      double precision w(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine is_contour_clockwise'

      Nd=0
      Ni=0
      do i=1,Np
         i1=i
         if (i.eq.1) then
            i3=Np
         else
            i3=i1-1
         endif
         if (i.eq.Np) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
            x3(j)=ctr(i3,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         x3(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,x1x2)
         call substract_vectors(dim,x3,x1,x1x3)
         call normalize_vector(dim,x1x2,v12)
         call normalize_vector(dim,x1x3,v13)
         call vector_product_normalized(dim,v12,v13,w)
         if (w(3).gt.0.0D+0) then
            Nd=Nd+1
         else
            Ni=Ni+1
         endif
      enddo                     ! i
c
      if (Nd.ge.Ni) then
         clockwise=.false.
      else
         clockwise=.true.
      endif
c     
      return
      end


      
      subroutine is_contour_clockwise2(dim,Np,ctr,clockwise)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: find whether or not the provided contour
c     is clockwise (which is really bad)
c     
c     This version is based in the orgininal one, and was modified
c     by T. Nagel
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c     
c     Output:
c       + clockwise: true if contour is clockwise
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical clockwise
c     temp
      integer i,j,i1,i2,i3
      double precision lx1x2,lx1x3,in_angle,det,angle,pol_angle,prod
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision x1x3(1:Ndim_mx)
      double precision w(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine is_contour_clockwise2'
      
      pol_angle=0.
      do i=1,Np
         i1=i
         if (i.eq.1) then
            i3=Np
         else
            i3=i1-1
         endif
         if (i.eq.Np) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
            x3(j)=ctr(i3,j)
         enddo                  ! j
c         
         call substract_vectors(dim-1,x2,x1,x1x2)
         x1x2(dim)=0.0D+0
c         call vector_length(dim,x1x2,lx1x2)
c         
         call substract_vectors(dim-1,x3,x1,x1x3)
         x1x3(dim)=0.0D+0
c         call vector_length(dim,x1x3,lx1x3)
c
         call scalar_product(dim,x1x2,x1x3,prod)
         in_angle=acos(prod)    ! rad
         in_angle=in_angle*180.0/pi ! deg
c
         call vector_product_normalized(dim,x1x2,x1x3,w)
         det=w(3)
         if (det.lt.0.0D+0) then
            angle=in_angle-180.0D+0
         else
            angle=180.0D+0-in_angle
         endif
         pol_angle=pol_angle+angle
      enddo                     ! i
c
      if (pol_angle<359.) then
         clockwise=.true.
      else
         clockwise=.false.
      endif
      
      return
      end

      
      
      subroutine segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make a segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of segment "iseg"
c       + i02: index of the second point of segment "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j
c     label
      character*(Nchar_mx) label
      label='subroutine segment_indexes'

      Nseg=Np
      i01=iseg
      if (iseg.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end
      

      
      subroutine previous_segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make the previous segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of the segment previous to "iseg"
c       + i02: index of the second point of the segment previous to "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j,iseg1
c     label
      character*(Nchar_mx) label
      label='subroutine previous_segment_indexes'

      Nseg=Np
      if (iseg.eq.1) then
         iseg1=Nseg
      else
         iseg1=iseg-1
      endif
      i01=iseg1
      if (iseg1.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end
      

      
      subroutine next_segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make the next segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of the segment next to "iseg"
c       + i02: index of the second point of the segment next to "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j,iseg1
c     label
      character*(Nchar_mx) label
      label='subroutine next_segment_indexes'

      Nseg=Np
      if (iseg.eq.Nseg) then
         iseg1=1
      else
         iseg1=iseg+1
      endif
      i01=iseg1
      if (iseg1.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end

      

      subroutine segments_intersection(dim,P0,P1,P2,inside,d,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c     
c     Puropse: to get the intersection position between the two lines
c     parallel to 2 segments of a closed contour, [P0,P1] and [P1,P2], at a distance d.
c     
c     Input:
c       + dim: dimension of space
c       + P0: point that defines the beginning of the first segment
c       + P1: point that defines the end of the first segment / beginning of the second segment
c       + P2: point that defines the end of the second segment
c       + inside: true if the new trackhas to be generated inside the closed input contour
c     
c     Output:
c       + intersection_found: true if intersection position was found
c       + Pint: intersection position between the line parallel to (P0P1) at a distance d (in direction n0)
c     and the line parallel to (P1P2) at a distance d (in direction n1), when intersection_found=.true.
c     
c     I/O
      integer dim
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      logical inside
      double precision d
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      double precision n0(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      double precision P0P1(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c      double precision n(1:Ndim_mx)
c      double precision u(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      double precision sp,alpha,beta,gamma
      double precision A(1:Ndim_mx)
      double precision B(1:Ndim_mx)
      logical identical
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine segments_intersection'

c     [P0P1]
      call substract_vectors(dim,P0,P1,P0P1)
      call normalize_vector(dim,P0P1,u0)
      call normal_for_segment(dim,P0,P1,n0)
      if (.not.inside) then
         call scalar_vector(dim,-1.0D+0,n0,n0)
      endif
c     [P1P2]
      call substract_vectors(dim,P2,P1,P1P2)
      call normalize_vector(dim,P1P2,u1)
      call normal_for_segment(dim,P1,P2,n1)
      if (.not.inside) then
         call scalar_vector(dim,-1.0D+0,n1,n1)
      endif
c     Intersection between parallel lines
      call scalar_product(dim,u0,u1,sp)
      if (sp.eq.-1.0D+0) then
         intersection_found=.false.
      else if (sp.eq.1.0D+0) then
         intersection_found=.true.
         call scalar_vector(dim,d,n1,t)
         call add_vectors(dim,p1,t,Pint)
      else
         intersection_found=.true.
         call scalar_vector(dim,d,n0,t)
         call add_vectors(dim,P0,t,A)
         call scalar_vector(dim,d,n1,t)
         call add_vectors(dim,P1,t,B)
         gamma=u1(2)*u0(1)-u1(1)*u0(2)
         if (gamma.eq.0.0D+0) then
            call error(label)
            write(*,*) 'gamma=',gamma
            write(*,*) 'should not happen at this point'
            stop
         endif
         beta=((A(2)-B(2))*u0(1)+(B(1)-A(1))*u0(2))/gamma
         if (u0(1).ne.0.0D+0) then
            alpha=(B(1)-A(1)+beta*u1(1))/u0(1)
         else
            if (u0(2).ne.0.0D+0) then
               alpha=(B(2)-A(2)+beta*u1(2))/u0(2)
            else
               write(*,*) 'u0=',(u0(j),j=1,2)
               write(*,*) 'is not normalized'
               stop
            endif
         endif
         call scalar_vector(dim,alpha,u0,t)
         call add_vectors(dim,A,t,Pint1)
         call scalar_vector(dim,beta,u1,t)
         call add_vectors(dim,B,t,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
         if (.not.identical) then
            call error(label)
            write(*,*) 'Pint1=',Pint1
            write(*,*) 'Pint2=',Pint2
            write(*,*) 'are not identical'
         else
            call copy_vector(dim,Pint1,Pint)
         endif
      endif

      return
      end

      

      subroutine segment_line_intersection(dim,P1,P2,P3,v,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c     
c     Puropse: to get the intersection position between a [P1,P2] segment 
c     and a line defined by a position P3 and a vector v
c     
c     Input:
c       + dim: dimension of space
c       + P1: point that defines the beginning of the segment
c       + P2: point that defines the end of the segment
c       + P3: point that defines the beginning of the intersecting line
c       + v: direction vector of the intersecting line
c     
c     Output:
c       + intersection_found: true if intersection position was found
c       + Pint: intersection position (if found)
c     
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      double precision alpha,beta
      double precision P1P2(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision d12
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      logical identical
c     label
      character*(Nchar_mx) label
      label='subroutine segment_line_intersection'

c     [P1P2]
      call substract_vectors(dim,P2,P1,P1P2)
      call normalize_vector(dim,P1P2,u12)
      call vector_length(dim,P1P2,d12)
c     beta: coordinate of Pint on the intersecting line
      if (u12(1)*v(2)-u12(2)*v(1).eq.0.0D+0) then
         call error(label)
         write(*,*) 'u12(1)*v(2)-u12(2)*v(1)=',u12(1)*v(2)-u12(2)*v(1)
         write(*,*) 'line (P3,v) is probably parallel to [P1P2]'
         stop
      else
         beta=((P3(1)-P1(1))*u12(2)-(P3(2)-P1(2))*u12(1))
     &        /(u12(1)*v(2)-u12(2)*v(1))
         if (u12(1).ne.0.0D+0) then
            alpha=(P3(1)-P1(1)+beta*v(1))/u12(1)
         else
            if (u12(2).eq.0.0D+0) then
               call error(label)
               write(*,*) 'u12(1)=',u12(1)
               write(*,*) 'u12(2)=',u12(2)
               write(*,*) 'should not both be null'
               stop
            else
               alpha=(P3(2)-P1(2)+beta*v(2))/u12(2)
            endif
         endif
         intersection_found=.false.
         call scalar_vector(dim,alpha,u12,t)
         call add_vectors(dim,P1,t,Pint1)
         call scalar_vector(dim,beta,v,t)
         call add_vectors(dim,P3,t,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
         if (identical) then
            intersection_found=.true.
            call copy_vector(dim,Pint1,Pint)
         endif
      endif

      return
      end      

      
      
      subroutine duplicate_ctr(dim,Np,ctr,Np2,ctr2)
      implicit none
      include 'max.inc'
c
c     Purpose: to duplicate a contour
c
c     Input:
c       + dim: dimension of space
c       + Np: number of points for the contour du duplicate
c       + ctr: contour tp duplicate
c     
c     Output:
c       + Np2: number of points for the duplicated contour
c       + ctr2: duplicated contour
c
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine duplicate_ctr'

      Np2=Np
      do i=1,Np2
         do j=1,dim-1
            ctr2(i,j)=ctr(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end
      

      
      subroutine segment_rotation_angle(dim,P1P2,theta)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to compute the horizontal roation angle for segment [P1P2]
c     
c     Input:
c       + dim: dimension of space
c       + P1P2: P1PZ vector
c     
c     Output:
c     + theta: horizontal rotation angle
c     
c     I/O
      integer dim
      double precision P1P2(1:Ndim_mx)
      double precision theta
c     temp
      double precision a
c     label
      character*(Nchar_mx) label
      label='subroutine segment_rotation_angle'

      if (P1P2(1).eq.0.0D+0) then
         a=pi/2.0D+0
      else
         a=dabs(datan(P1P2(2)/P1P2(1)))
      endif
      if ((P1P2(1).ge.0.0).and.(P1P2(2).ge.0.0D+0)) then
         theta=a
      else if ((P1P2(1).le.0.0).and.(P1P2(2).ge.0.0D+0)) then
         theta=pi-a
      else if ((P1P2(1).le.0.0).and.(P1P2(2).le.0.0D+0)) then
         theta=pi+a
      else if ((P1P2(1).ge.0.0).and.(P1P2(2).le.0.0D+0)) then
         theta=2.0*pi-a
      endif
      
      return
      end
      


      subroutine is_inside_contour(debug,dim,Ncontour,Nppc,contour,
     &     icontour,P,inside)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find whether or not a given position is inside a contour
c     
c     Input:
c       + debug: true for debug mode
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: contour for the existing list of contours
c       + icontour: index of the contour
c       + P: position to test
c     
c     Output:
c       + inside: true if "P" is inside contour index "icontour"
c     
c     I/O
      logical debug
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      double precision P(1:Ndim_mx)
      logical inside
c     temp
      integer Nsegments,isegment,Npoints,i1,i2
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision d,dmin
      integer isegment_dmin,iseg2
      double precision A(1:Ndim_mx)
      double precision AP(1:Ndim_mx)
      double precision nAP(1:Ndim_mx)
      double precision A_dmin(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision sp
      logical A_is_at_end,A_is_at_end_dmin
      integer end_side,end_side_dmin
c     label
      character*(Nchar_mx) label
      label='subroutine is_inside_contour'

c     Debug
      if (debug) then
         write(*,*) trim(label),' :in'
         write(*,*) 'icontour=',icontour
         write(*,*) 'P=',P
      endif                     ! debug
c     Debug
      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be <= Ncontour=',Ncontour
         stop
      endif

      call get_contour(Ncontour,Nppc,contour,icontour,
     &     Npoints,ctr)
c     find segment "P" is the closest from
      Nsegments=Npoints
c     Debug
      if (debug) then
         write(*,*) 'Nsegments=',Nsegments
      endif                     ! debug
c     Debug
      do isegment=1,Nsegments
         i1=isegment
         call get_position_from_ctr(dim,Npoints,ctr,i1,x1)
         if (isegment.eq.Nsegments) then
            i2=1
         else
            i2=isegment+1
         endif
         call get_position_from_ctr(dim,Npoints,ctr,i2,x2)
         call distance_to_vector(dim,x1,x2,P,d,A,A_is_at_end,end_side)
c     Debug
      if (debug) then
         write(*,*) 'isegment=',isegment
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'd=',d
      endif                     ! debug
c     Debug
         if (isegment.eq.1) then
            dmin=d
            isegment_dmin=1
            call copy_vector(dim,A,A_dmin)
            A_is_at_end_dmin=A_is_at_end
            end_side_dmin=end_side
         endif
         if (d.le.dmin) then
            dmin=d
            isegment_dmin=isegment
            call copy_vector(dim,A,A_dmin)
            A_is_at_end_dmin=A_is_at_end
            end_side_dmin=end_side
         endif
         if (d.le.0.0D+0) then  ! point P is on contour: inside=T
            inside=.true.
            goto 666
         endif
      enddo                     ! i
c     Debug
      if (debug) then
         write(*,*) 'isegment_dmin=',isegment_dmin
         write(*,*) 'dmin=',dmin
         write(*,*) 'A_dmin=',A_dmin
      endif                     ! debug
c     Debug
c     
      call normal_to_segment(dim,Ncontour,Nppc,contour,
     &     icontour,isegment_dmin,normal)
c     vector AP
      call substract_vectors(dim,P,A_dmin,AP)
c     
c     Debug
      if (debug) then
         write(*,*) 'A_is_at_end_dmin=',A_is_at_end_dmin
      endif
c     Debug
      if (A_is_at_end_dmin) then
         if (end_side_dmin.eq.1) then
            if (isegment_dmin.eq.1) then
               iseg2=Nsegments
            else
               iseg2=isegment_dmin-1
            endif
         else                   ! end_side_dmin=2
            if (isegment_dmin.eq.Nsegments) then
               iseg2=1
            else
               iseg2=isegment_dmin+1
            endif
         endif                  ! end_side_dmin=1 or 2
         call normal_to_segment(dim,Ncontour,Nppc,contour,
     &        icontour,iseg2,n2)
         call add_vectors(dim,normal,n2,tmp)
c     Debug
         if (debug) then
            write(*,*) 'iseg2=',iseg2
            write(*,*) 'n2=',n2
            write(*,*) 'normal=',normal
            write(*,*) 'tmp=',tmp
         endif
c     Debug
         call scalar_product(dim,AP,tmp,sp)
      else
c     then check "P" is on the side of the normal for that segment
c     normal to the current segment
c     scalar product between the normal and vector AP
         call normalize_vector(dim,AP,nAP)
         call scalar_product(dim,normal,nAP,sp)
c     Debug
         if (debug) then
            write(*,*) 'normal=',normal
            write(*,*) 'AP=',AP
            write(*,*) 'nAP=',nAP
         endif
c     Debug
      endif
c     Debug
      if (debug) then
         write(*,*) 'sp=',sp
      endif
c     Debug
      if (sp.ge.0.0D+0) then
         inside=.true.
      else
         inside=.false.
      endif

 666  continue
c     Debug
      if (debug) then
         write(*,*) 'inside=',inside
         write(*,*) trim(label),' :out'
      endif                     ! debug
c     Debug

      return
      end



      subroutine normal_to_segment(dim,Ncontour,Nppc,contour,
     &     icontour,isegment,normal)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the normal to a given segment of a given contour
c     that is directed toward the inside of the contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: list of contours
c       + icontour: index of the contour
c       + isegment: index of the segment
c     
c     Output:
c       + normal: normal to segment index "isegment" of contour index "icontour"
c     and directed toward the inside of the contour
c     
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      integer isegment
      double precision normal(1:Ndim_mx)
c     temp
      integer Npoints,Nsegments
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i1,i2,j
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision norm
c     label
      character*(Nchar_mx) label
      label='subroutine normal_to_segment'

      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be <= Ncontour=',Ncontour
         stop
      endif

      call get_contour(Ncontour,Nppc,contour,icontour,
     &     Npoints,ctr)
      Nsegments=Npoints
      if (isegment.lt.1) then
         call error(label)
         write(*,*) 'isegment=',isegment
         write(*,*) 'should be > 0'
         stop
      endif
      if (isegment.gt.Nsegments) then
         call error(label)
         write(*,*) 'isegment=',isegment
         write(*,*) 'should be <= Nsegments=',Nsegments
         stop
      endif
      i1=isegment
      call get_position_from_ctr(dim,Npoints,ctr,i1,x1)
      if (isegment.eq.Nsegments) then
         i2=1
      else
         i2=isegment+1
      endif
      call get_position_from_ctr(dim,Npoints,ctr,i2,x2)
      call substract_vectors(dim,x2,x1,u)
      norm=dsqrt(u(1)**2.0D+0+u(2)**2.0D+0)
      if (norm.le.0.0D+0) then
         call error(label)
         write(*,*) 'norm=',norm
         write(*,*) 'u=',(u(j),j=1,dim-1)
         stop
      else
         normal(1)=-u(2)/norm
         normal(2)=u(1)/norm
         normal(3)=0.0D+0
      endif

      return
      end



      subroutine normal_for_segment(dim,P1,P2,n)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the normal between the two points that define a segment
c     The returned normal points towards the inside of a closed contour if P1 and P2
c     are two consecutive positions over a closed contour defined counter-clockwise
c     
c     Input:
c       + dim: dimension of space
c       + P1: first point of the segment
c       + P2: second point of the segment
c     
c     Output:
c       + n: normal for the segment
c     
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision n(1:Ndim_mx)
c     temp
      double precision t(1:Ndim_mx)
      double precision u(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine normal_for_segment'

      call substract_vectors(dim,P2,P1,t)
      u(1)=-t(2)
      u(2)=t(1)
      u(3)=t(3)
      call normalize_vector(dim,u,n)

      return
      end
