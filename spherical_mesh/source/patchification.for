      subroutine patchification(dim,
     &     Ngroup,group_phi_limit,group_theta_limit,
     &     planet_radius,
     &     Nv,Nf,vertices,faces,Nmat,materials,
     &     attributed_mtl,attributed_mtl_idx,mtl_idx,
     &     Nlambda_default,lambda_default,reflectivity_default,brdf_default)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'param.inc'
c     
c     Purpose: to produce the right patches in order to define the
c     1° latitude/longitude grid that surrounds every data cube grid
c     
c     Prerequisites:
c     - the input data cubes must not overlap ! They actually have to
c     be separated by at least 1 degree over each border
c     - longitudes have to be in the (0-360)° range; if a data cube
c     has to cross the 0/360° boundary, the code has to be modified
c     - latitudes have to be in the (-89,89)° range: the last degree
c     is necessary for the representation of polar caps.
c     
c     
c     Input:
c       + dim: dimension of space
c       + Ngroup: number of groups of VIMS cubes
c       + group_phi_limit: longitude limit of each group of VIMS data cube (0,360) [deg]
c       + group_theta_limit: latitude limit of each group of VIMS data cube (-90,90) [deg]
c       + planet_radius: reference radius [m]
c       + Nv: number of vertices in the OBJ file to append
c       + Nf: number of faces in the OBJ file to append
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + Nmat: number of materials
c       + materials: array of materials (name of the material for each face)
c       + attributed_mtl: identifies whether or not a given (cube,ipix,jpix) material has been attributed
c       + attributed_mtl_idx: index of the material (in the "materials" list) of each attributed material
c       + mtl_idx: index of the material (in the "materials" list) for each face
c       + Nlambda_default: number of wavelength for the default material
c       + lambda_default: values of the wavelength for the default material [nm]
c       + reflectivity_default: reflectivity for each pixel, for each wavelength, for the default material
c       + brdf_default: BRDF for the default material
c     
c     Output: updated values of Nv, Nf, vertices, faces, materials, attributed_mtl, attributed_mtl_idx, mtl_idx
c     
c     I/O
      integer dim
      integer Ngroup
      double precision group_phi_limit(1:Ngroup_mx,1:2)
      double precision group_theta_limit(1:Ngroup_mx,1:2)
      double precision planet_radius
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      logical attributed_mtl(0:Ncube_mx,0:Npix_mx,0:Npix_mx)
      integer attributed_mtl_idx(0:Ncube_mx,0:Npix_mx,0:Npix_mx)
      integer mtl_idx(1:Nf_mx)
      integer Nlambda_default
      double precision lambda_default(1:Nlambda_mx)
      double precision reflectivity_default(1:Nlambda_mx)
      double precision brdf_default
c     temp
      integer igroup,ivertex,iface,iv
      integer Nlimit,Npatch
      double precision phi_grid(0:2*(Ngroup_mx+1))
      double precision theta_grid(0:2*(Ngroup_mx+1))
      integer i,j,ilambda
      double precision limit
      logical limit_found
      logical patch_in_group
      double precision theta1,theta2
      double precision phi1,phi2
      integer Ntheta,Nphi
      character*(Nchar_mx) mat,name,material
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
      logical invert_normal,is_thin,file_exists
      character*(Nchar_mx) brdf_type,mtl_file
      character*(Nchar_mx) front_name,middle_name,back_name
      logical glimit_used(1:Ngroup_mx,1:2)
      integer lidx(1:2)
      integer idx(1:3),Nv_init
      logical is_nan
c     label
      character*(Nchar_mx) label
      label='subroutine patchification'

      Nv_init=Nv
c     Create default material
      do j=1,3
         idx(j)=0
      enddo                     ! j
      call pixel_mtl_name(idx(1),idx(2),idx(3),material,name)
      if (.not.attributed_mtl(idx(1),idx(2),idx(3))) then
         attributed_mtl(idx(1),idx(2),idx(3))=.true.
         Nmat=Nmat+1
         if (Nmat.gt.Nmaterial_mx) then
            call error(label)
            write(*,*) 'Nmaterial_mx has been reached'
            stop
         endif
         materials(Nmat)=trim(name)
         attributed_mtl_idx(idx(1),idx(2),idx(3))=Nmat ! absolute index
      endif
      mtl_file='./results/'//trim(name)
      inquire(file=trim(mtl_file),exist=file_exists)
      if (.not.file_exists) then
         if (brdf_default.eq.0.0D+0) then
            brdf_type='lambertian'
         else if (brdf_default.eq.1.0D+0) then
            brdf_type='specular'
         else
            call error(label)
            write(*,*) 'brdf_default=',brdf_default
            write(*,*) 'is not supported yet'
            stop
         endif
         open(14,file=trim(mtl_file))
         write(14,23) 'wavelengths',Nlambda_default
         do ilambda=1,Nlambda_default
            write(14,*) lambda_default(ilambda), ! nm
     &           trim(brdf_type),
     &           reflectivity_default(ilambda)
         enddo                  ! ilambda
         close(14)
      endif                     ! file_exists=F

      Nlimit=2*(Ngroup+1)
c     Debug
c      write(*,*) 'Nlimit=',Nlimit
c      write(*,*) 'Ngroup=',Ngroup
c      do igroup=1,Ngroup
c         write(*,*) 'group index:',igroup
c         write(*,*) 'group_phi_limit=',(group_phi_limit(igroup,j),j=1,2)
c      enddo                     ! igroup
c     Debug
c     phi grid
      phi_grid(0)=0.0D+0
      do igroup=1,Ngroup
         do j=1,2
            glimit_used(igroup,j)=.false.
         enddo                  ! j
      enddo                     ! igroup
      do i=1,Nlimit-2
         limit=360.0D+0
         limit_found=.false.
c     Debug
c         write(*,*) 'limit index =',i
c         write(*,*) 'phi_grid(',i-1,')=',phi_grid(i-1)
c     Debug
         do igroup=1,Ngroup
            do j=1,2
               if ((group_phi_limit(igroup,j).ge.phi_grid(i-1)).and.
     &              (group_phi_limit(igroup,j).lt.limit).and.
     &              (.not.glimit_used(igroup,j))) then
                  limit_found=.true.
                  limit=group_phi_limit(igroup,j)
                  lidx(1)=igroup
                  lidx(2)=j
               endif
            enddo               ! j
         enddo                  ! igroup
         if (limit_found) then
            phi_grid(i)=limit
            glimit_used(lidx(1),lidx(2))=.true.
         else
            call error(label)
            write(*,*) 'Could not find longitude limit index:',i
            do j=1,i-1
               write(*,*) 'phi_grid(',j,')=',phi_grid(j)
            enddo               ! j
            stop
         endif
      enddo                     ! i
      phi_grid(Nlimit-1)=360.0D+0
c     Debug
c      do i=0,Nlimit-1
c         write(*,*) 'phi_grid(',i,')=',phi_grid(i),
c     &        phi_grid(i)*pi/180.0D+0
c      enddo                     ! i
c     Debug
c     
c     theta grid
      theta_grid(0)=-89.0D+0
      do i=1,Nlimit-2
         limit=88.0D+0
         limit_found=.false.
         do igroup=1,Ngroup
            do j=1,2
               if ((group_theta_limit(igroup,j).gt.theta_grid(i-1)).and.
     &              (group_theta_limit(igroup,j).lt.limit)) then
                  limit_found=.true.
                  limit=group_theta_limit(igroup,j)
               endif
            enddo               ! j
         enddo                  ! igroup
         if (limit_found) then
            theta_grid(i)=limit
         else
            call error(label)
            write(*,*) 'Could not find longitude limit index:',i
            do j=1,i-1
               write(*,*) 'theta_grid(',j,')=',theta_grid(j)
            enddo               ! j
            stop
         endif
      enddo                     ! i
      theta_grid(Nlimit-1)=89.0D+0
c     Debug
c      do i=0,Nlimit-1
c         write(*,*) 'theta_grid(',i,')=',theta_grid(i),
c     &        theta_grid(i)*pi/180.0D+0
c      enddo                     ! i
c     Debug
c     Produce patches
      invert_normal=.false.
      is_thin=.false.
      front_name='air'
      middle_name=''
      back_name=trim(material)
c     
c     south pole cap
      phi1=0.0D+0
      phi2=2.0D+0*pi
      theta1=-pi/2.0D+0
      theta2=-89.0D+0*pi/180.0D+0
      Nphi=360.0D+0
      Ntheta=1
      call partial_sphere_obj(dim,planet_radius,
     &     theta1,theta2,
     &     phi1,phi2,
     &     Ntheta,Nphi,
     &     Nv01,Nf01,v01,f01)
c     Debug
c      write(*,*) 'Nv01=',Nv01
c      do ivertex=1,10
c         write(*,*) (v01(ivertex,j),j=1,3)
c      enddo                     ! ivertex
c      stop
c     Debug
      do ivertex=1,Nv01
         Nv=Nv+1
         if (Nv.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         do j=1,3
c     Debug
c            call test_nan(v01(ivertex,j),is_nan)
c            if (is_nan) then
c               call error(label)
c               write(*,*) 'south polar cap'
c               write(*,*) 'v01(',ivertex,',',j,')=',v01(ivertex,j)
c               stop
c            endif
c     Debug
            vertices(Nv,j)=v01(ivertex,j)
         enddo                  ! j
      enddo                     ! iv
      do iface=1,Nf01
         Nf=Nf+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         do j=1,3
            faces(Nf,j)=Nv_init+f01(iface,j)
         enddo                  ! j
         mtl_idx(Nf)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      enddo                     ! iface
c     patches
      Npatch=(Nlimit-1)**2
      do i=1,Nlimit-1
         do j=1,Nlimit-1
            phi1=phi_grid(i-1)  ! deg
            phi2=phi_grid(i)    ! deg
            theta1=theta_grid(j-1) ! deg
            theta2=theta_grid(j) ! deg
c
            patch_in_group=.false.
            do igroup=1,Ngroup
               if ((phi1.ge.group_phi_limit(igroup,1)).and.
     &              (phi2.le.group_phi_limit(igroup,2)).and.
     &              (theta1.ge.group_theta_limit(igroup,1)).and.
     &              (theta2.le.group_theta_limit(igroup,2))) then
                  patch_in_group=.true.
                  goto 111
               endif
            enddo               ! igroup
 111        continue
c     Debug
c            write(*,*) i,j,patch_in_group
c     Debug
            if (.not.patch_in_group) then
               Nphi=int(phi2-phi1) ! resolution is 1° in longitude
               Ntheta=int(theta2-theta1) ! resolution is 1° in latitude
               phi1=phi1*pi/180.0D+0 ! rad
               phi2=phi2*pi/180.0D+0 ! rad
               theta1=theta1*pi/180.0D+0 ! rad
               theta2=theta2*pi/180.0D+0 ! rad
               if ((Nphi.gt.0).and.(Ntheta.gt.0)) then
                  Nv_init=Nv
                  call partial_sphere_obj(dim,planet_radius,
     &                 theta1,theta2,
     &                 phi1,phi2,
     &                 Ntheta,Nphi,
     &                 Nv01,Nf01,v01,f01)
                  do ivertex=1,Nv01
                     Nv=Nv+1
                     if (Nv.gt.Nv_mx) then
                        call error(label)
                        write(*,*) 'Nv_mx has been reached'
                        stop
                     endif
                     do iv=1,3
c     Debug
                        call test_nan(v01(ivertex,iv),is_nan)
                        if (is_nan) then
                           call error(label)
                           write(*,*) 'patch index:',i,',',j
                           write(*,*) 'phi1=',phi1
                           write(*,*) 'phi2=',phi2
                           write(*,*) 'Nphi=',Nphi
                           write(*,*) 'theta1=',theta1
                           write(*,*) 'theta2=',theta2
                           write(*,*) 'Ntheta=',Ntheta
                           write(*,*) 'v01(',ivertex,',',iv,')=',v01(ivertex,iv)
                           stop
                        endif
c     Debug
                        vertices(Nv,iv)=v01(ivertex,iv)
                     enddo      ! j
                  enddo         ! iv
                  do iface=1,Nf01
                     Nf=Nf+1
                     if (Nf.gt.Nf_mx) then
                        call error(label)
                        write(*,*) 'Nf_mx has been reached'
                        stop
                     endif
                     do iv=1,3
                        faces(Nf,iv)=Nv_init+f01(iface,iv)
                     enddo      ! j
                     mtl_idx(Nf)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
                  enddo         ! iface
               endif            ! Nphi>0 and Ntheta>0
            endif               ! patch_in_group=F
         enddo                  ! j
      enddo                     ! i
c     north pole cap
      phi1=0.0D+0
      phi2=2.0D+0*pi
      theta1=89.0D+0*pi/180.0D+0
      theta2=pi/2.0D+0
      Nphi=360.0D+0
      Ntheta=1
      Nv_init=Nv
      call partial_sphere_obj(dim,planet_radius,
     &     theta1,theta2,
     &     phi1,phi2,
     &     Ntheta,Nphi,
     &     Nv01,Nf01,v01,f01)
      do ivertex=1,Nv01
         Nv=Nv+1
         if (Nv.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         do j=1,3
c     Debug
            call test_nan(v01(ivertex,j),is_nan)
            if (is_nan) then
               call error(label)
               write(*,*) 'north polar cap'
               write(*,*) 'v01(',ivertex,',',j,')=',v01(ivertex,j)
               stop
            endif
c     Debug
            vertices(Nv,j)=v01(ivertex,j)
         enddo                  ! j
      enddo                     ! iv
      do iface=1,Nf01
         Nf=Nf+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         do j=1,3
            faces(Nf,j)=Nv_init+f01(iface,j)
         enddo                  ! j
         mtl_idx(Nf)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      enddo                     ! iface
c     Debug      
c      write(*,*) 'north polar cap'
c      write(*,*) 'idx=',(idx(i),i=1,3)
c      write(*,*) 'mtl_idx(',Nf,')=',mtl_idx(Nf)
c     Debug
      
      return
      end
