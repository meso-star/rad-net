      subroutine parse_group(dim,igroup,Ncube_in_group,cube_index,
     &     Npix,corner_theta,corner_phi,
     &     min_phi,max_phi,min_theta,max_theta,
     &     Nint_phi,Nint_theta,
     &     parsing_file)
      implicit none
      include 'max.inc'
c     
c     Purpose: to establish the index of the cubes / pixel over which
c     each vertex of a group belong
c     
c     Input:
c       + dim: dimension of space
c       + igroup: index of the group
c       + Ncube_in_group: number of files/cubes in each group
c       + cube_index: index of every cube in each group
c       + Npix: number of pixels along theta/phi directions
c       + corner_theta: value of theta for each corner of each pixel [deg]
c       + corner_phi: value of phi for each corner of each pixel [deg]
c       + min_phi / max_phi: absolute min and max longitude for the group [deg]
c       + min_theta / max_theta: absolute min and max latitude for the group [deg]
c       + Nint_phi: number of longitude intervals in the [min_phi, max_phi] range
c       + Nint_theta: number of latitude intervals in the [min_theta, max_theta] range
c       + parsing_file: parsing file to record
c     
c     Output: the required parsing_file
c     
c     I/O
      integer dim
      integer igroup
      integer Ncube_in_group(1:Ngroup_mx)
      integer cube_index(1:Ngroup_mx,1:Nfig_mx)
      integer Npix(1:Nfig_mx,1:2)
      double precision corner_theta(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision min_phi,max_phi
      double precision min_theta,max_theta
      integer Nint_phi,Nint_theta
      character*(Nchar_mx) parsing_file
c     temp
      integer icube,ipix,jpix,icorner,i,j,iacc,jacc
      double precision phi1,phi2
      double precision theta1,theta2
      double precision dtheta,dphi
      integer ii,ji
      integer Nacc
      double precision acc_grid(1:Nacc_mx,1:Nacc_mx,1:4)
      integer acc_Npixel(1:Nacc_mx,1:Nacc_mx)
      integer acc_pidx(1:Nacc_mx,1:Nacc_mx,1:Npiag_mx,1:3)
      double precision x(1:Ndim_mx-1)
      double precision x1,x2,y1,y2
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      double precision P(1:Ndim_mx)
      logical inside,intersect
      double precision deltax,deltay
      logical is_in_pixel
      integer Npixel
      integer pixel_index(1:Npix_mx,1:3)
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine parse_group'

c     phi/theta resolution
      dphi=(max_phi-min_phi)/dble(Nint_phi)
      dtheta=(max_theta-min_theta)/dble(Nint_theta)
c     Acceleration grid
      Nacc=50                    ! number of acceleration intervals in both directions
      if (Nacc.gt.Nacc_mx) then
         call error(label)
         write(*,*) 'Nacc=',Nacc
         write(*,*) '> Nacc_mx=',Nacc_mx
         stop
      endif
      deltax=(max_phi-min_phi)/dble(Nacc)
      deltay=(max_theta-min_theta)/dble(Nacc)
      do iacc=1,Nacc
         x1=min_phi+(iacc-1)*deltax
         x2=min_phi+iacc*deltax
         do jacc=1,Nacc
            y1=min_theta+(jacc-1)*deltay
            y2=min_theta+jacc*deltay
            acc_grid(iacc,jacc,1)=x1
            acc_grid(iacc,jacc,2)=x2
            acc_grid(iacc,jacc,3)=y1
            acc_grid(iacc,jacc,4)=y2
            acc_Npixel(iacc,jacc)=0
         enddo                  ! jacc
      enddo                     ! iacc
c
      write(*,*) 'Producing acceleration grid...'
c     --- progress display
      ntot=0
      do icube=1,Ncube_in_group(igroup)
         do ipix=1,Npix(icube,1)
            do jpix=1,Npix(icube,2)
               ntot=ntot+Nacc*Nacc
            enddo               ! jpix
         enddo                  ! ipix
      enddo                     ! icube
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display ---
      do icube=1,Ncube_in_group(igroup)
         do ipix=1,Npix(icube,1)
            do jpix=1,Npix(icube,2)
c     Identify (phi1, phi2, theta1, theta2) the limits of the bounding box for the current (icube,ipix,jpix) pixel
               phi1=corner_phi(icube,ipix,jpix,1)
               phi2=corner_phi(icube,ipix,jpix,1)
               theta1=corner_theta(icube,ipix,jpix,1)
               theta2=corner_theta(icube,ipix,jpix,1)
               do icorner=1,4
                  if (corner_phi(icube,ipix,jpix,icorner).lt.phi1) then
                     phi1=corner_phi(icube,ipix,jpix,icorner)
                  endif
                  if (corner_phi(icube,ipix,jpix,icorner).gt.phi2) then
                     phi2=corner_phi(icube,ipix,jpix,icorner)
                  endif
                  if (corner_theta(icube,ipix,jpix,icorner).lt.theta1) then
                     theta1=corner_theta(icube,ipix,jpix,icorner)
                  endif
                  if (corner_theta(icube,ipix,jpix,icorner).gt.theta2) then
                     theta2=corner_theta(icube,ipix,jpix,icorner)
                  endif
               enddo            ! icorner
c     ===============================================================================================================
c     Now all we have to do is to test every voxel that is in this bounding box !
c     ===============================================================================================================
c     now test every cell of the acceleration grid for intersection with the bounding-box of the pixel
               do iacc=1,Nacc
                  do jacc=1,Nacc
                     call intersecting_squares(acc_grid(iacc,jacc,1),acc_grid(iacc,jacc,2),acc_grid(iacc,jacc,3),acc_grid(iacc,jacc,4),
     &                    phi1,phi2,theta1,theta2,intersect)
                     if (intersect) then
                        acc_Npixel(iacc,jacc)=acc_Npixel(iacc,jacc)+1
                        if (acc_Npixel(iacc,jacc).gt.Npiag_mx) then
                           call error(label)
                           write(*,*) 'Npiag_mx has been reached'
                           stop
                        endif
                        acc_pidx(iacc,jacc,acc_Npixel(iacc,jacc),1)=icube
                        acc_pidx(iacc,jacc,acc_Npixel(iacc,jacc),2)=ipix
                        acc_pidx(iacc,jacc,acc_Npixel(iacc,jacc),3)=jpix                        
                     endif
c     
c     --- progress display
                     ndone=ndone+1
                     fdone=dble(ndone)/dble(ntot)*1.0D+2
                     ifdone=floor(fdone)
                     if (ifdone.gt.pifdone) then
                        do j=1,len+2
                           write(*,"(a)",advance='no') "\b"
                        enddo   ! j
                        write(*,trim(fmt),advance='no') floor(fdone),' %'
                        pifdone=ifdone
                     endif
c     progress display ---
                  enddo         ! jacc
               enddo            ! iacc
c                    
            enddo               ! jpix
         enddo                  ! ipix
      enddo                     ! icube
c     
      write(*,*)
      write(*,*) 'Parsing group index:',igroup
      Ncontour=1
      do icontour=1,Ncontour
         Nppc(icontour)=4
      enddo                     ! icontour
c     Parsing is now so fast the use of intermediary files is no longer required
c     (but I leave the code in the case it becomes necessary again)
c      open(13,file='./results/parsing_group1_test.txt')
c     --- progress display
      ntot=(Nint_phi+1)*(Nint_theta+1)
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display ---
      do ii=0,Nint_phi
         x(1)=min_phi+ii*dphi
         if (x(1).lt.max_phi) then
            iacc=int((x(1)-min_phi)/deltax)+1
         else
            iacc=Nacc
         endif
         do ji=0,Nint_theta
            x(2)=min_theta+ji*dtheta
            if (x(2).lt.max_theta) then
               jacc=int((x(2)-min_theta)/deltay)+1
            else
               jacc=Nacc
            endif
            P(1)=x(1)           ! phi
            P(2)=x(2)           ! theta
            P(3)=0.0D+0
            is_in_pixel=.false.
            Npixel=0
            if (acc_Npixel(iacc,jacc).gt.0) then
               do i=1,acc_Npixel(iacc,jacc)
                  icube=acc_pidx(iacc,jacc,i,1)
                  ipix=acc_pidx(iacc,jacc,i,2)
                  jpix=acc_pidx(iacc,jacc,i,3)
                  do icorner=1,Nppc(1)
c     VIMS pixels are defined clockwise while all contours should
c     be defined counter-clockwise
                     contour(1,icorner,1)=
     &                    corner_phi(icube,ipix,jpix,Nppc(1)-icorner+1)
                     contour(1,icorner,2)=
     &                    corner_theta(icube,ipix,jpix,Nppc(1)-icorner+1)
                  enddo         ! icorner
                  call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,1,P,inside)
                  if (inside) then
                     is_in_pixel=.true.
                     Npixel=Npixel+1
                     if (Npixel.gt.Npix_mx) then
                        call error(label)
                        write(*,*) 'Npix_mx has been reached'
                        stop
                     endif
                     pixel_index(Npixel,1)=icube
                     pixel_index(Npixel,2)=ipix
                     pixel_index(Npixel,3)=jpix
                  endif         ! inside
               enddo            ! i
            endif               ! acc_Npixel(iacc,jacc)>0
c            write(13,*) is_in_pixel
c            if (is_in_pixel) then
c               write(13,*) Npixel
c               do ipix=1,Npixel
c                  write(13,*) (pixel_index(ipix,j),j=1,3)
c               enddo            ! ipix
c            endif               ! is_in_pixel
c     --- progress display
            ndone=ndone+1
            fdone=dble(ndone)/dble(ntot)*1.0D+2
            ifdone=floor(fdone)
            if (ifdone.gt.pifdone) then
               do j=1,len+2
                  write(*,"(a)",advance='no') "\b"
               enddo            ! j
               write(*,trim(fmt),advance='no') floor(fdone),' %'
               pifdone=ifdone
            endif
c     progress display ---
         enddo                  ! ji
      enddo                     ! ii
c      close(13)
      
      return
      end
      

      
      subroutine parsing_file_name(igroup,
     &     min_theta,max_theta,min_phi,max_phi,
     &     Nint_theta,Nint_phi,
     &     parsing_file)
      implicit none
      include 'max.inc'
c
c     Purpose: to provide the name of the parsing file
c
c     Input:
c       + igroup: index of the group of VIMS cubes
c       + min_theta: lower value of latitude [deg]
c       + max_theta: higher value of latitude [deg]
c       + min_phi: lower value of longitude [deg]
c       + min_phi: higher value of longitude [deg]
c       + Nint_theta: number of latitude intervals
c       + Nint_phi: number of longitude intervals
c
c     Output:
c       + parsing_file: name of the parsing file
c
c     I/O
      integer igroup
      double precision min_theta
      double precision max_theta
      double precision min_phi
      double precision max_phi
      integer Nint_theta
      integer Nint_phi
      character*(Nchar_mx) parsing_file
c     temp
      logical err_code
      character*(Nchar_mx) group_str
      character*(Nchar_mx) min_theta_str
      character*(Nchar_mx) max_theta_str
      character*(Nchar_mx) min_phi_str
      character*(Nchar_mx) max_phi_str
      character*(Nchar_mx) Nint_theta_str
      character*(Nchar_mx) Nint_phi_str
c     label
      character*(Nchar_mx) label
      label='subroutine parsing_file_name'
      
      if (igroup.lt.10000) then
         call num2str4(igroup,group_str,err_code)
      else
         call error(label)
         write(*,*) 'igroup=',igroup
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'igroup=',igroup
         stop
      endif
c     
      if (int(min_theta).lt.10000) then
         call num2str4(int(min_theta),min_theta_str,err_code)
      else
         call error(label)
         write(*,*) 'min_theta=',int(min_theta)
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'min_theta=',int(min_theta)
         stop
      endif
c     
      if (int(max_theta).lt.10000) then
         call num2str4(int(max_theta),max_theta_str,err_code)
      else
         call error(label)
         write(*,*) 'max_theta=',int(max_theta)
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'max_theta=',int(max_theta)
         stop
      endif
c     
      if (int(min_phi).lt.10000) then
         call num2str4(int(min_phi),min_phi_str,err_code)
      else
         call error(label)
         write(*,*) 'min_phi=',int(min_phi)
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'min_phi=',int(min_phi)
         stop
      endif
c     
      if (int(max_phi).lt.10000) then
         call num2str4(int(max_phi),max_phi_str,err_code)
      else
         call error(label)
         write(*,*) 'max_phi=',int(max_phi)
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'max_phi=',int(max_phi)
         stop
      endif
c     
      if (Nint_theta.lt.10000) then
         call num2str4(Nint_theta,Nint_theta_str,err_code)
      else
         call error(label)
         write(*,*) 'Nint_theta=',Nint_theta
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'Nint_theta=',Nint_theta
         stop
      endif
c     
      if (Nint_phi.lt.10000) then
         call num2str4(Nint_phi,Nint_phi_str,err_code)
      else
         call error(label)
         write(*,*) 'Nint_phi=',Nint_phi
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'Nint_phi=',Nint_phi
         stop
      endif
c
      parsing_file='./data/parsing_'
     &     //trim(group_str)//'_'
     &     //trim(min_theta_str)//'_'
     &     //trim(max_theta_str)//'_'
     &     //trim(min_phi_str)//'_'
     &     //trim(max_phi_str)//'_'
     &     //trim(Nint_theta_str)//'_'
     &     //trim(Nint_phi_str)//'.dat'

      return
      end



      subroutine intersecting_squares(x11,x12,y11,y12,x21,x22,y21,y22,intersect)
      implicit none
      include 'max.inc'
c     
c     Purpose: to test whether or not two squares intersect
c     
c     Input:
c       + x11: lower x for cube 1
c       + x12: higher x for cube 1
c       + y11: lower y for cube 1
c       + y12: higher y for cube 1
c       + x21: lower x for cube 2
c       + x22: higher x for cube 2
c       + y21: lower y for cube 2
c       + y22: higher y for cube 2
c     
c     Output:
c       + intersect: true if the two squares intersect
c     
c     I/O
      double precision x11,x12
      double precision x21,x22
      double precision y11,y12
      double precision y21,y22
      logical intersect
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine intersecting_squares'

      intersect=.false.
c     testing whether or not a corner of cube 1 is inside cube 2
      if ((x11.ge.x21).and.(x11.le.x22).and.(y11.ge.y21).and.(y11.le.y22)) then
         intersect=.true.
         goto 666
      endif
      if ((x12.ge.x21).and.(x12.le.x22).and.(y11.ge.y21).and.(y11.le.y22)) then
         intersect=.true.
         goto 666
      endif
      if ((x12.ge.x21).and.(x12.le.x22).and.(y12.ge.y21).and.(y12.le.y22)) then
         intersect=.true.
         goto 666
      endif
      if ((x11.ge.x21).and.(x11.le.x22).and.(y12.ge.y21).and.(y12.le.y22)) then
         intersect=.true.
         goto 666
      endif
c     testing whether or not a corner of cube 2 is inside cube 1
      if ((x21.ge.x11).and.(x21.le.x12).and.(y21.ge.y11).and.(y21.le.y12)) then
         intersect=.true.
         goto 666
      endif
      if ((x22.ge.x11).and.(x22.le.x12).and.(y21.ge.y11).and.(y21.le.y12)) then
         intersect=.true.
         goto 666
      endif
      if ((x22.ge.x11).and.(x22.le.x12).and.(y22.ge.y11).and.(y22.le.y12)) then
         intersect=.true.
         goto 666
      endif
      if ((x21.ge.x11).and.(x21.le.x12).and.(y22.ge.y11).and.(y22.le.y12)) then
         intersect=.true.
         goto 666
      endif
 666  continue

      return
      end
