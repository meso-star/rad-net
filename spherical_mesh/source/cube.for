      subroutine group_mesh(dim,igroup,Ncube_in_group,cube_index,planet_radius,
     &     Npix,corner_theta,corner_phi,corner_z,
     &     brdf,Nlambda,lambda,reflectivity,Nv,Nf,vertices,faces,Nmat,materials,
     &     attributed_mtl,attributed_mtl_idx,mtl_idx,
     &     theta_limit,phi_limit)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to produce the 3D mesh of a VIMS data cube
c     
c     Input:
c       + dim: dimension of space
c       + igroup: index of the group
c       + Ncube_in_group: number of files/cubes in each group
c       + cube_index: index of every cube in each group
c       + planet_radius: planet radius for the surface grid [m]
c       + Npix: number of pixels along theta/phi directions
c       + corner_theta: value of theta for each corner of each pixel
c       + corner_phi: value of phi for each corner of each pixel
c       + brdf: BRDF for each pixel [0/1]
c       + Nlambda: number of wavelength
c       + lambda: values of the wavelength [nm]
c       + reflectivity: reflectivity for each pixel, for each wavelength
c       + Nv: number of vertices in the obj_file
c       + Nf: number of faces in the obj_file
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + Nmat: number of materials
c       + materials: array of materials (name of the material for each face)
c       + attributed_mtl: identifies whether or not a given (cube,ipix,jpix) material has been attributed
c       + attributed_mtl_idx: index of the material (in the "materials" list) of each attributed material
c       + mtl_idx: index of the material (in the "materials" list) for each face
c     
c     Output: a OBJ file for the group
c       + theta_limit: latitude limits [deg]
c       + phi_limit: longitude limits [deg]
c       + updated: Nv, Nf, ,vertices, faces, Nmat, material, attributed_mtl
c     
c     I/O
      integer dim
      integer igroup
      integer Ncube_in_group(1:Ngroup_mx)
      integer cube_index(1:Ngroup_mx,1:Nfig_mx)
      double precision planet_radius
      integer Npix(1:Nfig_mx,1:2)
      double precision corner_theta(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf(1:Nfig_mx,1:Npix_mx,1:Npix_mx)
      integer Nlambda(1:Nfig_mx)
      double precision lambda(1:Nfig_mx,1:Nlambda_mx)
      double precision reflectivity(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      logical attributed_mtl(0:Ncube_mx,0:Npix_mx,0:Npix_mx)
      integer attributed_mtl_idx(0:Ncube_mx,0:Npix_mx,0:Npix_mx)
      integer mtl_idx(1:Nf_mx)
      double precision theta_limit(1:2)
      double precision phi_limit(1:2)
c     temp
      integer ipix,jpix,icorner,i,j,ii,ji,icube,iacc,jacc,ivertex,iface
      double precision theta1,theta2,phi1,phi2
      double precision dphi1,dtheta1
      double precision min_theta,min_phi
      double precision max_theta,max_phi
      double precision dtheta,dphi
      integer Nint_theta,Nint_phi,n_theta,n_phi
      integer onedeg_Ntheta,onedeg_Nphi
      double precision x(1:Ndim_mx-1)
      logical is_in_pixel
      integer Npixel
      integer pixel_index(1:Npix_mx,1:3)
      integer pidx(1:Nv_mx,1:3)
      integer i1,i2,i3,i4
      double precision z,r,theta,phi
      double precision vertex(1:Ndim_mx)
      integer face(1:3)
      integer lidx(1:3)
      character*(Nchar_mx) mat,name,material,front_name
      integer n,Nint,Nint_onedeg,i0,iv_center,iv_ll
      double precision delta,phi0,theta0
      double precision origin(1:2)
      double precision v(1:4,1:Ndim_mx)
      integer Nv01,Nf01
      double precision v01(1:Nv_mx,1:Ndim_mx)
      integer f01(1:Nf_mx,1:3)
      integer Npix_sc(1:2)
      double precision corner_theta_sc(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi_sc(1:Npix_mx,1:Npix_mx,1:4)
      integer Nacc
      double precision acc_grid(1:Nacc_mx,1:Nacc_mx,1:4)
      integer acc_Npixel(1:Nacc_mx,1:Nacc_mx)
      integer acc_pidx(1:Nacc_mx,1:Nacc_mx,1:Npiag_mx,1:3)
      double precision x1,x2,y1,y2
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      double precision P(1:Ndim_mx)
      logical inside,intersect
      double precision deltax,deltay
      integer idx(1:3)
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine group_mesh'
c     
      do icube=1,Ncube_in_group(igroup)
         do j=1,2
            Npix_sc(j)= Npix(icube,j)
         enddo                  ! j
         do ipix=1,Npix_sc(1)
            do jpix=1,Npix_sc(2)
               do icorner=1,4
                  corner_theta_sc(ipix,jpix,icorner)=corner_theta(icube,ipix,jpix,icorner)
                  corner_phi_sc(ipix,jpix,icorner)=corner_phi(icube,ipix,jpix,icorner)
               enddo            ! icorner
            enddo               ! jpix
         enddo                  ! ipix
c     
         call analyze_cube(Npix_sc,corner_theta_sc,corner_phi_sc,
     &        phi1,phi2,theta1,theta2,dphi1,dtheta1)
c     put back corner_phi_sc into corner_phi, in case it has been adjusted to (0,360)°
         do ipix=1,Npix_sc(1)
            do jpix=1,Npix_sc(2)
               do icorner=1,4
                  corner_phi(icube,ipix,jpix,icorner)=corner_phi_sc(ipix,jpix,icorner)
               enddo            ! icorner
            enddo               ! jpix
         enddo                  ! ipix
         if (icube.eq.1) then
            min_phi=phi1
            max_phi=phi2
            min_theta=theta1
            max_theta=theta2
            dphi=dphi1
            dtheta=dtheta1
         else
            if (phi1.lt.min_phi) then
               min_phi=phi1
            endif
            if (phi2.gt.max_phi) then
               max_phi=phi2
            endif
            if (theta1.lt.min_theta) then
               min_theta=theta1
            endif
            if (theta2.gt.max_theta) then
               max_theta=theta2
            endif
            if (dphi1.lt.dphi) then
               dphi=dphi1
            endif
            if (dtheta1.lt.dtheta) then
               dtheta=dtheta1
            endif
         endif
      enddo                     ! icube
      
c     onedeg_Ntheta and onedeg_Nphi: number of latitude / longitude intervals for a 1° interval
      onedeg_Ntheta=int(1.0D+0/dtheta)
      onedeg_Nphi=int(1.0D+0/dphi)
c     2^n_theta: number of intervals along a 1° latitude interval (as a multiple of 2)
      call higher_power2(onedeg_Ntheta,n_theta)
c     2^n_phi: number of intervals along a 1° longitude interval (as a multiple of 2)
      call higher_power2(onedeg_Nphi,n_phi)
c     Nint_theta and Nint_phi: total number of latitude / longitude intervals
      Nint_theta=int(max_theta-min_theta)*(2**n_theta)
      Nint_phi=int(max_phi-min_phi)*(2**n_phi)
c      write(*,*) 'Nint_phi=',Nint_phi
c      write(*,*) 'Nint_theta=',Nint_theta
c
c     Limits
      theta_limit(1)=min_theta-1.0D+0
      theta_limit(2)=max_theta+1.0D+0
      phi_limit(1)=min_phi-1.0D+0
      phi_limit(2)=max_phi+1.0D+0

c     phi/theta resolution
      dphi=(max_phi-min_phi)/dble(Nint_phi)
      dtheta=(max_theta-min_theta)/dble(Nint_theta)
c     Acceleration grid
      Nacc=50                    ! number of acceleration intervals in both directions
      if (Nacc.gt.Nacc_mx) then
         call error(label)
         write(*,*) 'Nacc=',Nacc
         write(*,*) '> Nacc_mx=',Nacc_mx
         stop
      endif
      deltax=(max_phi-min_phi)/dble(Nacc)
      deltay=(max_theta-min_theta)/dble(Nacc)
      do iacc=1,Nacc
         x1=min_phi+(iacc-1)*deltax
         x2=min_phi+iacc*deltax
         do jacc=1,Nacc
            y1=min_theta+(jacc-1)*deltay
            y2=min_theta+jacc*deltay
            acc_grid(iacc,jacc,1)=x1
            acc_grid(iacc,jacc,2)=x2
            acc_grid(iacc,jacc,3)=y1
            acc_grid(iacc,jacc,4)=y2
            acc_Npixel(iacc,jacc)=0
         enddo                  ! jacc
      enddo                     ! iacc
c
      write(*,*) 'Producing acceleration grid...'
c     --- progress display
      ntot=0
      do icube=1,Ncube_in_group(igroup)
         do ipix=1,Npix(icube,1)
            do jpix=1,Npix(icube,2)
               ntot=ntot+Nacc*Nacc
            enddo               ! jpix
         enddo                  ! ipix
      enddo                     ! icube
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display ---
      do icube=1,Ncube_in_group(igroup)
         do ipix=1,Npix(icube,1)
            do jpix=1,Npix(icube,2)
c     Identify (phi1, phi2, theta1, theta2) the limits of the bounding box for the current (icube,ipix,jpix) pixel
               phi1=corner_phi(icube,ipix,jpix,1)
               phi2=corner_phi(icube,ipix,jpix,1)
               theta1=corner_theta(icube,ipix,jpix,1)
               theta2=corner_theta(icube,ipix,jpix,1)
               do icorner=1,4
                  if (corner_phi(icube,ipix,jpix,icorner).lt.phi1) then
                     phi1=corner_phi(icube,ipix,jpix,icorner)
                  endif
                  if (corner_phi(icube,ipix,jpix,icorner).gt.phi2) then
                     phi2=corner_phi(icube,ipix,jpix,icorner)
                  endif
                  if (corner_theta(icube,ipix,jpix,icorner).lt.theta1) then
                     theta1=corner_theta(icube,ipix,jpix,icorner)
                  endif
                  if (corner_theta(icube,ipix,jpix,icorner).gt.theta2) then
                     theta2=corner_theta(icube,ipix,jpix,icorner)
                  endif
               enddo            ! icorner
c     ===============================================================================================================
c     Now all we have to do is to test every voxel that is in this bounding box !
c     ===============================================================================================================
c     now test every cell of the acceleration grid for intersection with the bounding-box of the pixel
               do iacc=1,Nacc
                  do jacc=1,Nacc
                     call intersecting_squares(acc_grid(iacc,jacc,1),acc_grid(iacc,jacc,2),acc_grid(iacc,jacc,3),acc_grid(iacc,jacc,4),
     &                    phi1,phi2,theta1,theta2,intersect)
                     if (intersect) then
                        acc_Npixel(iacc,jacc)=acc_Npixel(iacc,jacc)+1
                        if (acc_Npixel(iacc,jacc).gt.Npiag_mx) then
                           call error(label)
                           write(*,*) 'Npiag_mx has been reached'
                           stop
                        endif
                        acc_pidx(iacc,jacc,acc_Npixel(iacc,jacc),1)=icube
                        acc_pidx(iacc,jacc,acc_Npixel(iacc,jacc),2)=ipix
                        acc_pidx(iacc,jacc,acc_Npixel(iacc,jacc),3)=jpix                        
                     endif
c     
c     --- progress display
                     ndone=ndone+1
                     fdone=dble(ndone)/dble(ntot)*1.0D+2
                     ifdone=floor(fdone)
                     if (ifdone.gt.pifdone) then
                        do j=1,len+2
                           write(*,"(a)",advance='no') "\b"
                        enddo   ! j
                        write(*,trim(fmt),advance='no') floor(fdone),' %'
                        pifdone=ifdone
                     endif
c     progress display ---
                  enddo         ! jacc
               enddo            ! iacc
c                    
            enddo               ! jpix
         enddo                  ! ipix
      enddo                     ! icube      
c
c     ============================================================================
c     Production of high-resolution grid for the data cube bounding box
c     ============================================================================
c
      Ncontour=1
      do icontour=1,Ncontour
         Nppc(icontour)=4
      enddo                     ! icontour
      Nv01=0
      write(*,*)
      write(*,*) 'Producing high-definition grid for group:',igroup
c     --- progress display
      ntot=(Nint_phi+1)*(Nint_theta+1)
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display ---
      do ii=0,Nint_phi
         x(1)=min_phi+ii*dphi
         if (x(1).lt.max_phi) then
            iacc=int((x(1)-min_phi)/deltax)+1
         else
            iacc=Nacc
         endif
         do ji=0,Nint_theta
            x(2)=min_theta+ji*dtheta
            if (x(2).lt.max_theta) then
               jacc=int((x(2)-min_theta)/deltay)+1
            else
               jacc=Nacc
            endif
c
            Nv01=Nv01+1
            if (Nv01.gt.Nv_mx) then
               call error(label)
               write(*,*) 'Nv_mx has been reached'
               stop
            endif
c     
            P(1)=x(1)           ! phi
            P(2)=x(2)           ! theta
            P(3)=0.0D+0
            is_in_pixel=.false.
            Npixel=0
            if (acc_Npixel(iacc,jacc).gt.0) then
               do i=1,acc_Npixel(iacc,jacc)
                  icube=acc_pidx(iacc,jacc,i,1)
                  ipix=acc_pidx(iacc,jacc,i,2)
                  jpix=acc_pidx(iacc,jacc,i,3)
                  do icorner=1,Nppc(1)
c     VIMS pixels are defined clockwise while all contours should
c     be defined counter-clockwise
                     contour(1,icorner,1)=
     &                    corner_phi(icube,ipix,jpix,Nppc(1)-icorner+1)
                     contour(1,icorner,2)=
     &                    corner_theta(icube,ipix,jpix,Nppc(1)-icorner+1)
                  enddo         ! icorner
                  call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,1,P,inside)
                  if (inside) then
                     is_in_pixel=.true.
                     Npixel=Npixel+1
                     if (Npixel.gt.Npix_mx) then
                        call error(label)
                        write(*,*) 'Npix_mx has been reached'
                        stop
                     endif
                     pixel_index(Npixel,1)=icube
                     pixel_index(Npixel,2)=ipix
                     pixel_index(Npixel,3)=jpix
                  endif         ! inside
               enddo            ! i
            endif               ! acc_Npixel(iacc,jacc)>0
c     
            if (is_in_pixel) then
               call altitude_in_pixel(dim,x,
     &              Npix,corner_theta,corner_phi,corner_z,
     &              Npixel,pixel_index,z)
c     "pidx" is required to affect material to each face: index of the last pixel "x" belongs to, if any
               do j=1,3
                  pidx(Nv01,j)=pixel_index(Npixel,j)
               enddo            ! j
            else                ! default value
               z=0.0D+0
c     takes a null value if "x" does not belong to any pixel
               do j=1,3
                  pidx(Nv01,j)=0
               enddo            ! j
            endif
            r=planet_radius+z   ! m
            theta=x(2)*pi/180.0D+0 ! rad
            phi=x(1)*pi/180.0D+0 ! rad
            call spher2cart(label,0,r,theta,phi,
     &           vertex(1),vertex(2),vertex(3))
            do j=1,3
               v01(Nv01,j)=vertex(j)
            enddo               ! j
c     --- progress display
            ndone=ndone+1
            fdone=dble(ndone)/dble(ntot)*1.0D+2
            ifdone=floor(fdone)
            if (ifdone.gt.pifdone) then
               do j=1,len+2
                  write(*,"(a)",advance='no') "\b"
               enddo            ! j
               write(*,trim(fmt),advance='no') floor(fdone),' %'
               pifdone=ifdone
            endif
c     progress display ---
         enddo                  ! ji
      enddo                     ! ii
      write(*,*)
c
      Nf01=0
      front_name='air'
      do ii=1,Nint_phi
         do ji=1,Nint_theta
c     (local) index of each vertex for the square face, by recording order
            i1=(Nint_theta+1)*(ii-1)+ji
            i2=(Nint_theta+1)*(ii-1)+ji+1
            i3=(Nint_theta+1)*ii+ji
            i4=(Nint_theta+1)*ii+ji+1
c     make face 1
            Nf01=Nf01+1
            if (Nf.gt.Nf_mx) then
               call error(label)
               write(*,*) 'Nf_mx has been reached'
               stop
            endif
            lidx(1)=i1
            lidx(2)=i3
            lidx(3)=i2
            do j=1,3
               f01(Nf01,j)=Nv+lidx(j)
            enddo               ! j
            call face_material(igroup,cube_index,lidx,pidx,idx)
            call pixel_mtl_name(idx(1),idx(2),idx(3),material,name)
            if (.not.attributed_mtl(idx(1),idx(2),idx(3))) then
               attributed_mtl(idx(1),idx(2),idx(3))=.true.
               Nmat=Nmat+1
               if (Nmat.gt.Nmaterial_mx) then
                  call error(label)
                  write(*,*) 'Nmaterial_mx has been reached'
                  stop
               endif
               materials(Nmat)=trim(name)
               attributed_mtl_idx(idx(1),idx(2),idx(3))=Nmat ! absolute index
            endif
            mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     make face 2
            Nf01=Nf01+1
            if (Nf.gt.Nf_mx) then
               call error(label)
               write(*,*) 'Nf_mx has been reached'
               stop
            endif
            lidx(1)=i2
            lidx(2)=i3
            lidx(3)=i4
            do j=1,3
               f01(Nf01,j)=Nv+lidx(j)
            enddo               ! j
            call face_material(igroup,cube_index,lidx,pidx,idx)
            call pixel_mtl_name(idx(1),idx(2),idx(3),material,name)
            if (.not.attributed_mtl(idx(1),idx(2),idx(3))) then
               attributed_mtl(idx(1),idx(2),idx(3))=.true.
               Nmat=Nmat+1
               if (Nmat.gt.Nmaterial_mx) then
                  call error(label)
                  write(*,*) 'Nmaterial_mx has been reached'
                  stop
               endif
               materials(Nmat)=trim(name)
               attributed_mtl_idx(idx(1),idx(2),idx(3))=Nmat ! absolute index
            endif
            mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
         enddo                  ! j
      enddo                     ! i
c
c     ============================================================================
c     Adding vertices / faces to match the default 1° grid
c     ============================================================================
c
      do j=1,3
         idx(j)=0
      enddo                     ! j
      call pixel_mtl_name(idx(1),idx(2),idx(3),material,name)
      if (.not.attributed_mtl(idx(1),idx(2),idx(3))) then
         attributed_mtl(idx(1),idx(2),idx(3))=.true.
         Nmat=Nmat+1
         if (Nmat.gt.Nmaterial_mx) then
            call error(label)
            write(*,*) 'Nmaterial_mx has been reached'
            stop
         endif
         materials(Nmat)=trim(name)
         attributed_mtl_idx(idx(1),idx(2),idx(3))=Nmat ! absolute index
      endif
c     ----------------------------------------------------------------------------
c     Top boundary
c     ----------------------------------------------------------------------------
      origin(1)=min_phi
      origin(2)=max_theta
      Nint_onedeg=int(max_phi-min_phi)
      Nint=2**n_phi             ! number of elementary squares over a 1° interval
      delta=1.0D+0/dble(Nint)   ! elementary square width
      do i0=1,Nint_onedeg
         phi0=origin(1)+dble(i0-1)
c     Center of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(phi0+0.50D+0)*pi/180.0D+0 ! rad
         theta=(origin(2)+0.50D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         iv_center=Nv+Nv01
c     lower-left corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=phi0*pi/180.0D+0 ! rad
         theta=origin(2)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         iv_ll=Nv+Nv01
         do i=1,Nint
            Nv01=Nv01+1
            if (Nv01.gt.Nv_mx) then
               call error(label)
               write(*,*) 'Nv_mx has been reached'
               stop
            endif
            phi=(phi0+i*delta)*pi/180.0D+0 ! rad
            theta=origin(2)*pi/180.0D+0 ! rad
            r=planet_radius
            call spher2cart(label,0,r,theta,phi,
     &           vertex(1),vertex(2),vertex(3))
            do j=1,3
               v01(Nv01,j)=vertex(j)
            enddo               ! j
            Nf01=Nf01+1
            if (Nf.gt.Nf_mx) then
               call error(label)
               write(*,*) 'Nf_mx has been reached'
               stop
            endif
            f01(Nf01,1)=Nv+Nv01-1
            f01(Nf01,2)=Nv+Nv01
            f01(Nf01,3)=iv_center
            mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
         enddo                  ! i
c     upper-right corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(origin(1)+dble(i0))*pi/180.0D+0   ! rad
         theta=(origin(2)+1.0D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01-1
         f01(Nf01,2)=Nv+Nv01
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     upper-left corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=phi0*pi/180.0D+0   ! rad
         theta=(origin(2)+1.0D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01-1
         f01(Nf01,2)=Nv+Nv01
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     lower-left corner of the 1° square
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01
         f01(Nf01,2)=iv_ll
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      enddo                     ! i0      
c     ----------------------------------------------------------------------------
c     Bottom boundary
c     ----------------------------------------------------------------------------
      origin(1)=min_phi
      origin(2)=min_theta
      Nint_onedeg=int(max_phi-min_phi)
      Nint=2**n_phi             ! number of elementary squares over a 1° interval
      delta=1.0D+0/dble(Nint)   ! elementary square width
      do i0=1,Nint_onedeg
         phi0=origin(1)+dble(i0-1)
c     Center of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(phi0+0.50D+0)*pi/180.0D+0 ! rad
         theta=(origin(2)-0.50D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         iv_center=Nv+Nv01
c     upper-right corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(phi0+1.0D+0)*pi/180.0D+0   ! rad
         theta=origin(2)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         iv_ll=Nv+Nv01
         do i=Nint-1,0,-1
            Nv01=Nv01+1
            if (Nv01.gt.Nv_mx) then
               call error(label)
               write(*,*) 'Nv_mx has been reached'
               stop
            endif
            phi=(phi0+i*delta)*pi/180.0D+0 ! rad
            theta=origin(2)*pi/180.0D+0 ! rad
            r=planet_radius
            call spher2cart(label,0,r,theta,phi,
     &           vertex(1),vertex(2),vertex(3))
            do j=1,3
               v01(Nv01,j)=vertex(j)
            enddo               ! j
            Nf01=Nf01+1
            if (Nf.gt.Nf_mx) then
               call error(label)
               write(*,*) 'Nf_mx has been reached'
               stop
            endif
            f01(Nf01,1)=Nv+Nv01-1
            f01(Nf01,2)=Nv+Nv01
            f01(Nf01,3)=iv_center
            mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
         enddo                  ! i
c     lower-left corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=phi0*pi/180.0D+0   ! rad
         theta=(origin(2)-1.0D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01-1
         f01(Nf01,2)=Nv+Nv01
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     lower-right corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(phi0+1.0D+0)*pi/180.0D+0   ! rad
         theta=(origin(2)-1.0D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01-1
         f01(Nf01,2)=Nv+Nv01
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     upper-right corner of the 1° square
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01
         f01(Nf01,2)=iv_ll
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      enddo                     ! i0
c     ----------------------------------------------------------------------------
c     Left boundary
c     ----------------------------------------------------------------------------
      origin(1)=min_phi
      origin(2)=min_theta
      Nint_onedeg=int(max_theta-min_theta)
      Nint=2**n_theta           ! number of elementary squares over a 1° interval
      delta=1.0D+0/dble(Nint)   ! elementary square width
      do i0=1,Nint_onedeg
         theta0=origin(2)+dble(i0-1)
c     Center of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(origin(1)-0.50D+0)*pi/180.0D+0 ! rad
         theta=(theta0+0.50D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         iv_center=Nv+Nv01
c     lower-right corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=origin(1)*pi/180.0D+0 ! rad
         theta=theta0*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         iv_ll=Nv+Nv01
         do i=1,Nint
            Nv01=Nv01+1
            if (Nv01.gt.Nv_mx) then
               call error(label)
               write(*,*) 'Nv_mx has been reached'
               stop
            endif
            phi=origin(1)*pi/180.0D+0 ! rad
            theta=(theta0+i*delta)*pi/180.0D+0 ! rad
            r=planet_radius
            call spher2cart(label,0,r,theta,phi,
     &           vertex(1),vertex(2),vertex(3))
            do j=1,3
               v01(Nv01,j)=vertex(j)
            enddo               ! j
            Nf01=Nf01+1
            if (Nf.gt.Nf_mx) then
               call error(label)
               write(*,*) 'Nf_mx has been reached'
               stop
            endif
            f01(Nf01,1)=Nv+Nv01-1
            f01(Nf01,2)=Nv+Nv01
            f01(Nf01,3)=iv_center
            mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
         enddo                  ! i
c     upper-left corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(origin(1)-1.0D+0)*pi/180.0D+0 ! rad
         theta=(theta0+1.0D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01-1
         f01(Nf01,2)=Nv+Nv01
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     lower-left corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(origin(1)-1.0D+0)*pi/180.0D+0   ! rad
         theta=theta0*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01-1
         f01(Nf01,2)=Nv+Nv01
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     lower-right corner of the 1° square
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01
         f01(Nf01,2)=iv_ll
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      enddo                     ! i0
c     ----------------------------------------------------------------------------
c     Right boundary
c     ----------------------------------------------------------------------------
      origin(1)=max_phi
      origin(2)=min_theta
      Nint_onedeg=int(max_theta-min_theta)
      Nint=2**n_theta           ! number of elementary squares over a 1° interval
      delta=1.0D+0/dble(Nint)   ! elementary square width
      do i0=1,Nint_onedeg
         theta0=origin(2)+dble(i0-1)
c     Center of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(origin(1)+0.50D+0)*pi/180.0D+0 ! rad
         theta=(theta0+0.50D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         iv_center=Nv+Nv01
c     upper-left corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=origin(1)*pi/180.0D+0 ! rad
         theta=(theta0+1.0D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         iv_ll=Nv+Nv01
         do i=Nint-1,0,-1
            Nv01=Nv01+1
            if (Nv01.gt.Nv_mx) then
               call error(label)
               write(*,*) 'Nv_mx has been reached'
               stop
            endif
            phi=origin(1)*pi/180.0D+0 ! rad
            theta=(theta0+i*delta)*pi/180.0D+0 ! rad
            r=planet_radius
            call spher2cart(label,0,r,theta,phi,
     &           vertex(1),vertex(2),vertex(3))
            do j=1,3
               v01(Nv01,j)=vertex(j)
            enddo               ! j
            Nf01=Nf01+1
            if (Nf.gt.Nf_mx) then
               call error(label)
               write(*,*) 'Nf_mx has been reached'
               stop
            endif
            f01(Nf01,1)=Nv+Nv01-1
            f01(Nf01,2)=Nv+Nv01
            f01(Nf01,3)=iv_center
            mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
         enddo                  ! i
c     lower-right corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(origin(1)+1.0D+0)*pi/180.0D+0 ! rad
         theta=theta0*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01-1
         f01(Nf01,2)=Nv+Nv01
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     upper-right corner of the 1° square
         Nv01=Nv01+1
         if (Nv01.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         phi=(origin(1)+1.0D+0)*pi/180.0D+0   ! rad
         theta=(theta0+1.0D+0)*pi/180.0D+0 ! rad
         r=planet_radius
         call spher2cart(label,0,r,theta,phi,
     &        vertex(1),vertex(2),vertex(3))
         do j=1,3
            v01(Nv01,j)=vertex(j)
         enddo                  ! j
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01-1
         f01(Nf01,2)=Nv+Nv01
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     upper-left corner of the 1° square
         Nf01=Nf01+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
         f01(Nf01,1)=Nv+Nv01
         f01(Nf01,2)=iv_ll
         f01(Nf01,3)=iv_center
         mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      enddo                     ! i0
c     ----------------------------------------------------------------------------
c     Corners !
c     ----------------------------------------------------------------------------
c     lower-left corner
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=(min_phi-1.0D+0)*pi/180.0D+0 ! rad
      theta=(min_theta-1.0D+0)*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=min_phi*pi/180.0D+0   ! rad
      theta=(min_theta-1.0D+0)*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=min_phi*pi/180.0D+0   ! rad
      theta=min_theta*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=(min_phi-1.0D+0)*pi/180.0D+0 ! rad
      theta=min_theta*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nf01=Nf01+1
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf_mx has been reached'
         stop
      endif
      f01(Nf01,1)=Nv+Nv01-3
      f01(Nf01,2)=Nv+Nv01-2
      f01(Nf01,3)=Nv+Nv01-1
      mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      Nf01=Nf01+1
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf_mx has been reached'
         stop
      endif
      f01(Nf01,1)=Nv+Nv01-3
      f01(Nf01,2)=Nv+Nv01-1
      f01(Nf01,3)=Nv+Nv01
      mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     lower-right corner
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=max_phi*pi/180.0D+0 ! rad
      theta=(min_theta-1.0D+0)*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=(max_phi+1.0D+0)*pi/180.0D+0   ! rad
      theta=(min_theta-1.0D+0)*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=(max_phi+1.0D+0)*pi/180.0D+0   ! rad
      theta=min_theta*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=max_phi*pi/180.0D+0 ! rad
      theta=min_theta*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nf01=Nf01+1
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf_mx has been reached'
         stop
      endif
      f01(Nf01,1)=Nv+Nv01-3
      f01(Nf01,2)=Nv+Nv01-2
      f01(Nf01,3)=Nv+Nv01-1
      mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      Nf01=Nf01+1
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf_mx has been reached'
         stop
      endif
      f01(Nf01,1)=Nv+Nv01-3
      f01(Nf01,2)=Nv+Nv01-1
      f01(Nf01,3)=Nv+Nv01
      mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     upper-right corner
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=max_phi*pi/180.0D+0 ! rad
      theta=max_theta*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=(max_phi+1.0D+0)*pi/180.0D+0   ! rad
      theta=max_theta*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=(max_phi+1.0D+0)*pi/180.0D+0   ! rad
      theta=(max_theta+1.0D+0)*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=max_phi*pi/180.0D+0 ! rad
      theta=(max_theta+1.0D+0)*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nf01=Nf01+1
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf_mx has been reached'
         stop
      endif
      f01(Nf01,1)=Nv+Nv01-3
      f01(Nf01,2)=Nv+Nv01-2
      f01(Nf01,3)=Nv+Nv01-1
      mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      Nf01=Nf01+1
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf_mx has been reached'
         stop
      endif
      f01(Nf01,1)=Nv+Nv01-3
      f01(Nf01,2)=Nv+Nv01-1
      f01(Nf01,3)=Nv+Nv01
      mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     upper-left corner
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=(min_phi-1.0D+0)*pi/180.0D+0 ! rad
      theta=max_theta*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=min_phi*pi/180.0D+0   ! rad
      theta=max_theta*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=min_phi*pi/180.0D+0   ! rad
      theta=(max_theta+1.0D+0)*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nv01=Nv01+1
      if (Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv_mx has been reached'
         stop
      endif
      phi=(min_phi-1.0D+0)*pi/180.0D+0 ! rad
      theta=(max_theta+1.0D+0)*pi/180.0D+0 ! rad
      r=planet_radius
      call spher2cart(label,0,r,theta,phi,
     &     vertex(1),vertex(2),vertex(3))
      do j=1,3
         v01(Nv01,j)=vertex(j)
      enddo                     ! j
      Nf01=Nf01+1
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf_mx has been reached'
         stop
      endif
      f01(Nf01,1)=Nv+Nv01-3
      f01(Nf01,2)=Nv+Nv01-2
      f01(Nf01,3)=Nv+Nv01-1
      mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
      Nf01=Nf01+1
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf_mx has been reached'
         stop
      endif
      f01(Nf01,1)=Nv+Nv01-3
      f01(Nf01,2)=Nv+Nv01-1
      f01(Nf01,3)=Nv+Nv01
      mtl_idx(Nf+Nf01)=attributed_mtl_idx(idx(1),idx(2),idx(3)) ! previously exists in the case attributed_mtl(idx(1),idx(2),idx(3))=T
c     
      do ivertex=1,Nv01
         Nv=Nv+1
         if (Nv.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv=',Nv
            write(*,*) '> Nv_mx=',Nv_mx
            stop
         endif
         do j=1,3
            vertices(Nv,j)=v01(ivertex,j)
         enddo                  ! j
      enddo                     ! iv
c     
      do iface=1,Nf01
         Nf=Nf+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf=',Nf
            write(*,*) '> Nf_mx=',Nf_mx
            stop
         endif
         do j=1,3
            faces(Nf,j)=f01(iface,j)
         enddo                  ! j
      enddo                     ! iface
c     
      return
      end
      


      subroutine face_material(igroup,cube_index,face,pidx,idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the material for a given face
c     
c     Input:
c       + igroup: index of the group
c       + cube_index: index of every data cube in each group
c       + face: local indexes of the 3 vertex for the face
c       + pidx: indexes of the pixel for each vertex
c     
c     Output:
c       + idx: the 3 indexes that will be used to generate the name of the material
c     
c     I/O
      integer igroup
      integer cube_index(1:Ngroup_mx,1:Nfig_mx)
      integer face(1:3)
      integer pidx(1:Nv_mx,1:3)
      integer idx(1:3)
c     temp
      integer v(1:3)
      integer i,j,ig
      integer Ng,n_in_g(1:3)
      integer g_idx(1:3,1:3),g0
      logical is_identical,g0_found
c     label
      character*(Nchar_mx) label
      label='subroutine face_material'
c     
c     The 3 vertices for the face are classified in "groups".
c     These groups are defined by the VIMS pixel each vertex belongs to.
c     If all 3 vertices belong to the same pixel, only one groupe is created.
c     At the contrary, if each vertex belongs to a different pixel, we
c     end up with 3 groups.
c     
      Ng=0
      do i=1,3                  ! i: index of the vertex
         do j=1,3
            v(j)=pidx(face(i),j)
         enddo                  ! j
         g0_found=.false.
         do ig=1,Ng             ! ig: index of the group
            is_identical=.true.
            do j=1,3
               if (v(j).ne.g_idx(ig,j)) then
                  is_identical=.false.
               endif
            enddo               ! j
            if (is_identical) then
               g0_found=.true.
               g0=ig
               goto 111
            endif
         enddo                  ! ig
 111     continue
         if (g0_found) then
            n_in_g(ig)=n_in_g(ig)+1
         else                   ! create new group and affect vertex to this group
            Ng=Ng+1
            n_in_g(Ng)=1
            do j=1,3
               g_idx(ig,j)=v(j)
            enddo               ! j
         endif
      enddo                     ! i
c     
c     material policy:
c     + if only one group is found, then the face is provided with the material
c     of that group (whether it is the default material or a actual pixel)
c     + if 2 groups have been found, the face is provided with the material of
c     the group that counts 2 verices
c     + if 3 groups have been found, the face is provided with the material
c     of the first group
c
c     default values
      do j=1,3
         idx(j)=0
      enddo                     ! j
      if (Ng.eq.1) then
         if (g_idx(1,1).gt.0) then
            idx(1)=cube_index(igroup,g_idx(1,1))
            idx(2)=g_idx(1,2)
            idx(3)=g_idx(1,3)
         endif
      else if (Ng.eq.2) then
         if (n_in_g(1).gt.n_in_g(2)) then
            if (g_idx(1,1).gt.0) then
               idx(1)=cube_index(igroup,g_idx(1,1))
               idx(2)=g_idx(1,2)
               idx(3)=g_idx(1,3)
            endif
         else
            if (g_idx(2,1).gt.0) then
               idx(1)=cube_index(igroup,g_idx(2,1))
               idx(2)=g_idx(2,2)
               idx(3)=g_idx(2,3)
            endif
         endif
      else if (Ng.eq.3) then
         if (g_idx(1,1).gt.0) then
            idx(1)=cube_index(igroup,g_idx(1,1))
            idx(2)=g_idx(1,2)
            idx(3)=g_idx(1,3)
         endif
      else
         call error(label)
         write(*,*) 'Ng=',Ng
         write(*,*) 'that should not be possible !'
         stop
      endif
      
      return
      end

      

      subroutine analyze_cube(Npix,corner_theta,corner_phi,
     &     min_phi,max_phi,min_theta,max_theta,dphi,dtheta)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get main information about a given VIMS data cube
c     
c     Input:
c       + Npix: number of pixel in theta/phi directions
c       + corner_theta: latitude of each corner for each pixel [deg]
c       + corner_phi: longitude of each corner for each pixel [deg]
c     
c     Output:
c       + min_phi: lower longitude of the bounding box (0,360) [deg]
c       + max_phi: higher longitude of the bounding box (0,360) [deg]
c       + min_theta: lower latitude of the bounding box (-90,90) [deg]
c       + max_theta: higher latitude of the bounding box (-90,90) [deg]
c       + dphi: longitude resolution [deg]
c       + dtheta: latitude resolution [deg]
c       + corner_phi: ajusted for longitudes in (0,360)° range
c     
c     I/O
      integer Npix(1:2)
      double precision corner_theta(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Npix_mx,1:Npix_mx,1:4)
      double precision min_phi,max_phi
      double precision min_theta,max_theta
      double precision dphi,dtheta
c     temp
      integer ipix,jpix,icorner
      double precision sum_dtheta,sum_dphi
      double precision avg_dtheta,avg_dphi
c     label
      character*(Nchar_mx) label
      label='subroutine analyze_cube'
      
      max_theta=corner_theta(1,1,1)
      max_phi=corner_phi(1,1,1)
      min_theta=corner_theta(1,1,1)
      min_phi=corner_phi(1,1,1)
      sum_dtheta=0.0D+0
      sum_dphi=0.0D+0
      do ipix=1,Npix(1)
         do jpix=1,Npix(2)
            do icorner=1,4
               if (corner_theta(ipix,jpix,icorner).gt.max_theta) then
                  max_theta=corner_theta(ipix,jpix,icorner)
               endif
               if (corner_theta(ipix,jpix,icorner).lt.min_theta) then
                  min_theta=corner_theta(ipix,jpix,icorner)
               endif
               if (corner_phi(ipix,jpix,icorner).gt.max_phi) then
                  max_phi=corner_phi(ipix,jpix,icorner)
               endif
               if (corner_phi(ipix,jpix,icorner).lt.min_phi) then
                  min_phi=corner_phi(ipix,jpix,icorner)
               endif
            enddo               ! icorner
            sum_dtheta=sum_dtheta
     &           +dabs(corner_theta(ipix,jpix,1)
     &           -corner_theta(ipix,jpix,4))
     &           +dabs(corner_theta(ipix,jpix,2)
     &           -corner_theta(ipix,jpix,3))
            sum_dphi=sum_dphi
     &           +dabs(corner_phi(ipix,jpix,2)
     &           -corner_phi(ipix,jpix,1))
     &           +dabs(corner_phi(ipix,jpix,3)
     &           -corner_phi(ipix,jpix,4))
         enddo                  ! jpix
      enddo                     ! ipix
c     round to the lower/higher integer value
      min_theta=dble(int(min_theta))
      max_theta=dble(int(max_theta)+1)
      min_phi=dble(int(min_phi))
      max_phi=dble(int(max_phi)+1)
c     avg_dtheta and avg_dphi: average latitude and longitude length of a VIMS pixel
      avg_dtheta=sum_dtheta/dble(2*Npix(1)*Npix(2))
      avg_dphi=sum_dphi/dble(2*Npix(1)*Npix(2))
c     dtheta and dphi: resolution over latitude and longitude
      dtheta=avg_dtheta/2.0D+0  ! resolution over latitude
      dphi=avg_dphi/2.0D+0      ! resolution over longitude
c     Adjust longitudes to [0-360]°
      if (min_phi.lt.0.0D+0) then
         min_phi=min_phi+360.0D+0
         max_phi=max_phi+360.0D+0
         do ipix=1,Npix(1)
            do jpix=1,Npix(2)
               do icorner=1,4
                  corner_phi(ipix,jpix,icorner)=
     &                 corner_phi(ipix,jpix,icorner)+360.0D+0
               enddo            ! icorner
            enddo               ! jpix
         enddo                  ! ipix
      endif                     ! min_theta < 0

      return
      end
      
      

      subroutine altitude_in_pixel(dim,x,
     &     Npix,corner_theta,corner_phi,corner_z,
     &     Npixel,pixel_index,z)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to interpolate the altitude for a given position
c     
c     Input:
c       + dim: dimension of space
c       + x: (phi,theta) position [deg, deg]
c       + Npix: number of pixels along theta/phi directions
c       + corner_theta: value of theta for each corner of each pixel
c       + corner_phi: value of phi for each corner of each pixel
c       + corner_z: value of z for each corner of each pixel
c       + Npixel: number of pixels position "x" belongs to
c       + pixel_index: index of each pixels "x" belgons to, when is_in_pixel=Tl
c     
c     Output:
c       + z: altitude for position (phi,theta)
c     
c     I/O
      integer dim
      double precision x(1:Ndim_mx-1)
      integer Npix(1:Nfig_mx,1:2)
      double precision corner_theta(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      integer Npixel
      integer pixel_index(1:Npix_mx,1:3)
      double precision z
c     temp
      double precision altitude(1:Npix_mx,1:4)
      double precision distance
      double precision weight(1:Npix_mx,1:4),sum_w
      integer ipix,icorner
      double precision P1(1:2)
      double precision P2(1:2)
c     label
      character*(Nchar_mx) label
      label='subroutine altitude_in_pixel'

      P1(1)=x(2)*pi/180.0D+0     ! theta [rad]
      P1(2)=x(1)*pi/180.0D+0     ! phi [rad]
c
      sum_w=0.0D+0
      do ipix=1,Npixel
         do icorner=1,4
            P2(1)=corner_theta(pixel_index(ipix,1),pixel_index(ipix,2),pixel_index(ipix,3),icorner)*pi/180.0D+0 ! [rad]
            P2(2)=corner_phi(pixel_index(ipix,1),pixel_index(ipix,2),pixel_index(ipix,3),icorner)*pi/180.0D+0 ! [rad]
            call distance_on_sphere(dim,P1,P2,1.0D+0,distance)
            if (distance.eq.0.0D+0) then
               z=corner_z(pixel_index(ipix,1),pixel_index(ipix,2),pixel_index(ipix,3),icorner)
               goto 666
            else
               weight(ipix,icorner)=1.0D+0/distance
               sum_w=sum_w+weight(ipix,icorner)
            endif
         enddo                  ! icorner
      enddo                     ! ipix
c      
      z=0.0D+0
      do ipix=1,Npixel
         do icorner=1,4
            weight(ipix,icorner)=weight(ipix,icorner)/sum_w
            z=z+corner_z(pixel_index(ipix,1),pixel_index(ipix,2),pixel_index(ipix,3),icorner)*weight(ipix,icorner)
         enddo                  ! icorner
      enddo                     ! ipix
 666  continue
c      
      return
      end


      
      subroutine read_cube_definition(definition_file,Ncube,definition)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the file that provides the spatial / spectral definition of each cube
c     
c     Input:
c       + definition_file: file to read
c     
c     Output:
c       + Ncube: number of cubes found
c       + definition(icube,1): pixel definition 1
c       + definition(icube,2): pixel definition 2
c       + definition(icube,3): spectral definition
c     
c     I/O
      character*(Nchar_mx) definition_file
      integer Ncube
      integer definition(1:Ncube_mx,1:3)
c     temp
      integer ios,ok
      logical keep_looking
      integer tmp1,tmp2,tmp3
c     label
      character*(Nchar_mx) label
      label='subroutine read_cube_definition'

      open(17,file=trim(definition_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(definition_file)
         stop
      endif
      read(17,*)                ! text line
      keep_looking=.true.
      Ncube=0
      do while (keep_looking)
         read(17,*,iostat=ok) tmp1,tmp2,tmp3
         if (ok.ne.0) then
            keep_looking=.false.
         else
            Ncube=Ncube+1
            if (Ncube.gt.Ncube_mx) then
               call error(label)
               write(*,*) 'Ncube_mx has been reached'
               stop
            endif
            definition(Ncube,1)=tmp1
            definition(Ncube,2)=tmp2
            definition(Ncube,3)=tmp3
         endif                  ! ok
      enddo                     ! while (keep_looking)
      close(17)
      if (Ncube.le.0) then
         call error(label)
         write(*,*) 'No information found in file: ',trim(definition_file)
         stop
      endif
      
      return
      end
