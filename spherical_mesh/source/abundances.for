      subroutine interpolate_molar_fraction(Nlev,pressure,x,P,molar_fraction)
      implicit none
      include 'max.inc'
c     
c     Purpose: to inter/extra-polate molar fractions
c     
c     Input:
c       + Nlev: number of reference levels
c       + pressure: reference pressure levels [Pa]
c       + x: reference molar fraction levels [mol/mol]
c       + P: interpolation pressure [Pa]
c     
c     Output:
c       + molar_fraction: interpolated value of the molar fraction @ P [mol/mol]
c     
c     I/O
      integer Nlev
      double precision pressure(1:Nlev_mx)
      double precision x(1:Nlev_mx)
      double precision P
      double precision molar_fraction
c     temp
      integer i,interval
      logical interval_found
c     label
      character*(Nchar_mx) label
      label='subroutine interpolate_molar_fraction'

      interval_found=.false.
      do i=1,Nlev-1
         if ((P.le.pressure(i)).and.(P.ge.pressure(i+1))) then
            interval_found=.true.
            interval=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.interval_found) then
         if (P.ge.pressure(1)) then
            interval_found=.true.
            interval=1
         endif
         if (P.le.pressure(Nlev)) then
            interval_found=.true.
            interval=Nlev-1
         endif
      endif
      if (.not.interval_found) then
         call error(label)
         write(*,*) 'Could not find interval for P=',P
         do i=1,Nlev
            write(*,*) 'pressure(',i,')=',pressure(i)
         enddo                  ! i
         stop
      endif
c     linearly interpolate with pressure
      molar_fraction=x(interval)+(x(interval+1)-x(interval))*(P-pressure(interval))/(pressure(interval+1)-pressure(interval))
c     track negative values (that may happen)
      if (molar_fraction.lt.0.0D+0) then
         molar_fraction=0.0D+0
      endif

      return
      end
