      subroutine read_phase_file(file_in,
     &     Nlambda,Nangle,lambda,angle,phase_function)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to read a phase function file for Titan haze
c     
c     Input:
c       + file_in: file to read
c     
c     Output:
c       + Nlambda: number of wavelength values
c       + Nangle: number of angles the phase function is discretized over
c       + lambda: wavelength values [nm]
c       + angle: discretization angles (0-pi) [rad]
c       + phase_function: phase function
c     
c     I/O
      character*(Nchar_mx) file_in
      integer Nlambda
      integer Nangle
      double precision lambda(1:Nlambda_mx)
      double precision angle(1:Nangle_mx)
      double precision phase_function(1:Nlambda_mx,1:Nangle_mx)
c     temp
      integer ios,raf,ilambda,i,Nint,iangle
      logical keep_reading
      double precision val1,dt(1:Nlambda_mx),dmu
      double precision sum,normalization_factor
c     label
      character*(Nchar_mx) label
      label='subroutine read_phase_file'

      open(12,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(file_in)
         stop
      endif
      read(12,*) Nlambda
      if (Nlambda.lt.1) then
         call error(label)
         write(*,*) 'Nlambda=',Nlambda
         write(*,*) 'should be positive'
         stop
      endif
      if (Nlambda.gt.Nlambda_mx) then
         call error(label)
         write(*,*) 'Nlambda=',Nlambda
         write(*,*) '> Nlambda_mx=',Nlambda_mx
         stop
      endif
      rewind(12)
      read(12,*) raf,(lambda(ilambda),ilambda=1,Nlambda)
      do ilambda=1,Nlambda
         if (lambda(ilambda).lt.0.0D+0) then
            call error(label)
            write(*,*) 'lambda(',ilambda,')=',lambda(ilambda)
            write(*,*) 'should be positive'
            stop
         endif
      enddo                     ! i
      do ilambda=2,Nlambda
         if (lambda(ilambda).le.lambda(ilambda-1)) then
            call error(label)
            write(*,*) 'lambda(',ilambda,')=',lambda(ilambda)
            write(*,*) 'should be > lambda(',ilambda-1,')=',lambda(ilambda-1)
            stop
         endif
      enddo                     ! i
c     Read angle and the phase array
      Nangle=0
      keep_reading=.true.
      do while (keep_reading)
         read(12,*,iostat=ios) val1,(dt(ilambda),ilambda=1,Nlambda) ! deg/nm
         if (ios.ne.0) then
            keep_reading=.false.
            goto 111
         else
            Nangle=Nangle+1
            if (Nangle.gt.Nangle_mx) then
               call error(label)
               write(*,*) 'Nangle_mx has been reached'
               stop
            endif
            angle(Nangle)=val1*pi/180.0D+0 ! deg -> rad
            do ilambda=1,Nlambda
               phase_function(ilambda,Nangle)=dt(ilambda)
            enddo               ! i
         endif                  ! ios.ne.0
 111     continue
      enddo                     ! while (keep_reading)
      close(12)

c     Normalization; we want int_{4\pi}[Phi(omega)d(omega)]=1
c     therefore with d(omega)=sin(theta)d(theta)d(phi)
c     we obtain: int_{0}^{\pi}[sin(theta)Phi(theta)d(theta)]=1/(2\pi)
      Nint=Nangle-1
      do ilambda=1,Nlambda
         sum=0.0D+0
         do i=1,Nint
            dmu=dcos(angle(i))-dcos(angle(i+1))
            sum=sum+(phase_function(ilambda,i)+phase_function(ilambda,i+1))*dmu
         enddo                  ! i
         sum=sum
c     sum should be devided by 2, and the normalization factor should be equal to
c     sum*2*pi, so the two "2" cancel out each other
         normalization_factor=sum*pi
         do iangle=1,Nangle
            phase_function(ilambda,iangle)=phase_function(ilambda,iangle)/normalization_factor
         enddo                  ! iangle
c     Debug
c$$$         write(*,*) 'ilambda=',ilambda
c$$$         write(*,*) 'Nint=',Nint
c$$$         write(*,*) 'Nangle=',Nangle
c$$$         write(*,*) 'i / angle [rad] / mu=cos(angle) / phase'
c$$$         do iangle=1,Nangle
c$$$            write(*,*) iangle-1,angle(iangle),dcos(angle(iangle)),phase_function(ilambda,iangle)
c$$$         enddo                  ! iangle
c$$$         sum=0.0D+0
c$$$         do iangle=1,Nangle-1
c$$$            dmu=dcos(angle(iangle))-dcos(angle(iangle+1))
c$$$            sum=sum+(phase_function(ilambda,iangle)+phase_function(ilambda,iangle+1))*dmu
c$$$         enddo                  ! iangle
c$$$         normalization_factor=sum*pi
c$$$         write(*,*) 'normalization_factor=',normalization_factor
c$$$         stop
c     Debug        
      enddo                     ! ilambda
      
      return
      end
