c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine complete_list(folder_in,
     &     Nfolders_out,folders_out,Nfiles_out,files_out)
      implicit none
      include 'max.inc'
c
c     Purpose: to get the complete list of all sub-folders and all files
c     from an initial folder.
c
c     Inputs:
c       + folder_in: input folder (whose content has to be listed)
c
c     Outputs:
c       + Nfolders_out: number of folders that reside within the input folder
c       + folders_out: names of the folders that reside within the input folder
c       + Nfiles_out: number of files that reside within the input folder
c       + files_out: names of the files that reside within the input folder
c

c     I/O
      character*(Nchar_mx) folder_in
      integer Nfolders_out,Nfiles_out
      character*(Nchar_mx) folders_out(1:Nfolders_mx)
      character*(Nchar_mx) files_out(1:Nfiles_mx)
c     temp
      integer cf,j
      character*(Nchar_mx) tfolder_in
      integer tNfolders_out,tNfiles_out
      character*(Nchar_mx) tfolders_out(1:Nfolders_mx)
      character*(Nchar_mx) tfiles_out(1:Nfiles_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine complete_list'

      cf=0
      tfolder_in=trim(folder_in)
      call base_list(tfolder_in,
     &     tNfolders_out,tfolders_out,tNfiles_out,tfiles_out)
      Nfolders_out=0
      Nfiles_out=0
      call add_temp(tNfolders_out,tfolders_out,tNfiles_out,tfiles_out,
     &     Nfolders_out,folders_out,Nfiles_out,files_out)
      do while (cf.lt.Nfolders_out)
         cf=cf+1
         tfolder_in=trim(folders_out(cf))
         call base_list(tfolder_in,
     &        tNfolders_out,tfolders_out,tNfiles_out,tfiles_out)
         call add_temp(tNfolders_out,tfolders_out,
     &        tNfiles_out,tfiles_out,
     &        Nfolders_out,folders_out,Nfiles_out,files_out)
c     Debug
c         write(*,*) 'cf=',cf,' Nfolders_out=',Nfolders_out
c     Debug
      enddo

      return
      end



      subroutine add_temp(tNfolders,tfolders,tNfiles,tfiles,
     &     Nfolders,folders,Nfiles,files)
      implicit none
      include 'max.inc'
c
c     Purpose: to add temporary folders and files to the complete list
c     of folders and files
c
c     Inputs:
c       + tNfolders: temporary number of folders
c       + tfolders: temporary list of folders
c       + tNfiles: temporary number of files
c       + tfiles: temporary list of files
c
c     I/O:
c       + Nfolders: total number of folders (has to be updated)
c       + folders: complete list of folders (has to be updated)
c       + Nfiles: total number of files (has to be updated)
c       + files: complete list of files (has to be updated)
c

c     I/O
      integer tNfolders,tNfiles
      character*(Nchar_mx) tfolders(1:Nfolders_mx)
      character*(Nchar_mx) tfiles(1:Nfiles_mx)
      integer Nfolders,Nfiles
      character*(Nchar_mx) folders(1:Nfolders_mx)
      character*(Nchar_mx) files(1:Nfiles_mx)
c     temp
      integer cf
c     label
      character*(Nchar_mx) label
      label='subroutine add_temp'

      do cf=1,tNfolders
         Nfolders=Nfolders+1
         if (Nfolders.gt.Nfolders_mx) then
            call error(label)
            write(*,*) 'Nfolders reached Nfolders_mx'
            stop
         else
            folders(Nfolders)=trim(tfolders(cf))
         endif
      enddo ! cf
      do cf=1,tNfiles
         Nfiles=Nfiles+1
         if (Nfiles.gt.Nfiles_mx) then
            call error(label)
            write(*,*) 'Nfiles reached Nfiles_mx'
            stop
         else
            files(Nfiles)=trim(tfiles(cf))
         endif
      enddo ! cf

      return
      end



      subroutine base_list(folder_in,
     &     Nfolders_out,folders_out,Nfiles_out,files_out)
      implicit none
      include 'max.inc'
c
c     Purpose: to list all folders and files in a given folder
c
c     Inputs:
c       + folder_in: input folder (whose content has to be listed)
c
c     Outputs:
c       + Nfolders_out: number of folders that reside within the input folder
c       + folders_out: names of the folders that reside within the input folder
c       + Nfiles_out: number of files that reside within the input folder
c       + files_out: names of the files that reside within the input folder
c

c     I/O
      character*(Nchar_mx) folder_in
      integer Nfolders_out,Nfiles_out
      character*(Nchar_mx) folders_out(1:Nfolders_mx)
      character*(Nchar_mx) files_out(1:Nfiles_mx)
c     temp
      integer islink,file,folder
      character*(Nchar_mx) ref,link,path
      integer exist
      character*(Nchar_mx) command,line
      character*(Nchar_mx) list_file
      integer nl,l,islash
      integer Nquotes
      integer positions(1:Nquotes_mx)
c     Debug
      integer i
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine base_list'

      call inquire_folder(folder_in,exist)
      if (exist.eq.0) then
         call error(label)
         write(*,*) 'Folder does not exist:'
         write(*,*) trim(folder_in)
         stop
      endif

      list_file='./list.txt'
      call add_slash_if_absent(folder_in)
c     l: list files
c     B: do not list implied entries ending with tilde
c     A: almost all (do not list . and ..)
c     p: append / indicator to directories
c     Q: enclose entry names in double quotes
c     
c     update 11/01/2011:
c     MacOS version of 'ls' does not accept the -Q option; I had to
c     install the "fileutils" package from fink in order to enable
c     the unix-like version of 'ls'
c
      command='ls -lBApQ '
     &     //trim(folder_in)
     &     //' > '
     &     //trim(list_file)
      call exec(command)
      call get_nlines(list_file,nl)
c     Debug
c      write(*,*) 'nl=',nl
c     Debug
      Nfolders_out=0
      Nfiles_out=0

      open(16,file=trim(list_file))
      do l=1,nl
         read(16,'(a)') line
 111     continue
         call find_quotes(line,Nquotes,positions)
c     Debug
c         write(*,*) 'Nquotes=',Nquotes
c         do i=1,Nquotes
c            write(*,*) 'positions(',i,')=',positions(i)
c         enddo
c     Debug
         islink=0
         if (Nquotes.ne.0) then
            if (Nquotes.eq.4) then ! most probably a symbolic link
               call is_link(folder_in,line,islink,file,folder,link,ref)
               if (islink.eq.1) then
                  if (((file.eq.1).and.(folder.eq.0)).or.
     &                 ((file.eq.0).and.(folder.eq.1))) then
                     path=trim(link)
                     goto 222
                  else
                     call error(label)
                     write(*,*) 'running "is_link" on line='
                     write(*,*) trim(line)
                     write(*,*) 'results are inconsistent:'
                     write(*,*) 'file=',file
                     write(*,*) 'folder=',folder
                     stop
                  endif
               else
                  call error(label)
                  write(*,*) 'Number of quotes in line='
                  write(*,*) trim(line)
                  write(*,*) 'contains Nquotes=',Nquotes,' quotes'
                  write(*,*) 'but result of "is_link" is:'
                  write(*,*) 'islink=',islink
                  stop
               endif ! islink=1
            endif
            if (Nquotes.ne.2) then
               call error(label)
               write(*,*) 'line analyzed is:'
               write(*,*) line
               write(*,*) 'Number of quotes=',Nquotes
               stop
            else
               call detect_slash_at_end(line,islash)
               path=line(positions(1)+1:positions(2)-1)
               if (islash.eq.1) then
                  folder=1
                  file=0
               else
                  folder=0
                  file=1
               endif
            endif
 222        continue

            if ((folder.eq.1).and.(file.eq.0)) then
               Nfolders_out=Nfolders_out+1
               if (Nfolders_out.gt.Nfolders_mx) then
                  call error(label)
                  write(*,*) 'Nfolders_out reached Nfolders_mx'
                  stop
               else
                  if (islink.eq.0) then
                     folders_out(Nfolders_out)=
     &                    trim(folder_in)
     &                    //trim(path)
                  else
                     folders_out(Nfolders_out)=trim(path)
                  endif
               endif
            else if ((folder.eq.0).and.(file.eq.1)) then
               Nfiles_out=Nfiles_out+1
               if (Nfiles_out.gt.Nfiles_mx) then
                  call error(label)
                  write(*,*) 'Nfiles_out reached Nfiles_mx'
                  stop
               else
                  if (islink.eq.0) then
                     files_out(Nfiles_out)=
     &                    trim(folder_in)
     &                    //trim(path)
                  else
                     files_out(Nfiles_out)=trim(path)
                  endif
               endif
            else
               call error(label)
               write(*,*) 'inconsistency between:'
               write(*,*) 'file=',file
               write(*,*) 'folder=',folder
               stop
            endif
         endif ! Nquotes.ne.0
      enddo ! line
      close(16)

      return
      end


      subroutine is_link(folder_in,line,islink,file,folder,link,ref)
      implicit none
      include 'max.inc'
c
c     Purpose: to detect symbolic links within lines returned by the "ls" command
c
c     Inputs:
c       + folder_in: folder the supposed link has been found in
c       + line: output from the "ls" command
c
c     Outputs:
c       + islink: 1 if "line" contains a symbolic link; 0 otherwise
c       + file: in the case a link exists, file=1 if the link refers to a file
c       + folder: in the case a likn exists, folder=1 if the link refers to a folder
c       + link: symbolic link
c       + ref: path to the file or folder refered by the link

c     I/O
      character*(Nchar_mx) folder_in,line
      integer islink,file,folder
      character*(Nchar_mx) link,ref
c     temp
      character*1 sstring
      logical ex
      integer ios,exist
      integer n,i,af,position,Nquotes
      integer positions(1:Nquotes_mx)
      character*(Nchar_mx) path
      character*2 arrow
c     label
      character*(Nchar_mx) label
      label='subroutine is_link'

      sstring='/'
c     looking for "->" 
      arrow='->'
      n=len_trim(line)
      af=0
      position=0
      do i=1,n-1
         if (line(i:i+1).eq.arrow(1:2)) then
            af=1
            position=i
            goto 123
         endif
      enddo ! i
 123  continue
      if (af.eq.1) then
         islink=1
      else
         islink=0
      endif

      if (islink.eq.1) then
         call find_quotes(line,Nquotes,positions)
         if (Nquotes.ne.4) then
            call error(label)
            write(*,*) 'line=',trim(line)
            write(*,*) 'Number of quotes=',Nquotes
            write(*,*) 'symbolic link was detected'
            stop
         endif
         path=trim(folder_in)
     &        //line(positions(1)+1:positions(2)-1)
         ref=line(positions(3)+1:positions(4)-1)
c     analyse of "ref"
         if (ref(1:1).ne.trim(sstring)) then
            ref=trim(folder_in)//trim(ref)
         endif
         
         open(13,file=trim(path),status='old',iostat=ios)
         if (ios.ne.0) then     ! file not found
            file=0
            folder=1
         else
            file=1
            folder=0
         endif
         close(13)

         if ((folder.eq.1).and.(file.eq.0)) then ! if folder detected
            call inquire_folder(ref,exist)
            if (exist.eq.0) then
               call error(label)
               write(*,*) 'ref=',trim(ref)
               write(*,*) 'was identified as a folder'
               write(*,*) 'but access could not be granted'
               stop
            else
               link=trim(path)
               goto 666
            endif
         endif
         if ((folder.eq.0).and.(file.eq.1)) then ! if file detected
            inquire(file=trim(ref),exist=ex)
c     Debug
c            write(*,*) 'ex=',ex
c     Debug
            if (.not.ex) then
               call error(label)
               write(*,*) 'ref=',trim(ref)
               write(*,*) 'was identified as a file'
               write(*,*) 'but inquire failed on it'
               stop
            else
               link=trim(path)
               goto 666
            endif
         endif
      endif
c
 666  continue
      return
      end
      


      subroutine inquire_folder(folder,exist)
      implicit none
      include 'max.inc'
c
c     Purpose: to detect whether the input folder exists or not
c
c     Inputs:
c       + folder: character string that contains the folder path to test
c
c     Outputs:
c       + exist: 1 if the folder exists, 0 otherwise
c

c     I/O
      character*(Nchar_mx) folder
      integer exist
c     temp
      character*(Nchar_mx) command
      character*(Nchar_mx) dt_file
      character*(Nchar_mx) local_dir
      integer ios,lc_status
c     label
      character*(Nchar_mx) label
      label='subroutine inquire_folder'

      dt_file='./local'
      command='pwd > '
     &     //trim(dt_file)
      call exec(command)
      open(14,file=trim(dt_file)
     &     ,status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'file not found:'
         write(*,*) trim(dt_file)
         stop
      else
         read(14,'(a)') local_dir
         call add_slash_if_absent(local_dir)
      endif
      close(14)
      dt_file='./status'
      command='cd '
     &     //trim(folder)
     &     //'; echo $? > '
     &     //trim(local_dir)
     &     //trim(dt_file)
      call exec(command)
      open(15,file=trim(dt_file)
     &     ,status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'file not found:'
         write(*,*) trim(dt_file)
         stop
      else
         read(15,*) lc_status
      endif
      close(15)

      if (lc_status.eq.0) then ! directory exists
         exist=1
      else
         exist=0
      endif

      return
      end



      subroutine detect_slash_at_end(line,islash)
      implicit none
      include 'max.inc'
c
c     Purpose: to detect a slash character '/' at the end of
c     a given character string
c
c     Inputs:
c       + line: character string to analyse
c
c     Outputs:
c       + islash: 1 if the last character of "line" is a slash; 0 otherwise
c

c     I/O
      character*(Nchar_mx) line
      integer islash
c     temp
      integer n
      character*1 sstring
c     label
      character*(Nchar_mx) label
      label='subroutine detect_slash_at_end'

      sstring='/'
      n=len_trim(line)
      if (line(n:n).eq.trim(sstring)) then
         islash=1
      else
         islash=0
      endif

      return
      end


      subroutine find_quotes(line,Nquotes,positions)
      implicit none
      include 'max.inc'
c
c     Purpose: to find quote characters '"' within a character string
c
c     Inputs:
c       + line: character string to analyse
c
c     Outputs:
c       + Nquotes: number of quote characters found in the "line" string
c       + positions: positions of the quote characters
c

c     I/O
      character*(Nchar_mx) line
      integer Nquotes
      integer positions(1:Nquotes_mx)
c     temp
      integer i,n
      character*1 qstring
c     label
      character*(Nchar_mx) label
      label='subroutine find_quotes'

      qstring='"'
      Nquotes=0
      n=len_trim(line)
      do i=1,n
         if (line(i:i).eq.trim(qstring)) then
            Nquotes=Nquotes+1
            positions(Nquotes)=i
         endif
      enddo ! i

      return
      end
