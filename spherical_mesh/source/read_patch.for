c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
	subroutine read_patch(datafile,dim,
     &     type,planet_radius,crater_radius,crater_height,
     &     theta_min,theta_max,phi_min,phi_max,
     &     Ntheta,Nphi,Nalpha,Nr,center,
     &     patch_material,crater_material,rim_material)
	implicit none
	include 'max.inc'
	include 'formats.inc'
	include 'param.inc'
c
c       Purpose: to read a patch input data file
c	
c       Inputut:
c         + datafile: name of the data file
c         + dim: dimension of space
c
c	Output:
c	  + type: character string; are curently supported: sphere_portion, impact_crater
c	  + planet_radius: sphere radius [m]
c	  + crater_radius: crater radius [m]
c	  + crater_height: crater height [m]
c         + theta_min: minimum latitude [degrees] (-90,90)
c	  + theta_max: maaximumlatitude [degrees] (-90,90)
c	  + phi_min: minimum longitude [degrees] (0,360)
c	  + phi_max: maximum longitude [degrees] (0,360)
c	  + Ntheta: number of latitude intervals for the sphere portion
c	  + Nphi: number of longitude intervals for the sphere portion
c         + Nalpha:
c	      - 0 if type=sphere_portion
c	      - number of angular sectors if type=impact_crater
c         + Nr:
c	      - 0 if type=sphere_portion
c	      - number of radius sectors if type=impact_crater
c	  + center: position of the center [m,m,m]
c         + patch_material: material for the patch
c         + crater_material: material for the crater
c         + rim_material: material for the rim
c
c	I/O
	character*(Nchar_mx) datafile
	integer dim
	character*(Nchar_mx) type
	double precision planet_radius
	double precision crater_radius
        double precision crater_height
	double precision theta_min
	double precision theta_max
	double precision phi_min
	double precision phi_max
	integer Ntheta
	integer Nphi
        integer Nalpha
	integer Nr
	double precision center(1:Ndim_mx)
	character*(Nchar_mx) patch_material
	character*(Nchar_mx) crater_material
	character*(Nchar_mx) rim_material
c       temp
	integer ios,i
c	label
	character*(Nchar_mx) label
	label='subroutine read_patch'

	open(11,file=trim(datafile),iostat=ios)
	if (ios.ne.0) then
	   call error(label)
	   write(*,*) 'File not found:'
	   write(*,*) trim(datafile)
	   stop
	endif
	read(11,*)
	read(11,10) type
	read(11,*)
	read(11,*) planet_radius
	read(11,*)
        if (trim(type).eq.'impact_crater') then
           read(11,*) crater_radius
           read(11,*)
           read(11,*) crater_height
           read(11,*)
        endif
	read(11,*) theta_min
	read(11,*)
	read(11,*) theta_max
	read(11,*)
	read(11,*) phi_min
	read(11,*)
	read(11,*) phi_max
	read(11,*)
	read(11,*) Ntheta
	read(11,*)
        read(11,*) Nphi
	read(11,*)
	if (trim(type).eq.'impact_crater') then
           read(11,*) Nalpha
           read(11,*)
	   read(11,*) Nr
           read(11,*)
	endif
	read(11,*) (center(i),i=1,dim)
	read(11,*)
	if (trim(type).eq.'sphere_portion') then
	   read(11,*) patch_material
	else if (trim(type).eq.'impact_crater') then
	   read(11,*) crater_material
	   read(11,*)
	   read(11,*) rim_material
	else
	   call error(label)
	   write(*,*) 'type=',trim(type)
	   write(*,*) 'is not supported'
	   stop
	endif
	close(11)

	if (planet_radius.le.0) then
	   call error(label)
	   write(*,*) 'planet_radius=',planet_radius
	   write(*,*) 'should be positive'
	   stop
	endif
        if (trim(type).eq.'impact_crater') then
           if (crater_radius.le.0) then
              call error(label)
              write(*,*) 'crater_radius=',crater_radius
              write(*,*) 'should be positive'
              stop
           endif
           if (crater_height.le.0) then
              call error(label)
              write(*,*) 'crater_height=',crater_height
              write(*,*) 'should be positive'
              stop
           endif
        else
           crater_radius=0.0D+0
           crater_height=0.0D+0
        endif
	if ((theta_min.lt.-90.0D+0).or.(theta_min.gt.90.0D+0)) then
	   call error(label)
	   write(*,*) 'theta_min=',theta_min
	   write(*,*) 'should be in the [-90,90] range'
	   stop
	endif
	if ((theta_max.lt.-90.0D+0).or.(theta_max.gt.90.0D+0)) then
	   call error(label)
	   write(*,*) 'theta_max=',theta_max
	   write(*,*) 'sould be in the [-90,90] range'
	endif
	if (theta_max.le.theta_min) then
	   call error(label)
	   write(*,*) 'theta_max=',theta_max
	   write(*,*) 'should be > theta_min=',theta_min
	   stop
	endif
	do while (phi_max.lt.phi_min)
	   phi_max=phi_max+360.0D+0
	enddo
	if (Ntheta.le.0) then
	   call error(label)
	   write(*,*) 'Ntheta=',Ntheta
	   write(*,*) 'should be positive'
	   stop
	endif
        if (Nphi.le.0) then
           call error(label)
           write(*,*) 'Nphi=',Nphi
           write(*,*) 'should be positive'
           stop
        endif
        if (trim(type).eq.'sphere_portion') then
           Nalpha=0
           Nr=0
           crater_material=''
           rim_material=''
        else if (trim(type).eq.'impact_crater') then
           patch_material=''
        endif

c       conversions
	theta_min=theta_min*pi/180.0D+0
	theta_max=theta_max*pi/180.0D+0
	phi_min=phi_min*pi/180.0D+0
	phi_max=phi_max*pi/180.0D+0
	
	return
	end
