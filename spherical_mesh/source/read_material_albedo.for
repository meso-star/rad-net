      subroutine read_material_albedo(file_in,Nlambda,lambda,albedo)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read a material albedo file
c     
c     Input:
c       + file_in: file to read
c     
c     Output:
c       + Nlambda: number of wavelength values
c       + lambda: values of the wavelenth [nm]
c       + albedo: values of the albedo
c     
c     I/O
      character*(Nchar_mx) file_in
      integer Nlambda
      double precision lambda(1:Nlambda_mat_mx)
      double precision albedo(1:Nlambda_mat_mx)
c     temp
      integer ios,ilambda
      character*11 str1
      character*10 str2
      character*(Nchar_mx) str,str3
      logical str2_found,keep_looking
      integer istr2,i
      integer errcode
      double precision tmp1,tmp2
c     label
      character*(Nchar_mx) label
      label='subroutine read_material_albedo'

 41   format(a11,i10)
      
      open(11,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(file_in)
         stop
      endif
      read(11,41) str1,Nlambda
c     Debug
c      write(*,*) 'Nlambda=',Nlambda
c     Debug
      if (Nlambda.lt.1) then
         call error(label)
         write(*,*) 'Nlambda=',Nlambda
         write(*,*) 'should be positive'
         stop
      endif
      if (Nlambda.gt.Nlambda_mat_mx) then
         call error(label)
         write(*,*) 'Nlambda=',Nlambda
         write(*,*) 'should be <= Nlambda_mat_mx=',Nlambda_mat_mx
         stop
      endif
      str2='lambertian'
      do ilambda=1,Nlambda
         read(11,10) str
         str2_found=.false.
         do i=1,len_trim(str)-len_trim(str2)
            if (str(i:i+len_trim(str2)).eq.trim(str2)) then
               str2_found=.true.
               istr2=i
               goto 111
            endif
         enddo                  ! i
 111     continue
         if (.not.str2_found) then
            call error(label)
            write(*,*) 'Could not identify character string: "',trim(str2),'"'
            write(*,*) 'in the following line:'
            write(*,*) trim(str)
            stop
         endif
         str3=str(1:istr2-1)
         keep_looking=.true.
         do while (keep_looking)
            if (str3(1:1).eq.' ') then
               str3=str3(2:len_trim(str3))
            else
               keep_looking=.false.
            endif
         enddo
         call str2dble(str3,tmp1,errcode)
         if (errcode.ne.0) then
            call error(label)
            write(*,*) 'while converting to double:'
            write(*,*) 'str3="',trim(str3),'"'
            stop
         endif
         if (tmp1.le.0.0D+0) then
            call error(label)
            write(*,*) 'tmp1=',tmp1
            write(*,*) 'should be positive'
            stop
         endif
         lambda(ilambda)=tmp1
         str3=str(istr2+len_trim(str2)+1:len_trim(str))
         keep_looking=.true.
         do while (keep_looking)
            if (str3(1:1).eq.' ') then
               str3=str3(2:len_trim(str3))
            else
               keep_looking=.false.
            endif
         enddo
         call str2dble(str3,tmp2,errcode)
         if (errcode.ne.0) then
            call error(label)
            write(*,*) 'while converting to double:'
            write(*,*) 'str3="',trim(str3),'"'
            stop
         endif
         if (tmp2.lt.0.0D+0) then
            call error(label)
            write(*,*) 'tmp2=',tmp2
            write(*,*) 'should be positive'
            stop
         endif
         if (tmp2.gt.1.0D+0) then
            call error(label)
            write(*,*) 'tmp2=',tmp2
            write(*,*) 'should be <= 1'
            stop
         endif
         albedo(ilambda)=tmp2
c     Debug
c         write(*,*) ilambda,lambda(ilambda),albedo(ilambda)
c     Debug
      enddo                     ! ilambda
      close(11)

      do ilambda=2,Nlambda
         if (lambda(ilambda).le.lambda(ilambda-1)) then
            call error(label)
            write(*,*) 'lambda(',ilambda,')=',lambda(ilambda)
            write(*,*) 'should be > lambda(',ilambda-1,')=',lambda(ilambda-1)
            stop
         endif
      enddo                     ! ilambda

      return
      end
