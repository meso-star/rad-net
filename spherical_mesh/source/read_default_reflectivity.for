      subroutine read_default_reflectivity(input_file,Nlambda,lambda,reflectivity)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the default reflectivity file
c     
c     Input:
c       + input_file: file to read
c     
c     Output:
c       + Nlambda: number of wavelength values needed to define the default reflectivity signal
c       + lambda: values of the wavelength [nm]
c       + reflectivity: default reflectivity signal
c     
c     I/O
      character*(Nchar_mx) input_file
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
c     temp
      integer file_ios,line_ios,ilambda
      double precision tmp1,tmp2
      logical keep_reading
c     label
      character*(Nchar_mx) label
      label='subroutine read_default_reflectivity'

      open(15,file=trim(input_file),iostat=file_ios)
      if (file_ios.eq.0) then
         write(*,*) 'Reading file: ',trim(input_file)
      else
         call error(label)
         write(*,*) 'File not found: ',trim(input_file)
         stop
      endif
      read(15,*)                ! first line is a comment
      Nlambda=0
      keep_reading=.true.
      do while (keep_reading)
         read(15,*,iostat=line_ios) tmp1,tmp2 ! tmp1: µm
         if (line_ios.eq.0) then
            Nlambda=Nlambda+1
            if (Nlambda.gt.Nlambda_mx) then
               call error(label)
               write(*,*) 'Nlambda_mx was reached'
               stop
            endif
            lambda(Nlambda)=tmp1*1.0D+3
            reflectivity(Nlambda)=tmp2
         else
            keep_reading=.false.
         endif
      enddo                     ! while (keep_reading)
      close(15)
c     check
      do ilambda=1,Nlambda
         if (lambda(ilambda).le.0.0D+0) then
            call error(label)
            write(*,*) 'Default reflectivity index:',ilambda
            write(*,*) 'lambda=',lambda(ilambda)
            write(*,*) 'should have a positive value'
            stop
         endif
         if ((reflectivity(ilambda).lt.0.0D+0).or.(reflectivity(ilambda).gt.1.0D+0)) then
            call error(label)
            write(*,*) 'Default reflectivity index:',ilambda
            write(*,*) 'reflectivity=',reflectivity(ilambda)
            write(*,*) 'should be in the [0, 1] range'
            stop
         endif
      enddo                     ! ilambda
      do ilambda=2,Nlambda
         if (lambda(ilambda).le.lambda(ilambda-1)) then
            call error(label)
            write(*,*) 'Default reflectivity index:',ilambda
            write(*,*) 'lambda(',ilambda,')=',lambda(ilambda)
            write(*,*) 'should be > lambda(',ilambda-1,')=',lambda(ilambda-1)
            stop
         endif
      enddo                     ! ilambda

      return
      end
