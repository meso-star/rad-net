      subroutine read_zones(file_in,Nzone,zone_theta,zone_phi,zone_file,
     &     default_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read a zone definition file
c     
c     Input:
c       + file_in: input zone definition file
c     
c     Output:
c       + Nzone: number of zones
c       + zone_theta: latitude limits for each zone [deg]
c       + zone_phi: longitude limits for each zone [deg]
c       + zone_file: kext file attached to each zone
c       + default_file: kext file by default
c     
c     I/O
      character*(Nchar_mx) file_in
      integer Nzone
      double precision zone_theta(1:Nzone_mx,1:2)
      double precision zone_phi(1:Nzone_mx,1:2)
      character*(Nchar_mx) zone_file(1:Nzone_mx)
      character*(Nchar_mx) default_file
c     temp
      integer i,ios
      logical keep_reading,file_exists
      double precision theta_min,theta_max,phi_min,phi_max
      character*(Nchar_mx) str,tstr,file_str
      integer Nspc,spc_idx(0:Nspc_mx)
      integer errcode,i0,i1
      logical default_found
c     label
      character*(Nchar_mx) label
      label='subroutine read_zone'

      open(11,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(file_in)
         stop
      else
         read(11,*)             ! comment line
         Nzone=0
         default_found=.false.
         keep_reading=.true.
         do while (keep_reading)
            read(11,10,iostat=ios) str
c     Debug
c            write(*,*) 'str="',trim(str),'"'
c            write(*,*) 'ios=',ios
c     Debug
            if (ios.ne.0) then
               keep_reading=.false.
               goto 123
            endif
c     Identify spaces from start of string
            Nspc=0
            spc_idx(0)=0
            do i=1,len_trim(str)-1
               if (str(i:i).eq.' ') then
                  Nspc=Nspc+1
                  if (Nspc.gt.Nspc_mx) then
                     call error(label)
                     write(*,*) 'Nspc_mx=',Nspc_mx
                     write(*,*) 'was reached'
                     stop
                  endif
                  spc_idx(Nspc)=i
               endif
            enddo               ! i
c     Debug
c            write(*,*) 'Nspc=',Nspc
c            do i=0,Nspc
c               write(*,*) 'spc_idx(',i,')=',spc_idx(i)
c            enddo               ! i
c     Debug
c     Identify type of line: definition of a zone, or default
            if (Nspc.eq.0) then
               if (default_found) then
                  call error(label)
                  write(*,*) 'More than one default line found'
                  write(*,*) 'in file: ',trim(file_in)
                  stop
               else
                  default_found=.true.
                  inquire(file=trim(str),exist=file_exists)
                  if (.not.file_exists) then
                     call error(label)
                     write(*,*) 'File does not exist:'
                     write(*,*) trim(str)
                     stop
                  endif
                  default_file=trim(str)
               endif            ! default_found
            else if (Nspc.ge.4) then
               i0=0
               call next_spc(Nspc,spc_idx,i0,i1)
               tstr=str(spc_idx(i1)+1:spc_idx(i1+1)-1)
               call str2dble(tstr,theta_min,errcode)
               if (errcode.ne.0) then
                  call error(label)
                  write(*,*) 'Could not convert to double:'
                  write(*,*) 'tstr="',trim(tstr),'"'
                  stop
               endif
c     Debug
c               write(*,*) 'theta_min=',theta_min
c     Debug
               i0=i1+1
               call next_spc(Nspc,spc_idx,i0,i1)
               tstr=str(spc_idx(i1)+1:spc_idx(i1+1)-1)
               call str2dble(tstr,theta_max,errcode)
               if (errcode.ne.0) then
                  call error(label)
                  write(*,*) 'Could not convert to double:'
                  write(*,*) 'tstr="',trim(tstr),'"'
                  stop
               endif
c     Debug
c               write(*,*) 'theta_max=',theta_max
c     Debug
               i0=i1+1
               call next_spc(Nspc,spc_idx,i0,i1)
               tstr=str(spc_idx(i1)+1:spc_idx(i1+1)-1)
               call str2dble(tstr,phi_min,errcode)
               if (errcode.ne.0) then
                  call error(label)
                  write(*,*) 'Could not convert to double:'
                  write(*,*) 'tstr="',trim(tstr),'"'
                  stop
               endif
c     Debug
c               write(*,*) 'phi_min=',phi_min
c     Debug
               i0=i1+1
               call next_spc(Nspc,spc_idx,i0,i1)
               tstr=str(spc_idx(i1)+1:spc_idx(i1+1)-1)
               call str2dble(tstr,phi_max,errcode)
               if (errcode.ne.0) then
                  call error(label)
                  write(*,*) 'Could not convert to double:'
                  write(*,*) 'tstr="',trim(tstr),'"'
                  stop
               endif
c     Debug
c               write(*,*) 'phi_max=',phi_max
c     Debug
               i0=i1+1
               call next_spc(Nspc,spc_idx,i0,i1)
               file_str=str(spc_idx(i1)+1:len_trim(str))
               Nzone=Nzone+1
               if (Nzone.gt.Nzone_mx) then
                  call error(label)
                  write(*,*) 'Nzone_mx=',Nzone_mx
                  write(*,*) 'has been reached'
                  stop
               endif
               if ((theta_min.lt.-90.0D+0).or.
     &              (theta_min.gt.90.0D+0)) then
                  call error(label)
                  write(*,*) 'theta_min=',theta_min
                  write(*,*) 'should be in the [-90,90] range'
                  stop
               endif
               if ((theta_max.lt.-90.0D+0).or.
     &              (theta_max.gt.90.0D+0)) then
                  call error(label)
                  write(*,*) 'theta_max=',theta_max
                  write(*,*) 'sould be in the [-90,90] range'
               endif
               if (theta_max.le.theta_min) then
                  call error(label)
                  write(*,*) 'theta_max=',theta_max
                  write(*,*) 'should be > theta_min=',theta_min
                  stop
               endif
               do while (phi_max.lt.phi_min)
                  phi_max=phi_max+360.0D+0
               enddo
               inquire(file=trim(file_str),exist=file_exists)
               if (.not.file_exists) then
                  call error(label)
                  write(*,*) 'File does not exist:'
                  write(*,*) trim(file_str)
                  stop
               endif
               zone_theta(Nzone,1)=theta_min
               zone_theta(Nzone,2)=theta_max
               zone_phi(Nzone,1)=phi_min
               zone_phi(Nzone,2)=phi_max
               zone_file(Nzone)=trim(file_str)
            else
               call error(label)
               write(*,*) 'str="',trim(str),'"'
               write(*,*) 'should contain either 0 or at least 4 spaces'
               stop
            endif
c            
 123        continue
         enddo                  ! while (keep_reading)
      endif
      close(11)

      if (Nzone.le.0) then
         call error(label)
         write(*,*) 'No zones defined in file: ',trim(file_in)
         stop
      endif
      if (.not.default_found) then
         call error(label)
         write(*,*) 'No default kext file found in file: ',
     &        trim(file_in)
         stop
      endif

      return
      end
