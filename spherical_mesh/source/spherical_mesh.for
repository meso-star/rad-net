c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      program spherical_mesh
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to produce the obj of a single sphere
c     
c     Variables
      integer dim
      character*(Nchar_mx) datafile
      logical produce_surface_properties
      logical produce_gas_properties
      logical produce_cloud_properties
      character*(Nchar_mx) data_dir
      double precision planet_radius
      character*(Nchar_mx) mtlidx_file
      character*(Nchar_mx) grid_file_surface
      character*(Nchar_mx) surface_properties_file
      integer Ntheta_gas,Nphi_gas
      integer Ntheta_cloud,Nphi_cloud
      integer Nlayer_gas,Nlayer_cloud
      double precision radius_gas,top_altitude_gas
      double precision radius_cloud,top_altitude_cloud
      integer ilambda
      character*(Nchar_mx) lambda_file
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) command
      double precision grid_theta_gas(0:Nlat_mx)
      double precision grid_phi_gas(0:Nlon_mx)
      double precision grid_alt_gas(0:Nlay_mx)
      double precision grid_theta_cloud(0:Nlat_mx)
      double precision grid_phi_cloud(0:Nlon_mx)
      double precision grid_alt_cloud(0:Nlay_mx)
      integer*8 Nnode,Ncell
      character*(Nchar_mx) gas_grid_file
      character*(Nchar_mx) gas_radiative_properties_file
      character*(Nchar_mx) gas_thermodynamic_properties_file
      character*(Nchar_mx) phase_function_list_file
      character*(Nchar_mx) cloud_grid_file
      character*(Nchar_mx) cloud_radiative_properties_file
      character*(Nchar_mx) phase_function_file
      double precision xnode(1:Nnode_mx,1:Ndim_mx)
      integer i,j
      character*(Nchar_mx) haze_zone_file
      character*(Nchar_mx) haze_ssa_file
      character*(Nchar_mx) haze_phase_file
      character*(Nchar_mx) kext_z_interpolation_mode
      character*(Nchar_mx) kext_z_extrapolation_mode
      character*(Nchar_mx) cT_z_interpolation_mode
      character*(Nchar_mx) cT_z_extrapolation_mode
      character*(Nchar_mx) ssa_z_interpolation_mode
      character*(Nchar_mx) ssa_z_extrapolation_mode
      character*(Nchar_mx) ssa_lambda_interpolation_mode
      character*(Nchar_mx) ssa_lambda_extrapolation_mode
      double precision kext_set_value
      double precision T_set_value
      integer ssa_set_Nlambda
      double precision ssa_set_lambda(1:Nlambda_mx)
      double precision ssa_set_value(1:Nlambda_mx)
      character*(Nchar_mx) gT_z_interpolation_mode
      character*(Nchar_mx) gT_z_extrapolation_mode
      character*(Nchar_mx) k_z_interpolation_mode
      character*(Nchar_mx) k_z_extrapolation_mode
      double precision k_set_value
      integer Nzone
      double precision zone_limit(1:Nzone_mx,1:2)
      integer zone_profile(1:Nzone_mx)
      integer Nband
      integer Nq
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision w(1:Nq_mx)
      double precision g(1:Nq_mx)
      character*(Nchar_mx) CH4_ck_file,CO_ck_file
      character*(Nchar_mx) CH4_abundance_file,CO_abundance_file
      logical reference
      logical data_found(1:Nlambda_mx)
      integer lambda_idx(1:Nlambda_mx)
      integer Nlev,Nprofile
      integer Npix_sc(1:2)
      double precision corner_theta_sc(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi_sc(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z_sc(1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf_sc(1:Npix_mx,1:Npix_mx)
      integer Nlambda_sc
      double precision lambda_sc(1:Nlambda_mx)
      double precision reflectivity_sc(1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
      integer Npix(1:Nfig_mx,1:2)
      double precision corner_theta(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf(1:Nfig_mx,1:Npix_mx,1:Npix_mx)
      integer Nlambda(1:Nfig_mx)
      double precision lambda(1:Nfig_mx,1:Nlambda_mx)
      integer Nlambda_gas
      double precision lambda_gas(1:Nlambda_mx)
      double precision reflectivity(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
      double precision theta_limit(1:2)
      double precision phi_limit(1:2)
      integer Nlambda_default
      double precision lambda_default(1:Nlambda_mx)
      double precision reflectivity_default(1:Nlambda_mx)
      double precision brdf_default
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nmat
      integer materials(1:Nf_mx)
      double precision group_theta_limit(1:Ngroup_mx,1:2)
      double precision group_phi_limit(1:Ngroup_mx,1:2)
      logical keep_looking,err_code
      character*(Nchar_mx) cube_file,default_reflectivity_file
      integer Ngroup
      integer Ncube_in_group(1:Ngroup_mx)
      character*(Nchar_mx) group_file(1:Ngroup_mx,1:Nfig_mx,1:2)
      integer cube_index(1:Ngroup_mx,1:Nfig_mx)
      integer igroup,icube,ipix,jpix,icorner
      character*(Nchar_mx) definition_file
      integer Ncube_def
      integer definition(1:Ncube_mx,1:3)
      integer cube_def(1:3)
      logical attributed_mtl(0:Ncube_mx,0:Npix_mx,0:Npix_mx)
      integer attributed_mtl_idx(0:Ncube_mx,0:Npix_mx,0:Npix_mx)
      integer mtl_idx(1:Nf_mx)
      character*(Nchar_mx) prefix
c     label
      character*(Nchar_mx) label
      label='program spherical_mesh'

      dim=3                     ! Dimension of physical space

      datafile='./data.in'
      call read_data(datafile,data_dir,
     &     produce_surface_properties,planet_radius,mtlidx_file,grid_file_surface,surface_properties_file,
     &     produce_gas_properties,radius_gas,top_altitude_gas,Ntheta_gas,Nphi_gas,Nlayer_gas,gT_z_interpolation_mode,gT_z_extrapolation_mode,k_z_interpolation_mode,k_z_extrapolation_mode,gas_grid_file,gas_radiative_properties_file,gas_thermodynamic_properties_file,
     &     produce_cloud_properties,radius_cloud,top_altitude_cloud,Ntheta_cloud,Nphi_cloud,Nlayer_cloud,
     &     kext_z_interpolation_mode,kext_z_extrapolation_mode,cT_z_interpolation_mode,cT_z_extrapolation_mode,
     &     ssa_z_interpolation_mode,ssa_z_extrapolation_mode,ssa_lambda_interpolation_mode,ssa_lambda_extrapolation_mode,
     &     phase_function_list_file,cloud_grid_file,cloud_radiative_properties_file,phase_function_file)

c     =======================================================
c     Production of surface properties
c     =======================================================
      if (produce_surface_properties) then
         write(*,*) '+--------------------------------------------------+'
         write(*,*) '|                     SURFACE                      |'
         write(*,*) '+--------------------------------------------------+'
         command='rm -f '//trim(grid_file_surface)
         call exec(command)
c     -------------------------------------------------------
c     Initialization
c     -------------------------------------------------------
         Nv=0
         Nf=0
         Nmat=0
         write(*,*) 'Generating geometry...'
         obj_file='./results/sphere.obj' ! OBJ file for recording the scene
         mtllib_file='materials.mtl' ! materials library file
c
         call init(obj_file,mtllib_file)
         do icube=0,Ncube_mx
            do ipix=0,Npix_mx
               do jpix=0,Npix_mx
                  attributed_mtl(icube,ipix,jpix)=.false.
                  attributed_mtl_idx(icube,ipix,jpix)=0
               enddo            ! jpix
            enddo               ! ipix
         enddo                  ! icbue
         definition_file=trim(data_dir)//'VIMS_cubes_definition.dat'
         call read_cube_definition(definition_file,Ncube_def,definition)
c     
         call cube_fusion(dim,data_dir,
     &        Ncube_def,definition,
     &        Ngroup,Ncube_in_group,group_file,cube_index)
         
         do igroup=1,Ngroup
            do icube=1,Ncube_in_group(igroup)
               cube_file=group_file(igroup,icube,1)
               lambda_file=group_file(igroup,icube,2)
               if (cube_index(igroup,icube).gt.Ncube_def) then
                  call error(label)
                  write(*,*) 'cube_index(',igroup,',',icube,')=',cube_index(igroup,icube)
                  write(*,*) '> number of cubes found in definition file:',Ncube_def
                  stop
               endif
               do j=1,3
                  cube_def(j)=definition(cube_index(igroup,icube),j)
               enddo            ! j
               call read_cube_file(dim,
     &              cube_file,lambda_file,cube_def,
     &              Npix_sc,corner_theta_sc,
     &              corner_phi_sc,corner_z_sc,brdf_sc,
     &              Nlambda_sc,lambda_sc,reflectivity_sc)
               do j=1,2
                  Npix(icube,j)=Npix_sc(j)
               enddo            ! j
               do ipix=1,Npix_sc(1)
                  do jpix=1,Npix_sc(2)
                     do icorner=1,4
                        corner_theta(icube,ipix,jpix,icorner)=corner_theta_sc(ipix,jpix,icorner)
                        corner_phi(icube,ipix,jpix,icorner)=corner_phi_sc(ipix,jpix,icorner)
                        corner_z(icube,ipix,jpix,icorner)=corner_z_sc(ipix,jpix,icorner)
                     enddo      ! icorner
                     brdf(icube,ipix,jpix)=brdf_sc(ipix,jpix)
                     do ilambda=1,Nlambda_sc
                        reflectivity(icube,ipix,jpix,ilambda)=reflectivity_sc(ipix,jpix,ilambda)
                     enddo      ! ilambda
                  enddo         ! jpix
               enddo            ! ipix
               Nlambda(icube)=Nlambda_sc
               do ilambda=1,Nlambda_sc
                  lambda(icube,ilambda)=lambda_sc(ilambda)
               enddo            ! ilambda
c
c     =================================================================================
c     Read default material spectral properties
               if ((igroup.eq.1).and.(icube.eq.1)) then
                  default_reflectivity_file=trim(data_dir)//'default_reflectivity.dat'
                  call read_default_reflectivity(default_reflectivity_file,Nlambda_default,lambda_default,reflectivity_default)
                  brdf_default=0.0D+0 ! diffuse reflection
               endif            ! first group / cube ever found
            enddo               ! icube
c     =================================================================================
c     
            call generate_mtl_for_group(dim,igroup,Ncube_in_group,cube_index,
     &           Npix,brdf,Nlambda,lambda,reflectivity,
     &           Nlambda_default,lambda_default,reflectivity_default,brdf_default,mtllib_file)
c     -------------------------------------------------------
c     Generate and record objects
c     -------------------------------------------------------
            call group_mesh(dim,
     &           igroup,Ncube_in_group,cube_index,planet_radius,
     &           Npix,corner_theta,corner_phi,corner_z,
     &           brdf,Nlambda,lambda,reflectivity,
     &           Nv,Nf,vertices,faces,Nmat,materials,
     &           attributed_mtl,attributed_mtl_idx,mtl_idx,
     &           theta_limit,phi_limit)
            do j=1,2
               group_phi_limit(igroup,j)=phi_limit(j)
               group_theta_limit(igroup,j)=theta_limit(j)
            enddo               ! j
         enddo                  ! igroup
c
         call patchification(dim,
     &        Ngroup,group_phi_limit,group_theta_limit,
     &        planet_radius,
     &        Nv,Nf,vertices,faces,Nmat,materials,
     &        attributed_mtl,attributed_mtl_idx,mtl_idx,
     &        Nlambda_default,lambda_default,reflectivity_default,brdf_default)
         call record_surface_data(dim,data_dir,mtlidx_file,grid_file_surface,obj_file,mtllib_file,surface_properties_file,
     &        Nv,Nf,vertices,faces,Nmat,materials,mtl_idx)
c     Debug
         write(*,*) 'Number of VIMS data cube groups:',Ngroup
         write(*,*) 'Number of vertices:',Nv
         write(*,*) 'Number of faces:',Nf
c     Debug
      endif                     ! produce_surface_properties
      
      
c     =======================================================
c     Production of gas mixture properties
c     =======================================================
      if (produce_gas_properties) then
         write(*,*) '+--------------------------------------------------+'
         write(*,*) '|                   GAS MIXTURE                    |'
         write(*,*) '+--------------------------------------------------+'
c     
         command='rm -f '//trim(gas_grid_file)
         call exec(command)
c     Example of high-definition data grid for the clouds:
c     homogenous in latitude, longitude and altitude
         call generate_homogeneous_latitude_grid(Ntheta_gas,
     &        grid_theta_gas)
         call generate_homogeneous_longitude_grid(Nphi_gas,
     &        grid_phi_gas)
         call generate_homogeneous_cartesian_grid(radius_gas,
     &        radius_gas+top_altitude_gas,Nlayer_gas,
     &        grid_alt_gas)
c     Produce node and tetrahedric cells
         call volumic_grid(dim,Ntheta_gas,Nphi_gas,Nlayer_gas,
     &        grid_theta_gas,grid_phi_gas,grid_alt_gas,
     &        gas_grid_file,Nnode,Ncell,xnode)
c     generate output files
         haze_zone_file=trim(data_dir)//'haze_zones.in'
         CH4_ck_file=trim(data_dir)//'CH4_2000-12000_TheoReTS_g16.DAT'
         CO_ck_file=trim(data_dir)//'CO_2000-8000_HITEMP_g16.DAT'
         CH4_abundance_file=trim(data_dir)//'profil_abondance_CH4.txt'
         CO_abundance_file=trim(data_dir)//'profil_abondance_CO.txt'
c     
         call fill_gas_grid(dim,
     &        haze_zone_file,
     &        CH4_ck_file,CO_ck_file,CH4_abundance_file,CO_abundance_file,
     &        Nlambda_gas,lambda_gas,Nq,g,w, ! those will be read from the "CH4_ck_file" and returned here (output data)
     &        Nnode,xnode,
     &        gT_z_interpolation_mode,gT_z_extrapolation_mode,0.0D+0,
     &        k_z_interpolation_mode,k_z_extrapolation_mode,0.0D+0,
     &        gas_radiative_properties_file,gas_thermodynamic_properties_file)
c     
         write(*,*) 'Volume (gas):'
         write(*,*) 'Number of nodes:',Nnode
         write(*,*) 'Number of cells:',Ncell
      endif                     ! produce_gas_properties
      
c     =======================================================
c     Production of cloud & haze properties
c     =======================================================
      if (produce_cloud_properties) then
         write(*,*) '+--------------------------------------------------+'
         write(*,*) '|                       HAZE                       |'
         write(*,*) '+--------------------------------------------------+'
         command='rm -f '//trim(cloud_grid_file)
         call exec(command)
c     Example of high-definition data grid for the clouds:
c     homogenous in latitude, longitude and altitude
         call generate_homogeneous_latitude_grid(Ntheta_cloud,
     &        grid_theta_cloud)
         call generate_homogeneous_longitude_grid(Nphi_cloud,
     &        grid_phi_cloud)
         call generate_homogeneous_cartesian_grid(radius_cloud,
     &        radius_cloud+top_altitude_cloud,Nlayer_cloud,
     &        grid_alt_cloud)
c     Produce node and tetrahedric cells
         call volumic_grid(dim,Ntheta_cloud,Nphi_cloud,Nlayer_cloud,
     &        grid_theta_cloud,grid_phi_cloud,grid_alt_cloud,
     &        cloud_grid_file,Nnode,Ncell,xnode)
         haze_zone_file=trim(data_dir)//'haze_zones.in'
         haze_ssa_file=trim(data_dir)//'ssaTomasko_Hirtzig.dat'
         haze_phase_file=trim(data_dir)//'fonction_phase_haze_Titan_80km.txt'
c     spectral mesh
         Nband=1
         lambda_min(1)=9.0D+3    ! nm
         lambda_max(1)=1.0D+4    ! nm
c     values to use when *_z_extrapolation_mode='set'
         kext_set_value=0.0D+0
         T_set_value=0.0D+0
         ssa_set_Nlambda=5
         ssa_set_lambda(1)=300.0D+0
         ssa_set_lambda(2)=500.0D+0
         ssa_set_lambda(3)=700.0D+0
         ssa_set_lambda(4)=1000.0D+0
         ssa_set_lambda(5)=5000.0D+0
         ssa_set_value(1)=0.50D+0
         ssa_set_value(2)=0.50D+0
         ssa_set_value(3)=0.50D+0
         ssa_set_value(4)=0.50D+0
         ssa_set_value(5)=0.50D+0
c     generate output files
         prefix='haze'
         call fill_cloud_grid(dim,haze_zone_file,haze_ssa_file,
     &        haze_phase_file,Nnode,xnode,
     &        Nband,lambda_min,lambda_max,
     &        kext_z_interpolation_mode,
     &        kext_z_extrapolation_mode,
     &        kext_set_value,
     &        cT_z_interpolation_mode,
     &        cT_z_extrapolation_mode,
     &        T_set_value,
     &        ssa_z_interpolation_mode,
     &        ssa_z_extrapolation_mode,
     &        ssa_set_Nlambda,ssa_set_lambda,ssa_set_value,
     &        ssa_lambda_interpolation_mode,
     &        ssa_lambda_extrapolation_mode,
     &        prefix,
     &        phase_function_list_file,
     &        cloud_radiative_properties_file,
     &        phase_function_file)
c     Print some statistics
         write(*,*) 'Volume (cloud):'
         write(*,*) 'Number of nodes:',Nnode
         write(*,*) 'Number of cells:',Ncell
      endif                     ! produce_cloud_properties

c     =======================================================
c     Recording planet_EMC input data files when needed
c     =======================================================
      write(*,*)      
c     -------------------------------------------------------
c     proper exit
c     -------------------------------------------------------
      
      command='rm -f list.txt'
      call exec(command)
      command='rm -f local'
      call exec(command)
      command='rm -f nlines_*'
      call exec(command)
      command='rm -f status'
      call exec(command)

      end
