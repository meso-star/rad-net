      subroutine read_ssa_file(file_in,
     &     Nlev,Nlambda,altitude,lambda,ssa)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a single-scattering albedo file for Titan haze
c     
c     Input:
c       + file_in: file to read
c     
c     Output:
c       + Nlev: number of altitude levels
c       + Nlambda: number of wavelength values
c       + altitude: altitude levels [m]
c       + lambda: wavelength values [nm]
c       + ssa: haze single-scattering albedo
c     
c     I/O
      character*(Nchar_mx) file_in
      integer Nlev
      integer Nlambda
      double precision altitude(1:Nlev_mx)
      double precision lambda(1:Nlambda_mx)
      double precision ssa(1:Nlev_mx,1:Nlambda_mx)
c     temp
      integer ios,raf,i
      logical keep_reading
      double precision val1,dt(1:Nlev_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine read_ssa_file'

      open(12,file=trim(file_in),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(file_in)
         stop
      endif
      read(12,*) Nlev
      if (Nlev.lt.1) then
         call error(label)
         write(*,*) 'Nlev=',Nlev
         write(*,*) 'should be positive'
         stop
      endif
      if (Nlev.gt.Nlev_mx) then
         call error(label)
         write(*,*) 'Nlev=',Nlev
         write(*,*) '> Nlev_mx=',Nlev_mx
         stop
      endif
      rewind(12)
      read(12,*) raf,(altitude(i),i=Nlev,1,-1)
      do i=1,Nlev
         altitude(i)=altitude(i)*1.0D+3 ! km -> m
      enddo                     ! i
      do i=1,Nlev
         if (altitude(i).lt.0.0D+0) then
            call error(label)
            write(*,*) 'altitude(',i,')=',altitude(i)
            write(*,*) 'should be positive'
            stop
         endif
      enddo                     ! i
      do i=2,Nlev
         if (altitude(i).le.altitude(i-1)) then
            call error(label)
            write(*,*) 'altitude(',i,')=',altitude(i)
            write(*,*) 'should be > altitude(',i-1,')=',altitude(i-1)
            stop
         endif
      enddo                     ! i
c     Read lambda and the ssa array
      Nlambda=0
      keep_reading=.true.
      do while (keep_reading)
         read(12,*,iostat=ios) val1,(dt(i),i=Nlev,1,-1)
         if (ios.ne.0) then
            keep_reading=.false.
            goto 111
         else
            Nlambda=Nlambda+1
            if (Nlambda.gt.Nlambda_mx) then
               call error(label)
               write(*,*) 'Nlambda_mx has been reached'
               stop
            endif
            lambda(Nlambda)=val1
            do i=1,Nlev
               ssa(i,Nlambda)=dt(i)
            enddo               ! i
         endif                  ! ios.ne.0
 111     continue
      enddo                     ! while (keep_reading)
      close(12)

      return
      end
