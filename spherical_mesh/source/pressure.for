      subroutine p2z(Nlev_ref,pressure_ref,altitude_ref,
     &     temperature_ref,p,z,T)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute a altitude from the reference altitude/pressure profile
c     
c     The pressure is supposed to evolve as a inverse exponential of altitude:
c     between two known pressure / temperature levels (p1,z1) and (p2,z2)
c     with p1 > p2 and therefore z1 < z2; for any value of z such that z1 < z < z2:
c     
c     
c     p2,z2 ----------------------------------+-
c                                            +
c                                           +
c                                          +
c     p(z)=p1*exp(-alpha*(z-z1))         + 
c                                      +  
c                                   +
c                               +
c     p1,z1 --------------+---------------------
c     
c     
c     
c     Therefore: alpha=ln(p1/p2)*1/(z2-z1)
c     This routine computes z for a given value of p:
c     
c     z=z1+ln(p1/p)*1/alpha
c     
c     The values of (p1,z1) and (p2,z2) are identified from the
c     provided reference pressure and altitude profile. A additionnal
c     pressure/altitude level is known for the surface.
c     
c     For any value of p greater than the greatest known pressure level,
c     p1 and p2 are taken as the two known highest values of pressure: the
c     altitude will be extrapolated from the first interval.
      
c     For any value of p lower than the lowest known pressure level,
c     p1 and p2 are taken as the two known lowest values of pressure: the
c     altitude will be extrapolated from the last interval.
c
c     The temperature is supposed to evolve linearly with pressure
c      
c     
c     Input:
c       + Nlev_ref: number of levels in the reference profile
c       + pressure_ref: reference pressure profile [Pa]
c       + altitude_ref: reference altitude profile [m]
c       + temperature_ref: reference temperature profile [K]
c       + p: arbitrary pressure level [Pa]
c     
c     Output:
c       + z: altitude for the input pressure level "p" [m]
c       + T: temperature for the input pressure level "p" [K]
c     
c     I/O
      integer Nlev_ref
      double precision pressure_ref(1:Nlev_mx)
      double precision altitude_ref(1:Nlev_mx)
      double precision temperature_ref(1:Nlev_mx)
      double precision p,z,T
c     temp
      double precision p1,p2
      double precision z1,z2
      double precision T1,T2
      double precision alpha
c     label
      character*(Nchar_mx) label
      label='subroutine p2z'

      call identify_pressure_interval(Nlev_ref,
     &     pressure_ref,altitude_ref,temperature_ref,.true.,p,
     &     p1,z1,T1,p2,z2,T2)

      call get_alpha(p1,z1,p2,z2,alpha)
      z=z1+log(p1/p)/alpha
      T=T1+(T2-T1)*(p1-p)/(p1-p2)

      return
      end



      subroutine z2p(Nlev_ref,pressure_ref,altitude_ref,
     &     temperature_ref,z,p,T)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute a pressure from the reference altitude/pressure profile
c     
c     The pressure is supposed to evolve as a inverse exponential of altitude:
c     between two known pressure / temperature levels (p1,z1) and (p2,z2)
c     with p1 > p2 and therefore z1 < z2; for any value of z such that z1 < z < z2:
c     
c     
c     p2,z2 ----------------------------------+-
c                                            +
c                                           +
c                                          +
c     p(z)=p1*exp(-alpha*(z-z1))         + 
c                                      +  
c                                   +
c                               +
c     p1,z1 --------------+---------------------
c     
c     
c     
c     Therefore: alpha=ln(p1/p2)*1/(z2-z1)
c     This routine computes p for a given value of z
c     
c     The values of (p1,z1) and (p2,z2) are identified from the
c     provided reference pressure and altitude profile. A additionnal
c     pressure/altitude level is known for the surface.
c     
c     For any value of p greater than the greatest known pressure level,
c     p1 and p2 are taken as the two known highest values of pressure: the
c     altitude will be extrapolated from the first interval.
      
c     For any value of p lower than the lowest known pressure level,
c     p1 and p2 are taken as the two known lowest values of pressure: the
c     altitude will be extrapolated from the last interval.
c
c     The temperature is supposed to evolve linearly with pressure
c      
c     
c     Input:
c       + Nlev_ref: number of levels in the reference profile
c       + pressure_ref: reference pressure profile [Pa]
c       + altitude_ref: reference altitude profile [m]
c       + temperature_ref: reference temperature profile [K]
c       + z: altitude for the input pressure level "p" [m]
c     
c     Output:
c       + p: arbitrary pressure level [Pa]
c       + T: temperature for the input pressure level "p" [K]
c     
c     I/O
      integer Nlev_ref
      double precision pressure_ref(1:Nlev_mx)
      double precision altitude_ref(1:Nlev_mx)
      double precision temperature_ref(1:Nlev_mx)
      double precision z,p,T
c     temp
      double precision p1,p2
      double precision z1,z2
      double precision T1,T2
      double precision alpha
c     Debug
c      integer i
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine z2p'
      
      call identify_altitude_interval(Nlev_ref,
     &     pressure_ref,altitude_ref,temperature_ref,z,
     &     p1,z1,T1,p2,z2,T2)

c     Debug
c      write(*,*) 'z1=',z1,' z2=',z2,' z=',z
c      do i=1,Nlev_ref
c         write(*,*) i,pressure_ref(i),altitude_ref(i)
c      enddo                     ! i
c     Debug
      call get_alpha(p1,z1,p2,z2,alpha)
c     Debug
c      write(*,*) 'alpha=',alpha
c     Debug
      p=p1*dexp(-alpha*(z-z1))
      T=T1+(T2-T1)*(p1-p)/(p1-p2)

      return
      end



      subroutine identify_pressure_interval(Nlev_ref,
     &     pressure_ref,altitude_ref,temperature_ref,add_ground_level,p,
     &     p1,z1,T1,p2,z2,T2)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to identify the pressure interval
c     
c     Input:
c       + Nlev_ref: number of levels in the reference profile
c       + pressure_ref: reference pressure profile [Pa]
c       + altitude_ref: reference altitude profile [m]
c       + temperature_ref: reference temperature profile [K]
c       + add_ground_level: true if ground pressure level must be considered
c       + p: arbitrary pressure level [Pa]
c     
c     Output:
c       + p1: pressure for level 1 [Pa]
c       + z1: altitude for level 1 [m]
c       + T1: temperature for level 1 [K]
c       + p2: pressure for level 2 [Pa]
c       + z2: altitude for level 2 [m]
c       + T2: temperature for level 2 [K]
c     
c     I/O
      integer Nlev_ref
      double precision pressure_ref(1:Nlev_mx)
      double precision altitude_ref(1:Nlev_mx)
      double precision temperature_ref(1:Nlev_mx)
      logical add_ground_level
      double precision p
      double precision p1,z1,T1
      double precision p2,z2,T2
c     temp
      logical interval_found
      integer int,i
c     label
      character*(Nchar_mx) label
      label='subroutine identify_pressure_interval'

      if (Nlev_ref.gt.Nlev_mx) then
         call error(label)
         write(*,*) 'Nlev=ref=',Nlev_ref
         write(*,*) '> Nlev_mx=',Nlev_mx
         stop
      endif      
      if (p.gt.pressure_ref(1)) then
         interval_found=.true.
         if (add_ground_level) then
            int=0
         else
            int=1
         endif
         goto 111
      else if (p.lt.pressure_ref(Nlev_ref)) then
         interval_found=.true.
         int=Nlev_ref-1
         goto 111
      else
         interval_found=.false.
         do i=1,Nlev_ref-1
            if ((p.le.pressure_ref(i)).and.
     &           (p.ge.pressure_ref(i+1))) then
               interval_found=.true.
               int=i
               goto 111
            endif
         enddo                  ! i
      endif
 111  continue
      if (.not.interval_found) then
         call error(label)
         write(*,*) 'Pressure interval not found for'
         write(*,*) 'p=',p
         write(*,*) 'P0=',P0_titan,' Pa'
         do i=1,Nlev_ref
            write(*,*) 'pressure_ref(',i,')=',pressure_ref(i),' Pa'
         enddo                  ! i
         stop
      endif

      if (int.eq.0) then
         p1=P0_titan
         z1=0.0D+0
         T1=T0_titan
      else
         p1=pressure_ref(int)
         z1=altitude_ref(int)
         T1=temperature_ref(int)
      endif
      p2=pressure_ref(int+1)
      z2=altitude_ref(int+1)
      T2=temperature_ref(int+1)
      if (z1.ge.z2) then
         call error(label)
         write(*,*) 'z1=',z1
         write(*,*) 'should be < z2=',z2
         stop
      endif
      if (p1.le.p2) then
         call error(label)
         write(*,*) 'p1=',p1
         write(*,*) 'should be > p2=',p2
         stop
      endif

      return
      end


      
      subroutine identify_altitude_interval(Nlev_ref,
     &     pressure_ref,altitude_ref,temperature_ref,z,
     &     p1,z1,T1,p2,z2,T2)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to identify the altitude interval
c     
c     Input:
c       + Nlev_ref: number of levels in the reference profile
c       + pressure_ref: reference pressure profile [Pa]
c       + altitude_ref: reference altitude profile [m]
c       + temperature_ref: reference temperature profile [K]
c       + z: arbitrary altitude level [m]
c     
c     Output:
c       + p1: pressure for level 1 [Pa]
c       + z1: altitude for level 1 [m]
c       + T1: temperature for level 1 [K]
c       + p2: pressure for level 2 [Pa]
c       + z2: altitude for level 2 [m]
c       + T2: temperature for level 2 [K]
c     
c     I/O
      integer Nlev_ref
      double precision pressure_ref(1:Nlev_mx)
      double precision altitude_ref(1:Nlev_mx)
      double precision temperature_ref(1:Nlev_mx)
      double precision z
      double precision p1,z1,T1
      double precision p2,z2,T2
c     temp
      logical interval_found
      integer int,i
c     label
      character*(Nchar_mx) label
      label='subroutine identify_altitude_interval'

      if (Nlev_ref.gt.Nlev_mx) then
         call error(label)
         write(*,*) 'Nlev=ref=',Nlev_ref
         write(*,*) '> Nlev_mx=',Nlev_mx
         stop
      endif
      if (z.lt.altitude_ref(1)) then
         interval_found=.true.
         int=0
         goto 111
      else if (z.gt.altitude_ref(Nlev_ref)) then
         interval_found=.true.
         int=Nlev_ref-1
         goto 111
      else
         interval_found=.false.
         do i=1,Nlev_ref-1
            if ((z.ge.altitude_ref(i)).and.
     &           (z.le.altitude_ref(i+1))) then
               interval_found=.true.
               int=i
               goto 111
            endif
         enddo                  ! i
      endif
 111  continue
      if (.not.interval_found) then
         call error(label)
         write(*,*) 'Altitude interval not found for'
         write(*,*) 'z=',z
         do i=1,Nlev_ref
            write(*,*) 'altitude_ref(',i,')=',altitude_ref(i),' m'
         enddo                  ! i
         stop
      endif
      
      if (int.eq.0) then
         p1=P0_titan
         z1=0.0D+0
         T1=T0_titan
      else
         p1=pressure_ref(int)
         z1=altitude_ref(int)
         T1=temperature_ref(int)
      endif
      p2=pressure_ref(int+1)
      z2=altitude_ref(int+1)
      T2=temperature_ref(int+1)
      if (z1.ge.z2) then
         call error(label)
         write(*,*) 'z1=',z1
         write(*,*) 'should be < z2=',z2
         stop
      endif
      if (p1.le.p2) then
         call error(label)
         write(*,*) 'p1=',p1
         write(*,*) 'should be > p2=',p2
         stop
      endif

      return
      end

      

      subroutine get_alpha(p1,z1,p2,z2,alpha)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the value of alpha in the pressure law:
c     
c     p2,z2 ----------------------------------+-
c                                            +
c                                           +
c                                          +
c     p(z)=p1*exp(-alpha*(z-z1))         + 
c                                      +  
c                                   +
c                               +
c     p1,z1 --------------+---------------------
c     
c     
c     Input:
c       + p1: pressure for level 1 [Pa]
c       + z1: altitude for level 1 [m]
c       + p2: pressure for level 2 [Pa]
c       + z2: altitude for level 2 [m]
c     
c     Output:
c       + alpha: value of alpha
c     
c     I/O
      double precision p1,z1
      double precision p2,z2
      double precision alpha
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine get_alpha'

      if (p1.le.p2) then
         call error(label)
         write(*,*) 'p1=',p1
         write(*,*) 'should be > p2=',p2
         stop
      endif
      if (z1.ge.z2) then
         call error(label)
         write(*,*) 'z1=',z1
         write(*,*) 'should be < z2=',z2
         stop
      endif
c      
      alpha=log(p1/p2)/(z2-z1)
c
      if (alpha.le.0.0D+0) then
         call error(label)
         write(*,*) 'alpha=',alpha
         write(*,*) 'should be positive'
         write(*,*) 'p1=',p1
         write(*,*) 'z1=',z1
         write(*,*) 'p2=',p2
         write(*,*) 'z2=',z2
         stop
      endif
c
      return
      end
