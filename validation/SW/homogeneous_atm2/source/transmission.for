      subroutine transmission_to_source(dim,center,R,Hatm,ka,Ds,Rs,theta_s,phi_s,x0,u0,transmissivity)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the transmission between the current position
c     and the source
c     
c     Input:
c       + dim: dimension of space
c       + center: center of the sphere [m,m,m]
c       + R: radius of the spherical body [m]
c       + Hatm: height of the atmosphere [m]
c       + ka: absorption coefficient [inv. m]
c       + Ds: distance to star [km]
c       + Rs: radius of star [km]
c       + theta_s: latitude of star in local spherical body referential [rad]
c       + phi_s: longitude of star in local spherical body referential [rad]
c       + x0: starting position
c       + u0: propagation direction
c     
c     Output:
c       + transmissivity: transmissivity between x0 and the source, in direction u0
c     
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision R
      double precision Hatm
      double precision ka
      double precision Ds
      double precision Rs
      double precision theta_s
      double precision phi_s
      double precision x0(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision transmissivity
c     temp
      double precision OS(1:Ndim_mx)
      double precision source_center(1:Ndim_mx)
      integer n_int
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision sigma1,sigma2
      logical intersects_source
      logical source_is_visible
c     label
      character*(Nchar_mx) label
      label='subroutine transmission_to_source'

c     OS: vector between the sphere center and the source center
      OS(1)=Ds*dcos(theta_s)*dcos(phi_s)
      OS(2)=Ds*dcos(theta_s)*dsin(phi_s)
      OS(3)=Ds*dsin(theta_s)
      call add_vectors(dim,center,OS,source_center)
c     test intersection between the (x0,u0) LOS and a spherical source
      call line_sphere_forward_intersect(.false.,dim,
     &     x0,u0,source_center,Rs,
     &     n_int,P1,sigma1,P2,sigma2)
      if (n_int.eq.0) then
         intersects_source=.false.
      else
         intersects_source=.true.
      endif
c     test visibility between x0 and the source
      call line_sphere_forward_intersect(.false.,dim,
     &     x0,u0,center,R,
     &     n_int,P1,sigma1,P2,sigma2)
      if (n_int.eq.0) then
         source_is_visible=.true.
      else
         source_is_visible=.false.
      endif
c     compute transmission
      if ((intersects_source).and.(source_is_visible)) then
c     Intersection between the (x0,u0) LOS and sphere of radius R+Hatm
         call line_sphere_forward_intersect(.false.,dim,
     &        x0,u0,center,R+Hatm,
     &        n_int,P1,sigma1,P2,sigma2)
         if (n_int.eq.0) then
            transmissivity=0.0D+0
         else if (n_int.eq.1) then
            transmissivity=dexp(-ka*sigma1)
         else if (n_int.eq.2) then
            transmissivity=dexp(-ka*(sigma2-sigma1))
         endif
      else
         transmissivity=0.0D+0
      endif
      
      return
      end
