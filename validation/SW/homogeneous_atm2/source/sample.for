      subroutine sample_cone_half_angle(theta_max,theta,phi)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to uniformly sample the solid angle corresponding to a cone
c     defined by its half-angle
c     
c     Input:
c       + theta_max: cone half-angle [rad]
c     
c     Output:
c       + theta: angle between the cone axis and the sampled direction
c       + phi: arbitrary sampled over [0-2pi]
c     
c     I/O
      double precision theta_max
      double precision theta
      double precision phi
c     temp
      double precision r1,r2
c     label
      character*(Nchar_mx) label
      label='subroutine sample_cone_half_angle'

      call random_gen(r1)
      call random_gen(r2)
      theta=dacos(1.0D+0-r1*(1.0D+0-dcos(theta_max)))
      phi=2.0D+0*pi*r2

      return
      end


      
      subroutine sample_cone_solid_angle(omega,theta,phi)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to uniformly sample the solid angle corresponding to a cone
c     defined by its solid angle
c     
c     Input:
c       + omega: cone solid angle [sr]
c     
c     Output:
c       + theta: angle between the cone axis and the sampled direction
c       + phi: arbitrary sampled over [0-2pi]
c     
c     I/O
      double precision omega
      double precision theta
      double precision phi
c     temp
      double precision theta_max
c     label
      character*(Nchar_mx) label
      label='subroutine sample_cone_solid_angle'

      theta_max=dacos(1.0D+0-omega/(2.0D+0*pi))
      call sample_cone_half_angle(theta_max,theta,phi)

      return
      end

      

      subroutine sample_uniform_double(val1,val2,alpha)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample a value between "val1" and "val2", uniformally
c     
c     Input:
c       + val1: lower boundary
c       + val2: higher boundary
c
c     Output:
c       + alpha: random number in [val1, val2] range
c
c     I/O
      double precision val1,val2,alpha
c     temp
      double precision r
c     label
      character*(Nchar_mx) label
      label='subroutine sample_uniform_double'

      if (val2.lt.val1) then
         call error(label)
         write(*,*) 'val2=',val2
         write(*,*) '< val1=',val1
         stop
      else
         call random_gen(r)
         alpha=val1+(val2-val1)*r
      endif

      return
      end

      

      subroutine sample_uniform_integer(N1,N2,i)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an integer between N1 and N2 (included)
c     on a random uniform basis
c
c     Inputs:
c       + N1: minimum value the integer can take
c       + N2: maximum value the integer can take
c
c     Output:
c       + i: integer value between N1 and N2
c
c     I/O
      integer N1,N2,i
c     temp
      double precision r
c     label
      character*(Nchar_mx) label
      label='subroutine sample_uniform_integer'

      call random_gen(r)
      i=N1+int((N2-N1+1)*r)

      return
      end

      

      subroutine sample_integer(N,cdf,i)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an integer according to a set of probabilities
c     This routine uses a dichotomy-based scheme in order to invert the cumulated probability function
c     This is supposed to speed things up for large values of N
c
c     Inputs:
c       + N: number of probabilities
c       + cdf: cumulated probabilities
c
c     Output:
c       + i: selected integer value
c
c     I/O
      integer N
      double precision cdf(0:Nproba_mx)
      integer i
c     temp
      double precision r
      integer idx,idx1,idx2
      logical keep_looking,i_found
      integer iter
c     parameters
      double precision epsilon_cdf
      parameter(epsilon_cdf=1.0D-6) ! max. error over the last value of the cdf
c     label
      character*(Nchar_mx) label
      label='subroutine sample_integer'

      if (N.gt.Nproba_mx) then
         call error(label)
         write(*,*) 'Number of elements in cdf=',N
         write(*,*) '> Nproba_mx=',Nproba_mx
         stop
      endif
      if (dabs(cdf(N)-1.0D+0).gt.epsilon_cdf) then
         call error(label)
         write(*,*) 'cdf(',N,')=',cdf(N)
         write(*,*) 'should be equal to 1'
         write(*,*) '|cdf(',N,')-1|=',dabs(cdf(N)-1.0D+0)
         write(*,*) '> epsilon_cdf=',epsilon_cdf
         stop
      endif

      call random_gen(r)
      keep_looking=.true.
      i_found=.false.
      idx1=0
      idx2=N
      iter=0
      do while (keep_looking)
         iter=iter+1
         if (iter.gt.Niter_mx) then
            call error(label)
            write(*,*) 'iter=',iter
            write(*,*) '> Niter_mx=',Niter_mx
            write(*,*) 'r=',r
            do idx=0,N
               write(*,*) 'cdf(',idx,')=',cdf(idx)
            enddo               ! idx
            stop
         else
            idx=(idx1+idx2)/2
            if ((r.gt.cdf(idx)).and.(r.le.cdf(idx+1))) then
               keep_looking=.false.
               i=idx+1
               i_found=.true.
            else
               if (r.gt.cdf(idx)) then
                  idx1=idx
               else
                  idx2=idx
               endif
            endif               ! cdf(idx) < r < cdf(idx+1)
         endif                  ! iter > Niter_mx            
      enddo                     ! while (keep_looping)
      if (.not.i_found) then
         call error(label)
         write(*,*) 'cdf could not be inverted'
         write(*,*) 'idx=',idx
         write(*,*) 'r=',r
         write(*,*) 'cdf(',Nproba_mx,')=',cdf(Nproba_mx)
         stop
      endif

      return
      end


      
      subroutine sample_emission_direction_on_surface(dim,normal,
     &     u,pdf_u)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an emission direction from a position on a surface
c     according to pdf(u)=|u.n|/pi
c
c     Input:
c       + dim: space dimension
c       + normal: normal to the surface @ emission position
c
c     Output:
c       + u: direction
c       + pdf_u: corresponding pdf
c       
c     I/O
      integer dim
      double precision normal(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision pdf_u
c     temp
      integer i
      double precision j1(1:Ndim_mx)
      double precision theta,phi
      double precision pdf_theta,pdf_phi
      double precision v(1:Ndim_mx)
      double precision product_normalj1
      double precision  product_un
      double precision normals(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine sample_emission_direction_on_surface'

      call vector_j1(dim,normal,j1)
      call sample_theta_surface(theta,pdf_theta)
      call sample_phi_uniform(phi,pdf_phi)

c     First rotation: vector "normal" is rotated by "theta" around "j1" -> vector "v"
      call rotation(dim,normal,theta,j1,v)
      call scalar_product(dim,normal,j1,product_normalj1)
      if (dabs(product_normalj1).gt.1.0D-8) then
         call error(label)
         write(*,*) 'normal=',(normal(i),i=1,dim)
         write(*,*) 'j1=',(j1(i),i=1,dim)
         write(*,*) '|normal.j1|=',product_normalj1
         write(*,*) 'should be null'
         call cart2spher(normal(1),normal(2),normal(3),normals(1),normals(2),normals(3))
         write(*,*) 'normals=',(normals(i),i=1,dim)
         stop
      endif
c     Second rotation: vector "v" is rotated by "phi" around "normal" -> vector "u"
      call rotation(dim,v,phi,normal,u)
c     compute pdf_u(u)
      pdf_u=pdf_theta*pdf_phi
c     double check
      call scalar_product(dim,normal,u,product_un)
      if (dabs(product_un-dcos(theta)).gt.1.0D-8) then
         call error(label)
         write(*,*) 'cos(',theta,')=',dcos(theta)
         write(*,*) '|u.n|=',product_un
         write(*,*) 'normal=',(normal(i),i=1,dim)
         write(*,*) 'j1=',(j1(i),i=1,dim)
         write(*,*) '|normal.j1|=',product_normalj1
         stop
      endif

      return
      end

      

      subroutine sample_theta_surface(theta,pdf_theta)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to sample an emission angle [0,pi/2] from a boundary
c     according to pdf(w)=|u.n|/pi
c
c     Output:
c       + theta: emission angle [0,pi/2] between the normal and a direction
c       + pdf_theta: probability density function corresponding to the chosen angle
c

c     output
      double precision theta
      double precision pdf_theta
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine sample_theta_surface'

      call random_gen(R)
      theta=dacos(dsqrt(R))
      pdf_theta=2.0D+0*dcos(theta)
c     Tests
      if ((theta.lt.0.0D+0).or.(theta.gt.pi/2.0D+0)) then
         call error(label)
         write(*,*) 'theta=',theta
         write(*,*) 'is not between 0 and pi/2'
         write(*,*) 'R=',R
         stop
      endif
c     Tests

      return
      end

      

      subroutine sample_phi_uniform(phi,pdf_phi)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to sample an emission angle (volumic emission) phi
c     uniformally in the [0,2*pi] range
c
c     Output:
c       + phi: value of phi in the [0,2*pi] range
c       + pdf_phi: associated probability density function
c
c     I/O
      double precision phi
      double precision pdf_phi
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine sample_phi_uniform'

      call random_gen(R)
      phi=2.0D+0*pi*R
      pdf_phi=1.0D+0/(2.0D+0*pi)

      return
      end
