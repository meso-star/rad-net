      program sw_Fobs
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the SW flux at a given position,
c     reflected by a sphere surrounded by a atmosphere, within the following model:
c     
c     + purely absorbing, homogeneous and gray atmosphere of fixed width
c     + diffuse surface reflectivity, homogeneous and gray
c     + integration in a given spectral interval
c     + illumination source (star) is a blackbody of known brightness temperature
c     
c     Variables
      integer dim
c     Data
      character*(Nchar_mx) datafile
      double precision R
      double precision D
      double precision Hatm
      double precision ka
      double precision rho
      double precision lambda_lo
      double precision lambda_hi
      double precision Ds
      double precision Rs
      double precision Ts
      double precision theta_s
      double precision phi_s
      integer Nevent
c     temp
      character*(Nchar_mx) seed_file
      integer seed,j
      double precision center(1:Ndim_mx)
      double precision Fobs
      double precision inc_Fobs
c     Debug
      double precision omega_source
      double precision planck_int_wavlng
c     Debug
c     label
      character*(Nchar_mx) label
      label='program sw_Fobs'

      dim=3
c     Reading 'data.in'
      datafile='./data.in'
      call read_data(datafile,R,D,Hatm,ka,rho,lambda_lo,lambda_hi,Ds,Rs,Ts,theta_s,phi_s,Nevent)
      
c     MC estimation
      do j=1,dim
         center(j)=0.0D+0
      enddo                     ! j
      seed_file='./data/seed'
      call read_seed(seed_file,seed)
      call initialize_random_generator(seed)
      call MC_sw_flux(dim,center,R,D,Hatm,ka,rho,lambda_lo,lambda_hi,Ds,Rs,Ts,theta_s,phi_s,Nevent,
     &     Fobs,inc_Fobs)
      write(*,*) 'Flux=',Fobs,' +/-',inc_Fobs,' W/m²'
      call cone_sphere_solid_angle(Rs,Ds,omega_source)

      end
