      subroutine MC_sw_flux(dim,center,R,D,Hatm,ka,rho,lambda_lo,lambda_hi,Ds,Rs,Ts,theta_s,phi_s,Nevent,
     &     Fobs,inc_Fobs)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the SW flux at a given position, in a given solid angle
c     
c     Input:
c       + dim: dimension of space
c       + center: center of the sphere [m,m,m]
c       + R: radius of the spherical body [m]
c       + D: distance between spherical body center and observation position [m]
c       + Hatm: height of the atmosphere [m]
c       + ka: absorption coefficient [inv. m]
c       + rho: surface reflectivity
c       + lambda_lo: lower wavelength [micrometers]
c       + lambda_hi: higher wavelength [micrometers]
c       + Ds: distance to star [km]
c       + Rs: radius of star [km]
c       + Ts: brightness temperature of the star [K]
c       + theta_s: latitude of star in local spherical body referential [rad]
c       + phi_s: longitude of star in local spherical body referential [rad]
c       + Nevent: number of statistical events
c     
c     Output:
c       + Fobs: flux [W/m²]
c       + inc_Fobs: standard deviation over the flux [W/m²]
c     
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision R
      double precision D
      double precision Hatm
      double precision ka
      double precision rho
      double precision lambda_lo
      double precision lambda_hi
      double precision Ds
      double precision Rs
      double precision Ts
      double precision theta_s
      double precision phi_s
      integer Nevent
      double precision Fobs
      double precision inc_Fobs
c     temp
      integer event
      double precision weight
      double precision sum_w,sum_w2
      double precision mean
      double precision variance
      double precision std_dev
      integer i,j
      double precision int_Bs
      double precision theta_obs,omega_obs
      double precision omega_s
      double precision theta,phi
      double precision x_obs(1:Ndim_mx)
      double precision x_int(1:Ndim_mx)
      double precision n_xint(1:Ndim_mx)
      double precision x_top1(1:Ndim_mx)
      double precision x_top2(1:Ndim_mx)
      double precision sigma_top1,sigma_top2
      double precision u(1:Ndim_mx)
      double precision us(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision minus_u0(1:Ndim_mx)
      double precision length,tau
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
      double precision Ms(1:Ndim_mx,1:Ndim_mx)
      logical source_is_visible
      double precision prod,pdf_u
      double precision solar_transmissivity
c     functions
      double precision planck_int_wavlng
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine MC_sw_flux'

      theta_obs=dasin(R/D)      ! rad
      call cone_sphere_solid_angle(R,D,omega_obs) ! omega_obs: solid angle under which the sphere is perceived from the observation position [sr]
      call cone_sphere_solid_angle(Rs,Ds,omega_s) ! omega_s: solid angle under which the source is perceived from the sphere (assuming a constant) [sr]
      int_Bs=planck_int_wavlng(lambda_lo,lambda_hi,Ts) ! [W//m²/sr]
      
      write(*,*) 'MC estimation...'
c     progress display ---
      ntot=Nevent
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     --- progress display
      sum_w=0.0D+0
      sum_w2=0.0D+0
      do event=1,Nevent
         weight=0.0D+0
c     Observation position in the referential of the sphere
         x_obs(1)=center(1)+D
         x_obs(2)=center(2)
         x_obs(3)=center(3)
c     Sample a (theta,phi) direction from the observation position
         call sample_cone_solid_angle(omega_obs,theta,phi)
c     get u0
         u0(1)=-dcos(theta)
         u0(2)=dsin(theta)*dcos(phi)
         u0(3)=dsin(theta)*dsin(phi)
c     intersection(s) between the (x_obs,u0) line and the sphere of radius R
         call line_sphere_intersect(.false.,dim,
     &        x_obs,u0,center,R,
     &        n_int,P0,k0,P1,k1,P2,k2)
         if (n_int.eq.0) then
            call error(label)
            write(*,*) 'No intersection between line:'
            write(*,*) 'x_obs=',(x_obs(j),j=1,dim)
            write(*,*) 'u0=',(u0(j),j=1,dim)
            write(*,*) 'And sphere:'
            write(*,*) 'center=',(center(j),j=1,dim)
            write(*,*) 'radius:',R
            stop
         else if (n_int.eq.1) then
            call copy_vector(dim,P0,x_int)
         else if (n_int.eq.2) then
            call copy_vector(dim,P1,x_int)
         endif
c     Intersection between (x_int,-u0) and sphere of radius R+Hatm
         call scalar_vector(dim,-1.0D+0,u0,minus_u0)
         call line_sphere_from_inside_intersect(.false.,dim,
     &        x_int,minus_u0,center,R+Hatm,x_top1,sigma_top1)
c     Sample direction over solar disk
c     1 - Rotation matrix to main solar direction
         call birotation_matrix(dim,theta_s,phi_s,Ms)
c     2 - Sample direction over the solar disk
         call sample_cone_solid_angle(omega_s,theta,phi)
         u(1)=dcos(theta)
         u(2)=dsin(theta)*dcos(phi)
         u(3)=dsin(theta)*dsin(phi)
c     3 - Rotate direction u: us is the direction sampled accross the solar disk, in the
c     local referential of the sphere
         call matrix_vector(dim,Ms,u,us)
c     Transmission between the source and position x_int in the us direction
         call transmission_to_source(dim,center,R,Hatm,ka,Ds,Rs,theta_s,phi_s,x_int,us,solar_transmissivity)
c     Sample propagation direction after reflexion
         call sample_emission_direction_on_surface(dim,n_xint,u,pdf_u) ! pdf_u [sr⁻¹]
c
         if (solar_transmissivity.gt.0.0D+0) then
            weight=weight+int_Bs*omega_s*pdf_u*rho*dexp(-ka*(sigma_top1))*solar_transmissivity ! W/m²
         endif
         sum_w=sum_w+weight
         sum_w2=sum_w2+weight**2.0D+0
c     
c     progress display ---
         ndone=ndone+1
         fdone=dble(ndone)/dble(ntot)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif
c     --- progress display
      enddo                     ! event
c      
c     progress display ---
      write(*,*)
c     --- progress display
      
      call statistics(Nevent,sum_w,sum_w2,mean,variance,std_dev)
      Fobs=mean
      inc_Fobs=std_dev

      return
      end
