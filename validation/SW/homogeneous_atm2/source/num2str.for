c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine num2str(num,str,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str
      logical err_code
c     temp
      integer absnum,sign
      character*(Nchar_mx) f
c     label
      character*(Nchar_mx) label
      label='subroutine num2str'

      if (num.lt.0) then
         sign=-1
      else
         sign=1
      endif
      absnum=abs(num)
      
      err_code=.false.
      if ((absnum.ge.0).and.(absnum.lt.10)) then
         write(str,101) absnum
      else if ((absnum.ge.10).and.(absnum.lt.100)) then
         write(str,102) absnum
      else if ((absnum.ge.100).and.(absnum.lt.1000)) then
         write(str,103) absnum
      else if ((absnum.ge.1000).and.(absnum.lt.10000)) then
         write(str,104) absnum
      else if ((absnum.ge.10000).and.(absnum.lt.100000)) then
         write(str,105) absnum
      else if ((absnum.ge.100000).and.(absnum.lt.1000000)) then
         write(str,106) absnum
      else if ((absnum.ge.1000000).and.(absnum.lt.10000000)) then
         write(str,107) absnum
      else if ((absnum.ge.10000000).and.(absnum.lt.100000000)) then
         write(str,108) absnum
      else if ((absnum.ge.100000000).and.(absnum.lt.1000000000)) then
         write(str,109) absnum
      else
         err_code=.true.
         goto 666
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end


      
      subroutine bignum2str(num,str,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer*8 num
      character*(Nchar_mx) str
      logical err_code
c     temp
      integer*8 absnum
      integer sign
      character*(Nchar_mx) f
c     label
      character*(Nchar_mx) label
      label='subroutine bignum2str'

      if (num.lt.0) then
         sign=-1
      else
         sign=1
      endif
      absnum=abs(num)
      
      err_code=.false.
      if ((absnum.ge.0).and.(absnum.lt.10)) then
         write(str,101) absnum
      else if ((absnum.ge.10).and.(absnum.lt.100)) then
         write(str,102) absnum
      else if ((absnum.ge.100).and.(absnum.lt.1000)) then
         write(str,103) absnum
      else if ((absnum.ge.1000).and.(absnum.lt.10000)) then
         write(str,104) absnum
      else if ((absnum.ge.10000).and.(absnum.lt.100000)) then
         write(str,105) absnum
      else if ((absnum.ge.100000).and.(absnum.lt.1000000)) then
         write(str,106) absnum
      else if ((absnum.ge.1000000).and.(absnum.lt.10000000)) then
         write(str,107) absnum
      else if ((absnum.ge.10000000).and.(absnum.lt.100000000)) then
         write(str,108) absnum
      else if ((absnum.ge.100000000).and.(absnum.lt.1000000000)) then
         write(str,109) absnum
      else
         err_code=.true.
         goto 666
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end



      subroutine num2str1(num,str1,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string of size 1
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str2: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str1
      logical err_code
c     temp
      character*1 kch1
c     label
      character*(Nchar_mx) label
      label='subroutine num2str1'

      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str1=trim(kch1)
         err_code=.false.
      else
         err_code=.true.
      endif

      return
      end



      subroutine num2str2(num,str2,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string of size 2
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str2: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str2
      logical err_code
c     temp
      character*1 zeroch,kch1
      character*2 kch2
c     label
      character*(Nchar_mx) label
      label='subroutine num2str2'

      err_code=.false.
      write(zeroch,11) 0
      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str2=trim(zeroch)//trim(kch1)
      else if ((num.ge.10).and.(num.lt.100)) then
         write(kch2,12) num
         str2=trim(kch2)
      else
         err_code=.true.
      endif

      return
      end



      subroutine num2str3(num,str3,err_code)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert an integer to a character string of size 3
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str3: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str3
      logical err_code
c     temp
      character*(Nchar_mx) str_tmp
c     label
      character*(Nchar_mx) label
      label='subroutine num2str3'

      call num2str(num,str_tmp,err_code)
      if (.not.err_code) then
         if (num.lt.10) then
            str3='00'//trim(str_tmp)
         else if (num.lt.100) then
            str3='0'//trim(str_tmp)
         else if (num.lt.1000) then
            str3=trim(str_tmp)
         else
            call error(label)
            write(*,*) 'Bad input argument:'
            write(*,*) 'num=',num
            write(*,*) 'should be < 1000'
            stop
         endif
      endif

      return
      end



      subroutine num2str4(num,str4,err_code)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert an integer to a character string of size 4
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str4: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c

c     I/O
      integer num
      character*(Nchar_mx) str4
      logical err_code
c     temp
      character*(Nchar_mx) str_tmp
c     label
      character*(Nchar_mx) label
      label='subroutine num2str4'
c
      call num2str(num,str_tmp,err_code)
c      
      if (.not.err_code) then
         if (num.lt.10) then
            str4='000'//trim(str_tmp)
         else if (num.lt.100) then
            str4='00'//trim(str_tmp)
         else if (num.lt.1000) then
            str4='0'//trim(str_tmp)
         else if (num.lt.10000) then
            str4=trim(str_tmp)
         else
            call error(label)
            write(*,*) 'Bad input argument:'
            write(*,*) 'num=',num
            write(*,*) 'should be < 10000'
            stop
         endif
      endif

      return
      end
