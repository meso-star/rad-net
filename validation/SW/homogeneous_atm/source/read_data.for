      subroutine read_data(datafile,R,D,Hatm,ka,epsilon,lambda_lo,lambda_hi,Ds,Rs,Ts,theta_s,phi_s,Nevent)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to read a input data file for program "sw_Fobs"
c     
c     Input:
c       + datafile: input data file
c     
c     Output:
c       + R: radius of the spherical body [m]
c       + D: distance between spherical body center and observation position [m]
c       + H: height of the atmosphere [m]
c       + ka: absorption coefficient [inv. m]
c       + epsilon: surface reflectivity
c       + lambda_lo: lower wavelength [micrometers]
c       + lambda_hi: higher wavelength [micrometers]
c       + Ds: distance to star [km]
c       + Rs: radius of star [km]
c       + Ts: brightness temperature of the star [K]
c       + theta_s: latitude of star in local spherical body referential [rad]
c       + phi_s: longitude of star in local spherical body referential [rad]
c       + Nevent: number of statistical events
c     
c     I/O
      character*(Nchar_mx) datafile
      double precision R
      double precision D
      double precision Hatm
      double precision ka
      double precision epsilon
      double precision lambda_lo
      double precision lambda_hi
      double precision Ds
      double precision Rs
      double precision Ts
      double precision theta_s
      double precision phi_s
      integer Nevent
c     temp
      integer i,ios
      double precision deg2rad
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
         do i=1,8
            read(11,*)
         enddo                  ! i
         read(11,*) Nevent
         do i=1,5
            read(11,*)
         enddo                  ! i
         read(11,*) ka
         read(11,*)
         read(11,*) epsilon
         read(11,*)
         read(11,*) lambda_lo
         read(11,*)
         read(11,*) lambda_hi
         do i=1,5
            read(11,*)
         enddo                  ! i
         read(11,*) R
         read(11,*)
         read(11,*) D
         read(11,*)
         read(11,*) Hatm
         do i=1,5
            read(11,*)
         enddo                  ! i
         read(11,*) Ds
         read(11,*)
         read(11,*) Rs
         read(11,*)
         read(11,*) Ts
         read(11,*)
         read(11,*) theta_s
         read(11,*)
         read(11,*) phi_s
      endif
      close(11)

c     Inconsistencies
      if (R.le.0.0D+0) then
         call error(label)
         write(*,*) 'R=',R
         write(*,*) 'should be positive'
         stop
      endif
      if (D.le.0.0D+0) then
         call error(label)
         write(*,*) 'D=',D
         write(*,*) 'should be positive'
         stop
      endif
      if (Hatm.le.0.0D+0) then
         call error(label)
         write(*,*) 'Hatm=',Hatm
         write(*,*) 'should be positive'
         stop
      endif
      if (ka.le.0.0D+0) then
         call error(label)
         write(*,*) 'ka=',ka
         write(*,*) 'should be positive'
         stop
      endif
      if ((epsilon.lt.0.0D+0).or.(epsilon.gt.1.0D+0)) then
         call error(label)
         write(*,*) 'epsilon=',epsilon
         write(*,*) 'should be in the [0,1] range'
         stop
      endif
      if (lambda_lo.le.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_lo=',lambda_lo
         write(*,*) 'should be positive'
         stop
      endif
      if (lambda_hi.le.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_hi=',lambda_hi
         write(*,*) 'should be positive'
         stop
      endif
      if (Ds.le.0.0D+0) then
         call error(label)
         write(*,*) 'Ds=',Ds
         write(*,*) 'should be positive'
         stop
      endif
      if (Rs.le.0.0D+0) then
         call error(label)
         write(*,*) 'Rs=',Rs
         write(*,*) 'should be positive'
         stop
      endif
      if (Ts.le.0.0D+0) then
         call error(label)
         write(*,*) 'Ts=',Ts
         write(*,*) 'should be positive'
         stop
      endif
      if ((theta_s.lt.-90.0D+0).or.(theta_s.gt.90.0D+0)) then
         call error(label)
         write(*,*) 'theta_s=',theta_s
         write(*,*) 'should be in the [-90,90]° range'
         stop
      endif
      if ((phi_s.lt.0.0D+0).or.(phi_s.gt.360.0D+0)) then
         call error(label)
         write(*,*) 'phi_s=',phi_s
         write(*,*) 'should be in the [0,360]° range'
         stop
      endif
      if (Nevent.le.0) then
         call error(label)
         write(*,*) 'Nevent=',Nevent
         write(*,*) 'should be positive'
         stop
      endif

c     Conversions
      R=R*1.0D+3                ! km -> m
      D=D*1.0D+3                ! km -> m
      Hatm=Hatm*1.0D+3          ! km -> m
      Ds=Ds*1.0D+3              ! km -> m
      Rs=Rs*1.0D+3              ! km -> m

      deg2rad=pi/180.0D+0       ! rad/deg
      theta_s=theta_s*deg2rad   ! deg -> rad
      phi_s=phi_s*deg2rad       ! deg -> rad

      return
      end
