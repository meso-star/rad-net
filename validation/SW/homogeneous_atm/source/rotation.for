c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine birotation_matrix(dim,theta,phi,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the rotation matrix of rotation
c     by angle "theta" around axis -Y and then by angle "phi"
c     around the Z-axis.
c
c     Inputs:
c       + dim: dimension of vectors
c       + theta: rotation angle around axis -Y [rad]
c       + phi: rotation angle around axis Z [rad]
c     
c     Outputs:
c       + M: rotation matrix
c
c     I/O
      integer dim
      double precision theta
      double precision phi
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      double precision axis(1:Ndim_mx)
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision M2(1:Ndim_mx,1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine birotation_matrix'

c     Rotation 1: axis -Y, angle theta
      axis(1)=0.0D+0
      axis(2)=-1.0D+0
      axis(3)=0.0D+0
      call rotation_matrix(dim,theta,axis,M1)

c     Rotation 2: axis Z, angle phi
      axis(1)=0.0D+0
      axis(2)=0.0D+0
      axis(3)=1.0D+0
      call rotation_matrix(dim,phi,axis,M2)

c     Combination of R1 then R2:
      call multiply_matrix(dim,M2,M1,M)

      return
      end
      


      subroutine rotation_matrix(dim,alpha,axis,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the rotation matrix of rotation
c     by angle "alpha" around a specified axis
c
c     Inputs:
c       + dim: dimension of vectors
c       + alpha: rotation angle
c       + axis: (ax,ay,az) coordinates of the elementary vector definig the direction of the rotation axis
c     
c     Outputs:
c       + M: rotation matrix
c

c     inputs
      integer dim
      double precision alpha
      double precision axis(1:Ndim_mx)
c     ouputs
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      double precision scalar
      double precision naxis(1:Ndim_mx)
      double precision I3(1:Ndim_mx,1:Ndim_mx)
      double precision Maxis1(1:Ndim_mx,1:Ndim_mx)
      double precision Maxis2(1:Ndim_mx,1:Ndim_mx)
      double precision Mt1(1:Ndim_mx,1:Ndim_mx)
      double precision Mt2(1:Ndim_mx,1:Ndim_mx)
      double precision Mt3(1:Ndim_mx,1:Ndim_mx)
      double precision Mt4(1:Ndim_mx,1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine rotation_matrix'

c     checking inputs are understood
      call normalize_vector(dim,axis,naxis)
      call axis_matrix(naxis,Maxis1,Maxis2)
c     computation of rotation matrix M
      call identity_matrix(dim,I3)
      scalar=dcos(alpha)
      call matrix_scalar(dim,I3,scalar,Mt1)
      scalar=1.0D+0-dcos(alpha)
      call matrix_scalar(dim,Maxis1,scalar,Mt2)
      scalar=dsin(alpha)
      call matrix_scalar(dim,Maxis2,scalar,Mt3)
      call add_matrix(dim,Mt1,Mt2,Mt4)
      call add_matrix(dim,Mt4,Mt3,M)

      return
      end



      subroutine axis_matrix(axis,Maxis1,Maxis2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute axis matrices "Maxis1" and "Maxis2"
c
c     Input:
c       + axis: coordinates of the rotation axis
c
c     Output:
c       + Maxis1, Maxis2: matrices used to compute the 3D rotation matrix
c
c     I/O
      double precision axis(1:Ndim_mx)
      double precision Maxis1(1:Ndim_mx,1:Ndim_mx)
      double precision Maxis2(1:Ndim_mx,1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine axis_matrix'

      Maxis1(1,1)=axis(1)**2.0D+0
      Maxis1(1,2)=axis(1)*axis(2)
      Maxis1(1,3)=axis(1)*axis(3)
      Maxis1(2,1)=axis(1)*axis(2)
      Maxis1(2,2)=axis(2)**2.0D+0
      Maxis1(2,3)=axis(2)*axis(3)
      Maxis1(3,1)=axis(1)*axis(3)
      Maxis1(3,2)=axis(2)*axis(3)
      Maxis1(3,3)=axis(3)**2.0D+0

      Maxis2(1,1)=0.0D+0
      Maxis2(1,2)=-axis(3)
      Maxis2(1,3)=axis(2)
      Maxis2(2,1)=axis(3)
      Maxis2(2,2)=0.0D+0
      Maxis2(2,3)=-axis(1)
      Maxis2(3,1)=-axis(2)
      Maxis2(3,2)=axis(1)
      Maxis2(3,3)=0.0D+0

      return
      end
      
