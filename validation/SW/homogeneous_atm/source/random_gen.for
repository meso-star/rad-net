c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_seed(seed_file,seed)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the random seed
c     
c     Input:
c       + seed_file: name of the seed file to read
c     
c     Output:
c       + seed: value of the random seed
c     
c     I/O
      character*(Nchar_mx) seed_file
      integer seed
c     temp
      integer ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_seed'
      
      open(10,file=trim(seed_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(seed_file)
         stop
      else
         read(10,*) seed
      endif                     ! ios.ne.0
      close(10)

      return
      end
      


      subroutine record_seed(seed_file,seed)
      implicit none
      include 'max.inc'
c     
c     Purpose: to record the random seed
c     
c     Input:
c       + seed_file: name of the seed file to record
c       + seed: value of the random seed
c     
c     I/O
      character*(Nchar_mx) seed_file
      integer seed
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine record_seed'
      
      open(10,file=trim(seed_file))
      write(10,*) seed
      close(10)

      return
      end


      subroutine initialize_random_generator(seed)
      implicit none
      include 'max.inc'
c     
c     Purpose: to initialize the random number generator
c     
c     Input:
c       + seed: value of the random seed
c     
c     I/O
      integer seed
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine initialize_random_generator'

      call zufalli(seed)

      return
      end
      


      subroutine random_gen(r)
      implicit none
      include 'max.inc'
c
c     Main pseudo-random number generator routine
c     (the one that must be called)
c     
c     Input:
c     
c     Output:
c       + r: random number in the [0,1] range
c
c     I/O
      double precision r
c     temp
      integer n,t
c     label
      character*(Nchar_mx) label
      label='subroutine random_gen'

      n=1
      t=0
 10   continue
      call zufall(n,r)
      if (r.eq.0.or.r.eq.1) then
         t=t+1
         if (t.gt.10) then
            call error(label)
            write(*,*) 'Too many zeroes'
            stop
         endif
         goto 10
      endif

      return
      end
