      program sw_Fobs
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the SW flux at a given position,
c     reflected by a sphere surrounded by a atmosphere, within the following model:
c     
c     + purely absorbing, homogeneous and gray atmosphere of fixed width
c     + diffuse surface reflectivity, homogeneous and gray
c     + integration in a given spectral interval
c     + illumination source (star) is a blackbody of known brightness temperature
c     
c     Variables
      integer dim
c     Data
      character*(Nchar_mx) datafile
      double precision R
      double precision D
      double precision Hatm
      double precision ka
      double precision epsilon
      double precision lambda_lo
      double precision lambda_hi
      double precision Ds
      double precision Rs
      double precision Ts
      double precision theta_s
      double precision phi_s
      integer Nevent
c     temp
      integer event
      double precision weight
      double precision sum_w,sum_w2
      double precision mean
      double precision variance
      double precision std_dev
      character*(Nchar_mx) seed_file
      integer seed
      integer i,j
      double precision int_Bs
      double precision omega_obs
      double precision omega_s
      double precision theta,phi
      double precision center(1:Ndim_mx)
      double precision x_obs(1:Ndim_mx)
      double precision x_int(1:Ndim_mx)
      double precision n_xint(1:Ndim_mx)
      double precision x_top1(1:Ndim_mx)
      double precision x_top2(1:Ndim_mx)
      double precision sigma_top1,sigma_top2
      double precision u(1:Ndim_mx)
      double precision us(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision minus_u0(1:Ndim_mx)
      double precision length,tau
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
      double precision Ms(1:Ndim_mx,1:Ndim_mx)
      logical source_is_visible
      double precision prod
c     functions
      double precision planck_int_wavlng
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='program sw_Fobs'

      dim=3
c     Reading 'data.in'
      datafile='./data.in'
      call read_data(datafile,R,D,Hatm,ka,epsilon,lambda_lo,lambda_hi,Ds,Rs,Ts,theta_s,phi_s,Nevent)
      
c     MC estimation
      do j=1,dim
         center(j)=0.0D+0
      enddo                     ! j
      call cone_sphere_solid_angle(R,D,omega_obs) ! omega_obs: solid angle under which the sphere is perceived from the observation position [sr]
      call cone_sphere_solid_angle(Rs,Ds,omega_s) ! omega_s: solid angle under which the source is perceived from the sphere (assuming a constant) [sr]
      int_Bs=planck_int_wavlng(lambda_lo,lambda_hi,Ts)
      seed_file='./data/seed'
      call read_seed(seed_file,seed)
      call initialize_random_generator(seed)

      write(*,*) 'MC estimation...'
c     progress display ---
      ntot=Nevent
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     --- progress display
      sum_w=0.0D+0
      sum_w2=0.0D+0
      do event=1,Nevent
         weight=0.0D+0
c     Observation position in the referential of the sphere
         x_obs(1)=D
         x_obs(2)=0.0D+0
         x_obs(3)=0.0D+0
c     Sample a (theta,phi) direction from the observation position
         call sample_cone_solid_angle(omega_obs,theta,phi)
c     get u0
         u0(1)=-dcos(theta)
         u0(2)=dsin(theta)*dcos(phi)
         u0(3)=dsin(theta)*dsin(phi)
c     intersection(s) between the (x_obs,u0) line and the sphere of radius R
         call line_sphere_intersect(.false.,dim,
     &        x_obs,u0,center,R,
     &        n_int,P0,k0,P1,k1,P2,k2)
         if (n_int.eq.0) then
            call error(label)
            write(*,*) 'No intersection between line:'
            write(*,*) 'x_obs=',(x_obs(j),j=1,dim)
            write(*,*) 'u0=',(u0(j),j=1,dim)
            write(*,*) 'And sphere:'
            write(*,*) 'center=',(center(j),j=1,dim)
            write(*,*) 'radius:',R
            stop
         else if (n_int.eq.1) then
            call copy_vector(dim,P0,x_int)
         else if (n_int.eq.2) then
            call copy_vector(dim,P1,x_int)
         endif
c     Intersection between (x_int,-u0) and sphere of radius R+Hatm
         call scalar_vector(dim,-1.0D+0,u0,minus_u0)
         call line_sphere_from_inside_intersect(.false.,dim,
     &        x_int,minus_u0,center,R+Hatm,x_top1,sigma_top1)
c     Sample direction over solar disk
c     1 - Rotation matrix to main solar direction
         call birotation_matrix(dim,theta_s,phi_s,Ms)
c     2 - Sample direction over the solar disk
         call sample_cone_solid_angle(omega_s,theta,phi)
         u(1)=dcos(theta)
         u(2)=dsin(theta)*dcos(phi)
         u(3)=dsin(theta)*dsin(phi)
c     3 - Rotate direction u: us is the direction sampled accross the solar disk, in the
c     local referential of the sphere
         call matrix_vector(dim,Ms,u,us)
c     Check Sun visibility along (x_int,us) line of sight
         call line_sphere_forward_intersect(.false.,dim,
     &        x_int,us,center,R,
     &        n_int,P0,k0,P1,k1,P2,k2)
c$$$         if (n_int.ne.0) then
c$$$            call error(label)
c$$$            write(*,*) 'No visibility of Sun from x_int=',(x_int(j),j=1,dim)
c$$$            write(*,*) 'omega_s=',omega_s
c$$$            write(*,*) 'theta=',theta
c$$$            write(*,*) 'phi=',phi
c$$$            write(*,*) 'u=',(u(j),j=1,dim)
c$$$            write(*,*) 'us=',(us(j),j=1,dim)
c$$$            write(*,*) 'Matrix Ms:'
c$$$            do i=1,dim
c$$$               write(*,*) (Ms(i,j),j=1,dim)
c$$$            enddo               ! i
c$$$            write(*,*) 'n_int=',n_int
c$$$            write(*,*) 'k1=',k1
c$$$            write(*,*) 'P1=',(P1(j),j=1,dim)
c$$$            write(*,*) 'k2=',k2
c$$$            write(*,*) 'P2=',(P2(j),j=1,dim)
c$$$            write(*,*) 'event=',event
c$$$            stop
c$$$         endif

         if (n_int.eq.0) then
            source_is_visible=.true.
         else
            source_is_visible=.false.
         endif

         if (source_is_visible) then
c     Intersection between (x_int,us) and sphere of radius R+Hatm
            call line_sphere_from_inside_intersect(.false.,dim,
     &           x_int,us,center,R+Hatm,x_top2,sigma_top2)
            call normal_on_sphere(dim,center,R,x_int,n_xint)
            call scalar_product(dim,us,n_xint,prod)
            weight=weight+int_Bs*omega_s*prod/pi*epsilon*dexp(-ka*(sigma_top1+sigma_top2))*omega_obs ! W/m²
c     Debug
c$$$            write(*,*) 'event:',event
c$$$            write(*,*) 'int_Bs=',int_Bs
c$$$            write(*,*) 'omega_s=',omega_s
c$$$            write(*,*) 'us=',(us(j),j=1,dim)
c$$$            write(*,*) 'n_xint=',(n_xint(j),j=1,dim)
c$$$            write(*,*) 'prod=',prod
c$$$            stop
c     Debug
         endif
         sum_w=sum_w+weight
         sum_w2=sum_w2+weight**2.0D+0
c     
c     progress display ---
         ndone=ndone+1
         fdone=dble(ndone)/dble(ntot)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif
c     --- progress display
      enddo                     ! event
c      
c     progress display ---
      write(*,*)
c     --- progress display
      
      call statistics(Nevent,sum_w,sum_w2,mean,variance,std_dev)
      write(*,*) 'Flux=',mean,' +/-',std_dev,' W/m²'

      end
