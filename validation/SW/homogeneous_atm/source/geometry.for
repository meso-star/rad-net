c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine line_sphere_from_inside_intersect(debug,dim,
     &     origin,u,center,radius,Pint,d2int)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the intersection position between a line
c     and a sphere in the case when the origin of the line is inside
c     the sphere: there must be only one intersection in the
c     forward direction.
c
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c
c     Output:
c       + Pint: cartesian coordinates of the intersection position
c       + d2int: distance to intersection position
c
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision radius
      double precision Pint(1:Ndim_mx)
      double precision d2int
c     temp
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
      double precision dOC
      double precision epsilon
c     label
      character*(Nchar_mx) label
      label='subroutine line_sphere_from_inside_intersect'

      epsilon=1.0D-8*radius
      
      call distance(dim,origin,center,dOC)
      if (dOC-radius.gt.epsilon) then
         call error(label)
         write(*,*) 'Distance origin-center=',dOC
         write(*,*) 'should be < radius=',radius
         stop
      endif
      
      call line_sphere_intersect(debug,dim,
     &     origin,u,center,radius,n_int,
     &     P0,k0,P1,k1,P2,k2)
      if (n_int.le.0) then
         call error(label)
         write(*,*) 'No intersection found'
         stop
      else if (n_int.eq.1) then
         call copy_vector(dim,P0,Pint)
      else if (n_int.eq.2) then
         if ((k1.gt.epsilon).and.(k2.gt.epsilon)) then
            call error(label)
            write(*,*) 'k1=',k1
            write(*,*) 'k2=',k2
            write(*,*) 'should be both < epsilon=',epsilon
            stop
         else
            if (k1.gt.epsilon) then
               call copy_vector(dim,P1,Pint)
               d2int=k1
            else if (k2.gt.epsilon) then
               call copy_vector(dim,P2,Pint)
               d2int=k2
            else
               call error(label)
               write(*,*) 'No suitable intersection found:'
               write(*,*) 'k1=',k1
               write(*,*) 'k2=',k2
               stop
            endif
         endif
      endif

      return
      end


      
      subroutine line_sphere_forward_intersect(debug,dim,
     &     origin,u,center,radius,
     &     n_int,P0,k0,P1,k1,P2,k2)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the intersection point(s) between a line
c     and a sphere in the general case (not centered at the origin)
c     but only intersections in the forward direction are provided.
c
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c
c     Output:
c       + n_int: number of intersections
c       + P0: cartesian coordinates of the intersection point when n_int=1
c       + k0: index of k for the solution when n_int=1
c       + P1,P2: cartesian coordinates of the intersections point when n_int=2, sorted by increasing distance to origin
c       + k1,k2: indexes of k for the solutions when n_int=2, sorted by increasing distance to origin
c
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision radius
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
c     temp
      double precision epsilon
      integer n_int_found
      double precision x0(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision t0,t1,t2
c     label
      character*(Nchar_mx) label
      label='subroutine line_sphere_forward_intersect'

      epsilon=radius*1.0D-8
      
      call line_sphere_intersect(debug,dim,
     &     origin,u,center,radius,
     &     n_int_found,x0,t0,x1,t1,x2,t2)

      n_int=0
      if (n_int_found.eq.1) then
         if (t0.gt.epsilon) then
            n_int=n_int+1
            call copy_vector(dim,x0,P0)
         endif
      else if (n_int_found.eq.2) then
         if (t1.gt.epsilon) then
            n_int=n_int+1
            call copy_vector(dim,x1,P1)
         endif
         if (t2.gt.epsilon) then
            n_int=n_int+1
            if (n_int.eq.1) then
               call copy_vector(dim,x2,P1)
            else if (n_int.eq.2) then
               call copy_vector(dim,x2,P2)
            endif
         endif            
      endif

      return
      end


      
      subroutine line_sphere_intersect(debug,dim,
     &     origin,u,center,radius,
     &     n_int,P0,k0,P1,k1,P2,k2)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the intersection point(s) between a line
c     and a sphere in the general case (not centered at the origin)
c     Update 2011-07-23:
c     only valid solutions (intersections met in the direction of propagation)
c     are provided.
c
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c
c     Output:
c       + n_int: number of intersections
c       + P0: cartesian coordinates of the intersection point when n_int=1
c       + k0: index of k for the solution when n_int=1
c       + P1,P2: cartesian coordinates of the intersections point when n_int=2, sorted by increasing distance to origin
c       + k1,k2: indexes of k for the solutions when n_int=2, sorted by increasing distance to origin
c
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision radius
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
c     temp
      double precision a,b,c,d
      integer nsol
      double precision un(1:Ndim_mx)
      double precision flight0(1:Ndim_mx)
      double precision flight1(1:Ndim_mx)
      double precision flight2(1:Ndim_mx)
      double precision epsilon_r
c     label
      character*(Nchar_mx) label
      label='subroutine line_sphere_intersect'

      epsilon_r=1.0D-5*radius

      call normalize_vector(dim,u,un)
      
      a=un(1)**2.0D+0+un(2)**2.0D+0+un(3)**2.0D+0
      b=2.0D+0*(un(1)*(origin(1)-center(1))
     &     +un(2)*(origin(2)-center(2))
     &     +un(3)*(origin(3)-center(3)))
      c=(origin(1)-center(1))**2.0D+0
     &     +(origin(2)-center(2))**2.0D+0
     &     +(origin(3)-center(3))**2.0D+0
     &     -radius**2.0D+0

c     find mathematical solutions
      call eq2deg(a,b,c,nsol,k0,k1,k2)

c     Debug
      if (debug) then
         write(*,*) 'a=',a
         write(*,*) 'b=',b
         write(*,*) 'c=',c
         write(*,*) 'nsol=',nsol
         write(*,*) 'k1=',k1
         write(*,*) 'k2=',k2
      endif
c     Debug

      n_int=nsol
      if (n_int.eq.1) then
         call scalar_vector(dim,k0,un,flight0)
         call add_vectors(dim,origin,flight0,P0)
         call distance(dim,center,P0,d)
         if (d.gt.epsilon_r) then
            n_int=0
            goto 666
         endif
      else if (n_int.eq.2) then
         call scalar_vector(dim,k1,un,flight1)
         call scalar_vector(dim,k2,un,flight2)
         call add_vectors(dim,origin,flight1,P1)
         call add_vectors(dim,origin,flight2,P2)
c     Debug
         if (debug) then
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
         endif
c     Debug
         call distance(dim,center,P1,d)
         if (dabs(d-radius).gt.epsilon_r) then
            n_int=0
            goto 666
         endif
         call distance(dim,center,P2,d)
         if (dabs(d-radius).gt.epsilon_r) then
            n_int=0
            goto 666
         endif
      endif

 666  continue
      return
      end


      
      subroutine eq2deg(a,b,c,nsol,x0,x1,x2)
      implicit none
      include 'max.inc'
c
c     Purpose: to solve a 2nd degree equation ax²+bx+c=0
c
c     Input:
c       + a,b,c: coefficients of the 2nd degree equation ax²+bx+c=0
c
c     Output:
c       + nsol: number of solution (0,1 or 2)
c       + x0: solution when nsol=1
c       + x1,x2: solutions when nsol=2 (x1<x2)
c
c     I/O
      double precision a,b,c
      integer nsol
      double precision x0,x1,x2
c     temp
      double precision t1,t2
      double precision delta
c     label
      character*(Nchar_mx) label
      label='subroutine eq2deg'

      delta=b**2.0D+0-4.0D+0*a*c
      if (delta.lt.0.0D+0) then
         nsol=0
      else if (delta.eq.0.0D+0) then
         nsol=1
         x0=-b/(2.0D+0*a)
      else if (delta.gt.0.0D+0) then
         nsol=2
         t1=(-b-dsqrt(delta))/(2.0D+0*a)
         t2=(-b+dsqrt(delta))/(2.0D+0*a)
         if (t1.lt.t2) then
            x1=t1
            x2=t2
         else
            x1=t2
            x2=t1
         endif
      else
         call error(label)
         write(*,*) 'delta=',delta
         stop
      endif

      return
      end


      
      subroutine normal_on_sphere(dim,
     &     center,radius,P,normal)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the outer normal to the sphere
c     for a given position
c
c     Input:
c       + dim: dimension of the vectors
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c       + P: position on the sphere
c
c     Output:
c       + normal: outgoing normal to the sphere @ P
c
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision radius
      double precision P(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
c     temp
      double precision CP(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine normal_on_sphere'

      call substract_vectors(dim,P,center,CP)
      call normalize_vector(dim,CP,normal)

      return
      end



      subroutine cone_sphere_solid_angle(R,D,omega)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the solid angle for a cone
c     tangent to a sphere
c     
c     Input:
c       + R: radius of the sphere [m] 
c       + D: distance between the tip of the cone and the center of the sphere [m]
c     
c     Output:
c       + omega: solid angle of the cone [sr]
c     
c     I/O
      double precision R
      double precision D
      double precision omega
c     temp
      double precision theta_max
c     label
      character*(Nchar_mx) label
      label='subroutine cone_solid_angle'

      theta_max=dasin(R/D)
      omega=2.0D+0*pi*(1.0D+0-dcos(theta_max))

      return
      end
