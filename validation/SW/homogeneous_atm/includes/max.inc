	integer Ndim_mx
	integer Nproba_mx
	integer Niter_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
	parameter(Nproba_mx=1000)
	parameter(Niter_mx=100)
	parameter(Nchar_mx=1000)
