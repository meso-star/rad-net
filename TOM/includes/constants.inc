c	Physical constants
c	
	double precision pi
	double precision k
	double precision c
	double precision h
	double precision sigma
	double precision Na
	double precision Rgp
	double precision AU
	double precision M_sun
	double precision M_earth
	double precision M_titan
	double precision R_sun
	double precision R_earth
	double precision R_titan
	double precision gravity_sun
	double precision gravity_earth
	double precision gravity_titan
	double precision T0_sun
	double precision T0_earth
	double precision T0_titan
	double precision P0_earth
	double precision P0_titan
	
        parameter(pi=3.141592653589793238462643383279502884197169399D+0)
	parameter(k=1.3806D-23)            ! J/K          (constante de Boltzmann)
	parameter(c=2.9979D+8)             ! m/s          (célérité de la lumière dans le vide)
	parameter(h=6.6262D-34)            ! Js           (constante de Planck)
	parameter(sigma=5.6696D-8)         ! W/m2/K4      (constante de Stefan-Boltzman)
        parameter(Na=6.02214179D+23)       ! molecule/mol (Avogadro)
	parameter(Rgp=8.31446261815324D+0) ! J/(mol.K)    (constante des gaz parfaits)
        parameter(AU=149597887.0D+3)       ! m            (unité astronomique)
	parameter(M_sun=1.9884D+30)        ! kg           (masse du Soleil)
	parameter(M_earth=5.9722D+24)      ! kg           (masse de la Terre)
	parameter(M_titan=1.345D+23)       ! kg           (masse de Titan)
	parameter(R_sun=6.95991756D+8)     ! m            (rayon du Soleil)
	parameter(R_earth=6378.136D+3)     ! m            (rayon de la Terre)
	parameter(R_titan=2575.50D+3)      ! m            (rayon de Titan)
	parameter(gravity_sun=273.95D+0)   ! m.s^-2       (acceleration de la gravité à la surface de la Terre)
	parameter(gravity_earth=9.8067D+0) ! m.s^-2       (acceleration de la gravité à la surface de la Terre)
	parameter(gravity_titan=1.352D+0)  ! m.s^-2       (acceleration de la gravité à la surface de Titan)
	parameter(T0_sun=5773.0D+0)        ! K            (température de brillance du Soleil)
	parameter(T0_earth=288.0D+0)       ! K            (température de brillance de la Terre)
	parameter(T0_titan=93.7D+0)        ! K            (température de brillance de Titan)
	parameter(P0_earth=1.013D+5)       ! Pa           (pression de surface sur Terre)
	parameter(P0_titan=1.467D+5)       ! Pa           (pression de surface sur Titan)
	