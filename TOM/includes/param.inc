c	Numeric parameters used by the MC algorithm
c
	double precision epsilon_intersection
	double precision epsilon_start

	parameter(epsilon_intersection=1.0D-1) ! accuracy for the detection of intersection [m]
	parameter(epsilon_start=1.0D-1) ! curvilign abscissa after which intersections are valid [m] ; has to be at least 10x the value of epsilon_intersection