c	Numeric parameters used for the size of arrays
c
	integer Ndim_mx
	integer Nproba_mx
	integer Ntry_mx
	integer Niter_mx
	integer Naerosol_mx
	integer Nlayer_mx
	integer Nlambda_sun_mx
	integer Nlambda_mx
	integer Nband_mx
	integer Nquad_mx
	integer Nicp_mx
	integer Nintersection_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
	parameter(Nproba_mx=700000)
	parameter(Ntry_mx=100)
	parameter(Niter_mx=100)
	parameter(Naerosol_mx=2)
	parameter(Nlayer_mx=100)
	parameter(Nlambda_mx=100)
	parameter(Nlambda_sun_mx=3500000)
	parameter(Nband_mx=100)
	parameter(Nquad_mx=20)
	parameter(Nicp_mx=4000)
	parameter(Nintersection_mx=100)
	parameter(Nchar_mx=1000)
