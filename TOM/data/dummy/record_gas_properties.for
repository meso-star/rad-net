      subroutine record_gas_radiative_properties_file(Nnode,Nband,Nq,
     &     lambda_min,lambda_max,w,band_idx,gas_radiative_properties_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record the gas radiative properties binary file.
c     Due to the huge amount of data the file is supposed to contain, this
c     routine must be called for every spectral interval (band). The
c     data arrays (ka and ks) have to be updated for the current band.
c     Triggers for opening/closing the binary file are respectively
c     band_idx=1 and band_idx=Nband, so the "band_idx" input has to
c     be updated accordingly between calls.
c     
c     Input:
c       + Nnode: number of nodes
c       + Nband: number of spectral intervals
c       + Nq: quadrature order
c       + lambda_min: lower wavelength for each interval [nm]
c       + lambda_max: higher wavelength for each interval [nm]
c       + w: quadrature weights
c       + band_idx: band index
c       + gas_radiative_properties_file: file to record
c     
c     Output: the required binary file
c     
c     I/O
      integer*8 Nnode
      integer Nband
      integer Nq
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nquad_mx)
      integer band_idx
      character*(Nchar_mx) gas_radiative_properties_file
c     temp
      logical open_file,close_file
      integer inode,iband,iq,i,ios
      integer*8 total_recorded,Nnode_tmp
      integer Nq_tmp
      integer remaining_byte
      logical*1 l1
      character*(Nchar_mx) ka_file,ks_file
      double precision ka,ks
c     label
      character*(Nchar_mx) label
      label='subroutine record_gas_radiative_properties_file'

      if (band_idx.eq.1) then
         open_file=.true.
      else
         open_file=.false.
      endif
      if (band_idx.eq.Nband) then
         close_file=.true.
      else
         close_file=.false.
      endif

      if (open_file) then
c     If binary file has to be opened, then write the preamble
         open(31,file=trim(gas_radiative_properties_file),form='unformatted',access='stream')
         write(31) int(pagesize,kind(Nnode))
         write(31) int(Nband,kind(Nnode))
         write(31) int(Nnode,kind(Nnode))
         do iband=1,Nband
            write(31) lambda_min(iband),lambda_max(iband),int(Nq,kind(Nnode))
            do iq=1,Nq
               write(31) w(iq)
            enddo               ! iq
         enddo                  ! iband
c     Padding file 31 ---
         total_recorded=(Nband+3)*size_of_int8+(Nq+2)*Nband*size_of_double
         call compute_padding2(pagesize,total_recorded,remaining_byte)
         write(31) (l1,i=1,remaining_byte)
c     --- Padding file 31
      endif                     ! open_file
      ka_file='./ka_gas.txt'
      ks_file='./ks_gas.txt'
      open(21,file=trim(ka_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(ka_file)
         stop
      endif
      read(21,*) Nnode_tmp
      if (Nnode_tmp.ne.Nnode) then
         call error(label)
         write(*,*) 'input Nnode=',Nnode
         write(*,*) 'written in file:',Nnode_tmp
         stop
      endif
      read(21,*) Nq_tmp
      if (Nq_tmp.ne.Nq) then
         call error(label)
         write(*,*) 'input Nq=',Nq
         write(*,*) 'written in file:',Nq_tmp
         stop
      endif
      open(22,file=trim(ks_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(ka_file)
         stop
      endif
      read(22,*) Nnode_tmp
      if (Nnode_tmp.ne.Nnode) then
         call error(label)
         write(*,*) 'input Nnode=',Nnode
         write(*,*) 'written in file:',Nnode_tmp
         stop
      endif
c     Then record all data for the current band
      do inode=1,Nnode
         read(22,*) ks
         write(31) real(ks)
      enddo                     ! inode
c     Padding file 31 ---
      total_recorded=Nnode*size_of_real
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(31) (l1,i=1,remaining_byte)
c     --- Padding file 31
      do iq=1,Nq
         do inode=1,Nnode
            read(21,*) ka
            write(31) real(ka)
         enddo                  ! inode
c     Padding file 31 ---
         total_recorded=Nnode*size_of_real
         call compute_padding2(pagesize,total_recorded,remaining_byte)
         write(31) (l1,i=1,remaining_byte)
c     --- Padding file 31
      enddo                     ! iq
      close(21)
      close(22)
      if (close_file) then
         close(31)
      endif

      return
      end



      subroutine record_gas_thermodynamic_properties_file(Nnode,gas_thermodynamic_properties_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record the gas thermodynamic properties file
c     
c     Input:
c       + Nnode: number of nodes
c       + gas_thermodynamic_properties_file: binary file to record
c     
c     Output: the required binay file
c     
c     I/O
      integer*8 Nnode
      character*(Nchar_mx) gas_thermodynamic_properties_file
c     temp
      integer inode,i
      logical*1 l1
      integer*8 total_recorded
      integer remaining_byte
      integer*8 record_size
      integer*8 alignment
      double precision T
      integer ios
      character*(Nchar_mx) Tfile
      integer*8 Nnode_tmp
c     label
      character*(Nchar_mx) label
      label='subroutine record_gas_thermodynamic_properties_file'

      Tfile='./T.txt'
      open(21,file=trim(Tfile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(Tfile)
         stop
      endif
      read(21,*) Nnode_tmp
      if (Nnode_tmp.ne.Nnode) then
         call error(label)
         write(*,*) 'input Nnode=',Nnode
         write(*,*) 'written in file:',Nnode_tmp
         stop
      endif
      
      record_size=int(size_of_real,kind(record_size))
      alignment=record_size
      
      open(32,file=trim(gas_thermodynamic_properties_file),form='unformatted',access='stream')
      write(32) int(pagesize,kind(Nnode))
      write(32) int(Nnode,kind(Nnode))
      write(32) record_size
      write(32) alignment
c     Padding file 32 ---
      total_recorded=4*size_of_int8
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(32) (l1,i=1,remaining_byte)
c     --- Padding file 32
      do inode=1,Nnode
         read(21,*) T
         write(32) real(T)
      enddo                     ! inode
c     Padding file 32 ---
      total_recorded=Nnode*record_size
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(32) (l1,i=1,remaining_byte)
c     --- Padding file 32
      close(32)
      close(21)
      
      return
      end
