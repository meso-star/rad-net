      subroutine aerosol_properties(dim,general_volumic_grid,Nlayer,Ntheta,Nphi,z_grid,Nnode,Nband,lambda_min,lambda_max,
     &     Naerosol,ka_aerosol,ks_aerosol,g_aerosol,prefix,iaerosol,
     &     phase_function_file,phase_function_list_file,aerosol_radiative_properties_file)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce aerosol related files for htrdr_planeto
c     
c     Input:
c       + dim: dimension of space
c       + general_volumic_grid: T: each node is shared by several cells; F: each cell has its own nodes, not shared by any other cell
c       + Nlayer: number of altitude interals
c       + Ntheta: number of latitude intervals
c       + Nphi: number of longitude intervals
c       + z_grid: radius at the top of each layer (relative to the center of the planet) [m]
c       + Nnode: number of nodes
c       + Nband: number of narrowbands
c       + lambda_min: lower wavelength for each band [µm]
c       + lambda_max: higher wavelength for each band [µm]
c       + Naerosol: number of aerosol modes
c       + ka_aerosol: absorption coefficient [m⁻¹]
c       + ks_aerosol: scattering coefficient [m⁻¹]
c       + g_aerosol: aerosol asymetry parameter
c       + prefix: character string that constitutes the basis of the phase function file names
c       + iaerosol: index of the aerosol 
c       + phase_function_file: phase function file to record
c       + phase_function_list_file: file to record (ascii)
c       + aerosol_radiative_properties_file: file for recording radiative properties of the aerosols
c     
c     Output: required files
c     
c     I/O
      integer dim
      logical general_volumic_grid
      integer Nlayer
      integer Ntheta
      integer Nphi
      double precision z_grid(0:Nlayer_mx)
      integer*8 Nnode
      integer Nband
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      integer Naerosol
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision g_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      character*(Nchar_mx) prefix
      integer iaerosol
      character*(Nchar_mx) phase_function_file
      character*(Nchar_mx) phase_function_list_file
      character*(Nchar_mx) aerosol_radiative_properties_file
c     temp
      integer phase_function_index
      integer phase_function_type
      integer Nlambda_HG
      double precision lambda_HG(1:Nlambda_mx)
      double precision g_HG(1:Nlambda_mx)
      integer phase_Nlambda
      double precision phase_lambda(1:Nlambda_mx)
      integer Nangle
      double precision phase_angle(1:Nangle_mx)
      double precision pf(1:Nlambda_mx,1:Nangle_mx)
      integer iband,ilayer,i,Npf
      integer*8 inode
      double precision z,theta,phi
      integer phase_function_idx
      double precision ka
      double precision ks
      double precision lambda_lo(1:Nband_mx)
      double precision lambda_hi(1:Nband_mx)
      double precision z1,z2,f
      integer ios,j
      character*(Nchar_mx) xnode_file,phase_function_idx_file,ka_file,ks_file
      double precision xnode(1:Ndim_mx)
      integer*8 Nnode_tmp
      integer iz,itheta,iphi,icell,inodeincell
      double precision z_mc,altitude
c     label
      character*(Nchar_mx) label
      label='subroutine aerosol_properties'

      ka_file='./ka_aerosol.txt'
      ks_file='./ks_aerosol.txt'
      xnode_file='./xnode.txt'
      open(13,file=trim(xnode_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(xnode_file)
         stop
      endif
      read(13,*) Nnode_tmp
      if (Nnode_tmp.ne.Nnode) then
         call error(label)
         write(*,*) 'input Nnode=',Nnode
         write(*,*) 'written in file:',Nnode_tmp
         stop
      endif
      write(*,*) 'Producing properties of aerosol mode ',iaerosol,' /',Naerosol

      phase_function_idx_file='./phase_function_idx.txt'
      open(21,file=trim(phase_function_idx_file))
      write(21,*) Nnode
      
      z1=0.0D+0                 ! [m]
      z2=1.0D+5                 ! [m]

      Nlambda_HG=Nband
      do iband=1,Nband
         lambda_HG(iband)=(lambda_min(iband)+lambda_max(iband))/2.0D+0*1.0D+3 ! µm -> nm
         lambda_lo(iband)=lambda_min(iband)*1.0D+3 ! µm -> nm
         lambda_hi(iband)=lambda_max(iband)*1.0D+3 ! µm -> nm
      enddo                     ! iband
      Nangle=0
      
      phase_function_type=0     ! 0: HG; 1: discrete
      phase_function_index=0
      do ilayer=1,Nlayer
         phase_function_index=phase_function_index+1
         if (iaerosol.eq.1) then
            if ((ilayer.ge.20).and.(ilayer.le.79)) then
               do iband=1,Nband
                  g_HG(iband)=0.20D+0
               enddo            ! iband
            else
               do iband=1,Nband
                  g_HG(iband)=0.0D+0
               enddo            ! iband
            endif
         else if (iaerosol.eq.2) then
            if ((ilayer.ge.30).and.(ilayer.le.39)) then
               do iband=1,Nband
                  g_HG(iband)=-0.30D+0
               enddo            ! iband
            else
               do iband=1,Nband
                  g_HG(iband)=0.0D+0
               enddo            ! iband
            endif
         else if (iaerosol.eq.3) then
            if ((ilayer.ge.50).and.(ilayer.le.69)) then
               do iband=1,Nband
                  g_HG(iband)=0.70D+0
               enddo            ! iband
            else
               do iband=1,Nband
                  g_HG(iband)=0.0D+0
               enddo            ! iband
            endif
         endif                  ! iaerosol
         call record_phase_function_definition_file(prefix,
     &        phase_function_index,phase_function_type,Nlambda_HG,lambda_HG,g_HG,
     &        phase_Nlambda,phase_lambda,Nangle,phase_angle,pf)
      enddo                     ! ilayer
      Npf=phase_function_index
      if (Npf.ne.Nlayer) then
         call error(label)
         write(*,*) 'Npf=',Npf
         write(*,*) 'should be equal to Nlayer=',Nlayer
         stop
      endif
      
c     binary file that defines the phase function at each node of the grid
      do inode=1,Nnode
         read(13,*) (xnode(j),j=1,dim)
         phi=xnode(1)     ! longitude (0,360) [deg]
         theta=xnode(2)   ! latitude (-90,90) [deg]
         z=xnode(3)       ! altitude relative to ground level [m]
c     Identify the vertical layer the node belongs to
         call identify_layer(dim,Nlayer,z_grid,z,iz)
         phase_function_idx=iz  ! index of the phase function definition file for each node
         write(21,*) phase_function_idx
      enddo                     ! inode
      close(21)
      call record_phase_function_file(Nnode,phase_function_file)
      write(*,*) 'File has been recorded:',trim(phase_function_file)
      write(*,*) 'Nnode=',Nnode

c     Record the list of phase function definition files
      call record_phase_function_list_file(prefix,Npf,phase_function_list_file)

      do iband=1,Nband
         write(*,*) 'Spectral interval ',iband,' /',Nband
         if (general_volumic_grid) then
            rewind(13)
            read(13,*) Nnode_tmp
         endif
         open(22,file=trim(ka_file))
         write(22,*) Nnode
         open(23,file=trim(ks_file))
         write(23,*) Nnode
         do inode=1,Nnode
            if (general_volumic_grid) then
               read(13,*) (xnode(j),j=1,dim)
               phi=xnode(1)     ! longitude (0,360) [deg]
               theta=xnode(2)   ! latitude (-90,90) [deg]
               z=xnode(3)       ! altitude relative to ground level [m]
               altitude=z
c     Identify the vertical layer the node belongs to
               call identify_layer(dim,Nlayer,z_grid,z,iz)
            else
c     retrieve the indexes for the (lon/lat/alt) box from the "inode" node index
               call inode2boxindex(Nlayer,Ntheta,Nphi,inode,iz,itheta,iphi,icell,inodeincell)
               z_mc=(z_grid(iz-1)+z_grid(iz))/2.0D+0-z_grid(0)
               altitude=z_mc
            endif
            if (iaerosol.eq.1) then
c     Aerosol mode 1
               if ((general_volumic_grid.and.((theta.ge.-45.0D+0).and.(theta.le.45.0D+0).and.(((phi.ge.0.0D+0).and.(phi.le.45.0D+0)).or.((phi.ge.315.0D+0).and.(phi.le.360.0D+0))).and.(z.ge.2.0D+4).and.(z.le.8.0D+4))).or.
     &              ((.not.general_volumic_grid).and.((iz.ge.20).and.(iz.le.79)))) then
                  call vertical_profile(0.0D+0,1.0D+0,1.0D+0,1,1,z1,z2,altitude,f)
                  ka=1.0D-6*f ! [m⁻¹]
                  ks=2.0D-6*f ! [m⁻¹]
               else
                  ka=0.0D+0 ! [m⁻¹]
                  ks=0.0D+0 ! [m⁻¹]
               endif
c$$$c     Aerosol mode 1
c$$$               if ((general_volumic_grid.and.((theta.ge.-45.0D+0).and.(theta.le.45.0D+0).and.(((phi.ge.0.0D+0).and.(phi.le.45.0D+0)).or.((phi.ge.315.0D+0).and.(phi.le.360.0D+0))).and.(z.ge.2.0D+4).and.(z.le.8.0D+4))).or.
c$$$     &              ((.not.general_volumic_grid).and.((itheta.ge.Ntheta/4+1).and.(itheta.le.3*Ntheta/4).and.(((iphi.ge.7*Nphi/8+1).and.(iphi.le.Nphi)).or.((iphi.ge.1).and.(iphi.le.Nphi/8))).and.(iz.ge.20).and.(iz.le.79)))) then
c$$$                  call vertical_profile(0.0D+0,1.0D+0,1.0D+0,1,1,z1,z2,altitude,f)
c$$$                  ka=1.0D-6*f ! [m⁻¹]
c$$$                  ks=2.0D-6*f ! [m⁻¹]
c$$$               else
c$$$                  ka=0.0D+0 ! [m⁻¹]
c$$$                  ks=0.0D+0 ! [m⁻¹]
c$$$               endif
c$$$            else if (iaerosol.eq.2) then
c$$$c     Aerosol mode 2
c$$$               if ((general_volumic_grid.and.((theta.ge.0.0D+0).and.(theta.le.90.0D+0).and.(phi.ge.0.0D+0).and.(phi.le.90.0D+0).and.(z.ge.3.0D+4).and.(z.le.4.0D+4))).or.
c$$$     &              ((.not.general_volumic_grid).and.((itheta.ge.Ntheta/2+1).and.(itheta.le.Ntheta).and.(iphi.ge.1).and.(iphi.le.Nphi/4).and.(iz.ge.30).and.(iz.le.39)))) then
c$$$                  call vertical_profile(0.0D+0,1.0D+0,1.0D+0,1,1,z1,z2,altitude,f)
c$$$                  ka=2.0D-6*f ! [m⁻¹]
c$$$                  ks=1.0D-6*f ! [m⁻¹]
c$$$               else
c$$$                  ka=0.0D+0 ! [m⁻¹]
c$$$                  ks=0.0D+0 ! [m⁻¹]
c$$$               endif
c$$$            else if (iaerosol.eq.3) then
c$$$c     Aerosol mode 3
c$$$               if ((general_volumic_grid.and.((theta.ge.-90.0D+0).and.(theta.le.0.0D+0).and.(phi.ge.-90.0D+0).and.(phi.le.0.0D+0).and.(z.ge.5.0D+4).and.(z.le.7.0D+4))).or.
c$$$     &              ((.not.general_volumic_grid).and.((itheta.ge.1).and.(itheta.le.Ntheta/2).and.(iphi.ge.3*Nphi/4+1).and.(iphi.le.Nphi).and.(iz.ge.50).and.(iz.le.69)))) then
c$$$                  call vertical_profile(0.0D+0,1.0D+0,1.0D+0,1,1,z1,z2,altitude,f)
c$$$                  ka=1.0D-7*f ! [m⁻¹]
c$$$                  ks=3.0D-6*f ! [m⁻¹]
c$$$               else
c$$$                  ka=0.0D+0 ! [m⁻¹]
c$$$                  ks=0.0D+0 ! [m⁻¹]
c$$$               endif
            endif
            write(22,*) ka
            write(23,*) ks
         enddo                  ! inode
         close(22)
         close(23)
         call record_aerosol_radiative_properties_file(Nnode,Nband,
     &        lambda_lo,lambda_hi,iband,aerosol_radiative_properties_file)
      enddo                     ! iband
      close(13)
      write(*,*) 'File has been recorded:',trim(aerosol_radiative_properties_file)
      write(*,*) 'Nnode=',Nnode
      write(*,*) 'Nband=',Nband

      return
      end
