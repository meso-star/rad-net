      subroutine inode2boxindex(Nz,Ntheta,Nphi,inode,iz,itheta,iphi,icell,inodeincell)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to identify the (iz,itheta,iphi) (alt/lat/lon) box node index "inode" belongs to
c     
c     Input:
c       + Nz: number of altitude intervals
c       + Ntheta: number of latitude intervals
c       + Nphi: number of longitude intervals
c       + inode: node index
c     
c     Output:
c       + iz: altitude interval
c       + itheta: latitude interval
c       + iphi: longitude interval
c       + icell: index of the cell in the box
c       + inodeincell: index of the node of the cell
c     
c     I/O
      integer Nz,Ntheta,Nphi
      integer*8 inode
      integer iz,itheta,iphi
      integer icell,inodeincell
c     temp
      integer Nnode_sl,Nnode_sb,Ncell_sb
      integer*8 i
      logical keep_running
c     label
      character*(Nchar_mx) label
      label='subroutine inode2boxindex'

      Nnode_sl=6*Nvincell*Nphi*(Ntheta-1)      
      iz=(inode-1)/Nnode_sl+1
c     Debug
      if ((iz.lt.1).or.(iz.gt.Nz)) then
         call error(label)
         write(*,*) 'iz=',iz
         if (iz.lt.1) then
            write(*,*) 'should be >= 1'
         else
            write(*,*) 'should be <= Nz=',Nz
         endif
         stop
      endif
c     Debug
      i=inode-Nnode_sl*(iz-1)

      itheta=1
      i=i-3*Nvincell*Nphi
      if (i.le.0) then
         keep_running=.false.
         i=i+3*Nvincell*Nphi
      else
         keep_running=.true.
         do while (keep_running)
            i=i-6*Nvincell*Nphi
            itheta=itheta+1
            if (i.le.0) then
               keep_running=.false.
               i=i+6*Nvincell*Nphi
            endif
         enddo                  ! while (keep_running)
      endif
c     Debug
      if ((itheta.lt.1).or.(itheta.gt.Ntheta)) then
         call error(label)
         write(*,*) 'itheta=',itheta
         if (itheta.lt.1) then
            write(*,*) 'should be >= 1'
         else
            write(*,*) 'should be <= Ntheta=',Ntheta
         endif
         stop
      endif
c     Debug
      if ((itheta.eq.1).or.(itheta.eq.Ntheta)) then
         Ncell_sb=3
      else
         Ncell_sb=6
      endif
      Nnode_sb=Ncell_sb*Nvincell
      iphi=(i-1)/Nnode_sb+1
c     Debug
      if ((iphi.lt.1).or.(iphi.gt.Nphi)) then
         call error(label)
         write(*,*) 'iphi=',iphi
         if (iphi.lt.1) then
            write(*,*) 'should be >= 1'
         else
            write(*,*) 'should be <= Nphi=',Nphi
         endif
         stop
      endif
c     Debug
      i=i-Nnode_sb*(iphi-1)
      icell=(i-1)/Nvincell+1
c     Debug
      if ((icell.lt.1).or.(icell.gt.Ncell_sb)) then
         call error(label)
         write(*,*) 'icell=',icell
         if (icell.lt.1) then
            write(*,*) 'should be >= 1'
         else
            write(*,*) 'should be <= Ncell_sb=',Ncell_sb
         endif
         stop
      endif
c     Debug
      inodeincell=i-Nvincell*(icell-1)
c     Debug
      if ((inodeincell.lt.1).or.(inodeincell.gt.Nvincell)) then
         call error(label)
         write(*,*) 'inodeincell=',inodeincell
         if (inodeincell.lt.1) then
            write(*,*) 'should be >= 1'
         else
            write(*,*) 'should be <= Nvincell=',Nvincell
         endif
         stop
      endif
c     Debug
      return
      end
