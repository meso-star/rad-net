      subroutine str2num(str,num,errcode)
      implicit none
      include 'max.inc'
c     
c     Purpose: convert a single character into a integer numeric value
c     
c     Input:
c       + str: character (of a integer between 0 and 9)
c     
c     Output:
c       + num: corresponding integer value (between 0 and 9)
c       + errcode: error code (a non-null value means there was a error)
c     
c     I/O
      character*1 str
      integer num
      integer errcode
c     label
      character*(Nchar_mx) label
      label='subroutine str2num'

      errcode=0
      if (str.eq."0") then
         num=0
      else if (str.eq."1") then
         num=1
      else if (str.eq."2") then
         num=2
      else if (str.eq."3") then
         num=3
      else if (str.eq."4") then
         num=4
      else if (str.eq."5") then
         num=5
      else if (str.eq."6") then
         num=6
      else if (str.eq."7") then
         num=7
      else if (str.eq."8") then
         num=8
      else if (str.eq."9") then
         num=9
      else
         errcode=1 ! character is not numeric
      endif

      return
      end
