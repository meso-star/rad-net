c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      
      subroutine triangle_area(dim,p1,p2,p3,area)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the area of a triangle
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c     
c     Output:
c       + area: area of the triangle
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision area
c     temp
      double precision v12(1:Ndim_mx)
      double precision v13(1:Ndim_mx)
      double precision w(1:Ndim_mx)
      double precision length
c     label
      character*(Nchar_mx) label
      label='subroutine triangle_area'

      call substract_vectors(dim,p2,p1,v12)
      call substract_vectors(dim,p3,p1,v13)
      call vector_product(dim,v12,v13,w)
      call vector_length(dim,w,length)
      area=length/2.0D+0

      return
      end



      subroutine tetrahedron_volume(dim,p1,p2,p3,p4,volume)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the volume of a tetrahedron from the
c     coordinates of its 4 nodes.
c     Note: the volume will be positive when (p1, p2, p3) define
c     a triangular base of the tetrahedron, provided a order such
c     that vector n=(p1p2)x(p1p3) is directed in the same direction
c     as vector (p1p4).
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c       + p4: coordinates of the 4th point
c     
c     Output:
c       + area: area of the triangle
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision p4(1:Ndim_mx)
      double precision volume
c     temp
      double precision u12(1:Ndim_mx)
      double precision u13(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision product
c     label
      character*(Nchar_mx) label
      label='subroutine tetrahedron_volume'

      call substract_vectors(dim,p2,p1,u12)
      call substract_vectors(dim,p3,p1,u13)
      call substract_vectors(dim,p4,p1,u14)
      call vector_product(dim,u12,u13,n)
      call vector_vector(dim,n,u14,product)
      volume=product/6.0D+0
      
      return
      end



      subroutine enclosure_volume(dim,Nv,Nf,v,f,volume)
      implicit none
      include 'max.inc'
c     
c     Purpose: to evaluate the volume of a enclosure
c     
c     Input:
c       + Nv,Nf,v,f: defintion of the enclosure as a trianglemesh
c     
c     Output:
c       + volume: enclosed volume
c     
c     I/O
      integer dim
      integer Nv
      integer Nf
      double precision v(1:Nv_mx,1:Ndim_mx)
      integer f(1:Nf_mx,1:3)
      double precision volume
c     temp
      integer vidx,fidx,j,i
      double precision minmax(1:Ndim_mx,1:2)
      double precision p0(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision dV
c     label
      character*(Nchar_mx) label
      label='subroutine enclosure_volume'

c     get the min and max of the enclose along each direction
      do j=1,dim
         minmax(j,2)=v(1,j)
      enddo                     ! j
      do vidx=1,Nv
         do j=1,dim
            if (v(vidx,j).gt.minmax(j,2)) then
               minmax(j,2)=v(vidx,j)
            endif
         enddo                  ! j
      enddo                     ! vidx
      do j=1,dim
         minmax(j,1)=minmax(j,2)
      enddo                     ! j
      do vidx=1,Nv
         do j=1,dim
            if (v(vidx,j).lt.minmax(j,1)) then
               minmax(j,1)=v(vidx,j)
            endif
         enddo                  ! j
      enddo                     ! vidx
      
c     guess a central position
      do j=1,dim
         p0(j)=(minmax(j,1)+minmax(j,2))/2.0D+0
      enddo                     ! j

c     compute the volume
      volume=0.0D+0
      do fidx=1,Nf
         do j=1,dim
            p1(j)=v(f(fidx,1),j)
            p2(j)=v(f(fidx,2),j)
            p3(j)=v(f(fidx,3),j)
         enddo                  ! j
         call tetrahedron_volume(dim,p1,p2,p3,p0,dV)
         volume=volume+dV
      enddo                     ! fidx
            
      return
      end

      
      
      subroutine total_length(N,a,e_in,e_ext,length)
      implicit none
      include 'max.inc'
c
c     Purpose: to get the total legnth of a given building along a given axis
c
c     Input:
c       + N: number of rooms along the chosen axis
c       + a: length of a room along the chosen axis
c       + e_in: thickness in internal walls
c       + e_ext: thickness of external walls
c
c     Output:
c       + length: total length of the building along the selected axis
c
c     I/O
      integer N
      double precision a
      double precision e_in
      double precision e_ext
      double precision length
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine total_length'

      length=a*N+e_in*(N-1)+2*e_ext

      return
      end


      
      subroutine distance_to_vector(dim,x1,x2,P,dmin,A,
     &     A_is_at_end,end_side)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the distance between a given vector and a position
c     
c     Input:
c       + dim: dimension of space
c       + x1: first position that defines the vector
c       + x2: second position that defines the vector
c       + P: position
c     
c     Output:
c       + dmin: smallest distance between P and vector
c       + A: closest position to P that belongs to the vector
c       + A_is_at_end: true if A is at one end of the [x1,x2] segment
c       + end_side: index (1 or 2) or the end side if A_is_at_end=T
c     
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision P(1:Ndim_mx)
      double precision dmin
      double precision A(1:Ndim_mx)
      logical A_is_at_end
      integer end_side
c     temp
      integer j
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision nu(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision AP(1:Ndim_mx)
      double precision norm_u,norm_v,L1,d,sp
c     label
      character*(Nchar_mx) label
      label='subroutine distance_to_vector'

      call substract_vectors(dim,x2,x1,u)
      call normalize_vector(dim,u,nu)
      call substract_vectors(dim,P,x1,v)
      call vector_length(dim,u,norm_u)
      call vector_length(dim,v,norm_v)
      call vector_vector(dim,u,v,sp)
      if (norm_u.le.0.0D+0) then
         call error(label)
         write(*,*) 'norm_u=',norm_u
         write(*,*) 'u=',u
         stop
      else
         L1=sp/norm_u
      endif
      if (L1.le.0.0D+0) then
         call copy_vector(dim,x1,A)
         do j=1,dim
            tmp(j)=0.0D+0
         enddo                  ! j
         A_is_at_end=.true.
         end_side=1
      else if (L1.ge.norm_u) then
         call copy_vector(dim,x2,A)
         call copy_vector(dim,u,tmp)
         A_is_at_end=.true.
         end_side=2
      else
         call scalar_vector(dim,L1,nu,tmp)
         call add_vectors(dim,x1,tmp,A)
         A_is_at_end=.false.
      endif
      call substract_vectors(dim,v,tmp,AP)
      call vector_length(dim,AP,dmin)

      return
      end
