      subroutine gas_properties(dim,general_volumic_grid,gas_radiative_properties_file,gas_thermodynamic_properties_file,
     &     Nlayer,Ntheta,Nphi,z_grid,Nnode,
     &     Nband,Nq,lambda_min,lambda_max,w,
     &     Tref,ka_ref,ks_ref,lambda1,lambda2,z1,z2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to attach gas properties to cells and record properties files
c     
c     Input:
c       + dim: dimension of space
c       + general_volumic_grid: T: each node is shared by several cells; F: each cell has its own nodes, not shared by any other cell
c       + gas_radiative_properties_file: file for radiative properties recording
c       + gas_thermodynamic_properties_file: file for thermodynamic properties recording
c       + Nlayer: number of altitude intervals
c       + Ntheta: number of latitude intervals
c       + Nphi: number of longitude intervals
c       + z_grid: radius at the top of each layer (relative to the center of the planet) [m]
c       + Nnode: number of nodes
c       + Nband: number of narrowbands
c       + Nq: quadrature order for each band
c       + lambda_min: lower wavelength for each band [µm]
c       + lambda_max: higher wavelength for each band [µm]
c       + w: quadrature weights
c       + Tref, ka_ref, ks_ref, lambda1, lambda2, z1, z2: parameters for retrieving temperature and radiative coefficients
c     
c     Output: gas radiative and thermodynamic files
c     
c     I/O
      integer dim
      logical general_volumic_grid
      character*(Nchar_mx) gas_radiative_properties_file
      character*(Nchar_mx) gas_thermodynamic_properties_file
      integer Nlayer
      integer Ntheta
      integer Nphi
      double precision z_grid(0:Nlayer_mx)
      integer*8 Nnode
      integer Nband
      integer Nq(1:Nband_mx)
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      double precision Tref
      double precision ka_ref
      double precision ks_ref
      double precision lambda1,lambda2
      double precision z1,z2
c     temp
      integer*8 inode
      integer iband,iq,i,j,Nq_band
      integer*8 Nnode_tmp
      double precision z,theta,phi
      double precision ka,ks,T
      double precision w_band(1:Nquad_mx)
      double precision lambda_lo(1:Nband_mx)
      double precision lambda_hi(1:Nband_mx)
      double precision lambda,f
      integer ios
      character*(Nchar_mx) xnode_file
      double precision xnode(1:Ndim_mx)
      character*(Nchar_mx) Tfile,ka_file,ks_file
      integer iz,itheta,iphi,icell,inodeincell
      double precision z_mc,altitude
c     label
      character*(Nchar_mx) label
      label='subroutine gas_properties'

      xnode_file='./xnode.txt'
      open(13,file=trim(xnode_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(xnode_file)
         stop
      endif
      read(13,*) Nnode_tmp
      if (Nnode_tmp.ne.Nnode) then
         call error(label)
         write(*,*) 'input Nnode=',Nnode
         write(*,*) 'written in file:',Nnode_tmp
         stop
      endif
      write(*,*) 'Producing gas properties...'

      Tfile='./T.txt'
      ka_file='./ka_gas.txt'
      ks_file='./ks_gas.txt'
      open(21,file=trim(Tfile))
      write(21,*) Nnode
      
      do iband=1,Nband
         lambda_lo(iband)=lambda_min(iband)*1.0D+3 ! µm -> nm
         lambda_hi(iband)=lambda_max(iband)*1.0D+3 ! µm -> nm
      enddo
      do iband=1,Nband
         write(*,*) 'Spectral interval ',iband,' /',Nband
         open(22,file=trim(ka_file))
         write(22,*) Nnode
         write(22,*) Nq(iband)
         open(23,file=trim(ks_file))
         write(23,*) Nnode
         
         lambda=(lambda_min(iband)+lambda_max(iband))/2.0D+0 ! [µm]
         Nq_band=Nq(iband)
         do iq=1,Nq(iband)
            w_band(iq)=w(iband,iq)
            rewind(13)
            read(13,*) Nnode_tmp
            do inode=1,Nnode
               if (general_volumic_grid) then
c     Interpolate gas properties for the node, band quand quadrature index
                  read(13,*) (xnode(j),j=1,dim)
                  phi=xnode(1)  ! longitude (0,360) [deg]
                  theta=xnode(2) ! latitude (-90,90) [deg]
                  z=xnode(3)    ! altitude relative to ground level [m]
                  altitude=z
               else             ! general_volumic_grid=F
c     Attribute gas properties as a function of the (lon/lat/alt) box
                  call inode2boxindex(Nlayer,Ntheta,Nphi,inode,iz,itheta,iphi,icell,inodeincell)
                  z_mc=(z_grid(iz-1)+z_grid(iz))/2.0D+0-z_grid(0)
                  altitude=z_mc
               endif            ! general_volumic_grid
c     Set temperature of node
               if ((iband.eq.1).and.(iq.eq.1)) then
                  call vertical_profile(0.0D+0,1.0D+0,1.0D+0,1,1,z1,z2,altitude,f)
                  if (f.lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'f=',f
                     write(*,*) 'z1=',z1
                     write(*,*) 'z2=',z2
                     write(*,*) 'z=',z
                     stop
                  else
                     T=Tref*f
                     write(21,*) T
                  endif
               endif
               if (iq.eq.1) then
c     Set scattering coefficient of the node
                  call vertical_profile(lambda1,lambda2,lambda,Nq(iband),1,z1,z2,altitude,f)
                  if (f.lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'f=',f
                     write(*,*) 'lambda1=',lambda1
                     write(*,*) 'lambda2=',lambda2
                     write(*,*) 'lambda=',lambda
                     write(*,*) 'Nq=',Nq(iband)
                     write(*,*) 'iq=1'
                     write(*,*) 'z1=',z1
                     write(*,*) 'z2=',z2
                     write(*,*) 'z=',z
                     stop
                  else
                     ks=ks_ref*f
                     write(23,*) ks
                  endif
               endif
c     Set absorption coefficient of the node
               call vertical_profile(lambda1,lambda2,lambda,Nq(iband),iq,z1,z2,altitude,f)
               if (f.lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'f=',f
                  write(*,*) 'lambda1=',lambda1
                  write(*,*) 'lambda2=',lambda2
                  write(*,*) 'lambda=',lambda
                  write(*,*) 'Nq=',Nq(iband)
                  write(*,*) 'iq=',iq
                  write(*,*) 'z1=',z1
                  write(*,*) 'z2=',z2
                  write(*,*) 'z=',z
                  stop
               else
                  ka=ka_ref*f
                  write(22,*) ka
               endif
            enddo               ! inode
         enddo                  ! iq
         close(22)
         close(23)
         call record_gas_radiative_properties_file(Nnode,Nband,Nq_band,
     &        lambda_lo,lambda_hi,w_band,iband,gas_radiative_properties_file)
      enddo                     ! iband
      close(13)
      close(21)
c     
      call record_gas_thermodynamic_properties_file(Nnode,gas_thermodynamic_properties_file)
      write(*,*) 'Files have been recorded:'
      write(*,*) trim(gas_radiative_properties_file)
      write(*,*) 'Nnode=',Nnode
      write(*,*) 'Nband=',Nband
      write(*,*) 'Nq=',Nq_band
      write(*,*) trim(gas_thermodynamic_properties_file)
      write(*,*) 'Nnode=',Nnode
               
      return
      end



      subroutine identify_layer(dim,Nlayer,z_grid,z,iz)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify which layer a given altitude belongs to
c     
c     Input:
c       + dim: dimension of space
c       + Nlayer: number of vertical layers
c       + z_grid: radius at the top of each layer (relative to the center of the planet) [m]
c       + z: altitude (relative to the ground) [m]
c     
c     Output:
c       + iz: index of the layer "z" belongs to
c     
c     I/O
      integer dim
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision z
      integer iz
c     temp
      integer i
      logical iz_found
c     label
      character*(Nchar_mx) label
      label='subroutine identify_layer'

      iz_found=.false.
      do i=1,Nlayer
         if ((z.ge.z_grid(i-1)-z_grid(0)).and.(z.le.z_grid(i)-z_grid(0))) then
            iz_found=.true.
            iz=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.iz_found) then
         call error(label)
         write(*,*) 'layer could not be identified for z=',z
         write(*,*) 'layer index / min altitude / max altitude'
         do i=1,Nlayer
            write(*,*) i,z_grid(i-1)-z_grid(0),z_grid(i)-z_grid(0)
         enddo                  ! i
         stop
      endif

      return
      end
