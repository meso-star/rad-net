      program test_inode2boxindex
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c
c     Purpose: to test the "inode2boxindex" subroutine
c
c     Variables
      integer Nz,Ntheta,Nphi
      integer*8 inode
      integer iz0,itheta0,iphi0
      integer icell0,inodeincell0
      integer iz,itheta,iphi
      integer icell,inodeincell
      integer i,Nnode_sl,Nnode_sb
c     label
      character*(Nchar_mx) label
      label='program test_inode2boxindex'

      Nz=100
      Ntheta=36
      Nphi=2*Ntheta
      Nnode_sl=6*Nvincell*Nphi*(Ntheta-1)

c     Set the index of the box
      iz0=50
      itheta0=36
      iphi0=72
c     Set the index of the cell in the box
      icell0=2
c     Set the index of the node in the cell
      inodeincell0=2
c     Compute absolute node index
      inode=Nnode_sl*(iz0-1)
      if (itheta0.gt.1) then
         inode=inode+3*Nvincell*Nphi
         if (itheta0.gt.2) then
            do i=2,itheta0-1
               inode=inode+6*Nvincell*Nphi
            enddo               ! i
         endif
      endif
      if ((itheta0.eq.1).or.(itheta0.eq.Ntheta)) then
         Nnode_sb=3*Nvincell
      else
         Nnode_sb=6*Nvincell
      endif
      inode=inode+Nnode_sb*(iphi0-1)
      inode=inode+Nvincell*(icell0-1)
      inode=inode+inodeincell0
c      write(*,*) 'inode=',inode
c     Test "inode2boxinde"
      call inode2boxindex(Nz,Ntheta,Nphi,inode,iz,itheta,iphi,icell,inodeincell)
      if (iz.ne.iz0) then
         write(*,*) 'Altitude interval incorrect:'
         write(*,*) 'iz=',iz
         write(*,*) 'expected:',iz0
         stop
      endif
      if (itheta.ne.itheta0) then
         write(*,*) 'latitude interval incorrect:'
         write(*,*) 'itheta=',itheta
         write(*,*) 'expected:',itheta0
         stop
      endif
      if (iphi.ne.iphi0) then
         write(*,*) 'longitude interval incorrect:'
         write(*,*) 'iphi=',iphi
         write(*,*) 'expected:',iphi0
         stop
      endif
      if (icell.ne.icell0) then
         write(*,*) 'cell index incorrect:'
         write(*,*) 'icell=',icell
         write(*,*) 'expected:',icell0
         stop
      endif
      if (inodeincell.ne.inodeincell0) then
         write(*,*) 'node in cell index incorrect:'
         write(*,*) 'inodeincell=',inodeincell
         write(*,*) 'expected:',inodeincell0
         stop
      endif
      write(*,*) 'ok'
      
      end
