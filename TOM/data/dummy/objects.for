c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine add_obj_to_obj(dim,Nv01,Nf01,v01,f01,
     &     Nnode,Nface,vertices,faces)
      implicit none
      include 'max.inc'
c     
c     Purpose: to add a obj to a obj
c     
c     Input:
c       + dim: dimension of space
c       + Nv01, Nf01, v01, f01: object to add
c       + Nnode, Nface, vertices, faces: object to be completed
c     
c     Output:
c       + Nnode, Nface, vertices, faces: updated
c     
c     I/O
      integer dim
      integer Nv01,Nf01
      double precision v01(1:Nv_mx,1:Ndim_mx)
      integer f01(1:Nf_mx,1:3)
      integer Nnode,Nface
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
c     temp
      integer iv,iface,i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_obj_to_obj'
      
      if (Nnode+Nv01.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nnode=',Nnode
         write(*,*) '+Nv01=',Nv01
         write(*,*) '=',Nnode+Nv01
         write(*,*) '> Nv_mx=',Nv_mx
         stop
      else
         do iv=1,Nv01
            do j=1,dim
               vertices(Nnode+iv,j)=v01(iv,j)
            enddo               ! j
         enddo                  ! iv
      endif
      if (Nface+Nf01.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nface=',Nface
         write(*,*) '+Nf01=',Nf01
         write(*,*) '=',Nface+Nf01
         write(*,*) '> Nf_mx=',Nf_mx
         stop
      else
         do iface=1,Nf01
            do j=1,3
               faces(Nface+iface,j)=Nnode+f01(iface,j)
            enddo               ! j
         enddo                  ! iface
      endif
      Nnode=Nnode+Nv01
      Nface=Nface+Nf01

      return
      end


      
      subroutine sphere_obj(dim,Ntheta,Nphi,add_vplane,add_hplane,
     &     Nv,Nf,v,f)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to produce a sphere (of unit radius) as a wavefront object
c
c     Input:
c       + dim: dimension of space
c       + Ntheta: number of latitude intervals (from pole to pole)
c       + Nphi: number of longitude intervals (from 0 to 2*pi)
c       + add_vplane: true if the reference longitude plane must be represented
c       + add_hplane: true if the equator plane must be represented
c
c     Output:
c       + Nv: number of vertices
c       + Nf: number of faces
c       + v: vertices
c       + f: faces
c
c     I/O
      integer dim
      integer Ntheta,Nphi
      logical add_vplane,add_hplane
      integer Nv,Nf
      double precision v(1:Nv_mx,1:Ndim_mx)
      integer f(1:Nf_mx,1:3)
c     temp
      double precision ascale,bscale
      double precision a,b
      double precision r,theta,phi
      double precision dtheta,dphi
      integer itheta,iphi,iv,if,i
      integer i1,i2,i3,i4
c     label
      character*(Nchar_mx) label
      label='subroutine sphere_obj'

      Nv=0
      Nf=0
      r=1.0D+0
      dtheta=pi/Ntheta
      dphi=2.0D+0*pi/Nphi
      ascale=1.2D+0
      bscale=1.5D+0

c     first point: south pole
      Nv=Nv+1
      v(Nv,1)=0.0D+0
      v(Nv,2)=0.0D+0
      v(Nv,3)=-r
c     then for each latitude interval
      do itheta=1,Ntheta-1
         theta=-pi/2.0D+0+itheta*dtheta
         do iphi=1,Nphi
            phi=iphi*dphi
c     vertices
            Nv=Nv+1
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
c     faces
            if (itheta.eq.1) then
               i1=1
               if (iphi.eq.1) then
                  i2=Nphi+1
                  i3=2
               else
                  i2=iphi
                  i3=iphi+1
               endif            ! iphi=1
               Nf=Nf+1
               f(Nf,1)=i1
               f(Nf,2)=i3
               f(Nf,3)=i2
            else
               if (iphi.eq.1) then
                  i1=(itheta-2)*Nphi+Nphi+1
                  i2=(itheta-2)*Nphi+iphi+1
                  i3=(itheta-1)*Nphi+Nphi+1
                  i4=(itheta-1)*Nphi+iphi+1
               else
                  i1=(itheta-2)*Nphi+iphi
                  i2=(itheta-2)*Nphi+iphi+1
                  i3=(itheta-1)*Nphi+iphi
                  i4=(itheta-1)*Nphi+iphi+1
               endif ! iphi=1
               Nf=Nf+1
               f(Nf,1)=i1
               f(Nf,2)=i2
               f(Nf,3)=i4
               Nf=Nf+1
               f(Nf,1)=i1
               f(Nf,2)=i4
               f(Nf,3)=i3
            endif ! itheta=1
         enddo ! iphi
      enddo ! itheta
c     last point: north pole
      Nv=Nv+1
      v(Nv,1)=0.0D+0
      v(Nv,2)=0.0D+0
      v(Nv,3)=r
c     faces
      do iphi=1,Nphi
         Nf=Nf+1
         if (iphi.eq.1) then
            i1=(Ntheta-1)*Nphi+1
            i2=(Ntheta-2)*Nphi+2
         else
            i1=(Ntheta-2)*Nphi+iphi
            i2=(Ntheta-2)*Nphi+iphi+1
         endif ! iphi=1
         i3=(Ntheta-1)*Nphi+2
         f(Nf,1)=i1
         f(Nf,2)=i2
         f(Nf,3)=i3
      enddo ! iphi

      a=r*ascale
      b=r*bscale

      if (add_vplane) then
         Nv=Nv+1
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=-a
         i1=Nv
         Nv=Nv+1
         v(Nv,1)=b
         v(Nv,2)=0.0D+0
         v(Nv,3)=-a
         i2=Nv
         Nv=Nv+1
         v(Nv,1)=b
         v(Nv,2)=0.0D+0
         v(Nv,3)=a
         i3=Nv
         Nv=Nv+1
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=a
         i4=Nv
         Nf=Nf+1
         f(Nf,1)=i1
         f(Nf,2)=i2
         f(Nf,3)=i3
         Nf=Nf+1
         f(Nf,1)=i1
         f(Nf,2)=i3
         f(Nf,3)=i4
         Nf=Nf+1
         f(Nf,1)=i1
         f(Nf,2)=i3
         f(Nf,3)=i2
         Nf=Nf+1
         f(Nf,1)=i1
         f(Nf,2)=i4
         f(Nf,3)=i3
      endif ! add_vplane

      if (add_hplane) then
         Nv=Nv+1
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=0.0D+0
         i1=Nv
         do iphi=1,Nphi
            phi=iphi*dphi
            Nv=Nv+1
            v(Nv,1)=b*dcos(phi)
            v(Nv,2)=b*dsin(phi)
            v(Nv,3)=0.0D+0
            if (iphi.eq.1) then
               i2=i1+Nphi
               i3=i1+1
            else
               i2=i1+iphi-1
               i3=i1+iphi
            endif
            Nf=Nf+1
            f(Nf,1)=i1
            f(Nf,2)=i2
            f(Nf,3)=i3
            Nf=Nf+1
            f(Nf,1)=i1
            f(Nf,2)=i3
            f(Nf,3)=i2
         enddo ! iphi
      endif ! add_hplane

      if (Nv.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv=',Nv
         write(*,*) '> Nv_mx=',Nv_mx
         stop
      endif
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf=',Nf
         write(*,*) '> Nf_mx=',Nf_mx
         stop
      endif
c     Debug
c      write(*,*) 'Nv=',Nv
c      do iv=1,Nv
c         write(*,*) 'iv=',iv,(v(iv,i),i=1,dim)
c      enddo ! iv
c      write(*,*) 'Nf=',Nf
c      do if=1,Nf
c         write(*,*) 'if=',if,(f(if,i),i=1,3)
c      enddo ! if
c     Debug

      return
      end



      subroutine partial_sphere_obj(dim,r,
     &     theta_min,theta_max,
     &     phi_min,phi_max,
     &     Ntheta,Nphi,
     &     Nv,Nf,v,f)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to produce a partial sphere as a wavefront object
c
c     Input:
c       + dim: dimension of space
c       + r: radius of the sphere
c       + theta_min: lower value for theta
c       + theta_max: higher value for theta
c       + phi_min: lower value for phi
c       + phi_max: higher value for phi
c       + Ntheta: number of latitude intervals (from theta_min to theta_max)
c       + Nphi: number of longitude intervals (from 0 to 2*pi)
c
c     Output:
c       + Nv: number of vertices
c       + Nf: number of faces
c       + v: vertices
c       + f: faces
c
c     I/O
      integer dim
      double precision r
      double precision theta_min,theta_max
      double precision phi_min,phi_max
      integer Ntheta,Nphi
      integer Nv,Nf
      double precision v(1:Nv_mx,1:Ndim_mx)
      integer f(1:Nf_mx,1:3)
c     temp
      double precision ascale,bscale
      double precision a,b
      double precision theta,phi
      double precision dtheta,dphi
      integer itheta,iphi,iv,iface,i
      integer i1,i2,i3,i4
      logical south_pole,north_pole
c     label
      character*(Nchar_mx) label
      label='subroutine partial_sphere_obj'
c
      if (theta_min.lt.-pi/2.0D+0) then
         call error(label)
         write(*,*) 'theta_min=',theta_min
         write(*,*) '< -pi/2'
         stop
      endif
      if (theta_max.gt.pi/2.0D+0) then
         call error(label)
         write(*,*) 'theta_max=',theta_max
         write(*,*) '> pi/2'
         stop
      endif
      if (theta_max.lt.theta_min) then
         call error(label)
         write(*,*) 'theta_max=',theta_max
         write(*,*) '< theta_min=',theta_min
         stop
      endif
c     
c      if (phi_min.lt.0.0D+0) then
c         call error(label)
c         write(*,*) 'phi_min=',phi_min
c         write(*,*) '< 0'
c         stop
c      endif
c      if (phi_max.gt.2.0D+0*pi) then
c         call error(label)
c         write(*,*) 'phi_max=',phi_max
c         write(*,*) '> 2pi'
c         stop
c      endif
      if (phi_max.lt.phi_min) then
         call error(label)
         write(*,*) 'phi_max=',phi_max
         write(*,*) '< phi_min=',phi_min
         stop
      endif
c
      if (dabs(theta_min+pi/2.0D+0).lt.1.0D-6) then
         south_pole=.true.
      else
         south_pole=.false.
      endif
      if (dabs(theta_max-pi/2.0D+0).lt.1.0D-6) then
         north_pole=.true.
      else
         north_pole=.false.
      endif
c     Debug
c      write(*,*) 'south_pole=',south_pole
c      write(*,*) 'north_pole=',north_pole
c     Debug

c     Angular discretization
      dtheta=(theta_max-theta_min)/dble(Ntheta)
      dphi=(phi_max-phi_min)/dble(Nphi)
      Nv=0
      Nf=0
c     special cases
      if ((south_pole).and.(.not.north_pole).and.(Ntheta.eq.1)) then
         Nv=Nv+1
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=-r
         theta=theta_max
         do iphi=0,Nphi
            phi=phi_min+iphi*dphi
c     vertices
            Nv=Nv+1
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
         enddo                  ! iphi
         i1=1
         do iphi=1,Nphi
            i2=iphi+1
            i3=iphi+2
            Nf=Nf+1
            f(Nf,1)=i1
            f(Nf,2)=i3
            f(Nf,3)=i2
         enddo                  ! iphi
         goto 666
      endif
      if ((north_pole).and.(.not.south_pole).and.(Ntheta.eq.1)) then
         Nv=Nv+1
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=r
         theta=theta_min
         do iphi=0,Nphi
            phi=phi_min+iphi*dphi
c     vertices
            Nv=Nv+1
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
         enddo                  ! iphi
         i1=1
         do iphi=1,Nphi
            i2=iphi+1
            i3=iphi+2
            Nf=Nf+1
            f(Nf,1)=i1
            f(Nf,2)=i2
            f(Nf,3)=i3
         enddo                  ! iphi
         goto 666
      endif
c     ------------------------------------------------------------
c     General case
c     Vertices      
      if (south_pole) then
c     first point: south pole
         Nv=Nv+1
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=-r
      else ! no south pole
c     first set of "Nphi" points
         theta=theta_min
         do iphi=0,Nphi
            phi=phi_min+iphi*dphi
            Nv=Nv+1
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
         enddo ! iphi
      endif ! south_pole or not
      do itheta=1,Ntheta-1
         theta=theta_min+itheta*dtheta
         do iphi=0,Nphi
            phi=phi_min+iphi*dphi
            Nv=Nv+1
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
         enddo ! iphi
      enddo ! itheta
c     last point: north pole
      if (north_pole) then
         Nv=Nv+1
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=r
      else ! no north pole
c     first set of "Nphi" points
         theta=theta_max
         do iphi=0,Nphi
            phi=phi_min+iphi*dphi
c     vertices
            Nv=Nv+1
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
         enddo ! iphi
      endif ! north_pole or not
      if (Nv.gt.Nv_mx) then
         call error(label)
         write(*,*) 'Nv=',Nv
         write(*,*) '> Nv_mx=',Nv_mx
         stop
      endif

c     Faces
      do itheta=1,Ntheta-1
         theta=theta_min+itheta*dtheta
         do iphi=1,Nphi
            if (itheta.eq.1) then
               if (south_pole) then
                  i1=1
                  i2=iphi+1
                  i3=iphi+2
                  Nf=Nf+1
                  f(Nf,1)=i1
                  f(Nf,2)=i3
                  f(Nf,3)=i2
               else             ! no south pole                  
                  i1=iphi
                  i2=iphi+1
                  i3=Nphi+iphi+1
                  i4=Nphi+iphi+2
                  Nf=Nf+1
                  f(Nf,1)=i1
                  f(Nf,2)=i2
                  f(Nf,3)=i4
                  Nf=Nf+1
                  f(Nf,1)=i1
                  f(Nf,2)=i4
                  f(Nf,3)=i3
               endif ! south_pole or not
            else ! itheta>1
               if (south_pole) then
                  i1=(itheta-2)*(Nphi+1)+iphi+1
                  i2=(itheta-2)*(Nphi+1)+iphi+2
                  i3=(itheta-1)*(Nphi+1)+iphi+1
                  i4=(itheta-1)*(Nphi+1)+iphi+2
               else ! no south pole
                  i1=(itheta-1)*(Nphi+1)+iphi
                  i2=(itheta-1)*(Nphi+1)+iphi+1
                  i3=(itheta)*(Nphi+1)+iphi
                  i4=(itheta)*(Nphi+1)+iphi+1
               endif            ! south_pole or not
               Nf=Nf+1
               f(Nf,1)=i1
               f(Nf,2)=i2
               f(Nf,3)=i4
               Nf=Nf+1
               f(Nf,1)=i1
               f(Nf,2)=i4
               f(Nf,3)=i3
            endif               ! itheta=1
         enddo ! iphi
      enddo ! itheta
      do iphi=1,Nphi
         if (north_pole) then
            if (south_pole) then
               i1=(Ntheta-2)*(Nphi+1)+iphi+1
               i2=(Ntheta-2)*(Nphi+1)+iphi+2
               i3=(Ntheta-1)*(Nphi+1)+2
            else ! no south pole
               i1=(Ntheta-1)*(Nphi+1)+iphi
               i2=(Ntheta-1)*(Nphi+1)+iphi+1
               i3=Ntheta*(Nphi+1)+1
            endif ! south pole or not
            Nf=Nf+1
            f(Nf,1)=i1
            f(Nf,2)=i2
            f(Nf,3)=i3
         else ! no north pole
            if (south_pole) then
               i1=(Ntheta-2)*(Nphi+1)+iphi+1
               i2=i1+1
               i3=(Ntheta-1)*(Nphi+1)+iphi+1
               i4=i3+1
            else                ! no south pole
               i1=(Ntheta-1)*(Nphi+1)+iphi
               i2=(Ntheta-1)*(Nphi+1)+iphi+1
               i3=Ntheta*(Nphi+1)+iphi
               i4=Ntheta*(Nphi+1)+iphi+1
            endif               ! south pole or not
            Nf=Nf+1
            f(Nf,1)=i1
            f(Nf,2)=i2
            f(Nf,3)=i4
            Nf=Nf+1
            f(Nf,1)=i1
            f(Nf,2)=i4
            f(Nf,3)=i3
         endif ! north pole or not
      enddo                     ! iphi
c     
 666  continue
      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf=',Nf
         write(*,*) '> Nf_mx=',Nf_mx
         stop
      endif

      return
      end

      

      subroutine impact_crater_obj(dim,
     &     planet_radius,crater_radius,crater_height,
     &     theta_min,theta_max,
     &     phi_min,phi_max,
     &     Ntheta,Nphi,Nalpha,Nr,
     &     Nv01,Nf01,v01,f01,
     &     Nv02,Nf02,v02,f02)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to produce a partial sphere with a impact crater
c      
c
c                 y
c                 ^
c                 |                                      |
c      theta_max --------------------------------------------
c                 |                                      |
c                 |               __                     |
c                 |             /   \                    |
c                 |            /     \                   |
c                 |           |       |                  |
c                 |           \      /                   |
c                 |            \    /                    |
c                 |              __                      |
c                 |                                      |
c                 |                                      |
c      theta_min --------------------------------------------> x
c                 |                                      |
c      
c                phi_min                               phi_max
c      
c      
c
c     Note: this routine should not be used for a sphere portion
c     that goes all the way to either pole: theta_min has to be
c     different than -90°, and theta_max has to be difference than 90°
c
c     Input:
c       + dim: dimension of space
c       + planet_radius: radius of the sphere [m]
c       + crater_radius: radius of the crater [m]
c       + crater_height: height of the crater [m]
c       + theta_min: lower value for theta [rad]
c       + theta_max: higher value for theta [rad]
c       + phi_min: lower value for phi [rad]
c       + phi_max: higher value for phi [rad]
c       + Ntheta: number of angular sectors for discretizing the sphere portion
c	+ Nphi: number of longitude intervals for the sphere portion
c       + Nalpha: number of angular sectors for discretizing the crater
c       + Nr: number of radius sectors for discretizing the crater
c
c     Output:
c       + Nv01, Nf01, v01, f01: trianglemesh of the crater
c       + Nv02, Nf02, v02, f02: trianglemesh of the rim
c
c     I/O
      integer dim
      double precision planet_radius
      double precision crater_radius
      double precision crater_height
      double precision theta_min,theta_max
      double precision phi_min,phi_max
      integer Ntheta,Nphi,Nalpha,Nr
      integer Nv01,Nf01
      double precision v01(1:Nv_mx,1:Ndim_mx)
      integer f01(1:Nf_mx,1:3)
      integer Nv02,Nf02
      double precision v02(1:Nv_mx,1:Ndim_mx)
      integer f02(1:Nf_mx,1:3)
c     temp
      integer i,j
      integer Np_ext
      double precision ext_ctr(1:Nppt_mx,1:Ndim_mx)
      double precision deltax,deltay
      double precision center(1:Ndim_mx)
      double precision vertex(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine impact_crater_obj'
c
      if (theta_min.le.-pi/2.0D+0) then
         call error(label)
         write(*,*) 'theta_min=',theta_min
         write(*,*) '< -pi/2'
         stop
      endif
      if (theta_max.ge.pi/2.0D+0) then
         call error(label)
         write(*,*) 'theta_max=',theta_max
         write(*,*) '> pi/2'
         stop
      endif
      if (theta_max.lt.theta_min) then
         call error(label)
         write(*,*) 'theta_max=',theta_max
         write(*,*) '< theta_min=',theta_min
         stop
      endif
      if (phi_max.lt.phi_min) then
         call error(label)
         write(*,*) 'phi_max=',phi_max
         write(*,*) '< phi_min=',phi_min
         stop
      endif
c

      deltax=planet_radius*(phi_max-phi_min)*dcos(theta_min)
      deltay=planet_radius*(theta_max-theta_min)
      center(1)=deltax/2.0D+0
      center(2)=deltay/2.0D+0
      center(3)=0.0D+0

      if (2.0D+0*crater_radius.gt.deltax) then
         call error(label)
         write(*,*) 'crater_radius=',crater_radius
         write(*,*) 'is incompatible with deltax=',deltax
         stop
      endif
      if (2.0D+0*crater_radius.gt.deltay) then
         call error(label)
         write(*,*) 'crater_radius=',crater_radius
         write(*,*) 'is incompatible with deltay=',deltay
         stop
      endif

c     ------------------------------------------------------------------
c     Trianglemesh of the crater
c     ------------------------------------------------------------------
      call crater(dim,
     &     crater_radius,crater_height,Nalpha,Nr,
     &     Nv01,Nf01,v01,f01,Np_ext,ext_ctr)
      call move_obj(dim,Nv01,v01,center)
      do i=1,Np_ext
         do j=1,dim
            ext_ctr(i,j)=ext_ctr(i,j)+center(j)
         enddo                  ! j
      enddo                     ! i

c     ------------------------------------------------------------------
c     Trianglemesh of the rim
c     ------------------------------------------------------------------
c      call rim_test(dim,
c     &     Np_ext,ext_ctr,deltax,deltay,
c     &     center,Ntheta,Nphi,
c     &     Nv02,Nf02,v02,f02)
      call rim_triangle(dim,
     &     Np_ext,ext_ctr,deltax,deltay,
     &     center,Ntheta,Nphi,
     &     Nv02,Nf02,v02,f02)
      
c     ------------------------------------------------------------------
c     Mapping on the sphere
c     ------------------------------------------------------------------
      do i=1,Nv01
         do j=1,dim
            vertex(j)=v01(i,j)
         enddo                  ! j
         call mapv_cart2spher(dim,
     &        planet_radius,
     &        theta_min,theta_max,
     &        phi_min,phi_max,
     &        deltax,deltay,
     &        vertex)
         do j=1,dim
            v01(i,j)=vertex(j)
         enddo                  ! j
      enddo                     ! i
      do i=1,Nv02
         do j=1,dim
            vertex(j)=v02(i,j)
         enddo                  ! j
         call mapv_cart2spher(dim,
     &        planet_radius,
     &        theta_min,theta_max,
     &        phi_min,phi_max,
     &        deltax,deltay,
     &        vertex)
         do j=1,dim
            v02(i,j)=vertex(j)
         enddo                  ! j
      enddo                     ! i
      
      return
      end


      
      subroutine rim_triangle(dim,
     &     Np_ext,ext_ctr,deltax,deltay,
     &     center,Ntheta,Nphi,
     &     Nv,Nf,v,f)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce the trianglemesh of the rim of a crater
c     This version uses a Delaunay triangulation of the rim, defined
c     by two contours. A special option is used 
c     
c     
c     
c     Input:
c       + dim: dimension of space
c       + Np_ext / ext_ctr: external contour of the crater, produced by subroutine "crater"
c       + deltax: length of the rectangle (external contour of the rim) in the X direction
c       + deltay: length of the rectangle (external contour of the rim) in the Y direction
c       + center: center of the rectangle
c       + Ntheta: number of angular sectors for discretizing the sphere portion
c	+ Nphi: number of longitude intervals for the sphere portion
c     
c     Output:
c       + Nv: number of vertices
c       + Nf: number of faces
c       + v: vertices
c       + f: faces
c
c     I/O
      integer dim
      integer Np_ext
      double precision ext_ctr(1:Nppt_mx,1:Ndim_mx)
      double precision deltax,deltay
      double precision center(1:Ndim_mx)
      integer Ntheta
      integer Nphi
      integer Nv,Nf
      double precision v(1:Nv_mx,1:Ndim_mx)
      integer f(1:Nf_mx,1:3)
c     temp
      integer i,j
      integer Np_out
      double precision out_ctr(1:Nppt_mx,1:Ndim_mx)
      integer Ncontour
      integer Npoints(1:Ncontour_mx)
      double precision points(1:Ncontour_mx,1:Nppt_mx,1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine rim_triangle'

      Nv=0
      Nf=0

c     Produce contour of the rectangle
      Np_out=1
      do j=1,dim
         out_ctr(1,j)=0.0D+0
      enddo                     ! j
      do i=1,Nphi
         Np_out=Np_out+1
         out_ctr(Np_out,1)=deltax*i/Nphi
         out_ctr(Np_out,2)=0.0D+0
         out_ctr(Np_out,3)=0.0D+0
      enddo                     ! i
      do i=1,Ntheta
         Np_out=Np_out+1
         out_ctr(Np_out,1)=deltax
         out_ctr(Np_out,2)=deltay*i/Ntheta
         out_ctr(Np_out,3)=0.0D+0
      enddo                     ! i
      do i=Nphi-1,0,-1
         Np_out=Np_out+1
         out_ctr(Np_out,1)=deltax*i/Nphi
         out_ctr(Np_out,2)=deltay
         out_ctr(Np_out,3)=0.0D+0
      enddo                     ! i
      do i=Ntheta-1,1,-1
         Np_out=Np_out+1
         out_ctr(Np_out,1)=0.0D+0
         out_ctr(Np_out,2)=deltay*i/Ntheta
         out_ctr(Np_out,3)=0.0D+0
      enddo                     ! i      

c     run delaunay triangulation
      Ncontour=2
      Npoints(1)=Np_out
      do i=1,Np_out
         do j=1,dim
            points(1,i,j)=out_ctr(i,j)
         enddo                  ! j
      enddo                     ! i
      Npoints(2)=Np_ext
      do i=1,Np_ext
         do j=1,dim
            points(2,i,j)=ext_ctr(i,j)
         enddo                  ! j
      enddo                     ! i
      call delaunay_triangulation(dim,Ncontour,Npoints,points,
     &     Nv,Nf,v,f)

      return
      end
      

      
      subroutine rim_test(dim,
     &     Np_ext,ext_ctr,deltax,deltay,
     &     center,Ntheta,Nphi,
     &     Nv,Nf,v,f)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce the trianglemesh of the rim of a crater
c     This version produces a non-quality trianglemesh, but
c     guaranteed without holes when stitching to a global spheric mesh
c     
c     Input:
c       + dim: dimension of space
c       + Np_ext / ext_ctr: external contour of the crater, produced by subroutine "crater"
c       + deltax: length of the rectangle (external contour of the rim) in the X direction
c       + deltay: length of the rectangle (external contour of the rim) in the Y direction
c       + center: center of the rectangle
c       + Ntheta: number of angular sectors for discretizing the sphere portion
c	+ Nphi: number of longitude intervals for the sphere portion
c     
c     Output:
c       + Nv: number of vertices
c       + Nf: number of faces
c       + v: vertices
c       + f: faces
c
c     I/O
      integer dim
      integer Np_ext
      double precision ext_ctr(1:Nppt_mx,1:Ndim_mx)
      double precision deltax,deltay
      double precision center(1:Ndim_mx)
      integer Ntheta
      integer Nphi
      integer Nv,Nf
      double precision v(1:Nv_mx,1:Ndim_mx)
      integer f(1:Nf_mx,1:3)
c     temp
      integer i,j
      integer idx1,idx2,idx3,idx4
      integer idx5,idx6,idx7,idx8
      integer i1,i2,i3
c     label
      character*(Nchar_mx) label
      label='subroutine rim_test'

      Nv=0
      Nf=0
c     idx1 & idx5
      Nv=Nv+1
      do j=1,dim
         v(Nv,1)=deltax
         v(Nv,2)=deltay
         v(Nv,3)=0.0D+0
      enddo                     ! j
      idx1=Nv
      do i=1,Np_ext/4+1
         Nv=Nv+1
         do j=1,dim
            v(Nv,j)=ext_ctr(i,j)+center(j)
         enddo                  ! j
      enddo                     ! i
      idx5=Nv
      do i=1,Np_ext/4
         Nf=Nf+1
         f(Nf,1)=idx1
         f(Nf,2)=idx1+i+1
         f(Nf,3)=idx1+i
      enddo                     ! i
c     idx2 & idx6
      Nv=Nv+1
      do j=1,dim
         v(Nv,1)=0.0D+0
         v(Nv,2)=deltay
         v(Nv,3)=0.0D+0
      enddo                     ! j
      idx2=Nv
      do i=Np_ext/4+1,Np_ext/2+1
         Nv=Nv+1
         do j=1,dim
            v(Nv,j)=ext_ctr(i,j)+center(j)
         enddo                  ! j
      enddo                     ! i
      idx6=Nv
      do i=1,Np_ext/4
         Nf=Nf+1
         f(Nf,1)=idx2
         f(Nf,2)=idx2+i+1
         f(Nf,3)=idx2+i
      enddo                     ! i
c     idx3 & idx7
      Nv=Nv+1
      do j=1,dim
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=0.0D+0
      enddo                     ! j
      idx3=Nv
      do i=Np_ext/2+1,3*Np_ext/4+1
         Nv=Nv+1
         do j=1,dim
            v(Nv,j)=ext_ctr(i,j)+center(j)
         enddo                  ! j
      enddo                     ! i
      idx7=Nv
      do i=1,Np_ext/4
         Nf=Nf+1
         f(Nf,1)=idx3
         f(Nf,2)=idx3+i+1
         f(Nf,3)=idx3+i
      enddo                     ! i
c     idx4 & idx8
      Nv=Nv+1
      do j=1,dim
         v(Nv,1)=deltax
         v(Nv,2)=0.0D+0
         v(Nv,3)=0.0D+0
      enddo                     ! j
      idx4=Nv
      do i=3*Np_ext/4+1,Np_ext+1
         Nv=Nv+1
         if (i.eq.Np_ext+1) then
            do j=1,dim
               v(Nv,j)=ext_ctr(1,j)+center(j)
            enddo               ! j
         else
            do j=1,dim
               v(Nv,j)=ext_ctr(i,j)+center(j)
            enddo               ! j
         endif
      enddo                     ! i
      idx8=Nv
      do i=1,Np_ext/4
         Nf=Nf+1
         f(Nf,1)=idx4
         f(Nf,2)=idx4+i+1
         f(Nf,3)=idx4+i
      enddo                     ! i
c     ---------------------------------------
c     Additional faces
c     ---------------------------------------
c     along top edge
      do i=1,Nphi
         if (i.lt.Nphi) then
            Nv=Nv+1
            v(Nv,1)=deltax*i/Nphi
            v(Nv,2)=deltay
            v(Nv,3)=0.0D+0
         endif                  ! i < Nphi
         i1=idx5
         if (i.eq.1) then
            i3=idx2
            i2=Nv
         else if (i.eq.Nphi) then
            i3=Nv
            i2=idx1
         else
            i3=Nv-1
            i2=Nv
         endif
         Nf=Nf+1
         f(Nf,1)=i1
         f(Nf,2)=i2
         f(Nf,3)=i3
      enddo                     ! i
c     along bottom edge
      do i=1,Nphi
         if (i.lt.Nphi) then
            Nv=Nv+1
            v(Nv,1)=deltax*i/Nphi
            v(Nv,2)=0.0D+0
            v(Nv,3)=0.0D+0
         endif                  ! i < Nphi
         i1=idx7
         if (i.eq.1) then
            i2=idx3
            i3=Nv
         else if (i.eq.Nphi) then
            i2=Nv
            i3=idx4
         else
            i2=Nv-1
            i3=Nv
         endif
         Nf=Nf+1
         f(Nf,1)=i1
         f(Nf,2)=i2
         f(Nf,3)=i3
      enddo                     ! i
c     along left edge
      do i=1,Ntheta
         if (i.lt.Ntheta) then
            Nv=Nv+1
            v(Nv,1)=0.0D+0
            v(Nv,2)=deltay*i/Ntheta
            v(Nv,3)=0.0D+0
         endif                  ! i < Nphi
         i1=idx6
         if (i.eq.1) then
            i3=idx3
            i2=Nv
         else if (i.eq.Ntheta) then
            i3=Nv
            i2=idx2
         else
            i3=Nv-1
            i2=Nv
         endif
         Nf=Nf+1
         f(Nf,1)=i1
         f(Nf,2)=i2
         f(Nf,3)=i3
      enddo                     ! i
c     along right edge
      do i=1,Ntheta
         if (i.lt.Ntheta) then
            Nv=Nv+1
            v(Nv,1)=deltax
            v(Nv,2)=deltay*i/Ntheta
            v(Nv,3)=0.0D+0
         endif                  ! i < Nphi
         i1=idx8
         if (i.eq.1) then
            i2=idx4
            i3=Nv
         else if (i.eq.Ntheta) then
            i2=Nv
            i3=idx1
         else
            i2=Nv-1
            i3=Nv
         endif
         Nf=Nf+1
         f(Nf,1)=i1
         f(Nf,2)=i2
         f(Nf,3)=i3
      enddo                     ! i

      return
      end
      
      

      subroutine crater(dim,
     &     radius,height,
     &     Nalpha,Nr,
     &     Nv,Nf,v,f,Np,ext_ctr)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to generate the trianglemesh for a circular crater
c     
c     Input:
c       + dim: dimension of space
c       + radius: radius of the crater [m]
c       + height: heigh of the crater [m]
c       + Nalpha: number of angular sectors for discretizing the crater
c       + Nr: number of radius sectors for discretizing the crater
c     
c     Output:
c       + Nv: number of vertices
c       + Nf: number of faces
c       + v: vertices
c       + f: faces
c       + Np: number of points for the external contour
c       + ext_ctr: external_contour
c     
c     I/O
      integer dim
      double precision radius
      double precision height
      integer Nalpha,Nr
      integer Nv,Nf
      double precision v(1:Nv_mx,1:Ndim_mx)
      integer f(1:Nf_mx,1:3)
      integer Np
      double precision ext_ctr(1:Nppt_mx,1:Ndim_mx)
c     temp
      integer itheta,ir,j
      double precision dtheta,dr,L,z1,z2
      double precision theta1,theta2,r1,r2
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision p4(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine crater'

      dtheta=2.0D+0*pi/dble(Nalpha)
      dr=radius/dble(Nr)
      L=4.0D+0*radius/3.0D+0
      Nv=0
      Nf=0
      Np=0
      
      Nv=1
      do j=1,dim-1
         v(1,j)=0.0D+0
      enddo                     ! j
      v(1,dim)=-height
      ir=1
      r1=0.0D+0
      r2=dr
      z2=height*dsin(2.0D+0*pi*r2/L-pi/2.0D+0)
      do itheta=1,Nalpha
         theta1=dtheta*dble(itheta-1)
         theta2=dtheta*dble(itheta)
         p1(1)=r2*dcos(theta1)
         p1(2)=r2*dsin(theta1)
         p1(3)=z2
         p2(1)=r2*dcos(theta2)
         p2(2)=r2*dsin(theta2)
         p2(3)=z2
         Nv=Nv+1
         do j=1,dim
            v(Nv,j)=p1(j)
         enddo                  ! j
         Nv=Nv+1
         do j=1,dim
            v(Nv,j)=p2(j)
         enddo                  ! j
         Nf=Nf+1
         f(Nf,1)=1
         f(Nf,2)=Nv-1
         f(Nf,3)=Nv
      enddo                     ! itheta
      do ir=2,Nr
         r1=dr*dble(ir-1)
         r2=dr*dble(ir)
         z1=height*dsin(2.0D+0*pi*r1/L-pi/2.0D+0)
         z2=height*dsin(2.0D+0*pi*r2/L-pi/2.0D+0)
         do itheta=1,Nalpha
            theta1=dtheta*dble(itheta-1)
            theta2=dtheta*dble(itheta)
            p1(1)=r1*dcos(theta1)
            p1(2)=r1*dsin(theta1)
            p1(3)=z1
            p2(1)=r2*dcos(theta1)
            p2(2)=r2*dsin(theta1)
            p2(3)=z2
            p3(1)=r2*dcos(theta2)
            p3(2)=r2*dsin(theta2)
            p3(3)=z2
            p4(1)=r1*dcos(theta2)
            p4(2)=r1*dsin(theta2)
            p4(3)=z1
            Nv=Nv+1
            do j=1,dim
               v(Nv,j)=p1(j)
            enddo               ! j
            Nv=Nv+1
            do j=1,dim
               v(Nv,j)=p2(j)
            enddo               ! j
            Nv=Nv+1
            do j=1,dim
               v(Nv,j)=p3(j)
            enddo               ! j
            Nv=Nv+1
            do j=1,dim
               v(Nv,j)=p4(j)
            enddo               ! j
            Nf=Nf+1
            f(Nf,1)=Nv-3
            f(Nf,2)=Nv-2
            f(Nf,3)=Nv-1
            Nf=Nf+1
            f(Nf,1)=Nv-3
            f(Nf,2)=Nv-1
            f(Nf,3)=Nv
            if (ir.eq.Nr) then
               if (itheta.eq.1) then
                  Np=Np+2
                  do j=1,dim
                     ext_ctr(1,j)=p2(j)
                     ext_ctr(2,j)=p3(j)
                  enddo         ! j
               else
                  Np=Np+1
                  do j=1,dim
                     ext_ctr(Np,j)=p3(j)
                  enddo         ! j
               endif            ! itheta=1
            endif               ! ir=Nr
         enddo                  ! itheta
      enddo                     ! ir

      return
      end


      
      subroutine mapv_cart2spher(dim,
     &     sphere_radius,
     &     theta_min,theta_max,
     &     phi_min,phi_max,
     &     deltax,deltay,
     &     vertex)
      implicit none
      include 'max.inc'
c     
c     Purpose: to map a vertex initialy defined in cartesian coordinates
c     onto spherical coordinates
c     
c     Input:
c       + dim: dimension of space
c       + sphere_radius: radius of the sphere [m]
c       + theta_min, theta_max: min and max latitude of the sphere portion
c       + phi_min, phi_max: min and max longuitude of the sphere portion
c       + deltax, deltay: length over the X and Y axis of the plane portion
c       + vertex: (x,y,z) cartesian coordinates
c     
c     Output
c       + vertex: (x,y,z) spherical coordinates
c
c     I/O
      integer dim
      double precision sphere_radius
      double precision theta_min,theta_max
      double precision phi_min,phi_max
      double precision deltax,deltay
      double precision vertex(1:Ndim_mx)
c     temp
      double precision x,y,z
      double precision r,theta,phi
c     label
      character*(Nchar_mx) label
      label='subroutine mapv_cart2spher'

      x=vertex(1)
      y=vertex(2)
      z=vertex(3)
      phi=phi_min+(phi_max-phi_min)*x/deltax
      theta=theta_min+(theta_max-theta_min)*y/deltay
      r=sphere_radius+z
      vertex(1)=r*dcos(theta)*dcos(phi)
      vertex(2)=r*dcos(theta)*dsin(phi)
      vertex(3)=r*dsin(theta)

      return
      end
