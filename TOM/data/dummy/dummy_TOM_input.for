      program dummy_TOM_input
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to generate a dummy input file for TOM
c
c     Variables
      character*(Nchar_mx) datafile
      integer dim
      integer Nlayer
      double precision radius_gas,Hatm
      double precision z_grid(0:Nlayer_mx)
      double precision surface_temperature
      integer Nlambda_reflectivity
      double precision lambda_reflectivity(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision Tgas(1:Nlayer_mx)
      integer Nband
      integer Nq(1:Nband_mx)
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      integer Naerosol
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision g_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
c     temp
      logical general_volumic_grid
      integer i,iband,iquad,ilambda,iaerosol,iface
      character*(Nchar_mx) str3
      logical err_code
      double precision delta_lambda
      double precision ka_ref,ks_ref
      double precision rho_lw,rho_sw
      integer Ntheta,Nphi
      double precision grid_theta_gas(0:Nlat_mx)
      double precision grid_phi_gas(0:Nlon_mx)
      integer*8 Nnode,Ncell
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      integer mtl_idx(1:Nf_mx)
      character*(Nchar_mx) prefix
      character*(Nchar_mx) gas_grid_file
      character*(Nchar_mx) gas_radiative_properties_file
      character*(Nchar_mx) gas_thermodynamic_properties_file
      character*(Nchar_mx) mtl_file
      character*(Nchar_mx) mtlidx_file
      character*(Nchar_mx) grid_file_surface
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) surface_properties_file
      character*(Nchar_mx) aerosol_grid_file
      character*(Nchar_mx) phase_function_file
      character*(Nchar_mx) phase_function_list_file
      character*(Nchar_mx) aerosol_radiative_properties_file
      logical record_obj_file
      double precision lambda1,lambda2,lambda
      double precision z1,z2,z
      double precision f
      character*(Nchar_mx) command
c     label
      character*(Nchar_mx) label
      label='program dumm_TOM_input'

c     general_volumic_grid:
c     + T: each node is shared by several cells
c     + F: each cell has its own nodes, not shared by any other cell
c      general_volumic_grid=.true.
      general_volumic_grid=.false.

c      ka=1.0D-5                 ! m⁻¹
c      ks=2.0D-5                 ! m⁻¹
      ka_ref=1.0D-5            ! m⁻¹
      ks_ref=2.0D-5             ! m⁻¹
      rho_sw=1.0D+0             ! SW reflectivity (reflective material)
      rho_lw=0.0D+0             ! LW reflectivity (emissive material)

c     Dimension of space
      dim=3
c     Vertical grid
      Nlayer=100
c      Nlayer=2
      if (Nlayer.gt.Nlayer_mx) then
         call error(label)
         write(*,*) 'Nlayer=',Nlayer
         write(*,*) '> Nlayer_mx=',Nlayer_mx
         stop
      endif
      radius_gas=1000.0D+3      ! m (radius of the solid core)
      Hatm=100.0D+3             ! m (height of the atmosphere)
      call generate_homogeneous_cartesian_grid(radius_gas,radius_gas+Hatm,Nlayer,z_grid)
c     Debug
c      write(*,*) 'radius_gas=',radius_gas
c      write(*,*) 'radius_gas+Hatm=',radius_gas+Hatm
c      write(*,*) 'z_grid=',z_grid
c      stop
c     Debug
      z1=0.0D+0
      z2=Hatm*1.20D+0
c     Atmosphere culminates at 10*10=100km, with a outer sphere of radius 1100km
c     Surface temperature
      surface_temperature=300.0D+0 ! K
c     Spectral reflectivity
      Nlambda_reflectivity=7
      if (Nlambda_reflectivity.lt.1) then
         call error(label)
         write(*,*) 'Nlambda_reflectivity=',Nlambda_reflectivity
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nlambda_reflectivity.gt.Nlambda_mx) then
         call error(label)
         write(*,*) 'Nlambda_reflectivity=',Nlambda_reflectivity
         write(*,*) 'should be < Nlambda_mx=',Nlambda_mx
         stop
      endif
      lambda_reflectivity(1)=0.250D+0 ! µm
      do ilambda=2,Nlambda_reflectivity
         if (lambda_reflectivity(ilambda-1).lt.1.0D+0) then
            delta_lambda=0.10   ! µm
         else
            delta_lambda=2.0D+0 ! µm
         endif
         lambda_reflectivity(ilambda)=lambda_reflectivity(ilambda-1)+delta_lambda ! µm
      enddo                     ! ilambda
c     Debug
      write(*,*) 'Reflectivity spectral range: [',lambda_reflectivity(1),'-',lambda_reflectivity(Nlambda_reflectivity),'] µm'
c     Debug
      do ilambda=1,Nlambda_reflectivity
         if (lambda_reflectivity(ilambda).le.1.0D+0) then
            reflectivity(ilambda)=rho_sw
         else
            reflectivity(ilambda)=rho_lw
         endif
      enddo                     ! ilambda
c     Gas temperature
      do i=1,Nlayer
         z=(z_grid(i-1)+z_grid(i))/2.0D+0-z_grid(0) ! relative to the ground [m]
         call vertical_profile(0.0D+0,1.0D+0,1.0D+0,1,1,z1,z2,z,f)
         if (f.lt.0.0D+0) then
            call error(label)
            write(*,*) 'f=',f
            write(*,*) 'z1=',z1
            write(*,*) 'z2=',z2
            write(*,*) 'z=',z
            stop
         else
            Tgas(i)=surface_temperature*f
         endif
      enddo                     ! i
c     Spectral grid
      Nband=Nlambda_reflectivity-1
      if (Nband.le.0) then
         call error(label)
         write(*,*) 'Nband=',Nband
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nband.gt.Nband_mx) then
         call error(label)
         write(*,*) 'Nband=',Nband
         write(*,*) '> Nband_mx=',Nband_mx
         stop
      endif
      do iband=1,Nband
         lambda_min(iband)=lambda_reflectivity(iband)
         lambda_max(iband)=lambda_reflectivity(iband+1)
      enddo                     ! iband
c     Debug
      write(*,*) 'Gas spectral range: [',lambda_min(1),'-',lambda_max(Nband),'] µm'
c     Debug
      do iband=1,Nband
         Nq(iband)=4
         if (Nq(iband).gt.Nquad_mx) then
            call error(label)
            write(*,*) 'Nq(',iband,')=',Nq(iband)
            write(*,*) '> Nquad_mx=',Nquad_mx
            stop
         endif
         do iquad=1,Nq(iband)
            w(iband,iquad)=1.0D+0/dble(Nq(iband))
         enddo                  ! iquad
      enddo                     ! iband
c     Number of aerosol modes
      Naerosol=1
c      Naerosol=0
      if (Naerosol.gt.Naerosol_mx) then
         call error(label)
         write(*,*) 'Naerosol=',Naerosol
         write(*,*) '> Naerosol_mx=',Naerosol_mx
         stop
      endif
c     Radiative properties
      lambda1=0.0D+0
      lambda2=lambda_max(Nband)
c     Debug
c      write(*,*) '================================================'
c     Debug
      do iband=1,Nband
         lambda=(lambda_min(iband)+lambda_max(iband))/2.0D+0 ! [µm]
         do i=1,Nlayer
            z=(z_grid(i-1)+z_grid(i))/2.0D+0-z_grid(0) ! relative to the ground [m]
            do iquad=1,Nq(iband)
               call vertical_profile(lambda1,lambda2,lambda,Nq(iband),iquad,z1,z2,z,f)
               if (f.lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'f=',f
                  write(*,*) 'lambda1=',lambda1
                  write(*,*) 'lambda2=',lambda2
                  write(*,*) 'lambda=',lambda
                  write(*,*) 'Nq=',Nq(iband)
                  write(*,*) 'iquad=',iquad
                  write(*,*) 'z1=',z1
                  write(*,*) 'z2=',z2
                  write(*,*) 'z=',z
                  stop
               else
                  ka_gas(i,iband,iquad)=ka_ref*f
               endif
            enddo               ! iquad
            call vertical_profile(lambda1,lambda2,lambda,Nq(iband),1,z1,z2,z,f)
            if (f.lt.0.0D+0) then
               call error(label)
               write(*,*) 'f=',f
               write(*,*) 'lambda1=',lambda1
               write(*,*) 'lambda2=',lambda2
               write(*,*) 'lambda=',lambda
               write(*,*) 'Nq=',Nq(iband)
               write(*,*) 'iquad=1'
               write(*,*) 'z1=',z1
               write(*,*) 'z2=',z2
               write(*,*) 'z=',z
               stop
            else
               ks_gas(i,iband)=ks_ref*f
            endif
            if (Naerosol.gt.0) then
c     Aerosol mode 1
               if ((i.ge.20).and.(i.le.79)) then
                  call vertical_profile(0.0D+0,1.0D+0,1.0D+0,1,1,0.0D+0,Hatm,z,f)
                  ka_aerosol(1,i,iband)=1.0D-6*f ! m¯¹
                  ks_aerosol(1,i,iband)=2.0D-6*f ! m¯¹
                  g_aerosol(1,i,iband)=0.20D+0
               else
                  ka_aerosol(1,i,iband)=0.0D+0 ! m¯¹
                  ks_aerosol(1,i,iband)=0.0D+0 ! m¯¹
                  g_aerosol(1,i,iband)=0.0D+0
               endif
c$$$c     Aerosol mode 2
c$$$               if ((i.ge.30).and.(i.le.39)) then
c$$$                  ka_aerosol(2,i,iband)=1.0D-4 ! m¯¹
c$$$                  ks_aerosol(2,i,iband)=1.0D-4 ! m¯¹
c$$$                  g_aerosol(2,i,iband)=-0.30D+0
c$$$               else
c$$$                  ka_aerosol(2,i,iband)=0.0D+0 ! m¯¹
c$$$                  ks_aerosol(2,i,iband)=0.0D+0 ! m¯¹
c$$$                  g_aerosol(2,i,iband)=0.0D+0
c$$$               endif
c$$$c     Aerosol mode 3
c$$$               if ((i.ge.50).and.(i.le.69)) then
c$$$                  ka_aerosol(3,i,iband)=1.0D-4 ! m¯¹
c$$$                  ks_aerosol(3,i,iband)=1.0D-4 ! m¯¹
c$$$                  g_aerosol(3,i,iband)=0.70D+0
c$$$               else
c$$$                  ka_aerosol(3,i,iband)=0.0D+0 ! m¯¹
c$$$                  ks_aerosol(3,i,iband)=0.0D+0 ! m¯¹
c$$$                  g_aerosol(3,i,iband)=0.0D+0
c$$$               endif
            endif               ! Naerosol > 0
         enddo                  ! i
      enddo                     ! iband
c     Debug
c      write(*,*) '================================================'
c     Debug

      write(*,*) '----------------------------------------------'
      write(*,*) 'Production of TOM input dataset'
      write(*,*) '----------------------------------------------'
      datafile='./results/dummy_dataset.dat'
      call record_tom_datafile(datafile,dim,Nlayer,z_grid,surface_temperature,
     &     Nlambda_reflectivity,lambda_reflectivity,reflectivity,
     &     Tgas,
     &     Nband,Nq,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,Naerosol,ka_aerosol,ks_aerosol,g_aerosol)

c     Debug
c      stop
c     Debug

      write(*,*) '----------------------------------------------'
      write(*,*) 'Production of htrdr_planeto input data files'
      write(*,*) '----------------------------------------------'
c     Number of latitude and longitude intervals for the 3D grid
      Ntheta=64
c      Ntheta=32 ! basic value
      Nphi=2*Ntheta
      if (Ntheta.gt.Nlat_mx) then
         call error(label)
         write(*,*) 'Ntheta=',Ntheta
         write(*,*) '> Nlat_mx=',Nlat_mx
         stop
      endif
      if (Nphi.gt.Nlon_mx) then
         call error(label)
         write(*,*) 'Nphi=',Nphi
         write(*,*) '> Nlon_mx=',Nlon_mx
         stop
      endif
      call generate_homogeneous_latitude_grid(Ntheta,grid_theta_gas)
      call generate_homogeneous_longitude_grid(Nphi,grid_phi_gas)
      
c     Surface properties
      write(*,*) '+ Surface properties'
      call partial_sphere_obj(dim,z_grid(0),-pi/2.0D+0,pi/2.0D+0,0.0D+0,2.0D+0*pi,Ntheta,Nphi,
     &     Nv,Nf,vertices,faces)
      mtlidx_file='./results/materials_list.txt'
      grid_file_surface='./results/sphere.bin'
      obj_file='./results/sphere.obj'
      mtllib_file='materials.mtl'
      surface_properties_file='./results/surface_properties.bin'
      Nmat=1
      do iface=1,Nf
         mtl_idx(iface)=1
      enddo                     ! iface
      materials(1)='mtl_001.dat'
      mtl_file='./results/'//trim(materials(1))
      call record_material_definition_file(0.0D+0,Nlambda_reflectivity,lambda_reflectivity,reflectivity,mtl_file)
      record_obj_file=.false.
      call record_surface_data(dim,mtlidx_file,grid_file_surface,record_obj_file,obj_file,mtllib_file,surface_properties_file,
     &     Nv,Nf,vertices,faces,Nmat,materials,mtl_idx,surface_temperature)
      write(*,*)
      write(*,*) '+ Gas properties'
c     Produce node and tetrahedric cells for the gas
      gas_grid_file='./results/gas_grid.bin'
      if (general_volumic_grid) then
         call volumic_grid(dim,Ntheta,Nphi,Nlayer,
     &        grid_theta_gas,grid_phi_gas,z_grid,
     &        gas_grid_file,Nnode,Ncell)
      else
         call volumic_grid2(dim,Ntheta,Nphi,Nlayer,
     &        grid_theta_gas,grid_phi_gas,z_grid,
     &        gas_grid_file,Nnode,Ncell)
      endif

c     Record gas radiative and thermodynamic properties
      gas_radiative_properties_file='./results/gas_radiative_properties.bin'
      gas_thermodynamic_properties_file='./results/gas_thermodynamic_properties.bin'
      call gas_properties(dim,general_volumic_grid,gas_radiative_properties_file,gas_thermodynamic_properties_file,
     &     Nlayer,Ntheta,Nphi,z_grid,Nnode,
     &     Nband,Nq,lambda_min,lambda_max,w,
     &     surface_temperature,ka_ref,ks_ref,lambda1,lambda2,z1,z2)

c     Aerosols
      if (Naerosol.gt.0) then
         write(*,*)
         write(*,*) '+ Aerosol properties'
      endif                     ! Naerosol > 0
      do iaerosol=1,Naerosol
         call num2str3(iaerosol,str3,err_code)
         if (err_code) then
            call error(label)
            write(*,*) 'cloud not convert to str3:'
            write(*,*) 'iaerosol=',iaerosol
            stop
         else
            prefix='aerosol'//trim(str3)
         endif
c     Produce node and tetrahedric cells for the aerosol
         aerosol_grid_file='./results/'//trim(prefix)//'_grid.bin'
         if (general_volumic_grid) then
            call volumic_grid(dim,Ntheta,Nphi,Nlayer,
     &           grid_theta_gas,grid_phi_gas,z_grid,
     &           aerosol_grid_file,Nnode,Ncell)
         else
            call volumic_grid2(dim,Ntheta,Nphi,Nlayer,
     &           grid_theta_gas,grid_phi_gas,z_grid,
     &           aerosol_grid_file,Nnode,Ncell)
         endif
c     Produce radiative properties files for the aerosol
         phase_function_file='./results/'//trim(prefix)//'_phase.bin'
         phase_function_list_file='./results/'//trim(prefix)//'_phase_function_list.txt'
         aerosol_radiative_properties_file='./results/'//trim(prefix)//'_radiative_properties.bin'
         call aerosol_properties(dim,general_volumic_grid,Nlayer,Ntheta,Nphi,z_grid,Nnode,Nband,lambda_min,lambda_max,
     &     Naerosol,ka_aerosol,ks_aerosol,g_aerosol,prefix,iaerosol,
     &     phase_function_file,phase_function_list_file,aerosol_radiative_properties_file)
      enddo                     ! iaerosol
c
      command='rm -f cell_idx.txt ka_aerosol.txt ka_gas.txt ks_aerosol.txt ks_gas.txt node_coord.txt phase_function_idx.txt T.txt xnode.txt'
      call exec(command)

      end



      subroutine vertical_profile(lambda1,lambda2,lambda,Nq,quad_index,z1,z2,z,f)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the value of a profile that is:
c     - linearly increasing with wavelength
c     - linearly decreasing with the quadrature index (1 for quad_index=1, 1/Nq for quad_index=Nq)
c     - linearly decreasing with altitude
c     
c     Input:
c       + lambda1: profile will be minimum (0) for this wavelength [µm]
c       + lambda2: profile will be maximum (1) for this wavelength [µm]
c       + lambda: actual value of the wavelength the profile is evaluated for [µm]
c       + Nq: quadrature order
c       + quad_index: quadrature index
c       + z1: profile will be maximum (1) for this altitude [m]
c       + z2: profile will be minimum (0) for this altitude [m]
c       + z: actual value of the altitude the profile is evaluated for [m]
c     
c     Output:
c       + f: value of the profile
c     
c     I/O
      double precision lambda1,lambda2,lambda
      integer Nq,quad_index
      double precision z1,z2,z
      double precision f
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine vertical_profile'

      if ((lambda.lt.lambda1).or.(lambda.gt.lambda2)) then
         call error(label)
         write(*,*) 'Inconsistent wavelength lambda=',lambda
         write(*,*) 'for range: [',lambda1,',',lambda2,']'
         stop
      endif
      if ((quad_index.lt.1).or.(quad_index.gt.Nq)) then
         call error(label)
         write(*,*) 'Inconsistent quadrature index quad_index=',quad_index
         write(*,*) 'for range: [0,',Nq,']'
         stop
      endif
      if ((z.lt.z1).or.(z.gt.z2)) then
         call error(label)
         write(*,*) 'Inconsistent altitude z=',z
         write(*,*) 'for range: [',z1,',',z2,']'
         stop
      endif
c     This one varies with quadrature index
      f=(lambda-lambda1)/(lambda2-lambda1)*(1.0D+0-dble(quad_index-1)/dble(Nq))*(1.0D+0-(z-z1)/(z2-z1))
c     This one does not
c      f=(lambda-lambda1)/(lambda2-lambda1)*(1.0D+0-(z-z1)/(z2-z1))
      
      return
      end
