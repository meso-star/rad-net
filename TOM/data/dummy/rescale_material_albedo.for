      subroutine rescale_material_albedo(Nlambda,lambda,lambda_min,lambda_max)
      implicit none
      include 'max.inc'
c     
c     Purpose: to modify the values of wavelength for a properties spectrum
c     in order to rescale it over a given spectral range
c     
c     Input:
c       + Nlambda: number of wavelength
c       + lambda: values of the wavelength [nm]
c       + lambda_min: minimum value of wavelength for rescaling [nm]
c       + lambda_max: maximum value of wavelength for rescaling [nm]
c     
c     Output:
c       + (modified) lambda
c     
c     I/O
      integer Nlambda
      double precision lambda(1:Nlambda_mat_mx)
      double precision lambda_min
      double precision lambda_max
c     temp
      integer ilambda
      double precision Delta_lambda,lambda1,alpha
c     label
      character*(Nchar_mx) label
      label='subroutine rescale_material_albedo'

      if (lambda_min.ge.lambda_max) then
         call error(label)
         write(*,*) 'lambda_min=',lambda_min
         write(*,*) 'should be < lambda_max=',lambda_max
         stop
      endif
      lambda1=lambda(1)
      Delta_lambda=lambda(Nlambda)-lambda(1)
      alpha=(lambda_max-lambda_min)/Delta_lambda
      do ilambda=1,Nlambda
         lambda(ilambda)=lambda_min+(lambda(ilambda)-lambda1)*alpha
      enddo                     ! ilambda

      return
      end
