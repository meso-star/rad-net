c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine delaunay_triangulation(dim,Ncontour,Npoints,points,
     &     Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
c     
c     Purpose: to perform a Delaunay triangulation of a close surface
c     provided its contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours (including holes)
c       + Npoints: number of points that define each contour
c       + points: list of points that define each contour
c     
c     Output:
c       + Nv: number of vertices
c       + Nf: number of triangular faces that have been generated
c       + vertices: vertices
c       + faces: index of the 3 vertices that define each face
c     
c     I/O
      integer dim
      integer Ncontour
      integer Npoints(1:Ncontour_mx)
      double precision points(1:Ncontour_mx,1:Nppt_mx,1:Ndim_mx)
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
c     temp
      integer i,j,ic
      character*(Nchar_mx) poly_file,outfile
      character*(Nchar_mx) command
      character*(Nchar_mx) output_file
      logical invert_normal
c     label
      character*(Nchar_mx) label
      label='subroutine delaunay_triangulation'

      poly_file='./triangle/contour'
      call record_poly_file(dim,Ncontour,Npoints,points,poly_file,
     &     outfile)
c     -----------------------------------------------------------------
c     run TRIANGLE
c      
c     quality mesh (-q option)
c     command='./triangle/triangle -Qpq0L '//trim(outfile)
c
c     Add no intermediary points to the provided contours (-YY option)
      command='./triangle/triangle -QpYY '//trim(outfile)
c     -----------------------------------------------------------------      
      call exec(command)
      call read_result_files(poly_file,dim,Nv,vertices,Nf,faces)

      return
      end
