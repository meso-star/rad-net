      subroutine record_surface_data(dim,mtlidx_file,grid_file_surface,record_obj_file,obj_file,mtllib_file,surface_properties_file,
     &     Nv,Nf,vertices,faces,Nmat,materials,mtl_idx,surface_temperature)
      implicit none
      include 'max.inc'
c     
c     Purpose: to record the surface data binary files
c     
c     Input:
c       + dim: dimension of space
c       + mtlidx_file: file that will record the index of each declared material
c       + grid_file_surface: file that will record geometric data of the surface
c       + record_obj_file: T if OBJ file has to be recorded
c       + obj_file: OBJ file to record
c       + mtllib_file: name of the materials library file
c       + surface_properties_file: file that will record material and temperature information for the surface
c       + Nv: total number of vertices
c       + Nf: total number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + Nmat: number of materials
c       + materials: array of materials (name of the material for each face)
c       + mtl_idx: index of the material (in the "materials" list) for each face
c       + Nt: number of theta/T values
c       + surface_temperature: values of the latitude [deg] and associated surface temperature [K]
c     
c     Ouput:
c       + the required binary files & materials list file
c     
c     I/O
      integer dim
      character*(Nchar_mx) mtlidx_file
      character*(Nchar_mx) grid_file_surface
      logical record_obj_file
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) surface_properties_file
      integer Nv
      integer Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      integer mtl_idx(1:Nf_mx)
      double precision surface_temperature
c     temp
      character*(Nchar_mx) temperature_file
      logical invert_normal
c     label
      character*(Nchar_mx) label
      label='subroutine record_surface_data'

c      write(*,*) 'Recording output files...'
c     -------------------------------------------------------------------------------
c     List of materials
      call record_surface_materials_list(Nmat,materials,mtlidx_file)
c     -------------------------------------------------------------------------------
c     Surface grid file
      call record_surface_grid_file(dim,Nv,Nf,vertices,faces,grid_file_surface)
c     -------------------------------------------------------------------------------
c     Surface properties file
      call record_surface_properties_file(dim,Nv,Nf,vertices,faces,mtl_idx,surface_temperature,surface_properties_file)
c     -------------------------------------------------------------------------------
c     OBJ file
      if (record_obj_file) then
         invert_normal=.false.
         call write_obj(dim,obj_file,mtllib_file,invert_normal,
     &        Nv,Nf,vertices,faces,Nmat,materials,mtl_idx)
      endif                     ! record_obj_file
      
      return
      end



      subroutine record_surface_properties_file(dim,Nv,Nf,vertices,faces,mtl_idx,surface_temperature,surface_properties_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'param.inc'
c     
c     Purpose: to record a surface properties binary file
c     
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: coordinates of each vertex
c       + faces: indexes of the vertices for each face
c       + mtl_idx: index of the material (in the "materials" list) for each face
c       + surface_temperature: values of the latitude [deg] and associated surface temperature [K]
c       + surface_properties_file: file to record
c     
c     Output: the required binary file
c     
c     I/O
      integer dim
      integer Nv
      integer Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer mtl_idx(1:Nf_mx)
      double precision surface_temperature
      character*(Nchar_mx) surface_properties_file
c     temp
      integer*8 Nv8
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
      integer*8 record_size,alignment
      double precision xG(1:Ndim_mx)
      double precision r,theta,phi,T
      integer iface,i
c     Debug
c$$$      integer mtlidx_min,mtlidx_max
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine record_surface_properties_file'

      record_size=int(size_of_int4+size_of_real,kind(record_size))
      alignment=record_size
      
      open(23,file=trim(surface_properties_file),form='unformatted',access='stream')
      write(23) int(pagesize,kind(Nv8))
      write(23) int(Nf,kind(Nv8))
      write(23) record_size
      write(23) alignment
c     Padding file 23 ---
      total_recorded=4*size_of_int8
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(23) (l1,i=1,remaining_byte)
c     --- Padding file 23
      do iface=1,Nf
c     Record information for the current face !
         write(23) mtl_idx(iface)-1,real(surface_temperature) ! material indexes should start @ 0
      enddo                     ! j
c     Padding file 23 ---
      total_recorded=Nf*record_size
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(23) (l1,i=1,remaining_byte)
c     --- Padding file 23
      close(23)
      write(*,*) 'File has been recorded: ',trim(surface_properties_file)

      return
      end



      subroutine record_surface_grid_file(dim,Nv,Nf,vertices,faces,grid_file_surface)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record a surface grid file
c     
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: coordinates of each vertex
c       + faces: indexes of the vertices for each face
c       + grid_file_surface: file to record
c     
c     Output: the required binary grid file
c     
c     I/O
      integer dim
      integer Nv
      integer Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      character*(Nchar_mx) grid_file_surface
c     temp
      integer*8 Nv8
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
      integer i,j,ivertex,iface
c     label
      character*(Nchar_mx) label
      label='subroutine record_surface_grid_file'

      open(22,file=trim(grid_file_surface),form='unformatted',access='stream')
      write(22) int(pagesize,kind(Nv8))
      write(22) int(Nv,kind(Nv8))
      write(22) int(Nf,kind(Nv8))
      write(22) dim
      write(22) Nvinface
c     Padding file 22 ---
      total_recorded=3*size_of_int8+2*size_of_int4
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(22) (l1,i=1,remaining_byte)
c     --- Padding file 22
      do ivertex=1,Nv
         write(22) (vertices(ivertex,j),j=1,dim)
      enddo                     ! iv
c     Padding file 22 ---
      total_recorded=dim*Nv*size_of_double
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(22) (l1,i=1,remaining_byte)
c     --- Padding file 22
      do iface=1,Nf
         write(22) (int(faces(iface,j),kind(Nv8))-1,j=1,Nvinface)
      enddo                     ! j
c     Padding file 22 ---
      total_recorded=Nvinface*Nf*size_of_int8
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(22) (l1,i=1,remaining_byte)
c     --- Padding file 22
      close(22)
      write(*,*) 'File has been recorded: ',trim(grid_file_surface)
      write(*,*) 'Number of vertices:',Nv
      write(*,*) 'Number of faces:',Nf

      return
      end
      

      
      subroutine record_surface_materials_list(Nmat,materials,mtlidx_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record the list of materials definition files
c     
c     Input:
c       + Nmat: number of materials
c       + materials: material definition file for each material
c       + mtlidx_file: materials list file to record
c     
c     Output: the required (ascii) file
c     
c     I/O
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      character*(Nchar_mx) mtlidx_file
c     temp
      integer imat
c     label
      character*(Nchar_mx) label
      label='subroutine record_surface_materials_list'

      open(21,file=trim(mtlidx_file))
      write(21,*) Nmat
      do imat=1,Nmat
         write(21,10) trim(materials(imat))
      enddo                     ! imat
      close(21)
      write(*,*) 'File has been recorded: ',trim(mtlidx_file)

      return
      end


      
      subroutine record_material_definition_file(brdf,Nlambda,lambda,reflectivity,mtl_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record a material definition file
c     
c     Input:
c       + brdf: type of BRDF (0: lambertian, 1: specular)
c       + Nlambda: number of wavelength in the definition of the reflectivity signal
c       + lambda: values of the wavelength [nm]
c       + reflectivity: associated values of the reflectivity
c       + mtl_file: ascii material definition file
c     
c     Output: the required ascii file
c     
c     I/O
      double precision brdf
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      character*(Nchar_mx) mtl_file
c     temp
      logical file_exists
      character*(Nchar_mx) brdf_type
      integer ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine record_material_definition_file'

      if (brdf.eq.0.0D+0) then
         brdf_type='lambertian'
      else if (brdf.eq.1.0D+0) then
         brdf_type='specular'
      else
         call error(label)
         write(*,*) 'brdf=',brdf
         write(*,*) 'is not supported yet'
         stop
      endif
      open(14,file=trim(mtl_file))
      write(14,23) 'wavelengths',Nlambda
      do ilambda=1,Nlambda
         write(14,*) lambda(ilambda), ! nm
     &        trim(brdf_type),
     &        reflectivity(ilambda)
      enddo                     ! ilambda
      close(14)

      return
      end
