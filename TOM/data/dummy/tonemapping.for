c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine filmic_tonemapping(dim,exposure_bias,whitescale,
     &     color_xyz)
      implicit none
      include 'max.inc'
c     
c     Purpose: apply tone mapping from Filmic
c     http://filmicworlds.com/blog/filmic-tonemapping-operators/
c     
c     Input:
c       + dim: dimension of colorspace
c       + exposure_bias: exposure factor
c       + whitescale: white scale factor
c       + color_xyz: color in the XYZ colorspace
c     
c     Output:
c       + color_xyz: modified color, after tone mapping
c     
c     I/O
      integer dim
      double precision exposure_bias
      double precision whitescale
      double precision color_xyz(1:Ndim_mx)
c     temp
      integer i
      double precision tmp1,tmp2
c     functions
      double precision Uncharted2Tonemap
c     label
      character*(Nchar_mx) label
      label='subroutine filmic_tonemapping'

      do i=1,dim
         tmp1=Uncharted2Tonemap(exposure_bias*16.0D+0*color_xyz(i))
         tmp2=1.0D+0/Uncharted2Tonemap(whitescale)
         color_xyz(i)=(tmp1*tmp2)**(1.0D+0/2.20D+0)
      enddo                     ! i

      return
      end
      

      
      double precision function Uncharted2Tonemap(x)
      implicit none
      include 'max.inc'
c     
c     function used by the Filmic tonemapper
c
c     I/O
      double precision x
c     parameters
      double precision A,B,C,D,E,F
      parameter(A=0.15D+0)
      parameter(B=0.50D+0)
      parameter(C=0.10D+0)
      parameter(D=0.20D+0)
      parameter(E=0.02D+0)
      parameter(F=0.30D+0)

      Uncharted2Tonemap=((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F

      return
      end
