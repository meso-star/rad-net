c	Numeric parameters used for the size of arrays
c
	integer Ndim_mx
	integer Nlat_mx
	integer Nlon_mx
	integer Nlev_mx
	integer Nlayer_mx
	integer Nproba_mx
	integer Ntry_mx
	integer Niter_mx
	integer Naerosol_mx
	integer Nlambda_mx
	integer Nband_mx
	integer Nquad_mx
	integer Nicp_mx
	integer Nv_mx
	integer Nf_mx
	integer Nmaterial_mx
	integer Ncontour_mx
	integer Nppt_mx
	integer Nppc_mx
	integer Nangle_mx
	integer Np_mx
	integer Nlambda_mat_mx
	integer Nlambda_ill_mx
	integer Nlambda_sun_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
        parameter(Nlat_mx=256)
        parameter(Nlon_mx=2*Nlat_mx)
	parameter(Nlev_mx=101)
	parameter(Nlayer_mx=Nlev_mx-1)
	parameter(Nproba_mx=1000)
	parameter(Ntry_mx=100)
	parameter(Niter_mx=100)
	parameter(Naerosol_mx=5)
	parameter(Nlambda_mx=100)
	parameter(Nband_mx=100)
	parameter(Nquad_mx=20)
	parameter(Nicp_mx=4000)
        parameter(Nv_mx=30000000)
        parameter(Nf_mx=20000000)
        parameter(Nmaterial_mx=20000)
	parameter(Ncontour_mx=1000)
	parameter(Nppt_mx=1000)
	parameter(Nppc_mx=Nppt_mx)
	parameter(Nangle_mx=100)
	parameter(Np_mx=100)
        parameter(Nlambda_mat_mx=1000)
        parameter(Nlambda_ill_mx=1000)
	parameter(Nlambda_sun_mx=3500000)
	parameter(Nchar_mx=1000)
