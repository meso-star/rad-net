 10   format(a)
 11   format(I1)
 12   format(I2)
 13   format(I3)
 14   format(I4)
 15   format(I5)
 16   format(I6)
 17   format(I7)
 18   format(I8)
 19   format(I9)
 100  format(a,i4,a)
 101   format(I1)
 102   format(I2)
 103   format(I3)
 104   format(I4)
 105   format(I5)
 106   format(I6)
 107   format(I7)
 108   format(I8)
 109   format(I9)
 110   format(I10)
 21   format(a,3e23.15)
 22   format(a,3i10)
 23   format(a,i6)
 24   format(a,i4,a,i3,a,i3,a)
 25   format(a22,i3,a1,i3)
 26   format(7x,4(f4.2,1x))
 27   format(8x,4(f4.2,1x))
 28   format(10x,4(f4.2,1x))
 31   format(a,i8)
 32   format(a,i8,a)
 33   format(a,f14.8,a)
 34   format(a20,i3)
 35   format(a22,i3,a1,i3)
 39   format(a29,i3)
 40   format(a,3(3(f14.8,1x),1x),a)
 50   format(1000e17.9)
 52   format(1000e23.15)
 53   format(I1,1000e23.15)
 62   format(i12,F13.5,F14.4,i5,E14.6)