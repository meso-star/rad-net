c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine xyz2rgb(dim,M,gamma_correction,
     &     color_xyz,color_rgb)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to convert a colour from the XYZ to the RGB colorspace
c     
c     Input:
c       + dim: dimension of colorspaces
c       + M: XYZ -> RGB conversion matrix
c       + gamma_correction: true if gamma correction is required
c       + color_xyz: colour in the XYZ colorspace
c     
c     Output:
c       + color_rgb: colour in RGB colorspace
c     
c     I/O
      integer dim
      double precision M(1:Ndim_mx,1:Ndim_mx)
      logical gamma_correction
      double precision color_xyz(1:Ndim_mx)
      double precision color_rgb(1:Ndim_mx)
c     temp
      integer i
c     parameters
      double precision mfactor
      parameter(mfactor=3.0)
c     label
      character*(Nchar_mx) label
      label='subroutine xyz2rgb'

c     convert XYZ to RGB
      call matrix_vector(dim,M,color_xyz,color_rgb)
c     Debug
c      write(*,*) 'raw rgb:',(color_rgb(i),i=1,dim)
c     Debug
      call constrain_rgb(dim,color_rgb)
c     Debug
c      write(*,*) 'rgb after constrain:',(color_rgb(i),i=1,dim)
c     Debug
c      do i=1,dim
c         color_rgb(i)=color_rgb(i)*mfactor
c      enddo                     ! i
c     Debug
c      write(*,*) 'rgb after multiplication:',(color_rgb(i),i=1,dim)
c     Debug
      call norm_rgb(dim,color_rgb)
c     Debug
c      write(*,20) 'rgb after norm: ',color_rgb(1),',',
c     &     color_rgb(2),',',color_rgb(3)
c     Debug
      if (gamma_correction) then
         call rgb_gamma_correction(dim,color_rgb)
c         call L_correction(dim,color_rgb)
      endif                     ! gamma_correction
      
      return
      end


      
      subroutine xyz2rgb_matrix(dim,cs,M)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce the XYZ -> RGB conversion matrix
c     
c     Input:
c       + dim: dimension of colorspaces
c       + cs: colour system
c     
c     Output:
c       + M: XYZ -> RGB conversion matrix
c
c     I/O
      integer dim
      character*(Nchar_mx) cs
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      double precision xred,yred,zred
      double precision xgreen,ygreen,zgreen
      double precision xblue,yblue,zblue
      double precision xw,yw,zw
      double precision rx,ry,rz,rw
      double precision gx,gy,gz,gw
      double precision bx,by,bz,bw
c     label
      character*(Nchar_mx) label
      label='subroutine xyz2rgb_matrix'
      
      call colour_system(cs,
     &     xred,yred,xgreen,ygreen,xblue,yblue,xw,yw)
      zred=1.0D+0-(xred+yred)
      if (zred.lt.0.0D+0) then
         call error(label)
         write(*,*) 'zred=',zred
         write(*,*) 'should be positive'
         stop
      endif
      zgreen=1.0D+0-(xgreen+ygreen)
      if (zgreen.lt.0.0D+0) then
         call error(label)
         write(*,*) 'zgreen=',zgreen
         write(*,*) 'should be positive'
         stop
      endif
      zblue=1.0D+0-(xblue+yblue)
      if (zblue.lt.0.0D+0) then
         call error(label)
         write(*,*) 'zblue=',zblue
         write(*,*) 'should be positive'
         stop
      endif
      zw=1.0D+0-(xw+yw)
      if (zw.lt.0.0D+0) then
         call error(label)
         write(*,*) 'zw=',zw
         write(*,*) 'should be positive'
         stop
      endif

c     XYZ -> RGB matrix
      rx=ygreen*zblue-yblue*zgreen
      ry=zgreen*xblue-zblue*xgreen
      rz=xgreen*yblue-xblue*ygreen
      gx=yblue*zred-yred*zblue
      gy=zblue*xred-zred*xblue
      gz=xblue*yred-xred*yblue
      bx=yred*zgreen-ygreen*zred
      by=zred*xgreen-zgreen*xred
      bz=xred*ygreen-xgreen*yred
c     white scaling factors
      rw=(rx*xw+ry*yw+rz*zw)/yw
      gw=(gx*xw+gy*yw+gz*zw)/yw
      bw=(bx*xw+by*yw+bz*zw)/yw
c     scale the matrix using white factors
      M(1,1)=rx/rw
      M(1,2)=ry/rw
      M(1,3)=rz/rw
      M(2,1)=gx/gw
      M(2,2)=gy/gw
      M(2,3)=gz/gw
      M(3,1)=bx/bw
      M(3,2)=by/bw
      M(3,3)=bz/bw

      return
      end


      
      subroutine specified_xyz2rgb_matrix(dim,ws,M)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide an alternative XYZ -> RGB matrix
c     
c     This one comes from:
c     https://en.wikipedia.org/wiki/CIE_1931_color_space
c     
c     Input:
c       + dim: dimension of colorspace
c       + ws: workspace (character string) can be one of:
c     Adobe_RGB
c     Apple_RGB
c     Best_RGB
c     Beta_RGB
c     Bruce_RGB
c     CIE_RGB
c     ColorMatch_RGB
c     Don_RGB4
c     ECI_RGB
c     EktaSpace_PS5
c     NTSC_RGB
c     PAL_SECAM_RGB
c     ProPhoto_RGB
c     SMPTE-C_RGB
c     WideGamut_RGB
c     sRGB
c     
c     Output:
c       + M: XYZ -> RGB conversion matrix
c     
c     I/O
      integer dim
      character*(Nchar_mx) ws
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      character*(Nchar_mx) filename
      integer ios,i,j
c     label
      character*(Nchar_mx) label
      label='subroutine specified_xyz2rgb_matrix'

      if (trim(ws).eq.'Adobe_RGB') then
         filename='./data/XYZ2RGB_matrices/M_AdobeRGB_D65.dat'
      else if (trim(ws).eq.'Apple_RGB') then
         filename='./data/XYZ2RGB_matrices/M_AppleRGB_D65.dat'
      else if (trim(ws).eq.'Best_RGB') then
         filename='./data/XYZ2RGB_matrices/M_BestRGB_D50.dat'
      else if (trim(ws).eq.'Beta_RGB') then
         filename='./data/XYZ2RGB_matrices/M_BetaRGB_D50.dat'
      else if (trim(ws).eq.'Bruce_RGB') then
         filename='./data/XYZ2RGB_matrices/M_BriceRGB_D65.dat'
      else if (trim(ws).eq.'CIE_RGB') then
         filename='./data/XYZ2RGB_matrices/M_CIERGB_E.dat'
      else if (trim(ws).eq.'ColorMatch_RGB') then
         filename='./data/XYZ2RGB_matrices/M_ColorMatchRGB_D50.dat'
      else if (trim(ws).eq.'Don_RGB4') then
         filename='./data/XYZ2RGB_matrices/M_DonRGB4_D50.dat'
      else if (trim(ws).eq.'ECI_RGB') then
         filename='./data/XYZ2RGB_matrices/M_ECIRGB_D50.dat'
      else if (trim(ws).eq.'EktaSpace_PS5') then
         filename='./data/XYZ2RGB_matrices/M_EktaSpacePS5_D50.dat'
      else if (trim(ws).eq.'NTSC_RGB') then
         filename='./data/XYZ2RGB_matrices/M_NTSCRGB_C.dat'
      else if (trim(ws).eq.'PAL_SECAM_RGB') then
         filename='./data/XYZ2RGB_matrices/M_PAL_SECAM_RGB_D65.dat'
      else if (trim(ws).eq.'ProPhoto_RGB') then
         filename='./data/XYZ2RGB_matrices/M_ProPhotoRGB_D50.dat'
      else if (trim(ws).eq.'SMPTE-C_RGB') then
         filename='./data/XYZ2RGB_matrices/M_SMPTE-C_RGB_D65.dat'
      else if (trim(ws).eq.'WideGamut_RGB') then
         filename='./data/XYZ2RGB_matrices/M_WideGamutRGB_D50.dat'
      else if (trim(ws).eq.'sRGB') then
         filename='./data/XYZ2RGB_matrices/M_sRGB_D65.dat'
      else if (trim(ws).eq.'wikipedia') then
         filename='./data/XYZ2RGB_matrices/M_wikipedia.dat'
      else
         call error(label)
         write(*,*) 'Bad input argument:'
         write(*,*) 'ws=',trim(ws)
         stop
      endif

      open(11,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(filename)
         stop
      else
         read(11,*)
         do i=1,dim
            read(11,*) (M(i,j),j=1,dim)
         enddo                  ! i
      endif
      close(11)

      return
      end



      subroutine constrain_rgb(dim,color_rgb)
      implicit none
      include 'max.inc'
c     
c     Purpose:  if the requested RGB shade contains a negative weight for
c     one of the primaries, it lies outside the colour gamut
c     accessible from the given triple of primaries.  Desaturate
c     it by adding white, equal quantities of R, G, and B, enough
c     to make RGB all positive.  The function returns 1 if the
c     components were modified, zero otherwise.
c     
c     Input:
c       + dim: dimension of colorspaces
c       + color_rgb: colour in the RGB space
c     
c     Output:
c       + color_rgb: colour in the RGB space (updated)
c     
c     I/O
      integer dim
      double precision color_rgb(1:Ndim_mx)
c     temp
      integer i
      double precision w
c     label
      character*(Nchar_mx) label
      label='subroutine constrain_rgb'

      w=-min(0.0D+0,min(color_rgb(1),min(color_rgb(2),color_rgb(3))))
      if (w.gt.0.0D+0) then
         do i=1,dim
            color_rgb(i)=color_rgb(i)+w
         enddo                  ! i
      endif

      return
      end
      


      subroutine norm_rgb(dim,color_rgb)
      implicit none
      include 'max.inc'
c     
c     Purpose: to normalise RGB components so the most intense (unless all
c     are zero) has a value of 1.
c     
c     Input:
c       + dim: dimension of colorspaces
c       + color_rgb: colour in the RGB space
c     
c     Output:
c       + color_rgb: colour in the RGB space (updated)
c     
c     I/O
      integer dim
      double precision color_rgb(1:Ndim_mx)
c     temp
      integer i
      double precision m
c     label
      character*(Nchar_mx) label
      label='subroutine norm_rgb'

      m=max(color_rgb(1),max(color_rgb(2),color_rgb(3)))
      if (m.gt.1.0D+0) then
         do i=1,dim
            color_rgb(i)=color_rgb(i)/m
         enddo                  ! i
      endif

      return
      end

      

      subroutine rgb_gamma_correction(dim,color_rgb)
      implicit none
      include 'max.inc'
c     
c     Purpose: transform linear RGB values to nonlinear RGB values.
c     Rec.709 is ITU-R Recommendation BT. 709 (1990) ``Basic
c     Parameter Values for the HDTV Standard for the Studio and
c     for International Programme Exchange'', formerly CCIR Rec.709.
c     For details see:
c       http://www.poynton.com/ColorFAQ.html
c       http://www.poynton.com/GammaFAQ.html
c     
c     Input:
c       + dim: dimension of colorspaces
c       + color_rgb: colour in the RGB space
c     
c     Output:
c       + color_rgb: colour in the RGB space (updated)
c     
c     I/O
      integer dim
      double precision color_rgb(1:Ndim_mx)
c     temp
      integer i
      double precision cc,a,gamma
      parameter(cc=3.1308D-3)
      parameter(a=5.5D-2)
      parameter(gamma=2.4D+0)
c     label
      character*(Nchar_mx) label
      label='subroutine rgb_gamma_correction'

      do i=1,dim
         if (color_rgb(i).lt.cc) then
            color_rgb(i)=12.92D+0*color_rgb(i)
         else
            color_rgb(i)=(1.0D+0+a)*color_rgb(i)**(1.0D+0/gamma)-a
         endif
c         color_rgb(i)=color_rgb(i)**(1.0D+0/2.50D+0)
      enddo                     ! i

      return
      end

      

      subroutine L_correction(dim,color_rgb)
      implicit none
      include 'max.inc'
c     
c     Purpose: transform linear RGB values to nonlinear RGB values.
c     
c     Input:
c       + dim: dimension of colorspaces
c       + color_rgb: colour in the RGB space
c     
c     Output:
c       + color_rgb: colour in the RGB space (updated)
c     
c     I/O
      integer dim
      double precision color_rgb(1:Ndim_mx)
c     temp
      integer i
      double precision epsilon,kappa
      parameter(epsilon=8.856D-3)
      parameter(kappa=9.033D+2)
c     label
      character*(Nchar_mx) label
      label='subroutine L_correction'

      do i=1,dim
         if (color_rgb(i).lt.epsilon) then
            color_rgb(i)=color_rgb(i)*kappa*1.0D-2
         else
            color_rgb(i)=1.16*(color_rgb(i)**(1.0D+0/3.0D+0))-0.16D+0
         endif
      enddo                     ! i

      return
      end
