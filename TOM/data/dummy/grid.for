      subroutine volumic_grid(dim,Ntheta,Nphi,Nz,
     &     grid_theta,grid_phi,grid_z,grid_file,
     &     Nnode,Ncell)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to produce a tetrahedric grid from a latitude / longitude / altitude grid
c     
c     Input:
c       + dim: dimension of space
c       + Ntheta: number of latitude cells
c       + Nphi: number of longitude cells
c       + Nz: number of altitude cells
c       + grid_theta: latitude grid in a spherical coordinates system
c         where latitude varies between -90 degrees (south pole)
c         to 90 degrees (north pole)
c         - grid_theta(0)=-90 [degrees]
c         - grid_theta(i)=-90+180/Ntheta*i end of cell index i [degrees]
c         - grid_theta(Ntheta)=390 [degrees]
c       + grid_phi: longitude grid in a spherical coordinates system
c         where longitude varies between 0 and 360 degrees
c         - grid_phi(0)=0 [degrees]
c         - grid_phi(i)=360/Nphi*i end of cell index i [degrees]
c         - grid_phi(Nphi=360 [degrees]
c       + grid_z: 1D altitude grid
c         - grid_z(0)=zmin [m]
c         - grid_z(i)=zmin+(zmax-zmin)/Nz*i top of cell index i [m]
c         - grid_z(Nz)=zmax [m]
c       + grid_file: name of the file where the grid is stored
c     
c     Output:
c       + Nnode: number of nodes
c       + Ncell: number of tetrahedric cells
c       + the required "grid_file" file
c     
c     I/O
      integer dim
      integer Ntheta,Nphi,Nz
      double precision grid_theta(0:Nlat_mx)
      double precision grid_phi(0:Nlon_mx)
      double precision grid_z(0:Nlayer_mx)
      character*(Nchar_mx) grid_file
      integer*8 Nnode,Ncell
c     temp
      integer surf_theta_idx,surf_phi_idx,surf_z_idx
      integer itheta,iphi,iz,j
      double precision spherical_coordinates(1:Ndim_mx)
      double precision cartesian_coordinates(1:Ndim_mx)
      integer Nnode_cell
      integer*8 index(1:8)
      integer Npages,i
      integer*8 inode,icell
      double precision node_coord(1:Ndim_mx)
      integer*8 cell_idx(1:Nvincell)
      double precision xnode(1:Ndim_mx)
      character*(Nchar_mx) node_coord_file,cell_idx_file,xnode_file
c     label
      character*(Nchar_mx) label
      label='subroutine volumic_grid'

      if (Ntheta.lt.2) then
         call error(label)
         write(*,*) 'Ntheta=',Ntheta
         write(*,*) 'should be greater than 1'
         stop
      endif
      if (Nphi.lt.1) then
         call error(label)
         write(*,*) 'Nphi=',Nphi
         write(*,*) 'should be greater than 0'
         stop
      endif
      if (Nz.lt.1) then
         call error(label)
         write(*,*) 'Nz=',Nz
         write(*,*) 'should be greater than 0'
         stop
      endif

      node_coord_file='./node_coord.txt'
      cell_idx_file='./cell_idx.txt'
      xnode_file='./xnode.txt'
      open(11,file=trim(node_coord_file))
      open(12,file=trim(cell_idx_file))
      open(13,file=trim(xnode_file))
      
      write(*,*) 'Producing volumic grid'
      write(*,*) 'Number of nodes per altitude interface:',(Nphi*(Ntheta-1)+2)
      Nnode=(Nz+1)*(Nphi*(Ntheta-1)+2)
      Ncell=3*Nz*Nphi*(2+2*(Ntheta-2))
      write(11,*) Nnode
      write(12,*) Ncell
      write(13,*) Nnode
      
c     Record node coordinates, sorted by increasing absolute node index
      inode=0
      do surf_z_idx=0,Nz
         do surf_theta_idx=0,Ntheta
            if ((surf_theta_idx.eq.0).or.
     &           (surf_theta_idx.eq.Ntheta)) then
               inode=inode+1
               call node_coordinates(Ntheta,Nphi,Nz,
     &              grid_theta,grid_phi,grid_z,
     &              surf_theta_idx,1,surf_z_idx,
     &              spherical_coordinates,cartesian_coordinates)
               do j=1,dim
                  node_coord(j)=cartesian_coordinates(j)
               enddo            ! j
               write(11,*) (node_coord(j),j=1,dim)
               xnode(1)=spherical_coordinates(3)
               xnode(2)=spherical_coordinates(2)
               xnode(3)=spherical_coordinates(1)-grid_z(0)
               write(13,*) (xnode(j),j=1,dim)
            else
               do surf_phi_idx=0,Nphi-1
                  inode=inode+1
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 surf_theta_idx,surf_phi_idx,surf_z_idx,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(j)=cartesian_coordinates(j)
                  enddo         ! j
                  write(11,*) (node_coord(j),j=1,dim)
                  xnode(1)=spherical_coordinates(3)
                  xnode(2)=spherical_coordinates(2)
                  xnode(3)=spherical_coordinates(1)-grid_z(0)
                  write(13,*) (xnode(j),j=1,dim)
               enddo            ! surf_phi_idx
            endif               ! value of surf_theta_idx
         enddo                  ! surf_theta_idx
      enddo                     ! surf_z_idx
c     Record cell vertices (absolute indexes of the 4 nodes that define each tetrahedron)
      icell=0
      do iz=1,Nz
         do itheta=1,Ntheta
            do iphi=1,Nphi
               call node_indexes(Ntheta,Nphi,Nz,
     &              itheta,iphi,iz,
     &              Nnode_cell,index)
c               do i=1,Nnode_cell
c                  index(i)=index(i)-1 ! htrdr wants indexes to start from 0 
c               enddo            ! i
               if (itheta.eq.1) then
                  if (Nnode_cell.ne.6) then
                     call error(label)
                     write(*,*) 'itheta=',itheta
                     write(*,*) 'is equal to 0'
                     write(*,*) 'Nnode_cell=',Nnode_cell
                     write(*,*) 'should be equal to 6'
                     stop
                  endif
c     cell 1
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(3)
                  cell_idx(3)=index(2)
                  cell_idx(4)=index(4)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 2
                  icell=icell+1
                  cell_idx(1)=index(2)
                  cell_idx(2)=index(3)
                  cell_idx(3)=index(6)
                  cell_idx(4)=index(4)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 3
                  icell=icell+1
                  cell_idx(1)=index(2)
                  cell_idx(2)=index(6)
                  cell_idx(3)=index(5)
                  cell_idx(4)=index(4)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     
               else if (itheta.eq.Ntheta) then
                  if (Nnode_cell.ne.6) then
                     call error(label)
                     write(*,*) 'itheta=',itheta
                     write(*,*) 'is equal to Ntheta=',Ntheta
                     write(*,*) 'Nnode_cell=',Nnode_cell
                     write(*,*) 'should be equal to 6'
                     stop
                  endif
c     cell 1
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(3)
                  cell_idx(3)=index(2)
                  cell_idx(4)=index(5)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 2
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(6)
                  cell_idx(3)=index(3)
                  cell_idx(4)=index(5)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 3
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(4)
                  cell_idx(3)=index(6)
                  cell_idx(4)=index(5)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     
               else
                  if (Nnode_cell.ne.8) then
                     call error(label)
                     write(*,*) 'itheta=',itheta
                     write(*,*) 'Nnode_cell=',Nnode_cell
                     write(*,*) 'should be equal to 8'
                     stop
                  endif
c     cell 1
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(7)
                  cell_idx(3)=index(3)
                  cell_idx(4)=index(4)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 2
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(8)
                  cell_idx(3)=index(7)
                  cell_idx(4)=index(4)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 3
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(2)
                  cell_idx(3)=index(8)
                  cell_idx(4)=index(4)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 4
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(5)
                  cell_idx(3)=index(7)
                  cell_idx(4)=index(6)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 5
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(7)
                  cell_idx(3)=index(8)
                  cell_idx(4)=index(6)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 6
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(8)
                  cell_idx(3)=index(2)
                  cell_idx(4)=index(6)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     
               endif            ! value of itheta
c     
            enddo               ! iphi
         enddo                  ! itheta
      enddo                     ! iz
      close(11)
      close(12)
      close(13)
      call record_volumic_grid_file(dim,Nnode,Ncell,grid_file)
c     Debug
      write(*,*) 'Ncell=',Ncell
c     Debug

      return
      end


      
      subroutine volumic_grid2(dim,Ntheta,Nphi,Nz,
     &     grid_theta,grid_phi,grid_z,grid_file,
     &     Nnode,Ncell)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to produce a tetrahedric grid from a latitude / longitude / altitude grid
c     This version produces a grid where each cell can be set as uniform, i.e. grid
c     nodes are duplicated multiple times so that the value given at any given node has
c     no influence on the property retrieved over a separate cell.
c     
c     Input:
c       + dim: dimension of space
c       + Ntheta: number of latitude cells
c       + Nphi: number of longitude cells
c       + Nz: number of altitude cells
c       + grid_theta: latitude grid in a spherical coordinates system
c         where latitude varies between -90 degrees (south pole)
c         to 90 degrees (north pole)
c         - grid_theta(0)=-90 [degrees]
c         - grid_theta(i)=-90+180/Ntheta*i end of cell index i [degrees]
c         - grid_theta(Ntheta)=390 [degrees]
c       + grid_phi: longitude grid in a spherical coordinates system
c         where longitude varies between 0 and 360 degrees
c         - grid_phi(0)=0 [degrees]
c         - grid_phi(i)=360/Nphi*i end of cell index i [degrees]
c         - grid_phi(Nphi=360 [degrees]
c       + grid_z: 1D altitude grid
c         - grid_z(0)=zmin [m]
c         - grid_z(i)=zmin+(zmax-zmin)/Nz*i top of cell index i [m]
c         - grid_z(Nz)=zmax [m]
c       + grid_file: name of the file where the grid is stored
c     
c     Output:
c       + Nnode: number of nodes
c       + Ncell: number of tetrahedric cells
c       + the required "grid_file" file
c     
c     I/O
      integer dim
      integer Ntheta,Nphi,Nz
      double precision grid_theta(0:Nlat_mx)
      double precision grid_phi(0:Nlon_mx)
      double precision grid_z(0:Nlayer_mx)
      character*(Nchar_mx) grid_file
      integer*8 Nnode,Ncell
c     temp
      integer Nnode_sl,Ncell_sl
      integer itheta,iphi,iz,i,j
      double precision spherical_coordinates(1:Ndim_mx)
      double precision cartesian_coordinates(1:Ndim_mx)
      integer*8 inode,icell
      integer Nnode_in_box
      integer*8 index(1:24)
      double precision node_coord(1:24,1:Ndim_mx)
      double precision xnode(1:24,1:Ndim_mx)
      integer*8 cell_idx(1:Nvincell)
      character*(Nchar_mx) node_coord_file,cell_idx_file,xnode_file
c     label
      character*(Nchar_mx) label
      label='subroutine volumic_grid2'
      
      if (Ntheta.lt.2) then
         call error(label)
         write(*,*) 'Ntheta=',Ntheta
         write(*,*) 'should be greater than 1'
         stop
      endif
      if (Nphi.lt.1) then
         call error(label)
         write(*,*) 'Nphi=',Nphi
         write(*,*) 'should be greater than 0'
         stop
      endif
      if (Nz.lt.1) then
         call error(label)
         write(*,*) 'Nz=',Nz
         write(*,*) 'should be greater than 0'
         stop
      endif

      write(*,*) 'Producing volumic grid'
      Ncell_sl=6*Nphi*(Ntheta-1) ! number of cells per single vertical layer
      Nnode_sl=Nvincell*Ncell_sl ! number of nodes per single vertical layer
      Nnode=Nz*Nnode_sl          !  total number of nodes
      Ncell=Nz*Ncell_sl          ! total number of cells
      node_coord_file='./node_coord.txt'
      cell_idx_file='./cell_idx.txt'
      xnode_file='./xnode.txt'
      open(11,file=trim(node_coord_file))
      open(12,file=trim(cell_idx_file))
      open(13,file=trim(xnode_file))
      write(11,*) Nnode
      write(12,*) Ncell
      write(13,*) Nnode
      inode=0
      icell=0
      do iz=1,Nz
         do itheta=1,Ntheta
            do iphi=1,Nphi
               if (itheta.eq.1) then ! latitude interval in contact with the south pole
c     Number of nodes in lon/lat/alt box
                  Nnode_in_box=6
c     -----------------------------------------------------------------
c     Node coordinates:
c     + node 1
                  inode=inode+1
                  index(1)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 0,iphi-1,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(1,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(1,1)=spherical_coordinates(3)
                  xnode(1,2)=spherical_coordinates(2)
                  xnode(1,3)=spherical_coordinates(1)-grid_z(0)
c     + node 2
                  inode=inode+1
                  index(2)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 1,iphi-1,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(2,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(2,1)=spherical_coordinates(3)
                  xnode(2,2)=spherical_coordinates(2)
                  xnode(2,3)=spherical_coordinates(1)-grid_z(0)
c     + node 3
                  inode=inode+1
                  index(3)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 1,iphi,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(3,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(3,1)=spherical_coordinates(3)
                  xnode(3,2)=spherical_coordinates(2)
                  xnode(3,3)=spherical_coordinates(1)-grid_z(0)
c     + node 4
                  inode=inode+1
                  index(4)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 0,iphi-1,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(4,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(4,1)=spherical_coordinates(3)
                  xnode(4,2)=spherical_coordinates(2)
                  xnode(4,3)=spherical_coordinates(1)-grid_z(0)
c     + node 5
                  inode=inode+1
                  index(5)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 1,iphi-1,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(5,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(5,1)=spherical_coordinates(3)
                  xnode(5,2)=spherical_coordinates(2)
                  xnode(5,3)=spherical_coordinates(1)-grid_z(0)
c     + node 6
                  inode=inode+1
                  index(6)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 1,iphi,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(6,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(6,1)=spherical_coordinates(3)
                  xnode(6,2)=spherical_coordinates(2)
                  xnode(6,3)=spherical_coordinates(1)-grid_z(0)
c     Additionnal nodes
c     + node 7
                  inode=inode+1
                  index(7)=inode
                  do j=1,dim
                     node_coord(7,j)=node_coord(2,j)
                     xnode(7,j)=xnode(2,j)
                  enddo         ! j
c     + node 8
                  inode=inode+1
                  index(8)=inode
                  do j=1,dim
                     node_coord(8,j)=node_coord(3,j)
                     xnode(8,j)=xnode(3,j)
                  enddo         ! j
c     + node 9
                  inode=inode+1
                  index(9)=inode
                  do j=1,dim
                     node_coord(9,j)=node_coord(4,j)
                     xnode(9,j)=xnode(4,j)
                  enddo         ! j
c     + node 10
                  inode=inode+1
                  index(10)=inode
                  do j=1,dim
                     node_coord(10,j)=node_coord(2,j)
                     xnode(10,j)=xnode(2,j)
                  enddo         ! j
c     + node 11
                  inode=inode+1
                  index(11)=inode
                  do j=1,dim
                     node_coord(11,j)=node_coord(6,j)
                     xnode(11,j)=xnode(6,j)
                  enddo         ! j
c     + node 12
                  inode=inode+1
                  index(12)=inode
                  do j=1,dim
                     node_coord(12,j)=node_coord(4,j)
                     xnode(12,j)=xnode(4,j)
                  enddo         ! j
                  do i=1,12
                     write(11,*) (node_coord(i,j),j=1,dim)
                     write(13,*) (xnode(i,j),j=1,dim)
                  enddo         ! j
c     -----------------------------------------------------------------
c     Cells: affect node indexes
c     cell 1
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(3)
                  cell_idx(3)=index(2)
                  cell_idx(4)=index(4)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 2
                  icell=icell+1
                  cell_idx(1)=index(7)
                  cell_idx(2)=index(8)
                  cell_idx(3)=index(6)
                  cell_idx(4)=index(9)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 3
                  icell=icell+1
                  cell_idx(1)=index(10)
                  cell_idx(2)=index(11)
                  cell_idx(3)=index(5)
                  cell_idx(4)=index(12)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
               else if (itheta.eq.Ntheta) then ! latitude interval in contact with the north pole
c     Number of nodes in lon/lat/alt box
                  Nnode_in_box=6
c     -----------------------------------------------------------------
c     Node coordinates:
c     + node 1
                  inode=inode+1
                  index(1)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 Ntheta-1,iphi-1,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(1,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(1,1)=spherical_coordinates(3)
                  xnode(1,2)=spherical_coordinates(2)
                  xnode(1,3)=spherical_coordinates(1)-grid_z(0)
c     + node 2
                  inode=inode+1
                  index(2)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 Ntheta,iphi,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(2,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(2,1)=spherical_coordinates(3)
                  xnode(2,2)=spherical_coordinates(2)
                  xnode(2,3)=spherical_coordinates(1)-grid_z(0)
c     + node 3
                  inode=inode+1
                  index(3)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 Ntheta-1,iphi,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(3,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(3,1)=spherical_coordinates(3)
                  xnode(3,2)=spherical_coordinates(2)
                  xnode(3,3)=spherical_coordinates(1)-grid_z(0)
c     + node 4
                  inode=inode+1
                  index(4)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 Ntheta-1,iphi-1,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(4,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(4,1)=spherical_coordinates(3)
                  xnode(4,2)=spherical_coordinates(2)
                  xnode(4,3)=spherical_coordinates(1)-grid_z(0)
c     + node 5
                  inode=inode+1
                  index(5)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 Ntheta,iphi,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(5,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(5,1)=spherical_coordinates(3)
                  xnode(5,2)=spherical_coordinates(2)
                  xnode(5,3)=spherical_coordinates(1)-grid_z(0)
c     + node 6
                  inode=inode+1
                  index(6)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 Ntheta-1,iphi,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(6,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(6,1)=spherical_coordinates(3)
                  xnode(6,2)=spherical_coordinates(2)
                  xnode(6,3)=spherical_coordinates(1)-grid_z(0)
c     Additionnal nodes
c     + node 7
                  inode=inode+1
                  index(7)=inode
                  do j=1,dim
                     node_coord(7,j)=node_coord(1,j)
                     xnode(7,j)=xnode(1,j)
                  enddo         ! j
c     + node 8
                  inode=inode+1
                  index(8)=inode
                  do j=1,dim
                     node_coord(8,j)=node_coord(3,j)
                     xnode(8,j)=xnode(3,j)
                  enddo         ! j
c     + node 9
                  inode=inode+1
                  index(9)=inode
                  do j=1,dim
                     node_coord(9,j)=node_coord(5,j)
                     xnode(9,j)=xnode(5,j)
                  enddo         ! j
c     + node 10
                  inode=inode+1
                  index(10)=inode
                  do j=1,dim
                     node_coord(10,j)=node_coord(1,j)
                     xnode(10,j)=xnode(1,j)
                  enddo         ! j
c     + node 11
                  inode=inode+1
                  index(11)=inode
                  do j=1,dim
                     node_coord(11,j)=node_coord(6,j)
                     xnode(11,j)=xnode(6,j)
                  enddo         ! j
c     + node 12
                  inode=inode+1
                  index(12)=inode
                  do j=1,dim
                     node_coord(12,j)=node_coord(5,j)
                     xnode(12,j)=xnode(5,j)
                  enddo         ! j
                  do i=1,12
                     write(11,*) (node_coord(i,j),j=1,dim)
                     write(13,*) (xnode(i,j),j=1,dim)
                  enddo         ! j
c     -----------------------------------------------------------------
c     Cells: affect node indexes
c     cell 1
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(3)
                  cell_idx(3)=index(2)
                  cell_idx(4)=index(5)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 2
                  icell=icell+1
                  cell_idx(1)=index(7)
                  cell_idx(2)=index(6)
                  cell_idx(3)=index(8)
                  cell_idx(4)=index(9)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 3
                  icell=icell+1
                  cell_idx(1)=index(10)
                  cell_idx(2)=index(4)
                  cell_idx(3)=index(11)
                  cell_idx(4)=index(12)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
               else             ! all other latitude intervalls
c     Number of nodes in lon/lat/alt box
                  Nnode_in_box=8
c     -----------------------------------------------------------------
c     Node coordinates:
c     + node 1
                  inode=inode+1
                  index(1)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 itheta-1,iphi-1,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(1,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(1,1)=spherical_coordinates(3)
                  xnode(1,2)=spherical_coordinates(2)
                  xnode(1,3)=spherical_coordinates(1)-grid_z(0)
c     + node 2
                  inode=inode+1
                  index(2)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 itheta,iphi-1,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(2,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(2,1)=spherical_coordinates(3)
                  xnode(2,2)=spherical_coordinates(2)
                  xnode(2,3)=spherical_coordinates(1)-grid_z(0)
c     + node 3
                  inode=inode+1
                  index(3)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 itheta-1,iphi,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(3,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(3,1)=spherical_coordinates(3)
                  xnode(3,2)=spherical_coordinates(2)
                  xnode(3,3)=spherical_coordinates(1)-grid_z(0)
c     + node 4
                  inode=inode+1
                  index(4)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 itheta,iphi,iz-1,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(4,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(4,1)=spherical_coordinates(3)
                  xnode(4,2)=spherical_coordinates(2)
                  xnode(4,3)=spherical_coordinates(1)-grid_z(0)
c     + node 5
                  inode=inode+1
                  index(5)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 itheta-1,iphi-1,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(5,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(5,1)=spherical_coordinates(3)
                  xnode(5,2)=spherical_coordinates(2)
                  xnode(5,3)=spherical_coordinates(1)-grid_z(0)
c     + node 6
                  inode=inode+1
                  index(6)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 itheta,iphi-1,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(6,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(6,1)=spherical_coordinates(3)
                  xnode(6,2)=spherical_coordinates(2)
                  xnode(6,3)=spherical_coordinates(1)-grid_z(0)
c     + node 7
                  inode=inode+1
                  index(7)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 itheta-1,iphi,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(7,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(7,1)=spherical_coordinates(3)
                  xnode(7,2)=spherical_coordinates(2)
                  xnode(7,3)=spherical_coordinates(1)-grid_z(0)
c     + node 8
                  inode=inode+1
                  index(8)=inode
                  call node_coordinates(Ntheta,Nphi,Nz,
     &                 grid_theta,grid_phi,grid_z,
     &                 itheta,iphi,iz,
     &                 spherical_coordinates,cartesian_coordinates)
                  do j=1,dim
                     node_coord(8,j)=cartesian_coordinates(j)
                  enddo         ! j
                  xnode(8,1)=spherical_coordinates(3)
                  xnode(8,2)=spherical_coordinates(2)
                  xnode(8,3)=spherical_coordinates(1)-grid_z(0)
c     Additionnal nodes
c     + node 9
                  inode=inode+1
                  index(9)=inode
                  do j=1,dim
                     node_coord(9,j)=node_coord(1,j)
                     xnode(9,j)=xnode(1,j)
                  enddo         ! j
c     + node 10
                  inode=inode+1
                  index(10)=inode
                  do j=1,dim
                     node_coord(10,j)=node_coord(7,j)
                     xnode(10,j)=xnode(7,j)
                  enddo         ! j
c     + node 11
                  inode=inode+1
                  index(11)=inode
                  do j=1,dim
                     node_coord(11,j)=node_coord(4,j)
                     xnode(11,j)=xnode(4,j)
                  enddo         ! j
c     + node 12
                  inode=inode+1
                  index(12)=inode
                  do j=1,dim
                     node_coord(12,j)=node_coord(1,j)
                     xnode(12,j)=xnode(1,j)
                  enddo         ! j
c     + node 13
                  inode=inode+1
                  index(13)=inode
                  do j=1,dim
                     node_coord(13,j)=node_coord(8,j)
                     xnode(13,j)=xnode(8,j)
                  enddo         ! j
c     + node 14
                  inode=inode+1
                  index(14)=inode
                  do j=1,dim
                     node_coord(14,j)=node_coord(4,j)
                     xnode(14,j)=xnode(4,j)
                  enddo         ! j
c     + node 15
                  inode=inode+1
                  index(15)=inode
                  do j=1,dim
                     node_coord(15,j)=node_coord(1,j)
                     xnode(15,j)=xnode(1,j)
                  enddo         ! j
c     + node 16
                  inode=inode+1
                  index(16)=inode
                  do j=1,dim
                     node_coord(16,j)=node_coord(7,j)
                     xnode(16,j)=xnode(7,j)
                  enddo         ! j
c     + node 17
                  inode=inode+1
                  index(17)=inode
                  do j=1,dim
                     node_coord(17,j)=node_coord(1,j)
                     xnode(17,j)=xnode(1,j)
                  enddo         ! j
c     + node 18
                  inode=inode+1
                  index(18)=inode
                  do j=1,dim
                     node_coord(18,j)=node_coord(7,j)
                     xnode(18,j)=xnode(7,j)
                  enddo         ! j
c     + node 19
                  inode=inode+1
                  index(19)=inode
                  do j=1,dim
                     node_coord(19,j)=node_coord(8,j)
                     xnode(19,j)=xnode(8,j)
                  enddo         ! j
c     + node 20
                  inode=inode+1
                  index(20)=inode
                  do j=1,dim
                     node_coord(20,j)=node_coord(6,j)
                     xnode(20,j)=xnode(6,j)
                  enddo         ! j
c     + node 21
                  inode=inode+1
                  index(21)=inode
                  do j=1,dim
                     node_coord(21,j)=node_coord(1,j)
                     xnode(21,j)=xnode(1,j)
                  enddo         ! j
c     + node 22
                  inode=inode+1
                  index(22)=inode
                  do j=1,dim
                     node_coord(22,j)=node_coord(8,j)
                     xnode(22,j)=xnode(8,j)
                  enddo         ! j
c     + node 23
                  inode=inode+1
                  index(23)=inode
                  do j=1,dim
                     node_coord(23,j)=node_coord(2,j)
                     xnode(23,j)=xnode(2,j)
                  enddo         ! j
c     + node 24
                  inode=inode+1
                  index(24)=inode
                  do j=1,dim
                     node_coord(24,j)=node_coord(6,j)
                     xnode(24,j)=xnode(6,j)
                  enddo         ! j
                  do i=1,24
                     write(11,*) (node_coord(i,j),j=1,dim)
                     write(13,*) (xnode(i,j),j=1,dim)
                  enddo         ! j
c     -----------------------------------------------------------------
c     Cells: affect node indexes
c     cell 1
                  icell=icell+1
                  cell_idx(1)=index(1)
                  cell_idx(2)=index(7)
                  cell_idx(3)=index(3)
                  cell_idx(4)=index(4)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 2
                  icell=icell+1
                  cell_idx(1)=index(9)
                  cell_idx(2)=index(8)
                  cell_idx(3)=index(10)
                  cell_idx(4)=index(11)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 3
                  icell=icell+1
                  cell_idx(1)=index(12)
                  cell_idx(2)=index(2)
                  cell_idx(3)=index(13)
                  cell_idx(4)=index(14)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 4
                  icell=icell+1
                  cell_idx(1)=index(15)
                  cell_idx(2)=index(5)
                  cell_idx(3)=index(16)
                  cell_idx(4)=index(6)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 5
                  icell=icell+1
                  cell_idx(1)=index(17)
                  cell_idx(2)=index(18)
                  cell_idx(3)=index(19)
                  cell_idx(4)=index(20)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
c     cell 6
                  icell=icell+1
                  cell_idx(1)=index(21)
                  cell_idx(2)=index(22)
                  cell_idx(3)=index(23)
                  cell_idx(4)=index(24)
                  write(12,*) (cell_idx(j),j=1,Nvincell)
               endif
            enddo               ! iphi
         enddo                  ! itheta
      enddo                     ! iz
      close(11)
      close(12)
      close(13)
c     
      call record_volumic_grid_file(dim,Nnode,Ncell,grid_file)
c     Debug
      write(*,*) 'Ncell=',Ncell
c     Debug

      return
      end
      
      

      subroutine node_indexes(Ntheta,Nphi,Nz,
     &     itheta,iphi,iz,
     &     Nnode,index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the number and indexes of
c     the nodes that delimit the elementary grid cell of interest
c     
c     Input:
c       + Ntheta: number of latitude cells
c       + Nphi: number of longitude cells
c       + Nz: number of altitude cells
c       + itheta: latitude cell index (1-Ntheta)
c       + iphi: longitude cell index (1-Nphi)
c       + iz: altitude cell index (1-Nz)
c     
c     Output:
c       + Nnode: number of nodes that delimit the cell
c       + index: absolute indexes of these Nnode nodes (warning: integer*8)
c     
c     I/O
      integer Ntheta,Nphi,Nz
      integer itheta,iphi,iz
      integer Nnode
      integer*8 index(1:8)
c     temp
      integer surf_theta_idx
      integer surf_phi_idx
      integer surf_z_idx
c     Debug
      integer j
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine node_indexes'

      if (itheta.lt.1) then
         call error(label)
         write(*,*) 'itheta=',itheta
         write(*,*) 'should be >= 1'
         stop
      endif
      if (itheta.gt.Ntheta) then
         call error(label)
         write(*,*) 'itheta=',itheta
         write(*,*) 'should be <= Ntheta=',Ntheta
         stop
      endif
      if (iphi.lt.1) then
         call error(label)
         write(*,*) 'iphi=',iphi
         write(*,*) 'should be >= 1'
         stop
      endif
      if (iphi.gt.Nphi) then
         call error(label)
         write(*,*) 'iphi=',iphi
         write(*,*) 'should be <= Nphi=',Nphi
         stop
      endif
      if (iz.lt.1) then
         call error(label)
         write(*,*) 'iz=',iz
         write(*,*) 'should be >= 1'
         stop
      endif
      if (iz.gt.Nz) then
         call error(label)
         write(*,*) 'iz=',iz
         write(*,*) 'should be <= Nz=',Nz
         stop
      endif
      
      if (itheta.eq.1) then
         Nnode=6
c     Node 1: lower theta, lower z
         surf_theta_idx=itheta-1
         surf_phi_idx=0
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(1))
c     Node 2: higher theta, lower phi, lower z
         surf_theta_idx=itheta
         surf_phi_idx=iphi-1
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(2))
c     Node 3: higher theta, higher phi, lower z
         surf_theta_idx=itheta
         if (iphi.eq.Nphi) then
            surf_phi_idx=0
         else
            surf_phi_idx=iphi
         endif
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(3))
c     Node 4: lower theta, higher z
         surf_theta_idx=itheta-1
         surf_phi_idx=0
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(4))
c     Node 5: higher theta, lower phi, higher z
         surf_theta_idx=itheta
         surf_phi_idx=iphi-1
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(5))
c     Node 6: higher theta, higher phi, higher z
         surf_theta_idx=itheta
         if (iphi.eq.Nphi) then
            surf_phi_idx=0
         else
            surf_phi_idx=iphi
         endif
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(6))
c     Debug
c         if (iphi.eq.Nphi) then
c            write(*,*) (index(j),j=1,6)
c            stop
c         endif
c     Debug
      else if (itheta.eq.Ntheta) then
         Nnode=6
c     Node 1: lower theta, lower phi, lower z
         surf_theta_idx=itheta-1
         surf_phi_idx=iphi-1
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(1))
c     Node 2: higher theta, lower z
         surf_theta_idx=itheta
         surf_phi_idx=0
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(2))
c     Node 3: lower theta, higher phi, lower z
         surf_theta_idx=itheta-1
         if (iphi.eq.Nphi) then
            surf_phi_idx=0
         else
            surf_phi_idx=iphi
         endif
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(3))
c     Node 4: lower theta, lower phi, higher z
         surf_theta_idx=itheta-1
         surf_phi_idx=iphi-1
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(4))
c     Node 5: higher theta, higher z
         surf_theta_idx=itheta
         surf_phi_idx=0
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(5))
c     Node 6: lower theta, higher phi, higher z
         surf_theta_idx=itheta-1
         if (iphi.eq.Nphi) then
            surf_phi_idx=0
         else
            surf_phi_idx=iphi
         endif
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(6))
      else
         Nnode=8
c     Node 1: lower theta, lower phi, lower z
         surf_theta_idx=itheta-1
         surf_phi_idx=iphi-1
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(1))
c     Node 2: higher theta, lower phi, lower z
         surf_theta_idx=itheta
         surf_phi_idx=iphi-1
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(2))
c     Node 3: lower theta, higher phi, lower z
         surf_theta_idx=itheta-1
         if (iphi.eq.Nphi) then
            surf_phi_idx=0
         else
            surf_phi_idx=iphi
         endif
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(3))
c     Node 4: higher theta, higher phi, lower z
         surf_theta_idx=itheta
         if (iphi.eq.Nphi) then
            surf_phi_idx=0
         else
            surf_phi_idx=iphi
         endif
         surf_z_idx=iz-1
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(4))
c     Node 5: lower theta, lower phi, higher z
         surf_theta_idx=itheta-1
         surf_phi_idx=iphi-1
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(5))
c     Node 6: higer theta, lower phi, higher z
         surf_theta_idx=itheta
         surf_phi_idx=iphi-1
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(6))
c     Node 7: lower theta, higher phi, higher z
         surf_theta_idx=itheta-1
         if (iphi.eq.Nphi) then
            surf_phi_idx=0
         else
            surf_phi_idx=iphi
         endif
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(7))
c     Node 8: higher theta, higher phi, higher z
         surf_theta_idx=itheta
         if (iphi.eq.Nphi) then
            surf_phi_idx=0
         else
            surf_phi_idx=iphi
         endif
         surf_z_idx=iz
         call absolute_index(Ntheta,Nphi,Nz,
     &        surf_theta_idx,surf_phi_idx,surf_z_idx,
     &        index(8))
      endif
      
      return
      end



      subroutine absolute_index(Ntheta,Nphi,Nz,
     &     surf_theta_idx,surf_phi_idx,surf_z_idx,
     &     abs_idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the absolute index of a node identified by surface indexes
c     
c     Input:
c       + Ntheta: number of latitude cells
c       + Nphi: number of longitude cells
c       + Nz: number of altitude cells
c       + surf_theta_idx: index of latitude surface (0-Ntheta)
c       + surf_phi_idx: index of longitude surface (0-Nphi-1)
c       + surf_z_idx: index of altitude surface (0-Nz)
c     
c     Output:
c       + abs_idx: absolute index of the node (Warning: integer*8)
c     
c     I/O
      integer Ntheta,Nphi,Nz
      integer surf_theta_idx
      integer surf_phi_idx
      integer surf_z_idx
      integer*8 abs_idx
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine absolute_index'

      if (surf_theta_idx.lt.0) then
         call error(label)
         write(*,*) 'surf_theta_idx=',surf_theta_idx
         write(*,*) 'should be positive'
         stop
      endif
      if (surf_theta_idx.gt.Ntheta) then
         call error(label)
         write(*,*) 'surf_theta_idx=',surf_theta_idx
         write(*,*) 'should be <= Ntheta=',Ntheta
         stop
      endif
      if (surf_phi_idx.lt.0) then
         call error(label)
         write(*,*) 'surf_phi_idx=',surf_phi_idx
         write(*,*) 'should be positive'
         stop
      endif
      if (surf_phi_idx.ge.Nphi) then
         call error(label)
         write(*,*) 'surf_phi_idx=',surf_phi_idx
         write(*,*) 'should be < Nphi=',Nphi
         stop
      endif
      if (surf_z_idx.lt.0) then
         call error(label)
         write(*,*) 'surf_iz_idx=',surf_z_idx
         write(*,*) 'should be positive'
         stop
      endif
      if (surf_z_idx.gt.Nz) then
         call error(label)
         write(*,*) 'surf_iz_idx=',surf_z_idx
         write(*,*) 'should be <= Nz=',Nz
         stop
      endif

c     Absolute index for surf_z_idx=0
      if (surf_theta_idx.eq.0) then
         abs_idx=1
      else if (surf_theta_idx.eq.Ntheta) then
         abs_idx=Nphi*(Ntheta-1)+2
      else
         abs_idx=2+Nphi*(surf_theta_idx-1)+surf_phi_idx
      endif
c     Absolute index for actual surf_z_idx
      abs_idx=abs_idx+(Nphi*(Ntheta-1)+2)*surf_z_idx
      
      return
      end
      


      subroutine node_coordinates(Ntheta,Nphi,Nz,
     &     grid_theta,grid_phi,grid_z,
     &     surf_theta_idx,surf_phi_idx,surf_z_idx,
     &     spherical_coordinates,cartesian_coordinates)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to retrive the coordinates for a single grid node
c     
c     Input:
c       + Ntheta: number of latitude cells
c       + Nphi: number of longitude cells
c       + Nz: number of altitude cells
c       + grid_theta: latitude grid in a spherical coordinates system
c         where latitude varies between -90 degrees (south pole)
c         to 90 degrees (north pole)
c         - grid_theta(0)=-90 [degrees]
c         - grid_theta(i)=-90+180/Ntheta*i end of cell index i [degrees]
c         - grid_theta(Ntheta)=390 [degrees]
c       + grid_phi: longitude grid in a spherical coordinates system
c         where longitude varies between 0 and 360 degrees
c         - grid_phi(0)=0 [degrees]
c         - grid_phi(i)=360/Nphi*i end of cell index i [degrees]
c         - grid_phi(Nphi=360 [degrees]
c       + grid_z: 1D altitude grid
c         - grid_z(0)=zmin [m]
c         - grid_z(i)=zmin+(zmax-zmin)/Nz*i top of cell index i [m]
c         - grid_z(Nz)=zmax [m]
c       + surf_theta_idx: index of latitude surface (0-Ntheta)
c       + surf_phi_idx: index of longitude surface (0-Nphi-1)
c       + surf_z_idx: index of altitude surface (0-Nz)
c     
c     Output:
c       + spherical_coordinates: (r,theta,phi) in a spherical referential
c       centered on the planet center [m,deg,deg]
c       + cartesian_coordinates:(x,y,z) in a cartesian referential
c       centered on the planet center [m,m,m]
c     
c     I/O
      integer Ntheta,Nphi,Nz
      double precision grid_theta(0:Nlat_mx)
      double precision grid_phi(0:Nlon_mx)
      double precision grid_z(0:Nlayer_mx)
      integer surf_theta_idx,surf_phi_idx,surf_z_idx
      double precision spherical_coordinates(1:Ndim_mx)
      double precision cartesian_coordinates(1:Ndim_mx)
c     temp
      double precision r,theta,phi
      integer phi_idx
c     label
      character*(Nchar_mx) label
      label='subroutine node_coordinates'

      if (surf_theta_idx.lt.0) then
         call error(label)
         write(*,*) 'surf_theta_idx=',surf_theta_idx
         write(*,*) 'should be positive'
         stop
      endif
      if (surf_theta_idx.gt.Ntheta) then
         call error(label)
         write(*,*) 'surf_theta_idx=',surf_theta_idx
         write(*,*) 'should be <= Ntheta=',Ntheta
         stop
      endif
      if (surf_phi_idx.lt.0) then
         call error(label)
         write(*,*) 'surf_phi_idx=',surf_phi_idx
         write(*,*) 'should be positive'
         stop
      endif
      if (surf_phi_idx.gt.Nphi) then
         call error(label)
         write(*,*) 'surf_phi_idx=',surf_phi_idx
         write(*,*) 'should be < Nphi=',Nphi
         stop
      endif
      if (surf_phi_idx.eq.Nphi) then
         phi_idx=0
      else
         phi_idx=surf_phi_idx
      endif
      if (surf_z_idx.lt.0) then
         call error(label)
         write(*,*) 'surf_z_idx=',surf_z_idx
         write(*,*) 'should be positive'
         stop
      endif
      if (surf_z_idx.gt.Nz) then
         call error(label)
         write(*,*) 'surf_z_idx=',surf_z_idx
         write(*,*) 'should be <= Nz=',Nz
         stop
      endif

      r=grid_z(surf_z_idx)
      theta=grid_theta(surf_theta_idx)
      phi=grid_phi(phi_idx)
c     r,theta,phi
      spherical_coordinates(1)=r ! m
      spherical_coordinates(2)=theta ! deg
      spherical_coordinates(3)=phi ! deg
c     deg -> rad
      theta=theta*pi/180.0D+0
      phi=phi*pi/180.0D+0
c     x,y,z
      cartesian_coordinates(1)=r*dcos(theta)*dcos(phi)
      cartesian_coordinates(2)=r*dcos(theta)*dsin(phi)
      cartesian_coordinates(3)=r*dsin(theta)
      
      return
      end

      

      subroutine generate_homogeneous_latitude_grid(Ncell,grid)
      implicit none
      include 'max.inc'
c
c     Purpose: to produce a homogeneous latitude grid
c
c     Input:
c       + Ncell: number of latitude cells
c
c     Output:
c       + grid: latitude grid in a spherical coordinates system
c         where latitude varies between -90 degrees (south pole)
c         to 90 degrees (north pole)
c         - grid(0)=-90 [degrees]
c         - grid(i)=-90+180/Ncell*i end of cell index i [degrees]
c         - grid(Ncell)=390 [degrees]
c
c     I/O
      integer Ncell
      double precision grid(0:Nlat_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine generate_homogeneous_latitude_grid'

      if (Ncell.le.0) then
         call error(label)
         write(*,*) 'Ncell=',Ncell
         write(*,*) 'should be positive'
         stop
      endif

      do i=0,Ncell
         grid(i)=180.0D+0*dble(i)/dble(Ncell)-90.0D+0
      enddo                     ! i

      return
      end


      
      subroutine generate_homogeneous_longitude_grid(Ncell,grid)
      implicit none
      include 'max.inc'
c
c     Purpose: to produce a homogeneous longitude grid
c
c     Input:
c       + Ncell: number of longitude cells
c
c     Output:
c       + grid: longitude grid in a spherical coordinates system
c         where longitude varies between 0 and 360 degrees
c         - grid(0)=0 [degrees]
c         - grid(i)=360/Ncell*i end of cell index i [degrees]
c         - grid(Ncell)=360 [degrees]
c
c     I/O
      integer Ncell
      double precision grid(0:Nlon_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine generate_homogeneous_longitude_grid'

      if (Ncell.le.0) then
         call error(label)
         write(*,*) 'Ncell=',Ncell
         write(*,*) 'should be positive'
         stop
      endif

      do i=0,Ncell
         grid(i)=360.0D+0*dble(i)/dble(Ncell)
      enddo                     ! i

      return
      end



      subroutine generate_homogeneous_cartesian_grid(xmin,xmax,Ncell,
     &     grid)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce a homogeneous grid in a 1D cartesian space
c     
c     Input:
c       + xmin: lower value of x [m]
c       + xmax: higher value of x [m]
c       + Ncell: number of cells
c     
c     Output:
c       + grid: 1D grid
c         - grid(0)=xmin [m]
c         - grid(i)=xmin+(xmax-xmin)/Ncell*i end of cell index i [m]
c         - grid(Ncell)=xmax [m]
c
c     I/O
      double precision xmin
      double precision xmax
      integer Ncell
      double precision grid(0:Nlayer_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine generate_homogeneous_cartesian_grid'

      if (Ncell.le.0) then
         call error(label)
         write(*,*) 'Ncell=',Ncell
         write(*,*) 'should be positive'
         stop
      endif
      
      do i=0,Ncell
         grid(i)=xmin+(xmax-xmin)*dble(i)/dble(Ncell)
      enddo                     ! i

      return
      end
