      subroutine record_tom_datafile(datafile,dim,Nlayer,z_grid,surface_temperature,
     &     Nlambda_reflectivity,lambda_reflectivity,reflectivity,
     &     Tgas,
     &     Nband,Nq,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,Naerosol,ka_aerosol,ks_aerosol,g_aerosol)
      implicit none
      include 'max.inc'
c     
c     Purpose: to record The Oignon Model input data file
c     
c     Input:
c       + datafile: file to record
c       + dim: dimension of space
c       + Nlayer: number of vertical layers
c       + z_grid: vertical grid [m]
c       + surface_temperature: surface temperature [K]
c       + Nlambda_reflectivity: number of wavelength points for the definition of the ground reflectivity
c       + lambda_reflectivity: values of the wavelength for the definition of the ground reflectivity
c       + reflectivity: values of the ground reflectivity
c       + Tgas: gas temperature [K]
c       + Nband: number of spectral intervals
c       + Nq: quadrature order, for each spectral interval
c       + lambda_min: lower wavelength of each interval [nm]
c       + lambda_max: higher wavelength of each interval [nm]
c       + w: gas quadrature weight
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + Naerosol: number of aerosol modes
c       + ka_aerosol: cloud absorption coefficient [m⁻¹]
c       + ks_aerosol: cloud scattering coefficient [m⁻¹]
c       + g_aerosol: cloud asymetry parameter [m⁻¹]
c     
c     Output:
c       + The required "datafile" file
c     
c     I/O
      character*(Nchar_mx) datafile
      integer dim
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision surface_temperature
      integer Nlambda_reflectivity
      double precision lambda_reflectivity(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision Tgas(1:Nlayer_mx)
      integer Nband
      integer Nq(1:Nband_mx)
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      integer Naerosol
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision g_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
c     temp
      integer ilayer,ilambda,iband,iquad,iaerosol
c     label
      character*(Nchar_mx) label
      label='subroutine record_tom_datafile'
      
      open(21,file=trim(datafile))
      write(*,*) 'Recording TOM input data file...'
c     Geometry
      write(21,*) dim
      write(21,*) Nlayer
      do ilayer=0,Nlayer
         write(21,*) z_grid(ilayer)
      enddo
c     Ground temperature
      write(21,*) surface_temperature
c     Ground reflectivity
      write(21,*) Nlambda_reflectivity
      do ilambda=1,Nlambda_reflectivity
         write(21,*) lambda_reflectivity(ilambda),reflectivity(ilambda)
      enddo                     ! ilambda
c     Medium temperature
      do ilayer=1,Nlayer
         write(21,*) Tgas(ilayer)
      enddo                     ! ilayer
c     Number of aerosol modes
      write(21,*) Naerosol
c     Spectral grid & radiative properties
      write(21,*) Nband
      do iband=1,Nband
         write(21,*) lambda_min(iband),lambda_max(iband)
         write(21,*) Nq(iband)
         do iquad=1,Nq(iband)
            write(21,*) w(iband,iquad)
            do ilayer=1,Nlayer
               write(21,*) ka_gas(ilayer,iband,iquad)
            enddo               ! ilayer
         enddo                  ! iquad
         do ilayer=1,Nlayer
            write(21,*) ks_gas(ilayer,iband)
         enddo                  ! ilayer
         do iaerosol=1,Naerosol
            do ilayer=1,Nlayer
               write(21,*) ka_aerosol(iaerosol,ilayer,iband)
            enddo               ! ilayer
            do ilayer=1,Nlayer
               write(21,*) ks_aerosol(iaerosol,ilayer,iband)
            enddo               ! ilayer
            do ilayer=1,Nlayer
               write(21,*) g_aerosol(iaerosol,ilayer,iband)
            enddo               ! ilayer
         enddo                  ! iaerosol
      enddo                     ! iband
      close(21)
      write(*,*) 'File was recorded: ',trim(datafile)

      return
      end
