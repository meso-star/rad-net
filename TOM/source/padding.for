      subroutine compute_padding1(page_size,record_size,Nrecord,padding)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the padding in order to align
c     a number of records (with a specified size) with the size of a page
c     
c     Input:
c       + page_size: page size [bytes]
c       + record_size: record size [bytes]
c       + Nrecord: number of records
c     
c     Output:
c       + padding: padding size [bytes]
c     
c     I/O
      integer page_size
      integer record_size
      integer*8 Nrecord
      integer padding
c     temp
      integer Npages
      integer*8 total_recorded,total_with_padding
c     label
      character*(Nchar_mx) label
      label='subroutine compute_padding1'
      
      total_recorded=record_size*Nrecord
      if (modulo(total_recorded,page_size).eq.0) then
         Npages=int(total_recorded/page_size)
      else
         Npages=int(total_recorded/page_size)+1
      endif
      total_with_padding=Npages*page_size
      padding=modulo(total_with_padding,total_recorded)
      
      if (padding.lt.0) then
         call error(label)
         write(*,*) 'padding=',padding
         write(*,*) 'record_size=',record_size
         write(*,*) 'Nrecord=',Nrecord
         write(*,*) 'Total bytes written:',total_recorded
         write(*,*) 'Number of pages:',Npages
         write(*,*) 'Total bytes in these pages:',total_with_padding
         stop
      endif

      return
      end


      subroutine compute_padding2(page_size,total_recorded,padding)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the padding in order to align
c     a number of records (with a specified size) with the size of a page
c     
c     Input:
c       + page_size: page size [bytes]
c       + total_recorded: total information already recorded [bytes]
c     
c     Output:
c       + padding: padding size [bytes]
c     
c     I/O
      integer page_size
      integer*8 total_recorded
      integer padding
c     temp
      integer Npages
      integer*8 total_with_padding
c     label
      character*(Nchar_mx) label
      label='subroutine compute_padding2'
      
      if (modulo(total_recorded,page_size).eq.0) then
         Npages=int(total_recorded/page_size)
      else
         Npages=int(total_recorded/page_size)+1
      endif
      total_with_padding=Npages*page_size
      padding=total_with_padding-total_recorded
      
      if (padding.lt.0) then
         call error(label)
         write(*,*) 'padding=',padding
         write(*,*) 'Total bytes written:',total_recorded
         write(*,*) 'Number of pages:',Npages
         write(*,*) 'Total bytes in these pages:',total_with_padding
         stop
      endif

      return
      end
