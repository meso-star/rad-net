      subroutine sample_cone_half_angle(theta_max,theta,phi)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to uniformly sample the solid angle corresponding to a cone
c     defined by its half-angle
c     
c     Input:
c       + theta_max: cone half-angle [rad]
c     
c     Output:
c       + theta: angle between the cone axis and the sampled direction
c       + phi: arbitrary sampled over [0-2pi]
c     
c     I/O
      double precision theta_max
      double precision theta
      double precision phi
c     temp
      double precision r1,r2
c     label
      character*(Nchar_mx) label
      label='subroutine sample_cone_half_angle'

      call random_gen(r1)
      call random_gen(r2)
      theta=dacos(1.0D+0-r1*(1.0D+0-dcos(theta_max)))
      phi=2.0D+0*pi*r2

      return
      end


      
      subroutine sample_cone_half_angle_general(theta_min,theta_max,theta,phi)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to uniformly sample the solid angle corresponding to a partial cone
c     defined by two half-angles
c     
c     Input:
c       + theta_min: cone loer half-angle [rad]
c       + theta_max: cone upper half-angle [rad]
c     
c     Output:
c       + theta: angle between the cone axis and the sampled direction
c       + phi: arbitrary sampled over [0-2pi]
c     
c     I/O
      double precision theta_min
      double precision theta_max
      double precision theta
      double precision phi
c     temp
      double precision r1,r2
c     label
      character*(Nchar_mx) label
      label='subroutine sample_cone_half_angle_general'

      call random_gen(r1)
      call random_gen(r2)
      theta=dacos(dcos(theta_min)+r1*(dcos(theta_max)-dcos(theta_min)))
      phi=2.0D+0*pi*r2

      return
      end


      
      subroutine sample_cone_solid_angle(omega,theta,phi)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to uniformly sample the solid angle corresponding to a cone
c     defined by its solid angle
c     
c     Input:
c       + omega: cone solid angle [sr]
c     
c     Output:
c       + theta: angle between the cone axis and the sampled direction
c       + phi: arbitrary sampled over [0-2pi]
c     
c     I/O
      double precision omega
      double precision theta
      double precision phi
c     temp
      double precision theta_max
c     label
      character*(Nchar_mx) label
      label='subroutine sample_cone_solid_angle'

      theta_max=dacos(1.0D+0-omega/(2.0D+0*pi))
      call sample_cone_half_angle(theta_max,theta,phi)

      return
      end


      
      subroutine sample_rectangular_solid_angle(hfov,vfov,u_Ro)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to uniformly sample the solid angle corresponding to a rectangle
c     
c     Input:
c       + hfov: horizontal FOV [rad]
c       + vfov: vertical FOV [rad]
c     
c     Output:
c       + u_Ro: sampled direction in observation referential
c     
c     I/O
      double precision hfov
      double precision vfov
      double precision u_Ro(1:Ndim_mx)
c     temp
      double precision theta_min,theta_max,theta
      double precision phi_min,phi_max,phi
      double precision r
c     label
      character*(Nchar_mx) label
      label='subroutine sample_rectangular_solid_angle'

      theta_min=-vfov/2.D+0
      theta_max=vfov/2.D+0
      phi_min=-hfov/2.0D+0
      phi_max=hfov/2.0D+0
c      
      call random_gen(r)
      theta=dasin(dsin(theta_max)-r*(dsin(theta_max)-dsin(theta_min)))
      call sample_uniform_double(phi_min,phi_max,phi)
c      
      u_Ro(1)=dcos(theta)*dcos(phi)
      u_Ro(2)=dcos(theta)*dsin(phi)
      u_Ro(3)=dsin(theta)
     
      return
      end

      

      subroutine sample_uniform_double(val1,val2,alpha)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample a value between "val1" and "val2", uniformally
c     
c     Input:
c       + val1: lower boundary
c       + val2: higher boundary
c
c     Output:
c       + alpha: random number in [val1, val2] range
c
c     I/O
      double precision val1,val2,alpha
c     temp
      double precision r
c     label
      character*(Nchar_mx) label
      label='subroutine sample_uniform_double'

      if (val2.lt.val1) then
         call error(label)
         write(*,*) 'val2=',val2
         write(*,*) '< val1=',val1
         stop
      else
         call random_gen(r)
c     Debug
c         r=0.50D+0
c     Debug
         alpha=val1+(val2-val1)*r
      endif

      return
      end

      

      subroutine sample_uniform_integer(N1,N2,i)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an integer between N1 and N2 (included)
c     on a random uniform basis
c
c     Input:
c       + N1: minimum value the integer can take
c       + N2: maximum value the integer can take
c
c     Output:
c       + i: integer value between N1 and N2
c
c     I/O
      integer N1,N2,i
c     temp
      double precision r
c     label
      character*(Nchar_mx) label
      label='subroutine sample_uniform_integer'

      call random_gen(r)
      i=N1+int((N2-N1+1)*r)

      return
      end

      

      subroutine sample_integer(N,cdf,i)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an integer according to a set of probabilities
c     This routine uses a dichotomy-based scheme in order to invert the cumulated probability function
c     This is supposed to speed things up for large values of N
c
c     Input:
c       + N: number of probabilities
c       + cdf: cumulated probabilities
c
c     Output:
c       + i: selected integer value
c
c     I/O
      integer N
      double precision cdf(0:Nproba_mx)
      integer i
c     temp
      double precision r
      integer idx,idx1,idx2
      logical keep_looking,i_found
      integer iter
c     parameters
      double precision epsilon_cdf
      parameter(epsilon_cdf=1.0D-6) ! max. error over the last value of the cdf
c     label
      character*(Nchar_mx) label
      label='subroutine sample_integer'

      if (N.gt.Nproba_mx) then
         call error(label)
         write(*,*) 'Number of elements in cdf=',N
         write(*,*) '> Nproba_mx=',Nproba_mx
         stop
      endif
      if (dabs(cdf(N)-1.0D+0).gt.epsilon_cdf) then
         call error(label)
         write(*,*) 'cdf(',N,')=',cdf(N)
         write(*,*) 'should be equal to 1'
         write(*,*) '|cdf(',N,')-1|=',dabs(cdf(N)-1.0D+0)
         write(*,*) '> epsilon_cdf=',epsilon_cdf
         stop
      endif

      call random_gen(r)
c     Debug
c      r=0.30D+0
c     Debug
      keep_looking=.true.
      i_found=.false.
      idx1=0
      idx2=N
      iter=0
      do while (keep_looking)
         iter=iter+1
         if (iter.gt.Niter_mx) then
            call error(label)
            write(*,*) 'iter=',iter
            write(*,*) '> Niter_mx=',Niter_mx
            write(*,*) 'r=',r
            do idx=0,N
               write(*,*) 'cdf(',idx,')=',cdf(idx)
            enddo               ! idx
            stop
         else
            idx=(idx1+idx2)/2
            if ((r.gt.cdf(idx)).and.(r.le.cdf(idx+1))) then
               keep_looking=.false.
               i=idx+1
               i_found=.true.
            else
               if (r.gt.cdf(idx)) then
                  idx1=idx
               else
                  idx2=idx
               endif
            endif               ! cdf(idx) < r < cdf(idx+1)
         endif                  ! iter > Niter_mx            
      enddo                     ! while (keep_looping)
      if (.not.i_found) then
         call error(label)
         write(*,*) 'cdf could not be inverted'
         write(*,*) 'idx=',idx
         write(*,*) 'r=',r
         write(*,*) 'cdf(',Nproba_mx,')=',cdf(Nproba_mx)
         stop
      endif
c     Debug
c      write(*,*) 'Interval=',i
c     Debug

      return
      end

      
      
      subroutine sample_emission_direction_on_surface(dim,normal,
     &     u,pdf_u)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an emission direction from a position on a surface
c     according to pdf(u)=|u.n|/pi
c
c     Input:
c       + dim: space dimension
c       + normal: normal to the surface @ emission position
c
c     Output:
c       + u: direction
c       + pdf_u: corresponding pdf
c       
c     I/O
      integer dim
      double precision normal(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision pdf_u
c     temp
      integer i
      double precision j1(1:Ndim_mx)
      double precision theta,phi
      double precision pdf_theta,pdf_phi
      double precision v(1:Ndim_mx)
      double precision product_normalj1
      double precision product_un
      double precision normal_spher(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine sample_emission_direction_on_surface'

      call vector_j1(dim,normal,j1)
      call sample_theta_surface(theta,pdf_theta)
      call sample_phi_uniform(phi,pdf_phi)

c     First rotation: vector "normal" is rotated by "theta" around "j1" -> vector "v"
      call rotation(dim,normal,theta,j1,v)
      call scalar_product(dim,normal,j1,product_normalj1)
      if (dabs(product_normalj1).gt.1.0D-8) then
         call error(label)
         write(*,*) 'normal=',(normal(i),i=1,dim)
         write(*,*) 'j1=',(j1(i),i=1,dim)
         write(*,*) '|normal.j1|=',product_normalj1
         write(*,*) 'should be null'
         call cart2spher(dim,normal,normal_spher)
         write(*,*) 'normal_spher=',(normal_spher(i),i=1,dim)
         stop
      endif
c     Second rotation: vector "v" is rotated by "phi" around "normal" -> vector "u"
      call rotation(dim,v,phi,normal,u)
c     compute pdf_u(u)
      pdf_u=pdf_theta*pdf_phi
c     double check
      call scalar_product(dim,normal,u,product_un)
      if (dabs(product_un-dcos(theta)).gt.1.0D-8) then
         call error(label)
         write(*,*) 'cos(',theta,')=',dcos(theta)
         write(*,*) '|u.n|=',product_un
         write(*,*) 'normal=',(normal(i),i=1,dim)
         write(*,*) 'j1=',(j1(i),i=1,dim)
         write(*,*) '|normal.j1|=',product_normalj1
         stop
      endif

      return
      end

      

      subroutine sample_theta_surface(theta,pdf_theta)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to sample an emission angle [0,pi/2] from a boundary
c     according to pdf(w)=|u.n|/pi
c
c     Output:
c       + theta: emission angle [0,pi/2] between the normal and a direction
c       + pdf_theta: probability density function corresponding to the chosen angle
c

c     output
      double precision theta
      double precision pdf_theta
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine sample_theta_surface'

      call random_gen(R)
      theta=dacos(dsqrt(R))
      pdf_theta=2.0D+0*dcos(theta)
c     Tests
      if ((theta.lt.0.0D+0).or.(theta.gt.pi/2.0D+0)) then
         call error(label)
         write(*,*) 'theta=',theta
         write(*,*) 'is not between 0 and pi/2'
         write(*,*) 'R=',R
         stop
      endif
c     Tests

      return
      end

      

      subroutine sample_phi_uniform(phi,pdf_phi)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to sample an emission angle (volumic emission) phi
c     uniformally in the [0,2*pi] range
c
c     Output:
c       + phi: value of phi in the [0,2*pi] range
c       + pdf_phi: associated probability density function
c
c     I/O
      double precision phi
      double precision pdf_phi
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine sample_phi_uniform'

      call random_gen(R)
      phi=2.0D+0*pi*R
      pdf_phi=1.0D+0/(2.0D+0*pi)

      return
      end

      

      subroutine sample_emission_direction_in_volume(dim,u,pdf_u)
      implicit none
      include 'max.inc'
c
c     Purpose: to samlpe a volumic emission direction (uniformally over 4*pi)
c
c     Input:
c       + dim: space dimension
c
c     Output:
c       + u: emission direction
c       + pdf_u: associated probability density function
c
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision pdf_u
c     temp
      double precision u_spherical(1:Ndim_mx)
      double precision pdf_theta
      double precision pdf_phi
      logical verif
c     label
      character*(Nchar_mx) label
      label='subroutine sample_emission_direction_in_volume'
      
      u_spherical(1)=1.0D+0
      call sample_theta_volume(u_spherical(2),pdf_theta)
      call sample_phi_uniform(u_spherical(3),pdf_phi)

      verif=.true.
      call spher2cart(dim,u_spherical,u)
      pdf_u=pdf_theta*pdf_phi

      return
      end


      
      subroutine sample_theta_volume(theta,pdf_theta)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an emission angle (volumic emission) theta
c     uniformally in the [-pi/2,pi/2] range
c
c     Output:
c       + theta: value of theta in the [-pi/2,pi/2] range
c       + pdf_theta: associated probability density function
c
c     I/O
      double precision theta
      double precision pdf_theta
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine sample_theta_volume'

      call random_gen(R)
      theta=dasin(2.0D+0*R-1.0D+0)
      pdf_theta=0.5D+0

      return
      end



      subroutine sample_scattering_angle(dim,
     &     generate_angle,
     &     pf,g_HG,norm_factor,
     &     alpha,phase_function)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample a scattering angle [0,pi] relative to a incoming direction
c
c     Input:
c       + dim: dimension of space
c       + generate_angle: if true, the this routine will generate a new scattering
c         angle ("alpha"). If false, this routine will take "alpha" as an input, and
c         generate the corresponding pdf ("phase_function").
c       + pf: option for choosing the phase function that must be used
c         - pf=0: use the Rayleigh phase function
c         - pf=1: use a isotropic phase function
c         - pf=2: use the Henyey-Greenstein phase function
c       + g_HG: Henyey-Greenstein phase-function asymetry factor
c       + norm_factor: normalization factor (so that the integral over 4pi sr is equal to 1)
c       + alpha: if "generate_angle" is false, alpha is an input
c     
c     Output:
c       + alpha: scattering angle [0,pi] if "generate_alpha" is true
c       + phase_function: value of the phase function for the selected angle
c
c     I/O
      integer dim
      logical generate_angle
      integer pf
      double precision g_HG
      double precision norm_factor
      double precision alpha
      double precision phase_function
c     temp
      double precision R
      integer i,angle,j,i0,i1
      double precision mu_icp(1:Nicp_mx)
      double precision icp_min,icp_max
      double precision dtheta
      double precision u,S,mu
      double precision sum
      logical interval_found
      integer interval
      double precision theta,pdf_theta,theta1,theta2
      double precision phi,pdf_phi
      double precision udir(1:Ndim_mx)
      double precision vdir(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine sample_scattering_angle'
      
      if (generate_angle) then
c     when generating the scattering angle is required, then compute "alpha" from "mu"
         call random_gen(R)
c     
         if (pf.eq.0) then      ! use the Rayleigh phase-function
            u=4.0D+0*R-2.0D+0
            S=(u+dsqrt(1.0D+0+u**2))**(1.0D+0/3.0D+0)
            mu=S-1.0D+0/S
c
         else if (pf.eq.1) then ! use the isotropic phase-function
            call sample_theta_volume(theta,pdf_theta)
            call sample_phi_uniform(phi,pdf_phi)
            udir(1)=dcos(theta)*dcos(phi)
            udir(2)=dcos(theta)*dsin(phi)
            udir(3)=dsin(theta)
            vdir(1)=1.0D+0
            vdir(2)=0.0D+0
            vdir(3)=0.0D+0
            call scalar_product(dim,udir,vdir,mu)
c     
         else if (pf.eq.2) then ! use the Henyey-Greenstein phase-function
            if ((g_HG.lt.-1.0D+0).or.(g_HG.gt.1.0D+0)) then
               call error(label)
               write(*,*) 'Bad value for input argument g_HG=',g_HG
               stop
            endif
            mu=2.D+0*R*(1.D+0+g_HG)**2*(g_HG*(R-1.D+0)+1.D+0)/
     &           (1.D+0-g_HG*(1.D+0-2.D+0*R))**2-1.D+0
c
         else
            call error(label)
            write(*,*) 'Bad input argument:'
            write(*,*) 'pf=',pf
            write(*,*) 'allowed values: {0,1,2}'
            stop
         endif                  ! pf
c
         alpha=dacos(mu)
      else                      ! generate_angle=F
c     otherwise we just want to know the value of "phase_function" for the provided
c     value of "alpha" -> "mu" has to be recomputed for the provided "alpha"
         mu=dcos(alpha)
      endif                     ! generate_angle
c
      call scattering_phase_function(pf,g_HG,norm_factor,
     &     mu,phase_function)

      return
      end



      subroutine sample_tau_limited(tau_max,tau,pdf_tau)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an optical depth in the [0,tau_max] range according to:
c     pdf(tau)=exp(-tau)/[1-exp(-tau_max)]
c
c     Input:
c       + tau_max: maximum optical depth
c
c     Output:
c       + tau: random optical depth chosen over the [0,tau_max] range according to pdf(tau)
c       + pdf_tau: value of pdf(tau)
c
c     I/O
      double precision tau_max
      double precision tau
      double precision pdf_tau
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine sample_tau_limited'

c     security
      if (tau_max.lt.0.0D+0) then
         call error(label)
         write(*,*) 'tau_max=',tau_max
         stop
      endif

c     special case tau_max=0
      if (tau_max.eq.0.0D+0) then
         tau=0.0D+0
         pdf_tau=1.0D+0
c     general case tau_max>0
      else
         call random_gen(R)
         tau=-dlog(1.0D+0-R*(1.0D+0-dexp(-tau_max)))
         pdf_tau=dexp(-tau)/(1.0D+0-dexp(-tau_max))
      endif

      return
      end



      subroutine sample_tau_unlimited(tau,pdf_tau)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample an optical depth in the [0,+infty) range according to:
c     pdf(tau)=exp(-tau)
c
c     Output:
c       + tau: random optical depth chosen over the [0,+infty) range according to pdf(tau)
c       + pdf_tau: value of pdf(tau)
c
c     I/O
      double precision tau
      double precision pdf_tau
c     temp
      double precision r
c     label
      character*(Nchar_mx) label
      label='subroutine sample_tau_unlimited'

      call random_gen(r)
      tau=-dlog(r)
      pdf_tau=dexp(-tau)

      return
      end



      subroutine sample_length_unlimited(k,length,pdf_l)
      use ieee_arithmetic
      implicit none
      include 'max.inc'
c
c     Purpose: to sample a length in the [0,+infty) range according to:
c     pdf(length)=k*exp(-k*length)
c
c     Input:
c       + k: optical coefficient (absorption, scattering or total extinction) (m^-1)
c
c     Output:
c       + length: random length chosen over the [0,+infty) range according to pdf(length)
c       + pdf_l: value of pdf(length)
c
c     I/O
      double precision k
      double precision length
      double precision pdf_l
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine sample_length_unlimited'

      if (k.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input k=',k
         stop
      else if (k.eq.0.0D+0) then
         length=ieee_value(length,ieee_positive_inf)
         pdf_l=0.0D+0
      else
         call random_gen(r)
         length=-1.0D+0/k*dlog(r)
         pdf_l=k*dexp(-k*length)
      endif

      return
      end
      
      

      subroutine build_lambda_pdf(Nlambda_source,source_intensity,lambda_inf,lambda_sup,
     &     N,lambda,intensity,pdf,cdf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to build the discrete pdf set for sampling a wavelength
c     
c     Input:
c       + Nlambda_source: number of wavelength/intensity values
c       + source_intensity: values of the wavelength [nm] and associated solar intensity [W/m²/sr/nm]
c       + lambda_inf: lower limit for spectral integration [µm]
c       + lambda_sup: lower limit for spectral integration [µm]
c     
c     Output:
c       + N: size of pdf and cdf arrays (number of spectral intervals)
c       + lambda: min and max wavelength per interval [nm]
c       + intensity: solar intensity at both ends of each interval [W/m²/sr/nm]
c       + pdf: discrete pdf dataset: probability to sample each interval
c       + cdf: cumulated discrete density dataset
c     
c     I/O
      integer*8 Nlambda_source
      double precision source_intensity(1:Nlambda_sun_mx,1:2)
      double precision lambda_inf
      double precision lambda_sup
      integer N
      double precision lambda(1:Nproba_mx,1:2)
      double precision intensity(1:Nproba_mx,1:2)
      double precision pdf(1:Nproba_mx)
      double precision cdf(0:Nproba_mx)
c     temp
      logical int_min_found,int_max_found
      integer i,int_min,int_max
      double precision int_Ls
c     label
      character*(Nchar_mx) label
      label='subroutine build_lambda_pdf'
c
      int_min_found=.false.
      do i=1,Nlambda_source-1
         if ((source_intensity(i,1)*1.0D-3.le.lambda_inf).and.(source_intensity(i+1,1)*1.0D-3.ge.lambda_inf)) then
            int_min_found=.true.
            int_min=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.int_min_found) then
         call error(label)
         write(*,*) 'lambda interval for lambda_inf=',lambda_inf
         write(*,*) 'could not be found in the provided source_intensity'
         write(*,*) 'source_intensity(',1,',1)=',source_intensity(1,1)
         write(*,*) 'source_intensity(',Nlambda_source,',1)=',source_intensity(Nlambda_source,1)
         stop
      endif
c     Debug
c      write(*,*) 'int_min=',int_min
c      write(*,*) 'lambda=[',source_intensity(int_min,1),'-',source_intensity(int_min+1,1),'] nm'
c      write(*,*) 'lambda_inf=',lambda_inf*1.0D+3,' nm'
c     Debug
c     
      int_max_found=.false.
      do i=1,Nlambda_source-1
         if ((source_intensity(i,1)*1.0D-3.le.lambda_sup).and.(source_intensity(i+1,1)*1.0D-3.ge.lambda_sup)) then
            int_max_found=.true.
            int_max=i
            goto 112
         endif
      enddo                     ! i
 112  continue
      if (.not.int_max_found) then
         call error(label)
         write(*,*) 'lambda interval for lambda_sup=',lambda_sup
         write(*,*) 'could not be found in the provided source_intensity'
         write(*,*) 'source_intensity(',1,',1)=',source_intensity(1,1)
         write(*,*) 'source_intensity(',Nlambda_source,',1)=',source_intensity(Nlambda_source,1)
         stop
      endif
c     Debug
c      write(*,*) 'int_max=',int_max
c      write(*,*) 'lambda=[',source_intensity(int_max,1),'-',source_intensity(int_max+1,1),'] nm'
c      write(*,*) 'lambda_sup=',lambda_sup*1.0D+3,' nm'
c     Debug
c
      if (int_min.gt.int_max) then
         call error(label)
         write(*,*) 'int_min=',int_min
         write(*,*) '> int_max=',int_max
         stop
      endif
c
      N=int_max-int_min+1       ! number of intervals
      if (N.le.0) then
         call error(label)
         write(*,*) 'N=',N
         write(*,*) 'int_min=',int_min
         write(*,*) 'int_max=',int_max
         write(*,*) 'should be positive'
         stop
      endif
      if (N.gt.Nproba_mx) then
         call error(label)
         write(*,*) 'N=',N
         write(*,*) '> Nproba_mx=',Nproba_mx
         stop
      endif
c     Build the pdf array
      lambda(1,1)=lambda_inf*1.0D+3 ! [nm]
      call linear_interpolation(source_intensity(int_min,1),source_intensity(int_min+1,1),lambda(1,1),source_intensity(int_min,2),source_intensity(int_min+1,2),intensity(1,1)) ! [W/m²/sr/nm]
c     Debug
c      write(*,*) 'lambda(1,1)=',lambda(1,1),' nm, intensity(1,1)=',intensity(1,1), 'W/m²/sr/nm'
c     Debug
      if (N.gt.1) then
         do i=1,N-1
            lambda(i,2)=source_intensity(int_min+i,1) ! [nm]
            intensity(i,2)=source_intensity(int_min+i,2) ! [W/m²/sr/nm]
            lambda(i+1,1)=lambda(i,2) ! [nm]
            intensity(i+1,1)=intensity(i,2) ! [W/m²/sr/nm]
         enddo                  ! i
      endif
      lambda(N,2)=lambda_sup*1.0D+3 ! [nm]
      call linear_interpolation(source_intensity(int_max,1),source_intensity(int_max+1,1),lambda(N,2),source_intensity(int_max,2),source_intensity(int_max+1,2),intensity(N,2)) ! [W/m²/sr/nm]
c     Debug
c      write(*,*) 'lambda(',N,',2)=',lambda(N,2),' nm, intensity(',N,',2)=',intensity(N,2), 'W/m²/sr/nm'
c     Debug

c     Debug
c      write(*,*) 'lambda_inf=',lambda_inf,' µm'
c      write(*,*) 'int_min=',int_min
c      write(*,*) 'lower lambda=',source_intensity(int_min,1),' nm'
c      write(*,*) 'higher lambda=',source_intensity(int_min+1,1),' nm'
c      write(*,*) 'lambda_sup',lambda_sup,' µm'
c      write(*,*) 'int_max=',int_max
c      write(*,*) 'lower lambda=',source_intensity(int_max,1),' nm'
c      write(*,*) 'higher lambda=',source_intensity(int_max+1,1),' nm'
c      write(*,*) 'N=',N
c      do i=1,N
c         write(*,*) i,lambda(i,1),lambda(i,2),intensity(i,1),intensity(i,2)
c      enddo                     ! i
c     Debug
      
      cdf(0)=0.0D+0
      int_Ls=0.0D+0
      do i=1,N
         pdf(i)=(intensity(i,1)+intensity(i,2))*(lambda(i,2)-lambda(i,1))/2.0D+0 ! [W/m²/sr]
         cdf(i)=cdf(i-1)+pdf(i) ! [W/m²/sr]
         int_Ls=int_Ls+pdf(i)   ! [W/m²/sr]
      enddo                     ! i
      do i=1,N
         pdf(i)=pdf(i)/int_Ls
         cdf(i)=cdf(i)/int_Ls
      enddo                     ! i
      
c     Debug
c      write(*,*) 'cdf(0)=',cdf(0)
c      do i=1,3
c         write(*,*) i,pdf(i),cdf(i)
c      enddo                     ! i
c      do i=N-1,N
c         write(*,*) i,pdf(i),cdf(i)
c      enddo                     ! i
c      write(*,*) 'int_LS=',int_Ls
c      stop
c     Debug

      return
      end
      
      

      subroutine build_band_pdf(Nband,lambda_min,lambda_max,Tref,lambda_inf,lambda_sup,
     &     N,pdf,cdf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to build the discrete pdf set for sampling spectral bands
c     
c     Input:
c       + Nband: number of spectral intervals
c       + lambda_min: lower wavelength of each interval [µm]
c       + lambda_max: higher wavelength of each interval [µm]
c       + Tref: reference temperature used in the evaluation of the Plack function [K]
c       + lambda_inf: lower limit for spectral integration [µm]
c       + lambda_sup: lower limit for spectral integration [µm]
c     
c     Output:
c       + N: size of pdf and cdf arrays
c       + pdf: discrete pdf dataset
c       + cdf: cumulated discrete density dataset
c     
c     I/O
      integer Nband
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision Tref
      double precision lambda_inf
      double precision lambda_sup
      integer N
      double precision pdf(1:Nproba_mx)
      double precision cdf(0:Nproba_mx)
c     temp
      logical band_min_found,band_max_found
      integer band_min,band_max
      integer iband
      double precision int_B_total,int_B_band
c     functions
      double precision planck_int_wavlng
c     label
      character*(Nchar_mx) label
      label='subroutine build_band_pdf'
c
      if (Nproba_mx.lt.Nband) then
         call error(label)
         write(*,*) 'Nproba_mx=',Nproba_mx
         write(*,*) 'should be > Nband=',Nband
         stop
      else
         N=Nband
      endif
c      
      band_min_found=.false.
      do iband=1,Nband
         if ((lambda_inf.ge.lambda_min(iband)).and.(lambda_inf.le.lambda_max(iband))) then
            band_min_found=.true.
            band_min=iband
            goto 111
         endif
      enddo                     ! iband
 111  continue
      if (.not.band_min_found) then
         call error(label)
         write(*,*) 'band could not be found for lambda_inf=',lambda_inf
         write(*,*) 'iband / lambda_min / lambda_max'
         do iband=1,Nband
            write(*,*) iband,lambda_min(iband),lambda_max(iband)
         enddo                  ! iband
         stop
      endif
c     
      band_max_found=.false.
      do iband=1,Nband
         if ((lambda_sup.ge.lambda_min(iband)).and.(lambda_sup.le.lambda_max(iband))) then
            band_max_found=.true.
            band_max=iband
            goto 112
         endif
      enddo                     ! iband
 112  continue
      if (.not.band_max_found) then
         call error(label)
         write(*,*) 'band could not be found for lambda_sup=',lambda_sup
         write(*,*) 'iband / lambda_min / lambda_max'
         do iband=1,Nband
            write(*,*) iband,lambda_min(iband),lambda_max(iband)
         enddo                  ! iband
         stop
      endif
c
      if (band_max.lt.band_min) then
         call error(label)
         write(*,*) 'band_max=',band_max
         write(*,*) 'should be > band_min=',band_min
         stop
      endif
c
      if (band_min.gt.1) then
         do iband=1,band_min-1
            pdf(iband)=0.0D+0
         enddo                  ! iband
      endif
      int_B_total=planck_int_wavlng(lambda_inf,lambda_sup,Tref)
c     Debug
c      write(*,*) 'in_t_B_total=',int_B_total
c     Debug
      if (int_B_total.eq.0.0D+0) then
         call error(label)
         write(*,*) 'integral of B over:'
         write(*,*) 'lambda_inf=',lambda_inf,' µm'
         write(*,*) 'lambda_sup=',lambda_sup,' µm'
         write(*,*) 'Tref=',Tref
         write(*,*) 'is:',int_B_total,' W/m²/sr'
         write(*,*) 'Tref is probably too low'
         stop
      endif
      if (band_min.eq.band_max) then
         int_B_band=planck_int_wavlng(lambda_inf,lambda_sup,Tref)
         pdf(band_min)=int_B_band/int_B_total
      else         
         do iband=band_min,band_max
            if (iband.eq.band_min) then
               int_B_band=planck_int_wavlng(lambda_inf,lambda_max(iband),Tref)
            else if (iband.eq.band_max) then
               int_B_band=planck_int_wavlng(lambda_min(iband),lambda_sup,Tref)
            else
               int_B_band=planck_int_wavlng(lambda_min(iband),lambda_max(iband),Tref)
            endif
            pdf(iband)=int_B_band/int_B_total
c     Debug
c            write(*,*) 'pdf(',iband,')=',pdf(iband)
c     Debug
         enddo                  ! iband
      endif
      if (band_max.lt.Nband) then
         do iband=band_max+1,Nband
            pdf(iband)=0.0D+0
         enddo                  ! iband
      endif
c
      cdf(0)=0.0D+0
      do iband=1,N
         cdf(iband)=cdf(iband-1)+pdf(iband)
      enddo ! iband
      if (dabs(cdf(N)-1.0D+0).gt.1.0D-6) then
         call error(label)
         write(*,*) 'cdf(',N,')=',cdf(N)
         write(*,*) 'Difference to 1 is:',dabs(cdf(N)-1.0D+0)
         write(*,*) 'is expected to be < 1.0D-6'
         stop
      endif
      
      return
      end


      
      subroutine build_quad_pdf(Nquad,w,iband,N,pdf,cdf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to build the pdf for quadrature points sampling
c     
c     Input:
c       + Nquad: number of quadrature points per band
c       + w: quadrature points, for every band
c       + iband: index of the band
c     
c     Output:
c       + N: size of pdf and cdf arrays
c       + pdf: pdf for sampling quadrature points
c       + cdf: associated cumulated distribution
c     
c     I/O
      integer Nquad(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      integer iband
      integer N
      double precision pdf(1:Nproba_mx)
      double precision cdf(0:Nproba_mx)
c     temp
      integer iquad
c     label
      character*(Nchar_mx) label
      label='subroutine build_quad_pdf'

      if (Nproba_mx.lt.Nquad(iband)) then
         call error(label)
         write(*,*) 'Nproba_mx=',Nproba_mx
         write(*,*) 'should be > Nquad(',iband,')=',Nquad(iband)
         stop
      else
         N=Nquad(iband)
      endif
c     
      do iquad=1,N
         pdf(iquad)=w(iband,iquad)
      enddo                     ! iquad
c     
      cdf(0)=0.0D+0
      do iquad=1,N
         cdf(iquad)=cdf(iquad-1)+pdf(iquad)
      enddo ! iband
      if (dabs(cdf(N)-1.0D+0).gt.1.0D-6) then
         call error(label)
         write(*,*) 'cdf(',N,')=',cdf(N)
         write(*,*) 'Difference to 1 is:',dabs(cdf(N)-1.0D+0)
         write(*,*) 'is expected to be < 1.0D-6'
         stop
      endif

      return
      end

      

      subroutine sample_wavelength_according_to_Planck(lambda_inf,lambda_sup,lambda_min,lambda_max,Tref,convergence,lambda,pdf)
      implicit none
      include 'max.inc'
c     
c     Purpose: sample a wavelength according to the Planck emission
c     
c     Input:
c       + lambda_inf: lower limit for spectral integration [µm]
c       + lambda_sup: lower limit for spectral integration [µm]
c       + lambda_min: lower integration wavelength of the band [µm]
c       + lambda_max: higher integration wavelength of the band [µm]
c       + Tref: reference temperature used for the evaluation of the Planck function [K]
c     
c     Output:
c       + convergence: T if convergence was reached
c       + lambda: wavelength [µm]
c       + pdf: value of the pdf for that wavelength [µm⁻¹]
c     
c     I/O
      double precision lambda_inf
      double precision lambda_sup
      double precision lambda_min
      double precision lambda_max
      double precision Tref
      logical convergence
      double precision lambda
      double precision pdf
c     temp
      integer Niter
      double precision lambda0,lambda1,lambda2
      double precision f1,f2,f
      double precision r
      logical keep_looking
      double precision int_B_total
      double precision epsilon_lambda,epsilon_f
c     functions
      double precision planck_int_wavlng,Blambda
c     label
      character*(Nchar_mx) label
      label='sample_wavelength_according_to_Planck'
c     
      call random_gen(r)
c
      if ((lambda_inf.ge.lambda_min).and.(lambda_inf.le.lambda_max).and.(lambda_sup.ge.lambda_min).and.(lambda_sup.le.lambda_max)) then
c     integration range is all within the same band
         lambda1=lambda_inf
         lambda2=lambda_sup
      else
c     integration range spans accross multiple bands
         if ((lambda_inf.ge.lambda_min).and.(lambda_inf.le.lambda_max)) then
            lambda1=lambda_inf
            lambda2=lambda_max
         else if ((lambda_sup.ge.lambda_min).and.(lambda_sup.le.lambda_max)) then
            lambda1=lambda_min
            lambda2=lambda_sup
         else
            lambda1=lambda_min
            lambda2=lambda_max
         endif
      endif
      lambda0=lambda1
      int_B_total=planck_int_wavlng(lambda1,lambda2,Tref) ! [W/m²/sr]
c         
      epsilon_lambda=1.0D-6
      epsilon_f=1.0D-6
c      lambda1=lambda_min
      f1=0.0D+0
c      lambda2=lambda_max
      f2=1.0D+0
      Niter=0
      keep_looking=.true.
      convergence=.false.
      do while (keep_looking)
         Niter=Niter+1
         lambda=(lambda1+lambda2)/2.0D+0
         f=planck_int_wavlng(lambda0,lambda,Tref)/int_B_total
         if (f.lt.r) then
            lambda1=lambda
            f1=f
         else
            lambda2=lambda
            f2=f
         endif
         if (dabs(lambda2-lambda1).lt.epsilon_lambda) then
            keep_looking=.false.
            convergence=.true.
         endif
         if (dabs(f2-f1).lt.epsilon_f) then
            keep_looking=.false.
            convergence=.true.
         endif
      enddo                     ! while (keep_looking)
      if (convergence) then
         pdf=Blambda(Tref,lambda)/int_B_total ! [W/m²/sr/µm] / [W/m²/sr] = [µm⁻¹]
      endif

      return
      end



      subroutine sample_wavelength_according_to_intensity0(lambda1,lambda2,i1,i2,lambda,pdf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to sample a wavelength according to a uniform pdf over a range
c     
c     Input:
c       + lambda1: lower wavelength [nm]
c       + lambda2: higher wavelength [nm]
c       + i1: intensity @ lambda1 [W/m²/sr/nm]
c       + i2: intensity @ lambda2 [W/m²/sr/nm]
c     
c     Output:
c       + lambda: wavelength sampled according to i(lambda)=i1+(i2-i1)/(lambda2-lambda1)*(lambda-lambda1) [µm]
c       + pdf: value of the pdf for lambda [µm⁻¹]
c     
c     I/O
      double precision lambda1
      double precision lambda2
      double precision i1
      double precision i2
      double precision lambda
      double precision pdf
c     temp
      double precision integral
      double precision a,b,c,r
      integer nsol
      double precision x0,x1,x2
      logical x0_is_in_range
      logical x1_is_in_range
      logical x2_is_in_range
c     label
      character*(Nchar_mx) label
      label='subroutine sample_wavelength_according_to_intensity0'

      call sample_uniform_double(lambda1,lambda2,lambda) ! lambda: [nm]
c     Debug
c      write(*,*) 'lambda=',lambda,' nm'
c     Debug
      pdf=1.0D+0/(lambda2-lambda1) ! [W/m²/sr/nm]/[W/m²/sr]=[nm⁻¹]
c     conversions
      lambda=lambda*1.0D-3      ! [µm]
      pdf=pdf*1.0D+3            ! [µm⁻¹]

      return
      end




      subroutine sample_wavelength_according_to_intensity(lambda1,lambda2,i1,i2,lambda,pdf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to sample a wavelength according to a linear pdf over a range
c     
c     Input:
c       + lambda1: lower wavelength [nm]
c       + lambda2: higher wavelength [nm]
c       + i1: intensity @ lambda1 [W/m²/sr/nm]
c       + i2: intensity @ lambda2 [W/m²/sr/nm]
c     
c     Output:
c       + lambda: wavelength sampled according to i(lambda)=i1+(i2-i1)/(lambda2-lambda1)*(lambda-lambda1) [µm]
c       + pdf: value of the pdf for lambda [µm⁻¹]
c     
c     I/O
      double precision lambda1
      double precision lambda2
      double precision i1
      double precision i2
      double precision lambda
      double precision pdf
c     temp
      double precision integral
      double precision a,b,c,r
      integer nsol
      double precision x0,x1,x2
      logical x0_is_in_range
      logical x1_is_in_range
      logical x2_is_in_range
c     label
      character*(Nchar_mx) label
      label='subroutine sample_wavelength_according_to_intensity'

      integral=0.5D+0*(i1+i2)*(lambda2-lambda1) ! [W/m²/sr]
      call random_gen(r)
      a=0.5D+0*(i2-i1)/(lambda2-lambda1)
      b=i1-lambda1*(i2-i1)/(lambda2-lambda1)
      c=0.5D+0*(i2-i1)/(lambda2-lambda1)*lambda1**2.0D+0-i1*lambda1-r*integral
      if (a.eq.0.0D+0) then
         x0=-c/b
         x0_is_in_range=.false.
         if ((x0.ge.lambda1).and.(x0.le.lambda2)) then
            x0_is_in_range=.true.
         endif
         if (x0_is_in_range) then
            lambda=x0
         else
            call error(label)
            write(*,*) '1st degree equation -- solution not in range'
            stop
         endif
      else
         call eq2deg(a,b,c,nsol,x0,x1,x2)
         if (nsol.eq.0) then
            call error(label)
            write(*,*) 'no solution found'
            stop
         else if (nsol.eq.1) then
            x0_is_in_range=.false.
            if ((x0.ge.lambda1).and.(x0.le.lambda2)) then
               x0_is_in_range=.true.
            endif
            if (x0_is_in_range) then
               lambda=x0
            else
               call error(label)
               write(*,*) 'Single solution found -- not in range'
               stop
            endif
         else if (nsol.eq.2) then
            x1_is_in_range=.false.
            if ((x1.ge.lambda1).and.(x1.le.lambda2)) then
               x1_is_in_range=.true.
            endif
            x2_is_in_range=.false.
            if ((x2.ge.lambda1).and.(x2.le.lambda2)) then
               x2_is_in_range=.true.
            endif
            if ((x1_is_in_range).and.(.not.x2_is_in_range)) then
               lambda=x1
            endif
            if ((.not.x1_is_in_range).and.(x2_is_in_range)) then
               lambda=x2
            endif
            if ((.not.x1_is_in_range).and.(.not.x2_is_in_range)) then
               call error(label)
               write(*,*) 'lambda1=',lambda1,' i1=',i1
               write(*,*) 'lambda2=',lambda2,' i2=',i2
               write(*,*) 'integral=',integral
               write(*,*) 'r=',r
               write(*,*) 'a=',a
               write(*,*) 'b=',b
               write(*,*) 'c=',c
               write(*,*) 'x1=',x1
               write(*,*) 'x2=',x2
               write(*,*) 'Two solutions found -- none in range'
               stop
            endif
            if ((x1_is_in_range).and.(x2_is_in_range)) then
               call error(label)
               write(*,*) 'x1=',x1
               write(*,*) 'x2=',x2
               write(*,*) 'Two solutions found -- both in range'
               stop
            endif
         endif                  ! value of nsol
      endif                     ! a=0 or not
      pdf=(i1+(i2-i1)/(lambda2-lambda1)*(lambda-lambda1))/integral ! [W/m²/sr/nm]/[W/m²/sr]=[nm⁻¹]
c     conversions
      lambda=lambda*1.0D-3      ! [µm]
      pdf=pdf*1.0D+3            ! [µm⁻¹]

      return
      end
