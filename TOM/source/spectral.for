      subroutine spectral_reflectivity(Nlambda,lambda_array,reflectivity_array,lambda,reflectivity)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the reflectivity at a given wavelegth
c     
c     Input:
c       + Nlambda: number of values of the wavelength for the definition of the reflectivity
c       + lambda_array: values of the wavelength where the reflectivity is defined [µm]
c       + reflectivity_array: values of the reflectivity, over the reflectivity array
c       + lambda: value of the wavelength for which the reflectivity must be retrieved [nm]
c     
c     Output:
c       + reflectivity: value of the reflectivity @ lambda
c     
c     I/O
      integer Nlambda
      double precision lambda_array(1:Nlambda_mx)
      double precision reflectivity_array(1:Nlambda_mx)
      double precision lambda
      double precision reflectivity
c     temp
      logical interval_found
      integer interval,i,ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine spectral_reflectivity'

      interval_found=.false.
      do i=1,Nlambda-1
         if ((lambda.ge.lambda_array(i)).and.(lambda.le.lambda_array(i+1))) then
            interval_found=.true.
            interval=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.interval_found) then
         if (lambda.lt.lambda_array(1)) then
            interval_found=.true.
            interval=1
         else if (lambda.gt.lambda_array(Nlambda)) then
            interval_found=.true.
            interval=Nlambda-1
         else
            write(*,*) 'This is not supposed to happen'
            write(*,*) 'lambda=',lambda
            do ilambda=1,Nlambda
               write(*,*) 'lambda_array(',ilambda,')=',lambda_array(ilambda)
            enddo               ! ilambda
            stop
         endif
      endif
c     
      reflectivity=reflectivity_array(interval)
     &     +(reflectivity_array(interval+1)-reflectivity_array(interval))
     &     /(lambda_array(interval+1)-lambda_array(interval))
     &     *(lambda-lambda_array(interval))
c     Interpolation when lambda<lambda_array(1) or lambda>lambda_array(Nlambda)
c     can lead to values < 0 or > 1
      if (reflectivity.lt.0.0D+0) then
         reflectivity=0.0D+0
      endif
      if (reflectivity.gt.1.0D+0) then
         reflectivity=1.0D+0
      endif
      
      return
      end



      subroutine ground_emission(dim,Nlambda,lambda_array,reflectivity_array,lambda_inf,lambda_sup,Tsurf,Lsurf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the integral of emissivity(lambda)*B(lambda,Tsurf)
c     over the [lambda_inf, lambda_sup] interval
c     
c     Input:
c       + dim: dimension of space
c       + Nlambda: number of values of the wavelength for the definition of the reflectivity
c       + lambda_array: values of the wavelength where the reflectivity is defined [µm]
c       + reflectivity_array: values of the reflectivity, over the reflectivity array
c       + lambda_inf: lower integration wavelength [µm]
c       + lambda_sup: higher integration wavelength [µm]
c       + Tsurf: ground temperature
c     
c     Output:
c       + Lsurf: integral of emissivity(lambda)*B(lambda,Tsurf) over the [lambda_inf, lambda_sup] interval
c     
c     I/O
      integer dim
      integer Nlambda
      double precision lambda_array(1:Nlambda_mx)
      double precision reflectivity_array(1:Nlambda_mx)
      double precision lambda_inf
      double precision lambda_sup
      double precision Tsurf
      double precision Lsurf
c     temp
      integer Ninterval,i
      double precision dlambda
      double precision lambda1,lambda2
      double precision epsilon1,epsilon2
      double precision rho1,rho2
c     functions
      double precision Blambda
c     label
      character*(Nchar_mx) label
      label='subroutine ground_emission'

      Ninterval=10000
      dlambda=(lambda_sup-lambda_inf)/dble(Ninterval)
      Lsurf=0.0D0
      do i=1,Ninterval
         lambda1=lambda_inf+dlambda*(i-1)
         lambda2=lambda_inf+dlambda*i
         call spectral_reflectivity(Nlambda,lambda_array,reflectivity_array,lambda1,rho1)
         epsilon1=1.0D+0-rho1
         call spectral_reflectivity(Nlambda,lambda_array,reflectivity_array,lambda2,rho2)
         epsilon2=1.0D+0-rho2
         Lsurf=Lsurf+(epsilon1*Blambda(Tsurf,lambda1)+epsilon2*Blambda(Tsurf,lambda2))*dlambda/2.0D+0 ! [W/m²/sr]
c     Debug
c         write(*,*) i,lambda1,lambda2,epsilon1,epsilon2,Lsurf
c     Debug
      enddo                     ! Ninterval
      
      return
      end
