c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine distance_on_sphere(n,P1,P2,r,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two positions on a sphere
c
c     Inputs:
c       + n: dimension of vectors
c       + P1: (theta1,phi1) latitude and longitude of the first position [rad,rad]
c       + P2: (theta2,phi2) latitude and longitude of the second position [rad,rad]
c       + r: radius of the sphere [m]
c     
c     Output:
c       + d: distance [m]
c

c     inputs
      integer n
      double precision P1(1:2)
      double precision P2(1:2)
      double precision r
c     output
      double precision d
c     temp
      double precision pos1(1:Ndim_mx)
      double precision pos2(1:Ndim_mx)
      double precision alpha,prod
c     label
      character*(Nchar_mx) label
      label='subroutine distance_on_sphere'

      pos1(1)=r
      pos1(2)=P1(1)
      pos1(3)=P1(2)
      pos2(1)=r
      pos2(2)=P2(1)
      pos2(3)=P2(2)
      call scalar_product_spher(n,pos1,pos2,prod)
      alpha=dacos(prod) ! rad
      d=alpha*r ! m

      return
      end



      subroutine distance_spher(dim,x1_spher,x2_spher,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two points defined
c     by their spherical coordinates
c     
c     Input
c       + dim: dimension of space
c       + x1_spher: spherical coordinates of position 1 [m,rad,rad]
c       + x2_spher: spherical coordinates of position 2 [m,rad,rad]
c     
c     Output:
c       + d: distance between positions 1 and 2 [m]
c     
c     I/O
      integer dim
      double precision x1_spher(1:Ndim_mx)
      double precision x2_spher(1:Ndim_mx)
      double precision d
c     temp
      double precision x1_cart(1:Ndim_mx)
      double precision x2_cart(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine distance_sphere'

      call spher2cart(dim,x1_spher,x1_cart)
      call spher2cart(dim,x2_spher,x2_cart)
      call distance_cart(dim,x1_cart,x2_cart,d)

      return
      end



      subroutine distance_cart(dim,x1,x2,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two points defined
c     by their cartesian coordinates
c
c     Input
c       + dim: dimension of space
c       + x1: cartesian coordinates of position 1 [m,m,m]
c       + x2: cartesian coordinates of position 2 [m,m,m]
c
c     Output:
c       + d: distance between positions 1 and 2 [m]
c
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision d
c     label
      character*(Nchar_mx) label
      label='subroutine distance_cart'

      call distance(dim,x1,x2,d)

      return
      end



      subroutine spher2cart(dim,x_spher,x_cart)
      implicit none
      include 'max.inc'
c     
c     Purpose: to convert (r,theta,phi) spherical coordinates
c     into (x,y,z) cartesian coordinates
c
c     Inputs:
c       + dim: dimension of space
c       + x_spher: spherical coordinates [m,rad,rad]
c
c     Outputs:
c       + x_cart: cartesian coordinates [m,m,m]
c
c     I/O
      integer dim
      double precision x_spher(1:Ndim_mx)
      double precision x_cart(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine spher2cart'

      x_cart(1)=x_spher(1)*dcos(x_spher(2))*dcos(x_spher(3))
      x_cart(2)=x_spher(1)*dcos(x_spher(2))*dsin(x_spher(3))
      x_cart(3)=x_spher(1)*dsin(x_spher(2))
      
      return
      end



      subroutine cart2spher(dim,x_cart,x_spher)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to convert (x,y,z) cartesian coordinates
c     into (r,theta,phi) spherical coordinates
c
c     Input:
c       + dim: dimension of space
c       + x_cart: cartesian coordinates [m,m,m]
c
c     Outputs:
c       + x_spher: spherical coordinates [m,rad,rad]
c
c     I/O
      integer dim
      double precision x_cart(1:Ndim_mx)
      double precision x_spher(1:Ndim_mx)
c     temp
      double precision x,y,z
      double precision r,theta,phi
      double precision cosp,sinp,cost,sint
      double precision a,t1,t2
c     label
      character*(Nchar_mx) label
      label='subroutine cart2spher'

      x=x_cart(1)               ! m
      y=x_cart(2)               ! m
      z=x_cart(3)               ! m
      a=dsqrt(x**2.0D+0+y**2.0D+0)
      r=dsqrt(x**2.0D+0+y**2.0D+0+z**2.0D+0)
      if (a.eq.0.0D+0) then
         cosp=1.0D+0
         sinp=0.0D+0
      else
         cosp=x/a
         sinp=y/a
      endif
      cost=a/r
      sint=z/r
c      
      t1=dacos(dabs(cosp))
      if (x.ge.0.0D+0) then
         if (sinp.ge.0.0D+0) then
            phi=t1
         else
            phi=2.0D+0*pi-t1
         endif
      else
         if (sinp.ge.0.0D+0) then
            phi=pi-t1
         else
            phi=pi+t1
         endif
      endif
c
      t2=dacos(cost)
      if (sint.ge.0.0D+0) then
         theta=t2
      else
         theta=-t2
      endif
      x_spher(1)=r              ! m
      x_spher(2)=theta          ! rad
      x_spher(3)=phi            ! rad
      
      return
      end
