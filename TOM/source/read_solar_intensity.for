      subroutine read_solar_intensity_file(solar_intensity_file,Nlambda,intensity)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record a solar intensity binary file
c     
c     Input:
c       + solar_intensity_file: file to record
c     
c     Output:
c       + Nlambda: number of wavelength/intensity values
c       + intensity: values of the wavelength [nm] and associated solar intensity [W/m²/sr/nm]
c     
c     I/O
      character*(Nchar_mx) solar_intensity_file
      integer*8 Nlambda
      double precision intensity(1:Nlambda_sun_mx,1:2)
c     temp
      integer*8 total_read
      integer remaining_byte
      logical*1 l1
      integer*8 record_size,alignment
      integer i,ilambda,ios
      integer*8 pgsz
c     label
      character*(Nchar_mx) label
      label='subroutine read_solar_intensity_file'
      
      open(23,file=trim(solar_intensity_file),status='old',form='unformatted',access='stream',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(solar_intensity_file)
         stop
      else
         write(*,*) 'Reading file: ',trim(solar_intensity_file)
         read(23) pgsz
         read(23) Nlambda
         read(23) record_size
         read(23) alignment
c     Padding file 23 ---
         total_read=4*size_of_int8
         call compute_padding2(pgsz,total_read,remaining_byte)
         read(23) (l1,i=1,remaining_byte)
c     --- Padding file 23
         do ilambda=1,Nlambda
            read(23) (intensity(ilambda,i),i=1,2) ! [nm], [W/m²/sr/m]
            intensity(ilambda,2)=intensity(ilambda,2)*1.0D-9 ! [W/m²/sr/nm]
         enddo                  ! j
c     Padding file 23 ---
         total_read=Nlambda*record_size
         call compute_padding2(pagesize,total_read,remaining_byte)
         read(23) (l1,i=1,remaining_byte)
c     --- Padding file 23
      endif
      close(23)
      
      return
      end
