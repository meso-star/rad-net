      subroutine read_data(datafile,Nevent,source,lambda_lo,lambda_hi,external_source_type,Ds,Rs,Ts,theta_s,phi_s)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'constants.inc'
c     
c     Purpose: to read a input data file for program "sw_Fobs"
c     
c     Input:
c       + datafile: input data file
c     
c     Output:
c       + Nevent: number of statistical events
c       + source: radiation source [internal / external]
c       + lambda_lo: lower wavelength [micrometers]
c       + lambda_hi: higher wavelength [micrometers]
c       + external_source_type: type of external source [1: Planck; 2: detailled intensity spectrum]
c       + Ds: distance to star [km]
c       + Rs: radius of star [km]
c       + Ts: brightness temperature of the star [K]
c       + theta_s: latitude of star in local spherical body referential [rad]
c       + phi_s: longitude of star in local spherical body referential [rad]
c     
c     I/O
      character*(Nchar_mx) datafile
      integer Nevent
      character*(Nchar_mx) source
      double precision lambda_lo
      double precision lambda_hi
      integer external_source_type
      double precision Ds
      double precision Rs
      double precision Ts
      double precision theta_s
      double precision phi_s
c     temp
      integer i,ios
      double precision deg2rad
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
         do i=1,8
            read(11,*)
         enddo                  ! i
         read(11,*) Nevent
         do i=1,5
            read(11,*)
         enddo                  ! i
         read(11,10) source
         read(11,*)
         read(11,*) lambda_lo
         read(11,*)
         read(11,*) lambda_hi
         read(11,*)
         read(11,*) external_source_type
         do i=1,5
            read(11,*)
         enddo                  ! i
         read(11,*) Ds
         read(11,*)
         read(11,*) Rs
         read(11,*)
         read(11,*) Ts
         read(11,*)
         read(11,*) theta_s
         read(11,*)
         read(11,*) phi_s
      endif
      close(11)

c     Inconsistencies
      if ((trim(source).ne.'internal').and.(trim(source).ne.'external')) then
         call error(label)
         write(*,*) 'source="',trim(source),'"'
         write(*,*) 'should be either "internal" or "external"'
         stop
      endif
      if (lambda_lo.le.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_lo=',lambda_lo
         write(*,*) 'should be positive'
         stop
      endif
      if (lambda_hi.le.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_hi=',lambda_hi
         write(*,*) 'should be positive'
         stop
      endif
      if (lambda_lo.gt.lambda_hi) then
         call error(label)
         write(*,*) 'lambda_lo=',lambda_lo
         write(*,*) 'should be < lambda_hi=',lambda_hi
         stop
      endif
      if ((trim(source).eq.'external').and.(external_source_type.ne.1).and.(external_source_type.ne.2)) then
         call error(label)
         write(*,*) 'external_source_type=',external_source_type
         write(*,*) 'allowed values: {1, 2}'
         stop
      endif
      if (Ds.le.0.0D+0) then
         call error(label)
         write(*,*) 'Ds=',Ds
         write(*,*) 'should be positive'
         stop
      endif
      if (Rs.le.0.0D+0) then
         call error(label)
         write(*,*) 'Rs=',Rs
         write(*,*) 'should be positive'
         stop
      endif
      if (Ts.le.0.0D+0) then
         call error(label)
         write(*,*) 'Ts=',Ts
         write(*,*) 'should be positive'
         stop
      endif
      if ((theta_s.lt.-90.0D+0).or.(theta_s.gt.90.0D+0)) then
         call error(label)
         write(*,*) 'theta_s=',theta_s
         write(*,*) 'should be in the [-90,90]° range'
         stop
      endif
      if ((phi_s.lt.0.0D+0).or.(phi_s.gt.360.0D+0)) then
         call error(label)
         write(*,*) 'phi_s=',phi_s
         write(*,*) 'should be in the [0,360]° range'
         stop
      endif
      if (Nevent.le.0) then
         call error(label)
         write(*,*) 'Nevent=',Nevent
         write(*,*) 'should be positive'
         stop
      endif
c     Inconsistencies
      if (Ds.lt.Rs) then
         call error(label)
         write(*,*) 'Ds=',Ds,' km'
         write(*,*) '< Rs=',Rs,' km'
         write(*,*) 'spherical body is inside the star !'
         stop
      endif

c     Conversions
      Ds=Ds*1.0D+3                  ! km -> m
      Rs=Rs*1.0D+3                  ! km -> m

      deg2rad=pi/180.0D+0           ! rad/deg
      theta_s=theta_s*deg2rad       ! deg -> rad
      phi_s=phi_s*deg2rad           ! deg -> rad

      return
      end
