      subroutine linear_interpolation(x1,x2,x0,f1,f2,f0)
      implicit none
      include 'max.inc'
c
c     Purpose: to perform a linear interpolation:
c     f(x)=f(x1)+[f(x2)-f(x1)]/[x2-x1]*[x-x1]
c
c     Input:
c       + x1: value of x1
c       + x2: value of x2
c       + x0: value of x where function "f" has to be interpolated
c       + f1: value of "f" at x1
c       + f2: value of "f" at x2
c     
c     Output:
c       + f0: value of "f" interpolated at x0
c
c     I/O
      double precision x1,x2,x0
      double precision f1,f2,f0
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine linear_interpolation'

      if ((x1.eq.x2).and.(x0.ne.x1)) then
         call error(label)
         write(*,*) 'Bad input arguments:'
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'while x0=',x0
         stop
      endif

      if (x0.eq.x1) then
         f0=f1
      else if (x0.eq.x2) then
         f0=f2
      else
         f0=f1+(f2-f1)/(x2-x1)*(x0-x1)
      endif

      return
      end
