      subroutine read_camera(dim,datafile,theta_obs,phi_obs,D,theta_target,phi_target,D_target,u_up,
     &     solid_angle_type,hfov,aspect_ratio)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'constants.inc'
c     
c     Purpose: to read a camera definition file
c     
c     Input:
c       + dim: dimension of space
c       + datafile: file to read
c     
c     Output:
c       + theta_obs: observer's latitude in local spherical body referential [rad]
c       + phi_obs: observer's longitude in local spherical body referential [rad]
c       + D: distance between spherical body center and observation position [m]
c       + theta_target: target's latitude [rad]
c       + phi_target: target's longitude [rad]
c       + D_target: distance between spherical body center and target position [m]
c       + u_up: UP direction for the camera [m, m, m]
c       + solid_angle_type: (1)for a conical integration solid angle, (2) for rectangular
c       + hfov: horizontal FOV in the case of a rectangular solid angle [rad]
c       + aspect_ratio: retangular solid angle horizontal/vertical aspect ratio
c     
c     I/O
      integer dim
      character*(Nchar_mx) datafile
      double precision theta_obs
      double precision phi_obs
      double precision D
      double precision theta_target
      double precision phi_target
      double precision D_target
      double precision u_up(1:Ndim_mx)
      integer solid_angle_type
      double precision hfov
      double precision aspect_ratio
c     temp
      integer i,ios
      double precision deg2rad
      character*(Nchar_mx) line,line1,line2
      character*1 char
      integer Nchar
      integer idx(1:Nchar_mx)
      integer errcode
      integer int1,int2
c     label
      character*(Nchar_mx) label
      label='subroutine read_camera'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
         do i=1,5
            read(11,*)
         enddo                  ! i
         read(11,*) theta_obs
         read(11,*)
         read(11,*) phi_obs
         read(11,*)
         read(11,*) D
         read(11,*)
         read(11,*) theta_target
         read(11,*)
         read(11,*) phi_target
         read(11,*)
         read(11,*) D_target
         read(11,*)
         read(11,*) (u_up(i),i=1,dim)
         read(11,*)
         read(11,*) solid_angle_type
         read(11,*)
         read(11,*) hfov
         read(11,*)
         read(11,10) line
      endif
      close(11)

c     Inconsistencies
      if ((theta_obs.lt.-90.0D+0).or.(theta_obs.gt.90.0D+0)) then
         call error(label)
         write(*,*) 'theta_obs=',theta_obs
         write(*,*) 'should be in the [-90,90]° range'
         stop
      endif
      if ((phi_obs.lt.0.0D+0).or.(phi_obs.gt.360.0D+0)) then
         call error(label)
         write(*,*) 'phi_obs=',phi_obs
         write(*,*) 'should be in the [0,360]° range'
         stop
      endif
      if (D.le.0.0D+0) then
         call error(label)
         write(*,*) 'D=',D
         write(*,*) 'should be positive'
         stop
      endif
      if ((theta_target.lt.-90.0D+0).or.(theta_target.gt.90.0D+0)) then
         call error(label)
         write(*,*) 'theta_target=',theta_target
         write(*,*) 'should be in the [-90,90]° range'
         stop
      endif
      if ((phi_target.lt.0.0D+0).or.(phi_target.gt.360.0D+0)) then
         call error(label)
         write(*,*) 'phi_target=',phi_target
         write(*,*) 'should be in the [0,360]° range'
         stop
      endif
      if (D_target.lt.0.0D+0) then
         call error(label)
         write(*,*) 'D_target=',D_target
         write(*,*) 'should be positive'
         stop
      endif
      if ((solid_angle_type.ne.1).and.(solid_angle_type.ne.2)) then
         call error(label)
         write(*,*) 'solid_angle_type=',solid_angle_type
         write(*,*) 'possible values: {1, 2}'
         stop
      endif
      if (solid_angle_type.eq.2) then
         if (hfov.le.0.0D+0) then
            call error(label)
            write(*,*) 'hfov=',hfov
            write(*,*) 'should be  > 0'
            stop
         endif
         char=':'
         call find_character(line,char,Nchar,idx)
         if (Nchar.ne.1) then
            call error(label)
            write(*,*) 'Aspect ratio specification incorrect: "',trim(line),'"'
            write(*,*) 'it should be composed by two values, separated by a semicolon'
            stop
         endif
         if (idx(1).le.1) then
            call error(label)
            write(*,*) 'Aspect ratio specification incorrect: "',trim(line),'"'
            write(*,*) 'it should be composed by two values, separated by a semicolon'
            stop
         endif
         line1=line(1:idx(1)-1)
         line2=line(idx(1)+1:len_trim(line))
         call str2int(line1,int1,errcode)
         if (errcode.eq.1) then
            call error(label)
            write(*,*) 'Could not convert to integer:'
            write(*,*) 'line1= "',trim(line1),'"'
            stop
         endif
         if (int1.le.0) then
            call error(label)
            write(*,*) 'int1=',int1
            write(*,*) 'should be positive'
            stop
         endif
         call str2int(line2,int2,errcode)
         if (errcode.eq.1) then
            call error(label)
            write(*,*) 'Could not convert to integer:'
            write(*,*) 'line2= "',trim(line2),'"'
            stop
         endif
         if (int2.le.0) then
            call error(label)
            write(*,*) 'int2=',int2
            write(*,*) 'should be positive'
            stop
         endif
         aspect_ratio=dble(int1)/dble(int2)
      endif                     ! solid_angle_type=2
      
c     Conversions
      D=D*1.0D+3                ! km -> m
      D_target=D_target*1.0D+3                ! km -> m
      
      deg2rad=pi/180.0D+0       ! rad/deg
      theta_obs=theta_obs*deg2rad ! deg -> rad
      phi_obs=phi_obs*deg2rad   ! deg -> rad
      theta_target=theta_target*deg2rad ! deg -> rad
      phi_target=phi_target*deg2rad ! deg -> rad
      hfov=hfov*deg2rad ! deg -> rad

c     post-processing
      call normalize_vector(dim,u_up,u_up)

      return
      end
