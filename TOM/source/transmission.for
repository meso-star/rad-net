      subroutine transmission_to_source(debug,dim,x0,u0,center,x_source,Nlayer,z_grid,D,Ds,Rs,theta_s,phi_s,
     &     ka_gas,ks_gas,Naerosol,ka_aerosol,ks_aerosol,band_index,quad_index,
     &     valid,transmissivity)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the transmission between the current position
c     and the source
c     
c     Input:
c       + dim: dimension of space
c       + x0: starting position
c       + u0: propagation direction between x0 and the source
c       + center: center of the sphere [m,m,m]
c       + x_source: source position [m,m,m]
c       + Nlayer: number of vertical layers
c       + z_grid: vertical grid [m]
c       + D: distance between spherical body center and observation position [m]
c       + Ds: distance to star [km]
c       + Rs: radius of star [km]
c       + theta_s: latitude of star in local spherical body referential [rad]
c       + phi_s: longitude of star in local spherical body referential [rad]
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + Naerosol: number of aerosols
c       + ka_aerosol: aerosol extinction coefficient [m⁻¹]
c       + ks_aerosol: aerosol scattering coefficient [m⁻¹]
c       + band_index: narrowband index
c       + quad_index: quadrature point index
c     
c     Output:
c       + valid: T if result is valid, F otherwise
c       + transmissivity: transmissivity between x0 and the source, in direction u0
c     
c     I/O
      logical debug
      integer dim
      double precision x0(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision x_source(1:Ndim_mx)
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision D
      double precision Ds
      double precision Rs
      double precision theta_s
      double precision phi_s
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      integer Naerosol
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      integer band_index
      integer quad_index
      logical valid
      double precision transmissivity
c     temp
      double precision xS(1:Ndim_mx)
      double precision theta_source,omega_source
      double precision prod
      double precision x(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      integer n_int
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision sigma1,sigma2
      logical intersects_source
      logical source_is_visible
      logical keep_running
      logical intersection_found,err
      double precision x_int(1:Ndim_mx)
      double precision d2int
      logical is_in_layer
      integer layer_index
      logical is_at_interface
      integer interface_index
      integer ilayer,next_layer
      integer iaerosol
      double precision kext
      integer j
      double precision dist
c     Debug
      double precision n_xint(1:Ndim_mx)
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine transmission_to_source'

c     Debug
      if (debug) then
         write(*,*) trim(label),': in'
      endif                     ! debug
c     Debug

      valid=.true.
c     test intersection between the LOS and the source
      call source_solid_angle(debug,dim,x0,x_source,Rs,theta_source,omega_source)
      call substract_vectors(dim,x_source,x0,xS)
      call scalar_product(dim,xS,u0,prod)
      if (prod.ge.dcos(theta_source)) then
         intersects_source=.true.
      else
         intersects_source=.false.
      endif

c      test visibility between x0 and the source
c     Debug
      if (debug) then
         write(*,*) 'Calling line_sphere_forward_intersect with:'
         write(*,*) 'x0=',x0
         write(*,*) 'u0=',u0
         write(*,*) 'center=',center
         write(*,*) 'radius=',z_grid(0)
      endif                     ! debug
c     Debug
      call line_sphere_forward_intersect(.false.,dim,
     &     x0,u0,center,z_grid(0),
     &     n_int,P1,sigma1,P2,sigma2)
      if (n_int.eq.0) then
         source_is_visible=.true.
      else
         source_is_visible=.false.
      endif
c     Debug
      if (debug) then
         write(*,*) 'intersects_source=',intersects_source
         write(*,*) 'x0=',x0
         write(*,*) 'u0=',u0
         write(*,*) 'z_grid(0)=',z_grid(0)
         write(*,*) 'n_int=',n_int
         write(*,*) 'source_is_visible=',source_is_visible
      endif                     ! debug
c     Debug
c     compute transmission
      if ((intersects_source).and.(source_is_visible)) then
         call copy_vector(dim,x0,x)
         call copy_vector(dim,u0,u)
c     looking for "ilayer", the index of the layer to take into account for ka and ks fields
         call locate_position(dim,Nlayer,z_grid,center,x,
     &        is_in_layer,layer_index,is_at_interface,interface_index)
         if (is_in_layer) then
            ilayer=layer_index
         else if (is_at_interface) then
            ilayer=interface_index+1 ! there isn't a layer index "Nlayer+1" in the case interface_index=Nlayer (TOA)
         endif
c     Debug
         if (debug) then
            write(*,*) 'ilayer=',ilayer
         endif                  ! debug
c     Debug
         if (ilayer.eq.Nlayer+1) then ! if position is out of the atmosphere
            call line_sphere_forward_intersect(debug,dim,
     &           x0,u0,center,z_grid(Nlayer),
     &           n_int,P1,sigma1,P2,sigma2)
c     Debug
            if (debug) then
               write(*,*) 'n_int=',n_int
               if (n_int.gt.0) then
                  write(*,*) 'P1=',(P1(j),j=1,dim)
                  call distance(dim,center,P1,dist)
                  write(*,*) 'distance to center is:',dist
               endif
            endif               ! debug
c     Debug
            if (n_int.gt.0) then
               call copy_vector(dim,P1,x)
               ilayer=Nlayer
               keep_running=.true.               
            else
               keep_running=.false.
            endif
         else                   ! ilayer =/= Nlayer+1
            keep_running=.true.
         endif                  ! ilayer=Nlayer+1

c     Debug
c         write(*,*) 'keep_running=',keep_running
c     Debug
         transmissivity=1.0D+0
         do while (keep_running)
            call next_grid_intersection(.false.,dim,
     &           x,u,center,Nlayer,z_grid,
     &           err,intersection_found,x_int,d2int,interface_index) ! no intersection will be found when "x" is located at the top interface (interface_index=Nlayer)
            if (err) then
               valid=.false.
               goto 666
            endif
c     Debug
            if (debug) then
               write(*,*) 'x=',(x(j),j=1,dim)
               call distance(dim,center,x,dist)
               write(*,*) 'radius @ x=',dist
               write(*,*) 'u=',(u(j),j=1,dim)
c     write(*,*) 'z_grid(',Nlayer,')=',z_grid(Nlayer)
               write(*,*) 'intersection_found=',intersection_found
               if (intersection_found) then
                  write(*,*) 'x_int=',(x_int(j),j=1,dim)
                  call normal_on_sphere(dim,
     &                 center,0.0D+0,x_int,n_xint)
                  call scalar_product(dim,n_xint,u,prod)
                  write(*,*) 'n_xint.u=',prod
                  call distance(dim,center,x_int,dist)
                  write(*,*) 'radius @ x_int=',dist
                  write(*,*) 'interface_index=',interface_index
               endif
            endif               ! debug
c     Debug
            if (intersection_found) then
               call total_extinction_coefficient(ilayer,band_index,quad_index,Nlayer,ka_gas,ks_gas,Naerosol,ka_aerosol,ks_aerosol,kext)
c     Debug
               if (debug) then
                  write(*,*) 'ilayer=',ilayer,'kext=',kext,' d2int=',d2int
               endif
c     Debug
               transmissivity=transmissivity*dexp(-kext*d2int)
               call copy_vector(dim,x_int,x)
               if (interface_index.eq.ilayer-1) then
                  next_layer=ilayer-1
               else if (interface_index.eq.ilayer) then
                  next_layer=ilayer+1
               else
                  valid=.false.
                  goto 666
               endif
c     Debug
               if (debug) then
                  write(*,*) 'next_layer=',next_layer
               endif
c     Debug
               ilayer=next_layer
            else                ! intersection_found=F
               keep_running=.false.
            endif
         enddo                  ! while (keep_running)
c     Debug
         if (debug) then
            write(*,*) 'transmissivity=',transmissivity
         endif                  ! debug
c     Debug
c
      else
         transmissivity=0.0D+0
      endif                     ! intersects_source.and.source_is_visible

 666  continue
c     Debug
      if (debug) then
         write(*,*) trim(label),': out'
      endif                     ! debug
c     Debug
      return
      end
