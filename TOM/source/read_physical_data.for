      subroutine read_physical_data(datafile,dim,
     &     Nlayer,z_grid,Tsurf,
     &     Nlambda,lambda,reflectivity,
     &     Tgas,Naerosol,Nband,Nquad,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,ka_aerosol,ks_aerosol,g_aerosol)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read all the physical data needed for the oignon model
c     
c     Input:
c       + datafile: file to record
c       + dim: dimension of space
c     
c     Output:
c       + Nlayer: number of vertical layers
c       + z_grid: vertical grid [m]
c       + Tsurf: surface temperature [K]
c       + Nlambda: number of wavelength points for the definition of the ground reflectivity
c       + lambda: values of the wavelength for the definition of the ground reflectivity
c       + reflectivity: values of the ground reflectivity
c       + Tgas: gas temperature [K]
c       + Naerosol: number of areosol modes
c       + Nband: number of spectral intervals
c       + Nquad: quadrature order, for each spectral interval
c       + lambda_min: lower wavelength of each interval [micrometers]
c       + lambda_max: higher wavelength of each interval [micrometers]
c       + w: gas quadrature weight
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + ka_aerosol: aerosol extinction coefficient [m⁻¹]
c       + ks_aerosol: aerosol scattering coefficient [m⁻¹]
c       + g_aerosol: aerosol asymetry parameter
c     
c     I/O
      character*(Nchar_mx) datafile
      integer dim
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision Tsurf
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision Tgas(1:Nlayer_mx)
      integer Naerosol
      integer Nband
      integer Nquad(1:Nband_mx)
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision g_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
c     temp
      integer ios,ilayer,ilambda,iband,iquad,iaerosol
      double precision sum_w
c     label
      character*(Nchar_mx) label
      label='subroutine read_physical_data'

      open(21,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:',trim(datafile)
         stop
      else
         write(*,*) 'Reading physical data file: ',trim(datafile)
c     Geometry
         read(21,*) dim
         read(21,*) Nlayer
         if (Nlayer.le.0) then
            call error(label)
            write(*,*) 'Nlayer=',Nlayer
            write(*,*) 'should be positive'
            stop
         endif
         if (Nlayer.gt.Nlayer_mx) then
            call error(label)
            write(*,*) 'Nlayer=',Nlayer
            write(*,*) '> Nlayer_mx=',Nlayer_mx
            stop
         endif
         do ilayer=0,Nlayer
            read(21,*) z_grid(ilayer)
            if (z_grid(ilayer).le.0.0D+0) then
               call error(label)
               write(*,*) 'z_grid(',ilayer,')=',z_grid(ilayer)
               write(*,*) 'should be positive'
               stop
            endif
         enddo
         do ilayer=1,Nlayer
            if (z_grid(ilayer).le.z_grid(ilayer-1)) then
               call error(label)
               write(*,*) 'z_grid(',ilayer,')=',z_grid(ilayer)
               write(*,*) 'should be > z_grid(',ilayer-1,')=',z_grid(ilayer-1)
               stop
            endif
         enddo                  ! ilayer
c     Ground temperature
         read(21,*) Tsurf
         if (Tsurf.lt.0.0D+0) then
            call error(label)
            write(*,*) 'Tsurf=',Tsurf
            write(*,*) 'should be > 0'
            stop
         endif
c     Ground reflectivity
         read(21,*) Nlambda
         if (Nlambda.le.0) then
            call error(label)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) 'should be positive'
            stop
         endif
         if (Nlambda.gt.Nlambda_mx) then
            call error(label)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) '> Nlambda_mx=',Nlambda_mx
            stop
         endif
         do ilambda=1,Nlambda
            read(21,*) lambda(ilambda),reflectivity(ilambda)
            if (lambda(ilambda).le.0.0D+0) then
               call error(label)
               write(*,*) 'lambda(',ilambda,')=',lambda(ilambda)
               write(*,*) 'should be positive'
               stop
            endif
            if ((reflectivity(ilambda).lt.0.0D+0).or.(reflectivity(ilambda).gt.1.0D+0)) then
               call error(label)
               write(*,*) 'reflectivity(',ilambda,')=',reflectivity(ilambda)
               write(*,*) 'should be in the [0,1] range'
               stop
            endif
         enddo                  ! ilambda
         do ilambda=2,Nlambda
            if (lambda(ilambda).le.lambda(ilambda-1)) then
               call error(label)
               write(*,*) 'lambda(',ilambda,')=',lambda(ilambda)
               write(*,*) 'should be > lambda(',ilambda-1,')=',lambda(ilambda-1)
               stop
            endif
         enddo                  ! ilambda
c     Medium temperature
         do ilayer=1,Nlayer
            read(21,*) Tgas(ilayer)
            if (Tgas(ilayer).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Tgas(',ilayer,')=',Tgas(ilayer)
               write(*,*) 'should be positive'
               stop
            endif
         enddo                  ! ilayer
c     Number of aerosol modes
         read(21,*) Naerosol
         if (Naerosol.lt.0) then
            call error(label)
            write(*,*) 'Naerosol=',Naerosol
            write(*,*) 'should be positive'
            stop
         endif
         if (Naerosol.gt.Naerosol_mx) then
            call error(label)
            write(*,*) 'Naerosol=',Naerosol
            write(*,*) '> Naerosol_mx=',Naerosol_mx
            stop
         endif
c     Spectral grid & radiative properties
         read(21,*) Nband
         if (Nband.le.0) then
            call error(label)
            write(*,*) 'Nband=',Nband
            write(*,*) 'should be positive'
            stop
         endif
         if (Nband.gt.Nband_mx) then
            call error(label)
            write(*,*) 'Nband=',Nband
            write(*,*) '> Nband_mx=',Nband_mx
            stop
         endif
         do iband=1,Nband
            read(21,*) lambda_min(iband),lambda_max(iband)
            if (lambda_min(iband).le.0.0D+0) then
               call error(label)
               write(*,*) 'lambda_min(',iband,')=',lambda_min(iband)
               write(*,*) 'should be positive'
               stop
            endif
            if (lambda_max(iband).le.0.0D+0) then
               call error(label)
               write(*,*) 'lambda_max(',iband,')=',lambda_max(iband)
               write(*,*) 'should be positive'
               stop
            endif
            if (lambda_max(iband).le.lambda_min(iband)) then
               call error(label)
               write(*,*) 'lambda_max(',iband,')=',lambda_max(iband)
               write(*,*) 'should be > lambda_min(',iband,')=',lambda_min(iband)
               stop
            endif
            read(21,*) Nquad(iband)
            if (Nquad(iband).le.0) then
               call error(label)
               write(*,*) 'Nquad(',iband,')=',Nquad(iband)
               write(*,*) 'should be positive'
               stop
            endif
            if (Nquad(iband).gt.Nquad_mx) then
               call error(label)
               write(*,*) 'Nquad(',iband,')=',Nquad(iband)
               write(*,*) '> Nquad_mx=',Nquad_mx
               stop
            endif
            sum_w=0.0D+0
            do iquad=1,Nquad(iband)
               read(21,*) w(iband,iquad)
               if (w(iband,iquad).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'w(',iband,',',iquad,')=',w(iband,iquad)
                  write(*,*) 'should be positive'
                  stop
               endif
               sum_w=sum_w+w(iband,iquad)
               do ilayer=1,Nlayer
                  read(21,*) ka_gas(ilayer,iband,iquad)
                  if (ka_gas(ilayer,iband,iquad).lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'ka_gas(',ilayer,',',iband,',',iquad,')=',ka_gas(ilayer,iband,iquad)
                     write(*,*) 'should be positive'
                     stop
                  endif
               enddo            ! ilayer
            enddo               ! iquad
            do ilayer=1,Nlayer
               read(21,*) ks_gas(ilayer,iband)
               if (ks_gas(ilayer,iband).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'ks_gas(',ilayer,',',iband,')=',ks_gas(ilayer,iband)
                  write(*,*) 'should be positive'
                  stop
               endif
            enddo               ! ilayer
            do iaerosol=1,Naerosol
               do ilayer=1,Nlayer
                  read(21,*) ka_aerosol(iaerosol,ilayer,iband)
                  if (ka_aerosol(iaerosol,ilayer,iband).lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'ka_aerosol(',iaerosol,',',ilayer,',',iband,')=',ka_aerosol(iaerosol,ilayer,iband)
                     write(*,*) 'should be positive'
                     stop
                  endif
               enddo            ! ilayer
               do ilayer=1,Nlayer
                  read(21,*) ks_aerosol(iaerosol,ilayer,iband)
                  if (ks_aerosol(iaerosol,ilayer,iband).lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'ks_aerosol(',iaerosol,',',ilayer,',',iband,')=',ks_aerosol(iaerosol,ilayer,iband)
                     write(*,*) 'should be positive'
                     stop
                  endif
               enddo            ! ilayer
               do ilayer=1,Nlayer
                  read(21,*) g_aerosol(iaerosol,ilayer,iband)
                  if ((g_aerosol(iaerosol,ilayer,iband).lt.-1.0D+0).or.(g_aerosol(iaerosol,ilayer,iband).gt.1.0D+0)) then
                     call error(label)
                     write(*,*) 'g_aerosol(',iaerosol,',',ilayer,',',iband,')=',g_aerosol(iaerosol,ilayer,iband)
                     write(*,*) 'should be in the [-1,1] range'
                     stop
                  endif
               enddo            ! ilayer
            enddo               ! iaerosol
            if (dabs(sum_w-1.0D+0).gt.1.0D-6) then
               call error(label)
               write(*,*) 'band index:',iband
               write(*,*) 'sum of w=',sum_w
               write(*,*) 'Difference to 1 is:',dabs(sum_w-1.0D+0)
               write(*,*) 'is expected to be < 1.0D-6'
               stop
            endif
         enddo                  ! iband
      endif                     ! ios
      close(21)

      return
      end
