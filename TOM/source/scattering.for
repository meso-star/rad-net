      subroutine direction_after_scattering(dim,u,alpha,v)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to compute vector "v", based on initial direction "u" and scattering angle "alpha".
c
c     Input:
c       + dim: dimension of vectors
c       + u: initial direction
c       + alpha: scattering angle [0,pi]
c
c     Output:
c       + v: final direction
c     
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision alpha
      double precision v(1:Ndim_mx)
c     temp
      integer i
      double precision alpha1,alpha2
      double precision j1(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      double precision v1,v2
c     label
      character*(Nchar_mx) label
      label='subroutine direction_after_scattering'

c     Tests
      if ((alpha.lt.0.0D+0).or.(alpha.gt.pi)) then
         call error(label)
         write(*,*) 'alpha=',alpha
         write(*,*) 'is not between 0 and pi'
         stop
      endif
c     Tests
c     FIRST ROTATION of vector "u", by angle "alpha" around vector "j1"
      call vector_j1(dim,u,j1)
      alpha1=alpha
      call rotation(dim,u,alpha1,j1,t)
c     SECOND ROTATION of vector "t", by an angle "alpha2" (chosen uniformally between 0 and 2pi), around vector "u"
      v1=0.0D+0
      v2=2.0D+0*pi
      call sample_uniform_double(v1,v2,alpha2)
      call rotation(dim,t,alpha2,u,v)

      return
      end



      subroutine scattering_phase_function(pf,g_HG,
     &     norm_factor,mu,phase_function)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the value of the phase function for a given scattering angle
c
c     Input:
c       + pf: option for choosing the phase function that must be used
c         - pf=0: use the Rayleigh phase function
c         - pf=1: use the isotropic phase function
c         - pf=2: use the Henyey-Greenstein phase function
c       + g_HG: Henyey-Greenstein phase-function asymetry factor
c       + norm_factor: normalization factor (so that the integral over 4pi sr is equal to 1)
c       + mu: cosinus of the scattering angle
c
c     Output:
c       + phase_function: value of the phase function for 'mu'
c
c     I/O
      integer pf
      double precision g_HG
      double precision norm_factor
      double precision mu
      double precision phase_function
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine scattering_phase_function'

      if (pf.eq.0) then         ! use the Rayleigh phase-function
         call Rayleigh_phase_function(mu,phase_function)
      else if (pf.eq.1) then    ! use the isotropic phase function
         call Isotropic_phase_function(phase_function)
      else if (pf.eq.2) then    ! use the Henyey-Greenstein phase-function
         call HenyeyGreenstein_phase_function(mu,g_HG,phase_function)
      else
         call error(label)
         write(*,*) 'Bad input argument: pf=',pf
         write(*,*) 'allowed values: {0,1,2}'
         stop
      endif

      return
      end



      subroutine Rayleigh_phase_function(mu,phase_function)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to compute the value of the Rayleigh phase function
c     for a given angle
c
c     Input:
c       + mu: cos(alpha) [-1;1]
c
c     Output:
c       + phase_function: value of the phase function for alpha
c
c     I/O
      double precision mu
      double precision phase_function
c     label
      character*(Nchar_mx) label
      label='subroutine Rayleigh_phase_function'

c     The following value of the Rayleigh phase function
c     is normalized over 4pi sr:
      phase_function=3.0D+0/(16.0D+0*pi)*(1.0D+0+mu**2.0D+0)
c     The corresponding sampling of angles (theta, phi) is performed using
c     the following probability density functions:
c     p(mu)=3/8*(1+mu^2) and then mu=cos(theta); int(p(mu)d(mu),-1,1)=1
c     p(phi)=1/(2pi); int(p(phi)d(phi),0,2pi)=1

      return
      end



      subroutine HenyeyGreenstein_phase_function(mu,g_HG,phase_function)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to compute the value of the Henyey-Greenstein phase function
c     for a given angle
c
c     Input:
c       + mu: cos(alpha) [-1;1]
c       + g_HG: Henyey-Greenstein phase function asymetry parameter (-1;1)
c
c     Output:
c       + phase_function: value of the phase function for alpha
c
c     I/O
      double precision mu,g_HG
      double precision phase_function
c     label
      character*(Nchar_mx) label
      label='subroutine HenyeyGreenstein_phase_function'

      if ((g_HG.eq.1.0D+0).and.(mu.eq.1.0D+0)) then
         phase_function=1.0D+0/(4.0D+0*pi)
      else
         phase_function=1.0D+0/(4.0D+0*pi)
     &        *(1.0D+0-g_HG**2.0D+0)
     &        /(1.0D+0+g_HG**2.0D+0-2.0D+0*g_HG*mu)**1.5D+0
      endif

      return
      end



      subroutine Isotropic_phase_function(phase_function)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to compute the value of the isotropic phase function
c     for a given angle
c
c     Input:
c      
c     Output:
c       + phase_function: value of the phase function for alpha
c
c     I/O
      double precision phase_function
c     label
      character*(Nchar_mx) label
      label='subroutine Isotropic_phase_function'

      phase_function=1.0D+0/(4.0D+0*pi)
 
      return
      end
