      subroutine detect_transit(dim,center,theta_obs,phi_obs,theta_source,phi_source,Rs,D,Ds,Nlayer,z_grid,
     &     x_obs,x_source,transit)
      implicit none
      include 'max.inc'
c     
c     Purpose: to detect a planetary transit configuration
c     
c     Input:
c       + dim: dimension of space
c       + center: center of the spherical body [m,m,m]
c       + theta_obs: observer's latitude in local spherical body referential [rad]
c       + phi_obs: observer's longitude in local spherical body referential [rad]
c       + theta_source: latitude of star in local spherical body referential [rad]
c       + phi_source: longitude of star in local spherical body referential [rad]
c       + Rs: radius of star [m]
c       + D: distance to observer [m]
c       + Ds: distance to star [m]
c       + Nlayer: number of vertical layers
c       + z_grid: vertical grid [m]
c     
c     Output:
c       + x_obs: observer's position [m,m,m]
c       + x_source: source position [m,m,m]
c       + transit: true if transit mode
c     
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision theta_obs
      double precision phi_obs
      double precision theta_source
      double precision phi_source
      double precision Rs
      double precision D
      double precision Ds
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision x_obs(1:Ndim_mx)
      double precision x_source(1:Ndim_mx)
      logical transit
c     temp
      double precision u_obs(1:Ndim_mx)
      double precision u_source (1:Ndim_mx)
      double precision uspher(1:Ndim_mx)
      double precision prod
      double precision delta_min,delta_max
c     label
      character*(Nchar_mx) label
      label='subroutine detect_transit'

c     Observation position in the referential of the sphere:
c     direction from center of the sphere to observer
      uspher(1)=1.0D+0
      uspher(2)=theta_obs
      uspher(3)=phi_obs
      call spher2cart(dim,uspher,u_obs)
      call flight(dim,center,D,u_obs,x_obs)
c     Source position in the referential of the sphere:
c     direction from center of the sphere to center of the source
      uspher(1)=1.0D+0
      uspher(2)=theta_source
      uspher(3)=phi_source
      call spher2cart(dim,uspher,u_source)
      call flight(dim,center,Ds,u_source,x_source)
c     Test for transit mode
      call scalar_product(dim,u_obs,u_source,prod)
      if (dabs(prod+1.0D+0).le.1.0D-10) then
         transit=.true.
      else
         transit=.false.
      endif

      if (transit) then
         write(*,*) 'Transit mode detected'
         if (Rs.lt.z_grid(0)) then
            write(*,*) 'However:'
            write(*,*) 'radius of the planet is:',z_grid(0)*1.0D-3,' km'
            write(*,*) 'should be < radius of the star:',Rs*1.0D-3,' km'
         else
c     delta_min is the minium distance between sphere center and observer
c     so that the star is visible when sampling the planetary limbs
c     delta_max is the distance between sphere center and observer that makes
c     possible to have the star in background for all direction sampled in
c     planetary limbs
            delta_min=Rs*Ds/(Rs-z_grid(0))
            delta_max=Rs*Ds/(Rs-z_grid(Nlayer))
            if (D.lt.delta_min) then
c     When D < delta_min, no direction sampled in the limbs will ever reach the star
               write(*,*) 'However:'
               write(*,*) 'Distance to observer:',D*1.0D-3,' km'
               write(*,*) 'is < delta_min :',delta_min*1.0D-3,' km'
            else if ((D.ge.delta_min).and.(D.lt.delta_max)) then
c     when delta_min < D < delta_max, not all directions sampled in the limbs will reach the star
               write(*,*) 'However:'
               write(*,*) 'Distance to observer:',D*1.0D-3,' km'
               write(*,*) 'is > delta_min :',delta_min*1.0D-3,' km'
               write(*,*) 'but < delta_max :',delta_max*1.0D-3,' km'
            else
c     If D > delta_max, all directions sampled in the limbs will reach the star
               write(*,*) 'transit configuration is correct'
            endif
         endif
      endif

      return
      end



      subroutine transit_average_intensity(dim,center,x_obs,x_source,Nlayer,z_grid,D,Rs,Ds,Ts,lambda_inf,lambda_sup,Lobs)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the average intensity
c     
c     Input:
c       + dim: dimension of space
c       + center: center of the spherical body [m,m,m]
c       + x_obs: observer's position [m,m,m]
c       + x_source: source position [m,m,m]
c       + Nlayer: number of vertical layers
c       + z_grid: vertical grid [m]
c       + D: distance between spherical body center and observation position [m]
c       + Ds: distance to star [m]
c       + Rs: radius of star [m]
c       + Ts: brightness temperature of the star [K]
c       + lambda_inf: lower limit for spectral integration [µm]
c       + lambda_sup: lower limit for spectral integration [µm]
c     
c     Output:
c       + Lobs: average intensity observed in transit mode [W/m²/sr]
c     
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision x_obs(1:Ndim_mx)
      double precision x_source(1:Ndim_mx)
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision D
      double precision Rs
      double precision Ds
      double precision Ts
      double precision lambda_inf
      double precision lambda_sup
      double precision Lobs
c     temp
      double precision delta_min,delta_max
      double precision theta_min,theta_source,theta_max
      double precision omega_min,omega_source,omega_max
      double precision alpha
c     functions
      double precision planck_int_wavlng
c     label
      character*(Nchar_mx) label
      label='subroutine transit_average_intensity'

      if (Rs.lt.z_grid(0)) then
         Lobs=0.0D+0
      else
         delta_min=Rs*Ds/(Rs-z_grid(0))
         delta_max=Rs*Ds/(Rs-z_grid(Nlayer))
         if (D.lt.delta_min) then
            Lobs=0.0D+0
         else
c     solid angle under which the solid core is seen from x_obs
            call cone_sphere_solid_angle(z_grid(0),D,theta_min,omega_min)
c     solid angle under which the atmosphere is seen from x_obs
            call cone_sphere_solid_angle(z_grid(Nlayer),D,theta_max,omega_max)
c     solid angle under which the source is seen from x_obs
            call cone_sphere_solid_angle(Rs,D+Ds,theta_source,omega_source)
            if (D.ge.delta_max) then
               alpha=1.0D+0
            else
               alpha=(omega_source-omega_min)/(omega_max-omega_min)
            endif
            Lobs=planck_int_wavlng(lambda_inf,lambda_sup,Ts)*alpha
         endif
      endif

      return
      end
