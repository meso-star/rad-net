      subroutine total_extinction_coefficient(layer_index,band_index,quad_index,Nlayer,ka_gas,ks_gas,Naerosol,ka_aerosol,ks_aerosol,kext)
      use ieee_arithmetic
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the total extinction coefficient for a given layer, band and quadrature point
c     
c     Input:
c       + layer_index: atmospheric layer index
c       + band_index: spectral narrowband index
c       + quad_index: quadrature point index
c       + Nlayer: number of vertical layers
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + Naerosol: number of aerosols
c       + ka_aerosol: aerosol extinction coefficient [m⁻¹]
c       + ks_aerosol: aerosol scattering coefficient [m⁻¹]
c     
c     Output:
c       + kext: total extinction coefficient [m⁻¹]
c     
c     I/O
      integer layer_index
      integer band_index
      integer quad_index
      integer Nlayer
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      integer Naerosol
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision kext
c     temp
      integer iaerosol
c     label
      character*(Nchar_mx) label
      label='subroutine total_extinction_coefficient'

      if (layer_index.eq.0) then
         kext=ieee_value(kext,ieee_positive_inf)
      else if (layer_index.eq.Nlayer+1) then
         kext=0.0D+0
      else
         kext=ka_gas(layer_index,band_index,quad_index)+ks_gas(layer_index,band_index)
         do iaerosol=1,Naerosol
            kext=kext+ka_aerosol(iaerosol,layer_index,band_index)+ks_aerosol(iaerosol,layer_index,band_index)
         enddo                  ! iaerosol
      endif

      return
      end


      
      subroutine total_absorption_coefficient(layer_index,band_index,quad_index,Nlayer,ka_gas,Naerosol,ka_aerosol,ka)
      use ieee_arithmetic
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the total absorption coefficient for a given layer, band and quadrature point
c     
c     Input:
c       + layer_index: atmospheric layer index
c       + band_index: spectral narrowband index
c       + quad_index: quadrature point index
c       + Nlayer: number of vertical layers
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + Naerosol: number of aerosols
c       + ka_aerosol: aerosol absorption coefficient [m⁻¹]
c     
c     Output:
c       + ka: total absorption coefficient [m⁻¹]
c     
c     I/O
      integer layer_index
      integer band_index
      integer quad_index
      integer Nlayer
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      integer Naerosol
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ka
c     temp
      integer iaerosol
c     label
      character*(Nchar_mx) label
      label='subroutine total_absorption_coefficient'

      if (layer_index.eq.0) then
         ka=ieee_value(ka,ieee_positive_inf)
      else if (layer_index.eq.Nlayer+1) then
         ka=0.0D+0
      else
         ka=ka_gas(layer_index,band_index,quad_index)
         do iaerosol=1,Naerosol
            ka=ka+ka_aerosol(iaerosol,layer_index,band_index)
         enddo                  ! iaerosol
      endif
      
      return
      end



      subroutine total_scattering_coefficient(layer_index,band_index,Nlayer,ks_gas,Naerosol,ks_aerosol,ks)
      use ieee_arithmetic
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the total scattering coefficient for a given layer and band 
c     
c     Input:
c       + layer_index: atmospheric layer index
c       + band_index: spectral narrowband index
c       + Nlayer: number of vertical layers
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + Naerosol: number of aerosols
c       + ks_aerosol: aerosol scattering coefficient [m⁻¹]
c     
c     Output:
c       + ks: total absorption coefficient [m⁻¹]
c     
c     I/O
      integer layer_index
      integer band_index
      integer Nlayer
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      integer Naerosol
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks
c     temp
      integer iaerosol
c     label
      character*(Nchar_mx) label
      label='subroutine total_scattering_coefficient'

      if (layer_index.eq.0) then
         ks=ieee_value(ks,ieee_positive_inf)
      else if (layer_index.eq.Nlayer+1) then
         ks=0.0D+0
      else
         ks=ks_gas(layer_index,band_index)
         do iaerosol=1,Naerosol
            ks=ks+ks_aerosol(iaerosol,layer_index,band_index)
         enddo                  ! iaerosol
      endif
      
      return
      end
