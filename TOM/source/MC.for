      subroutine MC_average_intensity(dim,center,x_obs,x_source,Nlayer,z_grid,D,Ds,Rs,Ts,theta_s,phi_s,
     &     Tsurf,Nlambda_reflectivity,lambda_reflectivity,reflectivity,Tgas,Naerosol,Nband,Nquad,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,ka_aerosol,ks_aerosol,g_aerosol,
     &     source,lambda_inf,lambda_sup,external_source_type,Nlambda_source,source_intensity,
     &     Nevent,transit,theta_target,phi_target,D_target,u_up,solid_angle_type,hfov,aspect_ratio,
     &     Lobs,inc_Lobs,Nfail,omega_obs)
      use ieee_arithmetic
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the SW flux at a given position, in a given solid angle
c     
c     Input:
c       + dim: dimension of space
c       + center: center of the sphere [m,m,m]
c       + x_obs: observer's position [m,m,m]
c       + x_source: source position [m,m,m]
c       + Nlayer: number of vertical layers
c       + z_grid: vertical grid [m]
c       + D: distance between spherical body center and observation position [m]
c       + Ds: distance to star [m]
c       + Rs: radius of star [m]
c       + Ts: brightness temperature of the star [K]
c       + theta_s: latitude of star in local spherical body referential [rad]
c       + phi_s: longitude of star in local spherical body referential [rad]
c       + Tsurf: surface temperature [K]
c       + Nlambda_reflectivity: number of wavelength points for the definition of the ground reflectivity
c       + lambda_reflectivity: values of the wavelength for the definition of the ground reflectivity
c       + reflectivity: values of the ground reflectivity
c       + Tgas: gas temperature [K]
c       + Naerosol: number of areosol modes
c       + Nband: number of spectral intervals
c       + Nquad: quadrature order, for each spectral interval
c       + lambda_min: lower wavelength of each interval [micrometers]
c       + lambda_max: higher wavelength of each interval [micrometers]
c       + w: gas quadrature weight
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + ka_aerosol: aerosol extinction coefficient [m⁻¹]
c       + ks_aerosol: aerosol scattering coefficient [m⁻¹]
c       + g_aerosol: aerosol asymetry parameter
c       + source: radiation source [internal / external]
c       + lambda_inf: lower limit for spectral integration [µm]
c       + lambda_sup: lower limit for spectral integration [µm]
c       + external_source_type: type of external source [1: Planck; 2: detailled intensity spectrum]
c       + Nlambda_source: number of wavelength/intensity values
c       + source_intensity: values of the wavelength [nm] and associated solar intensity [W/m²/sr/nm]
c       + Nevent: number of statistical events
c       + transit: T for transit mode
c       + theta_target: target's latitude [rad]
c       + phi_target: target's longitude [rad]
c       + D_target: distance between spherical body center and target position [m]
c       + u_up: UP direction for the camera [m, m, m]
c       + solid_angle_type: (1)for a conical integration solid angle, (2) for rectangular
c       + hfov: horizontal FOV in the case of a rectangular solid angle [rad]
c       + aspect_ratio: retangular solid angle horizontal/vertical aspect ratio
c     
c     Output:
c       + Lobs: average intensity [W/m²/sr]
c       + inc_Lobs: standard deviation over the average intensity [W/m²]
c       + Nfail: number of path failures
c       + omega_obs: observation solid angle [sr]
c     
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision x_obs(1:Ndim_mx)
      double precision x_source(1:Ndim_mx)
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision D
      double precision Ds
      double precision Rs
      double precision Ts
      double precision theta_s
      double precision phi_s
      double precision Tsurf
      integer Nlambda_reflectivity
      double precision lambda_reflectivity(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision Tgas(1:Nlayer_mx)
      integer Naerosol
      integer Nband
      integer Nquad(1:Nband_mx)
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision g_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      character*(Nchar_mx) source
      double precision lambda_inf,lambda_sup
      integer external_source_type
      integer*8 Nlambda_source
      double precision source_intensity(1:Nlambda_sun_mx,1:2)
      integer Nevent
      logical transit
      double precision theta_target
      double precision phi_target
      double precision D_target
      double precision u_up(1:Ndim_mx)
      integer solid_angle_type
      double precision hfov
      double precision aspect_ratio
      double precision Lobs
      double precision inc_Lobs
      integer Nfail
      double precision omega_obs
c     temp
      logical debug
      integer event
      double precision weight
      double precision sum_w,sum_w2
      double precision mean
      double precision variance
      double precision std_dev
      double precision Tref
      integer Ntry
      integer Nb
      double precision band_pdf(1:Nproba_mx)
      double precision band_cdf(0:Nproba_mx)
      integer Nsi
      double precision si_lambda(1:Nproba_mx,1:2)
      double precision si_intensity(1:Nproba_mx,1:2)
      double precision si_pdf(1:Nproba_mx)
      double precision si_cdf(0:Nproba_mx)
      double precision  spectral_interval_pdf
      integer Nq
      double precision quad_pdf(1:Nproba_mx)
      double precision quad_cdf(0:Nproba_mx)
      integer band_index,quad_index
      integer interval_index
      double precision lambda1,lambda2,i1,i2
      logical convergence
      double precision lambda,lambda_pdf
      double precision theta_min,omega_min
      double precision theta_max,omega_max
      double precision theta,phi
      double precision u0(1:Ndim_mx)
      double precision intensity
      logical valid_path
      integer n_int
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k1,k2
      integer iband
      logical err,band_found
      double precision Is
      logical use_HR_sun_spectrum
      double precision x_target_spher(1:Ndim_mx)
      double precision x_target(1:Ndim_mx)
      double precision OP(1:Ndim_mx)
      double precision u_OP(1:Ndim_mx)
      double precision up(1:Ndim_mx)
      double precision j_OP(1:Ndim_mx)
      double precision M_PO(1:Ndim_mx,1:Ndim_mx)
      double precision vfov
      double precision u_Ro(1:Ndim_mx)
c     progress display
      integer len,j,i
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine MC_average_intensity'

      use_HR_sun_spectrum=.false.
      if (trim(source).eq.'internal') then
         Tref=Tsurf
         call build_band_pdf(Nband,lambda_min,lambda_max,Tref,lambda_inf,lambda_sup,
     &        Nb,band_pdf,band_cdf)
      else                      ! source=external
         if (external_source_type.eq.1) then
            Tref=Ts
            call build_band_pdf(Nband,lambda_min,lambda_max,Tref,lambda_inf,lambda_sup,
     &           Nb,band_pdf,band_cdf)
         else
            call build_lambda_pdf(Nlambda_source,source_intensity,lambda_inf,lambda_sup,
     &           Nsi,si_lambda,si_intensity,si_pdf,si_cdf)
            use_HR_sun_spectrum=.true.
         endif
      endif
      if (solid_angle_type.eq.1) then ! conical solid angle
         call cone_sphere_solid_angle(z_grid(0),D,theta_min,omega_min)
         call cone_sphere_solid_angle(z_grid(Nlayer),D,theta_max,omega_max)         
         if (transit) then
            omega_obs=2.0D+0*pi*(dcos(theta_min)-dcos(theta_max))
         else
            omega_obs=2.0D+0*pi*(1.0D+0-dcos(theta_max))
         endif
      else if (solid_angle_type.eq.2) then ! rectangular solid angle
         vfov=hfov/aspect_ratio ! [rad]         
         omega_obs=2.0D+0*hfov*dsin(vfov/2.0D+0)
c     ------------------------------------------------------------------------------------------
c     M_PO rotation matrix
c     ------------------------------------------------------------------------------------------
c     u_OP is the main observation direction, also the X-axis in the observation referential
         x_target_spher(1)=D_target
         x_target_spher(2)=theta_target
         x_target_spher(3)=phi_target
         call spher2cart(dim,x_target_spher,x_target)
         call substract_vectors(dim,x_target,x_obs,OP)
         call normalize_vector(dim,OP,u_OP)
c     u_OP is the normal to the detector plane: compute UP direction
         call vector_projection(dim,u_OP,u_up,up) ! up is the local Z-axis in the observation referential
c     j_OP is the third axis (Y-aixs) in the observation referentiel
         call vector_product_normalized(dim,up,u_OP,j_OP)
c     M_PO is the rotation matrix that converts a direction from the observation referential
c     to the planetary referential: u(Rp)=M_PO.u(Ro)
         do j=1,dim
            M_PO(j,1)=u_OP(j)
            M_PO(j,2)=j_OP(j)
            M_PO(j,3)=up(j)
         enddo                  ! j
c     ------------------------------------------------------------------------------------------
      endif                     ! solid_angle_type
      
      write(*,*) 'MC estimation...'
c     progress display ---
      ntot=Nevent
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     --- progress display
      Nfail=0
      sum_w=0.0D+0
      sum_w2=0.0D+0
      do event=1,Nevent
c     Debug
c         write(*,*) 'event=',event
c         if (event.eq.30034) then
c            debug=.true.
c         else
            debug=.false.
c         endif
c     Debug
         Ntry=0
 111     continue
         Ntry=Ntry+1
         if (Ntry.gt.Ntry_mx) then ! maximum "Ntry_mx" tries per realization
            call error(label)
            write(*,*) 'Number of retries was exceeded'
            stop
         endif
         if ((trim(source).eq.'internal').or.((trim(source).eq.'external').and.(external_source_type.eq.1))) then
c     In the case of LW or SW + sampling according to Planck
c     Sample band
            call sample_integer(Nb,band_cdf,band_index)
            spectral_interval_pdf=band_pdf(band_index)
c     Sample wavelength
            call sample_wavelength_according_to_Planck(lambda_inf,lambda_sup,
     &           lambda_min(band_index),lambda_max(band_index),
     &           Tref,convergence,lambda,lambda_pdf) ! lambda [µm], lambda_pdf [µm⁻¹] 
            if (.not.convergence) then
               call error(label)
               write(*,*) 'event:',event
               write(*,*) 'could not sample wavelength'
               stop
            endif
         else
c     In the case of SW + sampling according to intensity signal
c     Debug
c            write(*,*) 'Sampling spectral interval:'
c     Debug
            call sample_integer(Nsi,si_cdf,interval_index)
            spectral_interval_pdf=si_pdf(interval_index)
            lambda1=si_lambda(interval_index,1) ! [nm]
            lambda2=si_lambda(interval_index,2) ! [nm]
            i1=si_intensity(interval_index,1) ! [W/m²/sr/nm]
            i2=si_intensity(interval_index,2) ! [W/m²/sr/nm]
c     Debug
c            write(*,*) 'Sampling spectral lambda:'
c     Debug
            call sample_wavelength_according_to_intensity0(lambda1,lambda2,i1,i2,lambda,lambda_pdf) ! lambda [µm], lambda_pdf [µm⁻¹]
c     Debug
c            write(*,*) 'lambda1=',lambda1,' nm'
c            write(*,*) 'lambda2=',lambda2,' nm'
c            write(*,*) 'Delta(lambda)=',lambda2-lambda1,' nm'
c            write(*,*) 'spectral_interval_pdf=',spectral_interval_pdf
c            write(*,*) 'lambda_pdf=',lambda_pdf*1.0D-3,' nm⁻¹'
c            write(*,*) 'pdf(lambda)=',spectral_interval_pdf*lambda_pdf*1.0D-3,' nm⁻¹'
c            stop
c     Debug
            Is=(i1+(i2-i1)/(lambda2-lambda1)*(lambda*1.0D+3-lambda1))*1.0D+3 ! [W/m²/sr/µm]
c     Looking for the gas band index, for sampling a quadrature weight
            band_found=.false.
            do iband=1,Nband
               if ((lambda.ge.lambda_min(iband)).and.(lambda.le.lambda_max(iband))) then
                  band_found=.true.
                  band_index=iband
                  goto 222
               endif
            enddo               ! iband
 222        continue
            if (.not.band_found) then
               call error(label)
               write(*,*) 'Band index not found for lambda=',lambda,' µm'
               write(*,*) 'band / lambda_min / lambda_max'
               do iband=1,Nband
                  write(*,*) iband,lambda_min(iband),lambda_max(iband)
               enddo            ! iband
               stop
            endif               ! band_found=F
         endif                  ! source / external_source_type
c     Build pdf for the current band
         call build_quad_pdf(Nquad,w,band_index,Nq,quad_pdf,quad_cdf)
c     Sample quadrature point
         call sample_integer(Nq,quad_cdf,quad_index)
c
c     Sample the initial propagation direction, from the observation position
         if (solid_angle_type.eq.1) then ! conical solid angle
c     Sample direction in observation cone
            if (transit) then
               call sample_cone_half_angle_general(theta_min,theta_max,theta,phi) ! solid angle of the atmospheric limb
            else
c               call sample_cone_half_angle_general(0.0D+0,theta_min,theta,phi) ! solid angle of the solid core
               call sample_cone_half_angle_general(0.0D+0,theta_max,theta,phi) ! solid angle of the solid core
            endif
            call angles_to_observation_direction(dim,center,x_obs,theta,phi,u0)
         else if (solid_angle_type.eq.2) then ! rectangular solid angle
            call sample_rectangular_solid_angle(hfov,vfov,u_Ro)
            call matrix_vector(dim,M_PO,u_Ro,u0)
         endif                  ! solid_angle_type
c     Debug
         if (debug) then
            write(*,*) 'Observation position: x_obs=',(x_obs(j),j=1,dim)
            write(*,*) 'u0=',u0
         endif                  ! debug
c     Debug
         
c     MC realization
         call intensity_single_realization(debug,dim,center,x_source,x_obs,u0,lambda,band_index,quad_index,
     &        Nlayer,z_grid,D,Ds,Rs,Ts,theta_s,phi_s,
     &        Tsurf,Nlambda_reflectivity,lambda_reflectivity,reflectivity,Tgas,Naerosol,Nband,Nquad,lambda_min,lambda_max,w,
     &        ka_gas,ks_gas,ka_aerosol,ks_aerosol,g_aerosol,source,use_HR_sun_spectrum,Is,
     &        intensity,valid_path) ! intensity [W/m²/sr/µm]
         if (valid_path) then
c     "band_pdf" has no unit
c     "lambda_pdf" is in unit of [µm⁻¹]
c     "intensity" is un unit of [W/m²/sr/µm]
            weight=intensity/(spectral_interval_pdf*lambda_pdf) ! [W/m²/sr]
            sum_w=sum_w+weight
            sum_w2=sum_w2+weight**2.0D+0
         else
            Nfail=Nfail+1
            goto 111
         endif
c     
c     progress display ---
         ndone=ndone+1
         fdone=dble(ndone)/dble(ntot)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif
c     --- progress display
      enddo                     ! event
c      
c     progress display ---
      write(*,*)
c     --- progress display
      
      call statistics(Nevent,sum_w,sum_w2,mean,variance,std_dev)
      Lobs=mean                 ! [W/m²/sr]
      inc_Lobs=std_dev          ! [W/m²/sr]

      return
      end

      

      subroutine intensity_single_realization(debug,dim,center,x_source,x0,u0,lambda,band_index,quad_index,
     &     Nlayer,z_grid,D,Ds,Rs,Ts,theta_s,phi_s,
     &     Tsurf,Nlambda_reflectivity,lambda_reflectivity,reflectivity,Tgas,Naerosol,Nband,Nquad,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,ka_aerosol,ks_aerosol,g_aerosol,source,use_HR_sun_spectrum,Is,
     &     weight,valid_path)
      use ieee_arithmetic
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to perform a single realization of the SW intensity at a given position, in a given direction
c     
c     Input:
c       + dim: dimension of space
c       + center: center of the sphere [m,m,m]
c       + x_source: source position [m,m,m]
c       + x0: position [m,m,m]
c       + u0: direction [m,m,m]
c       + lambda: wavelength [µm]
c       + band_index: spectral narrowband index
c       + quad_index: quadrature point index
c       + Nlayer: number of vertical layers
c       + z_grid: vertical grid [m]
c       + D: distance between spherical body center and observation position [m]
c       + Ds: distance to star [m]
c       + Rs: radius of star [m]
c       + Ts: brightness temperature of the star [K]
c       + theta_s: latitude of star in local spherical body referential [rad]
c       + phi_s: longitude of star in local spherical body referential [rad]
c       + Tsurf: surface temperature [K]
c       + Nlambda_reflectivity: number of wavelength points for the definition of the ground reflectivity
c       + lambda_reflectivity: values of the wavelength for the definition of the ground reflectivity
c       + reflectivity: values of the ground reflectivity
c       + Tgas: gas temperature [K]
c       + Naerosol: number of areosol modes
c       + Nband: number of spectral intervals
c       + Nquad: quadrature order, for each spectral interval
c       + lambda_min: lower wavelength of each interval [micrometers]
c       + lambda_max: higher wavelength of each interval [micrometers]
c       + w: gas quadrature weight
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + ka_aerosol: aerosol extinction coefficient [m⁻¹]
c       + ks_aerosol: aerosol scattering coefficient [m⁻¹]
c       + g_aerosol: aerosol asymetry parameter
c       + source: radiation source [internal / external]
c       + use_HR_sun_spectrum: T is using high-definition solar source
c       + Is: solar intensity to use when use_HR_sun_spectr=T [W/m²/sr/µm]
c     
c     Output:
c       + weight: MC weight [W/m²/sr/µm]
c       + valid_path: T if path is valid
c     
c     I/O
      logical debug
      integer dim
      double precision center(1:Ndim_mx)
      double precision x_source(1:Ndim_mx)
      double precision x0(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision lambda
      integer band_index
      integer quad_index
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision D
      double precision Ds
      double precision Rs
      double precision Ts
      double precision theta_s
      double precision phi_s
      double precision Tsurf
      integer Nlambda_reflectivity
      double precision lambda_reflectivity(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision Tgas(1:Nlayer_mx)
      integer Naerosol
      integer Nband
      integer Nquad(1:Nband_mx)
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision g_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      character*(Nchar_mx) source
      logical use_HR_sun_spectrum
      double precision Is
      double precision weight
      logical valid_path
c     temp
      integer i,j
      double precision d2int
      logical is_in_layer
      integer layer_index
      logical is_at_interface
      integer interface_index,int_idx
      logical intersection_found,err
      double precision Isource
      double precision theta_source,omega_source
      double precision theta,phi
      double precision x_int(1:Ndim_mx)
      double precision n_xint(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision u_bs(1:Ndim_mx)
      double precision us(1:Ndim_mx)
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
      double precision prod
      logical valid_transmissivity
      double precision solar_transmissivity
      integer current_layer,next_layer
      logical keep_running
      double precision ka,ks,kext
      double precision length,pdf_length
      double precision r
      double precision cdf_aerosol(0:Nproba_mx)
      integer iaerosol
      double precision scattering_theta
      integer pf
      double precision gHG
      double precision mu,phase_function
      double precision rho,epsilon,pdf_u
c     functions
      double precision Blambda
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine intensity_single_realization'

c     Debug
      if (debug) then
         write(*,*) trim(label),': in'
      endif                     ! debug
c     Debug

      valid_path=.true.
      if (use_HR_sun_spectrum) then
c     use the provided solar intensity 
         Isource=Is             ! [W/m²/sr/µm]
      else
c     use Plack specific intensity @ (Ts,lambda)
         Isource=Blambda(Ts,lambda) ! [W/m²/sr/µm]
      endif
c     epsilon: uniform emissivity of the ground @ lambda
      call spectral_reflectivity(Nlambda_reflectivity,lambda_reflectivity,reflectivity,lambda,rho)
      epsilon=1.0D+0-rho        ! emissivity
c     omega_source: solid angle under which the source is perceived [sr]
      call source_solid_angle(debug,dim,x0,x_source,Rs,theta_source,omega_source)
c     Debug
      if (debug) then
         write(*,*) 'band_index=',band_index
         write(*,*) 'lambda_min=',lambda_min(band_index)
         write(*,*) 'lambda_max=',lambda_max(band_index)
         write(*,*) 'lambda=',lambda
         write(*,*) 'quad_index=',quad_index
         write(*,*) 'rho=',rho,' epsilon=',epsilon
         write(*,*) 'theta_source=',theta_source,' rad'
         write(*,*) 'omega_source=',omega_source,' sr'
      endif                     ! debug
c     Debug

      if (trim(source).eq.'external') then
c     -----------------------------------------------------------------------
c     DIRECT COMPONENT
c     -----------------------------------------------------------------------
c     Debug
         if (debug) then
            write(*,*) '============================================'
            write(*,*) 'Evaluation of the direct component'
            write(*,*) '============================================'
         endif                  ! debug
c     Debug
         call transmission_to_source(debug,dim,x0,u0,center,x_source,Nlayer,z_grid,D,Ds,Rs,theta_s,phi_s,
     &        ka_gas,ks_gas,Naerosol,ka_aerosol,ks_aerosol,band_index,quad_index,
     &        valid_transmissivity,solar_transmissivity) ! transmissivity=0 if no direct LOS with the source
         if (.not.valid_transmissivity) then
            valid_path=.false.
            goto 666
         endif
c     Debug
         if (debug) then
            write(*,*) 'From x0=',(x0(j),j=1,dim)
            write(*,*) 'solar_transmissivity=',solar_transmissivity
         endif                  ! debug
c     Debug
         weight=Isource*solar_transmissivity
      else                      ! source=internal
         weight=0.0D+0
      endif

c     -----------------------------------------------------------------------
c     DIFFUSE COMPONENT
c     -----------------------------------------------------------------------
c     Debug
      if (debug) then
         write(*,*) '============================================'
         write(*,*) 'Evaluation of the diffuse component'
         write(*,*) '============================================'
      endif                     ! debug
c     Debug
      call locate_position(dim,Nlayer,z_grid,center,x0,
     &     is_in_layer,layer_index,is_at_interface,int_idx)
c     Debug
      if (debug) then
         write(*,*) 'is_in_layer=',is_in_layer
         if (is_in_layer) then
            write(*,*) 'layer_index=',layer_index
         endif
      endif                     ! debug
c     Debug
      
      if (is_in_layer) then
         if (layer_index.eq.Nlayer+1) then
c     intersection
            call next_grid_intersection(.false.,dim,
     &           x0,u0,center,Nlayer,z_grid,
     &           err,intersection_found,x_int,d2int,interface_index)
            if (err) then
               valid_path=.false.
               goto 666
            endif
            if (.not.intersection_found) then
               goto 666         ! skip the whole path-tracing procedure
            else
               call copy_vector(dim,x_int,x) ! intialize position: x
               current_layer=Nlayer !  index of the current atmospheric layer
            endif
         else                   ! layer_index=[1,Nlayer]
            current_layer=layer_index
            call copy_vector(dim,x0,x) ! intialize position: x
         endif
      else if (is_at_interface) then
c     intersection
         call next_grid_intersection(.false.,dim,
     &        x0,u0,center,Nlayer,z_grid,
     &        err,intersection_found,x_int,d2int,interface_index)
         if (err) then
            valid_path=.false.
            goto 666
         endif
         if (.not.intersection_found) then
            goto 666            ! skip the whole path-tracing procedure
         else
            if (interface_index.eq.int_idx+1) then
               current_layer=interface_index
            else if (interface_index.eq.int_idx-1) then
               current_layer=int_idx
            else
               call error(label)
               write(*,*) 'LOS: x0=',(x0(j),j=1,dim)
               write(*,*) 'u0=',(u0(j),j=1,dim)
               write(*,*) 'has been located at interface int_idx=',int_idx
               write(*,*) 'Intersection: interface_index=',interface_index
               write(*,*) 'inconsistent'
               stop
            endif
            call copy_vector(dim,x0,x) ! intialize position: x
         endif                  ! intersection_found
      endif                     ! is_in_layer or is_at_interface
      call copy_vector(dim,u0,u) ! initialize direction: u
c     Debug
      if (debug) then
         write(*,*) 'x=',(x(j),j=1,dim)
         write(*,*) 'u=',(u(j),j=1,dim)
         write(*,*) 'current_layer=',current_layer
      endif                     ! debug
c     Debug
c     
      keep_running=.true.
      do while (keep_running)
c     Debug
         if (debug) then
            write(*,*) 'Calling next_grid_intersection from:'
            write(*,*) 'x=',(x(j),j=1,dim)
            write(*,*) 'u=',(u(j),j=1,dim)
            endif               ! debug
c     Debug         
         call next_grid_intersection(debug,dim,
     &        x,u,center,Nlayer,z_grid,
     &        err,intersection_found,x_int,d2int,interface_index)
         if (err) then
            valid_path=.false.
            goto 666
         endif
         if (intersection_found) then
            if (interface_index.eq.current_layer) then
               next_layer=current_layer+1
            else if (interface_index.eq.current_layer-1) then
               next_layer=current_layer-1
            else
               valid_path=.false.
               keep_running=.false.
               goto 665
            endif
c     Debug
            if (debug) then
               write(*,*) 'x=',(x(j),j=1,dim)
               write(*,*) 'current_layer=',current_layer
               write(*,*) 'next_layer=',next_layer
               write(*,*) 'd2int=',d2int
            endif               ! debug
c     Debug
c     Get radiative properties in current layer
            call total_absorption_coefficient(current_layer,band_index,quad_index,Nlayer,ka_gas,Naerosol,ka_aerosol,ka)
            call total_scattering_coefficient(current_layer,band_index,Nlayer,ks_gas,Naerosol,ks_aerosol,ks)
            kext=ka+ks
c     Sample a length "length" according to kext*exp(-kext*length) over [0,+infty)
            call sample_length_unlimited(kext,length,pdf_length)
c     Debug
            if (debug) then
               write(*,*) 'kext=',kext,' length=',length
            endif               ! debug
c     Debug
c     Two possibilities: length < d2int or length > d2int
            if (length.lt.d2int) then
c     Debug
               if (debug) then
                  write(*,*) '< d2int=',d2int
                  write(*,*) 'ka=',ka
               endif            ! debug
c     Debug
c     Collision event @ position x+length.u
               call flight(dim,x,length,u,x)
               call random_gen(r)
               if (r.le.ka/kext) then ! collision is a absorption
                  if (trim(source).eq.'internal') then
                     weight=Blambda(Tgas(current_layer),lambda) ! [W/m²/sr/µm]
                  endif         ! source=internal
                  keep_running=.false.
c     Debug
                  if (debug) then
                     write(*,*) '--------------------------------------------'
                     write(*,*) 'absorption @ x=',(x(j),j=1,dim)
                     write(*,*) '--------------------------------------------'
                  endif         ! debug
c     Debug
               else             ! collision is a scattering event
c     Debug
                  if (debug) then
                     write(*,*) '--------------------------------------------'
                     write(*,*) 'scattering @ x=',(x(j),j=1,dim)
                     write(*,*) '--------------------------------------------'
                  endif         ! debug
c     Debug
                  cdf_aerosol(0)=0.0D+0
                  if (Naerosol.gt.0) then
                     do i=1,Naerosol
                        cdf_aerosol(i)=cdf_aerosol(i-1)+ks_aerosol(i,current_layer,band_index)/ks
                     enddo      ! i
                  endif         ! Naerosol > 0
                  cdf_aerosol(Naerosol+1)=cdf_aerosol(Naerosol)+ks_gas(current_layer,band_index)/ks
                  call sample_integer(Naerosol+1,cdf_aerosol,iaerosol)
                  if (iaerosol.le.Naerosol) then
c     scattering by aerosol mode index "iaerosol"
                     pf=2
                     gHG=g_aerosol(iaerosol,current_layer,band_index)
                  else
c     scattering by the gas mixture
                     pf=0
                     gHG=0.0D+0
                  endif         ! nature of the scattering species
                  call sample_scattering_angle(dim,
     &                 .true.,
     &                 pf,gHG,1.0D+0,
     &                 scattering_theta,phase_function)
                  call copy_vector(dim,u,u_bs) ! record propagation direction before scattering into vector "u_bs"
                  call direction_after_scattering(dim,u,scattering_theta,u)
c     Debug
                  if (debug) then
                     write(*,*) 'New propagation direction: u=',(u(j),j=1,dim)
                  endif         ! debug
c     Debug
c     Update the MC weight
                  if (trim(source).eq.'external') then
c     Debug
                     if (debug) then
                        write(*,*) 'Calling source_solid_angle with:'
                        write(*,*) 'x=',x
                        write(*,*) 'x_source=',x_source
                        write(*,*) 'Rs=',Rs
                     endif      ! debug
c     Debug
                     call source_solid_angle(debug,dim,x,x_source,Rs,theta_source,omega_source)
c     Debug
                     if (debug) then
                        write(*,*) 'theta_source=',theta_source,' rad'
                        write(*,*) 'omega_source=',omega_source,' sr'
                     endif      ! debug
c     Debug
                     call sample_cone_solid_angle(omega_source,theta,phi)
c     Debug
                     if (debug) then
                        write(*,*) 'Calling angles_to_solar_direction with:'
                        write(*,*) 'x=',x
                        write(*,*) 'theta_s=',theta_s,' rad'
                        write(*,*) 'phi_s=',phi_s,' rad'
                        write(*,*) 'theta=',theta,' rad'
                        write(*,*) 'phi=',phi,' rad'
                     endif      ! debug
c     Debug
                     call angles_to_solar_direction(dim,x_source,x,theta,phi,us)
c     Debug
                     if (debug) then
                        write(*,*) 'Calling transmission_to_source with:'
                        write(*,*) 'x=',x
                        write(*,*) 'us=',us
                        write(*,*) 'center=',center
                        write(*,*) 'x_source=',x_source
                     endif      ! debug
c     Debug
                     call transmission_to_source(debug,dim,x,us,center,x_source,Nlayer,z_grid,D,Ds,Rs,theta_s,phi_s,
     &                    ka_gas,ks_gas,Naerosol,ka_aerosol,ks_aerosol,band_index,quad_index,
     &                    valid_transmissivity,solar_transmissivity)
                     if (.not.valid_transmissivity) then
                        valid_path=.false.
                        goto 666
                     endif
c     Debug
                     if (debug) then
                        write(*,*) 'After computing transmission: x=',(x(j),j=1,dim)
                     endif      ! debug
c     Debug
                     call scalar_product(dim,u_bs,us,mu)
                     call scattering_phase_function(pf,gHG,1.0D+0,mu,phase_function) ! phase_function [sr⁻¹]
                     weight=weight+Isource*solar_transmissivity*omega_source*phase_function ! [W/m²/sr/µm]
                  endif         ! source=external
c     Debug
                  if (debug) then
                     write(*,*) 'Isource=',Isource,' W/m²/sr/µm'
                     write(*,*) 'solar_transmissivity=',solar_transmissivity
                     write(*,*) 'omega_source=',omega_source,' sr'
                     write(*,*) 'phase_function=',phase_function,' sr⁻¹'
                     write(*,*) 'weight=',weight,' W/m²/sr/µm'
                  endif         ! debug
c     Debug                  
c                  
               endif            ! nature of collision
c     
            else                ! length > d2int
c     Debug
               if (debug) then
                  write(*,*) '> d2int=',d2int
               endif            ! debug
c     Debug
               call copy_vector(dim,x_int,x)
c     2 things can happen: next_layer has the value 0 (groud) or Nlayer+1 (space)
               if (next_layer.eq.0) then
c     The solid core has been reached: the ground will either absorb or reflect the incoming ray
                  call random_gen(r)
                  if (r.le.epsilon) then
                     if (trim(source).eq.'internal') then
                        weight=Blambda(Tsurf,lambda) ! [W/m²/sr/µm]
                     endif      ! source=internal
c     Debug
                  if (debug) then
                     write(*,*) '--------------------------------------------'
                     write(*,*) 'absorption @ ground, x=',x
                     write(*,*) '--------------------------------------------'
                  endif         ! debug
c     Debug
                     keep_running=.false.
                     goto 665
                  else
c     Debug
                     if (debug) then
                        write(*,*) '--------------------------------------------'
                        write(*,*) 'reflexion @ ground, x=',x
                        write(*,*) '--------------------------------------------'
                     endif      ! debug
c     Debug
                     call normal_on_sphere(dim,center,z_grid(0),x,n_xint)
c     Debug
                     if (debug) then
                        write(*,*) 'n_xint=',(n_xint(j),j=1,dim)
                     endif      ! debug
c     Debug
                     call sample_emission_direction_on_surface(dim,n_xint,u,pdf_u) ! pdf_u [sr⁻¹]
                     call scalar_product(dim,u,n_xint,prod)
                     if (prod.lt.0.0D+0) then
                        call error(label)
                        write(*,*) 'Ground reflexion @ postion x_int=',(x_int(j),j=1,dim)
                        write(*,*) 'normal to the ground n_xint=',(n_xint(j),j=1,dim)
                        write(*,*) 'sampled propagation direction u=',(u(j),j=1,dim)
                        write(*,*) 'u.n_xint=',prod
                        write(*,*) 'should be positive'
                        stop
                     endif
                     current_layer=1
                     if (trim(source).eq.'external') then
c     Update the MC weight
                        call source_solid_angle(debug,dim,x_int,x_source,Rs,theta_source,omega_source)
                        call sample_cone_solid_angle(omega_source,theta,phi)
c     Debug
                        if (debug) then
                           write(*,*) 'theta=',theta,' phi=',phi
                        endif   ! debug
c     Debug
                        call angles_to_solar_direction(dim,x_source,x_int,theta,phi,us)
                        call transmission_to_source(debug,dim,x_int,us,center,x_source,Nlayer,z_grid,D,Ds,Rs,theta_s,phi_s,
     &                       ka_gas,ks_gas,Naerosol,ka_aerosol,ks_aerosol,band_index,quad_index,
     &                       valid_transmissivity,solar_transmissivity)
                        if (.not.valid_transmissivity) then
                           valid_path=.false.
                           goto 666
                        endif
                        call scalar_product(dim,us,n_xint,prod)
                        weight=weight+Isource*solar_transmissivity*omega_source*prod/pi ! [W/m²/sr/µm]
                     endif      ! source=external
                  endif         ! r < or > epsilon
               else if (next_layer.eq.Nlayer+1) then
c     Space has been reached: stop the realization
                  keep_running=.false.
               else
c     In all other cases, the optical path has to keep going
                  current_layer=next_layer
               endif            ! next_layer
            endif               ! length < or > d2int
c     
         else                   ! no intersection found with the grid
            keep_running=.false.
            goto 665
         endif                  ! intersection_found

 665     continue
      enddo                     ! while (keep_running)

 666  continue
c     Debug
      if (debug) then
         write(*,*) trim(label),': out'
      endif                     ! debug
c     Debug
      return
      end



      subroutine test_raytracing(dim,Nevent,center,x_obs,Nlayer,z_grid,D)
      implicit none
      include 'max.inc'
c     
c     Purpose: to test the intersection routines
c     
c     I/O
      integer dim
      integer Nevent
      double precision center(1:Ndim_mx)
      double precision x_obs(1:Ndim_mx)
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision D
c     temp
      logical debug,err
      double precision theta_min,theta_max
      double precision omega_min,omega_max
      double precision theta,phi
      double precision u0
      logical intersection_found
      double precision x_int(1:Ndim_mx)
      double precision d2int
      integer interface_index
      integer j,event
c     label
      character*(Nchar_mx) label
      label='subroutine test_raytracing'

      call cone_sphere_solid_angle(z_grid(0),D,theta_min,omega_min)
      call cone_sphere_solid_angle(z_grid(Nlayer),D,theta_max,omega_max)

      write(*,*) 'Running test...'
      do event=1,Nevent
c     Sample a (theta,phi) direction from the observation position
         call sample_cone_half_angle_general(0.0D+0,theta_min,theta,phi) ! sample the solid angle under which the solid core is perceived
c         call sample_cone_half_angle_general(theta_min,theta_max,theta,phi) ! sample only the limbs
         call angles_to_observation_direction(dim,center,x_obs,theta,phi,u0)

         debug=.false.
         call next_grid_intersection(debug,dim,
     &        x_obs,u0,center,Nlayer,z_grid,
     &        err,intersection_found,x_int,d2int,interface_index)
         if (.not.intersection_found) then
            call error(label)
            write(*,*) 'Intersection not found !'
            write(*,*) 'event:',event
            stop
         else
            if (interface_index.ne.Nlayer) then
               call error(label)
               write(*,*) 'Intersection found, but interface_index=',interface_index
               write(*,*) 'expected value: Nlayer=',Nlayer
               stop
            endif
         endif
      enddo                     ! event
      write(*,*) 'done'

      return
      end



      subroutine test_spectral_integration(Nevent,Nband,Nquad,lambda_min,lambda_max,w,Tref,lambda_inf,lambda_sup)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to test the spectral integration technique
c     
c     Input:
c       + Nevent: number of statistical events
c       + Nband: number of spectral intervals
c       + Nquad: quadrature order, for each spectral interval
c       + lambda_min: lower wavelength of each interval [µm]
c       + lambda_max: higher wavelength of each interval [µm]
c       + w: gas quadrature weight
c       + Tref: reference temperature for the evaluation of the spectral pdf [K]
c       + lambda_inf: lower limit for spectral integration [µm]
c       + lambda_sup: lower limit for spectral integration [µm]
c     
c     Output:
c     
c     I/O
      integer Nevent
      integer Nband
      integer Nquad(1:Nband_mx)
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      double precision Tref
      double precision lambda_inf
      double precision lambda_sup
c     temp
      integer N1
      double precision band_pdf(1:Nproba_mx)
      double precision band_cdf(0:Nproba_mx)
      integer N2
      double precision quad_pdf(1:Nproba_mx)
      double precision quad_cdf(0:Nproba_mx)
      integer event,iband,iquad
      logical convergence
      double precision lambda,lambda_pdf
      double precision sum_w,sum_w2,weight
      double precision mean,variance,std_dev
      double precision T,reference
c     functions
      double precision planck_int_wavlng,Blambda
c     label
      character*(Nchar_mx) label
      label='subroutine test_spectral_integration'

      call build_band_pdf(Nband,lambda_min,lambda_max,Tref,lambda_inf,lambda_sup,
     &     N1,band_pdf,band_cdf)
c     Debug
c      write(*,*) 'iband / band_pdf / band_cdf'
c      do iband=1,Nband
c         write(*,*) iband,band_pdf(iband),band_cdf(iband)
c      enddo                     ! iband
c     Debug
      T=2000                ! K
c     
      sum_w=0.0D+0
      sum_w2=0.0D+0
      do event=1,Nevent
c     Sample band
         call sample_integer(N1,band_cdf,iband)
c     Sample wavelength
         call sample_wavelength_according_to_Planck(lambda_min(iband),lambda_max(iband),Tref,convergence,lambda,lambda_pdf)
         if (.not.convergence) then
            call error(label)
            write(*,*) 'event:',event
            write(*,*) 'could not sample wavelength'
            stop
         endif
c     Build pdf for the current band
         call build_quad_pdf(Nquad,w,iband,N2,quad_pdf,quad_cdf)
c     Sample quadrature point
         call sample_integer(N2,quad_cdf,iquad)
c     weight
         weight=Blambda(T,lambda)/(band_pdf(iband)*lambda_pdf)
         sum_w=sum_w+weight
         sum_w2=sum_w2+weight**2.0D+0
      enddo                     ! event
      call statistics(Nevent,sum_w,sum_w2,mean,variance,std_dev)
      reference=planck_int_wavlng(lambda_inf,lambda_sup,T)
      write(*,*) 'Reference value:',reference,' W/m²/sr'
      write(*,*) 'mean=',mean,' +/-',std_dev,' W/m²/sr'
      write(*,*) 'mean-reference=',dabs(mean-reference)
      
      return
      end
