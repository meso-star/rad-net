c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine next_grid_intersection(debug,dim,
     &     origin,u,center,Nlayer,z_grid,
     &     err,intersection_found,x,d2int,interface_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the next intersection between the spherical grid and
c     the (origin,u) LOS
c     
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + Nlayer: number of atmospheric layers
c       + z_grid: vertical grid [m]
c     
c     Output:
c       + err: T if a error occured; F otherwise
c       + intersection_found: T is a intersection was found (in the forward direction)
c       + x: intersection position
c       + d2int: distance from "origin" to "x"
c       + interface_index: index of the grid interface the intersection is at:
c         - interface_index=0: ground / layer 1 interface
c         - interface_index=[1;Nlayer-1]: interface between layers interface_index and interface_index+1
c         - interface_index=Nlayer: interface between the last layer and space
c     
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      logical err
      logical intersection_found
      double precision x(1:Ndim_mx)
      double precision d2int
      integer interface_index
c     temp
      logical is_in_layer
      integer layer_index
      logical is_at_interface
      integer interface_idx
      integer j
      double precision OP_cart(1:Ndim_mx)
      double precision OP_spher(1:Ndim_mx)
      double precision r,theta,phi
      integer n_int
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k1,k2
      double precision normal(1:Ndim_mx)
      double precision prod
      integer Nintersection,idx
      double precision x_intersection(1:Nintersection_mx,1:Ndim_mx)
      double precision d2intersection(1:Nintersection_mx)
      integer intidx(1:Nintersection_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine next_grid_intersection'
      
c     Debug
      if (debug) then
         write(*,*) trim(label),' : in'
      endif                     ! debug
c     Debug
c
      err=.false.
      intersection_found=.false.
c      
      call locate_position(dim,Nlayer,z_grid,center,origin,
     &     is_in_layer,layer_index,is_at_interface,interface_idx)
c     Debug
      if (debug) then
         write(*,*) 'origin=',(origin(j),j=1,dim)
         write(*,*) 'is_in_layer=',is_in_layer
         if (is_in_layer) then
            write(*,*) 'layer_index=',layer_index
         endif
         write(*,*) 'is_at_interface=',is_at_interface
         if (is_at_interface) then
            write(*,*) 'interface_idx=',interface_idx
         endif
      endif                     ! debug
c     Debug
      if (is_in_layer) then
c     test only intersections with spheres of radius "z_grid(layer_index-1)" and "z_grid(layer_index)"
         if (layer_index.eq.0) then
            call error(label)
            write(*,*) 'Position origin=',(origin(j),j=1,dim)
            call substract_vectors(dim,origin,center,OP_cart)
            call cart2spher(dim,OP_cart,OP_spher)
            write(*,*) 'corresponding radius=',OP_spher(1)
            write(*,*) 'has been detected below ground, z_grid(0)=',z_grid(0)
            stop
         endif                  ! layer_index=0
         if (layer_index.eq.Nlayer+1) then
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(Nlayer),
     &           n_int,P1,k1,P2,k2)
c     Debug
            if (debug) then
               write(*,*) 'Number of intersections with sphere z_grid(',Nlayer,')=',n_int
            endif               ! debug
c     Debug
            if (n_int.gt.0) then
               intersection_found=.true.
               call copy_vector(dim,P1,x)
               d2int=k1
               interface_index=Nlayer
c     Debug
               if (debug) then
                  write(*,*) 'interface_index=',interface_index
               endif            ! debug
c     Debug
            endif
         endif                  ! layer_index=Nlayer
         if ((layer_index.ge.1).and.(layer_index.le.Nlayer)) then
            Nintersection=0
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(layer_index-1),
     &           n_int,P1,k1,P2,k2)
            if (n_int.gt.0) then
               Nintersection=Nintersection+1
               if (Nintersection.gt.Nintersection_mx) then
                  call error(label)
                  write(*,*) 'Nintersection_mx has been reached'
                  stop
               endif
               do j=1,dim
                  x_intersection(Nintersection,j)=P1(j)
               enddo            ! j
               d2intersection(Nintersection)=k1
               intidx(Nintersection)=layer_index-1
            endif
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(layer_index),
     &           n_int,P1,k1,P2,k2)
            if (n_int.gt.0) then
               Nintersection=Nintersection+1
               if (Nintersection.gt.Nintersection_mx) then
                  call error(label)
                  write(*,*) 'Nintersection_mx has been reached'
                  stop
               endif
               do j=1,dim
                  x_intersection(Nintersection,j)=P1(j)
               enddo            ! j
               d2intersection(Nintersection)=k1
               intidx(Nintersection)=layer_index
            endif
            if (Nintersection.gt.0) then
               intersection_found=.true.
               call identify_closest_intersection(Nintersection,d2intersection,idx)
               do j=1,dim
                  x(j)=x_intersection(idx,j)
               enddo            ! j
               d2int=d2intersection(idx)
               interface_index=intidx(idx)
            endif
            if (.not.intersection_found) then
               call error(label)
               write(*,*) 'Position origin=',(origin(j),j=1,dim)
               call substract_vectors(dim,origin,center,OP_cart)
               call cart2spher(dim,OP_cart,OP_spher)
               write(*,*) 'corresponding radius=',OP_spher(1)
               call locate_position(dim,Nlayer,z_grid,center,origin,
     &              is_in_layer,layer_index,is_at_interface,interface_idx)
               write(*,*) 'is_in_layer=',is_in_layer
               if (is_in_layer) then
                  write(*,*) 'layer_index=',layer_index
               endif
               write(*,*) 'is_at_interface=',is_at_interface
               if (is_at_interface) then
                  write(*,*) 'interface_idx=',interface_idx
               endif
               write(*,*) 'direction u=',u
               write(*,*) 'identified in layer index:',layer_index
               write(*,*) 'could not find intersection with either:'
               write(*,*) '- sphere of radius z_grid(',layer_index-1,')=',z_grid(layer_index-1)
               write(*,*) '- sphere of radius z_grid(',layer_index,')=',z_grid(layer_index)
               stop
            endif
         endif                  ! layer_index=[1;Nlayer]
c        
      else if (is_at_interface) then
c         
         if (interface_idx.eq.0) then
            call normal_on_sphere(dim,center,z_grid(0),origin,normal)
            call scalar_product(dim,u,normal,prod)
            if (prod.lt.0.0D+0) then
c$$$               call error(label)
c$$$               write(*,*) 'Position origin=',(origin(j),j=1,dim)
c$$$               call substract_vectors(dim,origin,center,OP_cart)
c$$$               call cart2spher(dim,OP_cart,OP_spher)
c$$$               write(*,*) 'corresponding radius=',OP_spher(1)
c$$$               write(*,*) 'has been indentified located at interface index:',interface_idx
c$$$               write(*,*) 'z_grid(0)=',z_grid(0)
c$$$               write(*,*) 'local normal is:',(normal(j),j=1,dim)
c$$$               write(*,*) 'u.normal=',prod
c$$$               write(*,*) 'should be positive'
c$$$               stop
               err=.true.
               goto 666
            endif
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(1),
     &           n_int,P1,k1,P2,k2)
            if (n_int.gt.0) then
               intersection_found=.true.
               call copy_vector(dim,P1,x)
               d2int=k1
               interface_index=1
            endif
            if (.not.intersection_found) then
               call error(label)
               write(*,*) 'Position origin=',(origin(j),j=1,dim)
               call substract_vectors(dim,origin,center,OP_cart)
               call cart2spher(dim,OP_cart,OP_spher)
               write(*,*) 'corresponding radius=',OP_spher(1)
               write(*,*) 'has been located at interface index:',interface_idx
               write(*,*) 'z_grid(0)=',z_grid(0)
               write(*,*) 'local normal is:',(normal(j),j=1,dim)
               write(*,*) 'u.normal=',prod
               write(*,*) 'no intersection found with sphere of radius z_grid(1)=',z_grid(1)
               stop
            endif
         endif                  ! interface_idx=0
         if (interface_idx.eq.Nlayer) then
            Nintersection=0
c     If the path starts from the TOA, a intersection can be found with the Nlayer-1 interface
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(Nlayer-1),
     &           n_int,P1,k1,P2,k2)
            if (n_int.gt.0) then
               Nintersection=Nintersection+1
               if (Nintersection.gt.Nintersection_mx) then
                  call error(label)
                  write(*,*) 'Nintersection_mx has been reached'
                  stop
               endif
               do j=1,dim
                  x_intersection(Nintersection,j)=P1(j)
               enddo            ! j
               d2intersection(Nintersection)=k1
               intidx(Nintersection)=Nlayer-1
            endif
c     But also with the TOA itself (LOS crosses the last layer)
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(Nlayer),
     &           n_int,P1,k1,P2,k2)
            if (n_int.gt.0) then
               Nintersection=Nintersection+1
               if (Nintersection.gt.Nintersection_mx) then
                  call error(label)
                  write(*,*) 'Nintersection_mx has been reached'
                  stop
               endif
               do j=1,dim
                  x_intersection(Nintersection,j)=P1(j)
               enddo            ! j
               d2intersection(Nintersection)=k1
               intidx(Nintersection)=Nlayer
            endif
            if (Nintersection.gt.0) then
               intersection_found=.true.
               call identify_closest_intersection(Nintersection,d2intersection,idx)
               do j=1,dim
                  x(j)=x_intersection(idx,j)
               enddo            ! j
               d2int=d2intersection(idx)
               interface_index=intidx(idx)
            endif
         endif                  ! interface_idx=Nlayer
         if ((interface_idx.ge.1).and.(interface_idx.le.Nlayer-1)) then
            Nintersection=0
c     Off course the intersection with interface "interface_idx-1" has to be tested
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(interface_idx-1),
     &           n_int,P1,k1,P2,k2)
            if (n_int.gt.0) then
               Nintersection=Nintersection+1
               if (Nintersection.gt.Nintersection_mx) then
                  call error(label)
                  write(*,*) 'Nintersection_mx has been reached'
                  stop
               endif
               do j=1,dim
                  x_intersection(Nintersection,j)=P1(j)
               enddo            ! j
               d2intersection(Nintersection)=k1
               intidx(Nintersection)=interface_idx-1
            endif
c     as well as the intersection with interface "interface_idx+1" 
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(interface_idx+1),
     &           n_int,P1,k1,P2,k2)
            if (n_int.gt.0) then
               Nintersection=Nintersection+1
               if (Nintersection.gt.Nintersection_mx) then
                  call error(label)
                  write(*,*) 'Nintersection_mx has been reached'
                  stop
               endif
               do j=1,dim
                  x_intersection(Nintersection,j)=P1(j)
               enddo            ! j
               d2intersection(Nintersection)=k1
               intidx(Nintersection)=interface_idx+1
            endif
c     But ! The intersection with interface "interface_idx" itself also has to be tested ! (LOS crosses layer index "interface_idx")
            call line_sphere_forward_intersect(debug,dim,origin,u,center,z_grid(interface_idx),
     &           n_int,P1,k1,P2,k2)
            if (n_int.gt.0) then
               Nintersection=Nintersection+1
               if (Nintersection.gt.Nintersection_mx) then
                  call error(label)
                  write(*,*) 'Nintersection_mx has been reached'
                  stop
               endif
               do j=1,dim
                  x_intersection(Nintersection,j)=P1(j)
               enddo            ! j
               d2intersection(Nintersection)=k1
               intidx(Nintersection)=interface_idx
            endif
            if (Nintersection.gt.0) then
               intersection_found=.true.
               call identify_closest_intersection(Nintersection,d2intersection,idx)
               do j=1,dim
                  x(j)=x_intersection(idx,j)
               enddo            ! j
               d2int=d2intersection(idx)
               interface_index=intidx(idx)
            endif
            if (.not.intersection_found) then
               call error(label)
               write(*,*) 'Position origin=',(origin(j),j=1,dim)
               call substract_vectors(dim,origin,center,OP_cart)
               call cart2spher(dim,OP_cart,OP_spher)
               write(*,*) 'corresponding radius=',OP_spher(1)
               write(*,*) 'has been located at interface index:',interface_idx
               write(*,*) 'could not find intersection with either:'
               write(*,*) '- sphere of radius z_grid(',interface_idx-1,')=',z_grid(interface_idx-1)
               write(*,*) '- sphere of radius z_grid(',interface_idx,')=',z_grid(interface_idx)
               write(*,*) '- sphere of radius z_grid(',interface_idx+1,')=',z_grid(interface_idx+1)
               stop
            endif
         endif                  ! interface_idx=[1,Nlayer-1]
c     
      endif
 666  continue
c     Debug
      if (debug) then
         write(*,*) trim(label),' : out'
      endif                     ! debug
c     Debug
      return
      end


      subroutine identify_closest_intersection(Nintersection,d,idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the closest intersection position
c     
c     Input:
c       + Nintersection: number of intersections
c       + d: list of distances to intersectionn
c     
c     Output:
c       + idx: index of the closest intersection
c     
c     I/O
      integer Nintersection
      double precision d(1:Nintersection_mx)
      integer idx
c     temp
      integer i
      double precision dmin
c     label
      character*(Nchar_mx) label
      label='subroutine identify_closest_intersection'

      idx=1
      dmin=d(1)
      do i=2,Nintersection
         if (d(i).lt.dmin) then
            idx=i
            dmin=d(i)
         endif
      enddo                     ! i

      return
      end


      
      subroutine locate_position(dim,Nlayer,z_grid,center,P,
     &     is_in_layer,layer_index,is_at_interface,interface_index)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to identify the layer position "P" is in or the interface it is located at
c     
c     Input:
c       + dim: dimension of space
c       + Nlayer: number of atmospheric layers
c       + z_grid: vertical grid [m]
c       + center: cartesian coordinates of the sphere center
c       + P: position to locate [m, m, m]
c     
c     Ouput:
c       + is_in_layer: T if position P is in a layer
c       + layer_index: index of the layer "P" is into
c         - layer_index=0: below ground
c         - layer_index=[1;Nlayer]: gas layer index "layer_index"
c         - layer_index=Nlayer+1: space
c       + is_at_interface: T if position P is on a layer interface
c       + interface_index: interface of the interface "P" is at:
c         - interface_index=0: ground / layer 1 interface
c         - interface_index=[1;Nlayer-1]: interface between layers interface_index and interface_index+1
c         - interface_index=Nlayer: interface between the last layer and space
c     
c     I/O
      integer dim
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision center(1:Ndim_mx)
      double precision P(1:Ndim_mx)
      logical is_in_layer
      integer layer_index
      logical is_at_interface
      integer interface_index
c     temp
      integer i,j,ilayer
      logical layer_found
      double precision OP_cart(1:Ndim_mx)
      double precision OP_spher(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine locate_position'
c
      call substract_vectors(dim,P,center,OP_cart)
      call cart2spher(dim,OP_cart,OP_spher)
c
      is_at_interface=.false.
      is_in_layer=.false.
      do i=0,Nlayer
         if (dabs(OP_spher(1)-z_grid(i)).le.epsilon_intersection) then
            is_at_interface=.true.
            interface_index=i
            goto 666
         endif
      enddo ! i 
c
      is_in_layer=.true.
      layer_found=.false.
      if (OP_spher(1).lt.z_grid(0)) then
         layer_found=.true.
         layer_index=0
         goto 111
      endif
      do ilayer=1,Nlayer
         if ((OP_spher(1).gt.z_grid(ilayer-1)).and.(OP_spher(1).lt.z_grid(ilayer))) then
            layer_found=.true.
            layer_index=ilayer
            goto 111
         endif
      enddo                     ! ilayer
      if (OP_spher(1).gt.z_grid(Nlayer)) then
         layer_found=.true.
         layer_index=Nlayer+1
         goto 111
      endif
 111  continue
      if ((is_in_layer).and.(.not.layer_found)) then
         call error(label)
         write(*,*) 'Could not locate P=',(P(j),j=1,dim)
         write(*,*) 'r=',OP_spher(1)
         write(*,*) 'Interface / z_grid'
         do i=0,Nlayer
            write(*,*) i,z_grid(i)
         enddo                  ! i
         stop
      endif
c      
 666  continue
      if (is_in_layer.and.is_at_interface) then
         call error(label)
         write(*,*) 'is_in_layer=',is_in_layer
         write(*,*) 'is_at_interface=',is_at_interface
         write(*,*) 'only one can be true'
         stop
      endif
c     
      return
      end



      subroutine line_sphere_forward_intersect(debug,dim,
     &     origin,u,center,radius,
     &     n_int,P1,k1,P2,k2)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to compute the intersection point(s) between a line
c     and a sphere in the general case (not centered at the origin)
c     but only intersections in the forward direction are provided.
c
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c
c     Output:
c       + n_int: number of intersections
c       + P1,P2: cartesian coordinates of the intersections, sorted by increasing distance to origin
c       + k1,k2: distance to intersections, sorted by increasing distance to origin
c
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision radius
      integer n_int
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k1,k2
c     temp
      integer n_int_found
      double precision x0(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision t0,t1,t2
c     label
      character*(Nchar_mx) label
      label='subroutine line_sphere_forward_intersect'

c     Debug
      if (debug) then
         write(*,*) trim(label),' : in'
      endif                     ! debug
c     Debug
      
      call line_sphere_intersect(debug,dim,
     &     origin,u,center,radius,
     &     n_int_found,x0,t0,x1,t1,x2,t2)

      n_int=0
      if (n_int_found.eq.1) then
         if (t0.gt.epsilon_start) then
            n_int=n_int+1
            k1=t0
            call copy_vector(dim,x0,P1)
         endif
      else if (n_int_found.eq.2) then
         if (t1.gt.epsilon_start) then
            n_int=n_int+1
            k1=t1
            call copy_vector(dim,x1,P1)
         endif
         if (t2.gt.epsilon_start) then
            n_int=n_int+1
            if (n_int.eq.1) then
               k1=t2
               call copy_vector(dim,x2,P1)
            else if (n_int.eq.2) then
               k2=t2
               call copy_vector(dim,x2,P2)
            endif
         endif            
      endif

c     Debug
      if (debug) then
         write(*,*) trim(label),' : out'
      endif                     ! debug
c     Debug
      
      return
      end


      
      subroutine line_sphere_intersect(debug,dim,
     &     origin,u,center,radius,
     &     n_int,P0,k0,P1,k1,P2,k2)
      use ieee_arithmetic
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to compute the intersection point(s) between a line
c     and a sphere in the general case (not centered at the origin)
c     Update 2011-07-23:
c     only valid solutions (intersections met in the direction of propagation)
c     are provided.
c
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c
c     Output:
c       + n_int: number of intersections
c       + P0: cartesian coordinates of the intersection point when n_int=1
c       + k0: index of k for the solution when n_int=1
c       + P1,P2: cartesian coordinates of the intersections point when n_int=2, sorted by increasing distance to origin
c       + k1,k2: indexes of k for the solutions when n_int=2, sorted by increasing distance to origin
c
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision radius
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
c     temp
      double precision a,b,c,d
      integer nsol
      double precision un(1:Ndim_mx)
      double precision CO(1:Ndim_mx)
      double precision prod,dCO
      double precision flight0(1:Ndim_mx)
      double precision flight1(1:Ndim_mx)
      double precision flight2(1:Ndim_mx)
      double precision epsilon_r
c     label
      character*(Nchar_mx) label
      label='subroutine line_sphere_intersect'

c     Debug
      if (debug) then
         write(*,*) trim(label),' : in'
      endif                     ! debug
c     Debug
      
      epsilon_r=epsilon_start*radius
      call normalize_vector(dim,u,un)
      call substract_vectors(dim,origin,center,CO)
      a=1.0D+0 ! because "un" is normalized, by definition
      call vector_length(dim,CO,dCO)
      call vector_vector(dim,un,CO,prod)
      b=2.0D+0*prod
      c=dCO**2.0D+0-radius**2.0D+0

c     find mathematical solutions
      call eq2deg(a,b,c,nsol,k0,k1,k2)

c     Debug
      if (debug) then
         write(*,*) 'a=',a
         write(*,*) 'b=',b
         write(*,*) 'c=',c
         write(*,*) 'b^2=',b**2.0D+0
         write(*,*) 'delta=',b**2.0D+0-4.0D+0*a*c
         write(*,*) 'nsol=',nsol
         write(*,*) 'k0=',k0
         write(*,*) 'k1=',k1
         write(*,*) 'k2=',k2
      endif
c     Debug

      n_int=nsol
      if (n_int.eq.1) then
         call scalar_vector(dim,k0,un,flight0)
         call add_vectors(dim,origin,flight0,P0)
         call distance(dim,center,P0,d)
         if (dabs(d-radius).gt.epsilon_r) then
c     Debug
            if (debug) then
               write(*,*) 'Distance between:'
               write(*,*) 'intersection position P0=',P0
               write(*,*) 'and sphere center=',center
               write(*,*) 'is d=',d
               write(*,*) '|d-radius|=',dabs(d-radius)
               write(*,*) '> epsilon_r=',epsilon_r
            endif
c     Debug
            n_int=0
            goto 666
         endif
      else if (n_int.eq.2) then
         call scalar_vector(dim,k1,un,flight1)
         call scalar_vector(dim,k2,un,flight2)
         call add_vectors(dim,origin,flight1,P1)
         call add_vectors(dim,origin,flight2,P2)
c     Debug
         if (debug) then
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
         endif
c     Debug
         call distance(dim,center,P1,d)
         if (dabs(d-radius).gt.epsilon_r) then
c     Debug
            if (debug) then
               write(*,*) 'Distance between:'
               write(*,*) 'intersection position P1=',P1
               write(*,*) 'and sphere center=',center
               write(*,*) 'is d=',d
               write(*,*) 'd-radius=',dabs(d-radius)
               write(*,*) '> epsilon_r=',epsilon_r
            endif
c     Debug
            n_int=0
            goto 666
         endif
         call distance(dim,center,P2,d)
         if (dabs(d-radius).gt.epsilon_r) then
c     Debug
            if (debug) then
               write(*,*) 'Distance between:'
               write(*,*) 'intersection position P2=',P2
               write(*,*) 'and sphere center=',center
               write(*,*) 'is d=',d
               write(*,*) 'd-radius=',dabs(d-radius)
               write(*,*) '> epsilon_r=',epsilon_r
            endif
c     Debug
            n_int=0
            goto 666
         endif
      endif

 666  continue
c     Debug
      if (debug) then
         write(*,*) 'n_int=',n_int
      endif
c     Debug
c     Debug
      if (debug) then
         write(*,*) trim(label),' : out'
      endif                     ! debug
c     Debug
      return
      end


      
      subroutine line_sphere_from_inside_intersect(debug,dim,
     &     origin,u,center,radius,Pint,d2int)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to compute the intersection position between a line
c     and a sphere in the case when the origin of the line is inside
c     the sphere: there must be only one intersection in the
c     forward direction.
c
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c
c     Output:
c       + Pint: cartesian coordinates of the intersection position
c       + d2int: distance to intersection position
c
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision radius
      double precision Pint(1:Ndim_mx)
      double precision d2int
c     temp
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
      double precision dOC
c     label
      character*(Nchar_mx) label
      label='subroutine line_sphere_from_inside_intersect'
      
      call distance(dim,origin,center,dOC)
      if (dOC-radius.gt.epsilon_start) then
         call error(label)
         write(*,*) 'Distance origin-center=',dOC
         write(*,*) 'should be < radius=',radius
         stop
      endif
      
      call line_sphere_intersect(debug,dim,
     &     origin,u,center,radius,n_int,
     &     P0,k0,P1,k1,P2,k2)
      if (n_int.le.0) then
         call error(label)
         write(*,*) 'No intersection found'
         stop
      else if (n_int.eq.1) then
         call copy_vector(dim,P0,Pint)
      else if (n_int.eq.2) then
         if ((k1.gt.epsilon_start).and.(k2.gt.epsilon_start)) then
            call error(label)
            write(*,*) 'k1=',k1
            write(*,*) 'k2=',k2
            write(*,*) 'should be both < epsilon_start=',epsilon_start
            stop
         else
            if (k1.gt.epsilon_start) then
               call copy_vector(dim,P1,Pint)
               d2int=k1
            else if (k2.gt.epsilon_start) then
               call copy_vector(dim,P2,Pint)
               d2int=k2
            else
               call error(label)
               write(*,*) 'No suitable intersection found:'
               write(*,*) 'k1=',k1
               write(*,*) 'k2=',k2
               stop
            endif
         endif
      endif

      return
      end


      
      subroutine eq2deg(a,b,c,nsol,x0,x1,x2)
      implicit none
      include 'max.inc'
c
c     Purpose: to solve a 2nd degree equation ax²+bx+c=0
c
c     Input:
c       + a,b,c: coefficients of the 2nd degree equation ax²+bx+c=0
c
c     Output:
c       + nsol: number of solution (0,1 or 2)
c       + x0: solution when nsol=1
c       + x1,x2: solutions when nsol=2 (x1<x2)
c
c     I/O
      double precision a,b,c
      integer nsol
      double precision x0,x1,x2
c     temp
      double precision t1,t2
      double precision delta
c     label
      character*(Nchar_mx) label
      label='subroutine eq2deg'

      delta=b**2.0D+0-4.0D+0*a*c
      if ((delta.lt.0.0D+0).and.(delta.gt.-1.0D-8)) then
         delta=0.0D+0
      endif
      if (delta.lt.0.0D+0) then
         nsol=0
      else if (delta.eq.0.0D+0) then
         nsol=1
         x0=-b/(2.0D+0*a)
      else if (delta.gt.0.0D+0) then
         nsol=2
         t1=(-b-dsqrt(delta))/(2.0D+0*a)
         t2=(-b+dsqrt(delta))/(2.0D+0*a)
         if (t1.lt.t2) then
            x1=t1
            x2=t2
         else
            x1=t2
            x2=t1
         endif
      else
         call error(label)
         write(*,*) 'delta=',delta
         stop
      endif

      return
      end


      
      subroutine normal_on_sphere(dim,
     &     center,radius,P,normal)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the outer normal to the sphere
c     for a given position
c
c     Input:
c       + dim: dimension of the vectors
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c       + P: position on the sphere
c
c     Output:
c       + normal: outgoing normal to the sphere @ P
c
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision radius
      double precision P(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
c     temp
      double precision OP(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine normal_on_sphere'

      call substract_vectors(dim,P,center,OP)
      call normalize_vector(dim,OP,normal)

      return
      end



      subroutine source_solid_angle(debug,dim,x,x_source,Rs,theta_source,omega_source)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the solid angle under which the source is perceived from a arbitrary position
c     
c     Input:
c       + dim: dimension of space
c       + x: observation position [m,m,m]
c       + x_source: source center position [m,m,m]
c       + Rs: radius of the source [m]
c     
c     Output:
c       + theta_source: cone half-angle [rad]
c       + omega_source: solid angle of the cone [sr]
c     
c     I/O
      logical debug
      integer dim
      double precision x(1:Ndim_mx)
      double precision x_source(1:Ndim_mx)
      double precision Rs
      double precision theta_source
      double precision omega_source
c     temp
      double precision Ds
c     label
      character*(Nchar_mx) label
      label='subroutine source_solid_angle'

c     Debug
      if (debug) then
         write(*,*) trim(label),' : in'
      endif                     ! debug
c     Debug


c     Debug
      if (debug) then
         write(*,*) 'x=',x
         write(*,*) 'x_source=',x_source
      endif                     ! debug
c     Debug      
      call distance(dim,x,x_source,Ds)
c     Debug
      if (debug) then
         write(*,*) 'Rs=',Rs
         write(*,*) 'Ds=',Ds
      endif                     ! debug
c     Debug
      call cone_sphere_solid_angle(Rs,Ds,theta_source,omega_source)

c     Debug
      if (debug) then
         write(*,*) trim(label),' : out'
      endif                     ! debug
c     Debug      
      return
      end



      subroutine cone_sphere_solid_angle(R,D,theta_max,omega)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the solid angle for a cone
c     tangent to a sphere
c     
c     Input:
c       + R: radius of the sphere [m] 
c       + D: distance between the tip of the cone and the center of the sphere [m]
c     
c     Output:
c       + theta_max: cone half-angle [rad]
c       + omega: solid angle of the cone [sr]
c     
c     I/O
      double precision R
      double precision D
      double precision theta_max
      double precision omega
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine cone_solid_angle'

      theta_max=dasin(R/D)
      omega=2.0D+0*pi*(1.0D+0-dcos(theta_max))

      return
      end


      
      subroutine angles_to_observation_direction(dim,center,origin,theta,phi,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute a propagation direction from sampled angles theta and phi
c     as seen from the observer's position
c     
c     Input:
c       + dim: dimension of space
c       + center: center of the referential (center of the spherical body)
c       + origin: origin of the ray
c       + theta: angle between the line that runs from the center of the sphere to the observer's position, and the direction of interest
c       + phi: projection of the direction of interest on a plane perpendicular to the main line
c     
c     Output:
c       + v: direction
c     
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision origin(1:Ndim_mx)
      double precision theta
      double precision phi
      double precision v(1:Ndim_mx)
c     temp
      double precision CO_cart(1:Ndim_mx)
      double precision CO_spher(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision u(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine angles_to_observation_direction'

      call substract_vectors(dim,origin,center,CO_cart)
      call cart2spher(dim,CO_cart,CO_spher)
      call birotation_matrix(dim,CO_spher(2),CO_spher(3),M)
      
      u(1)=-dcos(theta)
      u(2)=dsin(theta)*dcos(phi)
      u(3)=dsin(theta)*dsin(phi)
      call matrix_vector(dim,M,u,v)

      return
      end


      
      subroutine angles_to_solar_direction(dim,x_source,origin,theta,phi,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute a solar direction from sampled angles theta and phi
c     as seen from a given position
c     
c     Input:
c       + dim: dimension of space
c       + x_source: source position [m,m,m]
c       + origin: origin of the ray
c       + theta: angle between the line that runs from the center of the sphere to the observer's position, and the direction of interest
c       + phi: projection of the direction of interest on a plane perpendicular to the main line
c     
c     Output:
c       + v: direction to the star
c     
c     I/O
      integer dim
      double precision x_source(1:Ndim_mx)
      double precision origin(1:Ndim_mx)
      double precision theta
      double precision phi
      double precision v(1:Ndim_mx)
c     temp
      double precision OS_cart(1:Ndim_mx)
      double precision OS_spher(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision u(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine angles_to_solar_direction'

      call substract_vectors(dim,x_source,origin,OS_cart)
      call cart2spher(dim,OS_cart,OS_spher)
      call birotation_matrix(dim,OS_spher(2),OS_spher(3),M)
      
      u(1)=dcos(theta)
      u(2)=dsin(theta)*dcos(phi)
      u(3)=dsin(theta)*dsin(phi)
      call matrix_vector(dim,M,u,v)
      
      return
      end
