      program TOM
      use ieee_arithmetic
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the radiative flux at a given position,
c     reflected by a sphere surrounded by a atmosphere, within The Oignon Model:
c     
c     + purely absorbing, homogeneous and gray atmosphere of fixed width
c     + diffuse surface reflectivity, homogeneous and gray
c     + integration in a given spectral interval
c     + illumination source (star) is a blackbody of known brightness temperature
c     
c     Variables
      integer dim
c     Data
      character*(Nchar_mx) datafile
      integer Nevent
      character*(Nchar_mx) source
      double precision lambda_inf
      double precision lambda_sup
      integer external_source_type
      double precision theta_obs
      double precision phi_obs
      double precision D
      double precision theta_target
      double precision phi_target
      double precision D_target
      double precision u_up(1:Ndim_mx)
      integer solid_angle_type
      double precision hfov
      double precision aspect_ratio
      double precision Ds
      double precision Rs
      double precision Ts
      double precision theta_source
      double precision phi_source
c     Physical data
      integer Nlayer
      double precision z_grid(0:Nlayer_mx)
      double precision Tsurf
      integer Nlambda_reflectivity
      double precision lambda_reflectivity(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision Tgas(1:Nlayer_mx)
      integer Naerosol
      integer Nband
      integer Nquad(1:Nband_mx)
      double precision lambda_min(1:Nband_mx)
      double precision lambda_max(1:Nband_mx)
      double precision w(1:Nband_mx,1:Nquad_mx)
      double precision ka_gas(1:Nlayer_mx,1:Nband_mx,1:Nquad_mx)
      double precision ks_gas(1:Nlayer_mx,1:Nband_mx)
      double precision ka_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision ks_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      double precision g_aerosol(1:Naerosol_mx,1:Nlayer_mx,1:Nband_mx)
      character*(Nchar_mx) solar_intensity_file
      integer*8 Nlambda_source
      double precision source_intensity(1:Nlambda_sun_mx,1:2)
c     temp
      character*(Nchar_mx) seed_file
      integer seed,j
      double precision center(1:Ndim_mx)
      double precision x_obs(1:Ndim_mx)
      double precision x_source(1:Ndim_mx)
      logical transit
      double precision Lobs,Lobs_noatm,Lsurf
      double precision inc_Lobs
      integer Nfail
      double precision omega_obs
c     functions
      double precision planck_int_wavlng
c     label
      character*(Nchar_mx) label
      label='program TOM'
      
      dim=3      
c     Reading 'data.in'
      datafile='./data.in'
      call read_data(datafile,Nevent,source,lambda_inf,lambda_sup,external_source_type,Ds,Rs,Ts,theta_source,phi_source)
      datafile='./camera.in'
      call read_camera(dim,datafile,theta_obs,phi_obs,D,theta_target,phi_target,D_target,u_up,
     &     solid_angle_type,hfov,aspect_ratio)
c     Reading physical data
      datafile='./data/input_dataset.dat'
      call read_physical_data(datafile,dim,
     &     Nlayer,z_grid,Tsurf,
     &     Nlambda_reflectivity,lambda_reflectivity,reflectivity,
     &     Tgas,Naerosol,Nband,Nquad,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,ka_aerosol,ks_aerosol,g_aerosol)
      if ((trim(source).eq.'external').and.(external_source_type.eq.2)) then
c         solar_intensity_file='./data/sun_intensity_lite.bin'
         solar_intensity_file='./data/sun_intensity_kurudz.bin'
         call read_solar_intensity_file(solar_intensity_file,Nlambda_source,source_intensity)
      endif
c     MC estimation
      do j=1,dim
         center(j)=0.0D+0
      enddo                     ! j
c     Detect transit mode
      call detect_transit(dim,center,theta_obs,phi_obs,theta_source,phi_source,Rs,D,Ds,Nlayer,z_grid,
     &     x_obs,x_source,transit)
c     
      seed_file='./data/seed'
      call read_seed(seed_file,seed)
      call initialize_random_generator(seed)
      
c     Get MC estimation
      call MC_average_intensity(dim,center,x_obs,x_source,Nlayer,z_grid,D,Ds,Rs,Ts,theta_source,phi_source,
     &     Tsurf,Nlambda_reflectivity,lambda_reflectivity,reflectivity,Tgas,Naerosol,Nband,Nquad,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,ka_aerosol,ks_aerosol,g_aerosol,
     &     source,lambda_inf,lambda_sup,external_source_type,Nlambda_source,source_intensity,
     &     Nevent,transit,theta_target,phi_target,D_target,u_up,solid_angle_type,hfov,aspect_ratio,
     &     Lobs,inc_Lobs,Nfail,omega_obs)
      write(*,*) 'Observation solid angle:',omega_obs,' sr'
      write(*,*) 'Average intensity=',Lobs,' +/-',inc_Lobs,' W/m²/sr'
      write(*,*) 'Signal:',Lobs*omega_obs,' +/-',inc_Lobs*omega_obs,' W/m²'
c     Debug
c      write(*,*) 'Bstar=',planck_int_wavlng(lambda_inf,lambda_sup,Ts)
c     Debug
      if (Nfail.gt.0) then
         write(*,*) 'Number of failures:',Nfail
      endif
      if ((transit).and.(trim(source).eq.'external')) then
         call transit_average_intensity(dim,center,x_obs,x_source,Nlayer,z_grid,D,Rs,Ds,Ts,lambda_inf,lambda_sup,Lobs_noatm)
         write(*,*) 'Average intensity without atmosphere:',Lobs_noatm,' W/m²/sr'
      endif
      if (trim(source).eq.'internal') then
         call ground_emission(dim,Nlambda_reflectivity,lambda_reflectivity,reflectivity,lambda_inf,lambda_sup,Tsurf,Lsurf)
         write(*,*) 'Average ground emission=',Lsurf,' W/m²/sr'
      endif

      end
