\documentclass[epsf,psfig,fancyheadings,12pt]{article}
%\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage{moreverb}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{esvect}
\usepackage{graphics}
\usepackage{subfigure}
\usepackage[final]{epsfig}
\usepackage{mathrsfs}
%\usepackage{auto-pst-pdf}
%\usepackage{pstricks,pst-node,pst-text,pst-3d}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{animate}
\makeatletter
% double spacing lines 
\renewcommand{\baselinestretch}{1}
\newcommand{\LyX}{L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\spacefactor1000}
%pour faire reference toujours avec la meme norme
%lyx !
\newcommand{\eq}[1]{Eq.~\ref{eq:#1}}
\newcommand{\fig}[1]{Fig.~\ref{fig:#1}}
\newcommand{\tab}[1]{Tab.~\ref{tab:#1}}
\newcommand{\para}[1]{Sec.~\ref{para:#1}}
\newcommand{\ap}[1]{Appendix~\ref{ap:#1}}
\newcommand{\ul}{\underline}
%
\makeatother

%\fancyhf{}%
% Utiliser \fancyhead[L] pour mettre du texte dans la partie de gauche
% du header
%\fancyhead[L]{}%
%\fancyhead[R]{\thepage}%
%\renewcommand{\headrulewidth}{0pt} % ça c'est pour faire une ligne horizontale
%\headsep=25pt % ça c'est pour laisser de la place entre le header et le texte
%\pagestyle{fancy}%

% margins
\setlength{\topmargin}{-1.25in}
\setlength{\textheight}{10in}
\setlength{\oddsidemargin}{-.50in}
\setlength{\textwidth}{7.5in}



\begin{document}


\title{Format wavefront OBJ (trianglemesh)}

\maketitle
\begin{center}
\author{\bf|Méso|Star>}\\
 (\url{http://www.meso-star.com}) \\
\end{center}
\vspace{1cm}

\tableofcontents

\newpage
\section{Introduction}
\label{para:intro}

Ce document présente le format des fichiers .obj décrivant un maillage surfacique
{\it trianglemesh}, agrémenté de la notion de matériau, qui sera utilisé pour la
description des surfaces\footnote{Et plus généralement la description des interfaces
entre milieux, une surface solide n'étant jamais que l'interface entre un milieu solide
et un milieux gazeux} dans le projet ANR Rad-Net.

Nous utiliserons vraissemblablement la variante du fomat .obj qui utilise le triangle
comme unité de base, plutôt que d'autres formes, ce qui rend les fichiers produits
utilisables par de nombreux outils de manipulation de maillages, outils de rendu,
de calcul, etc, ainsi que par {\it htrdr}.
\newpage

\section{Format de base}
\label{para:format}

L'idée est de donner l'information nécessaire à la description de chaque facette
triangulaire. Pour cela, il est nécessaire de fournir les coordonnées spatiales (3D)
de chaque noeud du maillage (ou {\it vertex}). Chaque facette ({\it face}) triangulaire
sera ensuite identifiée à partir des indices des vertex qui la composent (dans leur
ordre de définition).

Ainsi, un fichier .obj n'est jamais qu'un fichier texte ascii standard, où sont
définis à minima deux types de lignes:

\begin{itemize}
\item les lignes démarrant par le caractère ``v'', suivi de trois valeurs numériques
  flottantes. Ce sont les lignes permettant de définir les coordonnées des vertex.
  Le vertex numéro 1 sera celui correspondant à la première ligne de ce type, le
  vertex numéro 2 sera celui correspondant à la seconde ligne de ce type, etc.
\item les lignes démarrant par le caractère ``f'', suivi de trois valeurs numériques
  entières. Il s'agit des lignes permettant de définir les indices des trois vertex
  qui constituent une facette triangulaire.
\end{itemize}

Une subtilité apparaît naturellement dans la définition des facettes: la
convention utilisée sur l'ordre dans lequel les indices des trois vertex sont fournis
définira la normale à la facette. Si on utilise par exemple la règle du trièdre direct
(ou règle du ``tire-bouchon'', sens des vis, etc) la normale à la facette est
définie comme $\vv{P_{1}P_{2}} \otimes \vv{P_{1}P_{3}}$ avec $P_{1}$, $P_{2}$ et $P_{3}$
les trois vertex de la facette. Si $i_{1}$, $i_{2}$ et $i_{3}$ sont les indices
respectifs des vertex $P_{1}$, $P_{2}$ et $P_{3}$, la facette triangulaire doit
être définie par la ligne ``f $i_{1}$ $i_{2}$ $i_{3}$'' (cf. figure \ref{fig:face}).
La ligne de définiton ``f $i_{1}$ $i_{3}$ $i_{2}$'' permettrait d'obtenir une
normale dans le sens opposé.


\begin{figure}[!h]
\centering
\includegraphics[width=0.30\textwidth,angle=0]{./figures/face.png}
\caption[]{Définition de la normale $\vec{n}$ à la facette [$P_{1}$, $P_{2}$, $P_{3}$] selon
la convention du trièdre direct}
\label{fig:face}
\end{figure}

On peut donc à présent définir une forme géométrique simple, par exemple
un cube. Un exemple est donné dans le fichier {\it cube.obj} qui est rappelé
ci-dessous:

\begin{center}
\verbatimtabinput[3]{cube.obj}
\end{center}

Les huit premières lignes définissent donc les coordonnées des vertex (les huit
sommets du cube), et les douze lignes suivantes définissent chacune des facettes
triangulaires à partir des indices de ces huit vertex, la normale à chaque
facette étant définie comme sortante du cube. De façon générale, c'est la
convention que nous nous proposons de conserver pour le projet: les normales
seront sortantes des solides. Etant donné que nous ne rencontrerons pas, à priori,
d'interfaces entre solides, toutes les interfaces qui seront définies seront
entre un solide et un milieu gazeux. Les normales aux facettes pointeront donc
vers le milieu gazeux (à conserver en mémoire pour le support des matériaux).

\begin{figure}[!h]
\centering
\includegraphics[width=0.50\textwidth,angle=0]{./figures/cube.png}
\caption[]{Visualisation du cube défini par le fichier {\it cube.obj} à l'aide de {\it meshlab},
avec représentation des normales aux facettes.}
\label{fig:cube}
\end{figure}

Le fichier {\it cube.obj} peut ensuite être visualisé par les outils standard
tels que {\it paraview} ou {\it meshlab}. La figure \ref{fig:cube} est une capture
d'écran de ce qui est représenté par {\it meshlab} après ouverture du fichier {\it cube.obj}.
Les axes et les normales aux facettes ont également été représentés.

Il est à noter que ces outils, que nous utiliserons afin de visualiser la conformité de
nos géométries par-rapport à l'attendu, permettent un rendu 'classique' des géométries.
Il ne s'agit pas d'un rendu par Monte-Carlo, ce qui sera réalisé ultérieurement par
le programme {\it htrdr}.
\newpage

\section{Support des matériaux}
\label{para:materials}

Dans le projet, nous souhaitons affecter des propriétés physiques à des groupes de
facettes triangulaires que nous appellerons {\it patch} (vocabulaire repris des formats
de fichier .h5).

\subsection{Le support des matériaux dans le format wavefront}

Le format wavefront supporte la définition des patchs. Le fichier obj doit ainsi commencer
par définir le fichier faisant office de bibliothèque de couleurs via la commande:

mtllib [file.mtl]

avec {\it file.mtl} le nom du fichier (d'extension .mtl) où sont définies les couleurs
RGB des différents matériaux. Chaque patch, ou groupe de facettes, utilisant une couleur
donnée, est défini à l'aide des conventions précédentes à base de lignes ``v'' et ``f'',
mais doit être précédé de la commande:

usemtl [material]

avec {\it material} le nom d'un matériau défini dans la bibliothèque de matériaux
(fichier .mtl).

\subsection{Le support des matériaux pour les besoins du projet}

Nous n'allons cependant pas exactement utiliser ce formalisme, car nous ne voulons pas
affecter une simple couleur à nos matériaux, mais des propriétés radiatives de surface.
Nos fichiers .obj vont donc utiliser le même mécanisme de définition des patchs de
surface, mais légèrement modifié:

\begin{itemize}
\item nous omettrons le fichier .mtl définissant la bibliothèque de couleurs
\item chaque patch sera précédé d'une ligne du type:
  usemtl [material1]:[material2]
\end{itemize}

Avec {\it material1} et {\it material2} les noms des deux matériaux qui composent
le milieu de part et d'autre de l'interface (le patch de surface), dans le sens de
la normale: le matériau {\it material1} sera le matériau du milieu trouvé sur la
face arrière (dans le sens opposé à la normale) et le matériau {\it material2} sera
le matériau du milieu rencontré sur la face avant (dans le sens de la normale
des facettes composant le patch). Bien entendu, toutes les normales des facettes
composant un patch doivent être définies avec la même convention, et être cohérentes
entre elles !

On peut également considérer que les deux matériaux {\it material1} et {\it material2}
sont rencontrés successivement lors d'une traversée de la surface dans le sens de
la normale.

Une variante exite avec trois matériaux:

usemtl [material1]:[material2]:[material3]

dans cette convention, {\it material1} est le matériau du milieu de la face arrière,
{\it material3} est le matériau du milieu rencontré en face avant, et {\it material2}
est le matériau utilisé pour l'interface elle-même. Il s'agit de la définition des
matériaux minces, où la facette n'est plus seulement une interface, mais porte
elle-même un matériau dont on ne désire pas représenter l'épaisseur, souvent pour
des raisons pratiques. Par exemple: les feuilles des arbres, pour lesquelles on
dispose de modèles trianglemesh. Mais définir le volume de chaque feuille serait
trop fastidieux, et pas forcément nécessaire pour des calculs à grande échelle.

\subsection{Les fichiers de propriétés radiatives}

Nous avons choisi de définir la réflectivité (spectrale) de chaque matériau
dans un fichier ascii portant le nom {\it material.dat} pour le matériau dont le nom
est {\it material}, et qui comporte une première ligne:

wavelengths [N]

avec {\it N} le nombre de longueurs d'onde qui vont être utilisées dans le fichier
pour décrire les propriétés radiatives du matériau. Le fichier comporte ensuite
trois colonnes:

\begin{itemize}
\item la longueur d'onde, en nanomètres
\item le type de fonction de réflectivité bidirectionnelle (BRDF). Pour l'instant, les
  mots-clef {\it lambertian} et {\it specular} sont supportés, d'autres types de BRDF
  peuvent bien entendu être définis.
\item la valeur de la réflectivité pour la longueur d'onde en question; pour les
  calculs SW (source de rayonnement externe au système), cette donnée sera utilisée
  telle-quelle puisqu'on n'a besoin que de l'albédo de la surface. Pour les calculs
  LW (rayonnement thermique interne) on a besoin de l'émissivité de la surface, qui
  est égale à 1 - la réflectivité.
\end{itemize}

Les valeurs de la réflectivité sont ainsi définies pour une collection de longueurs
d'onde. Une interpolation linéaire est utilisée pour retrouver la valeur de la
réflectivité entre deux valeurs de longueur d'onde successives.


\subsection{Exemple}

Le programme {\it spherical\_mesh} permet de produire un exemple de géométrie
multi-matériaux utilisant les formats de fichiers précédemment définis, pour une
géométrie sphérique.

La compilation du programme requiert l'utilisation du compilateur {\it gfortran}.
Si un autre compilateur doit être utilisé, le fichier {\it Makefile} doit être
modifié en conséquence. La commande {\it make all} exécutée dans le répertoire
principal {\it spherical\_mesh} permet de compiler l'exécutable {\it spherical\_mesh.exe}.
Ce dernier peut alors être exécuté. Par défaut, les fichiers suivants sont
générés dans le répertoire {\it results}:

\begin{itemize}
\item Le fichier {\it sphere.obj} qui définit plusieurs patchs de facettes triangulaires,
  avec les matériaux rencontrés en face arrière et en face avant. Par convention, le
  matériau de la face arrière est le matériau constituant le solide, et le matériau
  de la face avant est le mélange de gaz atmosphérique (``air'').
\item Les fichiers .dat de définition des propriétés radiatives de chaque matériau solide
  susceptible d'être utilisé dans l'application. Deux matériaux solides sont utilisés
  dans le fichier {\it sphere.obj}: il s'agit de {\it desert\_sand} et {\it forest\_green}.
  Le fichier {\it desert\_sand.dat} par exemple comporte les informations suivantes:
\verbatimtabinput[3]{desert_sand.dat}
\end{itemize}

Il s'agit des fichiers d'entrée dores et déjà utilisables par {\it htrdr}.

A seule fin de visualisation, les fichiers {\it sphere\_rgb.obj} et {\it materials.mtl}
sont également produits, dans la norme wavefront. Ces fichiers ne seront pas utilisés
pour les calculs de transfert radiatif, ils permettent simplement de visualiser la
géométrie produite par un outil tel que paraview ou meshlab (cf. figure \ref{fig:sphere}).

Le fichier {\it sphere.obj} produit par le programme {\it spherical\_mesh} est une
sphere (discrétisée en triangles) constituée pour l'essentiel d'un matériau {\it desert\_sand},
à l'exception d'une zone comprise entre 40 et 50 degrés de latitude, et entre 30 et
60 degrés de longitude, constituée du matériau {\it forest\_green}.

Les fichiers de propriétés radiatives (fichiers .dat) sont actuellement ceux hérités
du projet ModRadUrb, utilisés pour la description des villes. Les données physiques de base
sont comprises dans deux banques de données (fichiers .csv dans le répertoire {\it data} et
fichiers .properties contenus dans le répertoire {\it data/material\_properties}) qui sont
utilisées par {\it spherical\_mesh} afin de produire les fichiers .dat du répertoire {\it results}.
Bien entendu, il sera nécessaire de remplacer toutes ces données physiques par les
données pertinentes dans le cadre du projet Rad-Net.

\begin{figure}[!h]
\centering
\includegraphics[width=0.50\textwidth,angle=0]{./figures/sphere.png}
\caption[]{Visualisation par {\it meshlab} de la géométrie définie par le fichier {\it sphere\_rgb.obj}
  généré par le programme {\it spherical\_mesh}}
\label{fig:sphere}
\end{figure}

Le programme {\it spherical\_mesh} utilise un certain nombre de fichiers d'entrée
contenus dans le répertoire {\it data}, tels que les fichiers patch*.in permettant
de définir les différents patchs nécessaires pour couvrir la sphère. Ce programme est
fourni à titre d'exemple d'une chaîne de production de la donnée en amont de {\it htrdr}.

\section{Elévation}
\label{para:elevation}

Le format de fichier .obj décrit dans la partie \ref{para:format} est générique: il permet de décrire une géométrie 3D quelconque. Appliqué à l'étude des planètes, il permet de prendre en compte toute orographie. La seule contrainte étant de pouvoir produire les facettes triangulaires bien entendu ! Et ce n'est pas forcément immédiat: supposons que l'on dispose d'un jeu de données formidablement détaillé, consistant en un très grand nombre de positions spatiales (longitude / latitude / altiude) sur toute la surface de la planète, on n'a pas encore dit comment ces positions doivent être reliées entre elles pour former des triangles. Même dans l'exemple donné en partie \ref{para:materials}, la sphère est d'abord discrétisée en sections de longitude/latitude. Ces sections sont dans le cas général composées de 4 positions, sauf lorsqu'un des pôles est impliqué, auquel cas la section n'est composée que de 3 positions. Dans ce dernier cas, la facette triangulaire est immédiate, et dans le cas général, deux facettes triangulaires sont crées à partir des 4 positions en question. Cette façon de discrétiser la sphère résulte d'un choix, qui a le mérite d'être très facile à mettre en oeuvre, mais possède également de mauvaises propriétés: certains triangles sont beaucoup plus grands que d'autres, et le nombre de facettes qui se relient au pôles est égal au nombre d'intervalles de longitude. Il existe des méthodes de maillage triangulaire de la sphère de meilleure qualité (nombre réduit de triangles, triangles d'aires comparables, et composés d'angles compris entre 20 et 140 degrés), mais qui sont moins faciles à mettre en place.

Un des problèmes auxquels il faudra répondre dans le cadre de l'ANR Rad-Net sera d'être en mesure de produire des maillages de surface en tenant compte d'une carte d'élévation.
\begin{itemize}
\item Disposera t-on de jeux de données d'orographie sur toutes une planète ? J'en doute. Peut-être dispose t-on de ce genre de données pour la Terre. Dans ce cas, si ces données d'orographie ne viennent pas ``naturellement'' sous forme de facettes, il faudra être en mesure de produire les facettes triangulaires à partir des données fournies (positions ?)
\item Ce qui risque en revanche d'être plus courant, c'est la situation où on dispose d'un ensemble de positions spatiales (x,y,z) pour une petite zone de la planète. Je propose de s'intéresser à ce cas précis dans un premier temps.
\end{itemize}

L'idée est la suivante: on dispose d'une zone d'extension spatiale faible devant la planète, pour laquelle on dispose de données d'élévation. On se débarrasse pour l'instant de la production du maillage triangulaire dans cette zone (question séparée). On peut localement faire l'hypothèse que ce terrain est plat, et les positions spatiales pour lesquelles on dispose de l'élévation sont données dans un repère cartésien local (x,y). Le terrain $T$ peut être de forme quelconque. On sait identifier le secteur de planète $S$ où le terrain est situé, et les limites de $S$ sont ($\theta_{min}$, $\theta_{max}$, $\phi_{min}$, $\phi_{max}$) (voir figure \ref{fig:ST})

\begin{figure}[htbp]
\centering
 \mbox{
     \subfigure[]{\epsfig{figure=figures/ST1.png,width=0.4\textwidth}}\quad 
     \subfigure[]{\epsfig{figure=figures/ST2.png,width=0.4\textwidth}}}
  \caption[]{(a) Vue dans l'espace monde du secteur angulaire $S$ contenant le terrain $T$; (b) vue dans l'espace paramétrique cartésien attaché au domaine $S$}
\label{fig:ST}
\end{figure}

Si le terrain $T$ peut être entièrement positionné sur une facette triangulaire du secteur $S$, il ``suffit'' de remailler cette facette de façon à d'abord en exclure la zone correspondante à $T$, de façon à re-mailler uniquement la zone correspondante à la facette triangulaire moins $T$, puis rajouter le maillage correspondant à $T$. Mais il s'agit d'un cas très particulier; dans le cas général, la zone où doit être positionné $T$ va intersecter plusieurs facettes du secteur $S$. L'approche retenue dans l'exemple mis en oeuvre dans le petit code ``spherical\_mesh'' est la suivante:

\begin{figure}[!h]
\centering
\includegraphics[width=0.50\textwidth,angle=0]{./figures/crater.png}
\caption[]{Visualisation du ``cratère'' ajoûté à la sphère, comme exemple de production d'un sol avec relief}
\label{fig:crater}
\end{figure}

\begin{itemize}
\item Produire le maillage triangulaire du terrain $T$, en tenant compte de l'élévation, dans le repère cartésien local attaché à $T$. Dans le cas de notre exemple, il s'agit de produire le maillage associé à une forme évoquant un ``cratère'', produit analytiquement à partir de la rotation d'un profil sinusoïdal (voir figure \ref{fig:crater}). Ce maillage triangulaire doit ensuite être positionné dans un repère cartésien attaché au secteur angulaire $S$, où l'axe $x$ suit un parallèle, et où l'axe $y$ suit un méridien.
\item Identifier le contour externe de $T$, toujours dans le repère cartésien associé à $T$; par contour, on entend une suite de positions 2D (x,y) formant un contour fermé (avec une convention sur le sens de rotation). Il est important que l'altitude de toutes les positions de ce contour soient nulles: c'est le lieu des points où le maillage triangulaire de $T$ va rejoindre le maillage triangulaire de la sphère (points d'altitude nulle).
\item Appliquer un changement de repère à tous les vertex du maillage triangulaire de $T$: on souhaite ici déterminer les coordonnées 3D du vertex {\bf v}, dans le repère monde, à partir des coordonnées (x,y,z) du même vertex, dans le repère cartésien local associé à $S$. Connaissant ($\phi_{min}$, $\phi_{max}$, $\theta_{min}$ et $\theta_{max}$) les limites en latitude $\phi$ et longitude $\theta$ du secteur angulaire $S$, connaissant $\Delta x$ (dans le sens $\phi$) et $\Delta y$ (dans le sens $\theta$) les dimensions du rectangle associé au secteur angulaire $S$ dans le repère cartésien local, les coordonnées ($\phi$,$\theta$) du vertex de coordonnées (x,y) sont:
  \begin{equation}
    \begin{cases}
      \phi=\phi_{min}+\frac{\phi_{max}-\phi_{min}}{\Delta x}x \\
      \theta=\theta_{min}+\frac{\theta_{max}-\theta_{min}}{\Delta y}y
    \end{cases}
  \end{equation}

  On voit que même si on pourrait imaginer d'autres transformations, celle-ci a la bonne propriété que toutes les positions où x=0 correspondent bien à un méridien, toutes les positions où y=0 correspondent bien à un parallèle; le rectangle de dimensions ($\Delta x$, $\Delta y$) dans le repère cartésien local est bien transformé en le secteur $S$ de limites \{($\phi_{min}$, $\phi_{max}$), ($\theta_{min}$, $\theta_{max}$)\}.
  
  La coordonnée $r$ est simplement déterminée à partir du rayon de la planète $R$ et de l'altitude $z$ du vertex: $r=R+z$
  
  A partir des coordonnées sphériques ($r$,$\theta$,$\phi$) du vertex, on peut maintenant obtenir simplement ses coordonnées cartésiennes dans l'espace monde:
  
  \begin{equation}
    \begin{cases}
      x=r cos(\theta)cos(\phi) \\
      y=r cos(\theta)sin(\phi) \\
      z=r sin(\phi)
    \end{cases}
  \end{equation}
  A l'issue de cette étape, on dispose du maillage de $T$ dans l'espace monde.

\item Reste à obtenir un maillage de $S$-$T$; le maillage initial de $S$ faisait appel à un certain nombre de facette triangulaires. Ces dernières sont bien entendu perdues: il faut maintenant mailler l'espace compris entre le contour externe de $S$ et le contour externe de $T$. Ceci peut être fait, dans le cas général, dans le repère cartésien associé à $S$, à l'aide de n'importe quelle librairie de type ``triangulation de Delaunay''. Mais il faut le faire sans introduire de points supplémentaires dans le contour externe de $S$: si on introduisait de nouvelles positions dans ce contour, étant donné que ces positions doivent être situés à une altitude nulle, on introduirait des ``trous'' dans le maillage final (voir figure \ref{fig:remesh}). Une fois ce maillage obtenu dans le repère cartésien associé à $S$, il suffit d'appliquer la transformation précédente pour obtenir ce maillage dans l'espace monde.
  
\begin{figure}[htbp]
\centering
 \mbox{
     \subfigure[]{\epsfig{figure=figures/remesh1.png,width=0.4\textwidth}}\quad 
     \subfigure[]{\epsfig{figure=figures/remesh2.png,width=0.4\textwidth}}}
  \caption[]{(a) Vue dans un repère cartésien d'une facette triangulaire remaillée (en bleu) lorsque des points sont rajoutés sur l'arête commune avec une autre facette non retouchée (en noir); (b) vue après transformation dans l'espace monde, mettant en évidence le ``trou'' (polygone rouge) apparaissant après que les vertex soient positionnés sur la surface sphérique.}
\label{fig:remesh}
\end{figure}


\end{itemize}

\end{document}
