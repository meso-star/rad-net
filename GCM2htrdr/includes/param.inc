	double precision pi,kBz,c0,hPl,sigma
	double precision M_sun
	double precision M_earth
	double precision M_titan
	double precision R_sun
	double precision R_earth
	double precision R_titan
	double precision P0_earth
	double precision P0_titan
	double precision T0_titan
	double precision Earth_obliquity
	double precision Earth_synodic_period
	double precision Earth_sidereal_period
	double precision Earth_angular_speed
	double precision Earth_anomalistic_year
	double precision Rpg
	double precision Nav
	double precision Td,Tu,deltaT
        parameter(pi=3.141592653589793238462643383279502884197169399D+0)
	parameter(M_sun=1.9884D+30)    ! kg
	parameter(M_earth=5.9722D+24)  ! kg
	parameter(M_titan=1.345D+23)   ! kg
	parameter(R_sun=6.95991756D+8) ! m
	parameter(R_earth=6378.136D+3) ! m
	parameter(R_titan=2575.50D+3)  ! m
	parameter(P0_earth=1.013D+5)   ! Pa
	parameter(P0_titan=1.467D+5)   ! Pa
	parameter(T0_titan=93.7D+0)    ! K
c	-------------------------------------------------
c	parameter(Earth_anomalistic_year=365.259635864D+0) ! days of 24h
	parameter(Earth_anomalistic_year=360.0D+0) ! days of 24h
c	-------------------------------------------------
	parameter(Earth_obliquity=23.45D+0) ! deg
c	parameter(Earth_obliquity=23.58D+0) ! deg
c	-------------------------------------------------
c	Earth_synodic_period: time taken to be in the same position relative to the sun (def. of the day)
	parameter(Earth_synodic_period=24.0D+0) ! hours
c	-------------------------------------------------
c	Earth_sidereal_period: time taken to be in the same postion relative to distant stars
c	parameter(Earth_sidereal_period=23.93419D+0) ! hours
	parameter(Earth_sidereal_period=Earth_synodic_period
     &           *Earth_anomalistic_year
     &          /(Earth_anomalistic_year+1.0D+0)) ! hours
c	-------------------------------------------------
c	Earth angular speed (sidereal):
	parameter(Earth_angular_speed=2.0D+0*pi/Earth_sidereal_period) ! rad/hour
c	-------------------------------------------------
	parameter(kBz=1.3806D-23)    ! J/K     (Constante de Boltzmann)
	parameter(c0=2.9979D+8)      ! m/s     (célérité de la lumière dans le vide)
	parameter(hPl=6.6262D-34)    ! Js      (constante de Planck)
	parameter(sigma=5.6696D-8)   ! W/m2/K4 (constante de Stefan-Boltzman)
	parameter(Rpg=8.31446261815324D+0) ! J/(mol.K) (constante des gaz parfaits)
	parameter(Nav=6.02214076D+23 ) ! molec/mol (nombde d'Avogadro)

	parameter(Td=300.0D+0)
	parameter(Tu=320.0D+0)
	parameter(deltaT=10.0D+0)