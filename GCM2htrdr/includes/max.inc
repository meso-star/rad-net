	integer Ndim_mx
	integer Np_mx
	integer Ncell_mx
	integer*8 Nnode_mx
	integer Nprofile_mx
	integer Nlat_mx
	integer Nlon_mx
	integer Nlev_mx
	integer Nlay_mx
	integer Nspc_mx
	integer Nzone_mx
	integer Ncube_mx
	integer Ncontour_mx
	integer Nppt_mx
	integer Nppc_mx
        integer Nv_so_mx
        integer Nf_so_mx
	integer Nv_mx
	integer Nf_mx
	integer Npix_mx
	integer Npiag_mx
	integer Ncode_mx
	integer Nmaterial_mx
	integer Nmedium_mx
	integer Ndist_mx
	integer Nlambda_mx
	integer Nlambda_mat_mx
	integer Nlambda_ill_mx
	integer Nlambda_sun_mx
	integer Nb_mx
	integer Nq_mx
	integer Nangle_mx
	integer Nt_mx
	integer Niter_mx
	integer Nacc_mx
	integer Nfolders_mx
	integer Nfiles_mx
	integer Ngroup_mx
	integer Nfig_mx
	integer Nquotes_mx
	integer Nproba_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
	parameter(Np_mx=100)
	parameter(Ncell_mx=1600000)
	parameter(Nnode_mx=6300000)
	parameter(Nprofile_mx=5)
        parameter(Nlat_mx=100)
        parameter(Nlon_mx=2*Nlat_mx)
	parameter(Nlev_mx=120)
	parameter(Nlay_mx=Nlev_mx-1)
	parameter(Nspc_mx=100)
	parameter(Nzone_mx=100)
	parameter(Ncube_mx=20)
	parameter(Ncontour_mx=1000)
	parameter(Nppt_mx=1000)
	parameter(Nppc_mx=Nppt_mx)
        parameter(Nv_so_mx=1000000)
        parameter(Nf_so_mx=1000000)
        parameter(Nv_mx=10000000)
        parameter(Nf_mx=10000000)
	parameter(Npix_mx=64)
	parameter(Npiag_mx=500)
        parameter(Ncode_mx=200)
        parameter(Nmaterial_mx=20000)
        parameter(Nmedium_mx=20)
        parameter(Ndist_mx=100)
        parameter(Nlambda_mx=71)
        parameter(Nlambda_mat_mx=1000)
        parameter(Nlambda_ill_mx=1000)
	parameter(Nlambda_sun_mx=3500000)
        parameter(Nb_mx=Nlambda_mx-1)
	parameter(Nq_mx=16)
        parameter(Nangle_mx=100)
        parameter(Nt_mx=100)
        parameter(Niter_mx=100)
        parameter(Nacc_mx=50)
        parameter(Nfolders_mx=1000)
        parameter(Nfiles_mx=10000)
        parameter(Ngroup_mx=10)
        parameter(Nfig_mx=10)
	parameter(Nquotes_mx=100)
	parameter(Nproba_mx=100)
	parameter(Nchar_mx=1000)
