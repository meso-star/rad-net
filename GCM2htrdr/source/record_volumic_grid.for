      subroutine record_volumic_grid_file(dim,Nnode,Ncell,node_coord,cell_idx,volumic_grid_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record a volumic grid (binary) file
c     
c     Input:
c       + dim: dimension of space
c       + Nnode: number of nodes
c       + Ncell: number of cells
c       + node_coord: coordinates of each node
c       + cell_idx: indexes of the nodes for each cell
c       + volumic_grid_file: file to record
c     
c     Output: the required binary file
c     
c     I/O
      integer dim
      integer*8 Nnode
      integer*8 Ncell
      double precision node_coord(1:Nnode_mx,1:Ndim_mx)
      integer*8 cell_idx(1:Ncell_mx,1:Nvincell)
      character*(Nchar_mx) volumic_grid_file
c     temp
      integer*8 Nrecord,total_recorded
      integer record_size,remaining_byte
      logical*1 l1
      integer i,j,inode,icell
c     label
      character*(Nchar_mx) label
      label='subroutine record_volumic_grid_file'

      open(11,file=trim(volumic_grid_file),form='unformatted',access='stream')
c     record page size
      write(11) int(pagesize,kind(Nnode))
c     Record number of nodes
      write(11) Nnode
c     Record number of cells
      write(11) Ncell
c     Record dimension of space
      write(11) dim
c     Record number of nodes per cell
      write(11) Nvincell
c     ------------------------------------------------------------------
c     Padding file 11 ---
c     At this point 3 8-byte integers + 2 4-byte integers have been recorded (32 bytes)
      total_recorded=3*size_of_int8+2*size_of_int4
      call compute_padding2(pagesize,total_recorded,remaining_byte)
c     remaining_byte is the number of bytes that need to be added in order to obtain a full page
      write(11) (l1,i=1,remaining_byte)
c     --- Padding file 11
c     ------------------------------------------------------------------
      do inode=1,Nnode
         write(11) (node_coord(inode,j),j=1,dim)
      enddo
c     ------------------------------------------------------------------
c     Padding file 11 ---
c     "dim" double precisions have to be recorded each time (24 bytes).
      record_size=dim*size_of_double
      Nrecord=Nnode
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      write(11) (l1,i=1,remaining_byte)
c     --- Padding file 11
c     ------------------------------------------------------------------
      do icell=1,Ncell
         write(11) (cell_idx(icell,j)-1,j=1,Nvincell)
      enddo                     ! icell
c     ------------------------------------------------------------------
c     Padding file 11 ---
c     a record uses "Nvincell" 8-byte integers
      record_size=Nvincell*size_of_int8
      Nrecord=Ncell
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      write(11) (l1,i=1,remaining_byte)
c     --- Padding file 11
c     ------------------------------------------------------------------      
      close(11)
      write(*,*) 'File has been recorded: ',trim(volumic_grid_file)
   
      return
      end


      
      subroutine record_volumic_homogeneous_grid_file(dim,Nnode,Ncell,volumic_grid_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record a volumic grid (binary) file
c     
c     Input:
c       + dim: dimension of space
c       + Nnode: number of nodes
c       + Ncell: number of cells
c       + node_coord: coordinates of each node
c       + cell_idx: indexes of the nodes for each cell
c       + volumic_grid_file: file to record
c     
c     Output: the required binary file
c     
c     I/O
      integer dim
      integer*8 Nnode
      integer*8 Ncell
      character*(Nchar_mx) volumic_grid_file
c     temp
      integer*8 Nrecord,total_recorded
      integer record_size,remaining_byte
      logical*1 l1
      integer i,j,inode,icell,ios
      double precision node_coord(1:Ndim_mx)
      integer*8 cell_idx(1:Nvincell)
      character*(Nchar_mx) node_coord_file,cell_idx_file
      integer*8 Nnode_tmp,Ncell_tmp
c     label
      character*(Nchar_mx) label
      label='subroutine record_volumic_grid_file'

      node_coord_file='./node_coord.txt'
      cell_idx_file='./cell_idx.txt'
      open(11,file=trim(node_coord_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(node_coord_file)
         stop
      endif
      read(11,*) Nnode_tmp
      if (Nnode_tmp.ne.Nnode) then
         call error(label)
         write(*,*) 'input Nnode=',Nnode
         write(*,*) 'written in file:',Nnode_tmp
         stop
      endif
      open(12,file=trim(cell_idx_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(cell_idx_file)
         stop
      endif
      read(12,*) Ncell_tmp
      if (Ncell_tmp.ne.Ncell) then
         call error(label)
         write(*,*) 'input Ncell=',Ncell
         write(*,*) 'written in file:',Ncell_tmp
         stop
      endif
c      
      open(21,file=trim(volumic_grid_file),form='unformatted',access='stream')
c     record page size
      write(21) int(pagesize,kind(Nnode))
c     Record number of nodes
      write(21) Nnode
c     Record number of cells
      write(21) Ncell
c     Record dimension of space
      write(21) dim
c     Record number of nodes per cell
      write(21) Nvincell
c     ------------------------------------------------------------------
c     Padding file 21 ---
c     At this point 3 8-byte integers + 2 4-byte integers have been recorded (32 bytes)
      total_recorded=3*size_of_int8+2*size_of_int4
      call compute_padding2(pagesize,total_recorded,remaining_byte)
c     remaining_byte is the number of bytes that need to be added in order to obtain a full page
      write(21) (l1,i=1,remaining_byte)
c     --- Padding file 21
c     ------------------------------------------------------------------
      do inode=1,Nnode
         read(11,*) (node_coord(j),j=1,dim)
         write(21) (node_coord(j),j=1,dim)
      enddo
c     ------------------------------------------------------------------
c     Padding file 21 ---
c     "dim" double precisions have to be recorded each time (24 bytes).
      record_size=dim*size_of_double
      Nrecord=Nnode
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      write(21) (l1,i=1,remaining_byte)
c     --- Padding file 21
c     ------------------------------------------------------------------
      do icell=1,Ncell
         read(12,*) (cell_idx(j),j=1,Nvincell)
         write(21) (cell_idx(j)-1,j=1,Nvincell)
      enddo                     ! icell
c     ------------------------------------------------------------------
c     Padding file 21 ---
c     a record uses "Nvincell" 8-byte integers
      record_size=Nvincell*size_of_int8
      Nrecord=Ncell
      call compute_padding1(pagesize,record_size,Nrecord,remaining_byte)
      write(21) (l1,i=1,remaining_byte)
c     --- Padding file 21
c     ------------------------------------------------------------------      
      close(21)
      write(*,*) 'File has been recorded: ',trim(volumic_grid_file)
      write(*,*) 'Number of Nodes:',Nnode
      write(*,*) 'Number of cells:',Ncell

c     closing temporary files
      close(11)
      close(12)
      
      return
      end
