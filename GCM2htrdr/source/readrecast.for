      subroutine readrecast(optical_properties_file,temperature_file,
     &     nlatmx,nlayermx,nspecmx,ntt,
     &     wlnv0,wlnv1,lat0,lat1,z0,z1,
     &     thext,thsca,gh,tnext,tnsca,gn,
     &     tray,tgas,weight,temp,tsurf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read GCM data
c     
c     Input:
c       + optical_properties_file: file containing the geometrical and optical data
c       + temperature_file: file containing temperature data
c     
c     Output:
c       + nlatmx: number of latitude intervals
c       + nlayermx: number of altitude intervals
c       + nspecmx: number of spectral intervals
c       + ntt: quadrature order, for each spectral interval
c       + wlnv0: lower wavelength of each interval [nm]
c       + wlnv1: higher wavelength of each interval [nm]
c       + lat0: lower latitude for each interval [deg]
c       + lat1: higher latitude for each interval [deg]
c       + z0: lower altitude for latitude interval and each layer [m]
c       + z1: higher altitude for latitude interval end each layer [m]
c       + thext: haze extinction coefficient [m⁻¹]
c       + thsca: haze scattering coefficient [m⁻¹]
c       + gh: asymetry parameter for the haze
c       + tnext: cloud extinction coefficient [m⁻¹]
c       + tnsca: cloud scattering coefficient [m⁻¹]
c       + gn: asymetry parameter for the cloud
c       + tray: gas scattering coefficient [m⁻¹]
c       + tgas: gas absorption coefficient [m⁻¹]
c       + weight: gas quadrature weight
c       + temp: gas layer temperature [K]
c       + tsurf: surface temperature [K÷]
c     
c     I/O
      character*(Nchar_mx) optical_properties_file
      character*(Nchar_mx) temperature_file
      integer nlatmx
      integer nlayermx
      integer nspecmx
      integer ntt(1:Nb_mx)
      double precision wlnv0(1:Nb_mx)
      double precision wlnv1(1:Nb_mx)
      double precision lat0(1:Nlat_mx)
      double precision lat1(1:Nlat_mx)
      double precision z0(1:Nlat_mx,1:Nlay_mx)
      double precision z1(1:Nlat_mx,1:Nlay_mx)
      double precision thext(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision thsca(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision gh(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision tnext(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision tnsca(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision gn(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision tray(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision tgas(1:Nlat_mx,1:Nlay_mx,1:Nb_mx,1:Nq_mx)
      double precision weight(1:Nb_mx,1:Nq_mx)
      double precision temp(1:Nlat_mx,1:Nlay_mx)
      double precision tsurf(1:Nlat_mx)
c     temp
      real WLNV(1:Nb_mx)
      real LATI(1:Nlat_mx)
      character*1 ch
      integer ig,j,k,n,i1,i2,i3,i4,ios
      double precision t1,t2,t3,t4,t5
      double precision tauag,tausg,tauan,tausn,tauah,taush
c     label
      character*(Nchar_mx) label
      label='subroutine readrecast'

 121  format(4(1x,i5),50(1x,e18.11))
 122  format(2(1x,i5),50(1x,e18.11))

      open(unit=32,file=trim(optical_properties_file),status='old',iostat=ios)
      if (ios.eq.0) then
         write(*,*) 'Reading file: ',trim(optical_properties_file)
      else
         call error(label)
         write(*,*) 'File not found: ',trim(optical_properties_file)
         stop
      endif
      read(32,*) nlatmx,nlayermx,nspecmx ! dimension du modele (nbre de latitudes, couches, intervalles spectraux)
c     Debug
c      write(*,*) 'nlatmx=',nlatmx
c      write(*,*) 'nlayermx=',nlayermx
c     Debug
      do ig=1,nlatmx            ! boucle sur latitudes 
         do j=1,nlayermx        ! boucle sur altitude
            do k=1,nspecmx      ! boucle sur longueur d'onde
               read(32,*) ntt(k) ! nombre de termes par canal (Correlated -K)
               if (ntt(k).gt.Nq_mx) then
                  call error(label)
                  write(*,*) 'ntt(',k,')=',ntt(k)
                  write(*,*) '> Nq_mx=',Nq_mx
                  stop
               endif
               do n=1,ntt(k)
                  read(32,121) i1,i2,i3,i4,
     &                 wlnv(k)      ,lati(ig), ! longueur d'onde centrale de l'intervalle spectral, et latitude de la cellule [µm,°N] 
     &                 wlnv0(k)     ,wlnv1(k), ! bornes intervalle spectral (max, min) [µm, µm]
     &                 lat0(ig)     ,lat1(ig), ! bornes en latitude de la cellule (max, min) [°N]
     &                 z0(ig,j)     ,z1(ig,j), ! bornes en altitude de la cellule (max, min) [m]
     &                 thext(ig,j,k), thsca(ig,j,k)   ,gh(ig,j,k), ! brume  : k_ext,k_sca, g=<cos>               [1/m, 1/m, / ]
     &                 tnext(ig,j,k), tnsca(ig,j,k)   ,gn(ig,j,k), ! nuages : k_ext,k_sca, g=<cos>               [1/m, 1/m, / ]
     &                 tray(ig,j,k) ,  tgas(ig,j,k,n) ,weight(k,n) ! gaz    : k_Rayleigh, k_abs, poids k-correle [1/m, 1/m, / ]
               enddo            ! n
            enddo               ! k
         enddo
      enddo
      close(32)

c     Conversions
      do k=1,nspecmx
         wlnv(k)=wlnv(k)*1.0D+3 ! µm -> nm
         wlnv0(k)=wlnv0(k)*1.0D+3 ! µm -> nm
         wlnv1(k)=wlnv1(k)*1.0D+3 ! µm -> nm
c     Debug
c         write(*,*) k,wlnv0(k),wlnv1(k)
c     Debug
c     Invert limits when not in the correct order (min, max)
         if (wlnv0(k).gt.wlnv1(k)) then
            t1=wlnv0(k)
            wlnv0(k)=wlnv1(k)
            wlnv1(k)=t1
         endif
      enddo                     ! k
c     Detect inconsistent spectral grid:
c     - bands that are not specified by increasing wavelength
c     - bands that overlap
      do k=1,nspecmx-1
         if (wlnv1(k).gt.wlnv0(k+1)) then
            call error(label)
            write(*,*) 'Band index ',k,' [',wlnv0(k),'-',wlnv1(k),']'
            write(*,*) 'should precede band index ',k+1,' [',wlnv0(k+1),'-',wlnv1(k+1),']'
            stop
         endif
      enddo                     ! k

      open(unit=33,file=trim(temperature_file),status='old',iostat=ios)
      if (ios.eq.0) then
         write(*,*) 'Reading file: ',trim(temperature_file)
      else
         call error(label)
         write(*,*) 'File not found: ',trim(temperature_file)
         stop
      endif
      read(33,*) i1,i2!nlatmx,nlayermx ! dimension du modele (nbre de latitudes, couches)
      do ig=1,nlatmx            ! boucle sur latitudes 
         do j=1,nlayermx        ! boucle sur altitude
            read(33,122) i1,i2,
c     &           lat0(ig)     ,lat1(ig), ! borne latitude de la cellule                (°N)
c     &           z0(ig,j)     ,z1(ig,j), ! borne altitude de la cellule                (mètre)  !!  z1>z0
     &           t1,t2,t3,t4,
     &           temp(ig,j)     ! temperature de la cellule                   (K)
         enddo                  !j
         read(33,122)  i1,i2,
c     &        lat0(ig)     ,lat1(ig), ! borne latitude de la cellule                (°N)
c     &        z0(ig,j)     ,z1(ig,j), ! borne altitude de la cellule                (mètre)  !!  z1>z0
     &        t1,t2,t3,t4,
     &        tsurf(ig)         ! temperature de la surface / colonne ig      (K)
c         do j=1,nlayermx
c     Invert altitude limits that do not respect z1>z0
c            if (z1(ig,j).lt.z0(ig,j)) then
c               t1=z0(ig,j)
c               z0(ig,j)=z1(ig,j)
c               z1(ig,j)=t1
c            endif
c         enddo                  ! j
c     coherence check
c     Debug
c         if (ig.eq.1) then
c            do j=1,nlayermx
c               write(*,*) j,z0(ig,j),z1(ig,j)
c            enddo
c         endif
c     Debug
         do j=1,nlayermx-1
            if (z1(ig,j).ne.z0(ig,j+1)) then
               call error(label)
               write(*,*) 'z1(',ig,',',j,')=',z1(ig,j)
               write(*,*) 'z0(',ig,',',j+1,')=',z0(ig,j+1)
               write(*,*) 'should be equal'
               stop
            endif
         enddo                  ! j
      enddo                     ! ig
      close(33)
c     Debug
c      write(*,*) 'nlayermx=',nlayermx
c      do j=1,nlayermx
c         write(*,*) 'z0(',j,')=',z0(1,j),' z1(',j,')=',z1(1,j)
c      enddo                     ! j
c      stop
c     Debug
      
      
c     imperfect bounds
      lat0(1)=90.00
      lat1(nlatmx)=-90.00

      return
      end
