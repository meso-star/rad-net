c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine spectrum2colour(Nlambda_mat,lambda_mat,material_albedo,
     &     dim,source,Tsource,matrix,cs,ws,tone_mapping,exposure_bias,whitescale,gamma_correction,
     &     Np,xyz,omega_source,Nlambda_sun,lambda_sun,F_sun,Nlambda_ill,lambda_ill,spectrum_ill,
     &     color_rgb)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'param.inc'
      include 'constants.inc'
c     
c     Purpose: to test the conversion of a spectral signal into a colour
c
c     Input:
c       + dim: dimension of colorspace
c       + Nlambda_mat: number of values of the wavelength for material albedo
c       + lambda_mat: values of the wavelength for material albedo [nm]
c       + material_albedo: values of the material albedo
c       + source: (1) if HR Sun spectrum, (2) if Planck spectrum, (3) D65 Illuminant
c       + Tsource: temperature of Planck source
c       + matrix: (1) is XYZ -> RGB matrix must be computed, (2) if specified
c       + cs: colorspcae if matrix must be computed
c       + ws: working space if matrix is specified
c       + tone_mapping: T if tone mapping is enabled
c       + exposure_bias: exposure when tone mapping is enabled
c       + whitescale: white scale when tone mapping is enabled
c       + gamma_correction: T if gamma correction is enabled
c       + Np: number of values for x, y and z functions
c       + xyz: data
c        -xyz(i,1): value of the wavelength [nm]
c        -xyz(i,2): value of the x function
c        -xyz(i,3): value of the y function
c        -xyz(i,4): value of the z function
c       + omega_source: solid angle of solar source [sr]
c       + Nlambda_sun: number of wavelength points for HR solar spectrum
c       + lambda_sun: array of wavelength values for HR solar spectrum [nm]
c       + F_sun: array of corresponding solar flux density [W/m^2/micrometer]
c       + Nlambda_ill: number of lambda points for illuminant spectrum
c       + lambda_ill: values of the wavelength for illuminant spectrum [nm]
c       + spectrum_ill: values of the illuminant
c
c     Output
c       + color_rgb: output color (floats)
c
c     I/O
      integer Nlambda_mat
      double precision lambda_mat(1:Nlambda_mat_mx)
      double precision material_albedo(1:Nlambda_mat_mx)
      integer dim
      integer source
      double precision Tsource
      integer matrix
      character*(Nchar_mx) cs
      character*(Nchar_mx) ws
      logical tone_mapping
      double precision exposure_bias
      double precision whitescale
      logical gamma_correction
      integer Np
      double precision xyz(1:Np_mx,1:Ndim_mx+1)
      double precision omega_source
      integer*8 Nlambda_sun
      double precision lambda_sun(1:Nlambda_sun_mx)
      double precision F_sun(1:Nlambda_sun_mx)
      integer Nlambda_ill
      double precision lambda_ill(1:Nlambda_ill_mx)
      double precision spectrum_ill(1:Nlambda_ill_mx)
      double precision color_rgb(1:Ndim_mx)
c     temp
      character*(Nchar_mx) filename
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision inv_M(1:Ndim_mx,1:Ndim_mx)
      double precision lambda_ref(1:Np_mx)
      double precision illuminant(1:Np_mx)
      double precision spectrum(1:Np_mx)
      double precision signal(1:Np_mx)
      double precision color_xyz(1:Ndim_mx)
      double precision average_F
      double precision xyz_spectral_resolution
      double precision lambda,lambda_min,lambda_max
      double precision source_spectrum(1:Np_mx)
      double precision sum,sum_rgb,int_Yill
      double precision Xw,Yw,Zw
      integer ios,i,j,ilambda
      double precision albedo_spectrum(1:Nlambda_mat_mx)
      double precision v1,v2
      character*(Nchar_mx) command
c     functions
      double precision Blambda
      double precision sigmaT4
c     label
      character*(Nchar_mx) label
      label='program spectrum2colour'


c     ------------------------------------------------------------------------------------------------------------------
c     XYZ data
      do ilambda=1,Np
         lambda_ref(ilambda)=xyz(ilambda,1) ! [nm]
      enddo                     ! ilambda
      xyz_spectral_resolution=(lambda_ref(Np)-lambda_ref(1))/(Np-1) ! [nm]
      if (matrix.eq.1) then
         call xyz2rgb_matrix(dim,cs,M)
      else
         call specified_xyz2rgb_matrix(dim,ws,M)
      endif                     ! matrix
      call invert_matrix(dim,M,inv_M)
      Xw=inv_M(1,1)+inv_M(1,2)+inv_M(1,3)
      Yw=inv_M(2,1)+inv_M(2,2)+inv_M(2,3)
 2    Zw=inv_M(3,1)+inv_M(3,2)+inv_M(3,3)
c     Debug
c      write(*,*) 'M XYZ -> RGB='
c      do i=1,dim
c         write(*,*) (M(i,j),j=1,dim)
c      enddo                     ! i
c      write(*,*) 'Xw=',Xw
c      write(*,*) 'Yw=',Yw
c      write(*,*) 'Zw=',Zw
c     Debug
c     ------------------------------------------------------------------------------------------------------------------
c     
c     
c     ------------------------------------------------------------------------------------------------------------------
c     SOURCE
c
      do i=1,Np-1
         lambda_min=lambda_ref(i) ! [nm]
         lambda_max=lambda_ref(i+1) ! [nm]
         lambda=(lambda_min+lambda_max)/2.0D+0 !  [nm]
         if (source.eq.1) then
            source_spectrum(i)=
     &           Blambda(Tsource,lambda*1.0D-3)*omega_source/pi ! [W/m²/sr/µm]
         else if (source.eq.2) then
            call average_flux_over_interval(Nlambda_sun,
     &           lambda_sun,F_sun,
     &           lambda_min,lambda_max,average_F) ! [W/m²/µm]
            source_spectrum(i)=average_F/pi ! [W/m²/sr/µm]
         else if ((source.eq.3).or.(source.eq.4)) then
            call identify_illuminant(Nlambda_ill,lambda_ill,
     &           spectrum_ill,lambda_min,v1)
            call identify_illuminant(Nlambda_ill,lambda_ill,
     &           spectrum_ill,lambda_max,v2)
            source_spectrum(i)=(v1+v2)/2.0D+0
         endif                  ! source
      enddo                     ! i
c     Normalisation by the Y coordinate of the illuminant
      do i=1,Np-1
         signal(i)=source_spectrum(i)*(xyz(i,3)+xyz(i+1,3))/2.0D+0 ! [W/m^2/sr/micrometer]
      enddo                     ! i
      call integration_intervals(Np,lambda_ref,signal,int_Yill) ! [int_Yill]=W/m^2/sr/micrometer*nm
      int_Yill=int_Yill*1.0D-3 ! 
c     Debug
c      write(*,*) 'int_Yill=',int_Yill ! [W/m^2/sr]
c     Debug
      do i=1,Np-1
         source_spectrum(i)=source_spectrum(i)*3.0D+0/int_Yill ! [W/m²/sr/µm]
      enddo                     ! i
c     ------------------------------------------------------------------------------------------------------------------
c
c
c     ------------------------------------------------------------------------------------------------------------------
c     ALBEDO
c     average albedo over a 5 nm interval around each value of the reference wavelength
      filename='./data/albedo.dat'
      open(31,file=trim(filename))
      do i=1,Np-1
         lambda_min=lambda_ref(i) ! [nm]
         lambda_max=lambda_ref(i+1) ! [nm]
         lambda=(lambda_min+lambda_max)/2.0D+0 ! [nm]
         call average_albedo_over_interval(Nlambda_mat,
     &        lambda_mat,material_albedo,
     &        lambda_min,lambda_max,albedo_spectrum(i))
         write(31,*) lambda,albedo_spectrum(i) ! lambda in nm
      enddo                     ! i
      close(31)
c     ------------------------------------------------------------------------------------------------------------------
c
c      
c     ------------------------------------------------------------------------------------------------------------------
c     SPECTRAL SIGNAL TO CONVERT AS A COLOR
      do i=1,Np-1
         spectrum(i)=source_spectrum(i)*albedo_spectrum(i) ! [W/m²/sr/µm]
      enddo                     ! i
c     ------------------------------------------------------------------------------------------------------------------
c
c      
c     ------------------------------------------------------------------------------------------------------------------
c     CONVERSION
c     convert spectral signal in XYZ color
      call spectrum2xyz(dim,Np,xyz,spectrum,color_xyz)
c     tonemapping
      if (tone_mapping) then
         call filmic_tonemapping(dim,exposure_bias,whitescale,
     &        color_xyz)
      endif
c     convert XYZ into RGB
      call xyz2rgb(dim,M,gamma_correction,color_xyz,color_rgb)
c     display results
c      write(*,*) 'XYZ=',(color_xyz(i),i=1,dim)
      sum_rgb=0.0D+0
      do i=1,dim
         sum_rgb=sum_rgb+color_rgb(i)
      enddo                     ! i
c      do i=1,dim
c         RGB(i)=nint(color_rgb(i)*255)
c      enddo                     ! i
c      write(*,*) 'rgb=',(color_rgb(i)/sum_rgb*1.0D+2,i=1,dim)
c      write(*,21) 'RGB= ',nint(color_rgb(1)*255),' ',
c     &     nint(color_rgb(2)*255),' ',nint(color_rgb(3)*255)
c     ------------------------------------------------------------------------------------------------------------------

c     remove temporary files
      command='rm -f ./nlines*'
      call exec(command)
      
      end
