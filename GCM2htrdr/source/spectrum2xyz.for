c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine spectrum2xyz(dim,Np,xyz,spectrum,color_xyz)
      implicit none
      include 'max.inc'
c      
c      Purpose: to convert a spectral signal to a colour in XYZ colorspace
c      
c      Input:
c       + dim: dimension of colorspaces
c       + Np: number of values for x, y and z functions
c       + xyz: data
c        -xyz(i,1): value of the wavelength [nm]
c        -xyz(i,2): value of the x function
c        -xyz(i,3): value of the y function
c        -xyz(i,4): value of the z function
c       + spectrum: spectral signal defined for every wavelength of the "xyz" array [W/m^2/sr/micrometer]
c     
c     Output:
c       + color_xyz: colour in the XYZ colorspace 
c      
c     I/O
      integer dim
      integer Np
      double precision xyz(1:Np_mx,1:Ndim_mx+1)
      double precision spectrum(1:Np_mx)
      double precision color_xyz(1:Ndim_mx)
c     temp
      integer i,ip
      double precision lambda(1:Np_mx)
      double precision signal(1:Np_mx)
c      double precision xyz_spectral_resolution
c     label
      character*(Nchar_mx) label
      label='subroutine spectrum2xyz'

      do ip=1,Np
         lambda(ip)=xyz(ip,1)*1.0D-3 ! [nm] -> [micrometer]
      enddo                     ! ip
c      xyz_spectral_resolution=(xyz(Np,1)-xyz(1,1))*1.0D-3/(Np-1) ! [micrometers]
      do i=1,dim
         do ip=1,Np-1
            signal(ip)=spectrum(ip)*(xyz(ip,i+1)+xyz(ip+1,i+1))/2.0D+0 ! [W/m^2/sr/micrometer]
         enddo                  ! i
         call integration_intervals(Np,lambda,signal,color_xyz(i))
c     Debug
c         write(*,*) 'color_xyz(',i,')=',color_xyz(i)
c     Debug
      enddo                     ! ip
      
      return
      end
