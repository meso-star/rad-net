      subroutine fill_cloud_grid(dim,homogeneous_cloud_cells,debug,Nnode,Ntheta,Nphi,Nz,Nband,mft,mfz,
     &     lambda_min,lambda_max,kext_cloud,ks_cloud,g_cloud,
     &     Npf,cloud_radiative_properties_file,
     &     prefix,phase_function_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpse: to generate the radiative and thermodynamic properties files for the aerosol mode
c     
c     Input:
c       + dim: dimension of space
c       + homogeneous_cloud_cells: when T, cloud cells are homogeneous
c       + Nnode: number of nodes
c       + Ntheta: number of latitude intervals
c       + Nphi: number of longitude intervals
c       + Nz: number of altitude intervals
c       + Nband: number of spectral intervals
c       + mft: multiplication factor for latitude intervals
c       + mfz: multiplication factor for altitude intervals
c       + lambda_min: lower wavelength of each interval [nm]
c       + lambda_max: higher wavelength of each interval [nm]
c       + kext_cloud: cloud extinction coefficient [m⁻¹]
c       + ks_cloud: cloud scattering coefficient [m⁻¹]
c       + g_cloud: cloud asymetry parameter
c       + Npf: number of phase function definition files
c       + cloud_radiative_properties_file: binary file containing the optical properties of the haze (ka and ks)
c       + prefix: character string that constitutes the basis of the phase function file names
c       + phase_function_file: binary file containing the phase function of the haze
c     
c     Output: the required output files
c       + Nph: updated value
c     
c     I/O
      integer dim
      logical homogeneous_cloud_cells
      logical debug
      integer*8 Nnode
      integer Ntheta
      integer Nphi
      integer Nz
      integer Nband
      integer mft
      integer mfz
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision kext_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision ks_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision g_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      integer Npf
      character*(Nchar_mx) cloud_radiative_properties_file
      character*(Nchar_mx) prefix
      character*(Nchar_mx) phase_function_file
c     temp
      double precision ka_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      integer itheta,iphi,iz,inode,i,iband,ilambda,ilayer,Ncellinbox,jt,jz,ilat,ilay,jt_min,jt_max,jz_min,jz_max
      double precision ka(1:Nnode_mx)
      double precision ks(1:Nnode_mx)
      integer band_idx
      logical is_nan
      integer phase_function_type
      integer Nlambda_HG
      double precision lambda_HG(1:Nlambda_mx)
      double precision g_HG(1:Nlambda_mx)
      integer phase_Nlambda
      double precision phase_lambda(1:Nlambda_mx)
      integer Nangle
      double precision phase_angle(1:Nangle_mx)
      double precision pf(1:Nlambda_mx,1:Nangle_mx)
      integer phase_function_idx(1:Nnode_mx)
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone,j
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine fill_cloud_grid'
c
c     progress display ---
      ntot=int(Nband,kind(ntot))
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     --- progress display
      
c     Record radiative properties
      do iband=1,Nband
         do itheta=1,Ntheta
c     Compute the absorption coefficient
            do iz=1,Nz
               ka_cloud(itheta,iz,iband)=kext_cloud(itheta,iz,iband)-ks_cloud(itheta,iz,iband)
               if (ka_cloud(itheta,iz,iband).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'ka_cloud(',itheta,',',iz,',',iband,')=',ka_cloud(itheta,iz,iband)
                  write(*,*) 'kext_cloud(',itheta,',',iz,',',iband,')=',kext_cloud(itheta,iz,iband)
                  write(*,*) 'ks_cloud(',itheta,',',iz,',',iband,')=',ks_cloud(itheta,iz,iband)
                  stop
               endif
               call test_nan(ka_cloud(itheta,iz,iband),is_nan)
               if (is_nan) then
                  call error(label)
                  write(*,*) 'ka_cloud(',itheta,',',iz,',',iband,')=',ka_cloud(itheta,iz,iband)
                  stop
               endif
            enddo               ! iz
         enddo                  ! itheta
c     
         inode=0
         if (homogeneous_cloud_cells) then
c     use volumic grid produced by "volumic_grid_homogeneous_cells"
            do iz=1,Nz
               do jz=1,mfz
                  ilay=(iz-1)*mfz+jz
                  do itheta=1,Ntheta
                     do jt=1,mft
                        ilat=(itheta-1)*mft+jt
                        do iphi=1,Nphi
                           if (ilat.eq.1) then ! latitude interval in contact with the south pole
                              Ncellinbox=3
                           else if (ilat.eq.Ntheta) then ! latitude interval in contact with the north pole
                              Ncellinbox=3
                           else ! all other latitude intervalls
                              Ncellinbox=6
                           endif ! value of itheta
c     set identical properties over each node of each cell
                           do i=1,Nvincell*Ncellinbox
                              inode=inode+1
                              ka(inode)=ka_cloud(Ntheta-itheta+1,Nz-iz+1,iband)
                              ks(inode)=ks_cloud(Ntheta-itheta+1,Nz-iz+1,iband)
                              phase_function_idx(inode)=(Nz+1)*(itheta-1)+iz
c     Debug
c     if ((iband.eq.2).and.(iz.eq.1).and.(itheta.eq.1)) then
c     write(*,*) 'iphi=',iphi,' inode=',inode,' ka=',ka(inode),' ks=',ks(inode)
c     endif
c     Debug
                           enddo ! i
c     Debug
c                     stop
c     Debug
c     
                        enddo   ! iphi
                     enddo      ! jt
                  enddo         ! itheta
               enddo            ! jz
            enddo               ! iz
         else                   ! homogeneous_cloud_cells=F
c     use volumic grid produced by "volumic_grid"
c     itheta=1: cells that reunite at the south pole
            itheta=1
            do iphi=1,Nphi
               iz=1
               do i=1,3
                  inode=inode+1
                  ka(inode)=ka_cloud(Ntheta-itheta+1,Nz,iband)
                  ks(inode)=ks_cloud(Ntheta-itheta+1,Nz,iband)
                  phase_function_idx(inode)=(Nz+1)*(itheta-1)+iz
               enddo            ! i
               do iz=1,Nz
                  do jz=1,mfz
                     do i=1,3
                        inode=inode+1
                        ka(inode)=ka_cloud(Ntheta-itheta+1,Nz-iz+1,iband)
                        ks(inode)=ks_cloud(Ntheta-itheta+1,Nz-iz+1,iband)
                        phase_function_idx(inode)=(Nz+1)*(itheta-1)+iz+1
                     enddo      ! i
                  enddo         ! jz
               enddo            ! iz
            enddo               ! iphi
c     itheta=2,Ntheta-1: general case
            do itheta=1,Ntheta
               if (itheta.eq.1) then
                  jt_min=2
                  jt_max=mft
               else if (itheta.eq.Ntheta) then
                  jt_min=1
                  jt_max=mft-1
               else
                  jt_min=1
                  jt_max=mft
               endif
c                  
               do jt=jt_min,jt_max
                  do iphi=1,Nphi
                     iz=1
                     do i=1,4
                        inode=inode+1
                        ka(inode)=ka_cloud(Ntheta-itheta+1,Nz,iband)
                        ks(inode)=ks_cloud(Ntheta-itheta+1,Nz,iband)
                        phase_function_idx(inode)=(Nz+1)*(itheta-1)+iz
                     enddo      ! i
                     do iz=1,Nz
                        do jz=1,mfz
                           do i=1,4
                              inode=inode+1
                              ka(inode)=ka_cloud(Ntheta-itheta+1,Nz-iz+1,iband)
                              ks(inode)=ks_cloud(Ntheta-itheta+1,Nz-iz+1,iband)
                              phase_function_idx(inode)=(Nz+1)*(itheta-1)+iz+1
                           enddo ! i
                        enddo   ! jz
                     enddo      ! iz
                  enddo         ! iphi
               enddo            ! jt
            enddo               ! itheta
c     itheta=Ntheta: cells that reunite at the north pole
            itheta=Ntheta
            do iphi=1,Nphi
               iz=1
               do i=1,3
                  inode=inode+1
                  ka(inode)=ka_cloud(Ntheta-itheta+1,Nz,iband)
                  ks(inode)=ks_cloud(Ntheta-itheta+1,Nz,iband)
                  phase_function_idx(inode)=(Nz+1)*(itheta-1)+iz
               enddo            ! i
               do iz=1,Nz
                  do jz=1,mfz
                     do i=1,3
                        inode=inode+1
                        ka(inode)=ka_cloud(Ntheta-itheta+1,Nz-iz+1,iband)
                        ks(inode)=ks_cloud(Ntheta-itheta+1,Nz-iz+1,iband)
                        phase_function_idx(inode)=(Nz+1)*(itheta-1)+iz+1
                     enddo      ! i
                  enddo         ! jz
               enddo            ! iz
            enddo               ! iphi
         endif                  ! homogeneous_cloud_cells
c
         if (inode.ne.Nnode) then
            call error(label)
            write(*,*) 'inode=',inode
            write(*,*) 'should be equal to:',Nnode
            stop
         endif
         
c     progress display ---
         ndone=ndone+1
         fdone=dble(ndone)/dble(ntot)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif
c     --- progress display
         call record_cloud_radiative_properties_file(Nnode,Nband,
     &     lambda_min,lambda_max,ka,ks,iband,cloud_radiative_properties_file)
      enddo                     ! iband
c      
      write(*,*)
      write(*,*) 'Files have been recorded:'
      write(*,*) trim(cloud_radiative_properties_file)
      write(*,*) trim(phase_function_file)
      
c     Record phase function related files
      phase_function_type=0
      Nlambda_HG=Nband
      do ilambda=1,Nlambda_HG
         lambda_HG(ilambda)=(lambda_min(ilambda)+lambda_max(ilambda))/2.0D+0
      enddo                     ! ilambda
c$$$c     latitude bands itheta=1,Ntheta
c$$$      do itheta=1,Ntheta
c$$$         do jt=1,mft
c$$$            do iz=0,Nz
c$$$               if (iz.eq.0) then
c$$$                  jz_min=1
c$$$                  jz_max=1
c$$$               else
c$$$                  jz_min=1
c$$$                  jz_max=mfz
c$$$               endif
c$$$               do jz=jz_min,jz_max
c$$$                  Npf=Npf+1
c$$$                  if (iz.eq.0) then
c$$$                     ilayer=Nz
c$$$                  else
c$$$                     ilayer=Nz-iz+1
c$$$                  endif
c$$$                  do iband=1,Nband
c$$$                     g_HG(iband)=g_cloud(Ntheta-itheta+1,ilayer,iband)
c$$$                  enddo         ! iband
c$$$                  call record_phase_function_definition_file(prefix,Npf,
c$$$     &                 phase_function_type,Nlambda_HG,lambda_HG,g_HG,
c$$$     &                 phase_Nlambda,phase_lambda,Nangle,phase_angle,pf)
c$$$               enddo            ! jz
c$$$            enddo               ! iz
c$$$         enddo                  ! jt
c$$$      enddo                     ! itheta
      do itheta=1,Ntheta
         do iz=0,Nz
            Npf=Npf+1
            if (iz.eq.0) then
               ilayer=Nz
            else
               ilayer=Nz-iz+1
            endif
            do iband=1,Nband
               g_HG(iband)=g_cloud(Ntheta-itheta+1,ilayer,iband)
            enddo               ! iband
            call record_phase_function_definition_file(prefix,Npf,
     &           phase_function_type,Nlambda_HG,lambda_HG,g_HG,
     &           phase_Nlambda,phase_lambda,Nangle,phase_angle,pf)
         enddo                  ! iz
      enddo                     ! itheta
      write(*,*) 'Phase function definition files have been recorded'
c     record phase function file index per node
      call record_phase_function_file(Nnode,phase_function_idx,phase_function_file)
      
      return
      end
