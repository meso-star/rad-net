c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine write_obj(dim,output_file,mtllib_file,invert_normal,
     &     Nv,Nf,vertices,faces,Nmat,materials,mtl_idx)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'param.inc'
      include 'constants.inc'
c
c     Purpose: to write the main mesh to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file
c       + mtllib_file: name of the materials library file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + Nmat: number of materials
c       + materials: array of materials (name of the material for each face)
c       + mtl_idx: index of the material (in the "materials" list) for each face
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      character*(Nchar_mx) mtllib_file
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      integer mtl_idx(1:Nf_mx)
c     temp
      integer idim
      integer*8 iv,iface,tmp1,imat,i
      character*(Nchar_mx) front_name,line,mat_name,mat_file,libfile
      logical file_exists
      integer Nlambda_mat
      double precision lambda_mat(1:Nlambda_mat_mx)
      double precision material_albedo(1:Nlambda_mat_mx)
      double precision rgb(1:Ndim_mx)
      double precision d2source,Rsource,intF_sun
      character*(Nchar_mx) file_in
      double precision lambda_min,lambda_max
      integer dim_cs
      integer source
      double precision Tsource
      integer matrix
      character*(Nchar_mx) cs
      character*(Nchar_mx) ws
      logical tone_mapping
      double precision exposure_bias
      double precision whitescale
      logical gamma_correction
      integer Np
      double precision xyz(1:Np_mx,1:Ndim_mx+1)
      double precision omega_source
      integer*8 Nlambda_sun
      double precision lambda_sun(1:Nlambda_sun_mx)
      double precision F_sun(1:Nlambda_sun_mx)
      integer Nlambda_ill
      double precision lambda_ill(1:Nlambda_ill_mx)
      double precision spectrum_ill(1:Nlambda_ill_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine write_obj'

      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif

      if (invert_normal) then
         call invert_normals(Nf,faces)
      endif ! invert_normal

      front_name='air'
      open(12,file=trim(output_file))
      line='mtllib '//trim(mtllib_file)
      write(12,10) trim(line)
      do iv=1,Nv
         write(12,21) 'v',(vertices(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf
         mat_name=materials(mtl_idx(iface))(1:len_trim(materials(mtl_idx(iface)))-4)
         write(12,10) 'usemtl '//trim(mat_name)
         write(12,22) 'f',(faces(iface,iv),iv=1,3)
      enddo ! iface
      close(12)
c     ----------------------------------------------------------------------------------------------------
c     Configuration and input data for subroutine "spectrum2color"
c     ----------------------------------------------------------------------------------------------------
      dim_cs=3                  ! dimension of colorspace
      source=1                  ! (1) Planck spectrum, (2) Sun TOA HR spectrum, (3) std illuminant D65, (4) std illuminant A
      Tsource=5773.0D+0         ! Temperature of Planck source [K]
      matrix=1                  ! (1) produce it from colorsystem, (2) specified
      cs='Rec709'               ! NTSC, EBU, SMPTE, HDTV, CIE, Rec709
      ws='CIE_RGB'              ! Adobe_RGB, Apple_RGB, Best_RGB, Beta_RGB, Bruce_RGB, CIE_RGB, ColorMatch_RGB, Don_RGB4, ECI_RGB, EktaSpace_PS5, NTSC_RGB, PAL_SECAM_RGB, ProPhoto_RGB, SMPTE-C_RGB, WideGamut_RGB, sRGB
      tone_mapping=.false.
      exposure_bias=0.50D+0
      whitescale=11.2D+0
      gamma_correction=.false.

      file_in='./data/xyz.dat'
      call read_xyzdata(file_in,dim,Np,xyz)
      
c     Source spectrum
      if (source.eq.1) then
         d2source=1.0D+0*AU     ! [km]
         Rsource=1.0D+0*Rsun    ! [km]
         omega_source=pi*(Rsource/d2source)**2.0D+0 ! [sr] (6.79e-5 sr for Sun/Earth)
      else if (source.eq.2) then
         call read_HR_Sun_spectrum(Nlambda_sun,
     &        lambda_sun,F_sun,intF_sun) ! [micrometer] / [W/m^2/micrometer] / [W/m^2]
      else if (source.eq.3) then
         file_in='./data/standard_illuminant_D65.dat'
         call read_illuminant(file_in,
     &        Nlambda_ill,lambda_ill,spectrum_ill)
      else if (source.eq.4) then
         file_in='./data/standard_illuminant_A.dat'
         call read_illuminant(file_in,
     &        Nlambda_ill,lambda_ill,spectrum_ill)
      endif                     ! source
      lambda_min=360.0D+0       ! nm
      lambda_max=830.0D+0       ! nm
c     ----------------------------------------------------------------------------------------------------
c     Produce RGB color codes from the albedo spectral signal of each material
c     ----------------------------------------------------------------------------------------------------
      libfile='./results/'//trim(mtllib_file)
      inquire(file=trim(libfile),exist=file_exists)
      if (.not.file_exists) then
         open(12,file=trim(libfile))
      else
         open(12,file=trim(libfile),access='append')
      endif
      do imat=1,Nmat
         mat_file='./results/'//trim(materials(imat))
         mat_name=materials(imat)(1:len_trim(materials(imat))-4)
         call read_material_albedo(mat_file,Nlambda_mat,lambda_mat,material_albedo)
         call rescale_material_albedo(Nlambda_mat,lambda_mat,lambda_min,lambda_max)
         call spectrum2colour(Nlambda_mat,lambda_mat,material_albedo,
     &        dim_cs,source,Tsource,matrix,cs,ws,tone_mapping,exposure_bias,whitescale,gamma_correction,
     &        Np,xyz,omega_source,Nlambda_sun,lambda_sun,F_sun,Nlambda_ill,lambda_ill,spectrum_ill,
     &        rgb)
         line='newmtl '//trim(mat_name)
         write(12,10) trim(line)
         write(12,*) 'Ka ',(rgb(i),i=1,dim_cs)
         write(12,*) 'Kd ',(rgb(i),i=1,dim_cs)
         write(12,*)
      enddo                     ! imat
      close(12)

      return
      end



      subroutine write_single_obj(dim,output_file,invert_normal,
     &     Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to append the mesh for a single surface to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine write_single_obj'

      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif

      if (invert_normal) then
         call invert_normals(Nf,faces)
      endif ! invert_normal

      open(12,file=trim(output_file))
      do iv=1,Nv
         write(12,21) 'v',(vertices(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf
         write(12,22) 'f',(faces(iface,iv),iv=1,3)
      enddo ! iface
      close(12)
      
      return
      end


      
      subroutine append_single_obj(dim,output_file,invert_normal,
     &     Nv,Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to append the mesh for a single surface to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file to append
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: total number of vertices
c       + Nv01: number of vertices of the object
c       + Nf01: number of faces of the object
c       + v01: array of verices definition (3D position of each vertex)
c       + f01: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine append_single_obj'

      if (Nv01.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv01
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif
c
      if (invert_normal) then
         call invert_normals(Nf01,f01)
      endif                     ! invert_normal
c
      open(81,file=trim(output_file),access='append')
      do iv=1,Nv01
         write(81,21) 'v',(v01(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf01
         write(81,22) 'f',(Nv+f01(iface,iv),iv=1,3)
      enddo ! iface
      close(81)
c     
      return
      end


      
      subroutine append_single_obj_with_mtl(dim,
     &     output_file,invert_normal,
     &     is_thin,
     &     front_name,middle_name,back_name,
     &     Nv,Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to append the mesh for a single surface to a wavefront file
c     with the mtl file corresponding to input material/medium data
c
c     Front and Back are found relative to the normal:
c     - the FRONT material is the one encountered from the surface, in the
c     direction indicated by the normal
c     - the BACK material is the one encountered from the surface, in the
c     opposite direction from the one provided by the normal
c
c     + the MIDDLE material is the material that constitutes the interface
c     itself, only in the case of a thin material (is_thin=T)
c
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file to append
c       + invert_normal: true if the direction of normals has to be inverted
c       + is_thin: true if back material is thin
c       + front_name: name of the material or medium on the front
c       + middle_name: name of the material or medium on the middle (thin material only)
c       + back_name: name of the material or medium on the back
c       + Nv: total number of vertices
c       + Nv01: number of vertices of the object
c       + Nf01: number of faces of the object
c       + v01: array of verices definition (3D position of each vertex)
c       + f01: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      logical is_thin
      character*(Nchar_mx) front_name
      character*(Nchar_mx) middle_name
      character*(Nchar_mx) back_name
      integer Nv,Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:3)
c     temp
      integer idim,i,idot
      logical dot_found
      integer*8 iv,iface,tmp1
      character*(Nchar_mx) objmtl_file
c     label
      character*(Nchar_mx) label
      label='subroutine append_single_obj_with_mtl'

      if (Nv01.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv01
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif
c
      if (invert_normal) then
         call invert_normals(Nf01,f01)
      endif                     ! invert_normal

c     Append vertexes and faces to main OBJ file
      open(81,file=trim(output_file),access='append')
      if (is_thin) then
         write(81,10) 'usemtl '//trim(back_name)
     &        //':'//trim(middle_name)
     &        //':'//trim(front_name)
      else
         write(81,10) 'usemtl '//trim(back_name)
     &        //':'//trim(front_name)
      endif
      do iv=1,Nv01
         write(81,21) 'v',(v01(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf01
         write(81,22) 'f',(Nv+f01(iface,iv),iv=1,3)
      enddo ! iface
      close(81)
c     Append vertexes and faces to the OBJ file that uses a mtllib
c$$$      dot_found=.false.
c$$$      do i=len_trim(output_file),1,-1
c$$$         if (output_file(i:i).eq.'.') then
c$$$            dot_found=.true.
c$$$            idot=i
c$$$            goto 111
c$$$         endif
c$$$      enddo                     ! i
c$$$ 111  continue
c$$$      if (.not.dot_found) then
c$$$         call error(label)
c$$$         write(*,*) 'could not find a . in filename:',trim(output_file)
c$$$         stop
c$$$      else
c$$$         objmtl_file=output_file(1:idot-1)
c$$$     &        //'_rgb'
c$$$     &        //output_file(idot:len_trim(output_file))
c$$$      endif                     ! dot_found=F
c$$$c     
c$$$      open(82,file=trim(objmtl_file),access='append')
c$$$      write(82,10) 'usemtl '//trim(back_name)
c$$$      do iv=1,Nv01
c$$$         write(82,21) 'v',(v01(iv,idim),idim=1,dim)
c$$$      enddo ! iv
c$$$      do iface=1,Nf01
c$$$         write(82,22) 'f',(Nv+f01(iface,iv),iv=1,3)
c$$$      enddo ! iface
c$$$      close(82)
c$$$c     
      return
      end
