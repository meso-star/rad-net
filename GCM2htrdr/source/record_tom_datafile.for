      subroutine record_tom_datafile(datafile,dim,Ntheta,Nlayer,z_grid,theta_surface_temperature,
     &     Nlambda,lambda,reflectivity,
     &     Tgas,
     &     Nband,Nq,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,kext_cloud,ks_cloud,g_cloud,kext_haze,ks_haze,g_haze)
      implicit none
      include 'max.inc'
c     
c     Purpose: to record The Oignon Model input data file
c     
c     Input:
c       + datafile: file to record
c       + dim: dimension of space
c       + Ntheta: number of latitude intervals
c       + Nlayer: number of vertical layers
c       + z_grid: vertical grid [m]
c       + theta_surface_temperature: surface temperature [K]
c       + Nlambda: number of wavelength points for the definition of the ground reflectivity
c       + lambda: values of the wavelength for the definition of the ground reflectivity [nm]
c       + reflectivity: values of the ground reflectivity
c       + Tgas: gas temperature [K]
c       + Nq: quadrature order, for each spectral interval
c       + lambda_min: lower wavelength of each interval [nm]
c       + lambda_max: higher wavelength of each interval [nm]
c       + w: gas quadrature weight
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + kext_cloud: cloud extinction coefficient [m⁻¹]
c       + ks_cloud: cloud scattering coefficient [m⁻¹]
c       + g_cloud: cloud asymetry parameter
c       + kext_haze: haze extinction coefficient [m⁻¹]
c       + ks_haze: haze scattering coefficient [m⁻¹]
c       + g_haze: haze asymetry parameter
c     
c     Output:
c       + The required "datafile" file
c     
c     I/O
      character*(Nchar_mx) datafile
      integer dim
      integer Ntheta
      integer Nlayer
      double precision z_grid(1:Nlat_mx,0:Nlay_mx)
      double precision theta_surface_temperature(0:Nlat_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      double precision Tgas(1:Nlat_mx,1:Nlay_mx)
      integer Nband
      integer Nq(1:Nb_mx)
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision w(1:Nb_mx,1:Nq_mx)
      double precision ka_gas(1:Nlat_mx,1:Nlay_mx,1:Nb_mx,1:Nq_mx)
      double precision ks_gas(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision kext_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision ks_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision g_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision kext_haze(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision ks_haze(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision g_haze(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
c     temp
      integer itheta,ilayer,ilambda,iband,iquad
c     label
      character*(Nchar_mx) label
      label='subroutine record_tom_datafile'

      itheta=Ntheta/2
      
      open(21,file=trim(datafile))
      write(*,*) 'Recording TOM input data file...'
c     Geometry
      write(21,*) dim
      write(21,*) Nlayer
      do ilayer=0,Nlayer
         write(21,*) z_grid(itheta,ilayer)
      enddo
c     Ground temperature
      write(21,*) theta_surface_temperature(itheta)
c     Ground reflectivity
      write(21,*) Nlambda
      do ilambda=1,Nlambda
         write(21,*) lambda(ilambda)*1.0D-3,reflectivity(ilambda)
      enddo                     ! ilambda
c     Medium temperature
      do ilayer=1,Nlayer
         write(21,*) Tgas(Ntheta-itheta+1,Nlayer-ilayer+1)
      enddo                     ! ilayer
c     Number of aerosol modes
      write(21,*) 2
c     Spectral grid & radiative properties
      write(21,*) Nband
      do iband=1,Nband
         write(21,*) lambda_min(iband)*1.0D-3,lambda_max(iband)*1.0D-3
         write(21,*) Nq(iband)
         do iquad=1,Nq(iband)
            write(21,*) w(iband,iquad)
            do ilayer=1,Nlayer
               write(21,*) ka_gas(Ntheta-itheta+1,Nlayer-ilayer+1,iband,iquad)
            enddo               ! ilayer
         enddo                  ! iquad
         do ilayer=1,Nlayer
            write(21,*) ks_gas(Ntheta-itheta+1,Nlayer-ilayer+1,iband)
         enddo                  ! ilayer
         do ilayer=1,Nlayer
            write(21,*) kext_cloud(Ntheta-itheta+1,Nlayer-ilayer+1,iband)-ks_cloud(Ntheta-itheta+1,Nlayer-ilayer+1,iband)
         enddo                  ! ilayer
         do ilayer=1,Nlayer
            write(21,*) ks_cloud(Ntheta-itheta+1,Nlayer-ilayer+1,iband)
         enddo                  ! ilayer
         do ilayer=1,Nlayer
            write(21,*) g_cloud(Ntheta-itheta+1,Nlayer-ilayer+1,iband)
         enddo                  ! ilayer
         do ilayer=1,Nlayer
            write(21,*) kext_haze(Ntheta-itheta+1,Nlayer-ilayer+1,iband)-ks_haze(Ntheta-itheta+1,Nlayer-ilayer+1,iband)
         enddo                  ! ilayer
         do ilayer=1,Nlayer
            write(21,*) ks_haze(Ntheta-itheta+1,Nlayer-ilayer+1,iband)
         enddo                  ! ilayer
         do ilayer=1,Nlayer
            write(21,*) g_haze(Ntheta-itheta+1,Nlayer-ilayer+1,iband)
         enddo                  ! ilayer
      enddo                     ! iband
      close(21)
      write(*,*) 'done'

      return
      end
