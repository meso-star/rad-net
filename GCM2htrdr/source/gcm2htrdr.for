c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      program gcm2htrdr
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'formats.inc'
      include 'size_params.inc'
c     
c     data.in
      integer dim
      character*(Nchar_mx) datafile
      logical produce_surface_properties
      logical produce_gas_properties
      logical produce_cloud_properties
      logical produce_haze_properties
      character*(Nchar_mx) data_dir
      integer Nphi
      integer mft
      integer mfz
      logical record_tom_file
      double precision planet_radius
      character*(Nchar_mx) mtlidx_file
      character*(Nchar_mx) grid_file_surface
      character*(Nchar_mx) surface_properties_file
      double precision gas_radius
      character*(Nchar_mx) gas_grid_file
      character*(Nchar_mx) gas_radiative_properties_file
      character*(Nchar_mx) gas_thermodynamic_properties_file
      logical homogeneous_gas_cells
      integer Npf_cloud,Npf_haze
      character*(Nchar_mx) cloud_phase_function_list_file
      logical homogeneous_cloud_cells
      double precision cloud_radius
      character*(Nchar_mx) cloud_grid_file
      character*(Nchar_mx) cloud_radiative_properties_file
      character*(Nchar_mx) cloud_phase_function_file
      character*(Nchar_mx) haze_phase_function_list_file
      logical homogeneous_haze_cells
      double precision haze_radius
      character*(Nchar_mx) haze_grid_file
      character*(Nchar_mx) haze_radiative_properties_file
      character*(Nchar_mx) haze_phase_function_file
      character*(Nchar_mx) optical_properties_file
      character*(Nchar_mx) temperature_file
c     GCM data
      character*(Nchar_mx) default_reflectivity_file
      integer Ntheta
      integer Nlayer
      integer Nband
      integer Nq(1:Nb_mx)
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision theta_min(1:Nlat_mx)
      double precision theta_max(1:Nlat_mx)
      double precision z_min(1:Nlat_mx,1:Nlay_mx)
      double precision z_max(1:Nlat_mx,1:Nlay_mx)
      double precision kext_haze(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision ks_haze(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision g_haze(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision kext_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision ks_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision g_cloud(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision ks_gas(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision ka_gas(1:Nlat_mx,1:Nlay_mx,1:Nb_mx,1:Nq_mx)
      double precision w(1:Nb_mx,1:Nq_mx)
      double precision Tgas(1:Nlat_mx,1:Nlay_mx)
      double precision Tsurf(1:Nlat_mx)
c     default reflectivity signal
      integer Nlambda_default
      double precision lambda_default(1:Nlambda_mx)
      double precision reflectivity_default(1:Nlambda_mx)
      double precision brdf_default
c     temp
      character*(Nchar_mx) command,obj_file,mtllib_file
      double precision grid_theta_gas(0:Ncell_mx)
      double precision grid_phi_gas(0:Ncell_mx)
      double precision grid_alt_gas(0:Ncell_mx)
      double precision grid_theta_cloud(0:Ncell_mx)
      double precision grid_phi_cloud(0:Ncell_mx)
      double precision grid_alt_cloud(0:Ncell_mx)
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:3)
      integer*8 Nnode,Ncell
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      integer i,j,iband,iq,ivertex,iface,itheta,ilambda,ilayer,it,ilat
      double precision theta_grid(0:Nlat_mx)
      double precision phi_grid(0:Nlon_mx)
      double precision z_grid(1:Nlat_mx,0:Nlay_mx)
      double precision theta_surface_temperature(0:Nlat_mx)
      integer idx(1:3)
      character*(Nchar_mx) material,name,mtl_file,brdf_type
      logical file_exists
      integer mtl_idx(1:Nf_mx)
      character*(Nchar_mx) prefix
c     label
      character*(Nchar_mx) label
      label='program gcm2htrdr'

      dim=3                     ! Dimension of physical space
      datafile='./data.in'
      call read_data(datafile,data_dir,Nphi,mft,mfz,record_tom_file,
     &     produce_surface_properties,planet_radius,mtlidx_file,grid_file_surface,surface_properties_file,
     &     produce_gas_properties,gas_radius,gas_grid_file,gas_radiative_properties_file,gas_thermodynamic_properties_file,homogeneous_gas_cells,
     &     produce_cloud_properties,cloud_phase_function_list_file,cloud_radius,cloud_grid_file,cloud_radiative_properties_file,cloud_phase_function_file,homogeneous_cloud_cells,
     &     produce_haze_properties,haze_phase_function_list_file,haze_radius,haze_grid_file,haze_radiative_properties_file,haze_phase_function_file,homogeneous_haze_cells)
c
      obj_file='./results/sphere.obj' ! OBJ file for recording the scene
      mtllib_file='materials.mtl'     
      call init(Npf_cloud,Npf_haze,cloud_phase_function_list_file,haze_phase_function_list_file,obj_file,mtllib_file)
c     
c      optical_properties_file=trim(data_dir)//'tableGCM.dat'
      optical_properties_file=trim(data_dir)//'tableGCM.dat'
      temperature_file=trim(data_dir)//'tableTGCM.dat'
      call readrecast(optical_properties_file,temperature_file,
     &     Ntheta,Nlayer,Nband,Nq,
     &     lambda_min,lambda_max,theta_min,theta_max,z_min,z_max,
     &     kext_haze,ks_haze,g_haze,kext_cloud,ks_cloud,g_cloud,
     &     ks_gas,ka_gas,w,Tgas,Tsurf)
      
      write(*,*) 'Number of longitude intervals:',Nphi
      write(*,*) 'Number of latitude intervals:',Ntheta*mft
      write(*,*) 'Number of altitude intervals:',Nlayer*mfz
      write(*,*) 'Number of spectral intervals:',Nband
      
      if (Ntheta*mft.gt.Nlat_mx) then
         call error(label)
         write(*,*) 'Ntheta*mft=',Ntheta*mft
         write(*,*) '> Nlat_mx=',Nlat_mx
         stop
      endif
      if (Nphi.gt.Nlon_mx) then
         call error(label)
         write(*,*) 'Nphi=',Nphi
         write(*,*) '> Nlon_mx=',Nlon_mx
         stop
      endif
      if (Nlayer*mfz.gt.Nlay_mx) then
         call error(label)
         write(*,*) 'Nlayer*mfz=',Nlayer*mfz
         write(*,*) '> Nlay_mx=',Nlay_mx
         stop
      endif
c     theta_grid: limits of latitude intervals [deg]
      i=0
      theta_grid(i)=theta_max(Ntheta) ! reverse order
      do itheta=1,Ntheta
         do j=1,mft
            i=i+1
            if (i.gt.Nlat_mx) then
               call error(label)
               write(*,*) 'Nlat_mx has been reached'
               stop
            endif
            theta_grid(i)=theta_max(Ntheta-itheta+1)+(theta_min(Ntheta-itheta+1)-theta_max(Ntheta-itheta+1))/mft*j
         enddo                  ! j
      enddo                     ! itheta
c     Debug
c      do i=0,Ntheta*mft
c         write(*,*) 'theta_grid(',i,')=',theta_grid(i)
c      enddo                     ! i
c      stop
c     Debug

c     phi_grid: limits of longitude intervals [deg]
      call generate_homogeneous_longitude_grid(Nphi,phi_grid)
c     z_grid: altitude levels, per longitude interval [m]
      do itheta=1,Ntheta
         do it=1,mft
            ilat=(itheta-1)*mft+it
            i=0
            z_grid(ilat,i)=planet_radius+z_max(Ntheta-itheta+1,Nlayer)
            do ilayer=1,Nlayer
               do j=1,mfz
                  i=i+1
                  if (i.gt.Nlay_mx) then
                     call error(label)
                     write(*,*) 'Nlay_mx has been reached'
                     stop
                  endif
                  z_grid(ilat,i)=planet_radius+z_max(Ntheta-itheta+1,Nlayer-ilayer+1)+(z_min(Ntheta-itheta+1,Nlayer-ilayer+1)-z_max(Ntheta-itheta+1,Nlayer-ilayer+1))/mfz*j
               enddo            ! j
            enddo               ! ilayer
         enddo                  ! it
      enddo                     ! itheta
c     
c      do itheta=1,Ntheta
c         z_grid(itheta,0)=planet_radius+z_max(Ntheta-itheta+1,Nlayer)
c         do ilayer=1,Nlayer
c            z_grid(itheta,ilayer)=planet_radius+z_min(Ntheta-itheta+1,Nlayer-ilayer+1)
c         enddo                  ! iz
c     Debug
c      do ilayer=0,Nlayer*mfz
c         write(*,*) 'z_grid(',ilayer,')=',z_grid(3,ilayer)
c      enddo                     ! ilayer
c      stop
c     Debug

c     =======================================================
c     Production of surface properties
c     =======================================================
      if (produce_surface_properties) then
         write(*,*) '+--------------------------------------------------+'
         write(*,*) '|                     SURFACE                      |'
         write(*,*) '+--------------------------------------------------+'
         command='rm -f '//trim(grid_file_surface)
         call exec(command)
c     -------------------------------------------------------
c     Initialization
c     -------------------------------------------------------
c         default_reflectivity_file=trim(data_dir)//'default_reflectivity.dat'
         default_reflectivity_file=trim(data_dir)//'modified_reflectivity.dat'
         call read_default_reflectivity(default_reflectivity_file,Nlambda_default,lambda_default,reflectivity_default)
         brdf_default=0.0D+0    ! diffuse reflection
c     Create default material
         do j=1,3
            idx(j)=0
         enddo                  ! j
         call pixel_mtl_name(idx(1),idx(2),idx(3),material,name)
         Nmat=1
         materials(1)=trim(name)
         mtl_file='./results/'//trim(name)
         inquire(file=trim(mtl_file),exist=file_exists)
         if (.not.file_exists) then
            if (brdf_default.eq.0.0D+0) then
               brdf_type='lambertian'
            else if (brdf_default.eq.1.0D+0) then
               brdf_type='specular'
            else
               call error(label)
               write(*,*) 'brdf_default=',brdf_default
               write(*,*) 'is not supported yet'
               stop
            endif
            open(14,file=trim(mtl_file))
            write(14,23) 'wavelengths',Nlambda_default
            do ilambda=1,Nlambda_default
               write(14,*) lambda_default(ilambda), ! nm
     &              trim(brdf_type),
     &              reflectivity_default(ilambda)
            enddo               ! ilambda
            close(14)
            write(*,*) 'File was recorded: ',trim(mtl_file)
         endif                  ! file_exists=F
         
         Nv=0
         Nf=0
         write(*,*) 'Generating geometry...'
c     
c     theta_surface_temperature: surface temperature of latitude intervals [K]
         do itheta=1,Ntheta
            i=0
            do j=1,mft
               i=i+1
               theta_surface_temperature(i)=Tsurf(Ntheta-itheta+1) ! [K]
            enddo               ! j
         enddo                  ! itheta
c     produce the surface grid
         call partial_sphere_obj(dim,planet_radius,
     &        -pi/2.0D+0,pi/2.0D+0,
     &        0.0D+0,2.0D+0*pi,
     &        Ntheta*mft,Nphi,
     &        Nv_so,Nf_so,v_so,f_so)
         do ivertex=1,Nv_so
            Nv=Nv+1
            do j=1,dim
               vertices(Nv,j)=v_so(ivertex,j)
            enddo               ! j
         enddo                  ! ivertex
         do iface=1,Nf_so
            Nf=Nf+1
            do j=1,Nvinface
               faces(Nf,j)=f_so(iface,j)
            enddo               ! j
            mtl_idx(iface)=1    ! use default material for every face
         enddo                  ! iface
         call record_surface_data(dim,Ntheta*mft,theta_grid,theta_surface_temperature,
     &        mtlidx_file,grid_file_surface,obj_file,mtllib_file,surface_properties_file,
     &        Nv,Nf,vertices,faces,Nmat,materials,mtl_idx)
c     Debug
         write(*,*) 'Number of vertices:',Nv
         write(*,*) 'Number of faces:',Nf
c     Debug
      endif                     ! produce_surface_properties
      
      
c     =======================================================
c     Production of gas mixture properties
c     =======================================================
      if (produce_gas_properties) then
         write(*,*) '+--------------------------------------------------+'
         write(*,*) '|                   GAS MIXTURE                    |'
         write(*,*) '+--------------------------------------------------+'
c     
         command='rm -f '//trim(gas_grid_file)
         call exec(command)
c     Produce node and tetrahedric cells
         if (homogeneous_gas_cells) then
            call volumic_grid_homogeneous_cells(dim,Ntheta*mft,Nphi,Nlayer*mfz,theta_grid,phi_grid,z_grid,gas_grid_file,Nnode,Ncell)
         else
            call volumic_grid(dim,Ntheta*mft,Nphi,Nlayer*mfz,theta_grid,phi_grid,z_grid,gas_grid_file,Nnode,Ncell)
         endif
c     Record thermodynamic properties
         call fill_gas_grid(dim,homogeneous_gas_cells,Nnode,Ntheta,Nphi,Nlayer,Nband,Nq,mft,mfz,
     &        lambda_min,lambda_max,w,ka_gas,ks_gas,Tgas,
     &        gas_radiative_properties_file,gas_thermodynamic_properties_file)
         write(*,*) 'Volume (gas):'
         write(*,*) 'Number of nodes:',Nnode
         write(*,*) 'Number of cells:',Ncell
      endif                     ! produce_gas_properties
      
c     =======================================================
c     Production of cloud & haze properties
c     =======================================================
      if (produce_cloud_properties) then
         write(*,*) '+--------------------------------------------------+'
         write(*,*) '|                       CLOUD                      |'
         write(*,*) '+--------------------------------------------------+'
         prefix='cloud'
         command='rm -f '//trim(cloud_grid_file)
         call exec(command)
c     Produce node and tetrahedric cells
         if (homogeneous_cloud_cells) then
            call volumic_grid_homogeneous_cells(dim,Ntheta*mft,Nphi,Nlayer*mfz,theta_grid,phi_grid,z_grid,cloud_grid_file,Nnode,Ncell)
         else
            call volumic_grid(dim,Ntheta*mft,Nphi,Nlayer*mfz,theta_grid,phi_grid,z_grid,cloud_grid_file,Nnode,Ncell)
         endif
c     Record thermodynamic properties
         call fill_cloud_grid(dim,homogeneous_cloud_cells,.false.,Nnode,Ntheta,Nphi,Nlayer,Nband,mft,mfz,
     &        lambda_min,lambda_max,kext_cloud,ks_cloud,g_cloud,
     &        Npf_cloud,cloud_radiative_properties_file,
     &        prefix,cloud_phase_function_file)
c     Record the list of phase function definition files
         call record_phase_function_list_file(Npf_cloud,cloud_phase_function_list_file,prefix)
c     Print some statistics
         write(*,*) 'Volume (cloud):'
         write(*,*) 'Number of nodes:',Nnode
         write(*,*) 'Number of cells:',Ncell
      endif                     ! produce_cloud_properties
      if (produce_haze_properties) then
         write(*,*) '+--------------------------------------------------+'
         write(*,*) '|                       HAZE                      |'
         write(*,*) '+--------------------------------------------------+'
         prefix='haze'
         command='rm -f '//trim(haze_grid_file)
         call exec(command)
c     Produce node and tetrahedric cells
         if (homogeneous_haze_cells) then
            call volumic_grid_homogeneous_cells(dim,Ntheta*mft,Nphi,Nlayer*mfz,theta_grid,phi_grid,z_grid,haze_grid_file,Nnode,Ncell)
         else
            call volumic_grid(dim,Ntheta*mft,Nphi,Nlayer*mfz,theta_grid,phi_grid,z_grid,haze_grid_file,Nnode,Ncell)
         endif
c     Record thermodynamic properties
         call fill_cloud_grid(dim,homogeneous_haze_cells,.true.,Nnode,Ntheta,Nphi,Nlayer,Nband,mft,mfz,
     &        lambda_min,lambda_max,kext_haze,ks_haze,g_haze,
     &        Npf_haze,haze_radiative_properties_file,
     &        prefix,haze_phase_function_file)
c     Record the list of phase function definition files
         call record_phase_function_list_file(Npf_haze,haze_phase_function_list_file,prefix)
c     Print some statistics
         write(*,*) 'Volume (haze):'
         write(*,*) 'Number of nodes:',Nnode
         write(*,*) 'Number of cells:',Ncell
      endif                     ! produce_haze_properties

      if (record_tom_file) then
         datafile='./results/TOM_input.dat'
         call record_tom_datafile(datafile,dim,Ntheta,Nlayer,z_grid,theta_surface_temperature,
     &     Nlambda_default,lambda_default,reflectivity_default,
     &     Tgas,
     &     Nband,Nq,lambda_min,lambda_max,w,
     &     ka_gas,ks_gas,kext_cloud,ks_cloud,g_cloud,kext_haze,ks_haze,g_haze)
      endif

c     -------------------------------------------------------
c     proper exit
c     -------------------------------------------------------
      
      command='rm -f list.txt'
      call exec(command)
      command='rm -f local'
      call exec(command)
      command='rm -f nlines_*'
      call exec(command)
      command='rm -f status'
      call exec(command)

      end
