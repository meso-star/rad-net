      subroutine identify_nu(str,nu_found,nu)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify a value of the wavenumber within a character string
c     
c     Input:
c       + str: character string
c     
c     Output:
c       + nu_found: true if a value of the wavenumber has be found
c       + nu: value of the wavenumber when nu_found=.true. (double precision) [inv. cm]
c     
c     I/O
      character*(Nchar_mx) str
      logical nu_found
      double precision nu
c     temp
      logical possible_match
      integer i,i0
      integer Nspc,spc_idx(0:Nspc_mx)
      character*(Nchar_mx) nu_str
      integer errcode
c     label
      character*(Nchar_mx) label
      label='subroutine identify_nu'

      possible_match=.false.
      do i=1,len_trim(str)-4
         if (str(i:i+4).eq.'cm-1') then
            possible_match=.true.
            i0=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.possible_match) then
         nu_found=.false.
      else
c     Identify spaces from i0
         Nspc=0
         do i=i0,1,-1
            if (str(i:i).eq.' ') then
               Nspc=Nspc+1
               if (Nspc.gt.Nspc_mx) then
                  call error(label)
                  write(*,*) 'Nspc_mx=',Nspc_mx
                  write(*,*) 'was reached'
                  stop
               endif
               spc_idx(Nspc)=i
            endif
         enddo                  ! i
         if (Nspc.lt.2) then
            call error(label)
            write(*,*) 'character string is:"',trim(str),'"'
            write(*,*) 'Number of spaces found before cm-1:',Nspc
            write(*,*) 'should be at least equal to 2'
            stop
         endif
         nu_str=str(spc_idx(2)+1:spc_idx(1)-1)
         call str2dble(nu_str,nu,errcode)
         if (errcode.ne.0) then
            call error(label)
            write(*,*) 'Could not convert to double precision:'
            write(*,*) 'nu_str="',trim(nu_str),'"'
            stop
         endif                  !  errcode.ne.0
         
      endif                     ! possible_match
      
      return
      end



      subroutine next_spc(Nspc,spc_idx,i0,i1)
      implicit none
      include 'max.inc'
c     
c     Purpose: to return the last index in a continuous series
c     
c     Input:
c       + Npc: number od indexes
c       + spc_idx: indexes
c       + i0: a given index
c     
c     Output:
c       + i1: last index in the continuous series "i0" belongs to
c     
c     I/O
      integer Nspc
      integer spc_idx(0:Nspc_mx)
      integer i0
      integer i1
c     temp
      logical keep_searching
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine next_spc'

      keep_searching=.true.
      j=i0
      do while (keep_searching)
         if (j.eq.Nspc) then
            keep_searching=.false.
            i1=j
         else
            if (spc_idx(j+1).ne.spc_idx(j)+1) then
               keep_searching=.false.
               i1=j
            else
               j=j+1
            endif
         endif
      enddo                     ! while (keep_searching)
      
      return
      end
