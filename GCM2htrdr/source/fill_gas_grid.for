      subroutine fill_gas_grid(dim,homogeneous_gas_cells,Nnode,Ntheta,Nphi,Nz,Nband,Nq,mft,mfz,
     &     lambda_min,lambda_max,w,ka_gas,ks_gas,Tgas,
     &     gas_radiative_properties_file,gas_thermodynamic_properties_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpse: to generate the radiative and thermodynamic properties files for the gas mixture
c     
c     Input:
c       + dim: dimension of space
c       + homogeneous_gas_cells: when T, gas cells are homogeneous
c       + Nnode: number of nodes
c       + Ntheta: number of latitude intervals
c       + Nphi: number of longitude intervals
c       + Nz: number of altitude intervals
c       + Nband: number of spectral intervals
c       + Nq: quadrature order, for each spectral interval
c       + mft: multiplication factor for latitude intervals
c       + mfz: multiplication factor for altitude intervals
c       + lambda_min: lower wavelength of each interval [nm]
c       + lambda_max: higher wavelength of each interval [nm]
c       + w: gas quadrature weight
c       + ka_gas: gas absorption coefficient [m⁻¹]
c       + ks_gas: gas scattering coefficient [m⁻¹]
c       + Tgas: gas temperature [K]
c       + gas_radiative_properties_file: file that will record spectral data for the gas mixture
c       + gas_thermodynamic_properties_file: file that will record thermodynamic properties for the gas mixture
c     
c     Output: the required output files
c     
c     I/O
      integer dim
      logical homogeneous_gas_cells
      integer*8 Nnode
      integer Ntheta
      integer Nphi
      integer Nz
      integer Nband
      integer Nq(1:Nb_mx)
      integer mft
      integer mfz
      double precision lambda_min(1:Nb_mx)
      double precision lambda_max(1:Nb_mx)
      double precision w(1:Nb_mx,1:Nq_mx)
      double precision ka_gas(1:Nlat_mx,1:Nlay_mx,1:Nb_mx,1:Nq_mx)
      double precision ks_gas(1:Nlat_mx,1:Nlay_mx,1:Nb_mx)
      double precision Tgas(1:Nlat_mx,1:Nlay_mx)
      character*(Nchar_mx) gas_radiative_properties_file
      character*(Nchar_mx) gas_thermodynamic_properties_file
c     temp
      integer itheta,iphi,iz,inode,i,iband,iq,Ncellinbox,ilat,ilay,jz,jt,jt_min,jt_max
      double precision ka(1:Nnode_mx,1:Nq_mx)
      double precision ks(1:Nnode_mx)
      double precision T(1:Nnode_mx)
      double precision sum_w
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone,j
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     Debug
c      integer inode1,inode2
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine fill_gas_grid'

c     Debug
c      write(*,*) 'Ntheta=',Ntheta
c      stop
c     Debug
      
c     progress display ---
      ntot=int(Nband,kind(ntot))
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     --- progress display
      
      do iband=1,Nband
         sum_w=0.0D+0
         do iq=1,Nq(iband)
            sum_w=sum_w+w(iband,iq)
         enddo                  ! iq
         if (sum_w.ne.1.0D+0) then
            if (dabs(sum_w-1.0D+0).lt.1.0D-2) then
c     normalization
               do iq=1,Nq(iband)
                  w(iband,iq)=w(iband,iq)/sum_w
               enddo            ! iq
            else
c     error
               call error(label)
               write(*,*) 'band index:',iband
               write(*,*) 'Nq=',Nq(iband)
               do iq=1,Nq(iband)
                  write(*,*) 'w(',iq,')=',w(iband,iq)
               enddo            ! iq
               write(*,*) 'sum(w)=',sum_w
               write(*,*) 'Is too different from 1'
               stop
            endif
         endif                  ! sum_w != 0
      enddo                     ! iband
c     
      do iband=1,Nband
         inode=0
         if (homogeneous_gas_cells) then
c     use volumic grid produced by "volumic_grid_homogeneous_cells"
            do iz=1,Nz
               do jz=1,mfz
                  ilay=(iz-1)*mfz+jz
                  do itheta=1,Ntheta
                     do jt=1,mft
                        ilat=(itheta-1)*mft+jt
                        do iphi=1,Nphi
                           if (ilat.eq.1) then ! latitude interval in contact with the south pole
                              Ncellinbox=3
                           else if (ilat.eq.Ntheta) then ! latitude interval in contact with the north pole
                              Ncellinbox=3
                           else ! all other latitude intervalls
                              Ncellinbox=6
                           endif ! value of itheta
c     set identical properties over each node of each cell
                           do i=1,Nvincell*Ncellinbox
                              inode=inode+1         
                              do iq=1,Nq(iband)
                                 ka(inode,iq)=ka_gas(Ntheta-itheta+1,Nz-iz+1,iband,iq)
                              enddo ! iq
                              ks(inode)=ks_gas(Ntheta-itheta+1,Nz-iz+1,iband)
c     Debug
c                              if ((itheta.eq.1).and.(iphi.eq.1).and.(iband.eq.1).and.(i.eq.1)) then
c                                write(*,*) 'iz=',iz,' inode=',inode,' ka=',ka(inode,1),' ks=',ks(inode)
c                              endif
c     Debug
                              if (iband.eq.1) then
                                 T(inode)=Tgas(Ntheta-itheta+1,Nz-iz+1)
                              endif
                           enddo ! i
c     
                        enddo   ! iphi
                     enddo      ! jt
                  enddo         ! itheta
               enddo            ! jz
            enddo               ! iz
         else
c     use volumic grid produced by "volumic_grid"
c     itheta=1: cells that reunite at the south pole
            itheta=1
            do iphi=1,Nphi
               iz=1
               do i=1,3
                  inode=inode+1
                  do iq=1,Nq(iband)
                     ka(inode,iq)=ka_gas(Ntheta-itheta+1,Nz,iband,iq)
                  enddo         ! iq
                  ks(inode)=ks_gas(Ntheta-itheta+1,Nz,iband)
                  if (iband.eq.1) then
                     T(inode)=Tgas(Ntheta-itheta+1,Nz)
                  endif
               enddo            ! i
               do iz=1,Nz
                  do jz=1,mfz
                     do i=1,3
                        inode=inode+1
                        do iq=1,Nq(iband)
                           ka(inode,iq)=ka_gas(Ntheta-itheta+1,Nz-iz+1,iband,iq)
                        enddo   ! iq
                        ks(inode)=ks_gas(Ntheta-itheta+1,Nz-iz+1,iband)
                        if (iband.eq.1) then
                           T(inode)=Tgas(Ntheta-itheta+1,Nz-iz+1)
                        endif
                     enddo      ! i
                  enddo         ! jz
               enddo            ! iz
            enddo               ! iphi
c     itheta=2,Ntheta-1: general case
            do itheta=1,Ntheta
               if (itheta.eq.1) then
                  jt_min=2
                  jt_max=mft
               else if (itheta.eq.Ntheta) then
                  jt_min=1
                  jt_max=mft-1
               else
                  jt_min=1
                  jt_max=mft
               endif
c                  
               do jt=jt_min,jt_max
                  do iphi=1,Nphi
                     iz=1
                     do i=1,4
                        inode=inode+1
                        do iq=1,Nq(iband)
                           ka(inode,iq)=ka_gas(Ntheta-itheta+1,Nz,iband,iq)
                        enddo   ! iq
                        ks(inode)=ks_gas(Ntheta-itheta+1,Nz,iband)
                        if (iband.eq.1) then
                           T(inode)=Tgas(Ntheta-itheta+1,Nz)
                        endif
                     enddo      ! i
                     do iz=1,Nz
                        do jz=1,mfz
                           do i=1,4
                              inode=inode+1
                              do iq=1,Nq(iband)
                                 ka(inode,iq)=ka_gas(Ntheta-itheta+1,Nz-iz+1,iband,iq)
                              enddo ! iq
                              ks(inode)=ks_gas(Ntheta-itheta+1,Nz-iz+1,iband)
                              if (iband.eq.1) then
                                 T(inode)=Tgas(Ntheta-itheta+1,Nz-iz+1)
                              endif
                           enddo ! i
                        enddo   ! jz
                     enddo      ! iz
                  enddo         ! iphi
               enddo            ! jt
            enddo               ! itheta
c     itheta=Ntheta: cells that reunite at the north pole
            itheta=Ntheta
            do iphi=1,Nphi
               iz=1
               do i=1,3
                  inode=inode+1
                  do iq=1,Nq(iband)
                     ka(inode,iq)=ka_gas(Ntheta-itheta+1,Nz,iband,iq)
                  enddo         ! ;iq
                  ks(inode)=ks_gas(Ntheta-itheta+1,Nz,iband)
                  if (iband.eq.1) then
                     T(inode)=Tgas(Ntheta-itheta+1,Nz)
                  endif
               enddo            ! i
               do iz=1,Nz
                  do jz=1,mfz
                     do i=1,3
                        inode=inode+1
                        do iq=1,Nq(iband)
                           ka(inode,iq)=ka_gas(Ntheta-itheta+1,Nz-iz+1,iband,iq)
                        enddo   ! iq
                        ks(inode)=ks_gas(Ntheta-itheta+1,Nz-iz+1,iband)
                        if (iband.eq.1) then
                           T(inode)=Tgas(Ntheta-itheta+1,Nz-iz+1)
                        endif
                     enddo      ! i
                  enddo         ! jz
               enddo            ! iz
            enddo               ! iphi
         endif                  ! homogeneous_gas_cells
c
         if (inode.ne.Nnode) then
            call error(label)
            write(*,*) 'inode=',inode
            write(*,*) 'should be equal to:',Nnode
            stop
         endif
c         
c     progress display ---
         ndone=ndone+1
         fdone=dble(ndone)/dble(ntot)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif
c     --- progress display
c
c     Record volume properties
         if (iband.eq.1) then
            call record_gas_thermodynamic_properties_file(Nnode,T,gas_thermodynamic_properties_file)
         endif
         call record_gas_radiative_properties_file(Nnode,Nband,Nq,
     &        lambda_min,lambda_max,w,ka,ks,iband,gas_radiative_properties_file)
c     
      enddo                     ! iband
      write(*,*)
      write(*,*) 'Files have been recorded:'
      write(*,*) trim(gas_radiative_properties_file)
      write(*,*) trim(gas_thermodynamic_properties_file)

      return
      end
