c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine generate_mtl_for_group(dim,igroup,Ncube_in_group,cube_index,
     &     Npix,brdf,Nlambda,lambda,reflectivity,
     &     Nlambda_default,lambda_default,reflectivity_default,brdf_default)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to generate .dat material files for every pixel in a data cube
c     
c     Input:
c       + dim: dimension of space
c       + igroup: index of the group
c       + Ncube_in_group: number of files/cubes in each group
c       + cube_index: index of every cube in each group
c       + Npix: number of pixel in theta/phi directions
c       + brdf: BRDF for each pixel [0/1]
c       + Nlambda: number of wavelength
c       + lambda: values of the wavelength [nm]
c       + reflectivity: reflectivity for each pixel, for each wavelength
c       + Nlambda_default: number of wavelength for the default material
c       + lambda_default: values of the wavelength for the default material [nm]
c       + reflectivity_default: reflectivity for each pixel, for each wavelength, for the default material
c       + brdf_default: BRDF for the default material
c     
c     Output: a .dat material file for every pixel of the data cube
c     
c     I/O
      integer dim
      integer igroup
      integer Ncube_in_group(1:Ngroup_mx)
      integer cube_index(1:Ngroup_mx,1:Nfig_mx)
      integer Npix(1:Nfig_mx,1:2)
      double precision corner_theta(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf(1:Nfig_mx,1:Npix_mx,1:Npix_mx)
      integer Nlambda(1:Nfig_mx)
      double precision lambda(1:Nfig_mx,1:Nlambda_mx)
      double precision reflectivity(1:Nfig_mx,1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
      integer Nlambda_default
      double precision lambda_default(1:Nlambda_mx)
      double precision reflectivity_default(1:Nlambda_mx)
      double precision brdf_default
c     temp
      integer icube,Ncube,cube_idx,j,icorner,ilambda,ipix,jpix
      integer Npix_sc(1:2)
      double precision corner_theta_sc(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_phi_sc(1:Npix_mx,1:Npix_mx,1:4)
      double precision corner_z_sc(1:Npix_mx,1:Npix_mx,1:4)
      double precision brdf_sc(1:Npix_mx,1:Npix_mx)
      integer Nlambda_sc
      double precision lambda_sc(1:Nlambda_mx)
      double precision reflectivity_sc(1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_mtl_for_group'

      Ncube=Ncube_in_group(igroup)
      do icube=1,Ncube
         cube_idx=cube_index(igroup,icube)
         do j=1,2
           Npix_sc(j)= Npix(icube,j)
         enddo                  ! j
         Nlambda_sc=Nlambda(icube)
         do ilambda=1,Nlambda_sc
            lambda_sc(ilambda)=lambda(icube,ilambda)
         enddo                  ! ilambda
         do ipix=1,Npix_sc(1)
            do jpix=1,Npix_sc(2)
               do icorner=1,4
                  corner_theta_sc(ipix,jpix,icorner)=corner_theta(icube,ipix,jpix,icorner)
                  corner_phi_sc(ipix,jpix,icorner)=corner_phi(icube,ipix,jpix,icorner)
                  corner_z_sc(ipix,jpix,icorner)=corner_z(icube,ipix,jpix,icorner)
               enddo            ! icorner
               brdf_sc(ipix,jpix)=brdf(icube,ipix,jpix)
               do ilambda=1,Nlambda_sc
                  reflectivity_sc(ipix,jpix,ilambda)=reflectivity(icube,ipix,jpix,ilambda)
               enddo            ! ilambda
            enddo               ! jpix
         enddo                  ! ipix
         call generate_mtl_for_cube(dim,cube_idx,
     &        Npix_sc,brdf_sc,Nlambda_sc,lambda_sc,reflectivity_sc,
     &        Nlambda_default,lambda_default,reflectivity_default,brdf_default)
      enddo                     ! icube
         
      return
      end


      
      subroutine generate_mtl_for_cube(dim,cube_index,
     &     Npix,brdf,Nlambda,lambda,reflectivity,
     &     Nlambda_default,lambda_default,reflectivity_default,brdf_default)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to generate .dat material files for every pixel in a data cube
c     
c     Input:
c       + dim: dimension of space
c       + cube_index: index of the data cube
c       + Npix: number of pixel in theta/phi directions
c       + brdf: BRDF for each pixel [0/1]
c       + Nlambda: number of wavelength
c       + lambda: values of the wavelength [nm]
c       + reflectivity: reflectivity for each pixel, for each wavelength
c       + Nlambda_default: number of wavelength for the default material
c       + lambda_default: values of the wavelength for the default material [nm]
c       + reflectivity_default: reflectivity for each pixel, for each wavelength, for the default material
c       + brdf_default: BRDF for the default material
c     
c     Output: a .dat material file for every pixel of the data cube
c     
c     I/O
      integer dim
      integer cube_index
      integer Npix(1:2)
      double precision brdf(1:Npix_mx,1:Npix_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Npix_mx,1:Npix_mx,1:Nlambda_mx)
      integer Nlambda_default
      double precision lambda_default(1:Nlambda_mx)
      double precision reflectivity_default(1:Nlambda_mx)
      double precision brdf_default
c     temp
      integer ipix,jpix,ilambda
      character*(Nchar_mx) mat,name,mtl_file,brdf_type
      logical file_exists
      double precision ref(1:Nlambda_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_mtl_for_cube'
c
      write(*,*) 'Creating material files for cube:',cube_index
c
c     Default material
      call pixel_mtl_name(cube_index,0,0,mat,name)
      mtl_file='./results/'//trim(name)
      call record_material_definition_file(brdf_default,Nlambda_default,lambda_default,reflectivity_default,mtl_file)
c     Then one material per pixel
      do ipix=1,Npix(1)
         do jpix=1,Npix(2)
            call pixel_mtl_name(cube_index,ipix,jpix,mat,name)
            mtl_file='./results/'//trim(name)
            do ilambda=1,Nlambda
               ref(ilambda)=reflectivity(ipix,jpix,ilambda)
            enddo               ! ilambda
            call record_material_definition_file(brdf(ipix,jpix),Nlambda,lambda,ref,mtl_file)
         enddo                  ! jpix
      enddo                     ! ipix
      
      return
      end



      subroutine record_material_definition_file(brdf,Nlambda,lambda,reflectivity,mtl_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record a material definition file
c     
c     Input:
c       + brdf: type of BRDF (0: lambertian, 1: specular)
c       + Nlambda: number of wavelength in the definition of the reflectivity signal
c       + lambda: values of the wavelength [nm]
c       + reflectivity: associated values of the reflectivity
c       + mtl_file: ascii material definition file
c     
c     Output: the required ascii file
c     
c     I/O
      double precision brdf
      integer Nlambda
      double precision lambda(1:Nlambda_mx)
      double precision reflectivity(1:Nlambda_mx)
      character*(Nchar_mx) mtl_file
c     temp
      logical file_exists
      character*(Nchar_mx) brdf_type
      integer ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine record_material_definition_file'

      inquire(file=trim(mtl_file),exist=file_exists)
      if (.not.file_exists) then
         if (brdf.eq.0.0D+0) then
            brdf_type='lambertian'
         else if (brdf.eq.1.0D+0) then
            brdf_type='specular'
         else
            call error(label)
            write(*,*) 'brdf=',brdf
            write(*,*) 'is not supported yet'
            stop
         endif
         open(14,file=trim(mtl_file))
         write(14,23) 'wavelengths',Nlambda
         do ilambda=1,Nlambda
            write(14,*) lambda(ilambda), ! nm
     &           trim(brdf_type),
     &           reflectivity(ilambda)
         enddo                  ! ilambda
         close(14)
      endif                     ! file_exists

      return
      end
      

      
      subroutine pixel_mtl_name(cube_index,ipix,jpix,mat,filename)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a file name for the material in pixel (ipix,jpix)
c     
c     Input:
c       + cube_index: index of the data cube
c       + ipix, jpix: indexes of the pixel
c     
c     Output:
c       + mat: material
c       + filename: file name
c     
c     I/O
      integer cube_index
      integer ipix,jpix
      character*(Nchar_mx) mat
      character*(Nchar_mx) filename
c     temp
      character*(Nchar_mx) cube_index_str,ipix_str,jpix_str
      logical err_code
c     label
      character*(Nchar_mx) label
      label='subroutine pixel_mtl_name'
c
      if (cube_index.lt.10000) then
         call num2str4(cube_index,cube_index_str,err_code)
      else
         call error(label)
         write(*,*) 'cube_index=',cube_index
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'cube_index=',cube_index
         stop
      endif
c     
      if (ipix.lt.10000) then
         call num2str4(ipix,ipix_str,err_code)
      else
         call error(label)
         write(*,*) 'ipix=',ipix
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'ipix=',ipix
         stop
      endif
c     
      if (jpix.lt.10000) then
         call num2str4(jpix,jpix_str,err_code)
      else
         call error(label)
         write(*,*) 'jpix=',jpix
         write(*,*) 'maximum possible value is 10000'
         stop
      endif
      if (err_code) then
         call error(label)
         write(*,*) 'could not convert to string:'
         write(*,*) 'jpix=',jpix
         stop
      endif
c
      mat='mtl_'//trim(cube_index_str)
     &     //'_'//trim(ipix_str)
     &     //'_'//trim(jpix_str)
      filename=trim(mat)//'.dat'
c      
      return
      end
