      subroutine read_data(in_file,data_dir,Nphi,mft,mfz,record_tom_file,
     &     produce_surface_properties,planet_radius,mtlidx_file,grid_file_surface,surface_properties_file,
     &     produce_gas_properties,gas_radius,gas_grid_file,gas_radiative_properties_file,gas_thermodynamic_properties_file,homogeneous_gas_cells,
     &     produce_cloud_properties,cloud_phase_function_list_file,cloud_radius,cloud_grid_file,cloud_radiative_properties_file,cloud_phase_function_file,homogeneous_cloud_cells,
     &     produce_haze_properties,haze_phase_function_list_file,haze_radius,haze_grid_file,haze_radiative_properties_file,haze_phase_function_file,homogeneous_haze_cells)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read the input data file
c     
c     Input:
c       + in_file: file to read
c     
c     Output:
c       + data_dir: base directory for finding input data files
c       + Nphi: number of longitude intervals
c       + mft: multiplication factor for latitude intervals
c       + mfz: multiplication factor for altitude intervals
c       + record_tom_file: T if TOM input data file has to be generated
c       + produce_surface_properties: T if surface properties have to be generated
c       + planet_radius: radius of the spherical(ish) body for surface properties [m]
c       + mtlidx_file: file that will record the index of each declared material
c       + grid_file_surface: file that will record geometric data of the surface
c       + surface_properties_file: file that will record material and temperature information for the surface
c       + produce_gas_properties: T if gas mixture properties have to be generated
c       + gas_radius: radius of the spherical body for the gas mixture grid [m]
c       + gas_grid_file: file that will record the spatial grid for the gas mixture
c       + gas_radiative_properties_file: file that will record spectral data for the gas mixture
c       + gas_thermodynamic_properties_file: file that will record thermodynamic properties for the gas mixture
c       + homogeneous_gas_cells: when T, gas cells are homogeneous
c       + produce_cloud_properties: T if cloud properties have to be generated
c       + cloud_phase_function_list_file: file that holds the list of phase function definition files for the cloud
c       + cloud_radius: radius of the spherical body for the cloud grid [m]
c       + cloud_grid_file: file that will record the spatial grid for the clouds
c       + cloud_radiative_properties_file: file that will record the spectral properties of the clouds
c       + cloud_phase_function_file: file that will record the phase function of the clouds
c       + homogeneous_cloud_cells: when T, cloud cells are homogeneous
c       + produce_haze_properties: T if haze properties have to be generated
c       + haze_phase_function_list_file: file that holds the list of phase function definition files for the haze
c       + haze_radius: radius of the spherical body for the haze grid [m]
c       + haze_grid_file: file that will record the spatial grid for the hazes
c       + haze_radiative_properties_file: file that will record the spectral properties of the hazes
c       + haze_phase_function_file: file that will record the phase function of the hazes
c       + homogeneous_haze_cells: when T, haze cells are homogeneous
c     
c     I/O
      character*(Nchar_mx) in_file
      character*(Nchar_mx) data_dir
      integer Nphi
      integer mft
      integer mfz
      logical record_tom_file
      logical produce_surface_properties
      double precision planet_radius
      character*(Nchar_mx) mtlidx_file
      character*(Nchar_mx) grid_file_surface
      character*(Nchar_mx) surface_properties_file
      logical produce_gas_properties
      double precision gas_radius
      character*(Nchar_mx) gas_grid_file
      character*(Nchar_mx) gas_radiative_properties_file
      character*(Nchar_mx) gas_thermodynamic_properties_file
      logical homogeneous_gas_cells
      logical produce_cloud_properties
      character*(Nchar_mx) cloud_phase_function_list_file
      double precision cloud_radius
      character*(Nchar_mx) cloud_grid_file
      character*(Nchar_mx) cloud_radiative_properties_file
      character*(Nchar_mx) cloud_phase_function_file
      logical homogeneous_cloud_cells
      logical produce_haze_properties
      character*(Nchar_mx) haze_phase_function_list_file
      double precision haze_radius
      character*(Nchar_mx) haze_grid_file
      character*(Nchar_mx) haze_radiative_properties_file
      character*(Nchar_mx) haze_phase_function_file
      logical homogeneous_haze_cells
c     temp
      double precision radius
      logical data_dir_exists
      integer ios,j
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(11,file=trim(in_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(in_file)
         stop
      endif
      do j=1,8
         read(11,*)
      enddo
      read(11,*) radius  ! km
      read(11,*)
      read(11,10) data_dir
      read(11,*)
      read(11,*) Nphi
      read(11,*)
      read(11,*) mft
      read(11,*)
      read(11,*) mfz
      read(11,*)
      read(11,*) record_tom_file
      do j=1,5
         read(11,*)
      enddo
      read(11,*) produce_surface_properties
      read(11,*)
      read(11,10) mtlidx_file
      read(11,*)
      read(11,10) grid_file_surface
      read(11,*)
      read(11,10) surface_properties_file
      do j=1,5
         read(11,*)
      enddo
      read(11,*) produce_gas_properties
      read(11,*)
      read(11,10) gas_grid_file
      read(11,*)
      read(11,10) gas_radiative_properties_file
      read(11,*)
      read(11,10) gas_thermodynamic_properties_file
      read(11,*)
      read(11,*) homogeneous_gas_cells
      do j=1,5
         read(11,*)
      enddo
      read(11,*) produce_cloud_properties
      read(11,*)
      read(11,10) cloud_phase_function_list_file
      read(11,*)
      read(11,10) cloud_grid_file
      read(11,*)
      read(11,10) cloud_radiative_properties_file
      read(11,*)
      read(11,10) cloud_phase_function_file
      read(11,*)
      read(11,*) homogeneous_cloud_cells
      read(11,*)
      read(11,*) produce_haze_properties
      read(11,*)
      read(11,10) haze_phase_function_list_file
      read(11,*)
      read(11,10) haze_grid_file
      read(11,*)
      read(11,10) haze_radiative_properties_file
      read(11,*)
      read(11,10) haze_phase_function_file
      read(11,*)
      read(11,*) homogeneous_haze_cells
      close(11)
c     Check
      call add_slash_if_absent(data_dir)
      inquire(file=trim(data_dir),exist=data_dir_exists)
      if (.not.data_dir_exists) then
         call error(label)
         write(*,*) 'Base directory not found: ',trim(data_dir)
         stop
      endif
      if (radius.le.0.0D+0) then
         call error(label)
         write(*,*) 'radius=',radius
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nphi.le.0) then
         call error(label)
         write(*,*) 'Nphi=',Nphi
         write(*,*) 'should be > 0'
         stop
      endif
      if (mft.le.0) then
         call error(label)
         write(*,*) 'mft=',mft
         write(*,*) 'should be > 0'
         stop
      endif
      if (mfz.le.0) then
         call error(label)
         write(*,*) 'mfz=',mfz
         write(*,*) 'should be > 0'
         stop
      endif
c     Conversions
      radius=radius*1.0D+3      ! km -> m
      if (produce_surface_properties) then
         planet_radius=radius
      endif                     ! produce_surface_properties
      if (produce_gas_properties) then
         gas_radius=radius ! m
      endif                     ! produce_gas_properties
      if (produce_cloud_properties) then
         cloud_radius=radius ! m
      endif                     ! produce_cloud_properties
      if (produce_haze_properties) then
         haze_radius=radius ! m
      endif                     ! produce_haze_properties

      return
      end
