#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   68.9 S (FP3)
#Longitude             :   62.3 W (FP3)
#Observation date      :  2015-09-29 (T113 flyby)
#Solar longitude       :   71.7�
#Local time            :  13:47
#Solar zenith angle    :   98�
#Vertical resolution   :  34 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
460.59       1.09558e-03    6.42234e-06    5.15212e-06    7.69255e-06    1      169.82
450.67       1.32548e-03    5.61257e-06    4.52101e-06    6.70413e-06    1      170.80
440.77       1.60417e-03    5.21600e-06    4.21954e-06    6.21247e-06    1      171.78
430.89       1.94120e-03    5.11303e-06    4.15481e-06    6.07126e-06    1      172.72
421.01       2.34880e-03    5.15155e-06    4.20141e-06    6.10169e-06    1      173.57
411.14       2.84256e-03    5.13566e-06    4.21391e-06    6.05741e-06    1      174.34
401.29       3.43940e-03    4.91731e-06    4.05110e-06    5.78353e-06    1      175.08
391.48       4.16156e-03    4.42241e-06    3.65566e-06    5.18916e-06    1      175.83
381.68       5.03563e-03    3.75916e-06    3.11888e-06    4.39943e-06    1      176.51
371.92       6.09327e-03    3.08929e-06    2.56663e-06    3.61195e-06    1      176.96
362.19       7.37301e-03    2.54917e-06    2.12454e-06    2.97380e-06    1      176.98
352.53       8.92141e-03    2.18461e-06    1.82600e-06    2.54321e-06    1      176.47
342.96       1.07932e-02    1.98254e-06    1.65907e-06    2.30601e-06    1      175.40
333.51       1.30603e-02    1.89652e-06    1.59234e-06    2.20069e-06    1      173.86
324.20       1.58080e-02    1.86853e-06    1.57282e-06    2.16424e-06    1      172.04
315.04       1.91281e-02    1.84088e-06    1.55422e-06    2.12753e-06    1      170.23
306.04       2.31447e-02    1.75974e-06    1.48911e-06    2.03036e-06    1      168.72
297.17       2.80030e-02    1.61061e-06    1.36684e-06    1.85439e-06    1      167.59
288.40       3.38809e-02    1.42760e-06    1.21407e-06    1.64113e-06    1      166.74
279.74       4.09985e-02    1.25946e-06    1.07340e-06    1.44553e-06    1      165.80
271.18       4.96095e-02    1.14263e-06    9.76374e-07    1.30889e-06    1      164.97
262.72       6.00271e-02    1.08491e-06    9.29019e-07    1.24080e-06    1      164.04
254.36       7.26347e-02    1.07297e-06    9.20855e-07    1.22508e-06    1      162.66
246.12       8.78905e-02    1.07761e-06    9.26529e-07    1.22869e-06    1      160.60
238.04       1.06356e-01    1.04838e-06    9.01561e-07    1.19520e-06    1      157.43
230.16       1.28714e-01    9.54418e-07    8.19079e-07    1.08976e-06    1      153.04
222.55       1.55744e-01    7.97703e-07    6.79950e-07    9.15455e-07    1      147.36
215.26       1.88442e-01    6.16641e-07    5.22524e-07    7.10759e-07    1      140.58
208.33       2.28015e-01    4.63626e-07    3.89463e-07    5.37790e-07    1      133.12
201.79       2.75895e-01    3.62156e-07    3.01579e-07    4.22733e-07    1      125.61
195.64       3.33834e-01    3.14848e-07    2.60473e-07    3.69224e-07    1      118.70
189.85       4.03915e-01    3.18582e-07    2.63012e-07    3.74152e-07    1      113.01
184.35       4.88729e-01    3.82655e-07    3.15785e-07    4.49525e-07    1      109.10
