#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   41.6 N (FP3)
#Longitude             :  202.5 W (FP3)
#Observation date      :  2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  21:30
#Solar zenith angle    :  106�
#Vertical resolution   :  48 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
520.21       1.03861e-03    3.06504e-06    2.47489e-06    3.65518e-06    1      161.60
509.65       1.27529e-03    2.27592e-06    1.83924e-06    2.71260e-06    1      162.61
499.10       1.56574e-03    1.76881e-06    1.43212e-06    2.10551e-06    1      163.91
488.53       1.92238e-03    1.45251e-06    1.17990e-06    1.72512e-06    1      165.47
477.94       2.36007e-03    1.28273e-06    1.04818e-06    1.51728e-06    1      167.21
467.32       2.89772e-03    1.21920e-06    1.00354e-06    1.43486e-06    1      168.99
456.65       3.55777e-03    1.24647e-06    1.03509e-06    1.45786e-06    1      170.63
445.96       4.36798e-03    1.35120e-06    1.13284e-06    1.56956e-06    1      171.95
435.27       5.36322e-03    1.52347e-06    1.29012e-06    1.75683e-06    1      172.89
424.60       6.58529e-03    1.74672e-06    1.49387e-06    1.99957e-06    1      173.78
413.93       8.08541e-03    1.98969e-06    1.71494e-06    2.26444e-06    1      174.96
403.27       9.92718e-03    2.20230e-06    1.91122e-06    2.49337e-06    1      176.32
392.61       1.21906e-02    2.33029e-06    2.03208e-06    2.62851e-06    1      177.43
381.97       1.49665e-02    2.33472e-06    2.04073e-06    2.62871e-06    1      178.06
371.36       1.83730e-02    2.22081e-06    1.94263e-06    2.49898e-06    1      178.23
360.82       2.25610e-02    2.01222e-06    1.75985e-06    2.26459e-06    1      178.18
350.35       2.76993e-02    1.76310e-06    1.54205e-06    1.98415e-06    1      178.16
339.95       3.40104e-02    1.51821e-06    1.32914e-06    1.70729e-06    1      178.31
329.62       4.17602e-02    1.31480e-06    1.15266e-06    1.47694e-06    1      178.44
319.36       5.12699e-02    1.16349e-06    1.02102e-06    1.30596e-06    1      178.86
309.14       6.29484e-02    1.06686e-06    9.36206e-07    1.19752e-06    1      179.29
298.98       7.72877e-02    1.01876e-06    8.93704e-07    1.14381e-06    1      179.38
288.89       9.49175e-02    1.00358e-06    8.80752e-07    1.12641e-06    1      178.97
278.89       1.16539e-01    1.00534e-06    8.83872e-07    1.12681e-06    1      178.02
269.00       1.43047e-01    1.00678e-06    8.86763e-07    1.12679e-06    1      176.73
259.26       1.75625e-01    9.88190e-07    8.70758e-07    1.10562e-06    1      175.34
249.65       2.15660e-01    9.35886e-07    8.23898e-07    1.04787e-06    1      174.02
240.18       2.64807e-01    8.54337e-07    7.50967e-07    9.57707e-07    1      172.78
230.85       3.25134e-01    7.54580e-07    6.63248e-07    8.45912e-07    1      171.40
221.65       3.99200e-01    6.55604e-07    5.76953e-07    7.34254e-07    1      169.68
212.60       4.90115e-01    5.67790e-07    5.00551e-07    6.35029e-07    1      167.65
203.71       6.01780e-01    5.00188e-07    4.40899e-07    5.59477e-07    1      165.81
194.98       7.38857e-01    4.51024e-07    3.96627e-07    5.05422e-07    1      163.99
186.40       9.07080e-01    4.16467e-07    3.65295e-07    4.67640e-07    1      162.11
177.97       1.11363e+00    3.88734e-07    3.40723e-07    4.36745e-07    1      160.25
169.69       1.36730e+00    3.63428e-07    3.19227e-07    4.07629e-07    1      158.11
161.57       1.67911e+00    3.33850e-07    2.94197e-07    3.73502e-07    1      155.76
153.61       2.06213e+00    2.97959e-07    2.62826e-07    3.33091e-07    1      153.64
145.80       2.53168e+00    2.57972e-07    2.26825e-07    2.89118e-07    1      151.46
138.16       3.10812e+00    2.15753e-07    1.88588e-07    2.42918e-07    1      148.87
130.68       3.81637e+00    1.76263e-07    1.53285e-07    1.99241e-07    1      146.09
123.38       4.68583e+00    1.42774e-07    1.24017e-07    1.61531e-07    1      143.21
