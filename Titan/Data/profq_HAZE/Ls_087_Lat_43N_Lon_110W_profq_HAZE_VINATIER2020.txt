#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 microns, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   42.9 N (FP3)
#Longitude             :  109.6 W (FP3)
#Observation date      :  2017-02-17 (S98/262TI flyby)
#Solar longitude       :   87.1�
#Emission angle        :   25.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
190.17       8.65915e-01    4.41141e-07    3.41953e-07    5.40330e-07    1      160.84
182.36       1.04797e+00    4.31927e-07    3.37778e-07    5.26076e-07    1      159.33
174.68       1.26824e+00    4.26672e-07    3.36330e-07    5.17014e-07    1      158.34
167.10       1.53452e+00    4.19517e-07    3.32775e-07    5.06258e-07    1      157.70
159.59       1.85658e+00    4.06315e-07    3.23656e-07    4.88975e-07    1      157.06
152.15       2.24628e+00    3.82011e-07    3.04612e-07    4.59410e-07    1      156.03
144.80       2.71814e+00    3.49124e-07    2.77718e-07    4.20530e-07    1      154.11
137.59       3.28905e+00    3.10449e-07    2.45448e-07    3.75450e-07    1      151.59
130.54       3.97945e+00    2.69915e-07    2.11464e-07    3.28366e-07    1      148.32
123.66       4.81508e+00    2.31579e-07    1.79390e-07    2.83769e-07    1      144.42
