#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   74.4 N (FP3)
#Longitude             :  218.3 W (FP3)
#Observation date      :  2016-06-08 (T120 flyby)
#Solar longitude       :   79.4�
#Emission angle        :   38.8�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
270.31       1.28714e-01    2.48331e-06    1.73211e-06    3.23451e-06    1      184.98
260.84       1.55744e-01    2.39123e-06    1.71512e-06    3.06734e-06    1      183.25
251.52       1.88442e-01    1.99506e-06    1.46287e-06    2.52724e-06    1      180.79
242.38       2.28015e-01    1.54735e-06    1.15122e-06    1.94347e-06    1      177.81
233.43       2.75895e-01    1.21273e-06    9.05947e-07    1.51951e-06    1      174.70
224.71       3.33834e-01    1.01344e-06    7.52526e-07    1.27436e-06    1      171.72
216.18       4.03915e-01    9.21416e-07    6.73676e-07    1.16916e-06    1      169.10
207.83       4.88729e-01    8.78433e-07    6.29196e-07    1.12767e-06    1      166.96
199.64       5.91407e-01    8.28966e-07    5.78462e-07    1.07947e-06    1      164.34
191.62       7.15648e-01    7.22796e-07    4.90845e-07    9.54748e-07    1      162.13
183.76       8.65915e-01    5.58738e-07    3.69449e-07    7.48027e-07    1      159.76
176.05       1.04797e+00    3.80266e-07    2.45444e-07    5.15089e-07    1      157.31
