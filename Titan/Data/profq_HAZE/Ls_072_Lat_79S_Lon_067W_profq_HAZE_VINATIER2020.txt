#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   79.0 S (FP3)
#Longitude             :   66.9 W (FP3)
#Observation date      :  2015-09-29 (T113 flyby)
#Solar longitude       :   71.7�
#Local time            :  13:27
#Solar zenith angle    :  107�
#Vertical resolution   :  30 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
558.46       1.07524e-04    3.37356e-05    2.55929e-05    4.18782e-05    1      165.06
547.81       1.31057e-04    2.99736e-05    2.28291e-05    3.71180e-05    1      166.40
537.15       1.59766e-04    2.71381e-05    2.07815e-05    3.34948e-05    1      167.74
526.47       1.94746e-04    2.50595e-05    1.92820e-05    3.08371e-05    1      169.10
515.78       2.37339e-04    2.34985e-05    1.81982e-05    2.87988e-05    1      170.51
505.07       2.89280e-04    2.22520e-05    1.73567e-05    2.71473e-05    1      172.02
494.34       3.52622e-04    2.10983e-05    1.65857e-05    2.56109e-05    1      173.60
483.59       4.29794e-04    1.98346e-05    1.57156e-05    2.39536e-05    1      175.27
472.82       5.23881e-04    1.82949e-05    1.46002e-05    2.19896e-05    1      176.96
462.02       6.38570e-04    1.64848e-05    1.32358e-05    1.97337e-05    1      178.59
451.20       7.78336e-04    1.44598e-05    1.16682e-05    1.72515e-05    1      180.06
440.36       9.48518e-04    1.23779e-05    1.00607e-05    1.46950e-05    1      181.23
429.54       1.15630e-03    1.04755e-05    8.54180e-06    1.24093e-05    1      181.95
418.75       1.40961e-03    8.83239e-06    7.23459e-06    1.04302e-05    1      182.16
408.03       1.71806e-03    7.53915e-06    6.18971e-06    8.88859e-06    1      181.84
397.40       2.09425e-03    6.56824e-06    5.40951e-06    7.72698e-06    1      180.99
386.90       2.55249e-03    5.89149e-06    4.86929e-06    6.91369e-06    1      179.67
376.54       3.11124e-03    5.45368e-06    4.52716e-06    6.38019e-06    1      178.05
366.35       3.79241e-03    5.18717e-06    4.31866e-06    6.05568e-06    1      176.34
356.32       4.62237e-03    5.00748e-06    4.18756e-06    5.82741e-06    1      174.67
346.46       5.63389e-03    4.83107e-06    4.05486e-06    5.60727e-06    1      173.06
336.74       6.86686e-03    4.58685e-06    3.85639e-06    5.31731e-06    1      171.48
327.18       8.36995e-03    4.20822e-06    3.54409e-06    4.87235e-06    1      169.93
317.77       1.02007e-02    3.70813e-06    3.12561e-06    4.29064e-06    1      168.54
308.49       1.24338e-02    3.13221e-06    2.64082e-06    3.62361e-06    1      167.49
299.34       1.51560e-02    2.56796e-06    2.16730e-06    2.96863e-06    1      166.99
290.26       1.84741e-02    2.08768e-06    1.76453e-06    2.41083e-06    1      167.22
281.22       2.25199e-02    1.73009e-06    1.46583e-06    1.99436e-06    1      168.23
272.18       2.74455e-02    1.49940e-06    1.27346e-06    1.72533e-06    1      169.70
263.15       3.34557e-02    1.37064e-06    1.16797e-06    1.57332e-06    1      170.98
254.10       4.07804e-02    1.32033e-06    1.12816e-06    1.51250e-06    1      171.14
245.11       4.97016e-02    1.30851e-06    1.12019e-06    1.49684e-06    1      169.86
236.26       6.05781e-02    1.29031e-06    1.10451e-06    1.47611e-06    1      166.71
227.62       7.38380e-02    1.21374e-06    1.03483e-06    1.39264e-06    1      161.41
219.31       9.00036e-02    1.05739e-06    8.93130e-07    1.22166e-06    1      154.10
211.42       1.09698e-01    8.38411e-07    6.98108e-07    9.78714e-07    1      145.09
204.02       1.33695e-01    6.09319e-07    4.98352e-07    7.20285e-07    1      135.24
197.14       1.62952e-01    4.15825e-07    3.33512e-07    4.98138e-07    1      125.26
