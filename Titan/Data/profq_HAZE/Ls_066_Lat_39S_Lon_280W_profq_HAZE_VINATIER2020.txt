#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   39.0 S (FP3)
#Longitude             :  280.2 W (FP3)
#Observation date      :  2015-03-17 (T110 flyby)
#Solar longitude       :   65.8�
#Local time            :  16:19
#Solar zenith angle    :   89�
#Vertical resolution   :  31 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
402.86       8.36995e-03    2.54188e-07    1.33941e-07    3.74435e-07    1      168.08
393.07       1.02007e-02    2.92157e-07    1.61906e-07    4.22408e-07    1      168.53
383.31       1.24338e-02    3.24623e-07    1.90058e-07    4.59189e-07    1      168.77
373.60       1.51560e-02    3.48048e-07    2.15411e-07    4.80685e-07    1      168.90
363.95       1.84741e-02    3.56662e-07    2.33327e-07    4.79997e-07    1      169.07
354.34       2.25199e-02    3.53446e-07    2.43860e-07    4.63032e-07    1      169.46
344.78       2.74455e-02    3.42024e-07    2.47461e-07    4.36587e-07    1      170.07
335.25       3.34557e-02    3.26946e-07    2.46108e-07    4.07784e-07    1      170.71
325.75       4.07804e-02    3.15448e-07    2.44964e-07    3.85932e-07    1      170.90
316.29       4.97016e-02    3.10812e-07    2.47146e-07    3.74478e-07    1      171.18
306.89       6.05781e-02    3.16062e-07    2.55840e-07    3.76284e-07    1      171.56
297.52       7.38380e-02    3.33399e-07    2.74754e-07    3.92043e-07    1      172.01
288.18       9.00036e-02    3.63633e-07    3.05307e-07    4.21958e-07    1      172.62
278.88       1.09698e-01    4.07159e-07    3.46916e-07    4.67401e-07    1      173.21
269.61       1.33695e-01    4.58876e-07    3.95017e-07    5.22735e-07    1      173.66
260.38       1.62952e-01    5.08044e-07    4.39968e-07    5.76120e-07    1      173.73
251.20       1.98625e-01    5.40464e-07    4.70188e-07    6.10740e-07    1      173.24
242.11       2.42113e-01    5.42278e-07    4.73755e-07    6.10802e-07    1      172.18
233.12       2.95149e-01    5.12404e-07    4.48471e-07    5.76338e-07    1      170.66
224.28       3.59788e-01    4.58967e-07    4.01569e-07    5.16364e-07    1      168.86
215.58       4.38503e-01    4.02840e-07    3.52632e-07    4.53048e-07    1      167.03
207.02       5.34479e-01    3.57414e-07    3.13229e-07    4.01600e-07    1      165.38
198.60       6.51460e-01    3.32444e-07    2.91800e-07    3.73089e-07    1      164.01
190.30       7.94055e-01    3.29606e-07    2.88982e-07    3.70231e-07    1      162.80
182.12       9.68087e-01    3.46169e-07    3.02786e-07    3.89552e-07    1      161.20
174.07       1.17976e+00    3.75940e-07    3.29084e-07    4.22795e-07    1      159.11
166.16       1.43791e+00    4.01516e-07    3.53036e-07    4.49996e-07    1      156.89
158.40       1.75293e+00    4.06927e-07    3.58740e-07    4.55113e-07    1      154.47
