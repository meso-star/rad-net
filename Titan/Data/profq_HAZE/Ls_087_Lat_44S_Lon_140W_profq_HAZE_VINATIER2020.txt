#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 microns, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   43.8 S (FP3)
#Longitude             :  139.6 W (FP3)
#Observation date      :  2017-02-01 (S97/259TI flyby)
#Solar longitude       :   86.6�
#Emission angle        :   14.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
283.36       8.78905e-02    6.02310e-07    3.61859e-07    8.42760e-07    1      177.42
274.19       1.06356e-01    6.02700e-07    3.64635e-07    8.40765e-07    1      176.27
265.14       1.28714e-01    5.92177e-07    3.61096e-07    8.23259e-07    1      174.89
256.21       1.55744e-01    5.71205e-07    3.51076e-07    7.91333e-07    1      173.12
247.43       1.88442e-01    5.42850e-07    3.36570e-07    7.49131e-07    1      170.94
238.80       2.28015e-01    5.11097e-07    3.19909e-07    7.02286e-07    1      168.39
230.36       2.75895e-01    4.76929e-07    3.01547e-07    6.52311e-07    1      165.65
222.11       3.33834e-01    4.44603e-07    2.83491e-07    6.05716e-07    1      162.74
214.04       4.03915e-01    4.13948e-07    2.67706e-07    5.60190e-07    1      159.81
206.17       4.88729e-01    3.87468e-07    2.53519e-07    5.21417e-07    1      156.92
198.47       5.91407e-01    3.63806e-07    2.40980e-07    4.86632e-07    1      154.58
190.93       7.15648e-01    3.44508e-07    2.31681e-07    4.57335e-07    1      152.46
183.54       8.65915e-01    3.27755e-07    2.23301e-07    4.32209e-07    1      150.42
