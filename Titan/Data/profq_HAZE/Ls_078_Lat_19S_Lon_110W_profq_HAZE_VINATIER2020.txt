#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   18.7 S (FP3)
#Longitude             :  109.6 W (FP3)
#Observation date      :  2016-05-07 (T119 flyby)
#Solar longitude       :   78.4�
#Local time            :  06:21
#Solar zenith angle    :   95�
#Vertical resolution   :  39 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
507.11       1.09558e-03    1.49430e-06    6.14852e-07    2.37375e-06    1      159.89
497.49       1.32548e-03    1.15695e-06    4.57602e-07    1.85631e-06    1      160.06
487.92       1.60417e-03    8.73790e-07    3.36010e-07    1.41157e-06    1      160.18
478.40       1.94120e-03    6.62663e-07    2.51704e-07    1.07362e-06    1      160.29
468.95       2.34880e-03    5.25745e-07    2.02043e-07    8.49447e-07    1      160.33
459.54       2.84256e-03    4.53480e-07    1.80184e-07    7.26776e-07    1      160.32
450.18       3.43940e-03    4.37591e-07    1.84034e-07    6.91148e-07    1      160.52
440.88       4.16156e-03    4.74289e-07    2.15677e-07    7.32902e-07    1      160.97
431.60       5.03563e-03    5.64929e-07    2.81596e-07    8.48262e-07    1      161.88
422.33       6.09327e-03    7.06752e-07    3.90165e-07    1.02334e-06    1      163.00
413.05       7.37301e-03    8.76303e-07    5.37019e-07    1.21559e-06    1      164.24
403.77       8.92141e-03    1.01870e-06    6.87910e-07    1.34950e-06    1      165.40
394.47       1.07932e-02    1.07013e-06    7.81009e-07    1.35925e-06    1      166.49
385.18       1.30603e-02    1.00738e-06    7.69267e-07    1.24548e-06    1      167.32
375.90       1.58080e-02    8.70091e-07    6.76152e-07    1.06403e-06    1      168.02
366.64       1.91281e-02    7.24780e-07    5.65733e-07    8.83828e-07    1      168.63
357.40       2.31447e-02    6.13405e-07    4.83779e-07    7.43030e-07    1      169.34
348.17       2.80030e-02    5.52659e-07    4.44119e-07    6.61198e-07    1      170.08
338.98       3.38809e-02    5.39884e-07    4.42892e-07    6.36875e-07    1      170.74
329.80       4.09985e-02    5.66835e-07    4.70248e-07    6.63422e-07    1      170.81
320.67       4.96095e-02    6.20821e-07    5.15840e-07    7.25803e-07    1      170.95
311.60       6.00271e-02    6.80176e-07    5.66450e-07    7.93902e-07    1      171.14
302.57       7.26347e-02    7.21008e-07    6.08418e-07    8.33598e-07    1      171.35
293.58       8.78905e-02    7.30313e-07    6.28992e-07    8.31635e-07    1      171.78
284.62       1.06356e-01    7.12441e-07    6.21223e-07    8.03659e-07    1      172.22
275.71       1.28714e-01    6.82709e-07    5.92044e-07    7.73373e-07    1      172.71
266.82       1.55744e-01    6.63298e-07    5.68995e-07    7.57600e-07    1      173.07
257.98       1.88442e-01    6.59802e-07    5.66227e-07    7.53378e-07    1      173.16
249.17       2.28015e-01    6.75644e-07    5.87565e-07    7.63723e-07    1      172.93
240.45       2.75895e-01    6.99823e-07    6.15971e-07    7.83675e-07    1      172.42
231.80       3.33834e-01    7.12379e-07    6.23215e-07    8.01544e-07    1      171.61
223.24       4.03915e-01    6.96869e-07    6.00380e-07    7.93359e-07    1      170.54
214.78       4.88729e-01    6.45043e-07    5.53407e-07    7.36680e-07    1      169.27
206.43       5.91407e-01    5.66572e-07    4.92250e-07    6.40894e-07    1      168.28
198.18       7.15648e-01    4.83586e-07    4.26111e-07    5.41060e-07    1      167.19
190.05       8.65915e-01    4.12182e-07    3.59047e-07    4.65317e-07    1      165.64
182.03       1.04797e+00    3.62968e-07    3.06497e-07    4.19440e-07    1      164.12
174.13       1.26824e+00    3.38204e-07    2.79624e-07    3.96785e-07    1      162.49
166.35       1.53452e+00    3.35652e-07    2.79434e-07    3.91871e-07    1      160.72
158.70       1.85658e+00    3.50752e-07    3.01793e-07    3.99711e-07    1      158.71
151.19       2.24628e+00    3.72688e-07    3.28641e-07    4.16736e-07    1      156.52
143.82       2.71814e+00    3.85610e-07    3.28956e-07    4.42264e-07    1      154.30
136.60       3.28905e+00    3.72014e-07    2.97734e-07    4.46295e-07    1      151.11
129.57       3.97945e+00    3.22406e-07    2.45905e-07    3.98906e-07    1      147.93
122.71       4.81508e+00    2.47134e-07    1.87927e-07    3.06342e-07    1      144.88
