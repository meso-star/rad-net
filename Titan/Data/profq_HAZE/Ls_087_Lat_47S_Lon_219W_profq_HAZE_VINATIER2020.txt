#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   47.3 S (FP3)
#Longitude             :  218.8 W (FP3)
#Observation date      :  2017-02-01 (S97/259TI flyby)
#Solar longitude       :   86.6�
#Emission angle        :   13.6�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
279.42       8.78905e-02    4.99944e-07    3.06010e-07    6.93878e-07    1      177.14
270.28       1.06356e-01    4.87760e-07    3.01076e-07    6.74444e-07    1      175.73
261.27       1.28714e-01    4.67130e-07    2.90811e-07    6.43450e-07    1      174.06
252.41       1.55744e-01    4.39870e-07    2.76165e-07    6.03574e-07    1      171.97
243.71       1.88442e-01    4.06973e-07    2.57438e-07    5.56508e-07    1      169.45
235.18       2.28015e-01    3.72868e-07    2.37526e-07    5.08210e-07    1      166.55
226.86       2.75895e-01    3.39210e-07    2.17395e-07    4.61024e-07    1      163.46
218.74       3.33834e-01    3.06853e-07    1.97795e-07    4.15911e-07    1      160.20
210.81       4.03915e-01    2.78093e-07    1.80126e-07    3.76059e-07    1      156.95
203.10       4.88729e-01    2.53047e-07    1.64549e-07    3.41546e-07    1      153.75
195.57       5.91407e-01    2.30582e-07    1.50729e-07    3.10436e-07    1      151.15
188.21       7.15648e-01    2.11938e-07    1.39096e-07    2.84781e-07    1      148.79
181.01       8.65915e-01    1.95530e-07    1.28980e-07    2.62079e-07    1      146.58
