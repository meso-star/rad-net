#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   85.4 S (FP3)
#Longitude             :  174.3 W (FP3)
#Observation date      :  2016-05-05 (T119 flyby)
#Solar longitude       :   78.4�
#Emission angle        :   62.4�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
161.57       1.04797e+00    4.32838e-07    3.12254e-07    5.53421e-07    1      117.86
155.97       1.26824e+00    4.16444e-07    3.01398e-07    5.31490e-07    1      116.94
150.44       1.53452e+00    3.90243e-07    2.83479e-07    4.97007e-07    1      116.22
144.96       1.85658e+00    3.54923e-07    2.58919e-07    4.50927e-07    1      115.72
139.52       2.24628e+00    3.14259e-07    2.30364e-07    3.98155e-07    1      115.50
134.13       2.71814e+00    2.72059e-07    2.00447e-07    3.43671e-07    1      115.34
128.76       3.28905e+00    2.32931e-07    1.72534e-07    2.93327e-07    1      115.72
123.38       3.97945e+00    2.00478e-07    1.49223e-07    2.51732e-07    1      116.49
118.00       4.81508e+00    1.74436e-07    1.30339e-07    2.18534e-07    1      117.50
