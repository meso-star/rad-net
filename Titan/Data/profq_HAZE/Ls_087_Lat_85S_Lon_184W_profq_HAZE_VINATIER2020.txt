#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers of 0.00.05 micron radius each, density of 0.6 g/cm^3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   85.4 S (FP3)
#Longitude             :  184.2 W (FP3)
#Observation date      :  2017-02-01 (S97/259TI flyby)
#Solar longitude       :   86.6�
#Emission angle        :   48.8�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
284.00       6.00271e-02    3.37297e-07    2.05064e-07    4.69531e-07    1      178.53
274.76       7.26347e-02    3.51017e-07    2.15531e-07    4.86503e-07    1      176.81
265.67       8.78905e-02    3.93118e-07    2.43919e-07    5.42317e-07    1      175.07
256.73       1.06356e-01    4.57813e-07    2.87334e-07    6.28291e-07    1      173.06
247.94       1.28714e-01    5.40736e-07    3.43583e-07    7.37888e-07    1      170.73
239.33       1.55744e-01    6.35854e-07    4.09152e-07    8.62557e-07    1      167.83
230.92       1.88442e-01    7.10794e-07    4.62517e-07    9.59070e-07    1      164.31
222.73       2.28015e-01    7.50780e-07    4.93238e-07    1.00832e-06    1      160.15
214.79       2.75895e-01    7.28276e-07    4.81038e-07    9.75514e-07    1      155.55
207.12       3.33834e-01    6.46743e-07    4.28753e-07    8.64733e-07    1      150.59
199.74       4.03915e-01    5.74650e-07    3.81206e-07    7.68094e-07    1      145.58
192.63       4.88729e-01    5.10289e-07    3.38114e-07    6.82465e-07    1      140.79
