#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   63.8 S (FP3)
#Longitude             :  197.5 W (FP3)
#Observation date      :  2016-05-05 (T119 flyby)
#Solar longitude       :   78.4�
#Emission angle        :   42.4�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
166.84       1.04797e+00    2.28704e-07    1.66720e-07    2.90688e-07    1      136.25
160.35       1.26824e+00    2.19262e-07    1.59821e-07    2.78702e-07    1      133.80
154.00       1.53452e+00    2.04701e-07    1.49224e-07    2.60178e-07    1      131.51
147.79       1.85658e+00    1.85481e-07    1.35105e-07    2.35856e-07    1      129.42
141.71       2.24628e+00    1.62620e-07    1.18389e-07    2.06850e-07    1      127.60
135.74       2.71814e+00    1.39876e-07    1.01770e-07    1.77982e-07    1      125.85
129.87       3.28905e+00    1.18885e-07    8.63996e-08    1.51369e-07    1      124.66
124.08       3.97945e+00    1.01266e-07    7.34860e-08    1.29045e-07    1      123.90
118.35       4.81508e+00    8.79676e-08    6.37391e-08    1.12196e-07    1      123.46
