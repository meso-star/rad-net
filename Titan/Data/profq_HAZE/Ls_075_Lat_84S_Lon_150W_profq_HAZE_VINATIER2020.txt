#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 micron, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   84.0 S (FP3)
#Longitude             :  150.1 W (FP3)
#Observation date      :  2016-01-17 (T115 flyby)
#Solar longitude       :   75.1�
#Local time            :  04:10
#Solar zenith angle    :  118�
#Vertical resolution   :  29 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
582.62       1.07524e-04    7.12334e-05    3.80710e-05    1.04396e-04    1      156.90
572.33       1.31057e-04    5.96013e-05    3.20614e-05    8.71413e-05    1      158.58
562.01       1.59766e-04    5.14559e-05    2.80837e-05    7.48280e-05    1      160.24
551.65       1.94746e-04    4.42630e-05    2.47578e-05    6.37682e-05    1      161.93
541.23       2.37339e-04    3.67054e-05    2.12134e-05    5.21973e-05    1      163.67
530.79       2.89280e-04    3.09982e-05    1.85900e-05    4.34064e-05    1      165.55
520.29       3.52622e-04    2.66448e-05    1.65563e-05    3.67334e-05    1      167.58
509.74       4.29794e-04    2.25838e-05    1.45004e-05    3.06672e-05    1      169.79
499.11       5.23881e-04    1.87807e-05    1.24078e-05    2.51535e-05    1      172.13
488.42       6.38570e-04    1.55047e-05    1.05288e-05    2.04806e-05    1      174.53
477.66       7.78336e-04    1.30693e-05    9.11260e-06    1.70260e-05    1      176.85
466.84       9.48518e-04    1.14921e-05    8.19331e-06    1.47908e-05    1      178.90
455.97       1.15630e-03    1.06200e-05    7.77987e-06    1.34602e-05    1      180.48
445.09       1.40961e-03    1.01354e-05    7.59691e-06    1.26739e-05    1      181.36
434.23       1.71806e-03    9.78891e-06    7.48576e-06    1.20921e-05    1      181.40
423.45       2.09425e-03    9.26328e-06    7.18952e-06    1.13370e-05    1      180.56
412.79       2.55249e-03    8.45513e-06    6.61011e-06    1.03001e-05    1      178.81
402.31       3.11124e-03    7.46911e-06    5.84655e-06    9.09166e-06    1      176.38
392.04       3.79241e-03    6.50127e-06    5.08466e-06    7.91787e-06    1      173.60
381.99       4.62237e-03    5.72731e-06    4.47950e-06    6.97513e-06    1      170.79
372.17       5.63389e-03    5.21205e-06    4.07925e-06    6.34485e-06    1      168.10
362.58       6.86686e-03    4.91440e-06    3.86771e-06    5.96110e-06    1      165.55
353.18       8.36995e-03    4.68817e-06    3.72414e-06    5.65221e-06    1      163.13
343.98       1.02007e-02    4.36274e-06    3.49688e-06    5.22860e-06    1      160.81
334.97       1.24338e-02    3.81598e-06    3.07404e-06    4.55792e-06    1      158.65
326.14       1.51560e-02    3.06244e-06    2.45476e-06    3.67012e-06    1      156.62
317.47       1.84741e-02    2.28059e-06    1.80147e-06    2.75972e-06    1      154.76
308.95       2.25199e-02    1.64678e-06    1.27495e-06    2.01861e-06    1      153.22
300.57       2.74455e-02    1.22768e-06    9.32078e-07    1.52328e-06    1      151.90
292.31       3.34557e-02    1.01258e-06    7.59492e-07    1.26567e-06    1      150.64
284.17       4.07804e-02    9.58540e-07    7.18502e-07    1.19858e-06    1      149.07
276.15       4.97016e-02    1.03558e-06    7.88794e-07    1.28236e-06    1      147.55
268.27       6.05781e-02    1.18721e-06    9.28466e-07    1.44595e-06    1      146.15
260.49       7.38380e-02    1.28232e-06    1.02662e-06    1.53801e-06    1      144.81
252.83       9.00036e-02    1.13966e-06    9.14985e-07    1.36433e-06    1      143.58
