#Haze mass mixing ratio (from FP3 nadir data), assuming aggregates of 3000 monomers with a monomer radius of 0.05 microns, density of 0.6 g cm-3 and extinction cross section from Vinatier et al. (2012)
#Latitude              :   70.7 N (FP3)
#Longitude             :  111.9 W (FP3)
#Observation date      :  2017-02-17 (S98/262TI flyby)
#Solar longitude       :   87.1�
#Emission angle        :   54.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar)      q            q_min         q_max        sigma  Temperature(K)
186.99       8.65915e-01    2.12056e-07    1.27036e-07    2.97076e-07    1      154.39
179.52       1.04797e+00    2.05929e-07    1.24361e-07    2.87498e-07    1      152.65
172.18       1.26824e+00    2.03695e-07    1.24441e-07    2.82950e-07    1      151.68
164.92       1.53452e+00    2.05270e-07    1.27173e-07    2.83368e-07    1      151.30
157.72       1.85658e+00    2.11277e-07    1.33518e-07    2.89037e-07    1      151.15
150.57       2.24628e+00    2.23031e-07    1.44272e-07    3.01789e-07    1      150.76
143.49       2.71814e+00    2.40610e-07    1.60207e-07    3.21013e-07    1      149.57
136.49       3.28905e+00    2.65186e-07    1.82505e-07    3.47867e-07    1      147.80
129.62       3.97945e+00    2.97250e-07    2.12160e-07    3.82341e-07    1      145.24
122.89       4.81508e+00    3.36376e-07    2.49309e-07    4.23442e-07    1      141.99
