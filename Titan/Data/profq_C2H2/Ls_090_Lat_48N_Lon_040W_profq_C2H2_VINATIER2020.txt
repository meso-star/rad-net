#C2H2 volume mixing ratio profile (from nadir data)
#Latitude              :   47.9 N (FP3)
#Longitude             :   39.8 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :    6.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
218.00       4.44300e-01    3.73000e-06    3.25622e-06    4.20378e-06    1      171.63
209.55       5.37600e-01    3.25900e-06    2.84688e-06    3.67112e-06    1      170.20
201.23       6.50600e-01    2.87600e-06    2.51368e-06    3.23832e-06    1      168.76
193.04       7.87200e-01    2.57200e-06    2.24938e-06    2.89462e-06    1      166.84
185.00       9.52500e-01    2.34200e-06    2.04810e-06    2.63590e-06    1      164.31
177.14       1.15300e+00    2.17300e-06    1.90114e-06    2.44486e-06    1      161.47
169.46       1.39500e+00    2.05300e-06    1.79710e-06    2.30890e-06    1      158.39
161.97       1.68800e+00    1.97300e-06    1.72726e-06    2.21874e-06    1      155.49
154.65       2.04200e+00    1.92000e-06    1.68146e-06    2.15854e-06    1      152.96
147.48       2.47100e+00    1.89200e-06    1.65721e-06    2.12679e-06    1      150.98
140.43       2.99000e+00    1.87300e-06    1.64097e-06    2.10503e-06    1      149.23
133.48       3.61800e+00    1.85900e-06    1.62981e-06    2.08819e-06    1      148.02
126.63       4.37700e+00    1.84300e-06    1.61558e-06    2.07042e-06    1      146.90
119.87       5.29700e+00    1.82000e-06    1.59577e-06    2.04423e-06    1      145.51
113.22       6.40900e+00    1.78600e-06    1.56617e-06    2.00583e-06    1      143.41
106.72       7.75500e+00    1.74200e-06    1.52769e-06    1.95631e-06    1      140.45
100.40       9.38400e+00    1.68600e-06    1.47727e-06    1.89473e-06    1      136.62
