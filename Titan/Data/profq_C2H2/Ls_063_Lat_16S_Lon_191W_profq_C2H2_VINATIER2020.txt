#C2H2 volume mixing ratio profile (from nadir data)
#Latitude              :   16.4 S (FP3)
#Longitude             :  190.9 W (FP3)
#Observation date      :  2014-12-10 (T107 flyby)
#Solar longitude       :   62.8�
#Emission angle        :   16.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
221.50       4.44300e-01    3.63400e-06    2.98514e-06    4.28286e-06    1      175.14
212.86       5.37600e-01    3.49900e-06    2.88683e-06    4.11117e-06    1      173.82
204.34       6.50600e-01    3.21300e-06    2.66032e-06    3.76568e-06    1      172.30
195.96       7.87200e-01    2.86100e-06    2.37660e-06    3.34540e-06    1      170.56
187.71       9.52500e-01    2.52300e-06    2.10559e-06    2.94041e-06    1      168.65
