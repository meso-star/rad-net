#C2H2 volume mixing ratio profile (from nadir data)
#Latitude              :   59.1 S (FP3)
#Longitude             :  146.5 W (FP3)
#Observation date      :  2016-05-05 (T119 flyby)
#Solar longitude       :   78.4�
#Emission angle        :   39.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
344.13       1.73900e-02    8.58800e-06    6.00259e-06    1.11734e-05    1      170.59
334.94       2.10400e-02    8.69500e-06    6.10469e-06    1.12853e-05    1      169.95
325.83       2.54600e-02    8.72400e-06    6.15165e-06    1.12964e-05    1      169.62
316.80       3.08000e-02    8.60400e-06    6.09089e-06    1.11171e-05    1      169.38
307.84       3.72700e-02    8.30100e-06    5.89417e-06    1.07078e-05    1      169.04
298.97       4.51000e-02    7.81200e-06    5.55864e-06    1.00654e-05    1      167.96
290.22       5.45700e-02    7.15500e-06    5.09599e-06    9.21401e-06    1      166.64
281.60       6.60300e-02    6.35700e-06    4.52783e-06    8.18617e-06    1      165.06
273.11       7.99000e-02    5.47200e-06    3.89599e-06    7.04801e-06    1      163.18
264.78       9.66800e-02    4.55500e-06    3.23915e-06    5.87085e-06    1      161.32
256.58       1.17000e-01    3.68400e-06    2.61653e-06    4.75147e-06    1      159.48
248.52       1.41600e-01    2.91300e-06    2.06723e-06    3.75877e-06    1      157.89
240.58       1.71300e-01    2.28000e-06    1.61647e-06    2.94353e-06    1      156.56
232.75       2.07300e-01    1.78700e-06    1.26753e-06    2.30647e-06    1      155.53
225.00       2.50800e-01    1.41800e-06    1.00654e-06    1.82946e-06    1      154.77
217.32       3.03500e-01    1.14700e-06    8.16823e-07    1.47718e-06    1      154.22
209.72       3.67200e-01    9.48500e-07    6.77651e-07    1.21935e-06    1      153.70
202.18       4.44300e-01    8.01000e-07    5.74714e-07    1.02729e-06    1      153.03
194.73       5.37600e-01    6.90500e-07    4.97829e-07    8.83171e-07    1      152.00
187.37       6.50600e-01    6.08000e-07    4.40890e-07    7.75110e-07    1      150.65
180.13       7.87200e-01    5.49200e-07    4.00489e-07    6.97911e-07    1      149.13
173.01       9.52500e-01    5.12600e-07    3.75882e-07    6.49318e-07    1      146.72
166.06       1.15300e+00    4.99700e-07    3.68090e-07    6.31310e-07    1      143.93
159.28       1.39500e+00    5.12200e-07    3.78996e-07    6.45404e-07    1      140.85
152.68       1.68800e+00    5.56300e-07    4.13002e-07    6.99598e-07    1      137.73
146.21       2.04200e+00    6.41100e-07    4.77523e-07    8.04677e-07    1      136.59
139.84       2.47100e+00    7.84600e-07    5.86657e-07    9.82543e-07    1      134.99
133.58       2.99000e+00    1.01100e-06    7.59960e-07    1.26204e-06    1      133.17
127.42       3.61800e+00    1.35400e-06    1.02659e-06    1.68141e-06    1      131.38
121.38       4.37700e+00    1.85900e-06    1.42674e-06    2.29126e-06    1      129.70
115.44       5.29700e+00    2.55800e-06    1.99862e-06    3.11738e-06    1      128.13
