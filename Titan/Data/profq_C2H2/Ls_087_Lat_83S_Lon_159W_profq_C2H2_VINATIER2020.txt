#C2H2 volume mixing ratio profile (from nadir data)
#Latitude              :   83.2 S (FP3)
#Longitude             :  158.8 W (FP3)
#Observation date      :   2017-02-01 (S97/259TI flyby)
#Solar longitude       :   86.6�
#Emission angle        :   50.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
408.75       5.53900e-03    2.97700e-05    2.29351e-05    3.66049e-05    1      208.18
397.04       6.70300e-03    3.06600e-05    2.42013e-05    3.71188e-05    1      207.40
385.49       8.11000e-03    3.24300e-05    2.61683e-05    3.86917e-05    1      206.01
374.12       9.81400e-03    3.47600e-05    2.86039e-05    4.09161e-05    1      203.96
362.96       1.18700e-02    3.74000e-05    3.11862e-05    4.36138e-05    1      201.42
352.04       1.43700e-02    3.93100e-05    3.29367e-05    4.56833e-05    1      198.53
341.36       1.73900e-02    3.97200e-05    3.31020e-05    4.63380e-05    1      195.46
330.92       2.10400e-02    3.80000e-05    3.12330e-05    4.47670e-05    1      192.37
320.71       2.54600e-02    3.38600e-05    2.72820e-05    4.04380e-05    1      189.65
310.71       3.08000e-02    2.78800e-05    2.19762e-05    3.37838e-05    1      187.14
300.90       3.72700e-02    2.12300e-05    1.63758e-05    2.60842e-05    1      184.88
291.29       4.51000e-02    1.50200e-05    1.13338e-05    1.87062e-05    1      182.26
281.87       5.45700e-02    9.95700e-06    7.37234e-06    1.25417e-05    1      179.81
272.64       6.60300e-02    6.26300e-06    4.56374e-06    7.96226e-06    1      177.45
263.59       7.99000e-02    3.82800e-06    2.75498e-06    4.90102e-06    1      175.03
254.72       9.66800e-02    2.31900e-06    1.65225e-06    2.98575e-06    1      172.61
