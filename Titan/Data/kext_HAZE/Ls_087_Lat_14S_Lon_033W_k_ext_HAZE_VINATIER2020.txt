#Haze extinction coefficient profile at 1090 cm-1 (from FP3 limb data)
#Latitude              :   13.8 S (FP3)
#Longitude             :   32.5 W (FP3)
#Observation date      :  2017-02-02 (S98/259TI flyby)
#Solar longitude       :   86.6�
#Local time            :  11:35
#Solar zenith angle    :   45�
#Vertical resolution   :  64 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
435.64       5.03563e-03    2.55180e-12    2.07415e-12    3.02944e-12    1      164.21
426.20       6.09327e-03    2.91613e-12    2.37489e-12    3.45738e-12    1      164.78
416.78       7.37301e-03    3.41402e-12    2.78475e-12    4.04329e-12    1      165.63
407.36       8.92141e-03    4.09660e-12    3.34853e-12    4.84466e-12    1      166.64
397.94       1.07932e-02    5.02230e-12    4.11762e-12    5.92699e-12    1      167.71
388.53       1.30603e-02    6.26567e-12    5.15747e-12    7.37388e-12    1      168.73
379.11       1.58080e-02    7.92029e-12    6.55143e-12    9.28916e-12    1      169.70
369.71       1.91281e-02    1.01277e-11    8.43239e-12    1.18229e-11    1      170.64
360.32       2.31447e-02    1.30351e-11    1.09342e-11    1.51360e-11    1      171.65
350.92       2.80030e-02    1.67979e-11    1.42225e-11    1.93733e-11    1      172.73
341.53       3.38809e-02    2.16951e-11    1.85307e-11    2.48594e-11    1      173.74
332.16       4.09985e-02    2.79274e-11    2.40439e-11    3.18109e-11    1      174.32
322.83       4.96095e-02    3.58324e-11    3.10006e-11    4.06643e-11    1      174.46
313.55       6.00271e-02    4.56000e-11    3.95349e-11    5.16651e-11    1      174.40
304.34       7.26347e-02    5.75026e-11    4.98443e-11    6.51610e-11    1      174.12
295.21       8.78905e-02    7.15622e-11    6.19727e-11    8.11517e-11    1      173.68
286.16       1.06356e-01    8.77889e-11    7.60771e-11    9.95007e-11    1      173.18
277.20       1.28714e-01    1.05644e-10    9.18034e-11    1.19485e-10    1      172.67
268.31       1.55744e-01    1.25113e-10    1.09235e-10    1.40991e-10    1      172.22
259.51       1.88442e-01    1.44926e-10    1.27161e-10    1.62690e-10    1      171.89
250.76       2.28015e-01    1.64294e-10    1.44608e-10    1.83980e-10    1      171.70
242.08       2.75895e-01    1.82563e-10    1.60581e-10    2.04546e-10    1      171.66
233.45       3.33834e-01    2.00232e-10    1.75475e-10    2.24990e-10    1      171.70
224.88       4.03915e-01    2.17544e-10    1.89653e-10    2.45436e-10    1      171.64
216.36       4.88729e-01    2.37028e-10    2.05987e-10    2.68069e-10    1      171.32
207.92       5.91407e-01    2.62024e-10    2.27626e-10    2.96422e-10    1      170.63
199.58       7.15648e-01    2.97947e-10    2.59660e-10    3.36233e-10    1      169.43
191.36       8.65915e-01    3.50429e-10    3.06994e-10    3.93864e-10    1      167.60
183.30       1.04797e+00    4.29198e-10    3.77856e-10    4.80540e-10    1      165.17
175.40       1.26824e+00    5.43150e-10    4.79079e-10    6.07221e-10    1      162.38
167.69       1.53452e+00    7.02494e-10    6.18496e-10    7.86492e-10    1      159.55
160.15       1.85658e+00    9.05361e-10    7.93465e-10    1.01726e-09    1      157.01
152.76       2.24628e+00    1.13301e-09    9.88484e-10    1.27755e-09    1      154.90
145.49       2.71814e+00    1.35000e-09    1.17494e-09    1.52506e-09    1      153.20
138.34       3.28905e+00    1.49718e-09    1.30417e-09    1.69019e-09    1      151.86
131.28       3.97945e+00    1.53276e-09    1.34002e-09    1.72551e-09    1      150.75
124.32       4.81508e+00    1.46744e-09    1.28878e-09    1.64610e-09    1      149.48
