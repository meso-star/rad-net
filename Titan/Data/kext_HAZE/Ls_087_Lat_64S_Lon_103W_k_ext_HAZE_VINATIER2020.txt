#Haze extinction coefficient profile at 1090 cm-1 (from FP3 nadir data)
#Latitude              :   64.1 S (FP3)
#Longitude             :  103.4 W (FP3)
#Observation date      :  2017-02-01 (S97/259TI flyby)
#Solar longitude       :   86.6�
#Emission angle        :   35.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
292.96       4.96095e-02    1.44491e-11    1.11225e-11    1.77757e-11    1      182.42
283.51       6.00271e-02    1.70544e-11    1.31263e-11    2.09826e-11    1      180.41
274.23       7.26347e-02    1.98041e-11    1.52524e-11    2.43559e-11    1      178.25
265.12       8.78905e-02    2.26970e-11    1.74852e-11    2.79088e-11    1      175.95
256.20       1.06356e-01    2.57579e-11    1.98416e-11    3.16742e-11    1      173.48
247.46       1.28714e-01    2.92014e-11    2.25003e-11    3.59025e-11    1      170.74
238.93       1.55744e-01    3.31080e-11    2.55094e-11    4.07066e-11    1      167.67
230.61       1.88442e-01    3.76248e-11    2.89983e-11    4.62514e-11    1      164.18
222.51       2.28015e-01    4.29987e-11    3.31253e-11    5.28721e-11    1      160.35
214.67       2.75895e-01    4.92368e-11    3.79407e-11    6.05328e-11    1      156.30
207.06       3.33834e-01    5.66712e-11    4.36515e-11    6.96908e-11    1      152.17
199.70       4.03915e-01    6.51933e-11    5.02168e-11    8.01698e-11    1      148.10
