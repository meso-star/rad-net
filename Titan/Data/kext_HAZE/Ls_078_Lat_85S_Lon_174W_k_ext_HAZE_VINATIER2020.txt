#Haze extinction coefficient profile at 1090 cm-1 (from FP3 nadir data)
#Latitude              :   85.4 S (FP3)
#Longitude             :  174.3 W (FP3)
#Observation date      :  2016-05-05 (T119 flyby)
#Solar longitude       :   78.4�
#Emission angle        :   62.4�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
134.13       2.71814e+00    1.08401e-09    7.98676e-10    1.36935e-09    1      115.42
128.76       3.28905e+00    1.12197e-09    8.31056e-10    1.41289e-09    1      115.53
123.38       3.97945e+00    1.16257e-09    8.65345e-10    1.45979e-09    1      116.10
118.00       4.81508e+00    1.21466e-09    9.07591e-10    1.52172e-09    1      116.99
