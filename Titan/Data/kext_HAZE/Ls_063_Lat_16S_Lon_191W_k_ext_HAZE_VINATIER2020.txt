#Haze extinction coefficient profile at 1090 cm-1 (from FP3 nadir data)
#Latitude              :   16.4 S (FP3)
#Longitude             :  190.9 W (FP3)
#Observation date      :  2014-12-10 (T107 flyby)
#Solar longitude       :   62.8�
#Emission angle        :   16.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
183.66       1.04797e+00    4.74074e-10    3.40744e-10    6.07404e-10    1      167.65
175.63       1.26824e+00    6.58165e-10    4.85729e-10    8.30601e-10    1      165.60
167.75       1.53452e+00    8.69745e-10    6.54869e-10    1.08462e-09    1      163.18
160.04       1.85658e+00    1.05354e-09    8.01156e-10    1.30593e-09    1      160.49
152.49       2.24628e+00    1.13601e-09    8.62434e-10    1.40959e-09    1      157.93
145.11       2.71814e+00    1.11041e-09    8.31923e-10    1.38890e-09    1      155.46
137.89       3.28905e+00    1.01762e-09    7.45422e-10    1.28982e-09    1      152.92
130.82       3.97945e+00    9.28613e-10    6.61703e-10    1.19552e-09    1      150.32
123.90       4.81508e+00    8.91801e-10    6.16487e-10    1.16711e-09    1      147.68
