#Haze extinction coefficient profile at 1090 cm-1 (from FP3 nadir data)
#Latitude              :   60.2 N (FP3)
#Longitude             :  121.6 W (FP3)
#Observation date      :  2017-02-17 (S98/262TI flyby)
#Solar longitude       :   87.1�
#Emission angle        :   44.9�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
166.15       1.53452e+00    4.84668e-10    3.23347e-10    6.45989e-10    1      154.33
158.82       1.85658e+00    5.78356e-10    3.91996e-10    7.64716e-10    1      153.90
151.54       2.24628e+00    6.96276e-10    4.80245e-10    9.12307e-10    1      153.39
144.35       2.71814e+00    8.53847e-10    5.98897e-10    1.10880e-09    1      152.32
137.25       3.28905e+00    1.05639e-09    7.51889e-10    1.36089e-09    1      150.56
130.29       3.97945e+00    1.32308e-09    9.52387e-10    1.69377e-09    1      148.14
123.49       4.81508e+00    1.66716e-09    1.20648e-09    2.12785e-09    1      145.01
