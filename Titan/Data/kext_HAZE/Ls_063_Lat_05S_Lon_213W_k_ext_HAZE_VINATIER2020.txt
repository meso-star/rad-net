#Haze extinction coefficient profile at 1090 cm-1 (from FP3 nadir data)
#Latitude              :    4.5 S (FP3)
#Longitude             :  212.5 W (FP3)
#Observation date      :  2014-12-10 (T107 flyby)
#Solar longitude       :   62.8�
#Emission angle        :   23.8�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
184.73       1.04797e+00    3.05724e-10    2.14828e-10    3.96621e-10    1      169.58
176.58       1.26824e+00    4.17349e-10    2.99893e-10    5.34804e-10    1      167.82
168.60       1.53452e+00    5.45891e-10    3.99026e-10    6.92756e-10    1      165.64
160.76       1.85658e+00    6.56830e-10    4.84827e-10    8.28834e-10    1      163.14
153.08       2.24628e+00    7.12894e-10    5.26114e-10    8.99674e-10    1      160.70
145.57       2.71814e+00    7.05923e-10    5.16313e-10    8.95532e-10    1      158.21
138.21       3.28905e+00    6.60028e-10    4.74497e-10    8.45558e-10    1      155.56
131.02       3.97945e+00    6.20140e-10    4.35681e-10    8.04599e-10    1      152.74
124.01       4.81508e+00    6.15151e-10    4.21473e-10    8.08830e-10    1      149.79
