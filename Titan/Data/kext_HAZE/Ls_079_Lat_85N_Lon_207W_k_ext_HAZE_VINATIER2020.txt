#Haze extinction coefficient profile at 1090 cm-1 (from FP3 nadir data)
#Latitude              :   84.9 N (FP3)
#Longitude             :  207.3 W (FP3)
#Observation date      :  2016-06-08 (T120 flyby)
#Solar longitude       :   79.4�
#Emission angle        :   47.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
278.54       1.06356e-01    4.22839e-10    2.86195e-10    5.59484e-10    1      187.76
268.88       1.28714e-01    6.52134e-10    4.56599e-10    8.47669e-10    1      187.12
259.33       1.55744e-01    8.55906e-10    6.18922e-10    1.09289e-09    1      185.72
249.94       1.88442e-01    9.56774e-10    7.09911e-10    1.20364e-09    1      183.51
240.74       2.28015e-01    9.60660e-10    7.24483e-10    1.19684e-09    1      180.62
231.75       2.75895e-01    9.31081e-10    7.04699e-10    1.15746e-09    1      177.36
222.99       3.33834e-01    9.22428e-10    6.91884e-10    1.15297e-09    1      174.06
214.43       4.03915e-01    9.48048e-10    6.97517e-10    1.19858e-09    1      170.96
206.08       4.88729e-01    9.89721e-10    7.09955e-10    1.26949e-09    1      168.27
197.90       5.91407e-01    1.00643e-09    7.02110e-10    1.31075e-09    1      165.59
