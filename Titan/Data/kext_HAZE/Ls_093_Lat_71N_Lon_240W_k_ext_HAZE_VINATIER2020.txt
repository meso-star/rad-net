#Haze extinction coefficient profile at 1090 cm-1 (from FP3 limb data)
#Latitude              :   71.0 N (FP3)
#Longitude             :  239.9 W (FP3)
#Observation date      :  2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  19:27
#Solar zenith angle    :   72�
#Vertical resolution   :  54 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
523.34       1.03861e-03    1.05418e-12    7.41903e-13    1.36646e-12    1      165.92
512.42       1.27529e-03    1.03193e-12    7.28325e-13    1.33554e-12    1      168.12
501.43       1.56574e-03    1.07713e-12    7.63302e-13    1.39096e-12    1      170.24
490.39       1.92238e-03    1.20343e-12    8.56071e-13    1.55079e-12    1      172.02
479.33       2.36007e-03    1.43439e-12    1.02944e-12    1.83934e-12    1      173.23
468.30       2.89772e-03    1.80109e-12    1.30763e-12    2.29454e-12    1      173.81
457.33       3.55777e-03    2.35649e-12    1.73111e-12    2.98187e-12    1      173.85
446.44       4.36798e-03    3.17528e-12    2.37077e-12    3.97979e-12    1      173.61
435.64       5.36322e-03    4.34759e-12    3.30406e-12    5.39112e-12    1      173.54
424.90       6.58529e-03    6.00092e-12    4.64595e-12    7.35589e-12    1      174.09
414.18       8.08541e-03    8.27401e-12    6.52223e-12    1.00258e-11    1      175.48
403.43       9.92718e-03    1.14299e-11    9.17076e-12    1.36890e-11    1      177.54
392.64       1.21906e-02    1.57064e-11    1.27870e-11    1.86258e-11    1      179.83
381.78       1.49665e-02    2.14522e-11    1.77702e-11    2.51342e-11    1      181.85
370.90       1.83730e-02    2.91076e-11    2.45334e-11    3.36819e-11    1      183.23
360.05       2.25610e-02    3.87362e-11    3.32209e-11    4.42514e-11    1      183.83
349.26       2.76993e-02    5.05401e-11    4.38384e-11    5.72418e-11    1      183.74
338.58       3.40104e-02    6.36406e-11    5.53903e-11    7.18909e-11    1      183.08
328.02       4.17602e-02    7.75499e-11    6.72887e-11    8.78111e-11    1      182.01
317.60       5.12699e-02    9.14108e-11    7.90537e-11    1.03768e-10    1      180.86
307.30       6.29484e-02    1.04692e-10    9.06044e-11    1.18780e-10    1      180.01
297.12       7.72877e-02    1.17473e-10    1.02315e-10    1.32631e-10    1      179.45
287.04       9.49175e-02    1.30409e-10    1.14298e-10    1.46520e-10    1      179.05
277.06       1.16539e-01    1.44165e-10    1.26507e-10    1.61823e-10    1      178.67
267.17       1.43047e-01    1.59959e-10    1.39559e-10    1.80360e-10    1      178.14
257.38       1.75625e-01    1.77823e-10    1.54254e-10    2.01393e-10    1      177.35
247.71       2.15660e-01    1.98438e-10    1.71812e-10    2.25063e-10    1      176.15
238.19       2.64807e-01    2.20551e-10    1.91945e-10    2.49157e-10    1      174.51
228.83       3.25134e-01    2.42826e-10    2.12994e-10    2.72659e-10    1      172.33
219.67       3.99200e-01    2.64018e-10    2.32075e-10    2.95960e-10    1      169.62
210.72       4.90115e-01    2.82014e-10    2.45711e-10    3.18317e-10    1      166.57
201.98       6.01780e-01    2.93866e-10    2.52101e-10    3.35630e-10    1      163.68
193.43       7.38857e-01    2.99882e-10    2.53817e-10    3.45946e-10    1      161.18
185.07       9.07080e-01    2.99275e-10    2.52522e-10    3.46028e-10    1      158.99
176.86       1.11363e+00    2.93489e-10    2.49469e-10    3.37509e-10    1      157.11
168.79       1.36730e+00    2.85250e-10    2.46504e-10    3.23996e-10    1      155.43
160.85       1.67911e+00    2.76269e-10    2.42045e-10    3.10494e-10    1      153.76
