#Haze extinction coefficient profile at 1090 cm-1 (from FP3 limb data)
#Latitude              :   21.4 N (FP3)
#Longitude             :  195.5 W (FP3)
#Observation date      :  2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  22:00
#Solar zenith angle    :  126�
#Vertical resolution   :  43 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
518.11       1.03861e-03    4.25363e-12    3.22415e-12    5.28311e-12    1      157.14
507.84       1.27529e-03    4.16960e-12    3.16882e-12    5.17038e-12    1      157.85
497.58       1.56574e-03    4.07408e-12    3.10954e-12    5.03862e-12    1      158.80
487.32       1.92238e-03    4.04381e-12    3.10162e-12    4.98600e-12    1      160.02
477.04       2.36007e-03    4.14189e-12    3.20128e-12    5.08251e-12    1      161.46
466.73       2.89772e-03    4.43507e-12    3.46054e-12    5.40961e-12    1      163.04
456.40       3.55777e-03    4.99420e-12    3.94201e-12    6.04639e-12    1      164.64
446.04       4.36798e-03    5.88911e-12    4.70970e-12    7.06851e-12    1      166.11
435.66       5.36322e-03    7.21194e-12    5.84672e-12    8.57717e-12    1      167.39
425.27       6.58529e-03    9.03175e-12    7.42396e-12    1.06395e-11    1      168.63
414.88       8.08541e-03    1.13846e-11    9.46941e-12    1.32998e-11    1      170.15
404.46       9.92718e-03    1.41723e-11    1.19307e-11    1.64139e-11    1      172.01
393.99       1.21906e-02    1.73378e-11    1.47599e-11    1.99157e-11    1      173.85
383.50       1.49665e-02    2.07905e-11    1.78177e-11    2.37632e-11    1      175.26
373.02       1.83730e-02    2.45559e-11    2.11467e-11    2.79651e-11    1      176.08
362.58       2.25610e-02    2.87128e-11    2.47747e-11    3.26509e-11    1      176.36
352.20       2.76993e-02    3.34333e-11    2.88970e-11    3.79696e-11    1      176.38
341.90       3.40104e-02    3.90458e-11    3.38346e-11    4.42570e-11    1      176.37
331.67       4.17602e-02    4.59961e-11    3.99885e-11    5.20036e-11    1      176.39
321.50       5.12699e-02    5.44378e-11    4.74906e-11    6.13849e-11    1      176.59
311.39       6.29484e-02    6.47770e-11    5.66175e-11    7.29366e-11    1      177.04
301.32       7.72877e-02    7.71214e-11    6.74659e-11    8.67770e-11    1      177.46
291.30       9.49175e-02    9.13715e-11    8.00010e-11    1.02742e-10    1      177.62
281.36       1.16539e-01    1.07677e-10    9.44467e-11    1.20907e-10    1      177.35
271.52       1.43047e-01    1.25638e-10    1.10380e-10    1.40897e-10    1      176.70
261.77       1.75625e-01    1.44788e-10    1.27258e-10    1.62319e-10    1      175.85
252.15       2.15660e-01    1.65690e-10    1.45481e-10    1.85899e-10    1      174.96
242.64       2.64807e-01    1.88055e-10    1.64990e-10    2.11120e-10    1      174.11
233.24       3.25134e-01    2.13598e-10    1.87588e-10    2.39607e-10    1      173.17
223.96       3.99200e-01    2.43059e-10    2.13923e-10    2.72194e-10    1      171.91
214.82       4.90115e-01    2.78831e-10    2.45650e-10    3.12013e-10    1      170.27
205.82       6.01780e-01    3.21324e-10    2.82666e-10    3.59983e-10    1      168.54
196.98       7.38857e-01    3.70273e-10    3.24908e-10    4.15639e-10    1      166.90
188.27       9.07080e-01    4.27231e-10    3.74614e-10    4.79848e-10    1      165.27
179.71       1.11363e+00    4.90118e-10    4.30680e-10    5.49555e-10    1      163.67
171.28       1.36730e+00    5.60407e-10    4.93924e-10    6.26889e-10    1      161.95
163.00       1.67911e+00    6.37926e-10    5.62241e-10    7.13611e-10    1      159.99
154.87       2.06213e+00    7.18859e-10    6.30707e-10    8.07010e-10    1      157.96
146.89       2.53168e+00    8.05444e-10    7.02717e-10    9.08170e-10    1      155.86
139.08       3.10812e+00    8.97025e-10    7.80976e-10    1.01307e-09    1      153.35
131.45       3.81637e+00    9.96812e-10    8.70975e-10    1.12265e-09    1      150.33
124.01       4.68583e+00    1.10205e-09    9.69235e-10    1.23486e-09    1      147.01
