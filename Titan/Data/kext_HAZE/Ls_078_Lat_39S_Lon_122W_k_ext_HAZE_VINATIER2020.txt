#Haze extinction coefficient profile at 1090 cm-1 (from FP3 limb data)
#Latitude              :   38.6 S (FP3)
#Longitude             :  121.6 W (FP3)
#Observation date      :  2016-05-07 (T119 flyby)
#Solar longitude       :   78.4�
#Local time            :  05:25
#Solar zenith angle    :  113�
#Vertical resolution   :  44 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) kext(cm-1)       kext_min       kext_max     sigma  Temperature(K)
434.58       4.16156e-03    3.97487e-12    2.97467e-12    4.97507e-12    1      165.85
425.03       5.03563e-03    4.12971e-12    3.12607e-12    5.13335e-12    1      167.32
415.45       6.09327e-03    4.19271e-12    3.20782e-12    5.17759e-12    1      168.95
405.84       7.37301e-03    4.24325e-12    3.27896e-12    5.20753e-12    1      170.48
396.21       8.92141e-03    4.35514e-12    3.39821e-12    5.31207e-12    1      171.72
386.59       1.07932e-02    4.61954e-12    3.64102e-12    5.59807e-12    1      172.54
377.00       1.30603e-02    5.15377e-12    4.10334e-12    6.20419e-12    1      172.83
367.47       1.58080e-02    6.05474e-12    4.87105e-12    7.23842e-12    1      172.62
358.02       1.91281e-02    7.50320e-12    6.09530e-12    8.91109e-12    1      172.02
348.67       2.31447e-02    9.65482e-12    7.90830e-12    1.14013e-11    1      171.24
339.42       2.80030e-02    1.26877e-11    1.04611e-11    1.49143e-11    1      170.40
330.28       3.38809e-02    1.66777e-11    1.38272e-11    1.95282e-11    1      169.54
321.25       4.09985e-02    2.16259e-11    1.80537e-11    2.51980e-11    1      168.43
312.33       4.96095e-02    2.73814e-11    2.30954e-11    3.16675e-11    1      167.20
303.52       6.00271e-02    3.37714e-11    2.88158e-11    3.87271e-11    1      166.24
294.82       7.26347e-02    4.08083e-11    3.51788e-11    4.64378e-11    1      165.53
286.20       8.78905e-02    4.88475e-11    4.23311e-11    5.53640e-11    1      165.18
277.63       1.06356e-01    5.86650e-11    5.08086e-11    6.65215e-11    1      165.16
269.11       1.28714e-01    7.15077e-11    6.17862e-11    8.12293e-11    1      165.39
260.64       1.55744e-01    8.84061e-11    7.64401e-11    1.00372e-10    1      165.77
252.18       1.88442e-01    1.11151e-10    9.66460e-11    1.25655e-10    1      166.09
243.77       2.28015e-01    1.40524e-10    1.23152e-10    1.57897e-10    1      166.22
235.41       2.75895e-01    1.76619e-10    1.55491e-10    1.97746e-10    1      166.10
227.11       3.33834e-01    2.18644e-10    1.92009e-10    2.45279e-10    1      165.66
218.89       4.03915e-01    2.63325e-10    2.29686e-10    2.96964e-10    1      164.85
210.77       4.88729e-01    3.08437e-10    2.67881e-10    3.48993e-10    1      163.63
202.76       5.91407e-01    3.48995e-10    3.03625e-10    3.94364e-10    1      162.26
194.86       7.15648e-01    3.85185e-10    3.37630e-10    4.32740e-10    1      160.79
187.10       8.65915e-01    4.15565e-10    3.66333e-10    4.64796e-10    1      159.00
179.46       1.04797e+00    4.43461e-10    3.89336e-10    4.97585e-10    1      156.94
171.96       1.26824e+00    4.71525e-10    4.08136e-10    5.34913e-10    1      154.76
164.62       1.53452e+00    4.99313e-10    4.25488e-10    5.73139e-10    1      152.54
157.42       1.85658e+00    5.32820e-10    4.50034e-10    6.15606e-10    1      150.38
150.35       2.24628e+00    5.71714e-10    4.84376e-10    6.59053e-10    1      148.29
143.42       2.71814e+00    6.20086e-10    5.32994e-10    7.07179e-10    1      146.28
