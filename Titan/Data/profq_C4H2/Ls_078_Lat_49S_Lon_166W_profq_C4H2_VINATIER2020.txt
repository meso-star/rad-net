#C4H2 volume mixing ratio profile (from nadir data)
#Latitude              :   48.8 S (FP3)
#Longitude             :  165.7 W (FP3)
#Observation date      :  2016-05-05 (T119 flyby)
#Solar longitude       :   78.4�
#Emission angle        :   29.0�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
233.54       2.50800e-01    9.85700e-09    8.24905e-09    1.14649e-08    1      164.54
225.34       3.03500e-01    9.68500e-09    8.12273e-09    1.12473e-08    1      163.90
217.22       3.67200e-01    9.49700e-09    7.98089e-09    1.10131e-08    1      162.77
209.22       4.44300e-01    9.25700e-09    7.79067e-09    1.07233e-08    1      161.15
201.36       5.37600e-01    8.91000e-09    7.50399e-09    1.03160e-08    1      159.05
193.65       6.50600e-01    8.36700e-09    7.04636e-09    9.68764e-09    1      156.99
186.08       7.87200e-01    7.57500e-09    6.37408e-09    8.77592e-09    1      154.71
178.67       9.52500e-01    6.51700e-09    5.47491e-09    7.55909e-09    1      152.15
171.42       1.15300e+00    5.28700e-09    4.43268e-09    6.14132e-09    1      149.53
164.34       1.39500e+00    4.02500e-09    3.36586e-09    4.68414e-09    1      146.91
157.41       1.68800e+00    2.89100e-09    2.41202e-09    3.36998e-09    1      144.43
