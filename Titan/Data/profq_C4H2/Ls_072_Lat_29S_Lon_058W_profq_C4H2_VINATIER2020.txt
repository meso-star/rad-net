#C4H2 volume mixing ratio profile (from limb data)
#Latitude              :   28.5 S (FP3)
#Longitude             :   58.1 W (FP3)
#Observation date      :  2015-09-29 (T113 flyby)
#Solar longitude       :   71.7�
#Local time            :  13:58
#Solar zenith angle    :   63�
#Vertical resolution   :  44 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
451.21       3.12700e-03    2.62100e-08    2.12960e-08    3.11240e-08    1      169.42
441.39       3.78300e-03    2.70600e-08    2.20572e-08    3.20628e-08    1      169.19
431.65       4.57800e-03    2.82800e-08    2.31340e-08    3.34260e-08    1      169.02
421.97       5.53900e-03    2.96700e-08    2.43924e-08    3.49476e-08    1      168.98
412.36       6.70300e-03    3.10400e-08    2.56431e-08    3.64369e-08    1      169.03
402.81       8.11000e-03    3.20900e-08    2.66342e-08    3.75458e-08    1      169.09
393.31       9.81400e-03    3.24800e-08    2.70774e-08    3.78826e-08    1      169.11
383.88       1.18700e-02    3.18200e-08    2.66152e-08    3.70248e-08    1      169.05
374.51       1.43700e-02    2.99700e-08    2.51323e-08    3.48077e-08    1      168.91
365.21       1.73900e-02    2.69700e-08    2.26546e-08    3.12854e-08    1      168.75
355.97       2.10400e-02    2.32000e-08    1.94841e-08    2.69159e-08    1      168.67
346.80       2.54600e-02    1.93000e-08    1.62062e-08    2.23938e-08    1      168.80
337.66       3.08000e-02    1.57400e-08    1.32047e-08    1.82753e-08    1      169.10
328.57       3.72700e-02    1.28500e-08    1.07812e-08    1.49188e-08    1      169.40
319.52       4.51000e-02    1.06800e-08    8.95141e-09    1.24086e-08    1      169.42
310.53       5.45700e-02    9.13700e-09    7.65230e-09    1.06217e-08    1      169.51
301.59       6.60300e-02    8.09300e-09    6.78194e-09    9.40406e-09    1      169.71
292.69       7.99000e-02    7.37500e-09    6.18544e-09    8.56456e-09    1      169.99
283.82       9.66800e-02    6.85800e-09    5.75887e-09    7.95713e-09    1      170.43
274.99       1.17000e-01    6.43400e-09    5.41150e-09    7.45650e-09    1      170.86
266.19       1.41600e-01    6.01400e-09    5.06769e-09    6.96031e-09    1      171.22
257.44       1.71300e-01    5.56400e-09    4.69750e-09    6.43050e-09    1      171.36
248.74       2.07300e-01    5.09800e-09    4.31149e-09    5.88451e-09    1      171.15
240.11       2.50800e-01    4.63900e-09    3.92956e-09    5.34844e-09    1      170.58
231.57       3.03500e-01    4.22100e-09    3.57947e-09    4.86253e-09    1      169.71
223.13       3.67200e-01    3.85300e-09    3.27052e-09    4.43548e-09    1      168.61
214.80       4.44300e-01    3.53900e-09    3.00637e-09    4.07163e-09    1      167.38
206.58       5.37600e-01    3.24900e-09    2.76190e-09    3.73610e-09    1      166.26
198.45       6.50600e-01    2.95500e-09    2.51378e-09    3.39622e-09    1      165.45
190.40       7.87200e-01    2.62000e-09    2.22736e-09    3.01264e-09    1      164.73
182.44       9.52500e-01    2.23800e-09    1.89819e-09    2.57781e-09    1      163.93
174.57       1.15300e+00    1.82400e-09    1.54194e-09    2.10606e-09    1      163.07
166.78       1.39500e+00    1.40800e-09    1.18482e-09    1.63118e-09    1      162.05
159.10       1.68800e+00    1.04600e-09    8.75112e-10    1.21689e-09    1      160.57
151.55       2.04200e+00    7.59200e-10    6.31175e-10    8.87225e-10    1      158.51
144.15       2.47100e+00    5.57100e-10    4.60308e-10    6.53892e-10    1      155.75
136.93       2.99000e+00    4.20500e-10    3.45455e-10    4.95545e-10    1      152.18
129.94       3.61800e+00    3.19400e-10    2.61041e-10    3.77759e-10    1      147.81
124.45       4.22100e+00    2.63500e-10    2.14521e-10    3.12479e-10    1      144.46
