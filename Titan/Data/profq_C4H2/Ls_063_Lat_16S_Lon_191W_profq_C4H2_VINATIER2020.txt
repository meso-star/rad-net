#C4H2 volume mixing ratio profile (from nadir data)
#Latitude              :   16.4 S (FP3)
#Longitude             :  190.9 W (FP3)
#Observation date      :  2014-12-10 (T107 flyby)
#Solar longitude       :   62.8�
#Emission angle        :   16.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
266.14       1.71300e-01    4.57000e-09    3.31979e-09    5.82021e-09    1      177.48
257.07       2.07300e-01    4.53300e-09    3.30271e-09    5.76329e-09    1      177.67
248.05       2.50800e-01    4.54200e-09    3.31847e-09    5.76553e-09    1      177.51
239.11       3.03500e-01    4.50200e-09    3.29491e-09    5.70909e-09    1      177.01
230.25       3.67200e-01    4.31100e-09    3.15685e-09    5.46515e-09    1      176.21
221.50       4.44300e-01    3.90700e-09    2.86166e-09    4.95234e-09    1      175.14
212.86       5.37600e-01    3.31000e-09    2.42113e-09    4.19888e-09    1      173.82
204.34       6.50600e-01    2.60800e-09    1.90434e-09    3.31166e-09    1      172.30
195.96       7.87200e-01    1.92000e-09    1.39723e-09    2.44277e-09    1      170.56
187.71       9.52500e-01    1.34000e-09    9.72817e-10    1.70718e-09    1      168.65
