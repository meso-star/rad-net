#C4H2 volume mixing ratio profile (from limb data)
#Latitude              :   80.2 N (FP3)
#Longitude             :  260.9 W (FP3)
#Observation date      :  2015-05-09 (T111 flyby)
#Solar longitude       :   67.4�
#Local time            :  01:10
#Solar zenith angle    :   77�
#Vertical resolution   :  34 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
451.63       3.43500e-03    2.69800e-08    1.90904e-08    3.48696e-08    1      164.06
441.75       4.18700e-03    2.36700e-08    1.68152e-08    3.05248e-08    1      163.89
431.94       5.10300e-03    2.06300e-08    1.47147e-08    2.65453e-08    1      164.18
422.15       6.22000e-03    1.79500e-08    1.28747e-08    2.30253e-08    1      165.05
412.37       7.58100e-03    1.56900e-08    1.13521e-08    2.00279e-08    1      166.46
402.54       9.24100e-03    1.39200e-08    1.01858e-08    1.76542e-08    1      168.34
392.67       1.12600e-02    1.26800e-08    9.41338e-09    1.59466e-08    1      170.61
382.72       1.37300e-02    1.20000e-08    9.03607e-09    1.49639e-08    1      173.05
372.70       1.67300e-02    1.18900e-08    9.11569e-09    1.46643e-08    1      175.47
362.61       2.04000e-02    1.24100e-08    9.70661e-09    1.51134e-08    1      177.70
352.48       2.48600e-02    1.36300e-08    1.08718e-08    1.63882e-08    1      179.62
342.32       3.03000e-02    1.56300e-08    1.27301e-08    1.85299e-08    1      181.09
332.17       3.69400e-02    1.85600e-08    1.53450e-08    2.17750e-08    1      181.96
322.05       4.50200e-02    2.25200e-08    1.88733e-08    2.61667e-08    1      182.12
312.00       5.48700e-02    2.75900e-08    2.33614e-08    3.18186e-08    1      182.25
302.01       6.68800e-02    3.37800e-08    2.88349e-08    3.87251e-08    1      182.65
292.05       8.15200e-02    4.09300e-08    3.51465e-08    4.67135e-08    1      183.36
282.12       9.93700e-02    4.86400e-08    4.20293e-08    5.52507e-08    1      184.30
272.22       1.21100e-01    5.62900e-08    4.88805e-08    6.36995e-08    1      184.92
262.37       1.47600e-01    6.31100e-08    5.50073e-08    7.12127e-08    1      184.84
252.62       1.79900e-01    6.84300e-08    5.97558e-08    7.71042e-08    1      183.66
243.02       2.19300e-01    7.14000e-08    6.23701e-08    8.04299e-08    1      181.31
233.64       2.67300e-01    6.67700e-08    5.82749e-08    7.52651e-08    1      178.05
224.51       3.25900e-01    5.72100e-08    4.98477e-08    6.45723e-08    1      174.34
215.62       3.97200e-01    4.64100e-08    4.03169e-08    5.25031e-08    1      170.61
206.97       4.84100e-01    3.71900e-08    3.21900e-08    4.21900e-08    1      167.21
198.53       5.90100e-01    3.04800e-08    2.62781e-08    3.46819e-08    1      164.29
190.29       7.19200e-01    2.59400e-08    2.22821e-08    2.95979e-08    1      161.77
182.21       8.76700e-01    2.28000e-08    1.95298e-08    2.60702e-08    1      159.45
174.30       1.06900e+00    2.03900e-08    1.73826e-08    2.33974e-08    1      157.14
166.55       1.30200e+00    1.79100e-08    1.51856e-08    2.06344e-08    1      154.55
158.99       1.58800e+00    1.51300e-08    1.27500e-08    1.75100e-08    1      151.27
151.64       1.93500e+00    1.20800e-08    1.00619e-08    1.40981e-08    1      147.50
144.52       2.35900e+00    9.25100e-09    7.58282e-09    1.09192e-08    1      143.78
137.60       2.87500e+00    6.84100e-09    5.49896e-09    8.18304e-09    1      140.73
130.86       3.50400e+00    4.78800e-09    3.76319e-09    5.81281e-09    1      137.76
124.29       4.27100e+00    3.24500e-09    2.49130e-09    3.99870e-09    1      134.75
117.89       5.20600e+00    2.23200e-09    1.67494e-09    2.78906e-09    1      132.20
