#C4H2 volume mixing ratio profile (from nadir data)
#Latitude              :    0.5 N (FP3)
#Longitude             :   40.0 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   56.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
283.41       1.17000e-01    1.21500e-08    9.39464e-09    1.49054e-08    1      174.28
274.41       1.41600e-01    1.14600e-08    9.00178e-09    1.39182e-08    1      173.86
265.48       1.71300e-01    1.07100e-08    8.52633e-09    1.28937e-08    1      173.49
256.62       2.07300e-01    9.88400e-09    7.96106e-09    1.18069e-08    1      173.23
247.83       2.50800e-01    8.94600e-09    7.27050e-09    1.06215e-08    1      173.13
239.09       3.03500e-01    7.92200e-09    6.46643e-09    9.37756e-09    1      173.26
230.39       3.67200e-01    6.80600e-09    5.54926e-09    8.06274e-09    1      173.48
221.74       4.44300e-01    5.64400e-09    4.57378e-09    6.71422e-09    1      173.61
213.15       5.37600e-01    4.49000e-09    3.59860e-09    5.38140e-09    1      173.41
204.62       6.50600e-01    3.41800e-09    2.70184e-09    4.13416e-09    1      173.07
196.17       7.87200e-01    2.47600e-09    1.92553e-09    3.02647e-09    1      172.06
187.85       9.52500e-01    1.71600e-09    1.31420e-09    2.11780e-09    1      170.17
179.68       1.15300e+00    1.15000e-09    8.67826e-10    1.43217e-09    1      167.66
