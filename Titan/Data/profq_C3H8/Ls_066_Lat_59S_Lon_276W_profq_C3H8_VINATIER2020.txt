#C3H8 volume mixing ratio profile (from limb data)
#Latitude              :   58.9 S (FP3)
#Longitude             :  275.6 W (FP3)
#Observation date      :  2015-03-17 (T110 flyby)
#Solar longitude       :   65.8�
#Local time            :  16:35
#Solar zenith angle    :  102�
#Vertical resolution   :  36 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
430.07       3.43500e-03    4.15400e-06    2.92629e-06    5.38171e-06    1      163.10
420.38       4.18700e-03    4.51500e-06    3.24202e-06    5.78798e-06    1      163.37
410.73       5.10300e-03    4.76000e-06    3.48797e-06    6.03203e-06    1      163.81
401.11       6.22000e-03    4.84500e-06    3.63059e-06    6.05941e-06    1      164.37
391.52       7.58100e-03    4.76200e-06    3.64729e-06    5.87671e-06    1      164.92
381.97       9.24100e-03    4.53400e-06    3.54671e-06    5.52129e-06    1      165.32
372.46       1.12600e-02    4.20300e-06    3.35030e-06    5.05570e-06    1      165.61
362.99       1.37300e-02    3.81900e-06    3.09197e-06    4.54603e-06    1      165.73
353.58       1.67300e-02    3.42400e-06    2.80531e-06    4.04269e-06    1      165.82
344.23       2.04000e-02    3.05900e-06    2.52572e-06    3.59228e-06    1      166.02
334.91       2.48600e-02    2.75300e-06    2.28328e-06    3.22272e-06    1      166.52
325.62       3.03000e-02    2.52700e-06    2.10249e-06    2.95151e-06    1      167.26
316.35       3.69400e-02    2.39100e-06    1.99382e-06    2.78818e-06    1      168.03
307.11       4.50200e-02    2.34800e-06    1.96578e-06    2.73022e-06    1      168.22
297.92       5.48700e-02    2.39300e-06    2.01578e-06    2.77022e-06    1      168.36
288.78       6.68800e-02    2.50300e-06    2.12751e-06    2.87849e-06    1      168.37
279.70       8.15200e-02    2.64400e-06    2.26693e-06    3.02107e-06    1      168.16
270.70       9.93700e-02    2.75600e-06    2.37939e-06    3.13261e-06    1      167.88
261.78       1.21100e-01    2.78700e-06    2.41608e-06    3.15792e-06    1      167.36
252.94       1.47600e-01    2.70400e-06    2.34698e-06    3.06102e-06    1      166.64
244.20       1.79900e-01    2.52500e-06    2.19401e-06    2.85599e-06    1      165.64
235.58       2.19300e-01    2.30300e-06    2.00321e-06    2.60279e-06    1      164.30
227.09       2.67300e-01    2.09000e-06    1.81900e-06    2.36100e-06    1      162.71
218.73       3.25900e-01    1.92800e-06    1.67689e-06    2.17911e-06    1      161.00
210.51       3.97200e-01    1.82700e-06    1.58719e-06    2.06681e-06    1      159.25
202.43       4.84100e-01    1.77700e-06    1.54461e-06    2.00939e-06    1      157.55
194.47       5.90100e-01    1.75200e-06    1.52507e-06    1.97893e-06    1      155.96
186.64       7.19200e-01    1.70800e-06    1.49169e-06    1.92431e-06    1      154.43
178.93       8.76700e-01    1.61400e-06    1.40971e-06    1.81829e-06    1      152.89
171.37       1.06900e+00    1.45400e-06    1.26553e-06    1.64247e-06    1      150.54
163.97       1.30200e+00    1.24000e-06    1.07380e-06    1.40620e-06    1      147.39
156.78       1.58800e+00    1.01700e-06    8.76568e-07    1.15743e-06    1      144.30
149.77       1.93500e+00    8.18100e-07    7.01938e-07    9.34262e-07    1      141.05
142.94       2.35900e+00    6.67500e-07    5.69817e-07    7.65183e-07    1      138.46
136.27       2.87500e+00    5.69500e-07    4.82518e-07    6.56482e-07    1      136.07
129.75       3.50400e+00    5.21900e-07    4.38671e-07    6.05129e-07    1      133.70
123.36       4.27100e+00    5.21800e-07    4.36573e-07    6.07028e-07    1      131.47
117.11       5.20600e+00    5.69900e-07    4.78031e-07    6.61769e-07    1      129.59
