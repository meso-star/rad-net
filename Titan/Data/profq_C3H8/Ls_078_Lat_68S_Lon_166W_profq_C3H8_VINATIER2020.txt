#C3H8 volume mixing ratio profile (from nadir data)
#Latitude              :   68.4 S (FP3)
#Longitude             :  166.3 W (FP3)
#Observation date      :  2016-05-05 (T119 flyby)
#Solar longitude       :   78.4�
#Emission angle        :   46.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
294.98       3.72700e-02    6.88700e-06    4.15105e-06    9.62295e-06    1      169.20
286.22       4.51000e-02    6.56000e-06    3.98582e-06    9.13418e-06    1      166.65
277.65       5.45700e-02    6.07600e-06    3.72629e-06    8.42571e-06    1      164.08
269.26       6.60300e-02    5.55200e-06    3.43813e-06    7.66587e-06    1      161.57
261.04       7.99000e-02    5.06500e-06    3.16846e-06    6.96154e-06    1      159.14
252.98       9.66800e-02    4.67700e-06    2.95706e-06    6.39694e-06    1      157.00
245.08       1.17000e-01    4.39800e-06    2.81355e-06    5.98245e-06    1      155.01
237.32       1.41600e-01    4.25800e-06    2.75685e-06    5.75915e-06    1      153.22
229.68       1.71300e-01    4.25100e-06    2.78404e-06    5.71796e-06    1      151.46
222.18       2.07300e-01    4.36700e-06    2.88975e-06    5.84425e-06    1      149.62
214.81       2.50800e-01    4.57700e-06    3.05463e-06    6.09937e-06    1      147.64
207.58       3.03500e-01    4.81600e-06    3.23041e-06    6.40159e-06    1      145.53
200.50       3.67200e-01    4.98800e-06    3.35179e-06    6.62421e-06    1      143.24
193.57       4.44300e-01    4.98800e-06    3.34198e-06    6.63402e-06    1      140.78
186.79       5.37600e-01    4.72700e-06    3.14368e-06    6.31032e-06    1      138.18
180.17       6.50600e-01    4.20800e-06    2.76814e-06    5.64786e-06    1      135.93
173.68       7.87200e-01    3.49800e-06    2.27116e-06    4.72484e-06    1      133.70
167.34       9.52500e-01    2.73500e-06    1.75145e-06    3.71855e-06    1      131.41
