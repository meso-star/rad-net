#C3H8 volume mixing ratio profile (from nadir data)
#Latitude              :   69.2 N (FP3)
#Longitude             :   48.3 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   18.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
119.63       5.29700e+00    1.21100e-06    1.04310e-06    1.37890e-06    1      144.04
113.04       6.40900e+00    1.22000e-06    1.05226e-06    1.38774e-06    1      142.31
106.58       7.75500e+00    1.22400e-06    1.05481e-06    1.39319e-06    1      139.64
100.30       9.38400e+00    1.22400e-06    1.05154e-06    1.39646e-06    1      136.03
