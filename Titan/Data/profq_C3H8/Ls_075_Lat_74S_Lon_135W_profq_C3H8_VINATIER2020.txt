#C3H8 volume mixing ratio profile (from limb data)
#Latitude              :   73.8 S (FP3)
#Longitude             :  135.0 W (FP3)
#Observation date      :  2016-01-17 (T115 flyby)
#Solar longitude       :   75.1�
#Local time            :  05:47
#Solar zenith angle    :  116�
#Vertical resolution   :  32 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
504.21       5.62200e-04    6.65100e-06    5.45266e-06    7.84934e-06    1      162.12
492.81       7.02100e-04    6.31000e-06    5.17508e-06    7.44492e-06    1      163.98
481.36       8.76800e-04    5.92900e-06    4.86496e-06    6.99304e-06    1      165.88
469.87       1.09500e-03    5.52500e-06    4.53688e-06    6.51312e-06    1      167.75
458.34       1.36700e-03    5.12200e-06    4.20972e-06    6.03428e-06    1      169.49
446.78       1.70800e-03    4.74200e-06    3.90267e-06    5.58133e-06    1      171.02
435.22       2.13200e-03    4.40300e-06    3.63124e-06    5.17476e-06    1      172.29
423.68       2.66300e-03    4.14200e-06    3.42300e-06    4.86100e-06    1      173.17
412.18       3.32500e-03    3.96500e-06    3.28669e-06    4.64331e-06    1      173.67
400.74       4.15300e-03    3.89700e-06    3.24411e-06    4.54989e-06    1      173.91
389.38       5.18600e-03    3.95600e-06    3.30748e-06    4.60452e-06    1      174.00
378.11       6.47600e-03    4.16500e-06    3.50076e-06    4.82924e-06    1      173.85
366.95       8.08700e-03    4.54500e-06    3.84275e-06    5.24725e-06    1      173.30
355.92       1.01000e-02    5.08800e-06    4.32572e-06    5.85028e-06    1      172.30
345.04       1.26100e-02    5.73200e-06    4.90100e-06    6.56300e-06    1      170.85
334.36       1.57500e-02    6.35500e-06    5.46088e-06    7.24912e-06    1      169.03
323.87       1.96700e-02    6.73200e-06    5.80698e-06    7.65702e-06    1      166.95
313.58       2.45600e-02    6.64700e-06    5.74618e-06    7.54782e-06    1      164.81
303.51       3.06700e-02    6.03200e-06    5.21528e-06    6.84872e-06    1      162.59
293.64       3.83000e-02    5.04900e-06    4.35638e-06    5.74162e-06    1      160.07
284.02       4.78300e-02    4.00100e-06    3.43753e-06    4.56447e-06    1      157.08
274.63       5.97300e-02    3.10700e-06    2.65330e-06    3.56070e-06    1      154.15
265.47       7.45900e-02    2.45600e-06    2.08192e-06    2.83008e-06    1      151.42
260.53       8.42500e-02    2.16900e-06    1.83178e-06    2.50622e-06    1      150.08
