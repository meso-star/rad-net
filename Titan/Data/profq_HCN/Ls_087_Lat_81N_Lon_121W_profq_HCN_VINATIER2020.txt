#HCN volume mixing ratio profile (from nadir data)
#Latitude              :   80.9 N (FP3)
#Longitude             :  121.3 W (FP3)
#Observation date      :   2017-02-17 (S98/262TI flyby)
#Solar longitude       :   87.1�
#Emission angle        :   65.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
276.40       1.17000e-01    7.06300e-06    5.91085e-06    8.21515e-06    1      183.70
266.94       1.41600e-01    6.71000e-06    5.69371e-06    7.72629e-06    1      183.84
257.55       1.71300e-01    6.00700e-06    5.14424e-06    6.86976e-06    1      183.38
248.27       2.07300e-01    5.04000e-06    4.32726e-06    5.75274e-06    1      182.07
239.14       2.50800e-01    3.94500e-06    3.37453e-06    4.51547e-06    1      179.77
230.21       3.03500e-01    2.88800e-06    2.44746e-06    3.32854e-06    1      176.58
221.51       3.67200e-01    1.98400e-06    1.66066e-06    2.30734e-06    1      172.53
213.08       4.44300e-01    1.29200e-06    1.06447e-06    1.51953e-06    1      167.96
204.92       5.37600e-01    8.05800e-07    6.54348e-07    9.57252e-07    1      163.23
