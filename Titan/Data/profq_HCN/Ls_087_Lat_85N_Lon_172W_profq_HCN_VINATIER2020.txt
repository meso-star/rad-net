#HCN volume mixing ratio profile (from nadir data)
#Latitude              :   84.9 N (FP3)
#Longitude             :  171.5 W (FP3)
#Observation date      :   2017-02-17 (S98/262TI flyby)
#Solar longitude       :   87.1�
#Emission angle        :   69.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
276.60       1.17000e-01    5.41100e-06    4.52645e-06    6.29555e-06    1      184.45
267.10       1.41600e-01    5.13300e-06    4.35296e-06    5.91304e-06    1      184.44
257.69       1.71300e-01    4.61200e-06    3.94457e-06    5.27943e-06    1      183.82
248.39       2.07300e-01    3.89900e-06    3.34162e-06    4.45638e-06    1      182.34
239.24       2.50800e-01    3.08500e-06    2.63263e-06    3.53737e-06    1      179.89
230.31       3.03500e-01    2.28600e-06    1.93316e-06    2.63884e-06    1      176.56
221.61       3.67200e-01    1.58900e-06    1.32667e-06    1.85133e-06    1      172.39
213.19       4.44300e-01    1.04500e-06    8.60222e-07    1.22978e-06    1      167.75
205.05       5.37600e-01    6.57000e-07    5.32430e-07    7.81569e-07    1      162.98
