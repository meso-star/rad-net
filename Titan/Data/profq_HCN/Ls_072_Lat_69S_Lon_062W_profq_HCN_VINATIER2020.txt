#HCN volume mixing ratio profile (from limb data)
#Latitude              :   68.9 S (FP3)
#Longitude             :   62.3 W (FP3)
#Observation date      :  2015-09-29 (T113 flyby)
#Solar longitude       :   71.7�
#Local time            :  13:47
#Solar zenith angle    :   98�
#Vertical resolution   :  34 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
585.03       1.01100e-04    2.51000e-05    1.88932e-05    3.13068e-05    1      157.95
575.02       1.22300e-04    2.16700e-05    1.62954e-05    2.70446e-05    1      158.61
565.04       1.48000e-04    1.86700e-05    1.40454e-05    2.32946e-05    1      159.36
555.06       1.79100e-04    1.61000e-05    1.21140e-05    2.00860e-05    1      160.20
545.09       2.16700e-04    1.39900e-05    1.05295e-05    1.74505e-05    1      161.12
535.13       2.62300e-04    1.23400e-05    9.29203e-06    1.53880e-05    1      162.09
525.17       3.17300e-04    1.10600e-05    8.34562e-06    1.37744e-05    1      163.08
515.22       3.84000e-04    1.01600e-05    7.66478e-06    1.26552e-05    1      164.06
505.27       4.64600e-04    9.52900e-06    7.19424e-06    1.18638e-05    1      165.04
495.32       5.62200e-04    9.14400e-06    6.91227e-06    1.13757e-05    1      166.00
485.39       6.80300e-04    8.95800e-06    6.78613e-06    1.11299e-05    1      166.94
475.46       8.23200e-04    8.94600e-06    6.79510e-06    1.10969e-05    1      167.89
465.54       9.96100e-04    9.09000e-06    6.92507e-06    1.12549e-05    1      168.85
455.63       1.20500e-03    9.36900e-06    7.16488e-06    1.15731e-05    1      169.82
445.72       1.45800e-03    9.76100e-06    7.49661e-06    1.20254e-05    1      170.80
435.83       1.76500e-03    1.02100e-05    7.86470e-06    1.25553e-05    1      171.78
425.94       2.13500e-03    1.05600e-05    8.19357e-06    1.29264e-05    1      172.72
416.07       2.58400e-03    1.07700e-05    8.38782e-06    1.31522e-05    1      173.57
406.21       3.12700e-03    1.06600e-05    8.34074e-06    1.29793e-05    1      174.34
396.38       3.78300e-03    1.02100e-05    8.02232e-06    1.23977e-05    1      175.08
386.57       4.57800e-03    9.50800e-06    7.50111e-06    1.15149e-05    1      175.83
376.79       5.53900e-03    8.70500e-06    6.89627e-06    1.05137e-05    1      176.51
367.04       6.70300e-03    7.98000e-06    6.34766e-06    9.61234e-06    1      176.96
357.34       8.11000e-03    7.46600e-06    5.96040e-06    8.97160e-06    1      176.98
347.72       9.81400e-03    7.26100e-06    5.82050e-06    8.70150e-06    1      176.47
338.20       1.18700e-02    7.40800e-06    5.96259e-06    8.85341e-06    1      175.40
328.82       1.43700e-02    7.86100e-06    6.35185e-06    9.37015e-06    1      173.86
319.58       1.73900e-02    8.61700e-06    6.99072e-06    1.02433e-05    1      172.04
310.50       2.10400e-02    9.56300e-06    7.78732e-06    1.13387e-05    1      170.23
301.57       2.54600e-02    1.04500e-05    8.54690e-06    1.23531e-05    1      168.72
292.76       3.08000e-02    1.10000e-05    9.00474e-06    1.29953e-05    1      167.59
284.05       3.72700e-02    1.09100e-05    8.92152e-06    1.28985e-05    1      166.74
275.44       4.51000e-02    1.00400e-05    8.22103e-06    1.18590e-05    1      165.80
266.93       5.45700e-02    8.51900e-06    6.94381e-06    1.00942e-05    1      164.97
258.52       6.60300e-02    6.65300e-06    5.39023e-06    7.91577e-06    1      164.04
250.21       7.99000e-02    4.82400e-06    3.87283e-06    5.77517e-06    1      162.66
242.04       9.66800e-02    3.29300e-06    2.61862e-06    3.96738e-06    1      160.60
234.04       1.17000e-01    2.17000e-06    1.70652e-06    2.63348e-06    1      157.43
226.28       1.41600e-01    1.41600e-06    1.10055e-06    1.73145e-06    1      153.04
218.82       1.71300e-01    9.40700e-07    7.23789e-07    1.15761e-06    1      147.36
211.70       2.07300e-01    6.56900e-07    5.00794e-07    8.13006e-07    1      140.58
204.96       2.50800e-01    4.89100e-07    3.70343e-07    6.07857e-07    1      133.12
198.62       3.03500e-01    3.95100e-07    2.97611e-07    4.92589e-07    1      125.61
