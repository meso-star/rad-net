#HCN volume mixing ratio profile (from nadir data)
#Latitude              :   35.2 N (FP3)
#Longitude             :  210.8 W (FP3)
#Observation date      :  2015-01-12 (T108 flyby)
#Solar longitude       :   63.8�
#Emission angle        :   24.5�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
254.17       2.07300e-01    1.00400e-06    8.55064e-07    1.15294e-06    1      178.73
245.13       2.50800e-01    1.05100e-06    8.96786e-07    1.20521e-06    1      178.00
236.20       3.03500e-01    1.08400e-06    9.24896e-07    1.24310e-06    1      176.91
227.38       3.67200e-01    1.07600e-06    9.17641e-07    1.23436e-06    1      175.52
218.69       4.44300e-01    1.01000e-06    8.63042e-07    1.15696e-06    1      173.89
210.14       5.37600e-01    8.92700e-07    7.62846e-07    1.02255e-06    1      172.07
201.74       6.50600e-01    7.46500e-07    6.38074e-07    8.54926e-07    1      170.14
193.48       7.87200e-01    5.99300e-07    5.12397e-07    6.86203e-07    1      168.11
185.37       9.52500e-01    4.70800e-07    4.02589e-07    5.39011e-07    1      166.02
