#HCN volume mixing ratio profile (from limb data)
#Latitude              :   84.1 S (FP3)
#Longitude             :   74.8 W (FP3)
#Observation date      :  2015-09-29 (T113 flyby)
#Solar longitude       :   71.7�
#Local time            :  12:37
#Solar zenith angle    :  111�
#Vertical resolution   :  29 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
531.62       2.16700e-04    1.63000e-05    1.00392e-05    2.25608e-05    1      165.28
521.49       2.62300e-04    1.55400e-05    9.63316e-06    2.14468e-05    1      166.43
511.34       3.17300e-04    1.46100e-05    9.10399e-06    2.01160e-05    1      167.76
501.18       3.84000e-04    1.35800e-05    8.55988e-06    1.86001e-05    1      169.26
490.99       4.64600e-04    1.26400e-05    8.06153e-06    1.72185e-05    1      170.91
480.77       5.62200e-04    1.26200e-05    8.12341e-06    1.71166e-05    1      172.68
470.51       6.80300e-04    1.32400e-05    8.67873e-06    1.78013e-05    1      174.47
460.21       8.23200e-04    1.42900e-05    9.48505e-06    1.90949e-05    1      176.20
449.89       9.96100e-04    1.54600e-05    1.04460e-05    2.04740e-05    1      177.76
439.56       1.20500e-03    1.64800e-05    1.13820e-05    2.15780e-05    1      179.02
429.23       1.45800e-03    1.72200e-05    1.21281e-05    2.23119e-05    1      179.92
418.94       1.76500e-03    1.76100e-05    1.26173e-05    2.26027e-05    1      180.40
408.70       2.13500e-03    1.77800e-05    1.29733e-05    2.25867e-05    1      180.48
398.53       2.58400e-03    1.79800e-05    1.32785e-05    2.26815e-05    1      180.16
388.47       3.12700e-03    1.83000e-05    1.36960e-05    2.29040e-05    1      179.54
378.50       3.78300e-03    1.89100e-05    1.42723e-05    2.35477e-05    1      178.79
368.65       4.57800e-03    1.96600e-05    1.49049e-05    2.44151e-05    1      178.03
358.90       5.53900e-03    2.03900e-05    1.55183e-05    2.52617e-05    1      177.29
349.26       6.70300e-03    2.08500e-05    1.58772e-05    2.58229e-05    1      176.47
339.74       8.11000e-03    2.08500e-05    1.59145e-05    2.57855e-05    1      175.46
330.33       9.81400e-03    2.03800e-05    1.55463e-05    2.52137e-05    1      174.22
321.06       1.18700e-02    1.94800e-05    1.49220e-05    2.40380e-05    1      172.82
311.92       1.43700e-02    1.85900e-05    1.43068e-05    2.28732e-05    1      171.42
302.90       1.73900e-02    1.76700e-05    1.36262e-05    2.17138e-05    1      170.23
293.99       2.10400e-02    1.67400e-05    1.29729e-05    2.05071e-05    1      169.50
285.16       2.54600e-02    1.58000e-05    1.22367e-05    1.93633e-05    1      169.41
276.37       3.08000e-02    1.44600e-05    1.11244e-05    1.77956e-05    1      169.85
267.61       3.72700e-02    1.26700e-05    9.69939e-06    1.56406e-05    1      170.44
258.88       4.51000e-02    1.03800e-05    7.84737e-06    1.29126e-05    1      170.53
250.22       5.45700e-02    7.83400e-06    5.79701e-06    9.87099e-06    1      170.04
241.66       6.60300e-02    5.46900e-06    3.94653e-06    6.99147e-06    1      168.59
233.26       7.99000e-02    3.59000e-06    2.50967e-06    4.67033e-06    1      165.82
225.08       9.66800e-02    2.26700e-06    1.53511e-06    2.99889e-06    1      161.69
217.19       1.17000e-01    1.42500e-06    9.32579e-07    1.91742e-06    1      156.11
209.64       1.41600e-01    9.16400e-07    5.80907e-07    1.25189e-06    1      149.41
202.49       1.71300e-01    6.15000e-07    3.78963e-07    8.51037e-07    1      141.91
195.74       2.07300e-01    4.28800e-07    2.58641e-07    5.98959e-07    1      134.08
189.40       2.50800e-01    3.06000e-07    1.81478e-07    4.30523e-07    1      126.44
183.44       3.03500e-01    2.21100e-07    1.29839e-07    3.12361e-07    1      119.51
177.82       3.67200e-01    4.36900e-08    2.54141e-08    6.19659e-08    1      113.64
