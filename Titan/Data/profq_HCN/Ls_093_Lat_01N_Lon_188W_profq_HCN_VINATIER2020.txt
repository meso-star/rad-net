#HCN volume mixing ratio profile (from limb data)
#Latitude              :    1.2 N (FP3)
#Longitude             :  187.6 W (FP3)
#Observation date      : 2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  22:35
#Solar zenith angle    :  147�
#Vertical resolution   :  40 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
524.33       9.37200e-04    1.97500e-06    1.07189e-06    2.87811e-06    1      160.35
513.80       1.15100e-03    1.69200e-06    9.20960e-07    2.46304e-06    1      161.27
503.28       1.41300e-03    1.53700e-06    8.41224e-07    2.23278e-06    1      162.17
492.78       1.73500e-03    1.49000e-06    8.16818e-07    2.16318e-06    1      163.02
482.29       2.13000e-03    1.52500e-06    8.47222e-07    2.20278e-06    1      163.78
471.83       2.61500e-03    1.63700e-06    9.17005e-07    2.35700e-06    1      164.42
461.41       3.21100e-03    1.79200e-06    1.01787e-06    2.56613e-06    1      164.96
451.03       3.94200e-03    1.94900e-06    1.12066e-06    2.77734e-06    1      165.38
440.69       4.84000e-03    2.04800e-06    1.19538e-06    2.90062e-06    1      165.68
430.41       5.94300e-03    2.03100e-06    1.20346e-06    2.85854e-06    1      165.98
420.17       7.29700e-03    1.87700e-06    1.13159e-06    2.62241e-06    1      166.63
409.93       8.95900e-03    1.61900e-06    9.94164e-07    2.24384e-06    1      167.99
399.67       1.10000e-02    1.32700e-06    8.27379e-07    1.82662e-06    1      169.82
389.37       1.35100e-02    1.06200e-06    6.74765e-07    1.44923e-06    1      171.54
379.06       1.65800e-02    8.63700e-07    5.56554e-07    1.17085e-06    1      172.79
368.76       2.03600e-02    7.38900e-07    4.83459e-07    9.94341e-07    1      173.48
358.50       2.50000e-02    6.82300e-07    4.53149e-07    9.11451e-07    1      173.79
348.30       3.06900e-02    6.87600e-07    4.64718e-07    9.10482e-07    1      174.04
338.15       3.76900e-02    7.49400e-07    5.16718e-07    9.82082e-07    1      174.41
328.04       4.62700e-02    8.58300e-07    6.05191e-07    1.11141e-06    1      174.82
317.98       5.68100e-02    9.94500e-07    7.19194e-07    1.26981e-06    1      175.61
307.93       6.97500e-02    1.12600e-06    8.38336e-07    1.41366e-06    1      176.53
297.91       8.56400e-02    1.21800e-06    9.28431e-07    1.50757e-06    1      177.20
287.93       1.05200e-01    1.24900e-06    9.73274e-07    1.52473e-06    1      177.42
278.02       1.29100e-01    1.22800e-06    9.74997e-07    1.48100e-06    1      177.09
268.21       1.58500e-01    1.18800e-06    9.55534e-07    1.42047e-06    1      176.42
258.51       1.94600e-01    1.15800e-06    9.39263e-07    1.37674e-06    1      175.61
248.92       2.39000e-01    1.10700e-06    9.06188e-07    1.30781e-06    1      174.84
239.43       2.93400e-01    1.03700e-06    8.55344e-07    1.21866e-06    1      174.13
230.05       3.60300e-01    9.67500e-07    8.05537e-07    1.12946e-06    1      173.24
220.79       4.42300e-01    9.06500e-07    7.56827e-07    1.05617e-06    1      171.99
211.66       5.43100e-01    8.46900e-07    7.06809e-07    9.86991e-07    1      170.42
202.67       6.66800e-01    7.79700e-07    6.50595e-07    9.08805e-07    1      169.03
193.81       8.18700e-01    6.96500e-07    5.82794e-07    8.10206e-07    1      167.67
185.08       1.00500e+00    5.97900e-07    5.02623e-07    6.93177e-07    1      166.26
176.48       1.23400e+00    4.79200e-07    4.02803e-07    5.55597e-07    1      164.83
168.02       1.51500e+00    3.47000e-07    2.89092e-07    4.04908e-07    1      163.06
159.70       1.86100e+00    2.36000e-07    1.93339e-07    2.78661e-07    1      160.95
151.54       2.28500e+00    1.59000e-07    1.27375e-07    1.90625e-07    1      158.88
143.55       2.80500e+00    1.11600e-07    8.79418e-08    1.35258e-07    1      156.55
135.73       3.44400e+00    8.28600e-08    6.38060e-08    1.01914e-07    1      153.64
128.10       4.22900e+00    6.50300e-08    4.87491e-08    8.13109e-08    1      150.44
120.69       5.19200e+00    5.36600e-08    3.88889e-08    6.84311e-08    1      147.07
