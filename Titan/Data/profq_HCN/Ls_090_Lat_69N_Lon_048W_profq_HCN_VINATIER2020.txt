#HCN volume mixing ratio profile (from nadir data)
#Latitude              :   69.2 N (FP3)
#Longitude             :   48.3 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   18.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
287.21       9.66800e-02    4.99800e-06    4.13240e-06    5.86360e-06    1      182.27
277.77       1.17000e-01    5.10700e-06    4.26147e-06    5.95253e-06    1      181.47
268.45       1.41600e-01    5.05300e-06    4.25213e-06    5.85387e-06    1      180.38
259.25       1.71300e-01    4.80600e-06    4.06781e-06    5.54419e-06    1      178.93
250.19       2.07300e-01    4.37700e-06    3.71832e-06    5.03568e-06    1      177.22
241.28       2.50800e-01    3.80300e-06    3.23302e-06    4.37298e-06    1      175.32
232.52       3.03500e-01    3.15000e-06    2.67467e-06    3.62533e-06    1      173.41
223.92       3.67200e-01    2.49000e-06    2.10861e-06    2.87139e-06    1      171.43
215.46       4.44300e-01    1.88800e-06    1.59129e-06    2.18471e-06    1      169.36
207.16       5.37600e-01    1.37900e-06    1.15591e-06    1.60209e-06    1      167.09
199.02       6.50600e-01    9.77800e-07    8.15271e-07    1.14033e-06    1      164.96
191.04       7.87200e-01    6.77900e-07    5.62204e-07    7.93596e-07    1      162.54
183.23       9.52500e-01    4.63800e-07    3.82589e-07    5.45011e-07    1      159.70
