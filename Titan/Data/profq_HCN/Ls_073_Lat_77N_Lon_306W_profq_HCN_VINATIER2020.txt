#HCN volume mixing ratio profile (from limb data)
#Latitude              :   76.8 N (FP3)
#Longitude             :  305.5 W (FP3)
#Observation date      : 2015-11-213 (T114 flyby)
#Solar longitude       :   73.1�
#Local time            :  17:43
#Solar zenith angle    :   64�
#Vertical resolution   :  46 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
522.40       9.96100e-04    2.67900e-07    1.62049e-07    3.73751e-07    1      165.53
512.31       1.20500e-03    2.52200e-07    1.53197e-07    3.51203e-07    1      166.61
502.22       1.45800e-03    2.38700e-07    1.45608e-07    3.31792e-07    1      167.60
492.15       1.76500e-03    2.26600e-07    1.38798e-07    3.14402e-07    1      168.45
482.09       2.13500e-03    2.14900e-07    1.32373e-07    2.97427e-07    1      169.13
472.07       2.58400e-03    2.03200e-07    1.25948e-07    2.80452e-07    1      169.54
462.10       3.12700e-03    1.91300e-07    1.19043e-07    2.63557e-07    1      169.83
452.17       3.78300e-03    1.79600e-07    1.12616e-07    2.46584e-07    1      170.15
442.28       4.57800e-03    1.68900e-07    1.06297e-07    2.31503e-07    1      170.65
432.42       5.53900e-03    1.60200e-07    1.01618e-07    2.18782e-07    1      171.37
422.58       6.70300e-03    1.54800e-07    9.89777e-08    2.10622e-07    1      172.30
412.75       8.11000e-03    1.54200e-07    9.92513e-08    2.09149e-07    1      173.34
402.92       9.81400e-03    1.60000e-07    1.03977e-07    2.16023e-07    1      174.37
393.11       1.18700e-02    1.74600e-07    1.14329e-07    2.34871e-07    1      175.27
383.31       1.43700e-02    2.01300e-07    1.33476e-07    2.69124e-07    1      176.00
373.54       1.73900e-02    2.45200e-07    1.65456e-07    3.24944e-07    1      176.59
363.80       2.10400e-02    3.13700e-07    2.15942e-07    4.11458e-07    1      177.15
354.10       2.54600e-02    4.16900e-07    2.94273e-07    5.39527e-07    1      177.79
344.42       3.08000e-02    5.66300e-07    4.10769e-07    7.21831e-07    1      178.56
334.76       3.72700e-02    7.71900e-07    5.76640e-07    9.67160e-07    1      179.34
325.13       4.51000e-02    1.03600e-06    7.96874e-07    1.27513e-06    1      180.08
315.52       5.45700e-02    1.34900e-06    1.06172e-06    1.63628e-06    1      180.91
305.92       6.60300e-02    1.68200e-06    1.34785e-06    2.01615e-06    1      181.84
296.34       7.99000e-02    2.00100e-06    1.62746e-06    2.37454e-06    1      182.81
286.77       9.66800e-02    2.27300e-06    1.87202e-06    2.67398e-06    1      183.68
277.24       1.17000e-01    2.48400e-06    2.07687e-06    2.89113e-06    1      184.25
267.75       1.41600e-01    2.64200e-06    2.23927e-06    3.04473e-06    1      184.21
258.34       1.71300e-01    2.76900e-06    2.37100e-06    3.16700e-06    1      183.44
249.05       2.07300e-01    2.88900e-06    2.48284e-06    3.29516e-06    1      181.91
239.92       2.50800e-01    3.02000e-06    2.59105e-06    3.44895e-06    1      179.69
230.98       3.03500e-01    3.16500e-06    2.70835e-06    3.62165e-06    1      176.89
222.24       3.67200e-01    3.30600e-06    2.83070e-06    3.78130e-06    1      173.67
213.72       4.44300e-01    3.40300e-06    2.93006e-06    3.87594e-06    1      170.20
205.42       5.37600e-01    3.40400e-06    2.94643e-06    3.86157e-06    1      166.78
197.32       6.50600e-01    3.25900e-06    2.81613e-06    3.70187e-06    1      163.69
189.42       7.87200e-01    2.94500e-06    2.52325e-06    3.36675e-06    1      160.84
181.70       9.52500e-01    2.48900e-06    2.10770e-06    2.87030e-06    1      158.24
174.14       1.15300e+00    1.95900e-06    1.64604e-06    2.27196e-06    1      155.95
166.72       1.39500e+00    1.44000e-06    1.20622e-06    1.67378e-06    1      153.99
159.42       1.68800e+00    9.98600e-07    8.36580e-07    1.16062e-06    1      152.30
152.24       2.04200e+00    6.63600e-07    5.53735e-07    7.73465e-07    1      150.78
145.17       2.47100e+00    4.31900e-07    3.55488e-07    5.08312e-07    1      149.26
138.22       2.99000e+00    2.82500e-07    2.26658e-07    3.38342e-07    1      147.50
131.38       3.61800e+00    1.90800e-07    1.48180e-07    2.33420e-07    1      145.36
124.70       4.37700e+00    1.36500e-07    1.02246e-07    1.70754e-07    1      142.80
