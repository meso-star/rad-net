#Temperature profile
#Latitude              :   63.7 N (FP4)
#Longitude             :  255.6 W (FP4)
#Observation date      :  2015-05-09 (T111 flyby)
#Solar longitude       :   67.4�
#Local time            :  01:24
#Solar zenith angle    :   90�
#Vertical resolution   :  39 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) Temperature(K) T_min(K) T_max(K) sigma
670.94       5.37900e-05    157.91         155.14   160.68   1
659.98       6.55600e-05    157.74         154.97   160.51   1
649.10       7.99100e-05    157.59         154.83   160.34   1
638.31       9.74000e-05    157.45         154.70   160.21   1
627.61       1.18700e-04    157.35         154.60   160.09   1
616.96       1.44700e-04    157.27         154.53   160.01   1
606.40       1.76400e-04    157.23         154.50   159.96   1
595.90       2.15000e-04    157.34         154.63   160.06   1
585.48       2.62000e-04    157.58         154.87   160.28   1
575.08       3.19400e-04    157.99         155.29   160.68   1
564.73       3.89300e-04    158.45         155.78   161.11   1
554.42       4.74500e-04    158.98         156.34   161.62   1
544.14       5.78400e-04    159.54         156.92   162.15   1
533.89       7.05000e-04    160.11         157.52   162.70   1
523.67       8.59300e-04    160.70         158.15   163.25   1
513.50       1.04700e-03    161.29         158.77   163.81   1
503.30       1.27700e-03    161.85         159.37   164.33   1
493.20       1.55600e-03    162.33         159.87   164.79   1
483.10       1.89700e-03    162.69         160.27   165.10   1
473.07       2.31200e-03    162.90         160.52   165.28   1
463.09       2.81800e-03    162.91         160.57   165.25   1
453.18       3.43500e-03    162.85         160.56   165.14   1
443.32       4.18700e-03    163.18         160.95   165.42   1
433.49       5.10300e-03    164.46         162.30   166.62   1
423.65       6.22000e-03    165.21         163.14   167.27   1
413.83       7.58100e-03    166.33         164.39   168.26   1
403.99       9.24100e-03    167.67         165.89   169.45   1
394.16       1.12600e-02    169.18         167.57   170.79   1
384.27       1.37300e-02    170.60         169.14   172.06   1
374.39       1.67300e-02    172.01         170.67   173.35   1
364.47       2.04000e-02    173.42         172.15   174.68   1
354.56       2.48600e-02    174.96         173.82   176.10   1
344.62       3.03000e-02    176.58         175.58   177.58   1
334.65       3.69400e-02    178.04         177.16   178.93   1
324.70       4.50200e-02    178.83         177.97   179.69   1
314.77       5.48700e-02    179.51         178.65   180.37   1
304.88       6.68800e-02    180.05         179.22   180.89   1
295.02       8.15200e-02    180.43         179.68   181.17   1
285.21       9.93700e-02    180.76         180.08   181.43   1
275.47       1.21100e-01    180.81         180.16   181.46   1
265.80       1.47600e-01    180.51         179.87   181.14   1
256.22       1.79900e-01    179.61         179.07   180.16   1
246.77       2.19300e-01    177.97         177.49   178.45   1
237.49       2.67300e-01    175.64         175.09   176.19   1
228.39       3.25900e-01    172.86         172.20   173.52   1
219.52       3.97200e-01    169.86         169.19   170.54   1
210.85       4.84100e-01    166.98         166.42   167.54   1
202.37       5.90100e-01    164.41         163.94   164.88   1
194.07       7.19200e-01    162.21         161.61   162.81   1
185.91       8.76700e-01    160.28         159.50   161.06   1
177.88       1.06900e+00    158.54         157.71   159.37   1
170.03       1.30200e+00    156.74         156.03   157.46   1
162.28       1.58800e+00    154.49         153.95   155.02   1
154.72       1.93500e+00    151.93         151.36   152.51   1
147.30       2.35900e+00    149.54         148.71   150.37   1
140.06       2.87500e+00    146.92         145.88   147.96   1
132.99       3.50400e+00    143.87         142.76   144.97   1
126.10       4.27100e+00    141.21         140.14   142.27   1
119.36       5.20600e+00    138.71         137.63   139.79   1
112.78       6.34500e+00    136.24         134.96   137.51   1
107.68       7.41900e+00    134.37         132.84   135.91   1
105.84       7.85600e+00    133.80         133.80   133.80   0
100.69       9.24000e+00    131.00         131.00   131.00   0
 98.23       9.99900e+00    129.30         129.30   129.30   0
 94.33       1.13600e+01    127.46         127.46   127.46   0
 88.63       1.37400e+01    124.72         124.72   124.72   0
 83.06       1.66300e+01    122.06         122.06   122.06   0
 77.64       2.01200e+01    119.27         119.27   119.27   0
 72.57       2.43400e+01    108.25         108.25   108.25   0
 68.04       2.94600e+01     94.90          94.90    94.90   0
 64.97       3.41200e+01     84.96          84.96    84.96   0
 63.96       3.59300e+01     81.96          81.96    81.96   0
 60.66       4.31300e+01     73.67          73.67    73.67   0
 57.47       5.21800e+01     70.82          70.82    70.82   0
 54.38       6.31400e+01     69.56          69.56    69.56   0
 51.32       7.64100e+01     69.19          69.19    69.19   0
 48.28       9.24500e+01     69.26          69.26    69.26   0
 45.24       1.11900e+02     69.45          69.45    69.45   0
 42.20       1.35400e+02     69.68          69.68    69.68   0
 39.15       1.63800e+02     70.15          70.15    70.15   0
 36.09       1.98200e+02     70.79          70.79    70.79   0
 33.00       2.39800e+02     71.75          71.75    71.75   0
 29.86       2.90200e+02     72.88          72.88    72.88   0
 26.68       3.51100e+02     74.37          74.37    74.37   0
 23.44       4.24900e+02     76.09          76.09    76.09   0
 20.12       5.14100e+02     78.10          78.10    78.10   0
 16.72       6.22100e+02     80.30          80.30    80.30   0
 13.22       7.52800e+02     82.69          82.69    82.69   0
  9.61       9.10800e+02     85.46          85.46    85.46   0
  5.88       1.10200e+03     88.64          88.64    88.64   0
  1.99       1.33400e+03     92.08          92.08    92.08   0
  0.00       1.46700e+03     94.51          94.51    94.51   0
