#Temperature profile (from nadir data)
#Latitude              :   11.4 N (FP4)
#Longitude             :   26.5 W (FP4)
#Observation date      :  2017-06-8 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   43.4�
#          
#                   
#Reference             :Vinatier et al. (2020), A&A ; https://d
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) Temperature(K) T_min(K) T_max(K) sigma
445.45       4.57800e-03    162.58         161.67   163.49   1
436.00       5.53900e-03    163.83         162.92   164.74   1
426.52       6.70300e-03    165.41         164.51   166.31   1
417.01       8.11000e-03    167.20         166.30   168.09   1
407.46       9.81400e-03    168.90         168.00   169.80   1
397.90       1.18700e-02    170.50         169.62   171.39   1
388.27       1.43700e-02    171.74         170.85   172.62   1
378.66       1.73900e-02    172.77         171.90   173.64   1
369.07       2.10400e-02    173.61         172.74   174.49   1
359.49       2.54600e-02    174.50         173.64   175.36   1
349.94       3.08000e-02    175.34         174.50   176.19   1
340.39       3.72700e-02    176.06         175.23   176.89   1
330.89       4.51000e-02    176.12         175.30   176.94   1
321.45       5.45700e-02    176.14         175.33   176.94   1
312.07       6.60300e-02    176.07         175.29   176.85   1
302.75       7.99000e-02    175.84         175.08   176.59   1
293.51       9.66800e-02    175.62         174.90   176.33   1
284.34       1.17000e-01    175.29         174.61   175.96   1
275.24       1.41600e-01    174.98         174.34   175.61   1
266.23       1.71300e-01    174.64         174.04   175.24   1
257.28       2.07300e-01    174.32         173.78   174.87   1
248.42       2.50800e-01    174.08         173.57   174.59   1
239.60       3.03500e-01    174.00         173.53   174.47   1
230.86       3.67200e-01    173.94         173.51   174.37   1
222.17       4.44300e-01    173.77         173.37   174.17   1
213.55       5.37600e-01    173.26         172.85   173.66   1
205.00       6.50600e-01    172.63         172.24   173.02   1
196.56       7.87200e-01    171.40         171.01   171.79   1
188.25       9.52500e-01    169.37         168.96   169.77   1
180.08       1.15300e+00    166.80         166.40   167.21   1
172.12       1.39500e+00    163.84         163.43   164.24   1
164.33       1.68800e+00    160.86         160.46   161.26   1
156.74       2.04200e+00    158.12         157.73   158.51   1
149.30       2.47100e+00    155.79         155.40   156.18   1
142.00       2.99000e+00    153.62         153.25   154.00   1
134.84       3.61800e+00    151.96         151.57   152.35   1
127.79       4.37700e+00    150.38         149.97   150.80   1
120.85       5.29700e+00    148.55         148.10   149.01   1
114.05       6.40900e+00    146.06         145.55   146.56   1
107.42       7.75500e+00    142.74         142.18   143.30   1
100.99       9.38400e+00    138.59         137.97   139.21   1
 94.77       1.13600e+01    133.87         133.18   134.56   1
