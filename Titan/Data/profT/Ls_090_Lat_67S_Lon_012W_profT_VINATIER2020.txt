#Temperature profile
#Latitude              :   67.0 S (FP4)
#Longitude             :   12.4 W (FP4)
#Observation date      :  2017-05-25 (S99/275TI flyby)
#Solar longitude       :   90.0�
#Local time            :  12:25
#Solar zenith angle    :   94�
#Vertical resolution   :  55 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) Temperature(K) T_min(K) T_max(K) sigma
654.55       5.70700e-05    156.02         153.84   158.21   1
644.21       6.90500e-05    156.53         154.35   158.70   1
633.88       8.35600e-05    157.01         154.85   159.17   1
623.61       1.01100e-04    157.51         155.36   159.66   1
613.37       1.22300e-04    158.02         155.88   160.17   1
603.15       1.48000e-04    158.52         156.40   160.64   1
592.96       1.79100e-04    159.01         156.91   161.11   1
582.82       2.16700e-04    159.51         157.43   161.60   1
572.69       2.62300e-04    160.00         157.94   162.06   1
562.62       3.17300e-04    160.54         158.51   162.57   1
552.56       3.84000e-04    161.08         159.09   163.06   1
542.54       4.64600e-04    161.65         159.71   163.60   1
532.55       5.62200e-04    162.25         160.34   164.16   1
522.58       6.80300e-04    162.88         161.01   164.75   1
512.63       8.23200e-04    163.56         161.73   165.39   1
502.71       9.96100e-04    164.27         162.48   166.06   1
492.82       1.20500e-03    165.07         163.31   166.83   1
482.93       1.45800e-03    165.92         164.21   167.64   1
473.03       1.76500e-03    166.83         165.15   168.52   1
463.17       2.13500e-03    167.92         166.28   169.57   1
453.28       2.58400e-03    169.12         167.53   170.71   1
443.39       3.12700e-03    170.48         168.96   172.00   1
433.49       3.78300e-03    172.28         170.85   173.72   1
423.52       4.57800e-03    174.46         173.13   175.78   1
413.49       5.53900e-03    177.13         175.94   178.33   1
403.36       6.70300e-03    179.91         178.85   180.97   1
393.16       8.11000e-03    182.59         181.64   183.53   1
382.89       9.81400e-03    184.79         183.91   185.66   1
372.60       1.18700e-02    186.44         185.61   187.27   1
362.27       1.43700e-02    187.26         186.46   188.07   1
352.00       1.73900e-02    187.44         186.66   188.22   1
341.83       2.10400e-02    187.05         186.31   187.79   1
331.74       2.54600e-02    186.38         185.68   187.09   1
321.78       3.08000e-02    185.46         184.77   186.15   1
311.93       3.72700e-02    184.28         183.58   184.99   1
302.23       4.51000e-02    182.41         181.67   183.14   1
292.69       5.45700e-02    180.48         179.72   181.24   1
283.32       6.60300e-02    178.49         177.73   179.25   1
274.12       7.99000e-02    176.34         175.62   177.06   1
265.08       9.66800e-02    174.16         173.48   174.83   1
256.22       1.17000e-01    171.73         171.07   172.40   1
247.54       1.41600e-01    169.08         168.37   169.79   1
239.07       1.71300e-01    166.06         165.29   166.83   1
230.80       2.07300e-01    162.68         161.84   163.51   1
222.76       2.50800e-01    158.98         158.13   159.84   1
214.95       3.03500e-01    155.20         154.35   156.06   1
207.37       3.67200e-01    151.40         150.56   152.24   1
200.02       4.44300e-01    147.74         146.87   148.60   1
192.88       5.37600e-01    144.31         143.38   145.23   1
185.91       6.50600e-01    141.62         140.59   142.65   1
179.11       7.87200e-01    139.31         138.15   140.46   1
172.45       9.52500e-01    137.19         135.95   138.44   1
165.90       1.15300e+00    135.38         134.07   136.69   1
159.48       1.39500e+00    133.70         132.36   135.03   1
153.16       1.68800e+00    132.21         130.87   133.55   1
146.95       2.04200e+00    130.88         129.54   132.22   1
140.81       2.47100e+00    129.70         128.33   131.07   1
134.76       2.99000e+00    128.44         126.99   129.88   1
128.79       3.61800e+00    127.55         125.99   129.11   1
124.47       4.15800e+00    127.08         125.43   128.73   1
123.11       4.34500e+00    126.91         126.91   126.91   0
122.86       4.38000e+00    127.16         127.16   127.16   0
117.00       5.29700e+00    127.00         127.00   127.00   0
111.15       6.40900e+00    126.80         126.80   126.80   0
105.34       7.75500e+00    126.50         126.50   126.50   0
 99.57       9.38400e+00    126.19         126.19   126.19   0
 93.86       1.13600e+01    124.73         124.73   124.73   0
 88.27       1.37400e+01    122.79         122.79   122.79   0
 82.79       1.66300e+01    119.96         119.96   119.96   0
 77.55       2.01200e+01    113.67         113.67   113.67   0
 72.61       2.43400e+01    107.65         107.65   107.65   0
 68.10       2.94600e+01     94.58          94.58    94.58   0
 64.19       3.56400e+01     82.24          82.24    82.24   0
 60.69       4.31300e+01     75.44          75.44    75.44   0
 57.43       5.21800e+01     72.51          72.51    72.51   0
 54.26       6.31400e+01     71.07          71.07    71.07   0
 51.14       7.64100e+01     70.47          70.47    70.47   0
 48.06       9.24500e+01     70.30          70.30    70.30   0
 44.97       1.11900e+02     70.24          70.24    70.24   0
 41.90       1.35400e+02     70.24          70.24    70.24   0
 38.84       1.63800e+02     70.50          70.50    70.50   0
 35.76       1.98200e+02     70.94          70.94    70.94   0
 32.67       2.39800e+02     71.71          71.71    71.71   0
 29.54       2.90200e+02     72.66          72.66    72.66   0
 26.38       3.51100e+02     73.97          73.97    73.97   0
 23.15       4.24900e+02     75.51          75.51    75.51   0
 19.87       5.14100e+02     77.34          77.34    77.34   0
 16.50       6.22100e+02     79.39          79.39    79.39   0
 13.05       7.52800e+02     81.64          81.64    81.64   0
  9.48       9.10800e+02     84.31          84.31    84.31   0
  5.80       1.10200e+03     87.43          87.43    87.43   0
  1.97       1.33400e+03     90.88          90.88    90.88   0
  0.00       1.46700e+03     93.30          93.30    93.30   0
