#Temperature profile
#Latitude              :   17.8 S (FP4)
#Longitude             :   34.1 W (FP4)
#Observation date      :  2017-02-02 (S98/259TI flyby)
#Solar longitude       :   86.6�
#Local time            :  11:35
#Solar zenith angle    :   45�
#Vertical resolution   :  64 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) Temperature(K) T_min(K) T_max(K) sigma
679.21       5.37900e-05    158.59         156.37   160.82   1
668.13       6.55600e-05    159.05         156.85   161.26   1
657.08       7.99100e-05    159.48         157.29   161.68   1
646.09       9.74000e-05    159.93         157.74   162.11   1
635.14       1.18700e-04    160.41         158.25   162.58   1
624.22       1.44700e-04    160.93         158.78   163.08   1
613.34       1.76400e-04    161.50         159.37   163.64   1
602.50       2.15000e-04    162.15         160.03   164.27   1
591.70       2.62000e-04    162.81         160.71   164.91   1
580.90       3.19400e-04    163.54         161.46   165.62   1
570.15       3.89300e-04    164.25         162.19   166.31   1
559.42       4.74500e-04    164.92         162.88   166.96   1
548.71       5.78400e-04    165.50         163.48   167.53   1
538.06       7.05000e-04    165.96         163.96   167.97   1
527.45       8.59300e-04    166.27         164.28   168.27   1
516.91       1.04700e-03    166.41         164.43   168.38   1
506.39       1.27700e-03    166.37         164.40   168.33   1
496.01       1.55600e-03    166.12         164.18   168.07   1
485.68       1.89700e-03    165.71         163.78   167.64   1
475.47       2.31200e-03    165.18         163.24   167.11   1
465.36       2.81800e-03    164.54         162.62   166.46   1
455.35       3.43500e-03    164.00         162.09   165.90   1
445.42       4.18700e-03    163.78         161.89   165.66   1
435.57       5.10300e-03    164.04         162.21   165.87   1
425.75       6.22000e-03    164.77         163.00   166.53   1
415.94       7.58100e-03    165.80         164.11   167.50   1
406.12       9.24100e-03    166.97         165.36   168.58   1
396.32       1.12600e-02    168.18         166.65   169.71   1
386.48       1.37300e-02    169.28         167.81   170.74   1
376.68       1.67300e-02    170.29         168.88   171.69   1
366.86       2.04000e-02    171.23         169.88   172.59   1
357.07       2.48600e-02    172.26         170.97   173.55   1
347.28       3.03000e-02    173.27         172.06   174.47   1
337.50       3.69400e-02    174.09         172.99   175.19   1
327.77       4.50200e-02    174.15         173.14   175.15   1
318.10       5.48700e-02    173.98         173.03   174.94   1
308.51       6.68800e-02    173.61         172.66   174.55   1
299.01       8.15200e-02    173.01         172.06   173.97   1
289.60       9.93700e-02    172.43         171.49   173.38   1
280.30       1.21100e-01    171.83         170.95   172.72   1
271.08       1.47600e-01    171.38         170.61   172.15   1
261.94       1.79900e-01    171.08         170.48   171.68   1
252.86       2.19300e-01    170.95         170.49   171.41   1
243.85       2.67300e-01    171.01         170.58   171.43   1
234.87       3.25900e-01    171.16         170.64   171.68   1
225.96       3.97200e-01    171.17         170.53   171.82   1
217.12       4.84100e-01    170.79         170.07   171.50   1
208.36       5.90100e-01    169.96         169.25   170.67   1
199.72       7.19200e-01    168.61         167.99   169.23   1
191.21       8.76700e-01    166.46         165.99   166.94   1
182.87       1.06900e+00    163.71         163.36   164.07   1
174.76       1.30200e+00    160.72         160.35   161.08   1
166.80       1.58800e+00    157.88         157.38   158.38   1
159.04       1.93500e+00    155.56         154.91   156.20   1
151.40       2.35900e+00    153.88         153.15   154.61   1
143.89       2.87500e+00    152.59         151.86   153.32   1
136.48       3.50400e+00    151.74         151.07   152.40   1
129.14       4.27100e+00    150.91         150.33   151.49   1
121.89       5.20600e+00    149.58         149.03   150.13   1
114.77       6.34500e+00    147.29         146.64   147.95   1
107.83       7.73400e+00    143.85         142.99   144.71   1
101.10       9.42700e+00    139.30         138.18   140.41   1
 94.65       1.14900e+01    133.99         132.63   135.34   1
 88.48       1.40100e+01    128.13         126.56   129.70   1
 84.98       1.57500e+01    124.31         122.62   125.99   1
 84.44       1.60400e+01    123.81         123.81   123.81   0
 83.40       1.66300e+01    120.76         120.76   120.76   0
 80.00       1.87700e+01    117.46         117.46   117.46   0
 79.29       1.92600e+01    116.56         116.56   116.56   0
 73.06       2.43400e+01    110.44         110.44   110.44   0
 68.44       2.94600e+01     96.75          96.75    96.75   0
 64.44       3.56400e+01     83.99          83.99    83.99   0
 60.88       4.31300e+01     76.75          76.75    76.75   0
 57.56       5.21800e+01     73.48          73.48    73.48   0
 54.36       6.31400e+01     71.76          71.76    71.76   0
 51.21       7.64100e+01     70.95          70.95    70.95   0
 48.11       9.24500e+01     70.61          70.61    70.61   0
 45.01       1.11900e+02     70.44          70.44    70.44   0
 41.94       1.35400e+02     70.37          70.37    70.37   0
 38.87       1.63800e+02     70.58          70.58    70.58   0
 35.79       1.98200e+02     71.00          71.00    71.00   0
 32.69       2.39800e+02     71.75          71.75    71.75   0
 29.56       2.90200e+02     72.70          72.70    72.70   0
 26.40       3.51100e+02     74.00          74.00    74.00   0
 23.17       4.24900e+02     75.56          75.56    75.56   0
 19.88       5.14100e+02     77.39          77.39    77.39   0
 16.51       6.22100e+02     79.45          79.45    79.45   0
 13.06       7.52800e+02     81.71          81.71    81.71   0
  9.49       9.10800e+02     84.38          84.38    84.38   0
  5.80       1.10200e+03     87.49          87.49    87.49   0
  1.97       1.33400e+03     90.98          90.98    90.98   0
  0.00       1.46700e+03     93.37          93.37    93.37   0
