#Temperature profile (from nadir data)
#Latitude              :   47.8 N (FP4)
#Longitude             :   41.1 W (FP4)
#Observation date      :  2017-06-8 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :    6.3�
#          
#                   
#Reference             :Vinatier et al. (2020), A&A ; https://d
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) Temperature(K) T_min(K) T_max(K) sigma
442.10       4.57800e-03    162.07         161.16   162.98   1
432.70       5.53900e-03    163.31         162.40   164.22   1
423.27       6.70300e-03    164.88         163.98   165.78   1
413.81       8.11000e-03    166.70         165.80   167.59   1
404.30       9.81400e-03    168.45         167.55   169.35   1
394.78       1.18700e-02    170.15         169.27   171.04   1
385.19       1.43700e-02    171.50         170.62   172.39   1
375.61       1.73900e-02    172.71         171.84   173.58   1
366.04       2.10400e-02    173.79         172.92   174.66   1
356.46       2.54600e-02    174.94         174.09   175.80   1
346.89       3.08000e-02    176.11         175.26   176.95   1
337.32       3.72700e-02    177.16         176.33   177.99   1
327.76       4.51000e-02    177.58         176.76   178.40   1
318.25       5.45700e-02    177.93         177.14   178.72   1
308.79       6.60300e-02    178.15         177.39   178.92   1
299.38       7.99000e-02    178.14         177.40   178.88   1
290.04       9.66800e-02    178.03         177.31   178.74   1
280.77       1.17000e-01    177.67         176.99   178.34   1
271.57       1.41600e-01    177.16         176.53   177.80   1
262.49       1.71300e-01    176.45         175.87   177.04   1
253.48       2.07300e-01    175.59         175.05   176.14   1
244.59       2.50800e-01    174.64         174.14   175.15   1
235.79       3.03500e-01    173.73         173.25   174.21   1
227.11       3.67200e-01    172.75         172.29   173.20   1
218.53       4.44300e-01    171.63         171.18   172.07   1
210.06       5.37600e-01    170.20         169.76   170.64   1
201.70       6.50600e-01    168.76         168.32   169.20   1
193.49       7.87200e-01    166.84         166.39   167.30   1
185.43       9.52500e-01    164.31         163.84   164.78   1
177.53       1.15300e+00    161.46         160.99   161.93   1
169.84       1.39500e+00    158.38         157.90   158.87   1
162.33       1.68800e+00    155.49         155.02   155.96   1
154.99       2.04200e+00    152.96         152.49   153.43   1
147.80       2.47100e+00    150.98         150.52   151.43   1
140.73       2.99000e+00    149.23         148.79   149.68   1
133.76       3.61800e+00    148.02         147.56   148.47   1
126.89       4.37700e+00    146.90         146.43   147.37   1
120.11       5.29700e+00    145.51         145.02   146.01   1
113.44       6.40900e+00    143.41         142.87   143.96   1
106.92       7.75500e+00    140.45         139.85   141.05   1
100.59       9.38400e+00    136.62         135.97   137.27   1
 94.46       1.13600e+01    132.22         131.51   132.94   1
