#HC3N volume mixing ratio profile (from limb data)
#Latitude              :   38.0 S (FP3)
#Longitude             :    6.2 W (FP3)
#Observation date      :  2017-05-25 (S99/275TI flyby)
#Solar longitude       :   90.0�
#Local time            :  12:54
#Solar zenith angle    :   70�
#Vertical resolution   :  62 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 1-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
155.38       2.04200e+00    1.80100e-10    1.24300e-10    2.35900e-10    1      149.96
148.32       2.47100e+00    1.72200e-10    1.19028e-10    2.25372e-10    1      149.00
141.33       2.99000e+00    1.64200e-10    1.13375e-10    2.15025e-10    1      148.46
134.40       3.61800e+00    1.56500e-10    1.08293e-10    2.04707e-10    1      147.89
127.54       4.37700e+00    1.49500e-10    1.03595e-10    1.95405e-10    1      147.31
120.74       5.29700e+00    1.43700e-10    9.94756e-11    1.87924e-10    1      146.37
