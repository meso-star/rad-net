#HC3N volume mixing ratio profile (from limb data)
#Latitude              :   28.5 S (FP3)
#Longitude             :   58.1 W (FP3)
#Observation date      :  2015-09-29 (T113 flyby)
#Solar longitude       :   71.7�
#Local time            :  13:58
#Solar zenith angle    :   63�
#Vertical resolution   :  44 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
393.31       9.81400e-03    1.89500e-08    1.53267e-08    2.25733e-08    1      169.11
383.88       1.18700e-02    1.44500e-08    1.16751e-08    1.72249e-08    1      169.05
374.51       1.43700e-02    1.06700e-08    8.61358e-09    1.27264e-08    1      168.91
365.21       1.73900e-02    7.65600e-09    6.17557e-09    9.13643e-09    1      168.75
355.97       2.10400e-02    5.39700e-09    4.34810e-09    6.44590e-09    1      168.67
346.80       2.54600e-02    3.78400e-09    3.04484e-09    4.52316e-09    1      168.80
337.66       3.08000e-02    2.66700e-09    2.14408e-09    3.18992e-09    1      169.10
328.57       3.72700e-02    1.91000e-09    1.53438e-09    2.28562e-09    1      169.40
319.52       4.51000e-02    1.40300e-09    1.12573e-09    1.68027e-09    1      169.42
310.53       5.45700e-02    1.06300e-09    8.53000e-10    1.27300e-09    1      169.51
301.59       6.60300e-02    8.36300e-10    6.70596e-10    1.00200e-09    1      169.71
292.69       7.99000e-02    6.83600e-10    5.47993e-10    8.19207e-10    1      169.99
283.82       9.66800e-02    5.81900e-10    4.66397e-10    6.97403e-10    1      170.43
274.99       1.17000e-01    5.14200e-10    4.12185e-10    6.16215e-10    1      170.86
266.19       1.41600e-01    4.70500e-10    3.77148e-10    5.63852e-10    1      171.22
257.44       1.71300e-01    4.42100e-10    3.54565e-10    5.29635e-10    1      171.36
248.74       2.07300e-01    4.23300e-10    3.39541e-10    5.07059e-10    1      171.15
240.11       2.50800e-01    4.09300e-10    3.28298e-10    4.90302e-10    1      170.58
231.57       3.03500e-01    3.94600e-10    3.16679e-10    4.72521e-10    1      169.71
223.13       3.67200e-01    3.75600e-10    3.01469e-10    4.49731e-10    1      168.61
214.80       4.44300e-01    3.49600e-10    2.80569e-10    4.18631e-10    1      167.38
206.58       5.37600e-01    3.16900e-10    2.54396e-10    3.79404e-10    1      166.26
198.45       6.50600e-01    2.79000e-10    2.24070e-10    3.33930e-10    1      165.45
190.40       7.87200e-01    2.38900e-10    1.91834e-10    2.85966e-10    1      164.73
182.44       9.52500e-01    2.00000e-10    1.60577e-10    2.39423e-10    1      163.93
174.57       1.15300e+00    1.65200e-10    1.32587e-10    1.97813e-10    1      163.07
166.78       1.39500e+00    1.35600e-10    1.08886e-10    1.62314e-10    1      162.05
159.10       1.68800e+00    1.11900e-10    8.97147e-11    1.34085e-10    1      160.57
151.55       2.04200e+00    9.36500e-11    7.50796e-11    1.12220e-10    1      158.51
144.15       2.47100e+00    8.01700e-11    6.42516e-11    9.60884e-11    1      155.75
136.93       2.99000e+00    7.06300e-11    5.65821e-11    8.46779e-11    1      152.18
129.94       3.61800e+00    6.42100e-11    5.14316e-11    7.69884e-11    1      147.81
124.45       4.22100e+00    6.10900e-11    4.89126e-11    7.32674e-11    1      144.46
