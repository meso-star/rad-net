#HC3N volume mixing ratio profile (from limb data)
#Latitude              :   23.5 S (FP3)
#Longitude             :   39.5 W (FP3)
#Observation date      :  2017-02-02 (S98/259TI flyby)
#Solar longitude       :   86.6�
#Local time            :  11:01
#Solar zenith angle    :   55�
#Vertical resolution   :  63 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
218.11       4.44300e-01    1.75200e-10    9.60085e-11    2.54391e-10    1      167.91
209.82       5.37600e-01    1.56000e-10    8.54915e-11    2.26509e-10    1      167.33
201.62       6.50600e-01    1.39600e-10    7.63934e-11    2.02807e-10    1      166.55
193.52       7.87200e-01    1.25700e-10    6.87474e-11    1.82653e-10    1      165.35
185.53       9.52500e-01    1.13700e-10    6.22634e-11    1.65137e-10    1      163.59
177.69       1.15300e+00    1.03300e-10    5.66180e-11    1.49982e-10    1      161.36
170.00       1.39500e+00    9.42400e-11    5.15590e-11    1.36921e-10    1      158.89
162.48       1.68800e+00    8.61700e-11    4.71037e-11    1.25236e-10    1      156.41
155.10       2.04200e+00    7.89300e-11    4.31615e-11    1.14699e-10    1      154.14
147.87       2.47100e+00    7.24000e-11    3.95513e-11    1.05249e-10    1      152.12
140.77       2.99000e+00    6.65000e-11    3.63253e-11    9.66747e-11    1      150.26
133.79       3.61800e+00    6.12000e-11    3.34218e-11    8.89782e-11    1      148.64
126.91       4.37700e+00    5.64400e-11    3.08151e-11    8.20649e-11    1      147.05
120.15       5.29700e+00    5.21900e-11    2.84762e-11    7.59038e-11    1      145.11
