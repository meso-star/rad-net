#HC3N volume mixing ratio profile (from limb data)
#Latitude              :   61.6 N (FP3)
#Longitude             :  217.0 W (FP3)
#Observation date      : 2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  20:39
#Solar zenith angle    :   85�
#Vertical resolution   :  52 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
148.87       2.28500e+00    1.52400e-10    7.48915e-11    2.29909e-10    1      151.92
141.23       2.80500e+00    1.46800e-10    7.20270e-11    2.21573e-10    1      149.97
133.74       3.44400e+00    1.40600e-10    6.90955e-11    2.12105e-10    1      147.65
126.42       4.22900e+00    1.34400e-10    6.61641e-11    2.02636e-10    1      145.01
119.27       5.19200e+00    1.28400e-10    6.34272e-11    1.93373e-10    1      142.13
