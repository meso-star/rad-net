#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   69.2 N (FP3)
#Longitude             :   48.3 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   18.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
133.06       3.61800e+00    6.21500e-10    3.82938e-10    8.60062e-10    1      145.61
126.31       4.37700e+00    6.36900e-10    3.94740e-10    8.79060e-10    1      144.98
119.63       5.29700e+00    6.45000e-10    4.01216e-10    8.88784e-10    1      144.04
113.04       6.40900e+00    6.44400e-10    4.01208e-10    8.87592e-10    1      142.31
106.58       7.75500e+00    6.34300e-10    3.94136e-10    8.74464e-10    1      139.64
100.30       9.38400e+00    6.15100e-10    3.80576e-10    8.49624e-10    1      136.03
