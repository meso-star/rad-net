#HC3N volume mixing ratio profile (from limb data)
#Latitude              :   31.5 N (FP3)
#Longitude             :  198.9 W (FP3)
#Observation date      : 2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  21:45
#Solar zenith angle    :  116�
#Vertical resolution   :  45 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
183.62       1.00500e+00    1.04700e-10    6.03525e-11    1.49047e-10    1      163.64
175.17       1.23400e+00    1.07100e-10    6.17194e-11    1.52481e-10    1      162.03
166.86       1.51500e+00    1.09200e-10    6.31698e-11    1.55230e-10    1      160.06
158.71       1.86100e+00    1.10900e-10    6.41086e-11    1.57691e-10    1      157.73
150.73       2.28500e+00    1.11900e-10    6.46609e-11    1.59139e-10    1      155.44
142.91       2.80500e+00    1.12000e-10    6.51728e-11    1.58827e-10    1      152.93
135.28       3.44400e+00    1.11300e-10    6.49107e-11    1.57689e-10    1      149.93
127.84       4.22900e+00    1.09800e-10    6.36258e-11    1.55974e-10    1      146.78
120.61       5.19200e+00    1.07500e-10    6.26046e-11    1.52395e-10    1      143.65
