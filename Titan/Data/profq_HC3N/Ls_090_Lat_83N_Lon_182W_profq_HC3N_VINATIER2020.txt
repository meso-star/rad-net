#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   83.2 N (FP3)
#Longitude             :  182.1 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   39.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
139.48       2.99000e+00    2.67000e-09    1.66231e-09    3.67769e-09    1      146.01
132.68       3.61800e+00    2.76700e-09    1.81050e-09    3.72350e-09    1      145.40
125.93       4.37700e+00    2.89600e-09    1.98090e-09    3.81110e-09    1      144.78
119.26       5.29700e+00    3.05800e-09    2.15962e-09    3.95638e-09    1      143.80
112.69       6.40900e+00    3.25100e-09    2.32154e-09    4.18046e-09    1      141.97
106.26       7.75500e+00    3.47200e-09    2.44057e-09    4.50343e-09    1      139.20
100.00       9.38400e+00    3.71300e-09    2.50791e-09    4.91809e-09    1      135.48
