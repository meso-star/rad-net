#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   33.4 N (FP3)
#Longitude             :  103.5 W (FP3)
#Observation date      :   2017-02-17 (S98/262TI flyby)
#Solar longitude       :   87.1�
#Emission angle        :   15.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
127.32       4.37700e+00    1.57600e-10    4.64536e-11    2.68746e-10    1      149.20
120.51       5.29700e+00    1.56700e-10    4.64585e-11    2.66941e-10    1      145.12
113.94       6.40900e+00    1.54800e-10    4.57492e-11    2.63851e-10    1      140.49
107.61       7.75500e+00    1.51700e-10    4.43957e-11    2.59004e-10    1      135.61
101.49       9.38400e+00    1.47900e-10    4.24444e-11    2.53356e-10    1      132.58
 95.49       1.13600e+01    1.43600e-10    4.02096e-11    2.46990e-10    1      131.76
 89.56       1.37400e+01    1.38700e-10    3.77856e-11    2.39614e-10    1      130.79
 83.76       1.66300e+01    1.33800e-10    3.53168e-11    2.32283e-10    1      126.70
