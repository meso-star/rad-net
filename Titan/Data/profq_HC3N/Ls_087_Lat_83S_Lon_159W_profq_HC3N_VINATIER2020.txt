#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   83.2 S (FP3)
#Longitude             :  158.8 W (FP3)
#Observation date      :   2017-02-01 (S97/259TI flyby)
#Solar longitude       :   86.6�
#Emission angle        :   50.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
330.92       2.10400e-02    2.55800e-07    1.89281e-07    3.22319e-07    1      192.37
320.71       2.54600e-02    2.69200e-07    2.02225e-07    3.36175e-07    1      189.65
310.71       3.08000e-02    2.80600e-07    2.14555e-07    3.46645e-07    1      187.14
300.90       3.72700e-02    2.89400e-07    2.25146e-07    3.53654e-07    1      184.88
291.29       4.51000e-02    2.94400e-07    2.33098e-07    3.55702e-07    1      182.26
281.87       5.45700e-02    2.94900e-07    2.37230e-07    3.52570e-07    1      179.81
272.64       6.60300e-02    2.90200e-07    2.36184e-07    3.44216e-07    1      177.45
263.59       7.99000e-02    2.80200e-07    2.29472e-07    3.30928e-07    1      175.03
254.72       9.66800e-02    2.65000e-07    2.17253e-07    3.12747e-07    1      172.61
246.03       1.17000e-01    2.45700e-07    2.00091e-07    2.91309e-07    1      169.96
237.54       1.41600e-01    2.23300e-07    1.79778e-07    2.66822e-07    1      167.04
229.26       1.71300e-01    1.99400e-07    1.57747e-07    2.41053e-07    1      163.61
221.21       2.07300e-01    1.75200e-07    1.36092e-07    2.14308e-07    1      159.65
