#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   47.3 S (FP3)
#Longitude             :  218.8 W (FP3)
#Observation date      :   2017-02-01 (S97/259TI flyby)
#Solar longitude       :   86.6�
#Emission angle        :   13.6�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
137.03       2.99000e+00    1.19700e-10    8.27414e-11    1.56659e-10    1      138.86
130.58       3.61800e+00    1.18700e-10    8.20634e-11    1.55337e-10    1      138.16
124.19       4.37700e+00    1.18300e-10    8.19587e-11    1.54641e-10    1      137.36
117.87       5.29700e+00    1.18500e-10    8.18723e-11    1.55128e-10    1      136.25
111.65       6.40900e+00    1.19000e-10    8.23501e-11    1.55650e-10    1      134.58
105.54       7.75500e+00    1.19500e-10    8.28278e-11    1.56172e-10    1      132.28
 99.59       9.38400e+00    1.19900e-10    8.29325e-11    1.56867e-10    1      129.37
 93.79       1.13600e+01    1.19900e-10    8.29325e-11    1.56867e-10    1      126.05
