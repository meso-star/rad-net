#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   56.6 N (FP3)
#Longitude             :   36.0 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :    7.0�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
154.20       2.04200e+00    1.44200e-09    9.35077e-10    1.94892e-09    1      151.61
147.09       2.47100e+00    1.65200e-09    1.10283e-09    2.20117e-09    1      149.75
140.10       2.99000e+00    1.87000e-09    1.28547e-09    2.45453e-09    1      148.13
133.20       3.61800e+00    2.07600e-09    1.46922e-09    2.68278e-09    1      147.05
126.39       4.37700e+00    2.24300e-09    1.62632e-09    2.85968e-09    1      146.05
119.67       5.29700e+00    2.34400e-09    1.72220e-09    2.96580e-09    1      144.77
113.05       6.40900e+00    2.35400e-09    1.73527e-09    2.97273e-09    1      142.76
106.58       7.75500e+00    2.26800e-09    1.65188e-09    2.88412e-09    1      139.88
100.29       9.38400e+00    2.09500e-09    1.49099e-09    2.69901e-09    1      136.12
