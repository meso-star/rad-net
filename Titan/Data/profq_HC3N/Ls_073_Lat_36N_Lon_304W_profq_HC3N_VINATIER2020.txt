#HC3N volume mixing ratio profile (from limb data)
#Latitude              :   36.3 N (FP3)
#Longitude             :  303.7 W (FP3)
#Observation date      : 2015-11-213 (T114 flyby)
#Solar longitude       :   73.1�
#Local time            :  17:51
#Solar zenith angle    :   74�
#Vertical resolution   :  35 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
185.40       9.52500e-01    1.01500e-10    7.57578e-11    1.27242e-10    1      164.86
177.49       1.15300e+00    9.29700e-11    6.93885e-11    1.16552e-10    1      162.81
169.73       1.39500e+00    8.61300e-11    6.42933e-11    1.07967e-10    1      160.84
162.10       1.68800e+00    8.11600e-11    6.05963e-11    1.01724e-10    1      158.91
154.60       2.04200e+00    7.82300e-11    5.84272e-11    9.80328e-11    1      156.98
147.23       2.47100e+00    7.75600e-11    5.79325e-11    9.71875e-11    1      155.03
140.01       2.99000e+00    7.93700e-11    5.92893e-11    9.94507e-11    1      152.90
132.92       3.61800e+00    8.40400e-11    6.27643e-11    1.05316e-10    1      150.31
126.01       4.37700e+00    9.20100e-11    6.87121e-11    1.15308e-10    1      147.44
119.26       5.29700e+00    1.03900e-10    7.75337e-11    1.30266e-10    1      144.28
