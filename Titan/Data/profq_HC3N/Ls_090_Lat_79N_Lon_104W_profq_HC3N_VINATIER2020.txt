#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   79.4 N (FP3)
#Longitude             :  104.1 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   31.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
139.75       2.99000e+00    1.32700e-09    9.07209e-10    1.74679e-09    1      145.74
132.95       3.61800e+00    1.28800e-09    8.96324e-10    1.67968e-09    1      145.21
126.22       4.37700e+00    1.26400e-09    8.92747e-10    1.63525e-09    1      144.71
119.54       5.29700e+00    1.25700e-09    8.95688e-10    1.61831e-09    1      143.85
112.96       6.40900e+00    1.26900e-09    9.04376e-10    1.63362e-09    1      142.17
106.51       7.75500e+00    1.30300e-09    9.20298e-10    1.68570e-09    1      139.53
100.24       9.38400e+00    1.35900e-09    9.43358e-10    1.77464e-09    1      135.93
