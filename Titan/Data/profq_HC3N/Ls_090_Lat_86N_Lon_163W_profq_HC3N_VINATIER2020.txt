#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   85.5 N (FP3)
#Longitude             :  163.1 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   41.4�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
125.83       4.37700e+00    3.00600e-09    2.14302e-09    3.86898e-09    1      144.93
119.16       5.29700e+00    3.24400e-09    2.42116e-09    4.06684e-09    1      143.90
112.58       6.40900e+00    3.43100e-09    2.60483e-09    4.25717e-09    1      142.00
106.15       7.75500e+00    3.53700e-09    2.63445e-09    4.43955e-09    1      139.14
 99.89       9.38400e+00    3.54000e-09    2.50724e-09    4.57276e-09    1      135.35
