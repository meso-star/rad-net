#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :    8.6 S (FP3)
#Longitude             :   61.6 W (FP3)
#Observation date      :   2017-09-12 (S101/293TI flyby)
#Solar longitude       :   93.3�
#Emission angle        :   26.0�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
155.65       2.04200e+00    1.50700e-10    7.70564e-11    2.24344e-10    1      159.07
148.17       2.47100e+00    1.50600e-10    7.69592e-11    2.24241e-10    1      157.47
140.83       2.99000e+00    1.49000e-10    7.63749e-11    2.21625e-10    1      154.81
133.67       3.61800e+00    1.45800e-10    7.47210e-11    2.16879e-10    1      151.57
126.70       4.37700e+00    1.41300e-10    7.22889e-11    2.10311e-10    1      148.40
119.91       5.29700e+00    1.35700e-10    6.92732e-11    2.02127e-10    1      145.16
113.27       6.40900e+00    1.29600e-10    6.62569e-11    1.92943e-10    1      143.08
106.80       7.75500e+00    1.23400e-10    6.31433e-11    1.83657e-10    1      139.56
100.48       9.38400e+00    1.17500e-10    6.03214e-11    1.74679e-10    1      137.37
