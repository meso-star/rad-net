#HC3N volume mixing ratio profile (from limb data)
#Latitude              :   33.5 S (FP3)
#Longitude             :   49.1 W (FP3)
#Observation date      :  2017-02-02 (S98/259TI flyby)
#Solar longitude       :   86.6�
#Local time            :  10:19
#Solar zenith angle    :   68�
#Vertical resolution   :  62 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
182.32       9.52500e-01    2.00800e-10    1.51522e-10    2.50078e-10    1      158.60
174.74       1.15300e+00    1.84500e-10    1.39137e-10    2.29863e-10    1      156.42
167.30       1.39500e+00    1.68500e-10    1.27036e-10    2.09964e-10    1      153.90
160.03       1.68800e+00    1.53200e-10    1.15596e-10    1.90804e-10    1      151.21
152.93       2.04200e+00    1.39000e-10    1.04793e-10    1.73207e-10    1      148.53
145.99       2.47100e+00    1.26100e-10    9.52177e-11    1.56982e-10    1      145.94
139.20       2.99000e+00    1.15000e-10    8.67410e-11    1.43259e-10    1      143.49
132.55       3.61800e+00    1.05600e-10    7.96489e-11    1.31551e-10    1      141.40
126.02       4.37700e+00    9.78700e-11    7.39133e-11    1.21827e-10    1      139.72
119.59       5.29700e+00    9.18400e-11    6.93619e-11    1.14318e-10    1      138.19
