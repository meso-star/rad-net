#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   40.0 N (FP3)
#Longitude             :   50.5 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   11.9�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
155.16       2.04200e+00    3.24400e-10    2.52378e-10    3.96422e-10    1      154.30
147.92       2.47100e+00    3.26400e-10    2.53915e-10    3.98885e-10    1      152.28
140.81       2.99000e+00    3.28300e-10    2.55358e-10    4.01242e-10    1      150.48
133.80       3.61800e+00    3.29900e-10    2.56689e-10    4.03111e-10    1      149.19
126.90       4.37700e+00    3.31100e-10    2.57815e-10    4.04385e-10    1      147.97
120.08       5.29700e+00    3.31800e-10    2.58471e-10    4.05129e-10    1      146.48
113.39       6.40900e+00    3.32000e-10    2.58489e-10    4.05511e-10    1      144.27
106.86       7.75500e+00    3.31500e-10    2.58190e-10    4.04810e-10    1      141.20
100.51       9.38400e+00    3.30400e-10    2.57328e-10    4.03472e-10    1      137.26
