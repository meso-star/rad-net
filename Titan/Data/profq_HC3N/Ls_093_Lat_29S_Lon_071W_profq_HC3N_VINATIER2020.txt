#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   28.7 S (FP3)
#Longitude             :   70.9 W (FP3)
#Observation date      :   2017-09-12 (S101/293TI flyby)
#Solar longitude       :   93.3�
#Emission angle        :   42.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
153.73       2.04200e+00    1.20100e-10    6.13922e-11    1.78808e-10    1      153.98
146.50       2.47100e+00    1.18700e-10    6.05169e-11    1.76883e-10    1      152.47
139.40       2.99000e+00    1.16400e-10    5.92522e-11    1.73548e-10    1      150.02
132.46       3.61800e+00    1.13200e-10    5.80837e-11    1.68316e-10    1      147.08
125.69       4.37700e+00    1.09400e-10    5.60408e-11    1.62759e-10    1      144.28
119.09       5.29700e+00    1.05300e-10    5.37548e-11    1.56845e-10    1      141.48
112.61       6.40900e+00    1.01100e-10    5.18085e-11    1.50392e-10    1      139.85
106.28       7.75500e+00    9.72400e-11    4.96587e-11    1.44821e-10    1      136.80
100.09       9.38400e+00    9.38800e-11    4.79463e-11    1.39814e-10    1      135.06
