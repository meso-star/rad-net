#HC3N volume mixing ratio profile (from nadir data)
#Latitude              :   58.7 N (FP3)
#Longitude             :  218.0 W (FP3)
#Observation date      :  2016-06-08 (T120 flyby)
#Solar longitude       :   79.4�
#Emission angle        :   22.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
151.79       2.04200e+00    2.77200e-10    1.91612e-10    3.62788e-10    1      151.83
144.69       2.47100e+00    2.90300e-10    2.00799e-10    3.79801e-10    1      149.35
137.75       2.99000e+00    3.04100e-10    2.10378e-10    3.97822e-10    1      147.07
130.94       3.61800e+00    3.19300e-10    2.20739e-10    4.17861e-10    1      144.72
124.30       4.37700e+00    3.36800e-10    2.32744e-10    4.40856e-10    1      141.42
117.83       5.29700e+00    3.58100e-10    2.47824e-10    4.68376e-10    1      138.95
