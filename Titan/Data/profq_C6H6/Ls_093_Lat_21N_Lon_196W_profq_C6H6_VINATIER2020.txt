#C6H6 volume mixing ratio profile (from limb data)
#Latitude              :   21.4 N (FP3)
#Longitude             :  195.5 W (FP3)
#Observation date      : 2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  22:00
#Solar zenith angle    :  126�
#Vertical resolution   :  43 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
183.96       1.00500e+00    1.98400e-10    1.06764e-10    2.90036e-10    1      164.46
175.46       1.23400e+00    2.30700e-10    1.25242e-10    3.36158e-10    1      162.87
167.10       1.51500e+00    2.69900e-10    1.48033e-10    3.91767e-10    1      161.04
158.90       1.86100e+00    3.15400e-10    1.74553e-10    4.56247e-10    1      158.95
150.84       2.28500e+00    3.64600e-10    2.04183e-10    5.25017e-10    1      156.97
142.94       2.80500e+00    4.12700e-10    2.34168e-10    5.91232e-10    1      154.76
135.21       3.44400e+00    4.52900e-10    2.58871e-10    6.46929e-10    1      151.93
127.68       4.22900e+00    4.77400e-10    2.74060e-10    6.80740e-10    1      148.74
120.35       5.19200e+00    4.80500e-10    2.76113e-10    6.84887e-10    1      145.29
