#C6H6 volume mixing ratio profile (from limb data)
#Latitude              :   26.2 N (FP3)
#Longitude             :  304.4 W (FP3)
#Observation date      : 2015-11-213 (T114 flyby)
#Solar longitude       :   73.1�
#Local time            :  17:50
#Solar zenith angle    :   78�
#Vertical resolution   :  32 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
186.07       9.52500e-01    3.26300e-10    2.37331e-10    4.15269e-10    1      166.21
178.09       1.15300e+00    3.62900e-10    2.66606e-10    4.59194e-10    1      164.42
170.23       1.39500e+00    4.10800e-10    3.05486e-10    5.16114e-10    1      162.63
162.51       1.68800e+00    4.70200e-10    3.54135e-10    5.86265e-10    1      160.77
154.93       2.04200e+00    5.39600e-10    4.11379e-10    6.67821e-10    1      158.76
147.48       2.47100e+00    6.14900e-10    4.74645e-10    7.55155e-10    1      156.61
140.19       2.99000e+00    6.89700e-10    5.37375e-10    8.42025e-10    1      154.20
133.05       3.61800e+00    7.55900e-10    5.92256e-10    9.19544e-10    1      151.30
126.09       4.37700e+00    8.05200e-10    6.31803e-10    9.78598e-10    1      148.17
119.32       5.29700e+00    8.32200e-10    6.50972e-10    1.01343e-09    1      144.82
