#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   69.2 N (FP3)
#Longitude             :   48.3 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   18.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
139.88       2.99000e+00    1.15400e-08    7.87409e-09    1.52059e-08    1      146.30
133.06       3.61800e+00    9.46700e-09    6.42966e-09    1.25043e-08    1      145.61
126.31       4.37700e+00    7.89200e-09    5.33529e-09    1.04487e-08    1      144.98
119.63       5.29700e+00    6.75400e-09    4.53818e-09    8.96982e-09    1      144.04
113.04       6.40900e+00    5.95200e-09    3.96956e-09    7.93444e-09    1      142.31
106.58       7.75500e+00    5.34400e-09    3.52882e-09    7.15918e-09    1      139.64
100.30       9.38400e+00    4.90000e-09    3.19655e-09    6.60345e-09    1      136.03
