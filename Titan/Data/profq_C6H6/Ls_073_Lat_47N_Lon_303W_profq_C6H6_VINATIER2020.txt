#C6H6 volume mixing ratio profile (from limb data)
#Latitude              :   46.5 N (FP3)
#Longitude             :  303.1 W (FP3)
#Observation date      : 2015-11-213 (T114 flyby)
#Solar longitude       :   73.1�
#Local time            :  17:52
#Solar zenith angle    :   71�
#Vertical resolution   :  37 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
179.62       1.06900e+00    4.76300e-10    3.10212e-10    6.42388e-10    1      161.88
171.59       1.30200e+00    5.21400e-10    3.46058e-10    6.96742e-10    1      159.77
163.72       1.58800e+00    5.72200e-10    3.86947e-10    7.57453e-10    1      157.80
155.98       1.93500e+00    6.27300e-10    4.31913e-10    8.22687e-10    1      155.90
148.37       2.35900e+00    6.84800e-10    4.78744e-10    8.90856e-10    1      154.06
140.91       2.87500e+00    7.41900e-10    5.25155e-10    9.58645e-10    1      152.08
133.59       3.50400e+00    7.94900e-10    5.67632e-10    1.02217e-09    1      149.67
126.43       4.27100e+00    8.39700e-10    6.01909e-10    1.07749e-09    1      146.86
119.46       5.20600e+00    8.72000e-10    6.24668e-10    1.11933e-09    1      143.64
