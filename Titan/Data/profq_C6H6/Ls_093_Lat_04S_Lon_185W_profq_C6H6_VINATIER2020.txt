#C6H6 volume mixing ratio profile (from limb data)
#Latitude              :    3.9 S (FP3)
#Longitude             :  185.0 W (FP3)
#Observation date      : 2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  22:47
#Solar zenith angle    :  153�
#Vertical resolution   :  39 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
185.39       1.00500e+00    2.49300e-10    1.61959e-10    3.36641e-10    1      166.36
176.78       1.23400e+00    2.96300e-10    1.95125e-10    3.97475e-10    1      165.02
168.30       1.51500e+00    3.51800e-10    2.34333e-10    4.69267e-10    1      163.36
159.97       1.86100e+00    4.12600e-10    2.78613e-10    5.46587e-10    1      161.34
151.79       2.28500e+00    4.72800e-10    3.23003e-10    6.22597e-10    1      159.29
143.77       2.80500e+00    5.23500e-10    3.61766e-10    6.85234e-10    1      156.88
135.94       3.44400e+00    5.55500e-10    3.85766e-10    7.25234e-10    1      153.78
128.31       4.22900e+00    5.61500e-10    3.90114e-10    7.32886e-10    1      150.30
120.91       5.19200e+00    5.40100e-10    3.73474e-10    7.06726e-10    1      146.83
