#C6H6 volume mixing ratio profile (from limb data)
#Latitude              :   48.7 S (FP3)
#Longitude             :  131.2 W (FP3)
#Observation date      :  2016-05-07 (T119 flyby)
#Solar longitude       :   78.4�
#Local time            :  04:41
#Solar zenith angle    :  122�
#Vertical resolution   :  47 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
179.73       9.52500e-01    2.22700e-09    1.78384e-09    2.67016e-09    1      149.72
172.59       1.15300e+00    2.52800e-09    2.03595e-09    3.02005e-09    1      147.38
165.59       1.39500e+00    2.80500e-09    2.27253e-09    3.33747e-09    1      145.12
158.74       1.68800e+00    3.03100e-09    2.47013e-09    3.59187e-09    1      143.05
152.00       2.04200e+00    3.17900e-09    2.60537e-09    3.75263e-09    1      141.26
145.38       2.47100e+00    3.22800e-09    2.65391e-09    3.80209e-09    1      139.71
138.86       2.99000e+00    3.17400e-09    2.61061e-09    3.73739e-09    1      138.20
132.44       3.61800e+00    3.02400e-09    2.47701e-09    3.57099e-09    1      137.17
128.12       4.11600e+00    2.88100e-09    2.34806e-09    3.41394e-09    1      136.67
