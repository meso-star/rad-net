#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   84.6 N (FP3)
#Longitude             :  177.2 W (FP3)
#Observation date      :   2017-09-12 (S101/293TI flyby)
#Solar longitude       :   93.3�
#Emission angle        :   26.9�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
182.12       9.52500e-01    3.24100e-10    1.70347e-10    4.77853e-10    1      158.09
174.54       1.15300e+00    3.48900e-10    1.85224e-10    5.12576e-10    1      156.38
167.09       1.39500e+00    3.68400e-10    1.96891e-10    5.39909e-10    1      154.71
159.76       1.68800e+00    3.79800e-10    2.05051e-10    5.54549e-10    1      152.90
152.57       2.04200e+00    3.81400e-10    2.07570e-10    5.55230e-10    1      150.74
145.52       2.47100e+00    3.72800e-10    2.04060e-10    5.41540e-10    1      148.39
138.61       2.99000e+00    3.62500e-10    1.99868e-10    5.25132e-10    1      146.21
131.83       3.61800e+00    3.50300e-10    1.93832e-10    5.06768e-10    1      144.12
125.19       4.37700e+00    3.36100e-10    1.85858e-10    4.86342e-10    1      142.15
118.66       5.29700e+00    3.19900e-10    1.76910e-10    4.62890e-10    1      140.32
112.25       6.40900e+00    3.02000e-10    1.65832e-10    4.38168e-10    1      138.37
105.98       7.75500e+00    2.82800e-10    1.53975e-10    4.11625e-10    1      135.72
 99.85       9.38400e+00    2.62700e-10    1.42211e-10    3.83189e-10    1      133.14
 93.87       1.13600e+01    2.42500e-10    1.29381e-10    3.55619e-10    1      130.63
 88.03       1.37400e+01    1.59000e-10    8.41495e-11    2.33851e-10    1      128.13
