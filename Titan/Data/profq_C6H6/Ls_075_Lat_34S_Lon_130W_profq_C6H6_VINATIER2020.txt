#C6H6 volume mixing ratio profile (from limb data)
#Latitude              :   33.5 S (FP3)
#Longitude             :  129.6 W (FP3)
#Observation date      :  2016-01-17 (T115 flyby)
#Solar longitude       :   75.1�
#Local time            :  06:19
#Solar zenith angle    :  101�
#Vertical resolution   :  43 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
183.41       9.52500e-01    1.88800e-10    1.50274e-10    2.27326e-10    1      162.12
175.64       1.15300e+00    1.83200e-10    1.46139e-10    2.20261e-10    1      160.31
168.00       1.39500e+00    1.78900e-10    1.42751e-10    2.15049e-10    1      158.49
160.49       1.68800e+00    1.75800e-10    1.40326e-10    2.11274e-10    1      156.65
153.10       2.04200e+00    1.73900e-10    1.38865e-10    2.08935e-10    1      154.74
145.85       2.47100e+00    1.73300e-10    1.38460e-10    2.08140e-10    1      152.78
138.73       2.99000e+00    1.74000e-10    1.38958e-10    2.09042e-10    1      150.77
131.75       3.61800e+00    1.75900e-10    1.40419e-10    2.11381e-10    1      148.58
124.91       4.37700e+00    1.79100e-10    1.42937e-10    2.15263e-10    1      146.20
118.22       5.29700e+00    1.83600e-10    1.46512e-10    2.20688e-10    1      143.42
