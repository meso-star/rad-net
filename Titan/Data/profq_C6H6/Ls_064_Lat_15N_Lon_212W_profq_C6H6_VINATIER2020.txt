#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   14.8 N (FP3)
#Longitude             :  212.4 W (FP3)
#Observation date      :  2015-01-12 (T108 flyby)
#Solar longitude       :   63.8�
#Emission angle        :   35.0�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
142.09       2.99000e+00    4.11900e-10    2.85356e-10    5.38444e-10    1      156.98
134.81       3.61800e+00    4.81200e-10    3.33536e-10    6.28864e-10    1      154.35
127.70       4.37700e+00    5.50500e-10    3.81439e-10    7.19561e-10    1      151.55
120.75       5.29700e+00    6.13200e-10    4.25256e-10    8.01144e-10    1      148.64
