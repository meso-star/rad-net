#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   25.0 N (FP3)
#Longitude             :  217.0 W (FP3)
#Observation date      :  2015-01-12 (T108 flyby)
#Solar longitude       :   63.8�
#Emission angle        :   45.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
142.74       2.99000e+00    4.26600e-10    2.95795e-10    5.57405e-10    1      156.53
135.48       3.61800e+00    4.99400e-10    3.46209e-10    6.52591e-10    1      154.05
128.37       4.37700e+00    5.72000e-10    3.96988e-10    7.47012e-10    1      151.43
121.42       5.29700e+00    6.37500e-10    4.42370e-10    8.32630e-10    1      148.76
