#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   79.4 N (FP3)
#Longitude             :  104.1 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   31.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
139.75       2.99000e+00    1.57900e-08    9.49938e-09    2.20806e-08    1      145.74
132.95       3.61800e+00    1.31400e-08    7.95753e-09    1.83225e-08    1      145.21
126.22       4.37700e+00    1.09800e-08    6.70142e-09    1.52586e-08    1      144.71
119.54       5.29700e+00    9.28600e-09    5.70888e-09    1.28631e-08    1      143.85
112.96       6.40900e+00    7.98500e-09    4.92098e-09    1.10490e-08    1      142.17
106.51       7.75500e+00    6.91400e-09    4.24057e-09    9.58743e-09    1      139.53
100.24       9.38400e+00    6.05900e-09    3.66552e-09    8.45248e-09    1      135.93
