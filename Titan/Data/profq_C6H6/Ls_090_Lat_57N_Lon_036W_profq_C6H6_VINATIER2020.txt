#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   56.6 N (FP3)
#Longitude             :   36.0 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :    7.0�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
154.20       2.04200e+00    1.44100e-08    1.00874e-08    1.87326e-08    1      151.61
147.09       2.47100e+00    1.22500e-08    8.59686e-09    1.59031e-08    1      149.75
140.10       2.99000e+00    1.04200e-08    7.29608e-09    1.35439e-08    1      148.13
133.20       3.61800e+00    8.92600e-09    6.24413e-09    1.16079e-08    1      147.05
126.39       4.37700e+00    7.75500e-09    5.40649e-09    1.01035e-08    1      146.05
119.67       5.29700e+00    6.89900e-09    4.77917e-09    9.01883e-09    1      144.77
113.05       6.40900e+00    6.30500e-09    4.32809e-09    8.28191e-09    1      142.76
106.58       7.75500e+00    5.85300e-09    3.96996e-09    7.73604e-09    1      139.88
100.29       9.38400e+00    5.52700e-09    3.69124e-09    7.36276e-09    1      136.12
