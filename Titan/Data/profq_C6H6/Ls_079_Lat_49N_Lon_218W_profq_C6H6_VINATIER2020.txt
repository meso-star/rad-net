#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   49.0 N (FP3)
#Longitude             :  218.3 W (FP3)
#Observation date      :  2016-06-08 (T120 flyby)
#Solar longitude       :   79.4�
#Emission angle        :   14.5�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
138.18       2.99000e+00    1.44500e-09    1.00333e-09    1.88667e-09    1      148.48
131.32       3.61800e+00    1.59100e-09    1.10676e-09    2.07524e-09    1      145.92
124.60       4.37700e+00    1.67800e-09    1.16492e-09    2.19108e-09    1      143.73
118.00       5.29700e+00    1.70200e-09    1.18230e-09    2.22170e-09    1      141.84
