#C6H6 volume mixing ratio profile (from limb data)
#Latitude              :    1.2 N (FP3)
#Longitude             :  187.6 W (FP3)
#Observation date      : 2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  22:35
#Solar zenith angle    :  147�
#Vertical resolution   :  40 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
151.54       2.28500e+00    3.62900e-10    2.30727e-10    4.95072e-10    1      158.88
143.55       2.80500e+00    3.98300e-10    2.55312e-10    5.41288e-10    1      156.55
135.73       3.44400e+00    4.22500e-10    2.71883e-10    5.73117e-10    1      153.64
128.10       4.22900e+00    4.30900e-10    2.77202e-10    5.84598e-10    1      150.44
120.69       5.19200e+00    4.21900e-10    2.69729e-10    5.74071e-10    1      147.07
