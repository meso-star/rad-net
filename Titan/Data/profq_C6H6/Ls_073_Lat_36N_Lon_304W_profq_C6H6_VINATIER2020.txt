#C6H6 volume mixing ratio profile (from limb data)
#Latitude              :   36.3 N (FP3)
#Longitude             :  303.7 W (FP3)
#Observation date      : 2015-11-213 (T114 flyby)
#Solar longitude       :   73.1�
#Local time            :  17:51
#Solar zenith angle    :   74�
#Vertical resolution   :  35 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
185.40       9.52500e-01    2.84600e-10    2.20558e-10    3.48642e-10    1      164.86
177.49       1.15300e+00    3.07400e-10    2.40027e-10    3.74773e-10    1      162.81
169.73       1.39500e+00    3.40600e-10    2.68192e-10    4.13008e-10    1      160.84
162.10       1.68800e+00    3.86600e-10    3.07278e-10    4.65922e-10    1      158.91
154.60       2.04200e+00    4.47600e-10    3.59306e-10    5.35894e-10    1      156.98
147.23       2.47100e+00    5.24800e-10    4.25760e-10    6.23840e-10    1      155.03
140.01       2.99000e+00    6.17400e-10    5.05695e-10    7.29105e-10    1      152.90
132.92       3.61800e+00    7.21400e-10    5.94867e-10    8.47933e-10    1      150.31
126.01       4.37700e+00    8.28800e-10    6.84534e-10    9.73066e-10    1      147.44
119.26       5.29700e+00    9.28400e-10    7.63373e-10    1.09343e-09    1      144.28
