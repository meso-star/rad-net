#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   57.9 S (FP3)
#Longitude             :   72.8 W (FP3)
#Observation date      :   2017-09-12 (S101/293TI flyby)
#Solar longitude       :   93.3�
#Emission angle        :   73.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
197.21       5.37600e-01    9.98400e-10    5.24273e-10    1.47253e-09    1      159.84
189.51       6.50600e-01    1.00100e-09    5.26798e-10    1.47520e-09    1      156.43
182.01       7.87200e-01    1.00300e-09    5.33098e-10    1.47290e-09    1      153.15
174.71       9.52500e-01    1.00600e-09    5.36495e-10    1.47551e-09    1      149.87
167.59       1.15300e+00    1.00900e-09    5.40858e-10    1.47714e-09    1      147.13
160.64       1.39500e+00    1.01200e-09    5.44737e-10    1.47926e-09    1      144.40
153.86       1.68800e+00    1.01500e-09    5.47648e-10    1.48235e-09    1      141.65
147.24       2.04200e+00    1.01800e-09    5.48624e-10    1.48738e-09    1      138.78
140.76       2.47100e+00    1.02000e-09    5.50565e-10    1.48944e-09    1      137.36
134.38       2.99000e+00    1.02200e-09    5.49120e-10    1.49488e-09    1      135.29
128.14       3.61800e+00    1.02300e-09    5.47671e-10    1.49833e-09    1      133.01
122.02       4.37700e+00    1.02300e-09    5.46220e-10    1.49978e-09    1      131.09
116.02       5.29700e+00    6.11400e-10    3.22884e-10    8.99916e-10    1      129.36
110.09       6.40900e+00    4.38300e-10    2.29887e-10    6.46713e-10    1      128.94
104.23       7.75500e+00    1.97300e-10    1.02931e-10    2.91669e-10    1      127.17
 98.45       9.38400e+00    1.40200e-10    7.26756e-11    2.07724e-10    1      126.73
