#C6H6 volume mixing ratio profile (from nadir data)
#Latitude              :   33.4 N (FP3)
#Longitude             :  103.5 W (FP3)
#Observation date      :   2017-02-17 (S98/262TI flyby)
#Solar longitude       :   87.1�
#Emission angle        :   15.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
127.32       4.37700e+00    3.24800e-09    2.05908e-09    4.43692e-09    1      149.20
120.51       5.29700e+00    2.47400e-09    1.54568e-09    3.40232e-09    1      145.12
113.94       6.40900e+00    1.98300e-09    1.22366e-09    2.74234e-09    1      140.49
107.61       7.75500e+00    1.66400e-09    1.01604e-09    2.31196e-09    1      135.61
101.49       9.38400e+00    1.46800e-09    8.87010e-10    2.04899e-09    1      132.58
 95.49       1.13600e+01    1.36800e-09    8.20560e-10    1.91544e-09    1      131.76
 89.56       1.37400e+01    1.34700e-09    8.02715e-10    1.89129e-09    1      130.79
 83.76       1.66300e+01    1.39800e-09    8.28008e-10    1.96799e-09    1      126.70
