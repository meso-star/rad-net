#C2H4 volume mixing ratio profile (from nadir data)
#Latitude              :   14.8 N (FP3)
#Longitude             :  212.4 W (FP3)
#Observation date      :  2015-01-12 (T108 flyby)
#Solar longitude       :   63.8�
#Emission angle        :   35.0�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
142.09       2.99000e+00    1.04400e-07    8.27927e-08    1.26007e-07    1      156.98
134.81       3.61800e+00    1.35400e-07    1.07633e-07    1.63167e-07    1      154.35
127.70       4.37700e+00    1.70500e-07    1.36116e-07    2.04884e-07    1      151.55
120.75       5.29700e+00    2.05400e-07    1.64247e-07    2.46553e-07    1      148.64
