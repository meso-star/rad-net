#C2H4 volume mixing ratio profile (from nadir data)
#Latitude              :    4.5 S (FP3)
#Longitude             :  212.5 W (FP3)
#Observation date      :  2014-12-10 (T107 flyby)
#Solar longitude       :   62.8�
#Emission angle        :   23.8�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
141.85       2.99000e+00    1.22300e-07    1.01348e-07    1.43252e-07    1      156.93
134.57       3.61800e+00    1.60200e-07    1.33147e-07    1.87253e-07    1      154.19
127.47       4.37700e+00    2.03400e-07    1.69488e-07    2.37312e-07    1      151.28
120.54       5.29700e+00    2.46100e-07    2.05473e-07    2.86727e-07    1      148.29
