#C2H4 volume mixing ratio profile (from limb data)
#Latitude              :   61.6 N (FP3)
#Longitude             :  217.0 W (FP3)
#Observation date      : 2017-09-11 (S101/292TI flyby)
#Solar longitude       :   93.3�
#Local time            :  20:39
#Solar zenith angle    :   85�
#Vertical resolution   :  52 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
429.75       5.94300e-03    4.51000e-08    2.11313e-08    6.90687e-08    1      173.75
419.02       7.29700e-03    4.14700e-08    1.97277e-08    6.32123e-08    1      174.91
408.29       8.95900e-03    4.00400e-08    1.95153e-08    6.05647e-08    1      176.52
397.53       1.10000e-02    4.12900e-08    2.07641e-08    6.18159e-08    1      178.33
386.74       1.35100e-02    4.60000e-08    2.40680e-08    6.79320e-08    1      180.02
375.94       1.65800e-02    5.55900e-08    3.04126e-08    8.07674e-08    1      181.34
365.15       2.03600e-02    7.26400e-08    4.17102e-08    1.03570e-07    1      182.19
354.40       2.50000e-02    1.01000e-07    6.07726e-08    1.41227e-07    1      182.62
343.72       3.06900e-02    1.45600e-07    9.20202e-08    1.99180e-07    1      182.72
333.12       3.76900e-02    2.10000e-07    1.39317e-07    2.80683e-07    1      182.44
322.63       4.62700e-02    2.91000e-07    2.03062e-07    3.78938e-07    1      181.67
312.25       5.68100e-02    3.72700e-07    2.72847e-07    4.72553e-07    1      180.86
302.00       6.97500e-02    4.27700e-07    3.26921e-07    5.28479e-07    1      180.05
291.87       8.56400e-02    4.33100e-07    3.40318e-07    5.25882e-07    1      179.15
281.86       1.05200e-01    3.88000e-07    3.06826e-07    4.69174e-07    1      178.22
271.97       1.29100e-01    3.15000e-07    2.46106e-07    3.83894e-07    1      177.23
262.20       1.58500e-01    2.41600e-07    1.84926e-07    2.98274e-07    1      176.28
252.56       1.94600e-01    1.84900e-07    1.38502e-07    2.31298e-07    1      175.32
243.03       2.39000e-01    1.49300e-07    1.10192e-07    1.88408e-07    1      174.27
233.63       2.93400e-01    1.33300e-07    9.71304e-08    1.69470e-07    1      172.99
224.37       3.60300e-01    1.35300e-07    9.75085e-08    1.73092e-07    1      171.25
215.29       4.42300e-01    1.56700e-07    1.12360e-07    2.01040e-07    1      168.93
206.39       5.43100e-01    2.02300e-07    1.45033e-07    2.59567e-07    1      166.24
197.69       6.66800e-01    2.76400e-07    2.01138e-07    3.51662e-07    1      163.75
189.16       8.18700e-01    3.75200e-07    2.81514e-07    4.68886e-07    1      161.40
180.81       1.00500e+00    4.75600e-07    3.69642e-07    5.81558e-07    1      159.20
172.62       1.23400e+00    5.35000e-07    4.27155e-07    6.42845e-07    1      157.31
164.56       1.51500e+00    5.18500e-07    4.15061e-07    6.21939e-07    1      155.53
156.65       1.86100e+00    4.31400e-07    3.36458e-07    5.26342e-07    1      153.74
148.87       2.28500e+00    3.15100e-07    2.35407e-07    3.94793e-07    1      151.92
141.23       2.80500e+00    2.10600e-07    1.50173e-07    2.71027e-07    1      149.97
133.74       3.44400e+00    1.36000e-07    9.26062e-08    1.79394e-07    1      147.65
126.42       4.22900e+00    8.93300e-08    5.84411e-08    1.20219e-07    1      145.01
119.27       5.19200e+00    6.25200e-08    3.91743e-08    8.58657e-08    1      142.13
