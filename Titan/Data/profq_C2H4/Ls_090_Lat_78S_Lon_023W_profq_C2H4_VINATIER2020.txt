#C2H4 volume mixing ratio profile (from limb data)
#Latitude              :   78.2 S (FP3)
#Longitude             :   23.0 W (FP3)
#Observation date      :  2017-05-25 (S99/275TI flyby)
#Solar longitude       :   90.0�
#Local time            :  11:13
#Solar zenith angle    :  109�
#Vertical resolution   :  51 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
559.69       3.17300e-04    1.72600e-05    1.47255e-05    1.97945e-05    1      168.35
549.19       3.84000e-04    1.67800e-05    1.43646e-05    1.91954e-05    1      169.19
538.70       4.64600e-04    1.58300e-05    1.35981e-05    1.80619e-05    1      170.22
528.21       5.62200e-04    1.45000e-05    1.25003e-05    1.64997e-05    1      171.50
517.71       6.80300e-04    1.29000e-05    1.11506e-05    1.46494e-05    1      173.09
507.17       8.23200e-04    1.11800e-05    9.69158e-06    1.26684e-05    1      175.02
496.58       9.96100e-04    9.47600e-06    8.23467e-06    1.07173e-05    1      177.23
485.92       1.20500e-03    7.89500e-06    6.87503e-06    8.91497e-06    1      179.68
475.19       1.45800e-03    6.51700e-06    5.68450e-06    7.34950e-06    1      182.12
464.40       1.76500e-03    5.37700e-06    4.69603e-06    6.05797e-06    1      184.31
453.56       2.13500e-03    4.47300e-06    3.91018e-06    5.03582e-06    1      186.21
442.72       2.58400e-03    3.79300e-06    3.31789e-06    4.26811e-06    1      187.46
431.89       3.12700e-03    3.30900e-06    2.89567e-06    3.72233e-06    1      188.02
421.13       3.78300e-03    2.99600e-06    2.62221e-06    3.36979e-06    1      188.11
410.45       4.57800e-03    2.83600e-06    2.48279e-06    3.18921e-06    1      187.71
399.88       5.53900e-03    2.78300e-06    2.43690e-06    3.12910e-06    1      187.11
389.42       6.70300e-03    2.84900e-06    2.49479e-06    3.20321e-06    1      186.13
379.10       8.11000e-03    3.04600e-06    2.66756e-06    3.42444e-06    1      184.80
368.94       9.81400e-03    3.40900e-06    2.98594e-06    3.83206e-06    1      183.01
358.96       1.18700e-02    3.97400e-06    3.48112e-06    4.46688e-06    1      180.84
349.17       1.43700e-02    4.78200e-06    4.18958e-06    5.37442e-06    1      178.35
339.59       1.73900e-02    5.86500e-06    5.13936e-06    6.59064e-06    1      175.67
330.21       2.10400e-02    7.22600e-06    6.33312e-06    8.11888e-06    1      172.96
321.03       2.54600e-02    8.78800e-06    7.70239e-06    9.87361e-06    1      170.58
312.02       3.08000e-02    1.03700e-05    9.08865e-06    1.16513e-05    1      168.46
303.18       3.72700e-02    1.16900e-05    1.02423e-05    1.31377e-05    1      166.65
294.49       4.51000e-02    1.24100e-05    1.08620e-05    1.39580e-05    1      164.63
285.94       5.45700e-02    1.22800e-05    1.07375e-05    1.38225e-05    1      162.99
277.53       6.60300e-02    1.12500e-05    9.81913e-06    1.26809e-05    1      161.70
269.22       7.99000e-02    9.54000e-06    8.30608e-06    1.07739e-05    1      160.63
261.01       9.66800e-02    7.49600e-06    6.50747e-06    8.48453e-06    1      159.87
252.88       1.17000e-01    5.49900e-06    4.75744e-06    6.24056e-06    1      159.13
244.84       1.41600e-01    3.79900e-06    3.27487e-06    4.32313e-06    1      158.35
236.89       1.71300e-01    2.50400e-06    2.14970e-06    2.85830e-06    1      157.24
229.05       2.07300e-01    1.60000e-06    1.36804e-06    1.83196e-06    1      155.70
221.35       2.50800e-01    1.00400e-06    8.54940e-07    1.15306e-06    1      153.62
213.80       3.03500e-01    6.28300e-07    5.33169e-07    7.23431e-07    1      151.13
206.42       3.67200e-01    3.97600e-07    3.36273e-07    4.58927e-07    1      148.24
199.23       4.44300e-01    2.57500e-07    2.17118e-07    2.97882e-07    1      145.16
192.23       5.37600e-01    1.72600e-07    1.45180e-07    2.00020e-07    1      142.07
185.39       6.50600e-01    1.20700e-07    1.01288e-07    1.40112e-07    1      139.62
180.11       7.56000e-01    9.43000e-08    7.90134e-08    1.09587e-07    1      138.00
