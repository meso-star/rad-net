#C2H4 volume mixing ratio profile (from nadir data)
#Latitude              :   59.1 S (FP3)
#Longitude             :  146.5 W (FP3)
#Observation date      :  2016-05-05 (T119 flyby)
#Solar longitude       :   78.4�
#Emission angle        :   39.2�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
217.32       3.03500e-01    2.64600e-07    2.15964e-07    3.13236e-07    1      154.22
209.72       3.67200e-01    3.09000e-07    2.52197e-07    3.65803e-07    1      153.70
202.18       4.44300e-01    3.45600e-07    2.82137e-07    4.09063e-07    1      153.03
194.73       5.37600e-01    3.66100e-07    2.98912e-07    4.33288e-07    1      152.00
187.37       6.50600e-01    3.64800e-07    2.97940e-07    4.31660e-07    1      150.65
180.13       7.87200e-01    3.41200e-07    2.78759e-07    4.03641e-07    1      149.13
173.01       9.52500e-01    3.00300e-07    2.45187e-07    3.55413e-07    1      146.72
166.06       1.15300e+00    2.50600e-07    2.04742e-07    2.96458e-07    1      143.93
159.28       1.39500e+00    2.00800e-07    1.63975e-07    2.37625e-07    1      140.85
