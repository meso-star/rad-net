#C2H4 volume mixing ratio profile (from nadir data)
#Latitude              :   25.3 S (FP3)
#Longitude             :  173.5 W (FP3)
#Observation date      :  2014-12-10 (T107 flyby)
#Solar longitude       :   62.8�
#Emission angle        :   12.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
140.62       2.99000e+00    1.22500e-07    1.00866e-07    1.44134e-07    1      151.40
133.60       3.61800e+00    1.60800e-07    1.32488e-07    1.89112e-07    1      148.78
126.75       4.37700e+00    2.04300e-07    1.68680e-07    2.39920e-07    1      146.18
120.04       5.29700e+00    2.47400e-07    2.04502e-07    2.90298e-07    1      143.70
