#C2H4 volume mixing ratio profile (from nadir data)
#Latitude              :   35.2 N (FP3)
#Longitude             :  210.8 W (FP3)
#Observation date      :  2015-01-12 (T108 flyby)
#Solar longitude       :   63.8�
#Emission angle        :   24.5�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
139.92       2.99000e+00    1.75500e-07    1.38976e-07    2.12024e-07    1      151.91
132.88       3.61800e+00    2.37200e-07    1.88673e-07    2.85727e-07    1      149.38
126.01       4.37700e+00    3.09100e-07    2.46571e-07    3.71629e-07    1      146.71
119.29       5.29700e+00    3.81200e-07    3.04652e-07    4.57748e-07    1      144.01
