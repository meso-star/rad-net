#C2H4 volume mixing ratio profile (from limb data)
#Latitude              :   84.0 S (FP3)
#Longitude             :  150.1 W (FP3)
#Observation date      :  2016-01-17 (T115 flyby)
#Solar longitude       :   75.1�
#Local time            :  04:10
#Solar zenith angle    :  118�
#Vertical resolution   :  29 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
504.44       4.74500e-04    5.69800e-06    4.61810e-06    6.77790e-06    1      169.79
493.79       5.78400e-04    5.56600e-06    4.55609e-06    6.57591e-06    1      172.13
483.06       7.05000e-04    5.46000e-06    4.51383e-06    6.40617e-06    1      174.53
472.27       8.59300e-04    5.38300e-06    4.49467e-06    6.27133e-06    1      176.85
461.41       1.04700e-03    5.35400e-06    4.51216e-06    6.19584e-06    1      178.90
450.53       1.27700e-03    5.40400e-06    4.59204e-06    6.21596e-06    1      180.48
439.64       1.55600e-03    5.58000e-06    4.77482e-06    6.38518e-06    1      181.36
428.81       1.89700e-03    5.93800e-06    5.10870e-06    6.76730e-06    1      181.40
418.08       2.31200e-03    6.53600e-06    5.64533e-06    7.42667e-06    1      180.56
407.50       2.81800e-03    7.41900e-06    6.42463e-06    8.41337e-06    1      178.81
397.12       3.43500e-03    8.58500e-06    7.44660e-06    9.72340e-06    1      176.38
386.96       4.18700e-03    9.92700e-06    8.61858e-06    1.12354e-05    1      173.60
377.03       5.10300e-03    1.11700e-05    9.70288e-06    1.26371e-05    1      170.79
367.32       6.22000e-03    1.18900e-05    1.03254e-05    1.34546e-05    1      168.10
357.83       7.58100e-03    1.16800e-05    1.01315e-05    1.32285e-05    1      165.55
348.54       9.24100e-03    1.04000e-05    8.99014e-06    1.18099e-05    1      163.13
339.43       1.12600e-02    8.35900e-06    7.18115e-06    9.53685e-06    1      160.81
330.51       1.37300e-02    6.11500e-06    5.20415e-06    7.02585e-06    1      158.65
321.76       1.67300e-02    4.15400e-06    3.49321e-06    4.81479e-06    1      156.62
