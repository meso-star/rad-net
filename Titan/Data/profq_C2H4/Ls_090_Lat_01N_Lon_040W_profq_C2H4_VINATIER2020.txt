#C2H4 volume mixing ratio profile (from nadir data)
#Latitude              :    0.5 N (FP3)
#Longitude             :   40.0 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   56.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
141.57       2.99000e+00    1.53000e-07    1.25249e-07    1.80751e-07    1      153.73
134.43       3.61800e+00    1.82100e-07    1.50300e-07    2.13900e-07    1      151.86
127.40       4.37700e+00    2.12000e-07    1.75769e-07    2.48231e-07    1      150.12
120.50       5.29700e+00    2.38700e-07    1.98575e-07    2.78825e-07    1      148.18
113.74       6.40900e+00    2.58200e-07    2.14763e-07    3.01637e-07    1      145.62
107.14       7.75500e+00    2.69000e-07    2.22953e-07    3.15047e-07    1      142.30
100.75       9.38400e+00    2.69200e-07    2.21659e-07    3.16741e-07    1      138.18
