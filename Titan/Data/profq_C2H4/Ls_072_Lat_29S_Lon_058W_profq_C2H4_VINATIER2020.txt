#C2H4 volume mixing ratio profile (from limb data)
#Latitude              :   28.5 S (FP3)
#Longitude             :   58.1 W (FP3)
#Observation date      :  2015-09-29 (T113 flyby)
#Solar longitude       :   71.7�
#Local time            :  13:58
#Solar zenith angle    :   63�
#Vertical resolution   :  44 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
310.53       5.45700e-02    3.50000e-08    2.52058e-08    4.47942e-08    1      169.51
301.59       6.60300e-02    2.90400e-08    2.08925e-08    3.71875e-08    1      169.71
292.69       7.99000e-02    2.47400e-08    1.77825e-08    3.16975e-08    1      169.99
283.82       9.66800e-02    2.15600e-08    1.55091e-08    2.76109e-08    1      170.43
274.99       1.17000e-01    1.91100e-08    1.37668e-08    2.44532e-08    1      170.86
266.19       1.41600e-01    1.71000e-08    1.23449e-08    2.18551e-08    1      171.22
257.44       1.71300e-01    1.53300e-08    1.11022e-08    1.95578e-08    1      171.36
248.74       2.07300e-01    1.37800e-08    1.00197e-08    1.75403e-08    1      171.15
240.11       2.50800e-01    1.24800e-08    9.14202e-09    1.58180e-08    1      170.58
231.57       3.03500e-01    1.15500e-08    8.53417e-09    1.45658e-08    1      169.71
223.13       3.67200e-01    1.11200e-08    8.30949e-09    1.39305e-08    1      168.61
214.80       4.44300e-01    1.13700e-08    8.60271e-09    1.41373e-08    1      167.38
206.58       5.37600e-01    1.25500e-08    9.65197e-09    1.54480e-08    1      166.26
198.45       6.50600e-01    1.50700e-08    1.17880e-08    1.83520e-08    1      165.45
190.40       7.87200e-01    1.96300e-08    1.55737e-08    2.36863e-08    1      164.73
182.44       9.52500e-01    2.72700e-08    2.19041e-08    3.26359e-08    1      163.93
174.57       1.15300e+00    3.92600e-08    3.18844e-08    4.66356e-08    1      163.07
166.78       1.39500e+00    5.65500e-08    4.63748e-08    6.67252e-08    1      162.05
159.10       1.68800e+00    7.85200e-08    6.50622e-08    9.19778e-08    1      160.57
151.55       2.04200e+00    1.01700e-07    8.51720e-08    1.18228e-07    1      158.51
144.15       2.47100e+00    1.20900e-07    1.01965e-07    1.39835e-07    1      155.75
136.93       2.99000e+00    1.30700e-07    1.10192e-07    1.51208e-07    1      152.18
129.94       3.61800e+00    1.29600e-07    1.08071e-07    1.51129e-07    1      147.81
124.45       4.22100e+00    1.23000e-07    1.00804e-07    1.45196e-07    1      144.46
