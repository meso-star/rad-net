#C2H4 volume mixing ratio profile (from limb data)
#Latitude              :   71.3 N (FP3)
#Longitude             :  256.9 W (FP3)
#Observation date      :  2015-05-09 (T111 flyby)
#Solar longitude       :   67.4�
#Local time            :  01:21
#Solar zenith angle    :   86�
#Vertical resolution   :  37 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
392.59       1.12600e-02    1.98300e-07    1.08547e-07    2.88053e-07    1      170.52
382.65       1.37300e-02    1.50400e-07    8.29618e-08    2.17838e-07    1      172.74
372.66       1.67300e-02    1.07200e-07    5.96448e-08    1.54755e-07    1      174.83
362.62       2.04000e-02    7.36500e-08    4.14544e-08    1.05846e-07    1      176.73
352.55       2.48600e-02    5.06800e-08    2.89737e-08    7.23863e-08    1      178.40
342.46       3.03000e-02    3.66500e-08    2.14232e-08    5.18768e-08    1      179.81
332.37       3.69400e-02    2.93700e-08    1.77499e-08    4.09901e-08    1      180.86
322.31       4.50200e-02    2.74400e-08    1.73170e-08    3.75630e-08    1      181.39
312.28       5.48700e-02    3.10500e-08    2.07290e-08    4.13710e-08    1      181.94
302.29       6.68800e-02    4.33100e-08    3.07514e-08    5.58686e-08    1      182.66
292.33       8.15200e-02    7.35300e-08    5.55819e-08    9.14781e-08    1      183.45
282.40       9.93700e-02    1.45300e-07    1.15885e-07    1.74715e-07    1      184.19
272.51       1.21100e-01    3.10100e-07    2.57042e-07    3.63158e-07    1      184.40
262.70       1.47600e-01    6.50200e-07    5.49464e-07    7.50936e-07    1      183.79
253.01       1.79900e-01    1.21300e-06    1.03278e-06    1.39322e-06    1      182.17
243.50       2.19300e-01    1.85200e-06    1.58317e-06    2.12083e-06    1      179.57
234.21       2.67300e-01    2.19100e-06    1.87940e-06    2.50260e-06    1      176.28
225.16       3.25900e-01    1.99300e-06    1.70198e-06    2.28402e-06    1      172.74
216.34       3.97200e-01    1.44400e-06    1.21195e-06    1.67605e-06    1      169.30
207.74       4.84100e-01    8.99900e-07    7.32429e-07    1.06737e-06    1      166.26
199.34       5.90100e-01    5.31500e-07    4.16854e-07    6.46147e-07    1      163.68
191.11       7.19200e-01    3.28500e-07    2.48986e-07    4.08014e-07    1      161.46
183.04       8.76700e-01    2.30300e-07    1.70214e-07    2.90386e-07    1      159.41
175.12       1.06900e+00    1.92100e-07    1.39908e-07    2.44292e-07    1      157.34
167.35       1.30200e+00    1.92400e-07    1.39651e-07    2.45149e-07    1      154.98
159.76       1.58800e+00    2.25200e-07    1.64029e-07    2.86371e-07    1      152.08
152.36       1.93500e+00    2.93000e-07    2.14601e-07    3.71399e-07    1      148.77
145.15       2.35900e+00    3.97800e-07    2.92990e-07    5.02610e-07    1      145.64
138.14       2.87500e+00    5.31400e-07    3.92228e-07    6.70572e-07    1      142.42
131.32       3.50400e+00    6.66500e-07    4.91007e-07    8.41993e-07    1      139.14
124.68       4.27100e+00    7.63300e-07    5.58037e-07    9.68563e-07    1      136.43
118.19       5.20600e+00    7.90500e-07    5.70044e-07    1.01096e-06    1      134.17
