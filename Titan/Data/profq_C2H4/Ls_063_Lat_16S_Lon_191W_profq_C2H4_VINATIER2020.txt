#C2H4 volume mixing ratio profile (from nadir data)
#Latitude              :   16.4 S (FP3)
#Longitude             :  190.9 W (FP3)
#Observation date      :  2014-12-10 (T107 flyby)
#Solar longitude       :   62.8�
#Emission angle        :   16.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
141.46       2.99000e+00    1.44600e-07    1.19229e-07    1.69971e-07    1      154.20
134.31       3.61800e+00    1.92100e-07    1.58883e-07    2.25317e-07    1      151.63
127.32       4.37700e+00    2.46800e-07    2.04500e-07    2.89100e-07    1      149.00
120.49       5.29700e+00    3.01400e-07    2.49914e-07    3.52886e-07    1      146.37
