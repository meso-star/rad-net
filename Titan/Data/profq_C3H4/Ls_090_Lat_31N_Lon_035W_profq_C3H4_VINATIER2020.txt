#C3H4 volume mixing ratio profile (from nadir data)
#Latitude              :   31.2 N (FP3)
#Longitude             :   34.7 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   23.1�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
220.02       4.44300e-01    1.44700e-08    1.08847e-08    1.80553e-08    1      172.71
211.50       5.37600e-01    1.37400e-08    1.04239e-08    1.70561e-08    1      171.76
203.08       6.50600e-01    1.32700e-08    1.01724e-08    1.63676e-08    1      170.76
194.77       7.87200e-01    1.29400e-08    1.00170e-08    1.58630e-08    1      169.21
186.60       9.52500e-01    1.26100e-08    9.86030e-09    1.53597e-08    1      166.97
178.60       1.15300e+00    1.21900e-08    9.61796e-09    1.47620e-08    1      164.31
170.78       1.39500e+00    1.15700e-08    9.20409e-09    1.39359e-08    1      161.33
163.14       1.68800e+00    1.08000e-08    8.66221e-09    1.29378e-08    1      158.44
155.68       2.04200e+00    9.95300e-09    8.01562e-09    1.18904e-08    1      155.84
148.36       2.47100e+00    9.14600e-09    7.37885e-09    1.09132e-08    1      153.70
141.19       2.99000e+00    8.39400e-09    6.75970e-09    1.00283e-08    1      151.76
134.13       3.61800e+00    7.46300e-09    5.97712e-09    8.94888e-09    1      150.32
127.17       4.37700e+00    6.55900e-09    5.20773e-09    7.91027e-09    1      148.96
120.31       5.29700e+00    5.85400e-09    4.59686e-09    7.11114e-09    1      147.33
113.58       6.40900e+00    4.96500e-09    3.84793e-09    6.08207e-09    1      145.01
107.01       7.75500e+00    4.08600e-09    3.12460e-09    5.04740e-09    1      141.83
100.64       9.38400e+00    3.32400e-09    2.50912e-09    4.13888e-09    1      137.81
 94.49       1.13600e+01    2.73200e-09    2.03617e-09    3.42783e-09    1      133.23
