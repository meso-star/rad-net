#C3H4 volume mixing ratio profile (from nadir data)
#Latitude              :   70.2 N (FP3)
#Longitude             :  162.7 W (FP3)
#Observation date      :   2017-09-12 (S101/293TI flyby)
#Solar longitude       :   93.3�
#Emission angle        :   23.5�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
511.72       1.20500e-03    1.39300e-08    9.90498e-09    1.79550e-08    1      167.07
501.62       1.45800e-03    1.36300e-08    9.69234e-09    1.75677e-08    1      167.97
491.53       1.76500e-03    1.34000e-08    9.52810e-09    1.72719e-08    1      168.57
481.48       2.13500e-03    1.32600e-08    9.43131e-09    1.70887e-08    1      168.85
471.50       2.58400e-03    1.32300e-08    9.40274e-09    1.70573e-08    1      168.82
461.58       3.12700e-03    1.33100e-08    9.47894e-09    1.71411e-08    1      168.59
451.74       3.78300e-03    1.35200e-08    9.62413e-09    1.74159e-08    1      168.31
441.98       4.57800e-03    1.38600e-08    9.85658e-09    1.78634e-08    1      168.21
432.28       5.53900e-03    1.43100e-08    1.01755e-08    1.84445e-08    1      168.48
422.59       6.70300e-03    1.48900e-08    1.05817e-08    1.91983e-08    1      169.73
412.87       8.11000e-03    1.55900e-08    1.10840e-08    2.00960e-08    1      171.87
403.08       9.81400e-03    1.64200e-08    1.16918e-08    2.11482e-08    1      174.51
393.20       1.18700e-02    1.74700e-08    1.24360e-08    2.25040e-08    1      177.38
383.24       1.43700e-02    1.87500e-08    1.33444e-08    2.41556e-08    1      179.75
373.23       1.73900e-02    2.04100e-08    1.45417e-08    2.62783e-08    1      181.54
363.21       2.10400e-02    2.25200e-08    1.60580e-08    2.89820e-08    1      182.60
353.22       2.54600e-02    2.52200e-08    1.80082e-08    3.24318e-08    1      183.09
343.27       3.08000e-02    2.86700e-08    2.04900e-08    3.68500e-08    1      183.16
333.40       3.72700e-02    3.29200e-08    2.36056e-08    4.22344e-08    1      182.96
323.62       4.51000e-02    3.80500e-08    2.73764e-08    4.87236e-08    1      182.21
313.94       5.45700e-02    4.40100e-08    3.18455e-08    5.61745e-08    1      181.66
304.35       6.60300e-02    5.05300e-08    3.67917e-08    6.42683e-08    1      181.25
294.85       7.99000e-02    5.72800e-08    4.20813e-08    7.24787e-08    1      180.79
285.43       9.66800e-02    6.37400e-08    4.73086e-08    8.01714e-08    1      180.37
276.10       1.17000e-01    6.92900e-08    5.20617e-08    8.65183e-08    1      179.74
266.87       1.41600e-01    7.33800e-08    5.59212e-08    9.08388e-08    1      179.01
257.73       1.71300e-01    7.55600e-08    5.84560e-08    9.26640e-08    1      178.11
248.71       2.07300e-01    7.56500e-08    5.94550e-08    9.18450e-08    1      176.97
239.81       2.50800e-01    7.36200e-08    5.87164e-08    8.85236e-08    1      175.53
231.05       3.03500e-01    6.96400e-08    5.62683e-08    8.30117e-08    1      173.72
222.44       3.67200e-01    6.41500e-08    5.23676e-08    7.59324e-08    1      171.38
214.02       4.44300e-01    5.75700e-08    4.72624e-08    6.78776e-08    1      168.55
205.79       5.37600e-01    5.05000e-08    4.15202e-08    5.94798e-08    1      165.44
197.75       6.50600e-01    4.34000e-08    3.55841e-08    5.12159e-08    1      162.82
189.87       7.87200e-01    3.66500e-08    2.98470e-08    4.34530e-08    1      160.56
182.15       9.52500e-01    3.03600e-08    2.44997e-08    3.62203e-08    1      158.42
174.55       1.15300e+00    2.44700e-08    1.95340e-08    2.94060e-08    1      156.81
167.08       1.39500e+00    1.91800e-08    1.51224e-08    2.32376e-08    1      155.22
159.73       1.68800e+00    1.47000e-08    1.14476e-08    1.79524e-08    1      153.46
152.51       2.04200e+00    1.10500e-08    8.51004e-09    1.35900e-08    1      151.33
145.43       2.47100e+00    8.21900e-09    6.26220e-09    1.01758e-08    1      148.97
138.49       2.99000e+00    6.15800e-09    4.64876e-09    7.66724e-09    1      146.75
