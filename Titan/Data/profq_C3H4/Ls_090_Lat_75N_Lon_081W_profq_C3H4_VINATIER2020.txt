#C3H4 volume mixing ratio profile (from nadir data)
#Latitude              :   74.7 N (FP3)
#Longitude             :   80.7 W (FP3)
#Observation date      :   2017-06-08 (S100/278TI flyby)
#Solar longitude       :   90.4�
#Emission angle        :   26.0�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
183.01       9.52500e-01    2.48700e-08    1.92392e-08    3.05008e-08    1      159.08
175.41       1.15300e+00    2.53100e-08    1.97881e-08    3.08319e-08    1      156.19
167.99       1.39500e+00    2.53100e-08    2.00222e-08    3.05978e-08    1      153.26
160.75       1.68800e+00    2.48800e-08    1.98998e-08    2.98602e-08    1      150.68
153.65       2.04200e+00    2.40400e-08    1.94234e-08    2.86566e-08    1      148.61
146.67       2.47100e+00    2.30100e-08    1.87350e-08    2.72850e-08    1      147.17
139.79       2.99000e+00    2.18100e-08    1.78096e-08    2.58104e-08    1      146.02
132.99       3.61800e+00    1.97900e-08    1.61406e-08    2.34394e-08    1      145.38
126.25       4.37700e+00    1.75000e-08    1.41901e-08    2.08099e-08    1      144.79
119.57       5.29700e+00    1.54800e-08    1.24086e-08    1.85514e-08    1      143.88
112.99       6.40900e+00    1.27900e-08    1.01290e-08    1.54510e-08    1      142.17
106.54       7.75500e+00    1.00900e-08    7.85796e-09    1.23220e-08    1      139.51
100.26       9.38400e+00    7.75000e-09    5.93332e-09    9.56668e-09    1      135.91
 94.19       1.13600e+01    5.93000e-09    4.46865e-09    7.39135e-09    1      131.73
