#C3H4 volume mixing ratio profile (from limb data)
#Latitude              :   31.7 N (FP3)
#Longitude             :  309.8 W (FP3)
#Observation date      :  2016-02-02 (T116 flyby)
#Solar longitude       :   75.6�
#Local time            :  18:18
#Solar zenith angle    :   82�
#Vertical resolution   :  43 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
461.04       3.12700e-03    3.34300e-08    2.78888e-08    3.89712e-08    1      162.43
451.55       3.78300e-03    3.44200e-08    2.87602e-08    4.00798e-08    1      162.66
442.10       4.57800e-03    3.56700e-08    2.98694e-08    4.14706e-08    1      163.17
432.67       5.53900e-03    3.71400e-08    3.11792e-08    4.31008e-08    1      164.04
423.24       6.70300e-03    3.87200e-08    3.25970e-08    4.48430e-08    1      165.19
413.80       8.11000e-03    4.02500e-08    3.39848e-08    4.65152e-08    1      166.49
404.34       9.81400e-03    4.15400e-08    3.51855e-08    4.78945e-08    1      167.83
394.87       1.18700e-02    4.23800e-08    3.60152e-08    4.87448e-08    1      169.16
385.39       1.43700e-02    4.26300e-08    3.63379e-08    4.89221e-08    1      170.43
375.89       1.73900e-02    4.22200e-08    3.60903e-08    4.83497e-08    1      171.68
366.39       2.10400e-02    4.08700e-08    3.50118e-08    4.67282e-08    1      173.02
356.87       2.54600e-02    3.86300e-08    3.31638e-08    4.40962e-08    1      174.50
347.33       3.08000e-02    3.57300e-08    3.07323e-08    4.07277e-08    1      176.05
337.77       3.72700e-02    3.24600e-08    2.79658e-08    3.69542e-08    1      177.38
328.22       4.51000e-02    2.92400e-08    2.52361e-08    3.32439e-08    1      178.29
318.69       5.45700e-02    2.63200e-08    2.27550e-08    2.98850e-08    1      178.89
309.19       6.60300e-02    2.39400e-08    2.07321e-08    2.71479e-08    1      179.26
299.75       7.99000e-02    2.21300e-08    1.91946e-08    2.50654e-08    1      179.38
290.36       9.66800e-02    2.08500e-08    1.81189e-08    2.35811e-08    1      179.28
281.05       1.17000e-01    1.99700e-08    1.73879e-08    2.25521e-08    1      178.98
271.82       1.41600e-01    1.93700e-08    1.68893e-08    2.18507e-08    1      178.48
262.68       1.71300e-01    1.88700e-08    1.64788e-08    2.12612e-08    1      177.78
253.64       2.07300e-01    1.83600e-08    1.60483e-08    2.06717e-08    1      176.87
244.70       2.50800e-01    1.77700e-08    1.55415e-08    1.99985e-08    1      175.76
235.88       3.03500e-01    1.71400e-08    1.50032e-08    1.92768e-08    1      174.53
227.18       3.67200e-01    1.65600e-08    1.45045e-08    1.86155e-08    1      173.17
218.61       4.44300e-01    1.61300e-08    1.41430e-08    1.81170e-08    1      171.71
210.16       5.37600e-01    1.59600e-08    1.40112e-08    1.79088e-08    1      170.23
201.83       6.50600e-01    1.61000e-08    1.41461e-08    1.80539e-08    1      168.85
193.62       7.87200e-01    1.65100e-08    1.45064e-08    1.85136e-08    1      167.34
185.54       9.52500e-01    1.70500e-08    1.49820e-08    1.91180e-08    1      165.54
177.59       1.15300e+00    1.74800e-08    1.53567e-08    1.96033e-08    1      163.59
169.79       1.39500e+00    1.74900e-08    1.53723e-08    1.96077e-08    1      161.55
162.13       1.68800e+00    1.67600e-08    1.47440e-08    1.87760e-08    1      159.41
154.62       2.04200e+00    1.52400e-08    1.34200e-08    1.70600e-08    1      157.19
147.25       2.47100e+00    1.30600e-08    1.15028e-08    1.46172e-08    1      154.95
140.03       2.99000e+00    1.06200e-08    9.34776e-09    1.18922e-08    1      152.64
132.96       3.61800e+00    8.29800e-09    7.28894e-09    9.30706e-09    1      150.06
126.05       4.37700e+00    6.33800e-09    5.55627e-09    7.11973e-09    1      147.43
119.29       5.29700e+00    4.84200e-09    4.23747e-09    5.44653e-09    1      145.00
