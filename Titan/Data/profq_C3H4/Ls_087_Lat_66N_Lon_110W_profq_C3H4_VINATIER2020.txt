#C3H4 volume mixing ratio profile (from nadir data)
#Latitude              :   66.0 N (FP3)
#Longitude             :  109.6 W (FP3)
#Observation date      :   2017-02-17 (S98/262TI flyby)
#Solar longitude       :   87.1�
#Emission angle        :   49.5�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
191.50       7.87200e-01    3.11800e-08    2.26092e-08    3.97508e-08    1      158.69
183.87       9.52500e-01    4.11000e-08    3.12241e-08    5.09759e-08    1      156.06
176.38       1.15300e+00    4.91700e-08    3.89999e-08    5.93401e-08    1      154.33
169.01       1.39500e+00    5.19900e-08    4.25514e-08    6.14286e-08    1      153.31
161.70       1.68800e+00    4.84500e-08    3.99959e-08    5.69041e-08    1      152.82
154.46       2.04200e+00    3.99000e-08    3.23910e-08    4.74090e-08    1      152.50
147.28       2.47100e+00    2.97000e-08    2.32652e-08    3.61348e-08    1      151.93
140.17       2.99000e+00    2.05900e-08    1.54190e-08    2.57610e-08    1      150.54
