#C3H4 volume mixing ratio profile (from nadir data)
#Latitude              :   64.1 S (FP3)
#Longitude             :  103.4 W (FP3)
#Observation date      :   2017-02-01 (S97/259TI flyby)
#Solar longitude       :   86.6�
#Emission angle        :   35.3�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
251.78       1.17000e-01    9.40200e-08    7.54645e-08    1.12576e-07    1      172.18
243.14       1.41600e-01    8.44500e-08    6.72958e-08    1.01604e-07    1      169.31
234.71       1.71300e-01    7.41500e-08    5.85191e-08    8.97809e-08    1      166.02
226.50       2.07300e-01    6.34000e-08    4.94775e-08    7.73225e-08    1      162.35
218.53       2.50800e-01    5.26800e-08    4.06239e-08    6.47361e-08    1      158.35
210.80       3.03500e-01    4.29100e-08    3.27080e-08    5.31120e-08    1      154.24
203.32       3.67200e-01    3.44900e-08    2.60077e-08    4.29723e-08    1      150.10
196.08       4.44300e-01    2.77500e-08    2.07153e-08    3.47847e-08    1      146.09
189.06       5.37600e-01    2.25600e-08    1.67101e-08    2.84099e-08    1      142.33
182.24       6.50600e-01    1.87200e-08    1.37688e-08    2.36712e-08    1      139.33
175.59       7.87200e-01    1.58700e-08    1.16043e-08    2.01357e-08    1      136.70
169.09       9.52500e-01    1.37500e-08    1.00244e-08    1.74756e-08    1      134.30
162.73       1.15300e+00    1.21100e-08    8.79200e-09    1.54280e-08    1      132.24
156.50       1.39500e+00    1.07500e-08    7.77483e-09    1.37252e-08    1      130.32
150.38       1.68800e+00    9.57800e-09    6.91820e-09    1.22378e-08    1      128.64
