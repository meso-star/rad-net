#C2H6 volume mixing ratio profile (from limb data)
#Latitude              :    5.9 N (FP3)
#Longitude             :  306.4 W (FP3)
#Observation date      : 2015-11-213 (T114 flyby)
#Solar longitude       :   73.1�
#Local time            :  17:46
#Solar zenith angle    :   85�
#Vertical resolution   :  28 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
521.98       9.96100e-04    2.10700e-05    1.79729e-05    2.41671e-05    1      161.80
512.15       1.20500e-03    2.10600e-05    1.79903e-05    2.41297e-05    1      161.78
502.39       1.45800e-03    2.10300e-05    1.79892e-05    2.40708e-05    1      161.72
492.70       1.76500e-03    2.09800e-05    1.79696e-05    2.39904e-05    1      161.61
483.07       2.13500e-03    2.08900e-05    1.79261e-05    2.38539e-05    1      161.45
473.51       2.58400e-03    2.07400e-05    1.78276e-05    2.36524e-05    1      161.21
464.03       3.12700e-03    2.05100e-05    1.76682e-05    2.33518e-05    1      160.95
454.62       3.78300e-03    2.01800e-05    1.74401e-05    2.29199e-05    1      160.85
445.26       4.57800e-03    1.97800e-05    1.71246e-05    2.24354e-05    1      161.09
435.94       5.53900e-03    1.93000e-05    1.67568e-05    2.18432e-05    1      161.77
426.62       6.70300e-03    1.88100e-05    1.63577e-05    2.12623e-05    1      162.83
417.29       8.11000e-03    1.83400e-05    1.59844e-05    2.06956e-05    1      164.19
407.94       9.81400e-03    1.79400e-05    1.56710e-05    2.02090e-05    1      165.73
398.55       1.18700e-02    1.76500e-05    1.54524e-05    1.98476e-05    1      167.37
389.13       1.43700e-02    1.74800e-05    1.53229e-05    1.96371e-05    1      169.02
379.69       1.73900e-02    1.74100e-05    1.52871e-05    1.95329e-05    1      170.65
370.21       2.10400e-02    1.73800e-05    1.52781e-05    1.94819e-05    1      172.31
360.70       2.54600e-02    1.73300e-05    1.52444e-05    1.94156e-05    1      174.01
351.15       3.08000e-02    1.71700e-05    1.51125e-05    1.92275e-05    1      175.68
341.59       3.72700e-02    1.68400e-05    1.48294e-05    1.88506e-05    1      177.10
332.02       4.51000e-02    1.63500e-05    1.43999e-05    1.83001e-05    1      178.13
322.47       5.45700e-02    1.57300e-05    1.38594e-05    1.76006e-05    1      178.95
312.94       6.60300e-02    1.50900e-05    1.33007e-05    1.68793e-05    1      179.71
303.43       7.99000e-02    1.45200e-05    1.28033e-05    1.62367e-05    1      180.40
293.95       9.66800e-02    1.41100e-05    1.24466e-05    1.57734e-05    1      181.01
284.50       1.17000e-01    1.39200e-05    1.22812e-05    1.55588e-05    1      181.44
275.11       1.41600e-01    1.39500e-05    1.23099e-05    1.55901e-05    1      181.56
265.78       1.71300e-01    1.41800e-05    1.25131e-05    1.58469e-05    1      181.24
256.54       2.07300e-01    1.45200e-05    1.28135e-05    1.62265e-05    1      180.40
247.41       2.50800e-01    1.48800e-05    1.31333e-05    1.66267e-05    1      179.09
238.42       3.03500e-01    1.51600e-05    1.33806e-05    1.69394e-05    1      177.45
229.57       3.67200e-01    1.52800e-05    1.34866e-05    1.70734e-05    1      175.65
220.86       4.44300e-01    1.52000e-05    1.34175e-05    1.69825e-05    1      173.84
212.30       5.37600e-01    1.49600e-05    1.32055e-05    1.67145e-05    1      172.27
203.85       6.50600e-01    1.45900e-05    1.28800e-05    1.63000e-05    1      171.07
195.51       7.87200e-01    1.41500e-05    1.24914e-05    1.58086e-05    1      169.99
187.27       9.52500e-01    1.36700e-05    1.20675e-05    1.52725e-05    1      168.74
179.15       1.15300e+00    1.31300e-05    1.15918e-05    1.46682e-05    1      167.31
171.15       1.39500e+00    1.25300e-05    1.10619e-05    1.39981e-05    1      165.63
163.29       1.68800e+00    1.18400e-05    1.04526e-05    1.32274e-05    1      163.60
155.58       2.04200e+00    1.10600e-05    9.76377e-06    1.23562e-05    1      161.22
148.02       2.47100e+00    1.02400e-05    9.03962e-06    1.14404e-05    1      158.63
140.63       2.99000e+00    9.44700e-06    8.33887e-06    1.05551e-05    1      155.86
133.42       3.61800e+00    8.74700e-06    7.72029e-06    9.77371e-06    1      152.84
126.39       4.37700e+00    8.21100e-06    7.24647e-06    9.17553e-06    1      149.84
119.52       5.29700e+00    7.88800e-06    6.96053e-06    8.81547e-06    1      147.01
