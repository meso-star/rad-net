#C2H6 volume mixing ratio profile (from limb data)
#Latitude              :   78.9 N (FP3)
#Longitude             :   63.4 W (FP3)
#Observation date      :  2015-05-09 (T111 flyby)
#Solar longitude       :   67.4�
#Local time            :  14:12
#Solar zenith angle    :   58�
#Vertical resolution   :  29 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
514.94       1.04700e-03    2.10600e-05    1.68199e-05    2.53001e-05    1      167.53
504.41       1.27700e-03    2.32000e-05    1.87384e-05    2.76616e-05    1      168.05
493.92       1.55600e-03    2.60100e-05    2.12262e-05    3.07938e-05    1      168.27
483.51       1.89700e-03    2.93400e-05    2.42330e-05    3.44470e-05    1      168.14
473.19       2.31200e-03    3.29700e-05    2.75047e-05    3.84353e-05    1      167.69
462.97       2.81800e-03    3.64600e-05    3.07191e-05    4.22008e-05    1      166.98
452.86       3.43500e-03    3.93200e-05    3.33644e-05    4.52756e-05    1      166.24
442.85       4.18700e-03    4.01200e-05    3.42752e-05    4.59648e-05    1      165.80
432.92       5.10300e-03    3.81000e-05    3.26916e-05    4.35084e-05    1      165.90
423.03       6.22000e-03    3.43400e-05    2.95800e-05    3.91000e-05    1      166.64
413.15       7.58100e-03    3.02300e-05    2.61154e-05    3.43446e-05    1      168.08
403.22       9.24100e-03    2.67700e-05    2.32147e-05    3.03253e-05    1      170.20
393.21       1.12600e-02    2.44500e-05    2.12728e-05    2.76272e-05    1      173.09
383.09       1.37300e-02    2.33200e-05    2.03503e-05    2.62897e-05    1      176.36
372.85       1.67300e-02    2.31600e-05    2.02741e-05    2.60459e-05    1      179.73
362.51       2.04000e-02    2.36300e-05    2.07306e-05    2.65294e-05    1      182.65
352.08       2.48600e-02    2.42500e-05    2.13142e-05    2.71858e-05    1      184.89
341.63       3.03000e-02    2.46200e-05    2.16702e-05    2.75698e-05    1      186.30
331.20       3.69400e-02    2.44400e-05    2.15288e-05    2.73512e-05    1      186.98
320.82       4.50200e-02    2.36900e-05    2.08759e-05    2.65041e-05    1      186.88
310.52       5.48700e-02    2.25900e-05    1.99079e-05    2.52721e-05    1      187.05
300.26       6.68800e-02    2.14300e-05    1.88868e-05    2.39732e-05    1      187.78
290.04       8.15200e-02    2.04600e-05    1.80337e-05    2.28863e-05    1      188.77
279.84       9.93700e-02    1.99200e-05    1.75608e-05    2.22792e-05    1      189.36
269.72       1.21100e-01    1.99200e-05    1.75608e-05    2.22792e-05    1      188.44
259.77       1.47600e-01    2.05200e-05    1.80914e-05    2.29486e-05    1      185.72
250.07       1.79900e-01    2.18300e-05    1.92453e-05    2.44147e-05    1      181.62
240.66       2.19300e-01    2.37500e-05    2.09339e-05    2.65661e-05    1      177.09
231.53       2.67300e-01    2.60700e-05    2.29763e-05    2.91637e-05    1      173.05
222.65       3.25900e-01    2.82400e-05    2.48908e-05    3.15892e-05    1      169.87
213.98       3.97200e-01    2.95500e-05    2.60446e-05    3.30554e-05    1      167.22
205.49       4.84100e-01    2.93600e-05    2.58765e-05    3.28435e-05    1      164.71
197.18       5.90100e-01    2.76700e-05    2.43815e-05    3.09585e-05    1      162.11
189.06       7.19200e-01    2.51200e-05    2.21306e-05    2.81094e-05    1      159.44
181.11       8.76700e-01    2.26900e-05    1.99799e-05    2.54001e-05    1      156.84
173.34       1.06900e+00    2.11600e-05    1.86251e-05    2.36949e-05    1      154.35
165.74       1.30200e+00    2.01200e-05    1.77104e-05    2.25296e-05    1      151.66
158.32       1.58800e+00    1.94100e-05    1.70816e-05    2.17384e-05    1      148.38
151.12       1.93500e+00    1.87900e-05    1.65323e-05    2.10477e-05    1      144.71
144.13       2.35900e+00    1.80200e-05    1.58428e-05    2.01972e-05    1      141.22
137.33       2.87500e+00    1.68400e-05    1.47882e-05    1.88918e-05    1      138.45
130.71       3.50400e+00    1.51000e-05    1.32237e-05    1.69763e-05    1      135.23
124.27       4.27100e+00    1.28400e-05    1.11986e-05    1.44814e-05    1      132.04
118.00       5.20600e+00    1.02800e-05    8.92180e-06    1.16382e-05    1      129.46
