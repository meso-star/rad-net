#C2H6 volume mixing ratio profile (from limb data)
#Latitude              :   58.6 S (FP3)
#Longitude             :  131.0 W (FP3)
#Observation date      :  2016-01-17 (T115 flyby)
#Solar longitude       :   75.1�
#Local time            :  06:04
#Solar zenith angle    :  113�
#Vertical resolution   :  35 km
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
534.60       4.64600e-04    2.25800e-05    1.92401e-05    2.59199e-05    1      158.70
524.84       5.62200e-04    2.31100e-05    1.96843e-05    2.65357e-05    1      160.08
515.05       6.80300e-04    2.38800e-05    2.03646e-05    2.73954e-05    1      161.67
505.22       8.23200e-04    2.49200e-05    2.12713e-05    2.85687e-05    1      163.44
495.35       9.96100e-04    2.62600e-05    2.24672e-05    3.00528e-05    1      165.33
485.42       1.20500e-03    2.79200e-05    2.39340e-05    3.19060e-05    1      167.27
475.44       1.45800e-03    2.98300e-05    2.56256e-05    3.40344e-05    1      169.13
465.43       1.76500e-03    3.18600e-05    2.74402e-05    3.62798e-05    1      170.82
455.39       2.13500e-03    3.38700e-05    2.92500e-05    3.84900e-05    1      172.22
445.34       2.58400e-03    3.55900e-05    3.08109e-05    4.03691e-05    1      173.22
435.32       3.12700e-03    3.68200e-05    3.19573e-05    4.16827e-05    1      173.80
425.34       3.78300e-03    3.73000e-05    3.24674e-05    4.21326e-05    1      174.04
415.42       4.57800e-03    3.69700e-05    3.22473e-05    4.16927e-05    1      174.05
405.57       5.53900e-03    3.59300e-05    3.14007e-05    4.04593e-05    1      173.91
395.80       6.70300e-03    3.43700e-05    3.00864e-05    3.86536e-05    1      173.59
386.11       8.11000e-03    3.25800e-05    2.85539e-05    3.66061e-05    1      173.05
376.52       9.81400e-03    3.08200e-05    2.70364e-05    3.46036e-05    1      172.30
367.04       1.18700e-02    2.93400e-05    2.57486e-05    3.29314e-05    1      171.41
357.67       1.43700e-02    2.82100e-05    2.47711e-05    3.16489e-05    1      170.44
348.41       1.73900e-02    2.74200e-05    2.40783e-05    3.07617e-05    1      169.53
339.26       2.10400e-02    2.69100e-05    2.36256e-05    3.01944e-05    1      168.79
330.19       2.54600e-02    2.65500e-05    2.33060e-05    2.97940e-05    1      168.33
321.20       3.08000e-02    2.61200e-05    2.29328e-05    2.93072e-05    1      168.06
312.28       3.72700e-02    2.55600e-05    2.24356e-05    2.86844e-05    1      167.69
303.45       4.51000e-02    2.47000e-05    2.16891e-05    2.77109e-05    1      166.83
294.72       5.45700e-02    2.35900e-05    2.07206e-05    2.64594e-05    1      165.72
286.11       6.60300e-02    2.22800e-05    1.95744e-05    2.49856e-05    1      164.39
277.63       7.99000e-02    2.09500e-05    1.84025e-05    2.34975e-05    1      162.80
269.28       9.66800e-02    1.97500e-05    1.73458e-05    2.21542e-05    1      161.17
261.06       1.17000e-01    1.87900e-05    1.65021e-05    2.10779e-05    1      159.51
252.97       1.41600e-01    1.81700e-05    1.59601e-05    2.03799e-05    1      158.00
245.00       1.71300e-01    1.79400e-05    1.57560e-05    2.01240e-05    1      156.66
237.14       2.07300e-01    1.80600e-05    1.58705e-05    2.02495e-05    1      155.52
229.37       2.50800e-01    1.85200e-05    1.62785e-05    2.07615e-05    1      154.59
221.68       3.03500e-01    1.92100e-05    1.69043e-05    2.15157e-05    1      153.90
214.07       3.67200e-01    1.99800e-05    1.75926e-05    2.23674e-05    1      153.29
206.53       4.44300e-01    2.05900e-05    1.81436e-05    2.30364e-05    1      152.65
199.07       5.37600e-01    2.08500e-05    1.83736e-05    2.33264e-05    1      151.84
191.69       6.50600e-01    2.05200e-05    1.80867e-05    2.29533e-05    1      150.91
184.39       7.87200e-01    1.94900e-05    1.71756e-05    2.18044e-05    1      149.89
177.20       9.52500e-01    1.78000e-05    1.56856e-05    1.99144e-05    1      148.25
170.13       1.15300e+00    1.56500e-05    1.37938e-05    1.75062e-05    1      146.26
163.20       1.39500e+00    1.32400e-05    1.16669e-05    1.48131e-05    1      144.04
156.41       1.68800e+00    1.08800e-05    9.58407e-06    1.21759e-05    1      141.72
149.75       2.04200e+00    8.75900e-06    7.70670e-06    9.81130e-06    1      140.12
143.18       2.47100e+00    6.99900e-06    6.14859e-06    7.84941e-06    1      139.13
136.69       2.99000e+00    5.62800e-06    4.93374e-06    6.32226e-06    1      138.19
130.27       3.61800e+00    4.61300e-06    4.03679e-06    5.18921e-06    1      137.19
123.93       4.37700e+00    3.90400e-06    3.40828e-06    4.39972e-06    1      135.97
117.69       5.29700e+00    3.44700e-06    3.00265e-06    3.89135e-06    1      134.65
