#C2H6 volume mixing ratio profile (from nadir data)
#Latitude              :   58.7 N (FP3)
#Longitude             :  218.0 W (FP3)
#Observation date      :  2016-06-08 (T120 flyby)
#Solar longitude       :   79.4�
#Emission angle        :   22.7�
#          
#                   
#Reference             :  Vinatier et al. (2020), A&A ; https://doi.org/10.1051/0004-6361/202038411
#Sigma                 :  {0 = a priori; 1 = 1-sigma error, 2 = 2-sigma upper limit}
#  
#Altitude(km) Pressure(mbar) q(VMR)         q_min(VMR)     q_max(VMR)     sigma  Temperature(K)
286.14       9.66800e-02    1.85200e-05    1.44455e-05    2.25945e-05    1      181.72
276.74       1.17000e-01    1.77600e-05    1.39105e-05    2.16095e-05    1      180.80
267.47       1.41600e-01    1.70500e-05    1.34218e-05    2.06782e-05    1      179.54
258.32       1.71300e-01    1.66200e-05    1.31694e-05    2.00706e-05    1      178.04
249.31       2.07300e-01    1.66500e-05    1.32713e-05    2.00287e-05    1      176.46
240.43       2.50800e-01    1.70400e-05    1.36832e-05    2.03968e-05    1      174.92
231.69       3.03500e-01    1.76300e-05    1.42317e-05    2.10283e-05    1      173.49
223.07       3.67200e-01    1.80800e-05    1.46965e-05    2.14635e-05    1      172.13
214.57       4.44300e-01    1.81400e-05    1.48218e-05    2.14582e-05    1      170.71
206.19       5.37600e-01    1.76800e-05    1.45110e-05    2.08490e-05    1      169.20
197.94       6.50600e-01    1.68300e-05    1.38620e-05    1.97980e-05    1      167.60
189.83       7.87200e-01    1.58500e-05    1.30926e-05    1.86074e-05    1      165.62
181.87       9.52500e-01    1.50400e-05    1.24129e-05    1.76671e-05    1      163.11
174.08       1.15300e+00    1.44600e-05    1.19452e-05    1.69748e-05    1      160.34
166.47       1.39500e+00    1.41300e-05    1.16634e-05    1.65966e-05    1      157.45
159.04       1.68800e+00    1.39300e-05    1.14339e-05    1.64261e-05    1      154.58
151.79       2.04200e+00    1.35700e-05    1.10782e-05    1.60618e-05    1      151.83
144.69       2.47100e+00    1.28600e-05    1.04438e-05    1.52762e-05    1      149.35
137.75       2.99000e+00    1.17100e-05    9.47136e-06    1.39486e-05    1      147.07
130.94       3.61800e+00    1.02200e-05    8.20862e-06    1.22314e-05    1      144.72
124.30       4.37700e+00    8.61700e-06    6.87363e-06    1.03604e-05    1      141.42
117.83       5.29700e+00    7.16500e-06    5.67878e-06    8.65122e-06    1      138.95
