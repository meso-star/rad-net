      program readrecast
      implicit none
      include 'max.inc'
c     Variables
      integer  NSPEC,NLAT,NLAYER,NTERM
      parameter(NSPEC=100,NLAT=100,NLAYER=100,NTERM=5)
      real WLNV(NSPEC),WLNV0(NSPEC),WLNV1(NSPEC)
      real LATI(NLAT) ,LAT0(NLAT)  ,LAT1(NLAT)
      real Z0(NLAT,NLAYER),Z1(NLAT,NLAYER)
      real THEXT(NLAT,NLAYER,NSPEC),THSCA(NLAT,NLAYER,NSPEC)
      real GH(NLAT,NLAYER,NSPEC)
      real TNEXT(NLAT,NLAYER,NSPEC),TNSCA(NLAT,NLAYER,NSPEC)
      real GN(NLAT,NLAYER,NSPEC)
      real TRAY(NLAT,NLAYER,NSPEC)
      real TGAS(NLAT,NLAYER,NSPEC,NTERM)
      real WEIGHT(NTERM,NSPEC)
      real TEMP(NLAT,NLAYER)
      real TSURF(NLAT)
      integer NTT(NSPEC)
      character*1 ch
      integer ig,j,k,n,i1,i2,i3,i4,ios
      integer nlatmx,nlayermx,nspecmx
      character*(Nchar_mx) filename
      double precision sum
c     label
      character*(Nchar_mx) label
      label='program readrecast'

 121  format(4(1x,i5),50(1x,e18.11))
 122  format(2(1x,i5),50(1x,e18.11))

      filename='./tableGCM.dat'
      open(unit=32,file=trim(filename),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(filename)
         stop
      else
         write(*,*) 'Reading file: ',trim(filename)
      endif
      read(32,*) nlatmx,nlayermx,nspecmx ! dimension du modele (nbre de latitudes, couches, intervalles spectraux)
      do ig=1,nlatmx            ! boucle sur latitudes 
         do j=1,nlayermx        ! boucle sur altitude
            do k=1,nspecmx      ! boucle sur longueur d'onde
               read(32,*) ntt(k) ! nombre de termes par canal (Correlated -K) 
               do n=1,ntt(k)
                  read(32,121) i1,i2,i3,i4,                        
     &                 wlnv(k)      ,lati(ig), ! longueur d'onde et latitude cellule         (microm�tre,�N) 
     &                 wlnv0(k)     ,wlnv1(k), ! borne intervalle spectral                   (microm�tre)
     &                 lat0(ig)     ,lat1(ig), ! borne latitude de la cellule                (�N)
     &                 z0(ig,j)     ,z1(ig,j), ! borne altitude de la cellule                (m�tre)  !!  z1>z0 
     &                 thext(ig,j,k), thsca(ig,j,k)   ,gh(ig,j,k), ! brume  : k_ext,k_sca, g=<cos>               (1/m, 1/m, / )
     &                 tnext(ig,j,k), tnsca(ig,j,k)   ,gn(ig,j,k), ! nuages : k_ext,k_sca, g=<cos>               (1/m, 1/m, / )
     &                 tray(ig,j,k) ,  tgas(ig,j,k,n) ,weight(n,k) ! gaz    : k_Rayleigh, k_abs, poids k-correle (1/m, 1/m, / ) 
               enddo
            enddo
         enddo
      enddo
c     Debug
      write(*,*) 'Number of latitude intervals:',nlatmx
      write(*,*) 'Number of altitude intervals:',nlayermx
      write(*,*) 'Number of spectral intervals:',nspecmx
      do k=1,nspecmx
         write(*,*) wlnv0(k),wlnv1(k)
         write(*,*) 'Nq=',ntt(k)
         sum=0.0D+0
         do n=1,ntt(k)
            sum=sum+weight(n,k)
         enddo                  ! n
         write(*,*) 'sum(w)=',sum
      enddo                     ! k
c     Debug

      filename='./tableTGCM.dat'
      open(unit=33,file=trim(filename),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(filename)
         stop
      else
         write(*,*) 'Reading file: ',trim(filename)
      endif
      read(33,*) nlatmx,nlayermx ! dimension du modele (nbre de latitudes, couches)
      do ig=1,nlatmx            ! boucle sur latitudes 
         do j=1,nlayermx        ! boucle sur altitude
            read(33,122) i1,i2,
     &           lat0(ig)     ,lat1(ig), ! borne latitude de la cellule                (�N)
     &           z0(ig,j)     ,z1(ig,j), ! borne altitude de la cellule                (m�tre)  !!  z1>z0 
     &           temp(ig,j)     ! temperature de la cellule                   (K)
         enddo                  !j
         read(33,122)  i1,i2,
     &        lat0(ig)     ,lat1(ig), ! borne latitude de la cellule                (�N)
     &        z0(ig,j)     ,z1(ig,j), ! borne altitude de la cellule                (m�tre)  !!  z1>z0 
     &        tsurf(ig)         ! temperature de la surface / colonne ig      (K)
      enddo                     ! ig
      
      end
